
RELEASE NOTES
=============

See README for information about Guppi's requirements and prerequisites.

Obviously, these release notes are wildly out-of-date.

Guppi 0.35.0 (Released 19 Dec 2000)
============

New and improved constraint-based layout system -- the old scheme was
too much of a hack, and my "simplifying assumptions" just seemed to be
making things more complicated.  The new scheme is more complicated,
but is also much more powerful.

Re-designed the plot object framework, fixing a number of problems
that had emerged during development.  (Lots of code had to be
rewritten to do this, but everything is much nicer now... and better
now then later, when the change could have been *really* painful.)

Introduced device-independent measurement system, to replace all of the
pixel-based measurements that had previously been used.  This sort of
thing is very important for producing good WYSIWIG printed output.

Added a printing framework, and wrote printing code for all of
the existing plug-ins.

Added lots of new plug-ins: pie charts, bar charts, line graphs,
legends, stock (price bar) charts.

Added new data types: curves, time series, stock price series.

Fix a zillion bugs & memory leaks.

Added color palettes for doing more sophisticated color gradients.

Re-worked plot configuration scheme.  The current way of doing it is nice,
but not fully realized in the plug-ins.

Added the concept of container objects.  This vastly simplifies
layout.

Added libguppitank, a simplified framework for embedding guppi that
hides a lot of complexity (and features :( ) and guarantees forward
binary compatibility.  This system is being used by the GnuCash people
to embed Guppi in that fine app.

Added support for Gnumeric embedding.  This is mostly due to Jody Goldberg.

Added the ability to read/write data objects in a native Guppi XML
format.

All sorts of other stuff.  Read the ChangeLog if you are curious. :)



Guppi 0.34.5 (Released 3 May 2000)
============

Added gettext support for internationalization.

Volunteers have been done a number of translations: Danish, German,
Icelandic, Norwegian, Russian and Swedish.

Added a new data type for integer sequences.

Added code to manage plot layouts, arranging the different elements
on the "page".

Fixed the axis element to behave like an axis and to work properly
with the layout manager.

Added a "boxplot" plug-in, which produces a boxplot for a single real
variable.  It is meant to be layed-out along side of another element,
like a scatter plot.

Started work on some simple command line args.

Beautified (IMHO) the splash screen.



Guppi 0.34.4 (Released 14 Apr 2000)
============

Finally figured out how to use gnome-print to produce nice
anti-aliased text.  Added a little label on the regression line
produced by the slinreg plug-in.

Added a prototypical "Axis" plug-in, whose behavior is very far from
what it ultimately should be.  It is mainly a proof-of-concept: yes,
we have nice looking labels.  Yes, they don't have a measurable impact
on performance.

Re-organized the plug-ins/ directory to divide up the plug-ins by
type, and removed some horrible hardwired hacks from my directory
searching/file finding functions.

Cleaned up some of the Guppi app code.  Guppi now remembers your most
recently opened documents across sessions.

Added some (unfinished, incomplete) code for dealing with symbolic math.
None of it is hooked into the app yet --- but this will be the
foundation for things like function plotting, non-linear fitting, etc.

You can now resize a view window and actually have the plot resize as
well, rather than stay the same size and just move to the middle of
the window.  The implementation is a hack, but proper geometry
management will one day change all that.



Guppi 0.34.3 (released 10 Mar 2000)
============

Made a bunch of optimizations to the scatter plot drawing functions.
Things are now a *lot* faster --- if you aren't doing any marker size
gradients, there are no function calls inside the inner drawing loop.
(Assuming you aren't using a non-standard GuppiSeqScalar
implementation that doesn't support guppi_seq_scalar_raw() access.)
Dragging 10^5 points on my not-so-fast machine (400mhz P2) is now just
a little jerky (but painless), while 10^4 points is beautifully
flicker-free.  

While doing the scatter plot optimizations, I also found and fixed a
few reference counting glitches related to alpha templates.

Added caching of the min and max in our core GuppiSeqScalar
implementation.  This really helps speed up redraws of scatter plots
with gradients.

Improved plot tools.  Added a pointer grab and some cute cursors.

GuppiData objects don't emit "changed" signals everywhere they should.
(Due to a combination of laziness and forgetfulness during the initial
coding, I'm afraid.)  I fixed GuppiSeqScalar, which should now do the
right thing.

Added libguppispecfns, a special functions library.  This code was
copied from Goose, which in turn copied it from the Cephes math
library.  The Cephes math library was written by Stephen L. Moshier,
who deserves all the credit.

Added libguppistat, a statistics library.  The only thing it has right
now is code to calculate simple linear regressions.

Added the slinreg plot plug-in.  It is a layer, meant to be
superimposed over another plot, that draws (and updates, as necessary)
a simple linear regression line.

Rationalized lots of the stuff in libguppiplot:
* Moved more functionality into the base classes.  This simplified
  things and eliminated duplicate code from the plug-ins.
* Added coupling mechanisms so that different items can act in concert
  with regards to scaling, data, etc., without knowing anything about
  each other.
* Clarified the relationship between scales and bounds (which I'd
  never really thought through clearly) in GuppiCanvasItems, and added
  bounding box negotiation for coupled items.



Guppi 0.34.2  (released 28 Feb 2000)
============

Made guile readline a configure-time option.  Even so, added check for
working guile readline, and conditionally enable readline initiation
based on the results.

Fixed some small brushing bugs.

Added a new framework for plot tools, and implemented a set of basic
tools for generic navigation and brushing.  For experimental purposes,
connected a bunch of tools up to various mouse-clicks.

Fixed GuppiScatterStateControl to allow more than just the front style to
be editted.



Guppi 0.34.1  (released 21 Feb 2000)
============

This version doesn't add much in the way of user-visible features.
Instead, it represents a re-engineering of the 0.33 code to increase
flexibility and modularity.

Data importing has been moved into a plug-in architecture.  The existing
delimited text import code was used as the basis for the first (and
currently only) such plug-in.

Guile bindings are now automatically generated by processing
language-neutral specification files.  The plan is for other language
binding to be generated from the same spec files, reducing the
up-front work required to adding new scripting languages.

The way that data objects are handled has been totally re-vamped.  The
bias towards linear containers has been removed, and interfaces and
implementations have been separated.

The code has otherwise been reorganized and rationalized in too many
ways to mention.



Guppi 0.33.2  (released 25 Jan 2000)
============

This release addresses some problems with the build, based on the helpful
feedback I've gotten from the brave souls who tried out 0.33.1.

I've removed the dependency on guile-gtk and guile-gnome, due to a
number of problems with those libraries.

I've also fixed a number of minor bugs and tweaked some things to get
rid of compiler warnings.



Guppi 0.33.1  (released 21 Jan 2000)
============

OK, here is a very preliminary release, the first of the "guppi3"
series.  This version has just enough functionality to get some pretty
pictures up on the screen.

There is a very primitive form of point-and-click navigation of
scatter plots available in this version.  This a pure hack, and the code
will totally be replaced in future versions.  It doesn't really work quite
right, so don't be alarmed if strange things happen.

The navigation works as follows:
                Unshifted          With Shift Key
  Left Button : Zoom in            Brush to Style #1
 Right Button : Zoom out           Brush to Style #2
Middle Button : Recenter view      Brush to Style #3

Style #1 is the style that you can change with the style window.  You
have no control over the other two styles.

There are some sample data files in the sample-data directory.  Some of
these are pathological files that were made to test the importer.  Good
ones to play with are "random.data" and "trig.data".

Here are some known issues:

* Guppi will gracelessly abort if you try to make a scatter plot where the
  x-data set and y-data set have different sizes.

* Certain things in the scatter plot style window don't work properly.

* Certain aspects of a scatter plot's style can't currently be adjusted
  in the scatter plot style window.

* There is a serious performance hit associated with using size-gradients
  in scatter plots.  (There are many optimizations that could speed this
  up, but this isn't really high on my priority list right now.)

* Right now, certain parts of Guppi scale very poorly.  For example,
  loading a data set with 10^5 elements is painfully slow and
  consumes massive amounts of memory.  This will be fixed.

* Plot axes are not yet supported.  Layed plots are not yet supported.
  Printing is not yet supported.  Saving is not yet supported.  In fact,
  almost everything is not yet supported.






