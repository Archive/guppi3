/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-boxplot-state.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_BOXPLOT_STATE_H
#define _INC_GUPPI_BOXPLOT_STATE_H

/* #include <gnome.h> */
#include <guppi-defs.h>
#include <guppi-seq-scalar.h>
#include <guppi-element-state.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiBoxplotState GuppiBoxplotState;
typedef struct _GuppiBoxplotStateClass GuppiBoxplotStateClass;

struct _GuppiBoxplotState {
  GuppiElementState parent;

  gboolean stats_ready;
  double q1, md, q3, whisker_lower, whisker_upper, iqr;
};

struct _GuppiBoxplotStateClass {
  GuppiElementStateClass parent_class;
};

#define GUPPI_TYPE_BOXPLOT_STATE (guppi_boxplot_state_get_type())
#define GUPPI_BOXPLOT_STATE(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_BOXPLOT_STATE,GuppiBoxplotState))
#define GUPPI_BOXPLOT_STATE0(obj) ((obj) ? (GUPPI_BOXPLOT_STATE(obj)) : NULL)
#define GUPPI_BOXPLOT_STATE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_BOXPLOT_STATE,GuppiBoxplotStateClass))
#define GUPPI_IS_BOXPLOT_STATE(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_BOXPLOT_STATE))
#define GUPPI_IS_BOXPLOT_STATE0(obj) (((obj) == NULL) || (GUPPI_IS_BOXPLOT_STATE(obj)))
#define GUPPI_IS_BOXPLOT_STATE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_BOXPLOT_STATE))

GtkType guppi_boxplot_state_get_type (void);

GuppiElementState *guppi_boxplot_state_new (void);

void   guppi_boxplot_state_prepare_stats (GuppiBoxplotState *);
void guppi_boxplot_state_get_size (GuppiBoxplotState *, double *w, double *h);



END_GUPPI_DECLS

#endif /* _INC_GUPPI_BOXPLOT_STATE_H */

/* $Id$ */
