/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-boxplot-view.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_BOXPLOT_VIEW_H
#define _INC_GUPPI_BOXPLOT_VIEW_H

/* #include <gnome.h> */
#include <guppi-defs.h>
#include <guppi-element-view.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiBoxplotView GuppiBoxplotView;
typedef struct _GuppiBoxplotViewClass GuppiBoxplotViewClass;

struct _GuppiBoxplotView {
  GuppiElementView parent;
};

struct _GuppiBoxplotViewClass {
  GuppiElementViewClass parent_class;
};

#define GUPPI_TYPE_BOXPLOT_VIEW (guppi_boxplot_view_get_type())
#define GUPPI_BOXPLOT_VIEW(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_BOXPLOT_VIEW,GuppiBoxplotView))
#define GUPPI_BOXPLOT_VIEW0(obj) ((obj) ? (GUPPI_BOXPLOT_VIEW(obj)) : NULL)
#define GUPPI_BOXPLOT_VIEW_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_BOXPLOT_VIEW,GuppiBoxplotViewClass))
#define GUPPI_IS_BOXPLOT_VIEW(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_BOXPLOT_VIEW))
#define GUPPI_IS_BOXPLOT_VIEW0(obj) (((obj) == NULL) || (GUPPI_IS_BOXPLOT_VIEW(obj)))
#define GUPPI_IS_BOXPLOT_VIEW_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_BOXPLOT_VIEW))

GtkType guppi_boxplot_view_get_type (void);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_BOXPLOT_VIEW_H */

/* $Id$ */
