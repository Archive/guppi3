/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-boxplot-print.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-boxplot-state.h"
#include "guppi-boxplot-print.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_boxplot_print_finalize (GtkObject *obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/* 
   There is too much code duplication between this and GuppiBoxplotItem...
*/
static void
print (GuppiElementPrint *ep)
{
  GuppiBoxplotState *state;
  ArtVpath path[16];
  ArtVpath boxpath[6];
  double tsize, bsize, q1, q3, md, iqr, t0, t1, ta, tb, tm, w, wu, wl;
  guint32 box_color, frame_color;
  gboolean horizontal;
  gint i, j;

  state = GUPPI_BOXPLOT_STATE (guppi_element_print_state (ep));

  if (! state->stats_ready) {
    guppi_boxplot_state_prepare_stats (state);
  }

  if (! state->stats_ready)
    return;

  guppi_element_state_get (guppi_element_print_state (ep),
			   "line_thickness", &w,
			   "tick_size", &tsize,
			   "box_size", &bsize,
			   "horizontal", &horizontal,
			   "box_color", &box_color,
			   "frame_color", &frame_color,
			   NULL);

  q1 = state->q1;
  q3 = state->q3;
  md = state->md;
  iqr = q3 - q1;
  wu = state->whisker_upper;
  wl = state->whisker_lower;

  if (horizontal)
    guppi_element_print_get_bbox (ep, NULL, &t0, NULL, &t1);
  else
    guppi_element_print_get_bbox (ep, &t0, NULL, &t1, NULL);

  /* Adjust for line thickness */

  tm = (t0 + t1) / 2;

  t0 = tm - bsize / 2;
  t1 = tm + bsize / 2;

  ta = tm - tsize / 2;
  tb = tm + tsize / 2;

  i = 0;
  j = 0;

  /* Draw low-end tick */
  path[i].code = ART_MOVETO_OPEN;
  path[i].x = wl;
  path[i].y = ta;
  ++i;

  path[i].code = ART_LINETO;
  path[i].x = path[i - 1].x;
  path[i].y = tb;
  ++i;

  /* Draw low-end whisker */
  path[i].code = ART_MOVETO_OPEN;
  path[i].x = path[i - 2].x;
  path[i].y = tm;
  ++i;

  path[i].code = ART_LINETO;
  path[i].x = q1;
  path[i].y = tm;
  ++i;

  /* Draw IQR frame */
  path[i].code = ART_LINETO;
  boxpath[j].code = ART_MOVETO;
  path[i].x = boxpath[j].x = q1;
  path[i].y = boxpath[j].y = t1;
  ++i;
  ++j;


  path[i].code = boxpath[j].code = ART_LINETO;
  path[i].x = boxpath[j].x = q1;
  path[i].y = boxpath[j].y = t0;
  ++i;
  ++j;

  path[i].code = boxpath[j].code = ART_LINETO;
  path[i].x = boxpath[j].x = q3;
  path[i].y = boxpath[j].y = t0;
  ++i;
  ++j;

  path[i].code = boxpath[j].code = ART_LINETO;
  path[i].x = boxpath[j].x = q3;
  path[i].y = boxpath[j].y = t1;
  ++i;
  ++j;

  path[i].code = boxpath[j].code = ART_LINETO;
  path[i].x = boxpath[j].x = path[i - 4].x;
  path[i].y = boxpath[j].y = path[i - 4].y;
  ++i;
  ++j;

  boxpath[j].code = ART_END;

  /* Draw median line */

  path[i].code = ART_MOVETO_OPEN;
  path[i].x = md;
  path[i].y = t0;
  ++i;

  path[i].code = ART_LINETO;
  path[i].x = md;
  path[i].y = t1;
  ++i;

  /* Draw high-end tick */

  path[i].code = ART_MOVETO_OPEN;
  path[i].x = wu;
  path[i].y = ta;
  ++i;

  path[i].code = ART_LINETO;
  path[i].x = path[i - 1].x;
  path[i].y = tb;
  ++i;

  /* Draw high-end whisker */

  path[i].code = ART_MOVETO_OPEN;
  path[i].x = path[i - 1].x;
  path[i].y = tm;
  ++i;

  path[i].code = ART_LINETO;
  path[i].x = q3;
  path[i].y = tm;
  ++i;

  path[i].code = ART_END;
  path[i].x = path[i].y = 0;


  if (horizontal) {
    /* Transform x-coordinates */
    for (i = 0; path[i].code != ART_END; ++i)
      guppi_element_print_vp2pt_auto (ep, &(path[i].x), NULL);

    for (i = 0; boxpath[i].code != ART_END; ++i)
      guppi_element_print_vp2pt_auto (ep, &(boxpath[i].x), NULL);
  } else {			/* if (guppi_boxplot_state_vertical(state)) */
    /* Transpose path coordinates and transform y-coordinates */
    for (i = 0; path[i].code != ART_END; ++i) {
      double t = path[i].x;
      path[i].x = path[i].y;
      path[i].y = t;
      guppi_element_print_vp2pt_auto (ep, NULL, &(path[i].y));
    }
    for (i = 0; boxpath[i].code != ART_END; ++i) {
      double t = boxpath[i].x;
      boxpath[i].x = boxpath[i].y;
      boxpath[i].y = t;
      guppi_element_print_vp2pt_auto (ep, NULL, &(boxpath[i].y));
    }
  }

  guppi_element_print_setrgbacolor_uint (ep, box_color);
  guppi_element_print_newpath (ep);
  guppi_element_print_vpath (ep, boxpath, FALSE);
  guppi_element_print_fill (ep);

  guppi_element_print_setrgbacolor_uint (ep,frame_color);
  guppi_element_print_setlinewidth (ep, w);
  guppi_element_print_newpath (ep);
  guppi_element_print_vpath (ep, path, FALSE);

  guppi_element_print_stroke (ep);
}

static void
guppi_boxplot_print_class_init (GuppiBoxplotPrintClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementPrintClass *ep_class = GUPPI_ELEMENT_PRINT_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_PRINT);

  object_class->finalize = guppi_boxplot_print_finalize;
  ep_class->print = print;

}

static void
guppi_boxplot_print_init (GuppiBoxplotPrint *obj)
{

}

GtkType guppi_boxplot_print_get_type (void)
{
  static GtkType guppi_boxplot_print_type = 0;
  if (!guppi_boxplot_print_type) {
    static const GtkTypeInfo guppi_boxplot_print_info = {
      "GuppiBoxplotPrint",
      sizeof (GuppiBoxplotPrint),
      sizeof (GuppiBoxplotPrintClass),
      (GtkClassInitFunc) guppi_boxplot_print_class_init,
      (GtkObjectInitFunc) guppi_boxplot_print_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_boxplot_print_type =
      gtk_type_unique (GUPPI_TYPE_ELEMENT_PRINT, &guppi_boxplot_print_info);
  }
  return guppi_boxplot_print_type;
}

/* $Id$ */
