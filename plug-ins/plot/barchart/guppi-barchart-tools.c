/* $Id$ */

/*
 * guppi-barchart-tools.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-barchart-tools.h"

#include <gdk/gdkkeysyms.h>

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>

#include "guppi-barchart-state.h"
#include "guppi-barchart-view.h"
#include "guppi-barchart-item.h"

static void
cycle_style_cb (GuppiPlotTool *tool, GuppiCanvasItem *gci)
{
  GuppiBarchartState *state = GUPPI_BARCHART_STATE (guppi_canvas_item_state (gci));
  gboolean stacked;
  gboolean normalized;
  gint C;

  /* It doesn't make sense to cycle between stacks & non-stacked if
     we just have one column worth of data. */
  guppi_barchart_state_table_dimensions (state, NULL, &C);
  if (C == 1)
    return;

  guppi_element_state_get ((GuppiElementState *) state,
			   "stacked", &stacked,
			   "normalize_stacks", &normalized, 
			   NULL);

  if (!stacked) {
    stacked = TRUE;
  } else if (stacked) {
    if (normalized) {
      stacked = normalized = FALSE;
    } else {
      normalized = TRUE;
    }
  }

  guppi_element_state_set ((GuppiElementState *) state,
			   "stacked", stacked,
			   "normalize_stacks", normalized, 
			   NULL);
}

GuppiPlotTool *
guppi_barchart_tool_new_cycle_style (void)
{
  GuppiPlotTool *tool;

  tool = guppi_plot_tool_new ();
  guppi_plot_tool_set_name (tool, _("Cycle Bar Chart Style"));
  tool->supported_type = GUPPI_TYPE_BARCHART_ITEM;
  tool->tracks_motion = FALSE;
  tool->repeating = FALSE;
  tool->first = cycle_style_cb;

  return tool;
}

/**************************************************************************/

static void
cycle_orientation_cb (GuppiPlotTool *tool, GuppiCanvasItem *gci)
{
  GuppiElementState *state = guppi_canvas_item_state (gci);
  gboolean vert;

  guppi_element_state_get (state, "vertical_bars", &vert, NULL);
  guppi_element_state_set (state, "vertical_bars", !vert, NULL);
}

GuppiPlotTool *
guppi_barchart_tool_new_cycle_orientation (void)
{
  GuppiPlotTool *tool;

  tool = guppi_plot_tool_new ();
  guppi_plot_tool_set_name (tool, _("Cycle Bar Chart Orientation"));
  tool->supported_type = GUPPI_TYPE_BARCHART_ITEM;
  tool->tracks_motion = FALSE;
  tool->repeating = FALSE;
  tool->first = cycle_orientation_cb;

  return tool;
}

/**************************************************************************/

#if 0
static void
bar_callback_cb (GuppiPlotTool *tool, GuppiCanvasItem *gci)
{
  GuppiBarchartState *state =
    GUPPI_BARCHART_STATE (guppi_canvas_item_state (gci));
  GuppiBarchartView *view =
    GUPPI_BARCHART_VIEW (guppi_canvas_item_view (gci));
  gint c, c0, c1, r, r0, r1;
  double x0, y0, x1, y1;
  double click_x, click_y;
  GuppiBarFunc func = tool->ptr_arg1;

  guppi_canvas_item_c2vp (gci, tool->raw_x, tool->raw_y, &click_x, &click_y);

  guppi_barchart_state_col_bounds (state, &c0, &c1);

  for (c = c0; c <= c1; ++c) {

    guppi_barchart_state_rows_in_col (state, c, &r0, &r1);

    for (r = r0; r <= r1; ++r) {

      guppi_barchart_view_bar_position (view, r, c, &x0, &y0, &x1, &y1);

      if (x0 <= click_x && click_x < x1 && y0 <= click_y && click_y < y1) {
	func (r, c, tool->ptr_arg2 /* == user data */ );
	return;
      }

    }
  }
}

GuppiPlotTool *
guppi_barchart_tool_new_bar_callback (GuppiBarFunc func,
				      const gchar *name, gpointer user_data)
{
  GuppiPlotTool *tool;

  g_return_val_if_fail (func != NULL, NULL);

  tool = guppi_plot_tool_new ();
  tool->name = guppi_strdup (name);
  tool->supported_type = GUPPI_TYPE_BARCHART_ITEM;
  tool->tracks_motion = FALSE;
  tool->repeating = FALSE;
  tool->first = bar_callback_cb;
  tool->ptr_arg1 = (gpointer) func;
  tool->ptr_arg2 = user_data;

  return tool;
}

#endif

/***************************************************************************/

GuppiPlotToolkit *
guppi_barchart_toolkit_new_default (void)
{
  GuppiPlotToolkit *tk = guppi_plot_toolkit_new (_("Default"));

  guppi_plot_toolkit_set_key_tool (tk,
				   GDK_Return, 0,
				   guppi_barchart_tool_new_cycle_style ());


  guppi_plot_toolkit_set_key_tool (tk,
				   GDK_space, 0,
				   guppi_barchart_tool_new_cycle_orientation
				   ());

  return tk;
}

GuppiPlotToolkit *
guppi_barchart_toolkit_style_toggle (void)
{
  GuppiPlotToolkit *tk = guppi_plot_toolkit_new (_("Toggle Style"));
  tk->toolbar_button_image = "barchart-stackswap.png";

  guppi_plot_toolkit_set_button_tool (tk,
				      1, 0,
				      guppi_barchart_tool_new_cycle_style ());


  guppi_plot_toolkit_set_button_tool (tk,
				      3, 0,
				      guppi_barchart_tool_new_cycle_orientation
				      ());


  guppi_plot_toolkit_set_key_tool (tk,
				   GDK_Return, 0,
				   guppi_barchart_tool_new_cycle_style ());


  guppi_plot_toolkit_set_key_tool (tk,
				   GDK_space, 0,
				   guppi_barchart_tool_new_cycle_orientation
				   ());

  return tk;
}


/* $Id$ */
