/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-barchart-print.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-barchart-print.h"

#include "guppi-barchart-view.h"
#include "guppi-barchart-state.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_barchart_print_finalize (GtkObject * obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/***************************************************************************/

static void
print (GuppiElementPrint * ep)
{
  GuppiBarchartState *state;
  GuppiElementView *view;
  GuppiBarchartView *bc_view;
  gint r, c, R, C;
  double x0, y0, x1, y1;
  double eth;
  guint32 edge_color, bar_color;

  state = GUPPI_BARCHART_STATE (guppi_element_print_state (ep));
  view = guppi_element_print_view (ep);
  bc_view = GUPPI_BARCHART_VIEW (guppi_element_print_view (ep));

  guppi_element_state_get (GUPPI_ELEMENT_STATE (state),
			   "edge_thickness", &eth,
			   "edge_color", &edge_color,
			   NULL);

  guppi_barchart_state_table_dimensions (state, &R, &C);

  guppi_element_print_setlinewidth (ep, eth);
  
  for (c = 0; c < C; ++c) {
    for (r = 0; r < R; ++r ) {

      guppi_barchart_view_bar_position (bc_view, r, c, &x0, &y0, &x1, &y1, &bar_color);

      /* Draw colored part of bar */
      guppi_element_print_setrgbacolor_uint (ep, bar_color);
      guppi_element_print_newpath (ep);
      guppi_element_print_moveto (ep, x0, y0);
      guppi_element_print_lineto (ep, x1, y0);
      guppi_element_print_lineto (ep, x1, y1);
      guppi_element_print_lineto (ep, x0, y1);
      guppi_element_print_closepath (ep);
      guppi_element_print_fill (ep);

      /* Draw edge of bar */
      guppi_element_print_setrgbacolor_uint (ep, edge_color);
      guppi_element_print_newpath (ep);
      guppi_element_print_moveto (ep, x0 + eth / 2, y0 + eth / 2);
      guppi_element_print_lineto (ep, x1 - eth / 2, y0 + eth / 2);
      guppi_element_print_lineto (ep, x1 - eth / 2, y1 - eth / 2);
      guppi_element_print_lineto (ep, x0 + eth / 2, y1 - eth / 2);
      guppi_element_print_closepath (ep);
      guppi_element_print_stroke (ep);
    }
  }
}

/***************************************************************************/

static void
guppi_barchart_print_class_init (GuppiBarchartPrintClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementPrintClass *ep_class = GUPPI_ELEMENT_PRINT_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_PRINT);

  object_class->finalize = guppi_barchart_print_finalize;

  ep_class->print = print;
}

static void
guppi_barchart_print_init (GuppiBarchartPrint * obj)
{

}

GtkType guppi_barchart_print_get_type (void)
{
  static GtkType guppi_barchart_print_type = 0;
  if (!guppi_barchart_print_type) {
    static const GtkTypeInfo guppi_barchart_print_info = {
      "GuppiBarchartPrint",
      sizeof (GuppiBarchartPrint),
      sizeof (GuppiBarchartPrintClass),
      (GtkClassInitFunc) guppi_barchart_print_class_init,
      (GtkObjectInitFunc) guppi_barchart_print_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_barchart_print_type =
      gtk_type_unique (GUPPI_TYPE_ELEMENT_PRINT, &guppi_barchart_print_info);
  }
  return guppi_barchart_print_type;
}

/* $Id$ */
