/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-barchart-view.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_BARCHART_VIEW_H
#define _INC_GUPPI_BARCHART_VIEW_H

/* #include <gnome.h> */
#include <guppi-defs.h>
#include <guppi-element-view.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiBarchartView GuppiBarchartView;
typedef struct _GuppiBarchartViewClass GuppiBarchartViewClass;

struct _GuppiBarchartView {
  GuppiElementView parent;
};

struct _GuppiBarchartViewClass {
  GuppiElementViewClass parent_class;
};

#define GUPPI_TYPE_BARCHART_VIEW (guppi_barchart_view_get_type())
#define GUPPI_BARCHART_VIEW(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_BARCHART_VIEW,GuppiBarchartView))
#define GUPPI_BARCHART_VIEW0(obj) ((obj) ? (GUPPI_BARCHART_VIEW(obj)) : NULL)
#define GUPPI_BARCHART_VIEW_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_BARCHART_VIEW,GuppiBarchartViewClass))
#define GUPPI_IS_BARCHART_VIEW(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_BARCHART_VIEW))
#define GUPPI_IS_BARCHART_VIEW0(obj) (((obj) == NULL) || (GUPPI_IS_BARCHART_VIEW(obj)))
#define GUPPI_IS_BARCHART_VIEW_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_BARCHART_VIEW))

GtkType guppi_barchart_view_get_type (void);

gboolean
guppi_barchart_view_bar_position (GuppiBarchartView *view,
				  gint r, gint c,
				  double *x0, double *y0,
				  double *x1, double *y1,
				  guint32 *color);

gboolean
guppi_barchart_view_find_bar_at_position (GuppiBarchartView *view,
					  double x, double y,
					  gint *r, gint *c);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_BARCHART_VIEW_H */

/* $Id$ */
