/* $Id$ */

/*
 * guppi-pie-tool.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_PIE_TOOL_H
#define _INC_GUPPI_PIE_TOOL_H

/* #include <gnome.h> */
#include <guppi-defs.h>
#include <guppi-plot-tool.h>
#include <guppi-plot-toolkit.h>

BEGIN_GUPPI_DECLS

typedef void (*GuppiPieSliceFunc) (gint i, gpointer user_data);

GuppiPlotTool *guppi_pie_tool_new_explode_all (void);
GuppiPlotTool *guppi_pie_tool_new_explode_slice (void);
GuppiPlotTool *guppi_pie_tool_new_change_radius (void);
GuppiPlotTool *guppi_pie_tool_new_spin (void);
GuppiPlotTool *guppi_pie_tool_new_fixed_spin (double theta_degrees);
GuppiPlotTool *guppi_pie_tool_new_slice_callback (GuppiPieSliceFunc,
						  const gchar * name,
						  gpointer user_data);


GuppiPlotToolkit *guppi_pie_toolkit_default (void);
GuppiPlotToolkit *guppi_pie_toolkit_explode (void);
GuppiPlotToolkit *guppi_pie_toolkit_spin (void);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_PIE_TOOL_H */

/* $Id$ */
