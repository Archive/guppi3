/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-legend-print.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-legend-print.h"

#include <guppi-memory.h>
#include "guppi-legend-state.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_legend_print_finalize (GtkObject * obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
print (GuppiElementPrint * ep)
{
  GuppiLegendState *state =
    GUPPI_LEGEND_STATE (guppi_element_print_state (ep));
  guint32 box_edge_color, label_color;
  double box_width, box_height, box_edge_thickness;
  double edge_margin, label_offset;
  double bw2, bh2, abw2, abh2, box_y_step;
  GnomeFont *font;
  double x0, y0, x1, y1;
  gint i, i0, i1, N;

  guppi_element_print_get_bbox (ep, &x0, &y0, &x1, &y1);

  guppi_element_state_get (GUPPI_ELEMENT_STATE (state),
			   "edge_color", &box_edge_color,
			   "label_color", &label_color,
			   "swatch_width", &box_width,
			   "swatch_height", &box_height,
			   "edge_thickness", &box_edge_thickness,
			   "edge_margin", &edge_margin,
			   "label_offset", &label_offset,
			   "label_font", &font,
			   NULL);

  bw2 = box_width / 2;
  bh2 = box_height / 2;

  abw2 = bw2 - box_edge_thickness / 2;
  abh2 = bh2 - box_edge_thickness / 2;

  guppi_legend_state_entry_bounds (state, &i0, &i1);
  N = i1 - i0 + 1;

  box_y_step = N > 1 ? (y1 - y0 - 2 * edge_margin - box_height) / (N - 1) : 0;

  guppi_element_print_setfont (ep, font);
  guppi_element_print_setlinewidth (ep, box_edge_thickness);

  for (i = i0; i <= i1; ++i) {
    const gchar *label_text;
    guint32 box_color;
    double box_x_center, box_y_center;
    gint j;

    label_text = guppi_legend_state_entry_text (state, i);
    box_color = guppi_legend_state_entry_color (state, i);

    box_y_center = y1 - edge_margin - box_height / 2 - (i-i0) * box_y_step;
    box_x_center = x0 + edge_margin + bw2;

    for (j = 0; j < 2; ++j) {
      guppi_element_print_setrgbacolor_uint (ep,
					     j ? box_edge_color : box_color);
      guppi_element_print_newpath (ep);
      guppi_element_print_moveto (ep, box_x_center - abw2,
				  box_y_center - abh2);
      guppi_element_print_lineto (ep, box_x_center + abw2,
				  box_y_center - abh2);
      guppi_element_print_lineto (ep, box_x_center + abw2,
				  box_y_center + abh2);
      guppi_element_print_lineto (ep, box_x_center - abw2,
				  box_y_center + abh2);
      guppi_element_print_closepath (ep);
      if (j == 0)
	guppi_element_print_fill (ep);
      else
	guppi_element_print_stroke (ep);
    }


    guppi_element_print_setrgbacolor_uint (ep, label_color);
    guppi_element_print_moveto (ep,
				box_x_center + bw2 + label_offset,
				box_y_center -
				gnome_font_get_ascender (font) / 2);
    guppi_element_print_show (ep, label_text);
  }

  guppi_unref (font);
}

static void
guppi_legend_print_class_init (GuppiLegendPrintClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementPrintClass *ep_class = GUPPI_ELEMENT_PRINT_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_PRINT);

  object_class->finalize = guppi_legend_print_finalize;
  ep_class->print = print;
}

static void
guppi_legend_print_init (GuppiLegendPrint * obj)
{

}

GtkType guppi_legend_print_get_type (void)
{
  static GtkType guppi_legend_print_type = 0;
  if (!guppi_legend_print_type) {
    static const GtkTypeInfo guppi_legend_print_info = {
      "GuppiLegendPrint",
      sizeof (GuppiLegendPrint),
      sizeof (GuppiLegendPrintClass),
      (GtkClassInitFunc) guppi_legend_print_class_init,
      (GtkObjectInitFunc) guppi_legend_print_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_legend_print_type = gtk_type_unique (GUPPI_TYPE_ELEMENT_PRINT,
					       &guppi_legend_print_info);
  }
  return guppi_legend_print_type;
}


/* $Id$ */
