/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-legend-item.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_LEGEND_ITEM_H
#define _INC_GUPPI_LEGEND_ITEM_H

/* #include <gnome.h> */
#include <guppi-defs.h>
#include <guppi-canvas-item.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiLegendItem GuppiLegendItem;
typedef struct _GuppiLegendItemClass GuppiLegendItemClass;

struct _GuppiLegendItem {
  GuppiCanvasItem parent;

  GList *labels;
};

struct _GuppiLegendItemClass {
  GuppiCanvasItemClass parent_class;

  void (*clicked_box) (GuppiLegendItem *, gint box, guint button,
		       guint state);
};

#define GUPPI_TYPE_LEGEND_ITEM (guppi_legend_item_get_type())
#define GUPPI_LEGEND_ITEM(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_LEGEND_ITEM,GuppiLegendItem))
#define GUPPI_LEGEND_ITEM0(obj) ((obj) ? (GUPPI_LEGEND_ITEM(obj)) : NULL)
#define GUPPI_LEGEND_ITEM_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_LEGEND_ITEM,GuppiLegendItemClass))
#define GUPPI_IS_LEGEND_ITEM(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_LEGEND_ITEM))
#define GUPPI_IS_LEGEND_ITEM0(obj) (((obj) == NULL) || (GUPPI_IS_LEGEND_ITEM(obj)))
#define GUPPI_IS_LEGEND_ITEM_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_LEGEND_ITEM))

GtkType guppi_legend_item_get_type (void);

gboolean guppi_legend_item_in_box (GuppiLegendItem *, gint x, gint y,
				   gint * box);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_LEGEND_ITEM_H */

/* $Id$ */
