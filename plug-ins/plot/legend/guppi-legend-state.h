/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-legend-state.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_LEGEND_STATE_H
#define _INC_GUPPI_LEGEND_STATE_H

/* #include <gnome.h> */
#include <guppi-defs.h>
#include <guppi-element-state.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiLegendState GuppiLegendState;
typedef struct _GuppiLegendStateClass GuppiLegendStateClass;

struct _GuppiLegendState {
  GuppiElementState parent;
};

struct _GuppiLegendStateClass {
  GuppiElementStateClass parent_class;
};

#define GUPPI_TYPE_LEGEND_STATE (guppi_legend_state_get_type())
#define GUPPI_LEGEND_STATE(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_LEGEND_STATE,GuppiLegendState))
#define GUPPI_LEGEND_STATE0(obj) ((obj) ? (GUPPI_LEGEND_STATE(obj)) : NULL)
#define GUPPI_LEGEND_STATE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_LEGEND_STATE,GuppiLegendStateClass))
#define GUPPI_IS_LEGEND_STATE(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_LEGEND_STATE))
#define GUPPI_IS_LEGEND_STATE0(obj) (((obj) == NULL) || (GUPPI_IS_LEGEND_STATE(obj)))
#define GUPPI_IS_LEGEND_STATE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_LEGEND_STATE))

GtkType guppi_legend_state_get_type (void);

GuppiElementState *guppi_legend_state_new (void);

void guppi_legend_state_entry_bounds (GuppiLegendState *, gint *i0, gint *i1);
guint32 guppi_legend_state_entry_color (GuppiLegendState *, gint i);
const gchar *guppi_legend_state_entry_text (GuppiLegendState *, gint i);

double guppi_legend_state_max_label_width (GuppiLegendState *);
double guppi_legend_state_natural_width (GuppiLegendState *);
double guppi_legend_state_natural_height (GuppiLegendState *);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_LEGEND_STATE_H */

/* $Id$ */
