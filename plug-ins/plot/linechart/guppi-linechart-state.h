/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-linechart-state.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_LINECHART_STATE_H
#define _INC_GUPPI_LINECHART_STATE_H

#include <gnome.h>
#include <guppi-defs.h>
#include <guppi-element-state.h>
#include <guppi-marker.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiLinechartState GuppiLinechartState;
typedef struct _GuppiLinechartStateClass GuppiLinechartStateClass;

struct _GuppiLinechartState {
  GuppiElementState parent;
};

struct _GuppiLinechartStateClass {
  GuppiElementStateClass parent_class;
};

#define GUPPI_TYPE_LINECHART_STATE (guppi_linechart_state_get_type ())
#define GUPPI_LINECHART_STATE(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_LINECHART_STATE,GuppiLinechartState))
#define GUPPI_LINECHART_STATE0(obj) ((obj) ? (GUPPI_LINECHART_STATE(obj)) : NULL)
#define GUPPI_LINECHART_STATE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_LINECHART_STATE,GuppiLinechartStateClass))
#define GUPPI_IS_LINECHART_STATE(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_LINECHART_STATE))
#define GUPPI_IS_LINECHART_STATE0(obj) (((obj) == NULL) || (GUPPI_IS_LINECHART_STATE(obj)))
#define GUPPI_IS_LINECHART_STATE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_LINECHART_STATE))

GtkType guppi_linechart_state_get_type (void);

GuppiElementState *guppi_linechart_state_new (void);

gboolean guppi_linechart_state_get_node_bounds (GuppiLinechartState *state, gint *i0, gint *i1);

gboolean guppi_linechart_state_get_node_properties (GuppiLinechartState *state,
						    gint i, 
						    double *value,
						    GuppiMarker *marker,
						    guint32 *color);




END_GUPPI_DECLS;

#endif /* _INC_GUPPI_LINECHART_STATE_H */

/* $Id$ */
