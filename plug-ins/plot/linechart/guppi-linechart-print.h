/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-linechart-print.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_LINECHART_PRINT_H
#define _INC_GUPPI_LINECHART_PRINT_H

#include <gnome.h>
#include <guppi-defs.h>
#include <guppi-element-print.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiLinechartPrint GuppiLinechartPrint;
typedef struct _GuppiLinechartPrintClass GuppiLinechartPrintClass;

struct _GuppiLinechartPrint {
  GuppiElementPrint parent;
};

struct _GuppiLinechartPrintClass {
  GuppiElementPrintClass parent_class;
};

#define GUPPI_TYPE_LINECHART_PRINT (guppi_linechart_print_get_type ())
#define GUPPI_LINECHART_PRINT(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_LINECHART_PRINT,GuppiLinechartPrint))
#define GUPPI_LINECHART_PRINT0(obj) ((obj) ? (GUPPI_LINECHART_PRINT(obj)) : NULL)
#define GUPPI_LINECHART_PRINT_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_LINECHART_PRINT,GuppiLinechartPrintClass))
#define GUPPI_IS_LINECHART_PRINT(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_LINECHART_PRINT))
#define GUPPI_IS_LINECHART_PRINT0(obj) (((obj) == NULL) || (GUPPI_IS_LINECHART_PRINT(obj)))
#define GUPPI_IS_LINECHART_PRINT_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_LINECHART_PRINT))

GtkType guppi_linechart_print_get_type (void);

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_LINECHART_PRINT_H */

/* $Id$ */
