/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-linechart-view.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_LINECHART_VIEW_H
#define _INC_GUPPI_LINECHART_VIEW_H

#include <gnome.h>
#include <guppi-defs.h>
#include <guppi-element-view.h>
#include <guppi-marker.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiLinechartView GuppiLinechartView;
typedef struct _GuppiLinechartViewClass GuppiLinechartViewClass;

struct _GuppiLinechartView {
  GuppiElementView parent;
};

struct _GuppiLinechartViewClass {
  GuppiElementViewClass parent_class;
};

#define GUPPI_TYPE_LINECHART_VIEW (guppi_linechart_view_get_type ())
#define GUPPI_LINECHART_VIEW(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_LINECHART_VIEW,GuppiLinechartView))
#define GUPPI_LINECHART_VIEW0(obj) ((obj) ? (GUPPI_LINECHART_VIEW(obj)) : NULL)
#define GUPPI_LINECHART_VIEW_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_LINECHART_VIEW,GuppiLinechartViewClass))
#define GUPPI_IS_LINECHART_VIEW(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_LINECHART_VIEW))
#define GUPPI_IS_LINECHART_VIEW0(obj) (((obj) == NULL) || (GUPPI_IS_LINECHART_VIEW(obj)))
#define GUPPI_IS_LINECHART_VIEW_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_LINECHART_VIEW))

GtkType guppi_linechart_view_get_type (void);

gboolean guppi_linechart_view_node_position (GuppiLinechartView *view,
					     gint i,
					     double *x, double *y,
					     GuppiMarker *marker, guint32 *color);
					     

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_LINECHART_VIEW_H */

/* $Id$ */
