/* $Id$ */

/*
 * guppi-scatter-state.h
 *
 * Copyright (C) 1999, 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_SCATTER_STATE_H
#define _INC_GUPPI_SCATTER_STATE_H

/* #include <gnome.h> */
#include <guppi-defs.h>
#include <guppi-element-state.h>
#include <guppi-seq-boolean.h>
#include <guppi-seq-scalar.h>
#include <guppi-alpha-template.h>
#include <guppi-marker.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiScatterState GuppiScatterState;
typedef struct _GuppiScatterStateClass GuppiScatterStateClass;

struct _GuppiScatterState {
  GuppiElementState object;

  GuppiMarker last_marker;
  double last_size1, last_size2, last_scale;
  GuppiPixbuf *last_pixbuf;
};

struct _GuppiScatterStateClass {
  GuppiElementStateClass parent_class;
};

#define GUPPI_TYPE_SCATTER_STATE (guppi_scatter_state_get_type())
#define GUPPI_SCATTER_STATE(obj) (GTK_CHECK_CAST((obj), \
                                  GUPPI_TYPE_SCATTER_STATE,GuppiScatterState))
#define GUPPI_SCATTER_STATE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass), \
                                          GUPPI_TYPE_SCATTER_STATE, \
					  GuppiScatterStateClass))
#define GUPPI_IS_SCATTER_STATE(obj) (GTK_CHECK_TYPE((obj), \
                                     GUPPI_TYPE_SCATTER_STATE))
#define GUPPI_IS_SCATTER_STATE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), \
                                    GUPPI_TYPE_SCATTER_STATE))

GtkType guppi_scatter_state_get_type (void);

GuppiElementState *guppi_scatter_state_new (void);

/* convenience functions */
GuppiSeqScalar  *guppi_scatter_state_get_x_data (GuppiScatterState *);
GuppiSeqScalar  *guppi_scatter_state_get_y_data (GuppiScatterState *);
int		 guppi_scatter_state_get_x_axis_type (GuppiScatterState *);
int		 guppi_scatter_state_get_y_axis_type (GuppiScatterState *);
GuppiSeqBoolean *guppi_scatter_state_get_mask_data (GuppiScatterState *);
GuppiSeqScalar  *guppi_scatter_state_get_color_data (GuppiScatterState *);
GuppiSeqScalar  *guppi_scatter_state_get_size1_data (GuppiScatterState *);
GuppiSeqScalar  *guppi_scatter_state_get_size2_data (GuppiScatterState *);

void guppi_scatter_state_set_x_data     (GuppiScatterState *, GuppiSeqScalar *);
void guppi_scatter_state_set_y_data     (GuppiScatterState *, GuppiSeqScalar *);
void guppi_scatter_state_set_x_axis_type (GuppiScatterState *, int);
void guppi_scatter_state_set_y_axis_type (GuppiScatterState *, int);
void guppi_scatter_state_set_mask_data  (GuppiScatterState *, GuppiSeqBoolean *);
void guppi_scatter_state_set_color_data (GuppiScatterState *, GuppiSeqScalar *);
void guppi_scatter_state_set_size1_data (GuppiScatterState *, GuppiSeqScalar *);
void guppi_scatter_state_set_size2_data (GuppiScatterState *, GuppiSeqScalar *);

gboolean guppi_scatter_state_get_point_properties (GuppiScatterState *,
						   gint index,
						   gboolean *visible,
						   GuppiMarker *marker,
						   guint32 *color,
						   double *size1,
						   double *size2);

GuppiPixbuf *guppi_scatter_state_get_point_pixbuf (GuppiScatterState *,
						   gint index,
						   double scale_factor,
						   guint32 *color);


gboolean guppi_scatter_state_closest_point (GuppiScatterState *,
					    double x, double y, double max_r,
					    double x_scale, double y_scale,
					    gint *index);

void guppi_scatter_state_brush_rectangle (GuppiScatterState *,
					  double x0, double y0,
					  double x1, double y1,
					  gboolean hidden);

void guppi_scatter_state_brush_circle (GuppiScatterState *,
				       double x, double y, double r_pixels,
				       double x_scale, double y_scale,
				       gboolean hidden);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_SCATTER-STATE_H */

/* $Id$ */
