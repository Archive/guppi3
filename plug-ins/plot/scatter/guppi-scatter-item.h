/* $Id$ */

/*
 * guppi-scatter-item.h
 *
 * Copyright (C) 1999 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_SCATTER_ITEM_H
#define _INC_GUPPI_SCATTER_ITEM_H

/* #include <gnome.h> */
#include <guppi-defs.h>
#include "guppi-canvas-item.h"
#include "guppi-scatter-state.h"

BEGIN_GUPPI_DECLS

typedef struct _GuppiScatterItem GuppiScatterItem;
typedef struct _GuppiScatterItemClass GuppiScatterItemClass;

struct _GuppiScatterItem {
  GuppiCanvasItem item;

  GuppiSeqBoolean *local_x_visibility_mask;
  GuppiSeqBoolean *local_y_visibility_mask;
};

struct _GuppiScatterItemClass {
  GuppiCanvasItemClass parent_class;
};

#define GUPPI_TYPE_SCATTER_ITEM (guppi_scatter_item_get_type())
#define GUPPI_SCATTER_ITEM(obj) (GTK_CHECK_CAST((obj), \
                                   GUPPI_TYPE_SCATTER_ITEM, \
                                   GuppiScatterItem))
#define GUPPI_SCATTER_ITEM_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass), \
                                           GUPPI_TYPE_SCATTER_ITEM, \
                                           GuppiScatterItemClass))
#define GUPPI_IS_SCATTER_ITEM(obj) (GTK_CHECK_TYPE((obj), \
                                      GUPPI_TYPE_SCATTER_Item))
#define GUPPI_IS_SCATTER_ITEM_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), \
                                              GUPPI_TYPE_SCATTER_ITEM))

GtkType guppi_scatter_item_get_type (void);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_SCATTER-ITEM_H */

/* $Id$ */
