/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-scatter-view.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-scatter-view.h"

#include <math.h>
#include <guppi-convenient.h>
#include <guppi-debug.h>
#include "guppi-scatter-state.h"
#include "guppi-scatter-print.h"
#include "guppi-scatter-item.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_scatter_view_finalize (GtkObject *obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/**************************************************************************/

static GuppiCanvasItem *
make_canvas_item (GuppiElementView *gev, GnomeCanvas *canvas,
		  GnomeCanvasGroup *group)
{
  GnomeCanvasItem *gci;

  gci = gnome_canvas_item_new (group, GUPPI_TYPE_SCATTER_ITEM, NULL);

  return GUPPI_CANVAS_ITEM (gci);
}

static void
changed_state (GuppiElementView *gev)
{
  GuppiScatterView *view;
  GuppiScatterState *state;
  GuppiSeqScalar *x_data;
  GuppiSeqScalar *y_data;
  guppi_uniq_t xuid, yuid;

  view = GUPPI_SCATTER_VIEW (gev);
  state = GUPPI_SCATTER_STATE (guppi_element_view_state (gev));

  x_data = guppi_scatter_state_get_x_data (state);
  y_data = guppi_scatter_state_get_y_data (state);

  xuid = x_data ? guppi_data_unique_id (GUPPI_DATA (x_data)) : 0;
  yuid = y_data ? guppi_data_unique_id (GUPPI_DATA (y_data)) : 0;

  if (! guppi_uniq_eq (view->last_x, xuid)) {
    guppi_element_view_set_preferred_view (gev, GUPPI_X_AXIS);
    view->last_x = xuid;
  }

  if (! guppi_uniq_eq (view->last_y, yuid)) {
    guppi_element_view_set_preferred_view (gev, GUPPI_Y_AXIS);
    view->last_y = yuid;
  }

  if (GUPPI_ELEMENT_VIEW_CLASS (parent_class)->changed_state)
    GUPPI_ELEMENT_VIEW_CLASS (parent_class)->changed_state (gev);
}

static void
view_init (GuppiElementView *gev)
{
  GuppiScatterState *state = GUPPI_SCATTER_STATE (guppi_element_view_state (gev));
  GuppiScatterView *view = GUPPI_SCATTER_VIEW (gev);
  GuppiSeqScalar *x_data;
  GuppiSeqScalar *y_data;

  x_data = guppi_scatter_state_get_x_data (state);
  y_data = guppi_scatter_state_get_y_data (state);

  view->last_x = x_data ? guppi_data_unique_id (GUPPI_DATA (x_data)) : 0;
  view->last_y = y_data ? guppi_data_unique_id (GUPPI_DATA (y_data)) : 0;
 
  guppi_element_view_set_axis_marker_type (gev, GUPPI_X_AXIS,
    guppi_scatter_state_get_x_axis_type (state));
  guppi_element_view_set_axis_marker_type (gev, GUPPI_Y_AXIS,
    guppi_scatter_state_get_y_axis_type (state));
}

static gboolean
valid_range (GuppiViewInterval *vi, GuppiSeqScalar *data, double *a, double *b)
{
  gint i, i0, i1;
  double min, max, w;
  gboolean first_min = TRUE, first_max = TRUE;

  min = guppi_seq_scalar_min (data);
  max = guppi_seq_scalar_max (data);
  
  /* Automatically do the "smart" range-finding when we have a reasonable number of points. */
  if (!(guppi_view_interval_valid (vi, min) && guppi_view_interval_valid (vi, max))) {

    /* FIXME: We should take our visibility mask into account here. */

    for (i = i0; i <= i1; ++i) {
      double x = guppi_seq_scalar_get (data, i);

      guppi_seq_bounds (GUPPI_SEQ (data), &i0, &i1);
      
      if (guppi_view_interval_valid (vi, x)) {

	if (first_min) {
	  min = x;
	  first_min = FALSE;
	} else {
	  if (x < min)
	    min = x;
	}

	if (first_max) {
	  max = x;
	  first_max = FALSE;
	} else {
	  if (x > max)
	    max = x;
	}

      }
    }

    if (first_min || first_max)
      return FALSE;
  }

  /* Add 5% in 'margins' */
  w = max - min;
  min -= w * 0.025;
  max += w * 0.025;

  if (a) 
    *a = min;
  if (b)
    *b = max;

  return TRUE;
}

static gboolean
preferred_range (GuppiElementView *view, guppi_axis_t ax, double *a, double *b)
{
  GuppiScatterState *state = GUPPI_SCATTER_STATE (guppi_element_view_state (view));
  GuppiSeqScalar *data;

  if (ax == GUPPI_X_AXIS)
    data = guppi_scatter_state_get_x_data (state);
  else if (ax == GUPPI_Y_AXIS)
    data = guppi_scatter_state_get_y_data (state);
  else
    return FALSE;

  if (data)
    return valid_range (guppi_element_view_axis_view_interval (view, ax), data, a, b);

  return FALSE;
}

/**************************************************************************/

static void
guppi_scatter_view_class_init (GuppiScatterViewClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementViewClass *view_class = GUPPI_ELEMENT_VIEW_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_VIEW);

  object_class->finalize = guppi_scatter_view_finalize;

  view_class->make_canvas_item = make_canvas_item;
  view_class->print_type       = GUPPI_TYPE_SCATTER_PRINT;
  view_class->changed_state    = changed_state;
  view_class->view_init        = view_init;

  view_class->preferred_range  = preferred_range;
}

static void
guppi_scatter_view_init (GuppiScatterView * obj)
{

}

GtkType guppi_scatter_view_get_type (void)
{
  static GtkType guppi_scatter_view_type = 0;
  if (!guppi_scatter_view_type) {
    static const GtkTypeInfo guppi_scatter_view_info = {
      "GuppiScatterView",
      sizeof (GuppiScatterView),
      sizeof (GuppiScatterViewClass),
      (GtkClassInitFunc) guppi_scatter_view_class_init,
      (GtkObjectInitFunc) guppi_scatter_view_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_scatter_view_type =
      gtk_type_unique (GUPPI_TYPE_ELEMENT_VIEW, &guppi_scatter_view_info);
  }
  return guppi_scatter_view_type;
}

/***************************************************************************/

/* $Id$ */
