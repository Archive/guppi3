/* This is -*- C -*- */
/* vim: set sw=2: */

/*
 * guppi-xybox-state.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_XYBOX_STATE_H
#define _INC_GUPPI_XYBOX_STATE_H

/* #include <gnome.h> */
#include <guppi-defs.h>
#include <guppi-group-state.h>

BEGIN_GUPPI_DECLS 

typedef struct _GuppiXYBoxState GuppiXYBoxState;
typedef struct _GuppiXYBoxStateClass GuppiXYBoxStateClass;

struct _GuppiXYBoxState {
  GuppiGroupState parent;
};

struct _GuppiXYBoxStateClass {
  GuppiGroupStateClass parent_class;
};

#define GUPPI_TYPE_XYBOX_STATE (guppi_xybox_state_get_type ())
#define GUPPI_XYBOX_STATE(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_XYBOX_STATE,GuppiXYBoxState))
#define GUPPI_XYBOX_STATE0(obj) ((obj) ? (GUPPI_XYBOX_STATE(obj)) : NULL)
#define GUPPI_XYBOX_STATE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_XYBOX_STATE,GuppiXYBoxStateClass))
#define GUPPI_IS_XYBOX_STATE(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_XYBOX_STATE))
#define GUPPI_IS_XYBOX_STATE0(obj) (((obj) == NULL) || (GUPPI_IS_XYBOX_STATE(obj)))
#define GUPPI_IS_XYBOX_STATE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_XYBOX_STATE))

GtkType guppi_xybox_state_get_type (void);
GuppiElementState *guppi_xybox_state_new (void);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_XYBOX_STATE_H */

/* $Id$ */
