/* This is -*- C -*- */
/* vim: set sw=2: */

/*
 * guppi-xybox-state.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-xybox-state.h"

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>

#include <guppi-memory.h>

#include "guppi-xybox-view.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_xybox_state_finalize (GtkObject * obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_xybox_state_class_init (GuppiXYBoxStateClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementStateClass *state_class = GUPPI_ELEMENT_STATE_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_GROUP_STATE);

  object_class->finalize = guppi_xybox_state_finalize;

  state_class->name = _("X-Y Box");
  state_class->view_type = GUPPI_TYPE_XYBOX_VIEW;
}

static void
guppi_xybox_state_init (GuppiXYBoxState * obj)
{

}

GtkType guppi_xybox_state_get_type (void)
{
  static GtkType guppi_xybox_state_type = 0;
  if (!guppi_xybox_state_type) {
    static const GtkTypeInfo guppi_xybox_state_info = {
      "GuppiXYBoxState",
      sizeof (GuppiXYBoxState),
      sizeof (GuppiXYBoxStateClass),
      (GtkClassInitFunc) guppi_xybox_state_class_init,
      (GtkObjectInitFunc) guppi_xybox_state_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_xybox_state_type =
      gtk_type_unique (GUPPI_TYPE_GROUP_STATE, &guppi_xybox_state_info);
  }
  return guppi_xybox_state_type;
}

GuppiElementState *
guppi_xybox_state_new (void)
{
  return GUPPI_ELEMENT_STATE (guppi_type_new (guppi_xybox_state_get_type ()));
}
