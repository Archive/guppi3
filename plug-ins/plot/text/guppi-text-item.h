/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-text-item.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_TEXT_ITEM_H
#define _INC_GUPPI_TEXT_ITEM_H

#include <gnome.h>
#include <guppi-raster-text.h>
#include <guppi-canvas-item.h>
#include <guppi-defs.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiTextItem GuppiTextItem;
typedef struct _GuppiTextItemClass GuppiTextItemClass;

struct _GuppiTextItem {
  GuppiCanvasItem parent;

  GuppiRasterText *rt;
};

struct _GuppiTextItemClass {
  GuppiCanvasItemClass parent_class;
};

#define GUPPI_TYPE_TEXT_ITEM (guppi_text_item_get_type())
#define GUPPI_TEXT_ITEM(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_TEXT_ITEM,GuppiTextItem))
#define GUPPI_TEXT_ITEM0(obj) ((obj) ? (GUPPI_TEXT_ITEM(obj)) : NULL)
#define GUPPI_TEXT_ITEM_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_TEXT_ITEM,GuppiTextItemClass))
#define GUPPI_IS_TEXT_ITEM(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_TEXT_ITEM))
#define GUPPI_IS_TEXT_ITEM0(obj) (((obj) == NULL) || (GUPPI_IS_TEXT_ITEM(obj)))
#define GUPPI_IS_TEXT_ITEM_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_TEXT_ITEM))

GtkType guppi_text_item_get_type (void);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_TEXT_ITEM_H */

/* $Id$ */
