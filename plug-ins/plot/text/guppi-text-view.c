/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-text-view.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <guppi-metrics.h>
#include "guppi-text-item.h"
#include "guppi-text-print.h"
#include "guppi-text-view.h"
#include "guppi-text-state.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_text_view_finalize (GtkObject * obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/***************************************************************************/

static GuppiCanvasItem *
make_canvas_item (GuppiElementView *view, GnomeCanvas *canvas,
		  GnomeCanvasGroup *group)
{
  GnomeCanvasItem *gci;

  gci = gnome_canvas_item_new (group, GUPPI_TYPE_TEXT_ITEM, NULL);

  return GUPPI_CANVAS_ITEM (gci);
}

static void
changed_state (GuppiElementView *view)
{
  if (GUPPI_ELEMENT_VIEW_CLASS (parent_class)->changed_state)
    GUPPI_ELEMENT_VIEW_CLASS (parent_class)->changed_state (view);
}

static void
changed (GuppiElementView *view)
{
  GuppiTextState *state = GUPPI_TEXT_STATE (guppi_element_view_state (view));
  GuppiTextBlock *block;

  block = guppi_text_state_get_block (state);

  if (GUPPI_ELEMENT_VIEW_CLASS (parent_class)->changed)
    GUPPI_ELEMENT_VIEW_CLASS (parent_class)->changed (view);
}

/***************************************************************************/

static void
guppi_text_view_class_init (GuppiTextViewClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementViewClass *view_class = GUPPI_ELEMENT_VIEW_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_VIEW);

  object_class->finalize = guppi_text_view_finalize;

  view_class->make_canvas_item = make_canvas_item;
  view_class->print_type       = GUPPI_TYPE_TEXT_PRINT;
  view_class->changed_state    = changed_state;
  view_class->changed          = changed;
}

static void
guppi_text_view_init (GuppiTextView *obj)
{

}

GtkType guppi_text_view_get_type (void)
{
  static GtkType guppi_text_view_type = 0;
  if (!guppi_text_view_type) {
    static const GtkTypeInfo guppi_text_view_info = {
      "GuppiTextView",
      sizeof (GuppiTextView),
      sizeof (GuppiTextViewClass),
      (GtkClassInitFunc) guppi_text_view_class_init,
      (GtkObjectInitFunc) guppi_text_view_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_text_view_type =
      gtk_type_unique (GUPPI_TYPE_ELEMENT_VIEW, &guppi_text_view_info);
  }
  return guppi_text_view_type;
}


/* $Id$ */
