/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-text-item.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <guppi-convenient.h>
#include <guppi-useful.h>
#include "guppi-text-state.h"
#include "guppi-text-view.h"
#include "guppi-text-item.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_text_item_finalize (GtkObject * obj)
{
  GuppiTextItem *item = GUPPI_TEXT_ITEM (obj);

  guppi_unref0 (item->rt);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/***************************************************************************/

static void
update (GuppiCanvasItem * gci, double aff[6], ArtSVP * svp, gint flags)
{
  GuppiTextBlock *block;
  GuppiTextItem *item;

  item = GUPPI_TEXT_ITEM (gci);

  block = guppi_text_state_get_block (GUPPI_TEXT_STATE (guppi_canvas_item_state (gci)));

  if (item->rt == NULL) 
    item->rt = guppi_raster_text_new (block);

  guppi_raster_text_set_scale (item->rt, guppi_canvas_item_scale (gci));
}

static void
render (GuppiCanvasItem * gci, GnomeCanvasBuf * buf)
{
  GuppiTextItem *gti;
  GuppiAlphaTemplate *template = NULL;
  gint cx, cy, cx0, cy0, cx1, cy1;
  guint r, g, b, a;
  double sc;
  double tm, bm, lm, rm;
  gboolean gradient;
  guint32 color, color_final;

  gti = GUPPI_TEXT_ITEM (gci);

  guppi_element_state_get (guppi_canvas_item_state (gci),
			   "top_margin",    &tm,
			   "bottom_margin", &bm,
			   "left_margin",   &lm,
			   "right_margin",  &rm,
			   "gradient",      &gradient,
			   "color",         &color,
			   "color_final",   &color_final,
			   NULL);

  sc = guppi_canvas_item_scale (gci);

  guppi_canvas_item_get_bbox_c (gci, &cx0, &cy0, &cx1, &cy1);

  if (gti->rt)
    template = guppi_raster_text_template (gti->rt);

  if (template) {
    gint w = template->width, h = template->height;

    cx = (cx0 + cx1 - w) / 2;
    cy = (cy0 + cy1 - h) / 2;

    cx += guppi_x_pt2px (lm - rm) / 2;
    cy += guppi_y_pt2px (tm - bm) / 2;

    if (gradient) {
      guppi_alpha_template_gradient_print (template, cx, cy, color, color_final, GUPPI_NORTH, buf);
    } else {
      UINT_TO_RGBA (color, &r, &g, &b, &a);
      guppi_alpha_template_print (template, cx, cy, r, g, b, a, buf);
    }
  }
}

/***************************************************************************/

static void
guppi_text_item_class_init (GuppiTextItemClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiCanvasItemClass *gci_class = GUPPI_CANVAS_ITEM_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_CANVAS_ITEM);

  object_class->finalize = guppi_text_item_finalize;

  gci_class->guppi_update = update;
  gci_class->guppi_render = render;

}

static void
guppi_text_item_init (GuppiTextItem * obj)
{

}

GtkType guppi_text_item_get_type (void)
{
  static GtkType guppi_text_item_type = 0;
  if (!guppi_text_item_type) {
    static const GtkTypeInfo guppi_text_item_info = {
      "GuppiTextItem",
      sizeof (GuppiTextItem),
      sizeof (GuppiTextItemClass),
      (GtkClassInitFunc) guppi_text_item_class_init,
      (GtkObjectInitFunc) guppi_text_item_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_text_item_type =
      gtk_type_unique (GUPPI_TYPE_CANVAS_ITEM, &guppi_text_item_info);
  }
  return guppi_text_item_type;
}




/* $Id$ */
