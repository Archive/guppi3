/* $Id$ */

/*
 * guppi-axis-tools.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>

#include <guppi-view-interval.h>
#include "guppi-axis-tools.h"
#include "guppi-axis-state.h"
#include "guppi-axis-view.h"
#include "guppi-axis-item.h"

static void
toggle_log_cb (GuppiPlotTool *tool, GuppiCanvasItem *gci)
{
  GuppiAxisState *state = GUPPI_AXIS_STATE (guppi_canvas_item_state (gci));
  GuppiElementView *view = guppi_canvas_item_view (gci);
  GuppiViewInterval *vi;
  guppi_compass_t pos;

  guppi_element_state_get (GUPPI_ELEMENT_STATE (state),
			   "position", &pos,
			   NULL);

  vi = guppi_element_view_axis_view_interval (view, (pos == GUPPI_NORTH || pos == GUPPI_SOUTH) ? GUPPI_X_AXIS : GUPPI_Y_AXIS);

  if (guppi_view_interval_is_logarithmic (vi))
    guppi_view_interval_scale_linearly (vi);
  else
    guppi_view_interval_scale_logarithmically (vi, 10);
}

GuppiPlotTool *
guppi_axis_tool_new_toggle_log_scale (void)
{
  GuppiPlotTool *tool;

  tool = guppi_plot_tool_new ();
  tool->name = _("Switch Between Log/Linear Scale");
  tool->tracks_motion = FALSE;
  tool->repeating = FALSE;
  tool->first = toggle_log_cb;

  return tool;
}




/* $Id$ */
