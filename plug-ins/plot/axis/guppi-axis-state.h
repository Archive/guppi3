/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-axis-state.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_AXIS_STATE_H
#define _INC_GUPPI_AXIS_STATE_H

/* #include <gnome.h> */
#include <guppi-defs.h>
#include <libgnomeprint/gnome-font.h>
#include <guppi-data.h>
#include <guppi-axis-markers.h>

#include <guppi-enums.h>
#include <guppi-element-state.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiAxisState GuppiAxisState;
typedef struct _GuppiAxisStateClass GuppiAxisStateClass;

struct _GuppiAxisState {
  GuppiElementState parent;
};

struct _GuppiAxisStateClass {
  GuppiElementStateClass parent_class;
};

#define GUPPI_TYPE_AXIS_STATE (guppi_axis_state_get_type())
#define GUPPI_AXIS_STATE(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_AXIS_STATE,GuppiAxisState))
#define GUPPI_AXIS_STATE0(obj) ((obj) ? (GUPPI_AXIS_STATE(obj)) : NULL)
#define GUPPI_AXIS_STATE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_AXIS_STATE,GuppiAxisStateClass))
#define GUPPI_IS_AXIS_STATE(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_AXIS_STATE))
#define GUPPI_IS_AXIS_STATE0(obj) (((obj) == NULL) || (GUPPI_IS_AXIS_STATE(obj)))
#define GUPPI_IS_AXIS_STATE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_AXIS_STATE))

GtkType guppi_axis_state_get_type (void);

GuppiElementState *guppi_axis_state_new (void);

void guppi_axis_state_tick_properties (GuppiAxisState *,
				       const GuppiTick *,
				       gboolean *show_tick,
				       guint32 *color,
				       double *thickness,
				       double *length,
				       gboolean *show_label,
				       double *label_offset,
				       guint32 *label_color,
				       GnomeFont **label_font);

gchar *guppi_axis_state_displayed_legend (GuppiAxisState *);
double guppi_axis_state_legend_span (GuppiAxisState *);

double guppi_axis_state_maximum_span (GuppiAxisState *, double label_scale, GuppiAxisMarkers *);

double guppi_axis_state_label_shrink_to_fit_factor (GuppiAxisState *,
						    GuppiAxisMarkers *,
						    double span);

void guppi_axis_state_get_size (GuppiAxisState *ax, double label_scale, GuppiAxisMarkers *, double *w, double *h);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_AXIS_STATE_H */

/* $Id$ */
