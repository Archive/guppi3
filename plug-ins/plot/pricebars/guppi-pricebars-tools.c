/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-pricebars-tools.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>

#include <guppi-basic-tools.h>
#include "guppi-pricebars-tools.h"

GuppiPlotToolkit *
guppi_pricebars_toolkit_default (void)
{
  GuppiPlotToolkit *tk = guppi_plot_toolkit_new (_("Default"));

  guppi_plot_toolkit_set_button_tool (tk, 1, 0,
				      guppi_basic_tool_new_rescale (0.9));
  guppi_plot_toolkit_set_button_tool (tk, 2, 0, guppi_basic_tool_new_drag ());
  guppi_plot_toolkit_set_button_tool (tk, 3, 0,
				      guppi_basic_tool_new_rescale (1 / 0.9));

  guppi_add_keyboard_navigation_to_toolkit (tk);

  return tk;
}


/* $Id$ */
