/* This is -*- C -*- */
/* vim: set sw=2: */

/*
 * guppi-pricebars-item.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <guppi-convenient.h>
#include <guppi-rgb.h>
#include "guppi-pricebars-state.h"
#include "guppi-pricebars-view.h"
#include "guppi-pricebars-item.h"
#include "guppi-pricebars-tools.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_pricebars_item_destroy (GtkObject *obj)
{
  if (parent_class->destroy)
    parent_class->destroy (obj);
}

static void
guppi_pricebars_item_finalize (GtkObject *obj)
{
  GuppiPricebarsItem *item = GUPPI_PRICEBARS_ITEM (obj);


  guppi_free (item->t_buf);
  guppi_free (item->op_buf);
  guppi_free (item->hi_buf);
  guppi_free (item->lo_buf);
  guppi_free (item->cl_buf);
  
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
changed_state (GuppiCanvasItem *gci)
{
  GuppiPricebarsItem *item = GUPPI_PRICEBARS_ITEM (gci);

  guppi_free0 (item->t_buf);
  guppi_free0 (item->op_buf);
  guppi_free0 (item->hi_buf);
  guppi_free0 (item->lo_buf);
  guppi_free0 (item->cl_buf);

  if (GUPPI_CANVAS_ITEM_CLASS (parent_class)->changed_state)
    GUPPI_CANVAS_ITEM_CLASS (parent_class)->changed_state (gci);
}

static void
update (GuppiCanvasItem *gci, double aff[6], ArtSVP *clip_path, gint flags)
{
  GuppiPricebarsItem *item = GUPPI_PRICEBARS_ITEM (gci);
  GuppiPriceSeries *ser;
  double cx0, cx1;
  const GDate *sd, *ed;
  gint N;

  guppi_element_state_get (guppi_canvas_item_state (gci),
			   "data", &ser,
			   NULL);

  if (ser == NULL || guppi_date_indexed_empty (GUPPI_DATE_INDEXED (ser))) {
    guppi_unref (ser);
    return;
  }
  
  guppi_canvas_item_vp2c_d (gci, 0, 0, &cx0, NULL);
  guppi_canvas_item_vp2c_d (gci, 1, 0, &cx1, NULL);
  
  item->pixels_per_day = cx1 - cx0;
  item->horiz_tick_size = MIN ((gint)floor (item->pixels_per_day/2), 5);

  if (item->t_buf == NULL) {

    sd = guppi_date_indexed_start (GUPPI_DATE_INDEXED (ser));
    ed = guppi_date_indexed_end   (GUPPI_DATE_INDEXED (ser));
    N = g_date_julian ((GDate *) ed) - g_date_julian ((GDate *) sd) + 1;
  
    item->t_buf = g_new (double, N);
    item->op_buf = g_new (double, N);
    item->hi_buf = g_new (double, N);
    item->lo_buf = g_new (double, N);
    item->cl_buf = g_new (double, N);

    item->Nbuf = guppi_price_series_get_range (ser, sd, ed,
					       item->t_buf,
					       item->op_buf,
					       item->hi_buf,
					       item->lo_buf,
					       item->cl_buf);
  }

  guppi_unref (ser);
}

static void
render (GuppiCanvasItem *gci, GnomeCanvasBuf *buf)
{
  GuppiPricebarsItem* pb_item;
  GuppiPricebarsState *state;
  GuppiElementView *view;
  GuppiPriceSeries *ser;
  double sc;
  double x0, y0, x1, y1;
  gint r, g, b, a, i, hts;
  guint32 color;
  gint jul;
  double pr_c[4], t;
      
  state = GUPPI_PRICEBARS_STATE (guppi_canvas_item_state (gci));

  guppi_element_state_get (guppi_canvas_item_state (gci),
			   "data", &ser,
			   "color", &color,
			   NULL);

  if (ser == NULL || guppi_date_indexed_empty (GUPPI_DATE_INDEXED (ser))) {
    guppi_unref (ser);
    return;
  }

  UINT_TO_RGBA (color, &r, &g, &b, &a);
  
  pb_item = GUPPI_PRICEBARS_ITEM (gci);
  view = guppi_canvas_item_view (gci);

  guppi_canvas_item_get_bbox_vp (gci, &x0, &y0, &x1, &y1);

  sc = guppi_canvas_item_scale (gci);
  hts = (gint)rint (pb_item->horiz_tick_size * sc);

  for (i=0; i < pb_item->Nbuf && pb_item->t_buf[i] < x0; ++i);

  if (pb_item->pixels_per_day < 1) {

    /* The "sparse" renderer. */
    
    gint pending_x=-G_MAXINT, pending_min_y=0, pending_max_y=0;
    gint lo_y, hi_y;

    for (; i<pb_item->Nbuf; ++i) {
      t = pb_item->t_buf[i];
      if (t > x1)
	break;
      
      pr_c[0] = pb_item->hi_buf[i];
      pr_c[1] = pb_item->lo_buf[i];
      guppi_canvas_item_vp2c (gci, t, 0, &jul, NULL);
      guppi_canvas_item_y_vp2c_d_bulk (gci, pr_c, pr_c, 2);
      hi_y = (gint) pr_c[0];
      lo_y = (gint) pr_c[1];

      if (jul == pending_x) {
	if (hi_y < pending_min_y)
	  pending_min_y = hi_y;
	if (lo_y > pending_max_y)
	  pending_max_y = lo_y;
      } else {
	if (pending_x != G_MAXINT) {
	  PAINT_VERT (buf, r, g, b, a, pending_x, pending_min_y, pending_max_y);
	}
	pending_x = jul;
	pending_min_y = hi_y;
	pending_max_y = lo_y;
      }

    }

    PAINT_VERT (buf, r, g, b, a, pending_x, pending_min_y, pending_max_y);

  } else {
    
    for (; i<pb_item->Nbuf; ++i) {
      t = pb_item->t_buf[i];
      if (t > x1)
	break;
    
      pr_c[0] = pb_item->op_buf[i];
      pr_c[1] = pb_item->hi_buf[i];
      pr_c[2] = pb_item->lo_buf[i];
      pr_c[3] = pb_item->cl_buf[i];

      guppi_canvas_item_vp2c (gci, t, 0, &jul, NULL);
      guppi_canvas_item_y_vp2c_d_bulk (gci, pr_c, pr_c, 4);

      if (hts > 0) {
	PAINT_HORIZ (buf, r, g, b, a, jul-hts, jul, (gint)pr_c[0]);
	PAINT_HORIZ (buf, r, g, b, a, jul+1, jul+hts, (gint)pr_c[3]);
      }
      
      PAINT_VERT (buf, r, g, b, a, jul, (gint)pr_c[1]-1, (gint)pr_c[2]+1);
    }

  }

}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
guppi_pricebars_item_class_init (GuppiPricebarsItemClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;
  GuppiCanvasItemClass *gci_class = GUPPI_CANVAS_ITEM_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_CANVAS_ITEM);

  object_class->destroy = guppi_pricebars_item_destroy;
  object_class->finalize = guppi_pricebars_item_finalize;

  gci_class->changed_state = changed_state;
  gci_class->guppi_update = update;
  gci_class->guppi_render = render;
  gci_class->uses_vp_coordinates = TRUE;

  guppi_canvas_item_class_set_item_class_toolkit (gci_class,
						  guppi_pricebars_toolkit_default ());

}

static void
guppi_pricebars_item_init (GuppiPricebarsItem *obj)
{

}

GtkType
guppi_pricebars_item_get_type (void)
{
  static GtkType guppi_pricebars_item_type = 0;
  if (!guppi_pricebars_item_type) {
    static const GtkTypeInfo guppi_pricebars_item_info = {
      "GuppiPricebarsItem",
      sizeof (GuppiPricebarsItem),
      sizeof (GuppiPricebarsItemClass),
      (GtkClassInitFunc)guppi_pricebars_item_class_init,
      (GtkObjectInitFunc)guppi_pricebars_item_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    guppi_pricebars_item_type = gtk_type_unique (GUPPI_TYPE_CANVAS_ITEM, &guppi_pricebars_item_info);
  }
  return guppi_pricebars_item_type;
}

