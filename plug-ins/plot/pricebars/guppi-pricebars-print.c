/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-pricebars-print.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-pricebars-print.h"

#include <guppi-memory.h>
#include <guppi-convenient.h>
#include "guppi-pricebars-state.h"
#include "guppi-pricebars-view.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_pricebars_print_finalize (GtkObject *obj)
{
  guppi_finalized (obj);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
print (GuppiElementPrint *ep)
{
  GuppiPricebarsState *state;
  GuppiElementView *view;
  GuppiPriceSeries *ser;
  GDate start_date, end_date, dt;
  guint32 color;
  double horiz_tick_size, h0, h1, x0, x1;

  state = GUPPI_PRICEBARS_STATE (guppi_element_print_state (ep));
  guppi_element_state_get (guppi_element_print_state (ep),
			   "data", &ser,
			   "color", &color,
			   NULL);

  view = guppi_element_print_view (ep);

  if (ser == NULL || guppi_date_indexed_empty (GUPPI_DATE_INDEXED (ser))) {
    guppi_unref (ser);
    return;
  }

  guppi_pricebars_view_date_range (GUPPI_PRICEBARS_VIEW (view),
				   &start_date, &end_date);

  /* Make our ticks equal to 45% of the length of the distance
     between any two days. */
  guppi_element_print_vp2pt (ep, 0, 0, &h0, NULL);
  guppi_element_print_vp2pt (ep, 1, 0, &h1, NULL);
  horiz_tick_size = 0.45 * (h1-h0);

  /* Don't make 'em too big */
  guppi_element_print_get_bbox (ep, &x0, NULL, &x1, NULL);
  horiz_tick_size = MIN (horiz_tick_size, 0.01*(x1-x0));

  /* Below a certain size, don't even bother. */
  if (horiz_tick_size < 72/32.0)
    horiz_tick_size = 0;


  guppi_element_print_setrgbacolor_uint (ep, color);
  guppi_element_print_setlinewidth (ep, 0);

  for (dt=start_date; g_date_lteq (&dt, &end_date); g_date_add_days (&dt, 1)) {
    double x = g_date_julian (&dt);
    guint valid;

    valid = guppi_price_series_valid (ser, &dt);

    if ((valid & PRICE_HIGH) && (valid & PRICE_LOW)) {
      double hi = guppi_price_series_get (ser, PRICE_HIGH, &dt);
      double lo = guppi_price_series_get (ser, PRICE_LOW, &dt);

      guppi_element_print_newpath (ep);
      guppi_element_print_moveto_vp (ep, x, hi);
      guppi_element_print_lineto_vp (ep, x, lo);
      guppi_element_print_stroke (ep);
    }

    if ((valid & PRICE_OPEN) && horiz_tick_size > 0) {
      double xx;
      double op = guppi_price_series_get (ser, PRICE_OPEN, &dt);

      guppi_element_print_vp2pt (ep, x, op, &xx, &op);
      
      guppi_element_print_newpath (ep);
      guppi_element_print_moveto (ep, xx, op);
      guppi_element_print_lineto (ep, xx - horiz_tick_size, op);
      guppi_element_print_stroke (ep);
    }

    if ((valid & PRICE_CLOSE) && horiz_tick_size > 0) {
      double xx;
      double cl = guppi_price_series_get (ser, PRICE_CLOSE, &dt);

      guppi_element_print_vp2pt (ep, x, cl, &xx, &cl);
      
      guppi_element_print_newpath (ep);
      guppi_element_print_moveto (ep, xx, cl);
      guppi_element_print_lineto (ep, xx + horiz_tick_size, cl);
      guppi_element_print_stroke (ep);
    }
  }

  guppi_unref (ser);

}

static void
guppi_pricebars_print_class_init (GuppiPricebarsPrintClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;
  GuppiElementPrintClass *print_class = GUPPI_ELEMENT_PRINT_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_PRINT);

  print_class->print = print;

  object_class->finalize = guppi_pricebars_print_finalize;
}

static void
guppi_pricebars_print_init (GuppiPricebarsPrint *obj)
{

}

GtkType
guppi_pricebars_print_get_type (void)
{
  static GtkType guppi_pricebars_print_type = 0;
  if (!guppi_pricebars_print_type) {
    static const GtkTypeInfo guppi_pricebars_print_info = {
      "GuppiPricebarsPrint",
      sizeof (GuppiPricebarsPrint),
      sizeof (GuppiPricebarsPrintClass),
      (GtkClassInitFunc)guppi_pricebars_print_class_init,
      (GtkObjectInitFunc)guppi_pricebars_print_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    guppi_pricebars_print_type = gtk_type_unique (GUPPI_TYPE_ELEMENT_PRINT, &guppi_pricebars_print_info);
  }
  return guppi_pricebars_print_type;
}

GtkObject *
guppi_pricebars_print_new (void)
{
  return GTK_OBJECT (guppi_type_new (guppi_pricebars_print_get_type ()));
}
