/* This is -*- C -*- */
/* vim: set sw=2: */

/*
 * guppi-pricebars-state.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-pricebars-state.h"

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>

#include <guppi-memory.h>
#include <guppi-rgb.h>
#include <guppi-metrics.h>
#include <guppi-data-flavor.h>
#include <guppi-data-socket.h>
#include <guppi-price-series.h>

#include "guppi-pricebars-view.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_pricebars_state_finalize (GtkObject *obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_pricebars_state_class_init (GuppiPricebarsStateClass *klass)
{
  GtkObjectClass* object_class = (GtkObjectClass *)klass;
  GuppiElementStateClass *state_class = GUPPI_ELEMENT_STATE_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_STATE);

  object_class->finalize = guppi_pricebars_state_finalize;

  state_class->name = _("Price Bars");
  state_class->view_type = GUPPI_TYPE_PRICEBARS_VIEW;
}

static void
guppi_pricebars_state_init (GuppiPricebarsState *obj)
{
  GuppiAttributeBag *bag = guppi_element_state_attribute_bag (GUPPI_ELEMENT_STATE (obj));
  double inch = guppi_in2pt (1.0);

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DATA_SOCKET, "data::socket::adopt", NULL,
					guppi_data_socket_new_by_type (GUPPI_TYPE_PRICE_SERIES));

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "thickness", NULL, inch/32);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA,      "color",     NULL, RGBA_BLUE);

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_INT, "preferred_days", NULL, 365);
}

GtkType
guppi_pricebars_state_get_type (void)
{
  static GtkType guppi_pricebars_state_type = 0;
  if (!guppi_pricebars_state_type) {
    static const GtkTypeInfo guppi_pricebars_state_info = {
      "GuppiPricebarsState",
      sizeof (GuppiPricebarsState),
      sizeof (GuppiPricebarsStateClass),
      (GtkClassInitFunc)guppi_pricebars_state_class_init,
      (GtkObjectInitFunc)guppi_pricebars_state_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    guppi_pricebars_state_type = gtk_type_unique (GUPPI_TYPE_ELEMENT_STATE, &guppi_pricebars_state_info);
  }
  return guppi_pricebars_state_type;
}

GuppiElementState *
guppi_pricebars_state_new (void)
{
  return GUPPI_ELEMENT_STATE (guppi_type_new (guppi_pricebars_state_get_type ()));
}

