/* This is -*- C -*- */
/* vim: set sw=2: */

/*
 * guppi-pricebars-state.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_PRICEBARS_STATE_H
#define _INC_GUPPI_PRICEBARS_STATE_H

/* #include <gnome.h> */
#include <guppi-defs.h>
#include <guppi-element-state.h>
#include <guppi-price-series.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiPricebarsState GuppiPricebarsState;
typedef struct _GuppiPricebarsStateClass GuppiPricebarsStateClass;

struct _GuppiPricebarsState {
  GuppiElementState parent;
};

struct _GuppiPricebarsStateClass {
  GuppiElementStateClass parent_class;
};

#define GUPPI_TYPE_PRICEBARS_STATE (guppi_pricebars_state_get_type ())
#define GUPPI_PRICEBARS_STATE(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_PRICEBARS_STATE,GuppiPricebarsState))
#define GUPPI_PRICEBARS_STATE0(obj) ((obj) ? (GUPPI_PRICEBARS_STATE(obj)) : NULL)
#define GUPPI_PRICEBARS_STATE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_PRICEBARS_STATE,GuppiPricebarsStateClass))
#define GUPPI_IS_PRICEBARS_STATE(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_PRICEBARS_STATE))
#define GUPPI_IS_PRICEBARS_STATE0(obj) (((obj) == NULL) || (GUPPI_IS_PRICEBARS_STATE(obj)))
#define GUPPI_IS_PRICEBARS_STATE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_PRICEBARS_STATE))

GtkType guppi_pricebars_state_get_type (void);

GuppiElementState *guppi_pricebars_state_new (void);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_PRICEBARS_STATE_H */

/* $Id$ */
