/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-image-print.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <guppi-memory.h>
#include "guppi-image-print.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_image_print_finalize (GtkObject *obj)
{
  GuppiImagePrint *x = GUPPI_IMAGE_PRINT(obj);

  guppi_finalized (obj);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_image_print_class_init (GuppiImagePrintClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_PRINT);

  object_class->finalize = guppi_image_print_finalize;

}

static void
guppi_image_print_init (GuppiImagePrint *obj)
{

}

GtkType
guppi_image_print_get_type (void)
{
  static GtkType guppi_image_print_type = 0;
  if (!guppi_image_print_type) {
    static const GtkTypeInfo guppi_image_print_info = {
      "GuppiImagePrint",
      sizeof (GuppiImagePrint),
      sizeof (GuppiImagePrintClass),
      (GtkClassInitFunc)guppi_image_print_class_init,
      (GtkObjectInitFunc)guppi_image_print_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    guppi_image_print_type = gtk_type_unique (GUPPI_TYPE_ELEMENT_PRINT, &guppi_image_print_info);
  }
  return guppi_image_print_type;
}
