/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-image-state.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-image-state.h"

#include <guppi-memory.h>
#include <guppi-metrics.h>
#include "guppi-image-view.h"

static GtkObjectClass *parent_class = NULL;

GuppiElementState *
guppi_image_state_new (void)
{
  return GUPPI_ELEMENT_STATE (guppi_type_new (guppi_image_state_get_type ()));
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
guppi_image_state_finalize (GtkObject *obj)
{
  GuppiImageState *x = GUPPI_IMAGE_STATE(obj);

  guppi_pixbuf_unref (x->pixbuf);

  guppi_finalized (obj);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
changed (GuppiElementState *state)
{
  GuppiImageState *img_state = GUPPI_IMAGE_STATE (state);
  const gchar *attr;
  gchar *fn;

  attr = guppi_element_state_get_changed_attribute (state);
  if (attr && !strcmp (attr, "filename")) {
    GdkPixbuf *pixbuf;

    guppi_element_state_get (state,
			     "filename", &fn,
			     NULL);

    guppi_pixbuf_unref (img_state->pixbuf);
    img_state->pixbuf = NULL;

    pixbuf = gdk_pixbuf_new_from_file (fn);
    if (pixbuf) {
      img_state->pixbuf = guppi_pixbuf_new (pixbuf);
      guppi_element_state_changed_size (state,
					guppi_x_px2pt (gdk_pixbuf_get_width (pixbuf)),
					guppi_y_px2pt (gdk_pixbuf_get_height (pixbuf)));
      gdk_pixbuf_unref (pixbuf);
    } else {
      guppi_element_state_changed_size (state, 0, 0);
    }

    guppi_free (fn);
  }
}

static void
guppi_image_state_class_init (GuppiImageStateClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;
  GuppiElementStateClass *state_class = GUPPI_ELEMENT_STATE_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_STATE);

  object_class->finalize = guppi_image_state_finalize;

  state_class->name = _("Image");
  state_class->view_type = GUPPI_TYPE_IMAGE_VIEW;
  state_class->changed = changed;
}

static void
guppi_image_state_init (GuppiImageState *obj)
{
  GuppiAttributeBag *bag = guppi_element_state_attribute_bag (GUPPI_ELEMENT_STATE (obj));
  
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_STRING,  "filename",     NULL, NULL);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN, "scale_x",      NULL, FALSE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN, "scale_y",      NULL, FALSE);
}

GtkType
guppi_image_state_get_type (void)
{
  static GtkType guppi_image_state_type = 0;
  if (!guppi_image_state_type) {
    static const GtkTypeInfo guppi_image_state_info = {
      "GuppiImageState",
      sizeof (GuppiImageState),
      sizeof (GuppiImageStateClass),
      (GtkClassInitFunc)guppi_image_state_class_init,
      (GtkObjectInitFunc)guppi_image_state_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    guppi_image_state_type = gtk_type_unique (GUPPI_TYPE_ELEMENT_STATE, &guppi_image_state_info);
  }
  return guppi_image_state_type;
}


