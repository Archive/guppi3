/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-slinreg-state.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>

#include <guppi-convenient.h>
#include <guppi-defaults.h>
#include "guppi-slinreg-state.h"
#include "guppi-slinreg-view.h"
#include "guppi-slinreg-statviewer.h"

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0,
  ARG_X_DATA,
  ARG_Y_DATA,
  ARG_LINE_WIDTH,
  ARG_LINE_COLOR,
  ARG_SHOW_LABEL,
  ARG_LABEL_FONT,
  ARG_LABEL_SIZE,
  ARG_LABEL_COLOR
};

static void
guppi_slinreg_state_get_arg (GtkObject * obj, GtkArg * arg, guint arg_id)
{
  GuppiSlinregState *state = GUPPI_SLINREG_STATE (obj);

  switch (arg_id) {

  case ARG_X_DATA:
    GTK_VALUE_POINTER (*arg) = guppi_slinreg_state_x_data (state);
    break;

  case ARG_Y_DATA:
    GTK_VALUE_POINTER (*arg) = guppi_slinreg_state_y_data (state);
    break;

  case ARG_LINE_WIDTH:
    GTK_VALUE_DOUBLE (*arg) = guppi_slinreg_state_line_width (state);
    break;

  case ARG_LINE_COLOR:
    GTK_VALUE_UINT (*arg) = guppi_slinreg_state_line_color (state);
    break;

  case ARG_SHOW_LABEL:
    GTK_VALUE_BOOL (*arg) = guppi_slinreg_state_show_label (state);
    break;

  case ARG_LABEL_FONT:
    GTK_VALUE_POINTER (*arg) = guppi_slinreg_state_label_font (state);
    break;

  case ARG_LABEL_SIZE:
    GTK_VALUE_DOUBLE (*arg) = guppi_slinreg_state_label_size (state);
    break;

  case ARG_LABEL_COLOR:
    GTK_VALUE_UINT (*arg) = guppi_slinreg_state_label_color (state);
    break;

  default:
    break;
  };
}

static void
guppi_slinreg_state_set_arg (GtkObject * obj, GtkArg * arg, guint arg_id)
{
  GuppiSlinregState *state = GUPPI_SLINREG_STATE (obj);

  switch (arg_id) {
  case ARG_X_DATA:
    guppi_slinreg_state_set_x_data (state,
				    GUPPI_SEQ_SCALAR0 (GTK_VALUE_POINTER
						       (*arg)));
    break;

  case ARG_Y_DATA:
    guppi_slinreg_state_set_y_data (state,
				    GUPPI_SEQ_SCALAR0 (GTK_VALUE_POINTER
						       (*arg)));
    break;

  case ARG_LINE_WIDTH:
    guppi_slinreg_state_set_line_width (state, GTK_VALUE_DOUBLE (*arg));
    break;

  case ARG_LINE_COLOR:
    guppi_slinreg_state_set_line_color (state, GTK_VALUE_UINT (*arg));
    break;

  case ARG_SHOW_LABEL:
    guppi_slinreg_state_set_show_label (state, GTK_VALUE_BOOL (*arg));
    break;

  case ARG_LABEL_FONT:
    guppi_slinreg_state_set_label_font (state,
					GNOME_FONT (GTK_VALUE_POINTER
						    (*arg)));
    break;

  case ARG_LABEL_SIZE:
    guppi_slinreg_state_set_label_size (state, GTK_VALUE_DOUBLE (*arg));
    break;

  case ARG_LABEL_COLOR:
    guppi_slinreg_state_set_label_color (state, GTK_VALUE_UINT (*arg));
    break;

  default:
    break;
  };
}

static void
guppi_slinreg_state_destroy (GtkObject * obj)
{
  if (parent_class->destroy)
    parent_class->destroy (obj);
}

static void
guppi_slinreg_state_finalize (GtkObject * obj)
{
  GuppiSlinregState *state = GUPPI_SLINREG_STATE (obj);

  guppi_unref0 (state->slr);

  guppi_unref0 (state->label_font);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/***************************************************************************/

static GuppiElementView *
make_view (GuppiElementState * state)
{
  return GUPPI_ELEMENT_VIEW (guppi_type_new (GUPPI_TYPE_SLINREG_VIEW));
}

/***************************************************************************/

static GuppiConfigItem *
config_tree (GuppiElementState * es)
{
  GuppiSlinregState *state = GUPPI_SLINREG_STATE (es);
  GuppiConfigItem *info_node;
  GuppiConfigItem *config_node;

  info_node = guppi_config_item_new (_("Regression Results"),
				     _("Results"),
				     (GtkWidget *
				      (*)(gpointer))
				     guppi_slinreg_statviewer_new,
				     state->slr);

  config_node =
    guppi_config_item_new (_("Configure Simple Linear Regression"),
			   _("Configure"), NULL, state);

  guppi_config_item_append (info_node, config_node);
  guppi_unref0 (config_node);

  return info_node;
}

/***************************************************************************/

static void
guppi_slinreg_state_class_init (GuppiSlinregStateClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementStateClass *state_class = GUPPI_ELEMENT_STATE_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_STATE);

  object_class->get_arg = guppi_slinreg_state_get_arg;
  object_class->set_arg = guppi_slinreg_state_set_arg;
  object_class->destroy = guppi_slinreg_state_destroy;
  object_class->finalize = guppi_slinreg_state_finalize;

  state_class->name = _("Simple Linear Regression");

  state_class->make_view = make_view;
  state_class->config_tree = config_tree;

  gtk_object_add_arg_type ("GuppiSlinregState::x_data", GTK_TYPE_POINTER,
			   GTK_ARG_READWRITE, ARG_X_DATA);
  gtk_object_add_arg_type ("GuppiSlinregState::y_data", GTK_TYPE_POINTER,
			   GTK_ARG_READWRITE, ARG_Y_DATA);
  gtk_object_add_arg_type ("GuppiSlinregState::line_width", GTK_TYPE_DOUBLE,
			   GTK_ARG_READWRITE, ARG_LINE_WIDTH);
  gtk_object_add_arg_type ("GuppiSlinregState::line_color", GTK_TYPE_UINT,
			   GTK_ARG_READWRITE, ARG_LINE_COLOR);
  gtk_object_add_arg_type ("GuppiSlinregState::show_label", GTK_TYPE_BOOL,
			   GTK_ARG_READWRITE, ARG_SHOW_LABEL);
  gtk_object_add_arg_type ("GuppiSlinregState::label_font", GTK_TYPE_POINTER,
			   GTK_ARG_READWRITE, ARG_LABEL_FONT);
  gtk_object_add_arg_type ("GuppiSlinregState::label_size", GTK_TYPE_DOUBLE,
			   GTK_ARG_READWRITE, ARG_LABEL_SIZE);
  gtk_object_add_arg_type ("GuppiSlinregState::label_color", GTK_TYPE_UINT,
			   GTK_ARG_READWRITE, ARG_LABEL_COLOR);
}

static void
sh_x_change (GuppiShared * sh, GuppiSlinregState * state)
{
  guppi_simple_linreg_set_x_data (state->slr,
				  GUPPI_SEQ_SCALAR0 (guppi_shared_get (sh)));

}

static void
sh_y_change (GuppiShared * sh, GuppiSlinregState * state)
{
  guppi_simple_linreg_set_y_data (state->slr,
				  GUPPI_SEQ_SCALAR0 (guppi_shared_get (sh)));
}

static void
guppi_slinreg_state_init (GuppiSlinregState * obj)
{
  GuppiElementState *state = GUPPI_ELEMENT_STATE (obj);
  GuppiShared *xsh;
  GuppiShared *ysh;

  guppi_element_state_add_shared (state, SHARED_X_DATA,
				  xsh = guppi_shared_data ());
  guppi_element_state_add_shared (state, SHARED_Y_DATA,
				  ysh = guppi_shared_data ());

  gtk_signal_connect (GTK_OBJECT (xsh), "changed",
		      GTK_SIGNAL_FUNC (sh_x_change), obj);
  gtk_signal_connect (GTK_OBJECT (ysh), "changed",
		      GTK_SIGNAL_FUNC (sh_y_change), obj);

  obj->line_width = 3.5;
  obj->line_color = 0xff00ff8f;

  obj->show_label = TRUE;

  obj->label_font = guppi_default_font ();
  guppi_ref (obj->label_font);

  obj->label_size = 12;
  obj->label_color = 0x000000ff;

  obj->slr = guppi_simple_linreg_new ();
}

GtkType guppi_slinreg_state_get_type (void)
{
  static GtkType guppi_slinreg_state_type = 0;
  if (!guppi_slinreg_state_type) {
    static const GtkTypeInfo guppi_slinreg_state_info = {
      "GuppiSlinregState",
      sizeof (GuppiSlinregState),
      sizeof (GuppiSlinregStateClass),
      (GtkClassInitFunc) guppi_slinreg_state_class_init,
      (GtkObjectInitFunc) guppi_slinreg_state_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_slinreg_state_type =
      gtk_type_unique (GUPPI_TYPE_ELEMENT_STATE, &guppi_slinreg_state_info);
  }
  return guppi_slinreg_state_type;
}

GuppiElementState *
guppi_slinreg_state_new (void)
{
  return GUPPI_ELEMENT_STATE (guppi_type_new (guppi_slinreg_state_get_type ()));
}

GuppiSeqScalar *
guppi_slinreg_state_x_data (GuppiSlinregState * state)
{
  GtkObject *obj;
  g_return_val_if_fail (state != NULL
			&& GUPPI_IS_SLINREG_STATE (state), NULL);
  obj =
    guppi_element_state_get_shared (GUPPI_ELEMENT_STATE (state),
				    SHARED_X_DATA);
  return GUPPI_SEQ_SCALAR0 (obj);
}

GuppiSeqScalar *
guppi_slinreg_state_y_data (GuppiSlinregState * state)
{
  GtkObject *obj;
  g_return_val_if_fail (state != NULL
			&& GUPPI_IS_SLINREG_STATE (state), NULL);
  obj =
    guppi_element_state_get_shared (GUPPI_ELEMENT_STATE (state),
				    SHARED_Y_DATA);
  return GUPPI_SEQ_SCALAR0 (obj);
}

void
guppi_slinreg_state_set_x_data (GuppiSlinregState * state,
				GuppiSeqScalar * sd)
{
  g_return_if_fail (state != NULL && GUPPI_IS_SLINREG_STATE (state));

  guppi_element_state_set_shared (GUPPI_ELEMENT_STATE (state),
				  SHARED_X_DATA, sd);
}

void
guppi_slinreg_state_set_y_data (GuppiSlinregState * state,
				GuppiSeqScalar * sd)
{
  g_return_if_fail (state != NULL && GUPPI_IS_SLINREG_STATE (state));

  guppi_element_state_set_shared (GUPPI_ELEMENT_STATE (state),
				  SHARED_Y_DATA, sd);
}

void
guppi_slinreg_state_set_line_width (GuppiSlinregState * state, double x)
{
  g_return_if_fail (state != NULL);
  g_return_if_fail (x >= 0);

  if (state->line_width != x) {
    state->line_width = x;
    guppi_element_state_changed (GUPPI_ELEMENT_STATE (state));
  }
}

void
guppi_slinreg_state_set_line_color (GuppiSlinregState * state, guint32 x)
{
  g_return_if_fail (state != NULL);

  if (state->line_color != x) {
    state->line_color = x;
    guppi_element_state_changed (GUPPI_ELEMENT_STATE (state));
  }
}

void
guppi_slinreg_state_set_show_label (GuppiSlinregState * state, gboolean x)
{
  g_return_if_fail (state != NULL);

  if (state->show_label != x) {
    state->show_label = x;
    guppi_element_state_changed (GUPPI_ELEMENT_STATE (state));
  }
}

void
guppi_slinreg_state_set_label_font (GuppiSlinregState * state,
				    GnomeFont * font)
{
  g_return_if_fail (state != NULL);
  g_return_if_fail (font != NULL);

  if (state->label_font != font) {

    guppi_refcounting_assign (state->label_font, font);

    guppi_element_state_changed (GUPPI_ELEMENT_STATE (state));
  }
}

void
guppi_slinreg_state_set_label_size (GuppiSlinregState * state, double x)
{
  g_return_if_fail (state != NULL);
  g_return_if_fail (x > 0);

  if (state->label_size != x) {
    state->label_size = x;
    guppi_element_state_changed (GUPPI_ELEMENT_STATE (state));
  }
}

void
guppi_slinreg_state_set_label_color (GuppiSlinregState * state, guint32 x)
{
  g_return_if_fail (state != NULL);

  if (state->label_color != x) {
    state->label_color = x;
    guppi_element_state_changed (GUPPI_ELEMENT_STATE (state));
  }
}

gchar *
guppi_slinreg_state_label (GuppiSlinregState * state)
{
  double m, b, r2;

  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (GUPPI_IS_SLINREG_STATE (state), NULL);

  if (state->slr == NULL || !state->slr->valid)
    return NULL;

  m = guppi_simple_linreg_slope (state->slr);
  b = guppi_simple_linreg_intercept (state->slr);
  r2 = guppi_simple_linreg_R_squared (state->slr);

  return guppi_strdup_printf ("y=%.3gx+%.3g, R^2=%.3g", m, b, r2);
}

/* $Id$ */
