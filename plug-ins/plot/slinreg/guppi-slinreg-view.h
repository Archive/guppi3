/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-slinreg-view.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_SLINREG_VIEW_H
#define _INC_GUPPI_SLINREG_VIEW_H

/* #include <gnome.h> */
#include <guppi-element-view.h>
#include <guppi-defs.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiSlinregView GuppiSlinregView;
typedef struct _GuppiSlinregViewClass GuppiSlinregViewClass;

struct _GuppiSlinregView {
  GuppiElementView parent;
};

struct _GuppiSlinregViewClass {
  GuppiElementViewClass parent_class;
};

#define GUPPI_TYPE_SLINREG_VIEW (guppi_slinreg_view_get_type())
#define GUPPI_SLINREG_VIEW(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_SLINREG_VIEW,GuppiSlinregView))
#define GUPPI_SLINREG_VIEW0(obj) ((obj) ? (GUPPI_SLINREG_VIEW(obj)) : NULL)
#define GUPPI_SLINREG_VIEW_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_SLINREG_VIEW,GuppiSlinregViewClass))
#define GUPPI_IS_SLINREG_VIEW(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_SLINREG_VIEW))
#define GUPPI_IS_SLINREG_VIEW0(obj) (((obj) == NULL) || (GUPPI_IS_SLINREG_VIEW(obj)))
#define GUPPI_IS_SLINREG_VIEW_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_SLINREG_VIEW))

GtkType guppi_slinreg_view_get_type (void);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_SLINREG_VIEW_H */

/* $Id$ */
