/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-linegraph-item.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <math.h>

#include <libgnomeui/gnome-canvas.h>
#include <libgnomeui/gnome-canvas-util.h>
#include <libart_lgpl/libart.h>

#include <guppi-useful.h>
#include <guppi-i18n.h>
#include <guppi-basic-tools.h>
#include "guppi-linegraph-item.h"
#include "guppi-linegraph-view.h"
#include "guppi-linegraph-state.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_linegraph_item_destroy (GtkObject *obj)
{
  if (parent_class->destroy)
    parent_class->destroy (obj);
}

static void
guppi_linegraph_item_finalize (GtkObject *obj)
{
  GuppiLinegraphItem *item = GUPPI_LINEGRAPH_ITEM (obj);

  if (item->path) {
    guppi_free (item->path);
    item->path = NULL;
  }

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/**************************************************************************/

static void
update (GuppiCanvasItem *gci, double aff[6], ArtSVP *svp, gint flags)
{
  GuppiLinegraphItem *item = GUPPI_LINEGRAPH_ITEM (gci);
  GuppiLinegraphView *view = GUPPI_LINEGRAPH_VIEW (guppi_canvas_item_view (gci));
  GuppiLinegraphState *state = GUPPI_LINEGRAPH_STATE (guppi_canvas_item_state (gci));
  double sc = guppi_canvas_item_scale (gci);
  gint cx0, cy0, cx1, cy1;
  double x0, y0, x1, y1;
  double scale_x, scale_y;
  double width;
  gint i;

  if (item->path) {
    guppi_free (item->path);
    item->path = NULL;
  }

  guppi_element_state_get ((GuppiElementState *) state,
			   "width", &width,
			   NULL);

  guppi_canvas_item_get_bbox_c (gci, &cx0, &cy0, &cx1, &cy1);
  guppi_canvas_item_get_bbox_vp (gci, &x0, &y0, &x1, &y1);
  scale_x = x0 != x1 ? fabs ((cx1-cx0)/(x1-x0)) : 1;
  scale_y = y0 != y1 ? fabs ((cy1-cy0)/(y1-y0)) : 1;

  item->path = guppi_linegraph_view_build_path (view, 1, 1, scale_x, scale_y);

  /* Sweep through and change coordinates in our path. */
  if (item->path)
    guppi_canvas_item_vpath_vp2c (gci, item->path);

  /* Count the path's length */
  item->path_len = 0;
  if (item->path) {
    for (i = 0; item->path[i].code != ART_END && i < item->libart_length_threshold; ++i);
    item->path_len = i;
  }

  if (item->path_svp) {
    art_svp_free (item->path_svp);
    item->path_svp = NULL;
  }

  if (item->path && item->path_len < item->libart_length_threshold) {
    item->path_svp = art_svp_vpath_stroke (item->path,
					   ART_PATH_STROKE_JOIN_ROUND,
					   ART_PATH_STROKE_CAP_ROUND,
					   width * sc,
					   4, 0.25);
  }
  
}

static void
render (GuppiCanvasItem *gci, GnomeCanvasBuf *buf)
{
  GuppiLinegraphItem *item = GUPPI_LINEGRAPH_ITEM (gci);
  guint32 color;

    guppi_element_state_get (guppi_canvas_item_state (gci),
			     "color", &color,
			     NULL);
    
  if (item->path_svp) {

    gnome_canvas_render_svp (buf, item->path_svp, color);

  } else if (item->path) {

    guppi_paint_wide_curve (buf, item->path, 1, color);

  }
}

static GuppiPlotToolkit *
guppi_linegraph_toolkit_default (void)
{
  GuppiPlotToolkit *tk = guppi_plot_toolkit_new (_("Default"));

  guppi_plot_toolkit_set_button_tool (tk, 1, 0,
				      guppi_basic_tool_new_rescale (0.9));
  guppi_plot_toolkit_set_button_tool (tk, 2, 0, guppi_basic_tool_new_drag ());
  guppi_plot_toolkit_set_button_tool (tk, 3, 0,
				      guppi_basic_tool_new_rescale (1 / 0.9));

  guppi_add_keyboard_navigation_to_toolkit (tk);

  return tk;
}

/**************************************************************************/

static void
guppi_linegraph_item_class_init (GuppiLinegraphItemClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiCanvasItemClass *item_class = GUPPI_CANVAS_ITEM_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_CANVAS_ITEM);

  object_class->destroy = guppi_linegraph_item_destroy;
  object_class->finalize = guppi_linegraph_item_finalize;

  item_class->guppi_update = update;
  item_class->guppi_render = render;
  item_class->uses_vp_coordinates = TRUE;

  guppi_canvas_item_class_set_item_class_toolkit (item_class, guppi_linegraph_toolkit_default ());

}

static void
guppi_linegraph_item_init (GuppiLinegraphItem *obj)
{
  obj->libart_length_threshold = 200;
}

GtkType guppi_linegraph_item_get_type (void)
{
  static GtkType guppi_linegraph_item_type = 0;
  if (!guppi_linegraph_item_type) {
    static const GtkTypeInfo guppi_linegraph_item_info = {
      "GuppiLinegraphItem",
      sizeof (GuppiLinegraphItem),
      sizeof (GuppiLinegraphItemClass),
      (GtkClassInitFunc) guppi_linegraph_item_class_init,
      (GtkObjectInitFunc) guppi_linegraph_item_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_linegraph_item_type =
      gtk_type_unique (GUPPI_TYPE_CANVAS_ITEM, &guppi_linegraph_item_info);
  }
  return guppi_linegraph_item_type;
}

/* $Id$ */
