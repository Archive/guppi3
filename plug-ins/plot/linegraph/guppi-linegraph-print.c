/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-linegraph-print.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <guppi-memory.h>
#include "guppi-linegraph-state.h"
#include "guppi-linegraph-view.h"
#include "guppi-linegraph-print.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_linegraph_print_finalize (GtkObject *obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
print (GuppiElementPrint *ep)
{
  GuppiLinegraphState *state;
  GuppiLinegraphView *view;
  ArtVpath *path;
  double x0, y0, x1, y1;
  double px0, py0, px1, py1;
  double scale_x, scale_y;

  double width;
  guint32 color;

  state = GUPPI_LINEGRAPH_STATE (guppi_element_print_state (ep));
  view = GUPPI_LINEGRAPH_VIEW (guppi_element_print_view (ep));

  guppi_element_view_get_bbox_vp (GUPPI_ELEMENT_VIEW (view), &x0, &y0, &x1, &y1);
  guppi_element_view_get_bbox_pt (GUPPI_ELEMENT_VIEW (view), &px0, &py0, &px1, &py1);

  scale_x = (px1 - px0) / (x1 - x0);
  scale_y = (py1 - py0) / (y1 - y0);

  /* Hard wired for 4pt max error */
  path = guppi_linegraph_view_build_path (view, 4, 4, scale_x, scale_y);

  guppi_element_state_get (GUPPI_ELEMENT_STATE (state),
			   "width", &width,
			   "color", &color,
			   NULL);

  guppi_element_print_setlinewidth (ep, width);
  guppi_element_print_setrgbacolor_uint (ep, color);

  guppi_element_print_newpath (ep);
  guppi_element_print_vpath_vp (ep, path, FALSE);
  guppi_element_print_stroke (ep);

  guppi_free (path);
}

static void
guppi_linegraph_print_class_init (GuppiLinegraphPrintClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementPrintClass *ep_class = GUPPI_ELEMENT_PRINT_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_PRINT);

  object_class->finalize = guppi_linegraph_print_finalize;
  ep_class->print = print;

}

static void
guppi_linegraph_print_init (GuppiLinegraphPrint *obj)
{

}

GtkType guppi_linegraph_print_get_type (void)
{
  static GtkType guppi_linegraph_print_type = 0;
  if (!guppi_linegraph_print_type) {
    static const GtkTypeInfo guppi_linegraph_print_info = {
      "GuppiLinegraphPrint",
      sizeof (GuppiLinegraphPrint),
      sizeof (GuppiLinegraphPrintClass),
      (GtkClassInitFunc) guppi_linegraph_print_class_init,
      (GtkObjectInitFunc) guppi_linegraph_print_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_linegraph_print_type =
      gtk_type_unique (GUPPI_TYPE_ELEMENT_PRINT, &guppi_linegraph_print_info);
  }
  return guppi_linegraph_print_type;
}


/* $Id$ */
