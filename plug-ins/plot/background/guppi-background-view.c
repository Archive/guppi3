/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-background-view.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-background-view.h"

#include <guppi-memory.h>
#include "guppi-background-state.h"
#include "guppi-background-item.h"
#include "guppi-background-print.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_background_view_finalize (GtkObject *obj)
{
  GuppiBackgroundView *x = GUPPI_BACKGROUND_VIEW(obj);

  guppi_finalized (obj);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_background_view_class_init (GuppiBackgroundViewClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;
  GuppiElementViewClass *view_class = GUPPI_ELEMENT_VIEW_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_VIEW);

  object_class->finalize = guppi_background_view_finalize;

  view_class->canvas_item_type = GUPPI_TYPE_BACKGROUND_ITEM;
  view_class->print_type = GUPPI_TYPE_BACKGROUND_PRINT;
}

static void
guppi_background_view_init (GuppiBackgroundView *obj)
{

}

GtkType
guppi_background_view_get_type (void)
{
  static GtkType guppi_background_view_type = 0;
  if (!guppi_background_view_type) {
    static const GtkTypeInfo guppi_background_view_info = {
      "GuppiBackgroundView",
      sizeof (GuppiBackgroundView),
      sizeof (GuppiBackgroundViewClass),
      (GtkClassInitFunc)guppi_background_view_class_init,
      (GtkObjectInitFunc)guppi_background_view_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    guppi_background_view_type = gtk_type_unique (GUPPI_TYPE_ELEMENT_VIEW, &guppi_background_view_info);
  }
  return guppi_background_view_type;
}

GtkObject *
guppi_background_view_new (void)
{
  return GTK_OBJECT (guppi_type_new (guppi_background_view_get_type ()));
}
