/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-compass-box-state.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_COMPASS_BOX_STATE_H
#define _INC_GUPPI_COMPASS_BOX_STATE_H

#include <guppi-group-state.h>
#include <guppi-defs.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiCompassBoxState GuppiCompassBoxState;
typedef struct _GuppiCompassBoxStateClass GuppiCompassBoxStateClass;

struct _GuppiCompassBoxState {
  GuppiGroupState parent;
};

struct _GuppiCompassBoxStateClass {
  GuppiGroupStateClass parent_class;
};

#define GUPPI_TYPE_COMPASS_BOX_STATE (guppi_compass_box_state_get_type ())
#define GUPPI_COMPASS_BOX_STATE(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_COMPASS_BOX_STATE,GuppiCompassBoxState))
#define GUPPI_COMPASS_BOX_STATE0(obj) ((obj) ? (GUPPI_COMPASS_BOX_STATE(obj)) : NULL)
#define GUPPI_COMPASS_BOX_STATE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_COMPASS_BOX_STATE,GuppiCompassBoxStateClass))
#define GUPPI_IS_COMPASS_BOX_STATE(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_COMPASS_BOX_STATE))
#define GUPPI_IS_COMPASS_BOX_STATE0(obj) (((obj) == NULL) || (GUPPI_IS_COMPASS_BOX_STATE(obj)))
#define GUPPI_IS_COMPASS_BOX_STATE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_COMPASS_BOX_STATE))

GtkType guppi_compass_box_state_get_type (void);

GuppiElementState *guppi_compass_box_state_new (void);

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_COMPASS_BOX_STATE_H */

/* $Id$ */
