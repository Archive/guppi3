/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-frame-state.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-frame-state.h"

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>

#include <guppi-useful.h>
#include <guppi-rgb.h>

#include "guppi-frame-view.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_frame_state_finalize (GtkObject * obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/**************************************************************************/

static GuppiElementView *
make_view (GuppiElementState * state)
{
  return GUPPI_ELEMENT_VIEW (guppi_type_new (GUPPI_TYPE_FRAME_VIEW));
}

/**************************************************************************/

static void
guppi_frame_state_class_init (GuppiFrameStateClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementStateClass *state_class = GUPPI_ELEMENT_STATE_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_STATE);

  object_class->finalize = guppi_frame_state_finalize;

  state_class->name = _("Frame");

  state_class->make_view = make_view;
}

static void
guppi_frame_state_init (GuppiFrameState *obj)
{
  GuppiAttributeBag *bag;
  double inch = guppi_in2pt (1.0);

  bag = guppi_element_state_attribute_bag (GUPPI_ELEMENT_STATE (obj));

  /* A set of (hopefully) reasonable defaults */

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,   "show_major_ticks",     NULL, FALSE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA,      "major_tick_color",     NULL, RGBA_BLACK);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "major_tick_thickness", NULL, inch / 64);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "major_tick_length",    NULL, inch / 12);

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,   "show_minor_ticks",     NULL, FALSE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA,      "minor_tick_color",     NULL, RGBA_BLACK);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "minor_tick_thickness", NULL, inch / 96);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "minor_tick_length",    NULL, inch / 16);

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,   "show_micro_ticks",     NULL, FALSE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA,      "micro_tick_color",     NULL, RGBA_BLACK);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "micro_tick_thickness", NULL, inch / 96);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "micro_tick_length",    NULL, inch / 20);

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,   "show_major_rules",     NULL, TRUE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA,      "major_rule_color",     NULL, RGBA_GREY (0xff / 2));
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "major_rule_thickness", NULL, inch / 64);

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,   "show_minor_rules",     NULL, TRUE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA,      "minor_rule_color",     NULL, RGBA_GREY (3 * 0xff / 4));
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "minor_rule_thickness", NULL, inch / 96);

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,   "show_micro_rules",     NULL, FALSE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA,      "micro_rule_color",     NULL, RGBA_GREY (7 * 0xff / 7));
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "micro_rule_thickness", NULL, inch / 128);

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_INT,       "frame_flags",          NULL, 0);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA,      "frame_color",          NULL, RGBA_BLACK);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "frame_thickness",      NULL, inch / 48);
}

GtkType guppi_frame_state_get_type (void)
{
  static GtkType guppi_frame_state_type = 0;
  if (!guppi_frame_state_type) {
    static const GtkTypeInfo guppi_frame_state_info = {
      "GuppiFrameState",
      sizeof (GuppiFrameState),
      sizeof (GuppiFrameStateClass),
      (GtkClassInitFunc) guppi_frame_state_class_init,
      (GtkObjectInitFunc) guppi_frame_state_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_frame_state_type =
      gtk_type_unique (GUPPI_TYPE_ELEMENT_STATE, &guppi_frame_state_info);
  }
  return guppi_frame_state_type;
}

GuppiElementState *
guppi_frame_state_new (void)
{
  return GUPPI_ELEMENT_STATE (guppi_type_new (guppi_frame_state_get_type ()));
}


/* $Id$ */
