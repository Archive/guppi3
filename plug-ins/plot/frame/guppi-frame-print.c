/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-frame-print.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <guppi-memory.h>
#include <guppi-axis-markers.h>
#include "guppi-frame-state.h"
#include "guppi-frame-view.h"
#include "guppi-frame-print.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_frame_print_finalize (GtkObject *obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static gboolean
tick_calc (GuppiFrameState *state, const GuppiTick *tick,
	   double *th, double *l, guint32 *c, gboolean *is_rule)
{
  gboolean show_major_ticks, show_minor_ticks, show_micro_ticks;
  gboolean show_major_rules, show_minor_rules, show_micro_rules;

  guppi_element_state_get ((GuppiElementState *) state,
			   "show_major_ticks", &show_major_ticks,
			   "show_minor_ticks", &show_minor_ticks,
			   "show_micro_ticks", &show_micro_ticks,
			   "show_major_rules", &show_major_rules,
			   "show_minor_rules", &show_minor_rules,
			   "show_micro_rules", &show_micro_rules,
			   NULL);

  if (is_rule)
    *is_rule = guppi_tick_is_rule (tick);

  switch (guppi_tick_type (tick)) {

  case GUPPI_TICK_NONE:
    return FALSE;

  case GUPPI_TICK_MAJOR:
    if (! show_major_ticks)
      return FALSE;

    guppi_element_state_get ((GuppiElementState *) state,
			     "major_tick_thickness", th,
			     "major_tick_length", l,
			     "major_tick_color", c,
			     NULL);
    break;

  case GUPPI_TICK_MINOR:
    if (! show_minor_ticks)
      return FALSE;

    guppi_element_state_get ((GuppiElementState *) state,
			     "minor_tick_thickness", th,
			     "minor_tick_length", l,
			     "minor_tick_color", c,
			     NULL);
    break;

  case GUPPI_TICK_MICRO:
    if (! show_micro_ticks)
      return FALSE;

    guppi_element_state_get ((GuppiElementState *) state,
			     "micro_tick_thickness", th,
			     "micro_tick_length", l,
			     "micro_tick_color", c,
			     NULL);
    break;

  case GUPPI_TICK_MAJOR_RULE:
    if (! show_major_rules)
      return FALSE;

    guppi_element_state_get ((GuppiElementState *) state,
			     "major_rule_thickness", th,
			     "major_rule_color", c,
			     NULL);
    break;

  case GUPPI_TICK_MINOR_RULE:
    if (! show_minor_rules)
      return FALSE;

    guppi_element_state_get ((GuppiElementState *) state,
			     "minor_rule_thickness", th,
			     "minor_rule_color", c,
			     NULL);
    break;

  case GUPPI_TICK_MICRO_RULE:
    if (! show_micro_ticks)
      return FALSE;

    guppi_element_state_get ((GuppiElementState *) state,
			     "micro_rule_thickness", th,
			     "micro_rule_color", c,
			     NULL);
    break;

  default:
    g_assert_not_reached ();
  }

  return TRUE;
}

static void
print (GuppiElementPrint *ep)
{
  GuppiFrameState *state = GUPPI_FRAME_STATE (guppi_element_print_state (ep));
  GuppiElementView *view = guppi_element_print_view (ep);
  gint frame_flags;
  double frame_thickness;
  guint32 frame_color;
  double x0, y0, x1, y1;
  GuppiAxisMarkers *marks;
  gint j;

  guppi_element_print_get_bbox (ep, &x0, &y0, &x1, &y1);

  for (j = 0; j < 2; ++j) {

    marks = guppi_element_view_axis_markers (view, j == 0 ? GUPPI_X_AXIS : GUPPI_Y_AXIS);
    
    if (marks) {
      gint i, N;
      N = guppi_axis_markers_size (marks);
      for (i = 0; i < N; ++i) {
	const GuppiTick *tick = guppi_axis_markers_get (marks, i);
	double th, l, pos;
	guint32 c;
	gboolean is_rule;

	if (tick_calc (state, tick, &th, &l, &c, &is_rule)) {

	  pos = guppi_tick_position (tick);
	  if (j == 0)
	    guppi_element_print_vp2pt_auto (ep, &pos, NULL);
	  else
	    guppi_element_print_vp2pt_auto (ep, NULL, &pos);

	  guppi_element_print_setrgbacolor_uint (ep, c);
	  guppi_element_print_setlinewidth (ep, th);

	  guppi_element_print_newpath (ep);

	  if (is_rule) {

	    if (j == 0) {	/* vertical x-axis rule */
	      guppi_element_print_moveto (ep, pos, y0);
	      guppi_element_print_lineto (ep, pos, y1);
	    } else {		/* horizontal y-axis rule */
	      guppi_element_print_moveto (ep, x0, pos);
	      guppi_element_print_lineto (ep, x1, pos);
	    }

	  } else {

	    if (j == 0) {	/* vertical x-axis tick */
	      guppi_element_print_moveto (ep, pos, y0);
	      guppi_element_print_lineto (ep, pos, y0 + l);
	    } else {
	      guppi_element_print_moveto (ep, x0, pos);
	      guppi_element_print_lineto (ep, x0 + l, pos);
	    }

	  }

	  guppi_element_print_stroke (ep);

	}
      }
    }
  }

  guppi_element_state_get ((GuppiElementState *) state,
			   "frame_flags", &frame_flags,
			   "frame_thickness", &frame_thickness,
			   "frame_color", &frame_color,
			   NULL);

  if (frame_flags) {

    guppi_element_print_setrgbacolor_uint (ep, frame_color);
    guppi_element_print_setlinewidth (ep, frame_thickness);

    if (frame_flags & GUPPI_NORTH) {
      guppi_element_print_newpath (ep);
      guppi_element_print_moveto (ep, x1 - frame_thickness / 2, y0);
      guppi_element_print_lineto (ep, x1 - frame_thickness / 2, y1);
      guppi_element_print_stroke (ep);
    }
    if (frame_flags & GUPPI_SOUTH) {
      guppi_element_print_newpath (ep);
      guppi_element_print_moveto (ep, x0 + frame_thickness / 2, y0);
      guppi_element_print_lineto (ep, x0 + frame_thickness / 2, y1);
      guppi_element_print_stroke (ep);
    }
    if (frame_flags & GUPPI_WEST) {
      guppi_element_print_newpath (ep);
      guppi_element_print_moveto (ep, x0, y0 + frame_thickness / 2);
      guppi_element_print_lineto (ep, x1, y0 + frame_thickness / 2);
      guppi_element_print_stroke (ep);
    }
    if (frame_flags & GUPPI_EAST) {
      guppi_element_print_newpath (ep);
      guppi_element_print_moveto (ep, x0, y1 - frame_thickness / 2);
      guppi_element_print_lineto (ep, x1, y1 - frame_thickness / 2);
      guppi_element_print_stroke (ep);
    }
  }
}

static void
guppi_frame_print_class_init (GuppiFramePrintClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementPrintClass *ep_class = GUPPI_ELEMENT_PRINT_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_PRINT);

  object_class->finalize = guppi_frame_print_finalize;
  ep_class->print = print;



}

static void
guppi_frame_print_init (GuppiFramePrint *obj)
{

}

GtkType guppi_frame_print_get_type (void)
{
  static GtkType guppi_frame_print_type = 0;
  if (!guppi_frame_print_type) {
    static const GtkTypeInfo guppi_frame_print_info = {
      "GuppiFramePrint",
      sizeof (GuppiFramePrint),
      sizeof (GuppiFramePrintClass),
      (GtkClassInitFunc) guppi_frame_print_class_init,
      (GtkObjectInitFunc) guppi_frame_print_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_frame_print_type =
      gtk_type_unique (GUPPI_TYPE_ELEMENT_PRINT, &guppi_frame_print_info);
  }
  return guppi_frame_print_type;
}

GtkObject *
guppi_frame_print_new (void)
{
  return GTK_OBJECT (guppi_type_new (guppi_frame_print_get_type ()));
}



/* $Id$ */
