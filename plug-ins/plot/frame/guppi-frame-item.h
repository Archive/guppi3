/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-frame-item.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_FRAME_ITEM_H
#define _INC_GUPPI_FRAME_ITEM_H

/* #include <gnome.h> */
#include <guppi-defs.h>
#include <guppi-canvas-item.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiFrameItem GuppiFrameItem;
typedef struct _GuppiFrameItemClass GuppiFrameItemClass;

struct _GuppiFrameItem {
  GuppiCanvasItem parent;
};

struct _GuppiFrameItemClass {
  GuppiCanvasItemClass parent_class;
};

#define GUPPI_TYPE_FRAME_ITEM (guppi_frame_item_get_type())
#define GUPPI_FRAME_ITEM(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_FRAME_ITEM,GuppiFrameItem))
#define GUPPI_FRAME_ITEM0(obj) ((obj) ? (GUPPI_FRAME_ITEM(obj)) : NULL)
#define GUPPI_FRAME_ITEM_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_FRAME_ITEM,GuppiFrameItemClass))
#define GUPPI_IS_FRAME_ITEM(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_FRAME_ITEM))
#define GUPPI_IS_FRAME_ITEM0(obj) (((obj) == NULL) || (GUPPI_IS_FRAME_ITEM(obj)))
#define GUPPI_IS_FRAME_ITEM_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_FRAME_ITEM))

GtkType guppi_frame_item_get_type (void);

GtkObject *guppi_frame_item_new (void);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_FRAME_ITEM_H */

/* $Id$ */
