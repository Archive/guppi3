/* This is -*- C -*- */
/* vim: set sw=2: */

/*
 * guppi-importer-xml.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_IMPORTER_XML_H
#define _INC_GUPPI_IMPORTER_XML_H

/* #include <gnome.h> */
#include <guppi-defs.h>
#include <guppi-data-importer.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiImporterXML GuppiImporterXML;
typedef struct _GuppiImporterXMLClass GuppiImporterXMLClass;

struct _GuppiImporterXML {
  GuppiDataImporter parent;
};

struct _GuppiImporterXMLClass {
  GuppiDataImporterClass parent_class;
};

#define GUPPI_TYPE_IMPORTER_XML (guppi_importer_xml_get_type ())
#define GUPPI_IMPORTER_XML(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_IMPORTER_XML,GuppiImporterXML))
#define GUPPI_IMPORTER_XML0(obj) ((obj) ? (GUPPI_IMPORTER_XML(obj)) : NULL)
#define GUPPI_IMPORTER_XML_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_IMPORTER_XML,GuppiImporterXMLClass))
#define GUPPI_IS_IMPORTER_XML(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_IMPORTER_XML))
#define GUPPI_IS_IMPORTER_XML0(obj) (((obj) == NULL) || (GUPPI_IS_IMPORTER_XML(obj)))
#define GUPPI_IS_IMPORTER_XML_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_IMPORTER_XML))

GtkType guppi_importer_xml_get_type (void);

GuppiDataImporter *guppi_importer_xml_new (void);



END_GUPPI_DECLS

#endif /* _INC_GUPPI_IMPORTER_XML_H */

/* $Id$ */
