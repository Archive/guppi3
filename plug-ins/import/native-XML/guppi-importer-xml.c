/* This is -*- C -*- */
/* vim: set sw=2: */

/*
 * guppi-importer-xml.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <guppi-memory.h>
#include "guppi-importer-xml.h"

static GtkObjectClass * parent_class = NULL;

enum {
  ARG_0
};

static void
guppi_importer_xml_get_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_importer_xml_set_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_importer_xml_finalize (GtkObject *obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
import (GuppiDataImporter *imp,
	void (*iter_fn) (GuppiData *d, gpointer user_data),
	gpointer user_data)
{
  const gchar *filename;
  GuppiData *d;

  g_return_if_fail (imp && GUPPI_IS_DATA_IMPORTER (imp));

  filename = guppi_data_importer_filename (imp);

  if (filename) {
    d = guppi_data_read_xml_file (filename);
    iter_fn (d, user_data);
  }
}

static void
guppi_importer_xml_class_init (GuppiImporterXMLClass *klass)
{
  GtkObjectClass* object_class = (GtkObjectClass *)klass;
  GuppiDataImporterClass *imp_class = GUPPI_DATA_IMPORTER_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_DATA_IMPORTER);

  object_class->get_arg = guppi_importer_xml_get_arg;
  object_class->set_arg = guppi_importer_xml_set_arg;
  object_class->finalize = guppi_importer_xml_finalize;

  imp_class->import = import;
}

static void
guppi_importer_xml_init (GuppiImporterXML *obj)
{

}

GtkType
guppi_importer_xml_get_type (void)
{
  static GtkType guppi_importer_xml_type = 0;
  if (!guppi_importer_xml_type) {
    static const GtkTypeInfo guppi_importer_xml_info = {
      "GuppiImporterXML",
      sizeof(GuppiImporterXML),
      sizeof(GuppiImporterXMLClass),
      (GtkClassInitFunc)guppi_importer_xml_class_init,
      (GtkObjectInitFunc)guppi_importer_xml_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    guppi_importer_xml_type = gtk_type_unique (GUPPI_TYPE_DATA_IMPORTER
					       , &guppi_importer_xml_info);
  }
  return guppi_importer_xml_type;
}

GuppiDataImporter *
guppi_importer_xml_new (void)
{
  return GUPPI_DATA_IMPORTER (guppi_type_new (guppi_importer_xml_get_type ()));
}
