/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-delimited-importer.h
 *
 * Copyright (C) 1999 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_DELIMITED_IMPORTER_H
#define _INC_GUPPI_DELIMITED_IMPORTER_H

/* #include <gnome.h> */
#include <guppi-defs.h>
#include <guppi-data-importer.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiDelimitedImporter GuppiDelimitedImporter;
typedef struct _GuppiDelimitedImporterClass GuppiDelimitedImporterClass;

struct _GuppiDelimitedImporter {
  GuppiDataImporter parent;

  gint title_line;
  gint skip_lines_before;
  gint skip_lines_after;

  gboolean skip_noncontig_lines;
  gboolean skip_alpha_lines;
  gboolean autofilter;

  gchar delimiter;

  gint columns;

  /* What each column could be */
  gboolean *potential_bool;
  gboolean *potential_scalar;
  gboolean *potential_date;

  /* What the column is */
  GtkType *column_type;
  gboolean *column_ignore;
};

struct _GuppiDelimitedImporterClass {
  GuppiDataImporterClass parent_class;

  void (*changed) (GuppiDelimitedImporter *);
};

#define GUPPI_TYPE_DELIMITED_IMPORTER (guppi_delimited_importer_get_type())
#define GUPPI_DELIMITED_IMPORTER(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_DELIMITED_IMPORTER,GuppiDelimitedImporter))
#define GUPPI_DELIMITED_IMPORTER_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_DELIMITED_IMPORTER,GuppiDelimitedImporterClass))
#define GUPPI_IS_DELIMITED_IMPORTER(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_DELIMITED_IMPORTER))
#define GUPPI_IS_DELIMITED_IMPORTER_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_DELIMITED_IMPORTER))

GtkType guppi_delimited_importer_get_type (void);

GuppiDataImporter *guppi_delimited_importer_new (void);

gint guppi_delimited_importer_title_line (const GuppiDelimitedImporter *);
void guppi_delimited_importer_set_title_line (GuppiDelimitedImporter *, gint);

gint guppi_delimited_importer_skip_lines_before (const GuppiDelimitedImporter
						 *);
void guppi_delimited_importer_set_skip_lines_before (GuppiDelimitedImporter *,
						     gint);

gint guppi_delimited_importer_skip_lines_after (const GuppiDelimitedImporter
						*);
void guppi_delimited_importer_set_skip_lines_after (GuppiDelimitedImporter *,
						    gint);

gboolean guppi_delimited_importer_skip_noncontiguous (const
						      GuppiDelimitedImporter
						      *);
void guppi_delimited_importer_set_skip_noncontiguous (GuppiDelimitedImporter
						      *, gboolean);

gboolean guppi_delimited_importer_skip_alpha (const GuppiDelimitedImporter *);
void guppi_delimited_importer_set_skip_alpha (GuppiDelimitedImporter *,
					      gboolean);

gboolean guppi_delimited_importer_autofilter (const GuppiDelimitedImporter *);
void guppi_delimited_importer_set_autofilter (GuppiDelimitedImporter *,
					      gboolean);

gchar guppi_delimited_importer_delimiter (const GuppiDelimitedImporter *);
void guppi_delimited_importer_set_delimiter (GuppiDelimitedImporter *, gchar);

gint guppi_delimited_importer_columns (const GuppiDelimitedImporter *);
void guppi_delimited_importer_set_columns (GuppiDelimitedImporter *, gint);

GtkType guppi_delimited_importer_column_type (const GuppiDelimitedImporter *,
					      gint);
void guppi_delimited_importer_set_column_type (GuppiDelimitedImporter *, gint,
					       GtkType);

gboolean guppi_delimited_importer_column_ignore (const GuppiDelimitedImporter
						 *, gint);
void guppi_delimited_importer_set_column_ignore (GuppiDelimitedImporter *,
						 gint, gboolean);


gboolean guppi_delimited_importer_line_skip (GuppiDelimitedImporter *,
					     const gchar *, gint line_no);

gchar **guppi_delimited_importer_line_split (GuppiDelimitedImporter *,
					     const gchar *,
					     gint * token_count,
					     gint required_number,
					     gboolean count_only);

void guppi_delimited_importer_guess_defaults (GuppiDelimitedImporter *);
void guppi_delimited_importer_guess_types (GuppiDelimitedImporter *,
					   gint min_lines);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_DELIMITED_IMPORTER_H */

/* $Id$ */
