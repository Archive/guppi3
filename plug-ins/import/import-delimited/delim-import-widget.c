/* $Id$ */

/*
 * delim-import-widget.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 * and Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <stdlib.h>

#include <gtk/gtktogglebutton.h>
#include <gtk/gtkmenuitem.h>
#include <gtk/gtktable.h>
#include <gtk/gtkcheckbutton.h>
#include <gtk/gtkcompat.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkoptionmenu.h>
#include <gtk/gtkscrolledwindow.h>
#include <gtk/gtkspinbutton.h>
#include <gtk/gtkentry.h>
#include <gtk/gtkcombo.h>

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>

#include <ctype.h>
#include <glade/glade.h>
#include <guppi-paths.h>
#include <guppi-stream-preview.h>
#include <guppi-memory.h>
#include "delim-import-widget.h"
#include <guppi-seq-boolean.h>
#include <guppi-seq-scalar.h>
#include <guppi-seq-string.h>
#include <guppi-seq-categorical.h>
#include <guppi-seq-date.h>

/* These should match up with the options specified in the .glade file! */
static const gchar canned_delimiters[] = {
  ' ', ',', '\t', ':', '/', '|', '\0'
};

static void
checkbut_cb (GtkToggleButton * tb, GuppiDelimitedImporter * gdi)
{
  gint col;
  gboolean state;

  g_return_if_fail (tb != NULL);
  g_return_if_fail (gdi != NULL);

  col = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (tb), "col"));
  state = gtk_toggle_button_get_active (tb);
  guppi_delimited_importer_set_column_ignore (gdi, col, !state);
}

static void
type_menu_cb (GtkMenuItem * mi, GuppiDelimitedImporter * gdi)
{
  gint col;
  GtkType type;

  g_return_if_fail (mi != NULL);
  g_return_if_fail (gdi != NULL);

  col = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (mi), "col"));
  type = (GtkType) GPOINTER_TO_UINT (gtk_object_get_data (GTK_OBJECT (mi),
							  "type"));

  guppi_delimited_importer_set_column_type (gdi, col, type);
}

static void
build_type_chooser (GuppiDelimitedImporter * gdi, GladeXML * glade)
{
  GuppiStream *str;
  GtkWidget *w;
  GtkWidget *old_table;
  GtkTable *table;
  GtkWidget *checkbut;
  GtkWidget *label;
  GtkWidget *opt_menu;
  GtkWidget *menu;
  GtkWidget *mi;
  GList *iter;
  const gchar *line;
  gchar **titles = NULL;
  gint i, j, opt_start = 0, l, N;
  gchar buffer[256];
  gboolean x;

  g_return_if_fail (gdi != NULL);
  g_return_if_fail (glade != NULL);

  str = guppi_data_importer_stream (GUPPI_DATA_IMPORTER (gdi));

  N = guppi_delimited_importer_columns (gdi);
  l = guppi_delimited_importer_title_line (gdi);
  if (l > 0) {
    line = guppi_stream_get_sanitized_line (str, l - 1);
    titles = guppi_delimited_importer_line_split (gdi, line, NULL, N, FALSE);
  }

  table = GTK_TABLE (gtk_table_new (l, 3, FALSE));
  for (i = 0; i < N; ++i) {

    /* Check Button */
    checkbut = gtk_check_button_new ();
    x = !guppi_delimited_importer_column_ignore (gdi, i);
    gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (checkbut), x);
    gtk_widget_set_sensitive (checkbut,
			      guppi_delimited_importer_column_type (gdi,
								    i) != 0);
    if (guppi_delimited_importer_column_type (gdi, i) == 0)
      x = FALSE;

    gtk_object_set_data (GTK_OBJECT (checkbut), "col", GINT_TO_POINTER (i));
    gtk_signal_connect (GTK_OBJECT (checkbut),
			"toggled", GTK_SIGNAL_FUNC (checkbut_cb), gdi);
    gtk_table_attach (table, checkbut, 0, 1, i, i + 1, 0, 0, 2, 2);

    /* Label */
    if (titles) {
      label = gtk_label_new (titles[i]);
    } else {
      g_snprintf (buffer, 256, "Col %d", i + 1);
      label = gtk_label_new (buffer);
    }
    gtk_widget_set_sensitive (label, x);
    gtk_table_attach (table, label, 1, 2, i, i + 1, 0, 0, 2, 2);

    /* Menu --- This is pretty hacky */
    opt_menu = gtk_option_menu_new ();
    gtk_widget_set_sensitive (opt_menu, x);
    menu = gtk_menu_new ();

    j = 0;
    if (gdi->potential_bool && gdi->potential_bool[i]) {
      mi = gtk_menu_item_new_with_label (_("Boolean"));
      gtk_object_set_data (GTK_OBJECT (mi), "col", GINT_TO_POINTER (i));
      gtk_object_set_data (GTK_OBJECT (mi), "type",
			   GUINT_TO_POINTER (GUPPI_TYPE_SEQ_BOOLEAN));
      gtk_signal_connect (GTK_OBJECT (mi), "activate",
			  GTK_SIGNAL_FUNC (type_menu_cb), gdi);
      gtk_widget_show (mi);
      gtk_menu_append (GTK_MENU (menu), mi);
      if (gdi->column_type && gdi->column_type[i] == GUPPI_TYPE_SEQ_BOOLEAN)
	opt_start = j;
      ++j;
    }

    if (gdi->potential_scalar && gdi->potential_scalar[i]) {
      mi = gtk_menu_item_new_with_label (_("Scalar"));
      gtk_object_set_data (GTK_OBJECT (mi), "col", GINT_TO_POINTER (i));
      gtk_object_set_data (GTK_OBJECT (mi), "type",
			   GUINT_TO_POINTER (GUPPI_TYPE_SEQ_SCALAR));
      gtk_signal_connect (GTK_OBJECT (mi), "activate",
			  GTK_SIGNAL_FUNC (type_menu_cb), gdi);
      gtk_widget_show (mi);
      gtk_menu_append (GTK_MENU (menu), mi);
      if (gdi->column_type && gdi->column_type[i] == GUPPI_TYPE_SEQ_SCALAR) {
	opt_start = j;
      }
      ++j;
    }

    /* Anything can be a string */
    mi = gtk_menu_item_new_with_label (_("String"));
    gtk_object_set_data (GTK_OBJECT (mi), "col", GINT_TO_POINTER (i));
    gtk_object_set_data (GTK_OBJECT (mi), "type",
			 GUINT_TO_POINTER (GUPPI_TYPE_SEQ_STRING));
    gtk_signal_connect (GTK_OBJECT (mi), "activate",
			GTK_SIGNAL_FUNC (type_menu_cb), gdi);
    gtk_widget_show (mi);
    gtk_menu_append (GTK_MENU (menu), mi);
    if (gdi->column_type && gdi->column_type[i] == GUPPI_TYPE_SEQ_STRING) {
      opt_start = j;
    }
    ++j;

    /* ...and anything can be a categorical */
    mi = gtk_menu_item_new_with_label (_("Categorical"));
    gtk_object_set_data (GTK_OBJECT (mi), "col", GINT_TO_POINTER (i));
    gtk_object_set_data (GTK_OBJECT (mi), "type",
			 GUINT_TO_POINTER (GUPPI_TYPE_SEQ_CATEGORICAL));
    gtk_signal_connect (GTK_OBJECT (mi), "activate",
			GTK_SIGNAL_FUNC (type_menu_cb), gdi);
    gtk_widget_show (mi);
    gtk_menu_append (GTK_MENU (menu), mi);
    if (gdi->column_type && gdi->column_type[i] == GUPPI_TYPE_SEQ_CATEGORICAL)
      opt_start = j;
    ++j;

    if (gdi->potential_date && gdi->potential_date[i]) {
      mi = gtk_menu_item_new_with_label (_("Date"));
      gtk_object_set_data (GTK_OBJECT (mi), "col", GINT_TO_POINTER (i));
      gtk_object_set_data (GTK_OBJECT (mi), "type",
			   GUINT_TO_POINTER (GUPPI_TYPE_SEQ_DATE));
      gtk_signal_connect (GTK_OBJECT (mi), "activate",
			  GTK_SIGNAL_FUNC (type_menu_cb), gdi);
      gtk_widget_show (mi);
      gtk_menu_append (GTK_MENU (menu), mi);
      if (gdi->column_type && gdi->column_type[i] == GUPPI_TYPE_SEQ_DATE) {
	opt_start = j;
      }
      ++j;
    }

    gtk_widget_show (menu);
    gtk_option_menu_set_menu (GTK_OPTION_MENU (opt_menu), menu);
    gtk_option_menu_set_history (GTK_OPTION_MENU (opt_menu), opt_start);
    gtk_widget_show (opt_menu);
    gtk_table_attach (table, opt_menu, 2, 3, i, i + 1,
		      GTK_EXPAND | GTK_FILL, 0, 2, 2);



  }

  if (titles) {
    for (i = 0; titles[i] != NULL; ++i)
      guppi_free (titles[i]);
    guppi_free (titles);
  }

  /* Remove old table, and replace it with the new one */
  w = glade_xml_get_widget (glade, "type_window");
  iter = gtk_container_children (GTK_CONTAINER (w));
  old_table = iter ? GTK_WIDGET (iter->data) : NULL;
  if (old_table) {
    gtk_container_remove (GTK_CONTAINER (w), old_table);
  }
  gtk_widget_show_all (GTK_WIDGET (table));
  gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (w),
					 GTK_WIDGET (table));
}

static void
copy_state_to_widget (GuppiDelimitedImporter * gdi, GladeXML * glade)
{
  GtkWidget *w;
  GuppiStream *str;
  const gchar *s;
  gint l, i;
  gboolean done;
  gchar c;
  gchar buffer[256];

  g_return_if_fail (gdi != NULL);
  g_return_if_fail (glade != NULL);

  str = guppi_data_importer_stream (GUPPI_DATA_IMPORTER (gdi));

  w = glade_xml_get_widget (glade, "column_spin");
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (w),
			     guppi_delimited_importer_columns (gdi));

  w = glade_xml_get_widget (glade, "delimiter_menu");
  done = FALSE;
  c = guppi_delimited_importer_delimiter (gdi);
  for (i = 0; canned_delimiters[i] != '\0'; ++i) {
    if (c == canned_delimiters[i]) {
      gtk_option_menu_set_history (GTK_OPTION_MENU (w), i);
      done = TRUE;
      break;
    }
  }
  w = glade_xml_get_widget (glade, "other_label");
  gtk_widget_set_sensitive (w, !done);
  w = glade_xml_get_widget (glade, "other_entry");
  gtk_widget_set_sensitive (w, !done);
  if (!done) {
    buffer[0] = c;
    buffer[1] = '\0';
    gtk_entry_set_text (GTK_ENTRY (w), buffer);
  }

  w = glade_xml_get_widget (glade, "eol_combo");
  s = guppi_stream_eol_comment (str);
  if (s && *s == '\0')
    s = _("(none)");
  gtk_entry_set_text (GTK_ENTRY (GTK_COMBO (w)->entry), s);

  w = glade_xml_get_widget (glade, "beg_ml_combo");
  s = guppi_stream_ml_comment_start (str);
  if (s && *s == '\0')
    s = _("(none)");
  gtk_entry_set_text (GTK_ENTRY (GTK_COMBO (w)->entry), s);

  w = glade_xml_get_widget (glade, "end_ml_combo");
  s = guppi_stream_ml_comment_end (str);
  if (s && *s == '\0')
    s = _("(none)");
  gtk_entry_set_text (GTK_ENTRY (GTK_COMBO (w)->entry), s);

  w = glade_xml_get_widget (glade, "title_line_entry");
  l = guppi_delimited_importer_title_line (gdi);
  if (l >= 1) {
    g_snprintf (buffer, 256, "%d", l);
    gtk_entry_set_text (GTK_ENTRY (w), buffer);
  } else {
    gtk_entry_set_text (GTK_ENTRY (w), "");
  }

  w = glade_xml_get_widget (glade, "first_line_entry");
  l = guppi_delimited_importer_skip_lines_before (gdi);
  if (l >= 1) {
    g_snprintf (buffer, 256, "%d", l);
    gtk_entry_set_text (GTK_ENTRY (w), buffer);
  } else {
    gtk_entry_set_text (GTK_ENTRY (w), "");
  }

  w = glade_xml_get_widget (glade, "last_line_entry");
  l = guppi_delimited_importer_skip_lines_after (gdi);
  if (l >= 1) {
    g_snprintf (buffer, 256, "%d", l);
    gtk_entry_set_text (GTK_ENTRY (w), buffer);
  } else {
    gtk_entry_set_text (GTK_ENTRY (w), "");
  }

  w = glade_xml_get_widget (glade, "no_letters_check");
  gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (w),
			       guppi_delimited_importer_skip_alpha (gdi));

  w = glade_xml_get_widget (glade, "contiguous_check");
  gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (w),
			       guppi_delimited_importer_skip_noncontiguous
			       (gdi));
  gtk_widget_set_sensitive (w, FALSE);

  w = glade_xml_get_widget (glade, "autofilter_check");
  gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (w),
			       guppi_delimited_importer_autofilter (gdi));
  gtk_widget_set_sensitive (w, FALSE);

  build_type_chooser (gdi, glade);

  /* Do type menus */
}

/** Callbacks **/

static void
columns_cb (GtkSpinButton * spin, GuppiDelimitedImporter * gdi)
{
  gint x;

  g_return_if_fail (spin != NULL);
  g_return_if_fail (gdi != NULL);

  x = gtk_spin_button_get_value_as_int (spin);
  g_return_if_fail (x > 0);
  guppi_delimited_importer_set_columns (gdi, x);
}

static void
delim_menu_cb (GtkMenuItem * mi, GuppiDelimitedImporter * gdi)
{
  gchar *c;

  g_return_if_fail (mi != NULL);
  g_return_if_fail (gdi != NULL);

  c = (gchar *) gtk_object_get_data (GTK_OBJECT (mi), "delim");
  g_assert (c != NULL);
  guppi_delimited_importer_set_delimiter (gdi, *c);
}

static void
delim_entry_cb (GtkEntry * entry, GuppiDelimitedImporter * gdi)
{
  const gchar *s;

  g_return_if_fail (entry != NULL);
  g_return_if_fail (gdi != NULL);

  s = gtk_entry_get_text (entry);
  g_return_if_fail (s != NULL);
  guppi_delimited_importer_set_delimiter (gdi, s[0]);
}

static void
eol_entry_cb (GtkEntry * entry, GuppiStream * str)
{
  g_return_if_fail (entry != NULL);
  g_return_if_fail (str != NULL);

  guppi_stream_set_eol_comment (str, gtk_entry_get_text (entry));
}

static void
bml_entry_cb (GtkEntry * entry, GuppiStream * str)
{
  g_return_if_fail (entry != NULL);
  g_return_if_fail (str != NULL);

  guppi_stream_set_ml_comment_start (str, gtk_entry_get_text (entry));
}

static void
eml_entry_cb (GtkEntry * entry, GuppiStream * str)
{
  g_return_if_fail (entry != NULL);
  g_return_if_fail (str != NULL);

  guppi_stream_set_ml_comment_end (str, gtk_entry_get_text (entry));
}

static void
title_line_cb (GtkEntry * entry, GuppiDelimitedImporter * gdi)
{
  gint x;

  g_return_if_fail (entry != NULL);
  g_return_if_fail (gdi != NULL);

  x = atoi (gtk_entry_get_text (entry));
  guppi_delimited_importer_set_title_line (gdi, x);
}

static void
first_line_cb (GtkEntry * entry, GuppiDelimitedImporter * gdi)
{
  gint x;

  g_return_if_fail (entry != NULL);
  g_return_if_fail (gdi != NULL);

  x = atoi (gtk_entry_get_text (entry));
  guppi_delimited_importer_set_skip_lines_before (gdi, x);
}

static void
last_line_cb (GtkEntry * entry, GuppiDelimitedImporter * gdi)
{
  gint x;

  g_return_if_fail (entry != NULL);
  g_return_if_fail (gdi != NULL);

  x = atoi (gtk_entry_get_text (entry));
  if (x < 1)
    x = -1;
  guppi_delimited_importer_set_skip_lines_after (gdi, x);
}

static gboolean
stream_preview_line_skip_cb (GuppiStreamPreview * gsp,
			     const gchar * line, gint line_no, gpointer data)
{
  return guppi_delimited_importer_line_skip (GUPPI_DELIMITED_IMPORTER (data),
					     line, line_no);
}

static GdkColor *
stream_preview_extra_info_cb (GuppiStreamPreview * gsp,
			      const gchar * line, gint line_no,
			      gchar * sbuf, gint sbuf_len, gpointer user_data)
{
  GuppiDelimitedImporter *gdi;
  gint col_count;
  gint l;
  const gchar *p;

  g_return_val_if_fail (gsp != NULL, NULL);
  g_return_val_if_fail (line != NULL, NULL);
  g_return_val_if_fail (sbuf != NULL, NULL);

  g_return_val_if_fail (user_data != NULL, NULL);
  gdi = GUPPI_DELIMITED_IMPORTER (user_data);
  g_return_val_if_fail (gdi != NULL, NULL);

  if (sbuf_len <= 0)
    return NULL;
  
  if (line_no == guppi_delimited_importer_title_line (gdi)) {
    /* xgettext: "ttl" is abbreviation for "title" */
    g_snprintf (sbuf, sbuf_len, _("ttl"));
    return style_title_line_color ();
  }

  l = guppi_delimited_importer_skip_lines_before (gdi);
  if (l >= 0 && line_no < l) {
    /* xgettext: "pre" is abbreviation for "before" */
    g_snprintf (sbuf, sbuf_len, _("pre"));
    return style_inactive_line_number_color ();
  }
  
  l = guppi_delimited_importer_skip_lines_after (gdi);
  if (l >= 0 && line_no > l) {
    /* xgettext: "pst" is abbreviation for "after" */
    g_snprintf (sbuf, sbuf_len, _("pst"));
    return style_inactive_line_number_color ();
  }

  if (guppi_delimited_importer_skip_alpha (gdi)) {
    p = line;
    while (*p) {
      if (isalpha ((guchar)*p)) {
	g_snprintf (sbuf, sbuf_len, _("ABC"));
	return style_inactive_line_number_color ();
      }
      ++p;
    }
  }

  guppi_delimited_importer_line_split (gdi, line, &col_count, -1, TRUE);
  if (col_count > 0) {
    g_snprintf (sbuf, sbuf_len, "%-3d", col_count);
    return NULL;
  }

  /* padding */
  g_snprintf (sbuf, sbuf_len, "   ");

  return NULL;
}


static GdkColor *
stream_preview_line_color_cb (GuppiStreamPreview * gsp,
			      const gchar * line,
			      gint line_no, gpointer user_data)
{
  GuppiDelimitedImporter *gdi;

  gdi = GUPPI_DELIMITED_IMPORTER (user_data);

  if (guppi_delimited_importer_title_line (gdi) == line_no)
    return style_title_line_color ();

  return NULL;
}

GtkWidget *
guppi_delimited_importer_widget (GuppiDelimitedImporter * gdi)
{
  const gchar *path;
  GladeXML *glade;
  GtkWidget *w;
  GtkWidget *stream_preview;
  GuppiDataImporter *imp;
  GuppiStream *str;
  gint i;
  GList *iter;
  const gchar *entry_signals[] = { "changed", "activate" };

  g_return_val_if_fail (gdi != NULL, NULL);
  imp = GUPPI_DATA_IMPORTER (gdi);

  path = guppi_glade_path ("delimited-importer.glade");
  glade = path ? glade_xml_new (path, "main_table") : NULL;

  copy_state_to_widget (gdi, glade);

  stream_preview = glade_xml_get_widget (glade, "stream_preview");
  str = guppi_data_importer_stream (imp);
  guppi_stream_preview_set_stream (GUPPI_STREAM_PREVIEW (stream_preview),
				   str);
  guppi_stream_preview_set_line_skip_cb (GUPPI_STREAM_PREVIEW
					 (stream_preview),
					 stream_preview_line_skip_cb, gdi);
  guppi_stream_preview_set_line_color_cb (GUPPI_STREAM_PREVIEW
					  (stream_preview),
					  stream_preview_line_color_cb, gdi);
  guppi_stream_preview_set_extra_info_cb (GUPPI_STREAM_PREVIEW
					  (stream_preview),
					  stream_preview_extra_info_cb, gdi);

  /* Fix the delimiter menu up */
  w = glade_xml_get_widget (glade, "delimiter_menu");
  w = gtk_option_menu_get_menu (GTK_OPTION_MENU (w));
  iter = GTK_MENU_SHELL (w)->children;
  i = -1;
  do {
    ++i;
    gtk_object_set_data (GTK_OBJECT (iter->data), "delim",
			 (gchar *) & canned_delimiters[i]);
    gtk_signal_connect (GTK_OBJECT (iter->data),
			"activate", GTK_SIGNAL_FUNC (delim_menu_cb), gdi);
    iter = g_list_next (iter);
  } while (canned_delimiters[i] != '\0');


  /* Hook up all the rest of our signals */

  gtk_signal_connect (GTK_OBJECT (gdi),
		      "changed",
		      GTK_SIGNAL_FUNC (copy_state_to_widget), glade);
  gtk_signal_connect_object (GTK_OBJECT (gdi),
			     "changed",
			     GTK_SIGNAL_FUNC (guppi_stream_preview_refresh),
			     GTK_OBJECT (stream_preview));

  w = glade_xml_get_widget (glade, "column_spin");
  gtk_signal_connect (GTK_OBJECT (w),
		      "changed", GTK_SIGNAL_FUNC (columns_cb), gdi);

  w = glade_xml_get_widget (glade, "other_entry");
  for (i = 0; i < 2; ++i)
    gtk_signal_connect (GTK_OBJECT (w),
			entry_signals[i],
			GTK_SIGNAL_FUNC (delim_entry_cb), gdi);

  w = glade_xml_get_widget (glade, "eol_combo");
  for (i = 0; i < 2; ++i)
    gtk_signal_connect (GTK_OBJECT (GTK_COMBO (w)->entry),
			entry_signals[i],
			GTK_SIGNAL_FUNC (eol_entry_cb), str);

  w = glade_xml_get_widget (glade, "beg_ml_combo");
  for (i = 0; i < 2; ++i)
    gtk_signal_connect (GTK_OBJECT (GTK_COMBO (w)->entry),
			entry_signals[i],
			GTK_SIGNAL_FUNC (bml_entry_cb), str);

  w = glade_xml_get_widget (glade, "end_ml_combo");
  for (i = 0; i < 2; ++i)
    gtk_signal_connect (GTK_OBJECT (GTK_COMBO (w)->entry),
			entry_signals[i],
			GTK_SIGNAL_FUNC (eml_entry_cb), str);

  w = glade_xml_get_widget (glade, "title_line_entry");
  for (i = 0; i < 2; ++i)
    gtk_signal_connect (GTK_OBJECT (w),
			entry_signals[i],
			GTK_SIGNAL_FUNC (title_line_cb), gdi);

  w = glade_xml_get_widget (glade, "first_line_entry");
  for (i = 0; i < 2; ++i)
    gtk_signal_connect (GTK_OBJECT (w),
			entry_signals[i],
			GTK_SIGNAL_FUNC (first_line_cb), gdi);

  w = glade_xml_get_widget (glade, "last_line_entry");
  for (i = 0; i < 2; ++i)
    gtk_signal_connect (GTK_OBJECT (w),
			entry_signals[i],
			GTK_SIGNAL_FUNC (last_line_cb), gdi);

  w = glade_xml_get_widget (glade, "preload_button");
  gtk_signal_connect_object (GTK_OBJECT (w),
			     "clicked",
			     GTK_SIGNAL_FUNC (guppi_stream_load_more_lines),
			     GTK_OBJECT (str));

  w = glade_xml_get_widget (glade, "defaults_button");
  gtk_signal_connect_object (GTK_OBJECT (w),
			     "clicked",
			     GTK_SIGNAL_FUNC
			     (guppi_delimited_importer_guess_defaults),
			     GTK_OBJECT (gdi));

  return glade_xml_get_widget (glade, "main_table");
}


/* $Id$ */
