/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-delimited-importer.c
 *
 * Copyright (C) 1999 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>

#include <stdlib.h>

#include <gtk/gtksignal.h>
#include <gtk/gtkmain.h>

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-util.h>

#include <ctype.h>
#include <guppi-convenient.h>
#include <guppi-progress.h>
#include <guppi-stream.h>
#include <guppi-string.h>
#include <guppi-seq-string.h>
#include <guppi-seq-scalar.h>
#include <guppi-seq-boolean.h>
#include <guppi-seq-categorical.h>
#include <guppi-seq-date.h>
#include <guppi-struct.h>
#include "guppi-delimited-importer.h"
#include "delim-import-widget.h"

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0
};

enum {
  CHANGED,
  LAST_SIGNAL
};
static guint delim_signals[LAST_SIGNAL] = { 0 };

static void
guppi_delimited_importer_get_arg (GtkObject * obj, GtkArg * arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_delimited_importer_set_arg (GtkObject * obj, GtkArg * arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_delimited_importer_finalize (GtkObject * obj)
{
  GuppiDelimitedImporter *gdi = GUPPI_DELIMITED_IMPORTER (obj);

  guppi_free (gdi->potential_bool);
  guppi_free (gdi->potential_scalar);
  guppi_free (gdi->potential_date);
  guppi_free (gdi->column_type);
  guppi_free (gdi->column_ignore);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
source_hook_cb (GuppiDataImporter *imp)
{
  guppi_delimited_importer_guess_defaults (GUPPI_DELIMITED_IMPORTER (imp));
}

static GtkWidget *
edit_widget (GuppiDataImporter *imp)
{
  return guppi_delimited_importer_widget (GUPPI_DELIMITED_IMPORTER (imp));
}

static void
guppi_delimited_importer_class_init (GuppiDelimitedImporterClass * klass)
{
  static void guppi_delimited_importer_import (GuppiDataImporter *,
					       void (*)(GuppiData *,gpointer),
					       gpointer);

  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiDataImporterClass *imp_class = GUPPI_DATA_IMPORTER_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_DATA_IMPORTER);

  delim_signals[CHANGED] =
    gtk_signal_new ("changed",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GuppiDelimitedImporterClass, changed),
		    gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);
  gtk_object_class_add_signals (object_class, delim_signals, LAST_SIGNAL);

  object_class->get_arg = guppi_delimited_importer_get_arg;
  object_class->set_arg = guppi_delimited_importer_set_arg;
  object_class->finalize = guppi_delimited_importer_finalize;

  imp_class->set_source_hook = source_hook_cb;
  imp_class->import = guppi_delimited_importer_import;
  imp_class->state_edit_widget = edit_widget;
}

static void
guppi_delimited_importer_init (GuppiDelimitedImporter * obj)
{
  obj->skip_lines_before = -1;
  obj->skip_lines_after = -1;
  obj->skip_alpha_lines = FALSE;
  obj->title_line = -1;

  obj->delimiter = ' ';		/* whitespace is as good a default as any */

  obj->columns = 1;
}

GtkType guppi_delimited_importer_get_type (void)
{
  static GtkType guppi_delimited_importer_type = 0;
  if (!guppi_delimited_importer_type) {
    static const GtkTypeInfo guppi_delimited_importer_info = {
      "GuppiDelimitedImporter",
      sizeof (GuppiDelimitedImporter),
      sizeof (GuppiDelimitedImporterClass),
      (GtkClassInitFunc) guppi_delimited_importer_class_init,
      (GtkObjectInitFunc) guppi_delimited_importer_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_delimited_importer_type =
      gtk_type_unique (GUPPI_TYPE_DATA_IMPORTER,
		       &guppi_delimited_importer_info);
  }
  return guppi_delimited_importer_type;
}

GuppiDataImporter *
guppi_delimited_importer_new (void)
{
  return
    GUPPI_DATA_IMPORTER (guppi_type_new (guppi_delimited_importer_get_type ()));
}

/** Title Line **/

gint guppi_delimited_importer_title_line (const GuppiDelimitedImporter * gi)
{
  g_return_val_if_fail (gi != NULL, -1);
  return gi->title_line;
}

void
guppi_delimited_importer_set_title_line (GuppiDelimitedImporter * gi, gint x)
{
  g_return_if_fail (gi != NULL);
  if (x != gi->title_line) {
    gi->title_line = x;
    gtk_signal_emit (GTK_OBJECT (gi), delim_signals[CHANGED]);
  }
}

/** Skip Lines Before... */

gint
guppi_delimited_importer_skip_lines_before (const GuppiDelimitedImporter * gi)
{
  g_return_val_if_fail (gi != NULL, -1);
  return gi->skip_lines_before;
}

void
guppi_delimited_importer_set_skip_lines_before (GuppiDelimitedImporter * gi,
						gint x)
{
  g_return_if_fail (gi != NULL);
  if (x != gi->skip_lines_before) {
    gi->skip_lines_before = x;
    gtk_signal_emit (GTK_OBJECT (gi), delim_signals[CHANGED]);
  }
}

/** Skip Lines After... **/

gint
guppi_delimited_importer_skip_lines_after (const GuppiDelimitedImporter * gi)
{
  g_return_val_if_fail (gi != NULL, -1);
  return gi->skip_lines_after;
}

void
guppi_delimited_importer_set_skip_lines_after (GuppiDelimitedImporter * gi,
					       gint x)
{
  g_return_if_fail (gi != NULL);
  if (x != gi->skip_lines_after) {
    gi->skip_lines_after = x;
    gtk_signal_emit (GTK_OBJECT (gi), delim_signals[CHANGED]);
  }
}

/** Non-contiguous lines **/

gboolean
guppi_delimited_importer_skip_noncontiguous (const GuppiDelimitedImporter *
					     gi)
{
  g_return_val_if_fail (gi != NULL, FALSE);
  return gi->skip_noncontig_lines;
}

void
guppi_delimited_importer_set_skip_noncontiguous (GuppiDelimitedImporter * gi,
						 gboolean x)
{
  g_return_if_fail (gi != NULL);
  if (x != gi->skip_noncontig_lines) {
    gi->skip_noncontig_lines = x;
    gtk_signal_emit (GTK_OBJECT (gi), delim_signals[CHANGED]);
  }
}

/** Alpha Lines **/

gboolean
guppi_delimited_importer_skip_alpha (const GuppiDelimitedImporter * gi)
{
  g_return_val_if_fail (gi != NULL, FALSE);
  return gi->skip_alpha_lines;
}

void
guppi_delimited_importer_set_skip_alpha (GuppiDelimitedImporter * gi,
					 gboolean x)
{
  g_return_if_fail (gi != NULL);
  if (x != gi->skip_alpha_lines) {
    gi->skip_alpha_lines = x;
    gtk_signal_emit (GTK_OBJECT (gi), delim_signals[CHANGED]);
  }
}

/** Auto-Filter **/

gboolean
guppi_delimited_importer_autofilter (const GuppiDelimitedImporter * gi)
{
  g_return_val_if_fail (gi != NULL, FALSE);
  return gi->autofilter;
}

void
guppi_delimited_importer_set_autofilter (GuppiDelimitedImporter * gi,
					 gboolean x)
{
  g_return_if_fail (gi != NULL);
  if (x != gi->autofilter) {
    gi->autofilter = x;
    gtk_signal_emit (GTK_OBJECT (gi), delim_signals[CHANGED]);
  }
}

/** Delimiter **/

gchar guppi_delimited_importer_delimiter (const GuppiDelimitedImporter * gi)
{
  g_return_val_if_fail (gi != NULL, '\0');
  return gi->delimiter;
}

void
guppi_delimited_importer_set_delimiter (GuppiDelimitedImporter * gi, gchar x)
{
  g_return_if_fail (gi != NULL);
  if (x != gi->delimiter) {
    gi->delimiter = x;
    guppi_delimited_importer_guess_types (gi, -1);
    gtk_signal_emit (GTK_OBJECT (gi), delim_signals[CHANGED]);
  }
}

gint guppi_delimited_importer_columns (const GuppiDelimitedImporter * gi)
{
  g_return_val_if_fail (gi != NULL, -1);
  return gi->columns;
}

void
guppi_delimited_importer_set_columns (GuppiDelimitedImporter * gi, gint c)
{
  g_return_if_fail (gi != NULL);

  if (c != gi->columns) {
    gi->columns = c;
    guppi_free (gi->column_type);
    gi->column_type = NULL;
    guppi_delimited_importer_guess_types (gi, -1);
    gtk_signal_emit (GTK_OBJECT (gi), delim_signals[CHANGED]);
  }
}

GtkType
guppi_delimited_importer_column_type (const GuppiDelimitedImporter * gdi,
				      gint col)
{
  g_return_val_if_fail (gdi != NULL, 0);
  g_return_val_if_fail (col >= 0, 0);
  g_return_val_if_fail (col < gdi->columns, 0);

  if (gdi->column_type == NULL)
    return 0;
  else
    return gdi->column_type[col];
}

void
guppi_delimited_importer_set_column_type (GuppiDelimitedImporter * gdi,
					  gint col, GtkType type)
{
  g_return_if_fail (gdi != NULL);
  g_return_if_fail (col >= 0);
  g_return_if_fail (col < gdi->columns);

  if (gdi->column_type == NULL)
    gdi->column_type = guppi_new0 (GtkType, gdi->columns);

  if (gdi->column_type[col] != type) {
    gdi->column_type[col] = type;
    gtk_signal_emit (GTK_OBJECT (gdi), delim_signals[CHANGED]);
  }
}

gboolean
guppi_delimited_importer_column_ignore (const GuppiDelimitedImporter * gdi,
					gint col)
{
  g_return_val_if_fail (gdi != NULL, TRUE);
  g_return_val_if_fail (col >= 0, TRUE);
  g_return_val_if_fail (col < gdi->columns, TRUE);

  if (gdi->column_ignore == NULL)
    return FALSE;
  else
    return gdi->column_ignore[col];
}

void
guppi_delimited_importer_set_column_ignore (GuppiDelimitedImporter * gdi,
					    gint col, gboolean x)
{
  g_return_if_fail (gdi != NULL);
  g_return_if_fail (col >= 0);
  g_return_if_fail (col < gdi->columns);

  if (gdi->column_ignore == NULL)
    gdi->column_ignore = guppi_new0 (gboolean, gdi->columns);

  if (gdi->column_ignore[col] != x) {
    gdi->column_ignore[col] = x;
    gtk_signal_emit (GTK_OBJECT (gdi), delim_signals[CHANGED]);
  }
}


gboolean
guppi_delimited_importer_line_skip (GuppiDelimitedImporter * gi,
				    const gchar * line, gint line_no)
{
  const gchar *p;
  gboolean seen_alpha, seen_delim;
  gint token_count;

  g_return_val_if_fail (gi != NULL, TRUE);
  g_return_val_if_fail (line != NULL, TRUE);

  /* Explicitly don't skip the title line */
  if (gi->title_line == 1 + line_no)
    return FALSE;

  if (gi->skip_lines_before >= 0 &&
      line_no >= 0 && 1 + line_no < gi->skip_lines_before) return TRUE;

  if (gi->skip_lines_after >= 0 &&
      line_no >= 0 && 1 + line_no > gi->skip_lines_after) return TRUE;

  seen_alpha = !gi->skip_alpha_lines;
  seen_delim = gi->columns <= 1;
  p = line;
  while ((!seen_alpha || !seen_delim) && *p) {
    if (!seen_delim && *p == gi->delimiter)
      seen_delim = TRUE;
    if (!seen_alpha && isalpha ((guchar)*p))
      seen_alpha = TRUE;
    ++p;
  }

  if (gi->skip_alpha_lines && seen_alpha)
    return TRUE;

  if (!seen_delim)
    return TRUE;

  if (gi->columns > 0) {
    guppi_delimited_importer_line_split (gi, line, &token_count, -1, TRUE);
    if (gi->columns != token_count)
      return TRUE;
  }

  return FALSE;
}

gchar **
guppi_delimited_importer_line_split (GuppiDelimitedImporter * gi,
				     const gchar * line,
				     gint * token_count_ptr,
				     gint required_number_of_tokens,
				     gboolean count_only)
{
  const gchar *tok_start;
  const gchar *tok_end;
  const gchar *p;
  gboolean in_quote;
  gint token_count = 0;
  gchar **tokenv;
  GSList *tokenl = NULL;
  GSList *iter;
  gint i;
  gboolean unprocessed_delimiter = FALSE;
  GuppiStream *gs;
  gchar open_quote_char = '"', close_quote_char = '"';


  if (token_count_ptr != NULL)
    *token_count_ptr = 0;

  g_return_val_if_fail (gi != NULL, NULL);

  if (line == NULL)
    return NULL;

  gs = guppi_data_importer_stream (GUPPI_DATA_IMPORTER (gi));

  if (gs != NULL) {
    open_quote_char = guppi_stream_quote_start (gs);
    close_quote_char = guppi_stream_quote_end (gs);
  }

  p = line;
  while (*p) {

    /* First, skip leading whitespace */
    while (*p && isspace ((guchar)*p))
      ++p;

    /* Check for quoting */
    in_quote = FALSE;
    if (*p && *p == open_quote_char) {
      in_quote = TRUE;
      ++p;
    }

    /* Find the bounds of the token, adjusting for quoting as necessary */
    tok_start = tok_end = NULL;
    if (*p) {
      tok_start = p;
      unprocessed_delimiter = FALSE;

      /* If we are in a quote, search for the close quote. */
      if (in_quote) {
	while (*p && *p != close_quote_char)
	  ++p;
	tok_end = p;
      }

      /* Look for the next delimiter. */
      while (*p && *p != gi->delimiter)
	++p;

      /* If we see a delimiter, record that fact. */
      if (*p == gi->delimiter)
	unprocessed_delimiter = TRUE;

      /* If we haven't marked the end of a token yet, do so now. */
      if (tok_end == NULL)
	tok_end = p;

      /* Move our pointer past the delimiter. */
      if (*p && *p == gi->delimiter)
	++p;
    }

    /* We shouldn't have one of these NULL and the other non-NULL. */
    g_assert (tok_start == NULL ? tok_end == NULL : tok_end != NULL);

    /* Remove trailing whitespace from our token. */
    while (tok_end != NULL && tok_end > tok_start && isspace ((guchar)*(tok_end - 1))) {
      --tok_end;
    }


    /* Store the token in a linked list. */
    if (tok_start != NULL && tok_end != NULL) {
      g_assert (tok_start <= tok_end);
      ++token_count;
      if (!count_only) {
	tokenl = g_slist_prepend (tokenl,
				  guppi_strndup (tok_start,
						 tok_end - tok_start));
      }
    }
  }

  /* If the "unprocessed_delimiter" flag is set, that means that our line
     had to have ended with a delimiter.  We therefore record an extra,
     empty token... but only if our delimiter is something other than
     a space.  If we didn't make this exception, we'd end up treating
     trailing whitespace as meaningful.  (Which we shouldn't do, I think...) */
  if (unprocessed_delimiter && gi->delimiter != ' ') {
    ++token_count;
    if (!count_only)
      tokenl = g_slist_prepend (tokenl, guppi_strdup (""));
  }

  if (token_count_ptr != NULL)
    *token_count_ptr = token_count;

  /* If a specific number of tokens has been required by the caller, and
     if we've found a number of tokens that differs from that number,
     deallocate everything and return NULL. */
  if (count_only ||
      token_count == 0 ||
      (required_number_of_tokens > 0 &&
       token_count != required_number_of_tokens)) {
    g_slist_foreach (tokenl, guppi_free2, NULL);
    g_slist_free (tokenl);
    return NULL;
  }

  /* Create our null-terminated vector. */
  tokenv = guppi_new (gchar *, token_count + 1);
  tokenv[token_count] = NULL;

  /* Our tokens are stored in the list tokenl in reverse order, so we
     copy them into the vector tokenv from back to front. */
  iter = tokenl;
  for (i = token_count - 1; i >= 0; --i) {
    tokenv[i] = (gchar *) iter->data;
    iter = g_slist_next (iter);
  }
  g_slist_free (tokenl);

  return tokenv;
}


/***************************************************************************
 *
 * Delimited Importer Intuition
 *
 ***************************************************************************/

/** Line Statistics **/

typedef struct _LineStats LineStats;
struct _LineStats {
  gint length;
  gint alpha_count;
  gint numeric_count;
  gint space_count;
  gint other_count;

  gint space_tokens;
  gint comma_tokens;
  gint tab_tokens;
  gint pipe_tokens;
  gint colon_tokens;
};

static LineStats *
guppi_delimited_importer_line_stats (GuppiDelimitedImporter * gdi,
				     const gchar * line)
{
  const gchar *p = line;
  LineStats *stats;
  gchar saved_delim;

  g_return_val_if_fail (gdi != NULL, NULL);
  if (line == NULL)
    return NULL;

  /* leading whitespace doesn't count. */
  while (*p && isspace ((guchar)*p))
    ++p;

  /* Return null on a whitespace-only line */
  if (*p == '\0')
    return NULL;

  stats = guppi_new0 (LineStats, 1);

  while (*p) {
    ++stats->length;
    if (isalpha ((guchar)*p))
      ++stats->alpha_count;
    else if (isdigit ((guchar)*p))
      ++stats->numeric_count;
    else if (isspace ((guchar)*p))
      ++stats->space_count;
    else
      ++stats->other_count;
    ++p;
  }

  saved_delim = gdi->delimiter;

  gdi->delimiter = ' ';
  guppi_delimited_importer_line_split (gdi, line, &stats->space_tokens, 0,
				       TRUE);

  gdi->delimiter = ',';
  guppi_delimited_importer_line_split (gdi, line, &stats->comma_tokens, 0,
				       TRUE);

  gdi->delimiter = '\t';
  guppi_delimited_importer_line_split (gdi, line, &stats->tab_tokens, 0,
				       TRUE);

  gdi->delimiter = '|';
  guppi_delimited_importer_line_split (gdi, line, &stats->pipe_tokens, 0,
				       TRUE);

  gdi->delimiter = ':';
  guppi_delimited_importer_line_split (gdi, line, &stats->colon_tokens, 0,
				       TRUE);

  gdi->delimiter = saved_delim;

  return stats;
}

/** Look for Runs in Line Stats **/

typedef struct _RunTally RunTally;
struct _RunTally {
  gint tokens, best_tokens;
  gint run_len, best_run_len;
  gint run_start, best_run_start;
};

static void
run_tally_init (RunTally * rt)
{
  rt->tokens = rt->best_tokens = -1;
  rt->run_len = rt->best_run_len = 0;
  rt->run_start = rt->best_run_start = -1;
}

static void
run_tally_run_check (RunTally * rt, gint tokens, gint line_no)
{
  if (tokens == rt->tokens) {
    /* The streak continues */
    if (tokens > 0)
      ++rt->run_len;
  } else {
    /* The streak is broken ---
       Our rule of thumb: 
       - A longer streak trumps the shorter streak as long as either:
       + The longer streak has more than 1 token/line
       + or this is the first streak we've found
       + or the previous streak was less than 5 lines long.
       - The "trumping" is overridden if
       - The previous longest streak was for 1 token/line
       - and this new streak has more than 1 token/line
       - and the new streak is at least 5 lines long.
       This helps us to extract data sets that are surrounded by relatively
       large blocks of text before and after the meaningful lines.
     */
    if ((rt->run_len > rt->best_run_len &&
	 (rt->tokens > 1 || rt->best_tokens < 1 || rt->best_run_len < 5)) ||
	(rt->best_tokens <= 1 && rt->tokens > 1 && rt->run_len >= 5)) {
      rt->best_tokens = rt->tokens;
      rt->best_run_len = rt->run_len;
      rt->best_run_start = rt->run_start;
    }
    rt->tokens = tokens;
    rt->run_len = 1;
    rt->run_start = line_no;
  }
}

void
guppi_delimited_importer_guess_defaults (GuppiDelimitedImporter * gdi)
{
  GuppiStream *stream;
  LineStats *stats = NULL;
  LineStats *first_line = NULL;
  LineStats *second_line = NULL;
  double ap1, ap2;
  gint i;
  gint N;
  const gchar *sani;
  gint comma_best, tab_best, space_best;

  RunTally comma_rt, tab_rt, space_rt;

  g_return_if_fail (gdi != NULL);

  stream = guppi_data_importer_stream (GUPPI_DATA_IMPORTER (gdi));
  g_return_if_fail (stream != NULL);

  run_tally_init (&comma_rt);
  run_tally_init (&tab_rt);
  run_tally_init (&space_rt);

  guppi_stream_load_some_lines (stream);
  N = guppi_stream_number_of_preloaded_lines (stream);

  for (i = 0; i <= N; ++i) {
    if (i < N) {
      sani = guppi_stream_get_sanitized_line (stream, i);
      stats = guppi_delimited_importer_line_stats (gdi, sani);
    }
    if (i == N || stats != NULL) {
      run_tally_run_check (&comma_rt, i == N ? -1 : stats->comma_tokens, i);
      run_tally_run_check (&tab_rt, i == N ? -1 : stats->tab_tokens, i);
      run_tally_run_check (&space_rt, i == N ? -1 : stats->space_tokens, i);
    }
    guppi_free (stats);
    stats = NULL;
  }

  /* shorthand */
  comma_best = comma_rt.best_run_len;
  tab_best = tab_rt.best_run_len;
  space_best = space_rt.best_run_len;

  /*
     printf("\n");
     printf("comma: best=%d len=%d\n", comma_rt.best_tokens, comma_rt.best_run_len);
     printf("space: best=%d len=%d\n", space_rt.best_tokens, space_rt.best_run_len);
     printf("  tab: best=%d len=%d\n", tab_rt.best_tokens,tab_rt.best_run_len);
     printf("\n");
   */

  /* Set our delimit, column # and # of header lines to skip based on
     our analysis of the runs. */

  if (comma_rt.best_tokens > 1 &&
      comma_best >= MAX (space_rt.best_tokens > 1 ? space_best : 0,
			 tab_rt.best_tokens > 1 ? tab_best : 0)) {

    guppi_delimited_importer_set_delimiter (gdi, ',');
    guppi_delimited_importer_set_columns (gdi, comma_rt.best_tokens);
    guppi_delimited_importer_set_skip_lines_before (gdi,
						    comma_rt.best_run_start +
						    1);

  } else if (tab_rt.best_tokens > 1 &&
	     tab_rt.best_run_len >= space_rt.best_tokens > 1 ? space_best : 0) {

    guppi_delimited_importer_set_delimiter (gdi, '\t');
    guppi_delimited_importer_set_columns (gdi, tab_rt.best_tokens);
    guppi_delimited_importer_set_skip_lines_before (gdi,
						    tab_rt.best_run_start +
						    1);


  } else if (space_rt.best_tokens > 1) {

    guppi_delimited_importer_set_delimiter (gdi, ' ');
    guppi_delimited_importer_set_columns (gdi, space_rt.best_tokens);
    guppi_delimited_importer_set_skip_lines_before (gdi,
						    space_rt.best_run_start +
						    1);

  } else {			/* we give up */

    guppi_delimited_importer_set_delimiter (gdi, ' ');
    guppi_delimited_importer_set_columns (gdi, 1);

  }

  i = guppi_delimited_importer_skip_lines_before (gdi);
  if (i > 0) {
    --i;

    sani = guppi_stream_get_sanitized_line (stream, i);
    first_line = guppi_delimited_importer_line_stats (gdi, sani);

    if (first_line != NULL && first_line->length > 0) {

      /* Find second line */
      ++i;
      while (i < N) {
	sani = guppi_stream_get_sanitized_line (stream, i);
	second_line = guppi_delimited_importer_line_stats (gdi, sani);
	if (second_line != NULL && second_line->length > 0)
	  break;
	guppi_free (second_line);
	second_line = NULL;
	++i;
      }

      /* Our ad-hoc rule: the first line is assumed to be the title line
         if it has more that 10% alpha characters, and the % of alpha
         characters is twice that of the second line. */
      if (second_line != NULL && second_line->length > 0) {
	ap1 = first_line->alpha_count / (double) first_line->length;
	ap2 = second_line->alpha_count / (double) second_line->length;
	if (ap1 >= 0.10 && ap1 >= 2 * ap2)
	  guppi_delimited_importer_set_title_line (gdi,
						   guppi_delimited_importer_skip_lines_before
						   (gdi));
      }

      guppi_free (first_line);
      guppi_free (second_line);
    }
  }

  guppi_delimited_importer_guess_types (gdi, N);
}

/**************************************************************************/

typedef struct _ColumnInfo ColumnInfo;
struct _ColumnInfo {
  gint total_count;
  gint bool_count;
  gint number_count;
  gint int_count;
  gint nonneg_int_count;
  gint not_date_count;

  gint distinct;
  GHashTable *hash;
};


void
guppi_delimited_importer_guess_types (GuppiDelimitedImporter * gdi,
				      gint min_lines)
{
  const gchar *sani;
  GuppiStream *stream;
  gint i, j, N;
  gint inspected_lines = 0;
  ColumnInfo *ci;
  gchar **tokens;
  gint n;
  GDate dt;

  g_return_if_fail (gdi != NULL);

  guppi_free (gdi->potential_bool);
  guppi_free (gdi->potential_scalar);
  guppi_free (gdi->potential_date);
  guppi_free (gdi->column_type);
  guppi_free (gdi->column_ignore);

  gdi->potential_bool = guppi_new0 (gboolean, gdi->columns);
  gdi->potential_scalar = guppi_new0 (gboolean, gdi->columns);
  gdi->potential_date = guppi_new0 (gboolean, gdi->columns);
  gdi->column_type = guppi_new0 (GtkType, gdi->columns);
  gdi->column_ignore = guppi_new0 (gboolean, gdi->columns);

  ci = guppi_new0 (ColumnInfo, gdi->columns);
  for (i = 0; i < gdi->columns; ++i)
    ci[i].hash = g_hash_table_new (g_str_hash, g_str_equal);

  stream = guppi_data_importer_stream (GUPPI_DATA_IMPORTER (gdi));
  N = guppi_stream_number_of_preloaded_lines (stream);
  if (min_lines <= 0)
    min_lines = N;

  /* Collect statistics on lines */
  while (i < N && inspected_lines < min_lines) {
    sani = guppi_stream_get_sanitized_line (stream, i);
    if (!guppi_delimited_importer_line_skip (gdi, sani, i) &&
	i + 1 != guppi_delimited_importer_title_line (gdi)) {

      tokens = guppi_delimited_importer_line_split (gdi, sani, NULL,
						    gdi->columns, FALSE);
      if (tokens) {
	for (j = 0; tokens[j]; ++j) {

	  ++ci[j].total_count;

	  if (guppi_string_is_boolean (tokens[j]))
	    ++ci[j].bool_count;

	  if (guppi_string_is_number (tokens[j])) {
	    ++ci[j].number_count;

	    if (guppi_string_is_int (tokens[j])) {
	      ++ci[j].int_count;
	      n = atoi (tokens[j]);
	      if (n >= 0)
		++ci[j].nonneg_int_count;
	    }
	  }

	  /* Give up after a few failed dates, because
	     g_date_set_parse() is kind of expensive to call over and
	     over again. */
	  if (ci[j].not_date_count < 5) {
	    g_date_clear (&dt, 1);
	    g_date_set_parse (&dt, tokens[j]);
	    if (!g_date_valid (&dt))
	      ++ci[j].not_date_count;
	  }

	  if (g_hash_table_lookup (ci[j].hash, tokens[j]) == NULL) {
	    ++ci[j].distinct;
	    g_hash_table_insert (ci[j].hash, tokens[j], tokens[j]);
	  } else {
	    guppi_free (tokens[j]);
	  }
	}

	guppi_free (tokens);

	++inspected_lines;
      }
    }
    ++i;
  }

  /* Step through and make our guesses */
  for (i = 0; i < gdi->columns; ++i) {

    if (inspected_lines > 0) {

      /* Find possibilities */
      if (ci[i].bool_count == inspected_lines)
	gdi->potential_bool[i] = TRUE;
      if (ci[i].number_count == inspected_lines)
	gdi->potential_scalar[i] = TRUE;
      if (ci[i].not_date_count == 0)
	gdi->potential_date[i] = TRUE;

      /* Based on our possibilities, make a guess */
      gdi->column_type[i] = GUPPI_TYPE_SEQ_STRING;
      if (ci[i].distinct <= 0.05 * ci[i].total_count)
	gdi->column_type[i] = GUPPI_TYPE_SEQ_CATEGORICAL;
      if (gdi->potential_scalar[i])
	gdi->column_type[i] = GUPPI_TYPE_SEQ_SCALAR;
      if (gdi->potential_bool[i])
	gdi->column_type[i] = GUPPI_TYPE_SEQ_BOOLEAN;
      if (gdi->potential_date[i])
	gdi->column_type[i] = GUPPI_TYPE_SEQ_DATE;


    } else {

      gdi->column_type[i] = 0;

    }
  }


  /* Clean up */
  for (i = 0; i < gdi->columns; ++i) {
    g_hash_table_foreach (ci[i].hash, guppi_free_hash_key, NULL);
    g_hash_table_destroy (ci[i].hash);
  }
  guppi_free (ci);
}

/****************************************************************************
 *
 *  The actual importing function
 *
 ****************************************************************************/

static void
guppi_delimited_importer_import (GuppiDataImporter * imp,
				 void (*iter_fn) (GuppiData *, gpointer),
				 gpointer user_data)
{
  GuppiDelimitedImporter *gdi;
  GuppiStream *str;
  gint i, j, line_no, est_line_count;
  const gchar *line;
  GuppiData **datav;
  gchar buffer[256];
  gchar **tokenv;
  double x;
  gboolean b;
  GuppiData *data_struct = NULL;
  gchar *filename;

  g_return_if_fail (imp && GUPPI_IS_DELIMITED_IMPORTER (imp));
  gdi = GUPPI_DELIMITED_IMPORTER (imp);

  /* Create our vector of data objects to stuff our columns into.
     We give them non-offensive default names. */
  datav = guppi_new (GuppiData *, gdi->columns);
  for (i = 0; i < gdi->columns; ++i) {
    if (gdi->column_type[i] == 0 ||
	(gdi->column_ignore && gdi->column_ignore[i])) datav[i] = NULL;
    else if (gdi->column_type[i] == GUPPI_TYPE_SEQ_STRING)
      datav[i] = guppi_seq_string_new ();
    else if (gdi->column_type[i] == GUPPI_TYPE_SEQ_SCALAR)
      datav[i] = guppi_seq_scalar_new ();
    else if (gdi->column_type[i] == GUPPI_TYPE_SEQ_BOOLEAN)
      datav[i] = guppi_seq_boolean_new ();
    else if (gdi->column_type[i] == GUPPI_TYPE_SEQ_CATEGORICAL)
      datav[i] = guppi_seq_categorical_new ();
    else if (gdi->column_type[i] == GUPPI_TYPE_SEQ_DATE)
      datav[i] = guppi_seq_date_new ();
    else {
      g_message ("Unknown type.");
      datav[i] = NULL;
    }

    if (datav[i]) {
      g_snprintf (buffer, 256, _("Column %d"), i + 1);
      guppi_data_set_label (datav[i], buffer);
    }
  }

  str = guppi_data_importer_stream (imp);
  filename = guppi_strdup (guppi_stream_source (str));
  guppi_stream_set_sequential_mode (str);

  line_no = 0;
  est_line_count = guppi_stream_estimated_number_of_lines (str);
  while ((line = guppi_stream_get_sanitized_line (str, line_no))) {

    if (line && !guppi_delimited_importer_line_skip (gdi, line, line_no)) {

      tokenv = guppi_delimited_importer_line_split (gdi, line, NULL,
						    gdi->columns, FALSE);

      if (tokenv) {

	if (line_no + 1 == guppi_delimited_importer_title_line (gdi)) {
	  for (i = 0; i < gdi->columns; ++i) {
	    if (datav[i])
	      guppi_data_set_label (datav[i], tokenv[i]);
	    guppi_free (tokenv[i]);
	  }
	} else {
	  for (i = 0; i < gdi->columns; ++i) {

	    if (datav[i]) {
	      if (gdi->column_type[i] == GUPPI_TYPE_SEQ_STRING) {
		guppi_seq_string_append_nc (GUPPI_SEQ_STRING (datav[i]),
					    tokenv[i]);
		tokenv[i] = NULL;
	      } else if (gdi->column_type[i] == GUPPI_TYPE_SEQ_SCALAR) {
		x = atof (tokenv[i]);
		guppi_seq_scalar_append (GUPPI_SEQ_SCALAR (datav[i]), x);
	      } else if (gdi->column_type[i] == GUPPI_TYPE_SEQ_BOOLEAN) {
		b = guppi_string2boolean (tokenv[i]);
		guppi_seq_boolean_append (GUPPI_SEQ_BOOLEAN (datav[i]), b);
	      } else if (gdi->column_type[i] == GUPPI_TYPE_SEQ_CATEGORICAL) {
		guppi_seq_categorical_append (GUPPI_SEQ_CATEGORICAL
					      (datav[i]), tokenv[i]);
	      } else if (gdi->column_type[i] == GUPPI_TYPE_SEQ_DATE) {
		GDate dt;
		g_date_set_parse (&dt, tokenv[i]);
		guppi_seq_date_append (GUPPI_SEQ_DATE (datav[i]), &dt);
	      } else {
		g_assert_not_reached ();
	      }

	    }

	    guppi_free (tokenv[i]);

	  }
	}

	guppi_free (tokenv);
      }
    }
    ++line_no;

    if (line_no % 100 == 0) {

      if (est_line_count > 0)
	guppi_progress_set_percentage (MIN
				       (line_no / (double) est_line_count,
					1));
      else
	guppi_progress_show_activity ();

      /* In a big import, it is nice for our UI not to freeze */
      while (gtk_events_pending ())
	gtk_main_iteration ();
    }
  }

  if (est_line_count > 0)
    guppi_progress_set_percentage (0);

  /* "Collapse" datav */
  /*
     for (i=0; i<gdi->columns; ++i) {
     if (datav[i] == NULL) {
     for(j=i+1; j<gdi->columns && datav[j] == NULL; ++j);
     if (j<gdi->columns) {
     datav[i] = datav[j];
     datav[j] = NULL;
     }
     }
     }
   */

  j = 0;
  for (i = 0; i < gdi->columns; ++i)
    if (datav[i])
      ++j;

  if (j > 0) {

    data_struct = guppi_struct_new ();
    if (filename) {
      g_snprintf (buffer, 256, _("data from %s"),
		  g_filename_pointer (filename));
    } else {
      g_snprintf (buffer, 256, _("Imported data"));
    }
    guppi_data_set_label (data_struct, buffer);

    for (i = 0; i < gdi->columns; ++i) {
      if (datav[i]) {
	g_snprintf (buffer, 256, "C%d", i + 1);
	guppi_struct_add_free_field (GUPPI_STRUCT (data_struct), buffer);
	guppi_struct_set (GUPPI_STRUCT (data_struct), buffer, datav[i]);
	guppi_unref (datav[i]);
      }
    }
  }

  guppi_free (datav);
  guppi_free (filename);

  /* We always return a single item, so we only need to call our
     iter_fn once. */
  iter_fn (data_struct, user_data);

  guppi_unref (data_struct);
}




/* $Id$ */
