/* $Id$ */

/*
 * via-yahoo.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
  Yahoo of course refers to the web portal Yahoo! that provides all
  sorts of nice free services.  I'm sure you already know that Yahoo!
  is a trademark of Yahoo!, Inc.
*/

/* The historical data at Yahoo! seems to go back only to about 1962
   or so.  So this should be sufficient to go all of the way to the
   beginning... */

#define YAHOO_START_MONTH 1
#define YAHOO_START_DAY   1
#define YAHOO_START_YEAR  1960

#include <config.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#include <guppi-progress.h>
#include <guppi-memory.h>
#include "net-common.h"
#include "via-yahoo.h"


static gint
open_yahoo_connection (const gchar *symbol, GDate *start_date, GDate *end_date)
{
  GDate default_start_date, default_end_date;
  gchar *uri;
  gint fd;

  g_return_val_if_fail (symbol, -1);

  /* Plug in default dates, if necessary. */

  if (start_date == NULL) {
    g_date_set_dmy (&default_start_date,
		    YAHOO_START_DAY, YAHOO_START_DAY, YAHOO_START_YEAR);
    start_date = &default_start_date;
  }
  
  if (end_date == NULL) {

    /* We set the end-date to be tomorrow, just in case of weird 
       time-zone issues. */

    time_t now;
    struct tm* tm;


    now = time (NULL);
    tm = localtime (&now);

    g_date_set_dmy (&default_end_date, 
                    tm->tm_mday,
                    tm->tm_mon+1,
		    1900+tm->tm_year);
    g_date_add_days (&default_end_date, 1);

    end_date = &default_end_date;
  }

  g_return_val_if_fail (g_date_valid (start_date), FALSE);
  g_return_val_if_fail (g_date_valid (end_date), FALSE);

  if (g_date_compare (start_date, end_date) > 0)
    return -1;

  uri = guppi_strdup_printf ("/table.csv?s=%s&a=%d&b=%d&c=%d&d=%d&e=%d&f=%d&g=d&q=q&y=0",
			 symbol,
                        (gint)g_date_month (start_date),
                        (gint)g_date_day (start_date),
                        (gint)g_date_year (start_date),
                        (gint)g_date_month (end_date),
                        (gint)g_date_day (end_date),
                        (gint)g_date_year (end_date));

  /* Connect to Yahoo! */

  fd = connect_via_http ("chart.yahoo.com", 80, uri, NULL);
  guppi_free (uri);

  return fd;
}

gboolean
via_yahoo (GuppiNetHistQuotes *hq, GuppiPriceSeries *ser,
	   GDate *start_date, GDate *end_date)
{
  gint fd;
  FILE *in;
  gchar buffer[512];
  gint count = 0;

  g_return_val_if_fail (hq && GUPPI_IS_NET_HIST_QUOTES (hq), FALSE);
  g_return_val_if_fail (ser && GUPPI_IS_PRICE_SERIES (ser), FALSE);
  g_return_val_if_fail (guppi_data_can_change (GUPPI_DATA (ser)), FALSE);

  g_return_val_if_fail (guppi_net_hist_quotes_symbol (hq), FALSE);
  fd = open_yahoo_connection (guppi_net_hist_quotes_symbol (hq),
			      start_date, end_date);
  if (fd < 0)
    return FALSE;

  in = fdopen (fd, "r");
  g_return_val_if_fail (in, FALSE);

  while (fgets (buffer, 512, in)) {
    gchar *s = buffer;
    gchar *t = buffer;
    gboolean done = FALSE;
    GDate dt;
    double num[5];
    gint i = -1;

    while (!done) {
      while (*t != ',' && *t != '\0')
        ++t;

      if (*t == '\0')
	done = TRUE;
      else 
	*t = '\0';
      
      if (i == -1) {
	g_date_set_parse (&dt, s);
	if (!g_date_valid (&dt)) {
	  done = TRUE;
	  g_print ("bad date: %s\n", s);
	}
      } else {
	g_assert (i < 5);
	num[i] = atof (s);
      }
      
      if (!done) {
	++t;
	s = t;
      }
      
      ++i;
    }

    if (i == 5) {
      guppi_price_series_set (ser, PRICE_OPEN, &dt, num[0]);
      guppi_price_series_set (ser, PRICE_HIGH, &dt, num[1]);
      guppi_price_series_set (ser, PRICE_LOW, &dt, num[2]);
      guppi_price_series_set (ser, PRICE_CLOSE, &dt, num[3]);
      guppi_price_series_set (ser, PRICE_VOLUME, &dt, num[4]);
      
      ++count;
    } else {

      if (g_date_valid (&dt))
	g_message ("Incomplete record for %d-%d-%d",
		   g_date_year (&dt), g_date_month (&dt), g_date_day (&dt));
    }

  }

  g_message ("read %d records", count);

  return TRUE;
}




/* $Id$ */
