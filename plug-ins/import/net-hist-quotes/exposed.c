/* $Id$ */

/*
 * exposed.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <guppi-convenient.h>
#include "guppi-net-hist-quotes.h"
#include "exposed.h"

static void 
data_cb (GuppiData *d, gpointer user_data)
{
  *(GuppiData **)user_data = d;
  guppi_ref (d); /* We are leaking this right now... */
}

GuppiData *
load_stock_data (const gchar *symbol)
{
  GuppiDataImporter *imp;
  GuppiNetHistQuotes *hq;
  GuppiData *d = NULL;

  g_return_val_if_fail (symbol, NULL);

  imp = guppi_net_hist_quotes_new ();
  hq = GUPPI_NET_HIST_QUOTES (imp);

  guppi_net_hist_quotes_set_symbol (hq, symbol);

  guppi_data_importer_import (imp, data_cb, &d);
  guppi_unref (imp);

  return d;
}




/* $Id$ */
