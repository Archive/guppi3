/* This is -*- C -*- */
/* vim: set sw=2: */

/*
 * guppi-net-hist-quotes.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_NET_HIST_QUOTES_H
#define _INC_GUPPI_NET_HIST_QUOTES_H

/* #include <gnome.h> */
#include <guppi-defs.h>
#include <guppi-price-series.h>
#include <guppi-data-importer.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiNetHistQuotes GuppiNetHistQuotes;
typedef struct _GuppiNetHistQuotesClass GuppiNetHistQuotesClass;

struct _GuppiNetHistQuotes {
  GuppiDataImporter parent;

  gchar *symbol;

  gboolean (*get_method) (GuppiNetHistQuotes *, GuppiPriceSeries *,
			  GDate *, GDate *);
};

struct _GuppiNetHistQuotesClass {
  GuppiDataImporterClass parent_class;
};

#define GUPPI_TYPE_NET_HIST_QUOTES (guppi_net_hist_quotes_get_type ())
#define GUPPI_NET_HIST_QUOTES(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_NET_HIST_QUOTES,GuppiNetHistQuotes))
#define GUPPI_NET_HIST_QUOTES0(obj) ((obj) ? (GUPPI_NET_HIST_QUOTES(obj)) : NULL)
#define GUPPI_NET_HIST_QUOTES_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_NET_HIST_QUOTES,GuppiNetHistQuotesClass))
#define GUPPI_IS_NET_HIST_QUOTES(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_NET_HIST_QUOTES))
#define GUPPI_IS_NET_HIST_QUOTES0(obj) (((obj) == NULL) || (GUPPI_IS_NET_HIST_QUOTES(obj)))
#define GUPPI_IS_NET_HIST_QUOTES_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_NET_HIST_QUOTES))

GtkType guppi_net_hist_quotes_get_type (void);

GuppiDataImporter *guppi_net_hist_quotes_new (void);

const gchar *guppi_net_hist_quotes_symbol (GuppiNetHistQuotes *);
void guppi_net_hist_quotes_set_symbol (GuppiNetHistQuotes *, const gchar *);


END_GUPPI_DECLS

#endif /* _INC_GUPPI_NET_HIST_QUOTES_H */

/* $Id$ */
