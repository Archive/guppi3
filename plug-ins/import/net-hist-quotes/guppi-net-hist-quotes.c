/* This is -*- C -*- */
/* vim: set sw=2: */

/*
 * guppi-net-hist-quotes.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-util.h>

#include <ctype.h>
#include <time.h>

#include <guppi-convenient.h>
#include "guppi-net-hist-quotes.h"
#include "via-yahoo.h"

static GtkObjectClass * parent_class = NULL;

enum {
  ARG_0,
  ARG_SYMBOL
};

static void
guppi_net_hist_quotes_get_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  GuppiNetHistQuotes *imp = GUPPI_NET_HIST_QUOTES (obj);

  switch (arg_id) {

  case ARG_SYMBOL:
    GTK_VALUE_POINTER (*arg) = (gchar *)guppi_net_hist_quotes_symbol (imp);
    break;

  default:
    break;
  };
}

static void
guppi_net_hist_quotes_set_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  GuppiNetHistQuotes *imp = GUPPI_NET_HIST_QUOTES (obj);

  switch (arg_id) {

  case ARG_SYMBOL:
    guppi_net_hist_quotes_set_symbol (imp, (gchar *)GTK_VALUE_POINTER (*arg));
    break;

  default:
    break;
  };
}

static void
guppi_net_hist_quotes_finalize (GtkObject *obj)
{
  GuppiNetHistQuotes *hq = GUPPI_NET_HIST_QUOTES (obj);

  guppi_free (hq->symbol);
  hq->symbol = NULL;

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

#define GUPPI_DIR ".guppi"
#define CACHE_DIR "net-hist-quotes"
#define DIR_MODE 0700

static const char *
cache_dir (void)
{
  static gchar *cache = NULL;

  if (cache == NULL) {
    gchar *dir = g_concat_dir_and_file (g_get_home_dir (), GUPPI_DIR);
    guppi_outside_alloc (dir);
    mkdir (dir, DIR_MODE);

    cache = g_concat_dir_and_file (dir, CACHE_DIR);
    guppi_outside_alloc (cache);
    guppi_permanent_alloc (cache);
    mkdir (cache, DIR_MODE);

    guppi_free (dir);
  }

  return cache;
}

static gchar *
cache_filename (const gchar *symbol)
{
  gchar *filename = guppi_strdup_printf ("hist_%s.data", symbol);
  gchar *s = filename;
  gchar *path;

  while (*s) {
    if (isupper ((guchar)*s))
      *s = tolower ((guchar)*s);
    ++s;
  }
  
  path = g_concat_dir_and_file (cache_dir (), filename);
  guppi_outside_alloc (path);
  guppi_free (filename);

  return path;
}

static void
save_to_cache (GuppiPriceSeries *ser, const gchar *symbol)
{
  gchar *filename;

  g_return_if_fail (ser && GUPPI_IS_PRICE_SERIES (ser));
  g_return_if_fail (symbol);

  filename = cache_filename (symbol);
  guppi_data_write_xml_file (GUPPI_DATA (ser), filename);
  guppi_free (filename);
}

static GuppiPriceSeries *
load_from_cache (const gchar *symbol)
{
  gchar *filename;
  GuppiData *d;

  g_return_val_if_fail (symbol, NULL);

  filename = cache_filename (symbol);
  d = guppi_data_read_xml_file (filename);
  guppi_free (filename);

  return GUPPI_PRICE_SERIES0 (d);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static gboolean
needed_dates (GuppiPriceSeries *ser, GDate *sd, GDate *ed)
{
  time_t now;
  struct tm *tm;
  GDate today;
  GDate *ps_sd;
  GDate *ps_ed;

  now = time (NULL);
  tm = localtime (&now);
  g_date_set_dmy (&today, tm->tm_mday, tm->tm_mon+1, 1900+tm->tm_year);

  ps_sd = (GDate *)guppi_date_indexed_start (GUPPI_DATE_INDEXED (ser)); 
  ps_ed = (GDate *)guppi_date_indexed_end (GUPPI_DATE_INDEXED (ser));

  if (g_date_invalid (ps_sd) || g_date_invalid (ps_ed)
      || guppi_date_indexed_size (GUPPI_DATE_INDEXED (ser)) == 0) {
    g_date_set_dmy (sd, 1, 1, 1960); /* Based on Yahoo!'s earliest data */
    *ed = today;
    return TRUE;
  }

  if (g_date_lt (ps_ed, &today)) {
    *sd = *ps_ed;
    g_date_add_days (sd, 1);
    *ed = today;
    return TRUE;
  }

  return FALSE;
}

static void
import (GuppiDataImporter *imp,
	void (*fn) (GuppiData *, gpointer),
	gpointer user_data)
{
  GuppiPriceSeries *price_series;
  GuppiNetHistQuotes *hq = GUPPI_NET_HIST_QUOTES (imp);
  const gchar *symbol = guppi_net_hist_quotes_symbol (hq);
  GDate start_date, end_date;
  gboolean new_series = FALSE;
  gint sz;

  if (hq->get_method == NULL)
    return;

  price_series = load_from_cache (symbol);
  if (price_series == NULL) {
    price_series = GUPPI_PRICE_SERIES (guppi_price_series_new ());
    new_series = TRUE;
  }
  sz = guppi_date_indexed_size (GUPPI_DATE_INDEXED (price_series));

  if (needed_dates (price_series, &start_date, &end_date)) {
    gboolean get_retval;

    g_message ("requesting %d-%d-%d to %d-%d-%d",
	       g_date_year (&start_date),
	       g_date_month (&start_date),
	       g_date_day (&start_date),
	       g_date_year (&end_date),
	       g_date_month (&end_date),
	       g_date_day (&end_date));
    get_retval = hq->get_method (hq, price_series, &start_date, &end_date);

    if (get_retval) {
      
      if (guppi_date_indexed_size (GUPPI_DATE_INDEXED (price_series)) != sz)
	save_to_cache (price_series, symbol);

    } else {
      g_message ("could not connect to server");
      if (new_series)
	g_message ("No data for stock \"%s\" available.", symbol);
    }
  }

  guppi_data_set_label (GUPPI_DATA (price_series), symbol);

  fn (GUPPI_DATA (price_series), user_data);
  guppi_unref (price_series);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
guppi_net_hist_quotes_class_init (GuppiNetHistQuotesClass *klass)
{
  GtkObjectClass* object_class = (GtkObjectClass *)klass;
  GuppiDataImporterClass *imp_class = GUPPI_DATA_IMPORTER_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_DATA_IMPORTER);

  object_class->get_arg = guppi_net_hist_quotes_get_arg;
  object_class->set_arg = guppi_net_hist_quotes_set_arg;
  object_class->finalize = guppi_net_hist_quotes_finalize;

  imp_class->import = import;

  gtk_object_add_arg_type ("GuppiNetHistQuotes::symbol",
			   GTK_TYPE_POINTER,
			   GTK_ARG_READWRITE,
			   ARG_SYMBOL);
}

static void
guppi_net_hist_quotes_init (GuppiNetHistQuotes *obj)
{
  /* For now, our default method is via Yahoo! */
  
  obj->get_method = via_yahoo;
}

GtkType
guppi_net_hist_quotes_get_type (void)
{
  static GtkType guppi_net_hist_quotes_type = 0;
  if (!guppi_net_hist_quotes_type) {
    static const GtkTypeInfo guppi_net_hist_quotes_info = {
      "GuppiNetHistQuotes",
      sizeof (GuppiNetHistQuotes),
      sizeof (GuppiNetHistQuotesClass),
      (GtkClassInitFunc)guppi_net_hist_quotes_class_init,
      (GtkObjectInitFunc)guppi_net_hist_quotes_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    guppi_net_hist_quotes_type = gtk_type_unique (GUPPI_TYPE_DATA_IMPORTER, &guppi_net_hist_quotes_info);
  }
  return guppi_net_hist_quotes_type;
}

GuppiDataImporter *
guppi_net_hist_quotes_new (void)
{
  return GUPPI_DATA_IMPORTER (guppi_type_new (guppi_net_hist_quotes_get_type ()));
}

const gchar *
guppi_net_hist_quotes_symbol (GuppiNetHistQuotes *hq)
{
  g_return_val_if_fail (hq && GUPPI_IS_NET_HIST_QUOTES (hq), NULL);

  return hq->symbol;
}

void
guppi_net_hist_quotes_set_symbol (GuppiNetHistQuotes *hq, const gchar *str)
{
  g_return_if_fail (hq && GUPPI_IS_NET_HIST_QUOTES (hq));
  g_return_if_fail (str);

  guppi_free (hq->symbol);
  hq->symbol = guppi_strdup (str);
}
