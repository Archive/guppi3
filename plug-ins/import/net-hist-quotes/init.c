/* $Id$ */

/*
 * init.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 * and Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <guppi-data-importer-plug-in.h>
#include "guppi-net-hist-quotes.h"
#include "scm-net-hist-quotes.h"
#ifdef HAVE_PYTHON
void python_net_hist_quotes_init (void);
#endif

GuppiPlugIn *guppi_plug_in (void);

/*************************************************************************/

GuppiPlugIn *
guppi_plug_in (void)
{
  GuppiPlugIn *pi;
  GuppiDataImporterPlugIn *dipi;

  pi = guppi_data_importer_plug_in_new ();
  dipi = GUPPI_DATA_IMPORTER_PLUG_IN (pi);

  pi->magic_number = GUPPI_PLUG_IN_MAGIC_NUMBER;

  dipi->construct = guppi_net_hist_quotes_new;

  /* Do any other necessary initialization here. */

  scm_net_hist_quotes_init ();
  
#ifdef HAVE_PYTHON
  python_net_hist_quotes_init ();
#endif

  return pi;
}


/* $Id$ */
