/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-dataset-editor.c
 *
 * Copyright (C) 1999 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "guppi-dataset-editor.h"

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0
};

static void
guppi_dataset_editor_get_arg (GtkObject * obj, GtkArg * arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_dataset_editor_set_arg (GtkObject * obj, GtkArg * arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_dataset_editor_destroy (GtkObject * obj)
{
  gtk_signal_disconnect_by_data (GTK_OBJECT
				 (GUPPI_DATASET_EDITOR (obj)->dataset), obj);

  if (parent_class->destroy)
    parent_class->destroy (obj);
}

static void
guppi_dataset_editor_finalize (GtkObject * obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_dataset_editor_class_init (GuppiDatasetEditorClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;

  parent_class = gtk_type_class (GTK_TYPE_FRAME);

  object_class->get_arg = guppi_dataset_editor_get_arg;
  object_class->set_arg = guppi_dataset_editor_set_arg;
  object_class->destroy = guppi_dataset_editor_destroy;
  object_class->finalize = guppi_dataset_editor_finalize;

}

static void
guppi_dataset_editor_init (GuppiDatasetEditor * obj)
{

}

GtkType guppi_dataset_editor_get_type (void)
{
  static GtkType guppi_dataset_editor_type = 0;
  if (!guppi_dataset_editor_type) {
    static const GtkTypeInfo guppi_dataset_editor_info = {
      "GuppiDatasetEditor",
      sizeof (GuppiDatasetEditor),
      sizeof (GuppiDatasetEditorClass),
      (GtkClassInitFunc) guppi_dataset_editor_class_init,
      (GtkObjectInitFunc) guppi_dataset_editor_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_dataset_editor_type =
      gtk_type_unique (GTK_TYPE_FRAME, &guppi_dataset_editor_info);
  }
  return guppi_dataset_editor_type;
}

static void
on_name_entry_activate (GtkEntry * e, GuppiDataset * ds)
{
  guppi_dataset_set_name (ds, gtk_entry_get_text (e));
}

static void
on_name_entry_focus_out (GtkEntry * e, GdkEventFocus * foo, GuppiDataset * ds)
{
  on_name_entry_activate (e, ds);
}

static void
on_source_entry_activate (GtkEntry * e, GuppiDataset * ds)
{
  guppi_dataset_set_source (ds, gtk_entry_get_text (e));
}

static void
on_source_entry_focus_out (GtkEntry * e, GdkEventFocus * foo,
			   GuppiDataset * ds)
{
  on_source_entry_activate (e, ds);
}

static void
on_reference_entry_activate (GtkEntry * e, GuppiDataset * ds)
{
  guppi_dataset_set_reference (ds, gtk_entry_get_text (e));
}

static void
on_reference_entry_focus_out (GtkEntry * e, GdkEventFocus * foo,
			      GuppiDataset * ds)
{
  on_reference_entry_activate (e, ds);
}

static void
on_contact_entry_activate (GtkEntry * e, GuppiDataset * ds)
{
  guppi_dataset_set_contact (ds, gtk_entry_get_text (e));
}

static void
on_contact_entry_focus_out (GtkEntry * e, GdkEventFocus * foo,
			    GuppiDataset * ds)
{
  on_contact_entry_activate (e, ds);
}

static void
on_URL_entry_activate (GtkEntry * e, GuppiDataset * ds)
{
  guppi_dataset_set_URL (ds, gtk_entry_get_text (e));
}

static void
on_URL_entry_focus_out (GtkEntry * e, GdkEventFocus * foo, GuppiDataset * ds)
{
  on_URL_entry_activate (e, ds);
}

static void
on_notes_text_changed (GtkText * t, GuppiDataset * ds)
{
  guppi_dataset_set_notes (ds,
			   gtk_editable_get_chars (GTK_EDITABLE (t), 0, -1));
}

static void
guppi_dataset_editor_push_state (GuppiDatasetEditor * gde)
{
  g_return_if_fail (gde != NULL);

  gtk_entry_set_text (gde->name_entry, guppi_dataset_name (gde->dataset));
  gtk_entry_set_text (gde->source_entry, guppi_dataset_source (gde->dataset));
  gtk_entry_set_text (gde->reference_entry,
		      guppi_dataset_reference (gde->dataset));
  gtk_entry_set_text (gde->contact_entry,
		      guppi_dataset_contact (gde->dataset));
  gtk_entry_set_text (gde->URL_entry, guppi_dataset_URL (gde->dataset));

  gtk_text_freeze (gde->notes_text);
  gtk_text_set_point (gde->notes_text, 0);
  gtk_text_forward_delete (gde->notes_text,
			   gtk_text_get_length (gde->notes_text));
  gtk_text_insert (gde->notes_text, NULL, NULL, NULL,
		   guppi_dataset_notes (gde->dataset), -1);
  gtk_text_thaw (gde->notes_text);
}

GtkWidget *
guppi_dataset_editor_new (GuppiDataset * dataset)
{
  GuppiDatasetEditor *gde;

  GtkWidget *table1;
  GtkWidget *label5;
  GtkWidget *label4;
  GtkWidget *label3;
  GtkWidget *label2;
  GtkWidget *label1;
  GtkWidget *name_entry;
  GtkWidget *source_entry;
  GtkWidget *reference_entry;
  GtkWidget *contact_entry;
  GtkWidget *url_entry;
  GtkWidget *scrolledwindow1;
  GtkWidget *notes_text;
  GtkWidget *label6;

  g_return_val_if_fail (dataset != NULL, NULL);

  gde =
    GUPPI_DATASET_EDITOR (guppi_type_new (guppi_dataset_editor_get_type ()));

  gde->dataset = dataset;
  gtk_object_ref (GTK_OBJECT (gde->dataset));

  /* Now a frenzy of interface */

  gtk_frame_set_shadow_type (GTK_FRAME (gde), GTK_SHADOW_ETCHED_IN);
  gtk_container_set_border_width (GTK_CONTAINER (gde), 4);

  table1 = gtk_table_new (7, 2, FALSE);
  gtk_widget_show (table1);
  gtk_container_add (GTK_CONTAINER (gde), table1);
  gtk_table_set_row_spacings (GTK_TABLE (table1), 5);
  gtk_table_set_col_spacings (GTK_TABLE (table1), 5);

  label5 = gtk_label_new (_("URL"));
  gtk_widget_show (label5);
  gtk_table_attach (GTK_TABLE (table1), label5, 0, 1, 4, 5,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label5), 1, 0.5);

  label4 = gtk_label_new (_("Contact"));
  gtk_widget_show (label4);
  gtk_table_attach (GTK_TABLE (table1), label4, 0, 1, 3, 4,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label4), 1, 0.5);

  label3 = gtk_label_new (_("Reference"));
  gtk_widget_show (label3);
  gtk_table_attach (GTK_TABLE (table1), label3, 0, 1, 2, 3,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label3), 1, 0.5);

  label2 = gtk_label_new (_("Source"));
  gtk_widget_show (label2);
  gtk_table_attach (GTK_TABLE (table1), label2, 0, 1, 1, 2,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label2), 1, 0.5);

  label1 = gtk_label_new (_("Name"));
  gtk_widget_show (label1);
  gtk_table_attach (GTK_TABLE (table1), label1, 0, 1, 0, 1,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label1), 1, 0.5);

  name_entry = gtk_entry_new ();
  gtk_widget_show (name_entry);
  gtk_table_attach (GTK_TABLE (table1), name_entry, 1, 2, 0, 1,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  source_entry = gtk_entry_new ();
  gtk_widget_show (source_entry);
  gtk_table_attach (GTK_TABLE (table1), source_entry, 1, 2, 1, 2,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  reference_entry = gtk_entry_new ();
  gtk_widget_show (reference_entry);
  gtk_table_attach (GTK_TABLE (table1), reference_entry, 1, 2, 2, 3,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  contact_entry = gtk_entry_new ();
  gtk_widget_show (contact_entry);
  gtk_table_attach (GTK_TABLE (table1), contact_entry, 1, 2, 3, 4,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  url_entry = gtk_entry_new ();
  gtk_widget_show (url_entry);
  gtk_table_attach (GTK_TABLE (table1), url_entry, 1, 2, 4, 5,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  scrolledwindow1 = gtk_scrolled_window_new (NULL, NULL);
  gtk_widget_show (scrolledwindow1);
  gtk_table_attach (GTK_TABLE (table1), scrolledwindow1, 1, 2, 5, 7,
		    (GtkAttachOptions) (0),
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), 0, 0);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwindow1),
				  GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);

  notes_text = gtk_text_new (NULL, NULL);
  gtk_widget_show (notes_text);
  gtk_container_add (GTK_CONTAINER (scrolledwindow1), notes_text);
  gtk_text_set_editable (GTK_TEXT (notes_text), TRUE);

  label6 = gtk_label_new (_("Notes"));
  gtk_widget_show (label6);
  gtk_table_attach (GTK_TABLE (table1), label6, 0, 1, 5, 6,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (GTK_FILL), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label6), 1, 0);


  /* That should be enough frenzy for just about anyone... */

  gde->name_entry = GTK_ENTRY (name_entry);
  gde->source_entry = GTK_ENTRY (source_entry);
  gde->reference_entry = GTK_ENTRY (reference_entry);
  gde->contact_entry = GTK_ENTRY (contact_entry);
  gde->URL_entry = GTK_ENTRY (url_entry);
  gde->notes_text = GTK_TEXT (notes_text);

  guppi_dataset_editor_push_state (gde);
  gtk_signal_connect_object (GTK_OBJECT (gde->dataset),
			     "changed",
			     GTK_SIGNAL_FUNC
			     (guppi_dataset_editor_push_state),
			     GTK_OBJECT (gde));

  gtk_signal_connect (GTK_OBJECT (name_entry),
		      "activate",
		      GTK_SIGNAL_FUNC (on_name_entry_activate), gde->dataset);
  gtk_signal_connect (GTK_OBJECT (name_entry),
		      "focus_out_event",
		      GTK_SIGNAL_FUNC (on_name_entry_focus_out),
		      gde->dataset);

  gtk_signal_connect (GTK_OBJECT (source_entry),
		      "activate",
		      GTK_SIGNAL_FUNC (on_source_entry_activate),
		      gde->dataset);
  gtk_signal_connect (GTK_OBJECT (source_entry),
		      "focus_out_event",
		      GTK_SIGNAL_FUNC (on_source_entry_focus_out),
		      gde->dataset);

  gtk_signal_connect (GTK_OBJECT (reference_entry),
		      "activate",
		      GTK_SIGNAL_FUNC (on_reference_entry_activate),
		      gde->dataset);
  gtk_signal_connect (GTK_OBJECT (reference_entry),
		      "focus_out_event",
		      GTK_SIGNAL_FUNC (on_reference_entry_focus_out),
		      gde->dataset);

  gtk_signal_connect (GTK_OBJECT (contact_entry),
		      "activate",
		      GTK_SIGNAL_FUNC (on_contact_entry_activate),
		      gde->dataset);
  gtk_signal_connect (GTK_OBJECT (contact_entry),
		      "focus_out_event",
		      GTK_SIGNAL_FUNC (on_contact_entry_focus_out),
		      gde->dataset);

  gtk_signal_connect (GTK_OBJECT (url_entry),
		      "activate",
		      GTK_SIGNAL_FUNC (on_URL_entry_activate), gde->dataset);
  gtk_signal_connect (GTK_OBJECT (url_entry),
		      "focus_out_event",
		      GTK_SIGNAL_FUNC (on_URL_entry_focus_out), gde->dataset);

  gtk_signal_connect (GTK_OBJECT (notes_text),
		      "changed",
		      GTK_SIGNAL_FUNC (on_notes_text_changed), gde->dataset);

  return GTK_WIDGET (gde);
}



/* $Id$ */
