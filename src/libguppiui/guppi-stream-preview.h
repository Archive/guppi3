/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-stream-preview.h
 *
 * Copyright (C) 1999 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_STREAM_PREVIEW_H
#define _INC_GUPPI_STREAM_PREVIEW_H

#include <config.h>
#include <gnome.h>

#include  "guppi-stream.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS
/*
 Via the EXTRA_CONTROLS cpp symbol, I've "commented out" the code
 that contained the preload button & comment selection menus.  This is
 now just purely a view of the state of a GuppiStream object, not a
 mixed view/controller. (Behold my cool UI design lingo.  I hope I'm
 using these terms properly.)
*/
#undef EXTRA_CONTROLS
typedef struct _GuppiStreamPreview GuppiStreamPreview;
typedef struct _GuppiStreamPreviewClass GuppiStreamPreviewClass;

struct _GuppiStreamPreview {
  GtkFrame parent;

  GuppiStream *stream;
  GtkLabel *info_label;
  GtkText *text;
  GtkAdjustment *vadj;
  GtkFrame *preview_frame;

#ifdef EXTRA_CONTROLS
  GtkButton *preload_button;
  GtkEntry *eol_combo_entry;
  GtkEntry *begin_ml_combo_entry;
  GtkEntry *end_ml_combo_entry;
#endif

    gboolean (*line_skip) (GuppiStreamPreview *, const gchar * line, gint line_no,
			   gpointer user_data);
  gpointer line_skip_user_data;

  GdkColor *(*extra_info) (GuppiStreamPreview *, const gchar * line,
			   gint line_no, gchar * sbuf, gint sbuf_len,
			   gpointer user_data);
  gpointer extra_info_user_data;

  GdkColor *(*line_color) (GuppiStreamPreview *, const gchar * line,
			   gint line_no, gpointer user_data);
  gpointer line_color_user_data;
};

struct _GuppiStreamPreviewClass {
  GtkFrameClass parent_class;
};

#define GUPPI_TYPE_STREAM_PREVIEW (guppi_stream_preview_get_type())
#define GUPPI_STREAM_PREVIEW(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_STREAM_PREVIEW,GuppiStreamPreview))
#define GUPPI_STREAM_PREVIEW_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_STREAM_PREVIEW,GuppiStreamPreviewClass))
#define GUPPI_IS_STREAM_PREVIEW(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_STREAM_PREVIEW))
#define GUPPI_IS_STREAM_PREVIEW_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_STREAM_PREVIEW))

GtkType guppi_stream_preview_get_type (void);

void guppi_stream_preview_construct (GuppiStreamPreview *);
GtkWidget *guppi_stream_preview_new (GuppiStream *);
void guppi_stream_preview_set_stream (GuppiStreamPreview *, GuppiStream *);

void guppi_stream_preview_refresh (GuppiStreamPreview *);

void guppi_stream_preview_set_line_skip_cb (GuppiStreamPreview *,
					    gboolean (*)(GuppiStreamPreview *,
							 const gchar *,
							 gint,
							 gpointer), gpointer);
void guppi_stream_preview_set_extra_info_cb (GuppiStreamPreview *,
					     GdkColor *
					     (*)(GuppiStreamPreview *,
						 const gchar *, gint, gchar *,
						 gint, gpointer), gpointer);
void guppi_stream_preview_set_line_color_cb (GuppiStreamPreview *,
					     GdkColor *
					     (*)(GuppiStreamPreview *,
						 const gchar *, gint,
						 gpointer), gpointer);

GtkWidget *guppi_stream_preview_glade_custom_func (gchar * name,
						   gchar * dum1, gchar * dum2,
						   gint dum3, gint dum4);




END_GUPPI_DECLS
#endif /* _INC_GUPPI_STREAM_PREVIEW_H */
/* $Id$ */
