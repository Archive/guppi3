/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-type-changer.c
 *
 * Copyright (C) 1999 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "text-style.h"
#include "guppi-type-changer.h"

static GtkObjectClass *parent_class = NULL;

enum {
  TYPE_CHANGED,
  LAST_SIGNAL
};

static guint gtc_signals[LAST_SIGNAL] = { 0 };

enum {
  ARG_0
};

static void
guppi_type_changer_get_arg (GtkObject * obj, GtkArg * arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_type_changer_set_arg (GtkObject * obj, GtkArg * arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_type_changer_destroy (GtkObject * obj)
{
  if (parent_class->destroy)
    parent_class->destroy (obj);
}

static void
guppi_type_changer_finalize (GtkObject * obj)
{
  gtk_object_unref (GTK_OBJECT (GUPPI_TYPE_CHANGER (obj)->data));

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_type_changer_class_init (GuppiTypeChangerClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;

  parent_class = gtk_type_class (GTK_TYPE_FRAME);

  gtc_signals[TYPE_CHANGED] =
    gtk_signal_new ("type_changed",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GuppiTypeChangerClass, type_changed),
		    gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);
  gtk_object_class_add_signals (object_class, gtc_signals, LAST_SIGNAL);

  object_class->get_arg = guppi_type_changer_get_arg;
  object_class->set_arg = guppi_type_changer_set_arg;
  object_class->destroy = guppi_type_changer_destroy;
  object_class->finalize = guppi_type_changer_finalize;

}

static void
guppi_type_changer_init (GuppiTypeChanger * obj)
{
  obj->fully_valid = FALSE;
}

GtkType guppi_type_changer_get_type (void)
{
  static GtkType guppi_type_changer_type = 0;
  if (!guppi_type_changer_type) {
    static const GtkTypeInfo guppi_type_changer_info = {
      "GuppiTypeChanger",
      sizeof (GuppiTypeChanger),
      sizeof (GuppiTypeChangerClass),
      (GtkClassInitFunc) guppi_type_changer_class_init,
      (GtkObjectInitFunc) guppi_type_changer_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_type_changer_type =
      gtk_type_unique (GTK_TYPE_FRAME, &guppi_type_changer_info);
  }
  return guppi_type_changer_type;
}

/***************************************************************************/

static void
guppi_type_changer_render (GuppiTypeChanger * gtc)
{
  dindex_t i0, i1, i;
  gboolean valid;
  GdkFont *ln_f;
  GdkFont *l_f;
  GdkColor *ln_c;
  GdkColor *v_c;
  GdkColor *iv_c;
  gboolean repl_valid, repl_tested;
  gchar *invalid_repl;
  gint invalid_count;

  const gint buffer_len = 1024;
  gchar buffer[1024];

  g_return_if_fail (gtc != NULL);

  ln_f = style_line_number_font ();
  l_f = style_line_font ();
  ln_c = style_active_line_number_color ();
  v_c = style_active_line_color ();
  iv_c = style_invalid_line_color ();

  gtk_text_freeze (gtc->text);

  gtk_text_set_point (gtc->text, 0);
  gtk_text_forward_delete (gtc->text, gtk_text_get_length (gtc->text));

  invalid_count = 0;
  invalid_repl = gtk_entry_get_text (gtc->invalid_entry);
  repl_valid = FALSE;
  repl_tested = FALSE;

  gtc->fully_valid = TRUE;

  i0 = guppi_data_min_index (gtc->data);
  i1 = guppi_data_max_index (gtc->data);

  for (i = i0; i <= i1; ++i) {

    if (i != i0)
      gtk_text_insert (gtc->text, l_f, NULL, NULL, "\n", 1);

    g_snprintf (buffer, buffer_len, "%4d ", i);
    gtk_text_insert (gtc->text, ln_f, ln_c, NULL, buffer, -1);

    guppi_data_get (gtc->data, i, buffer, buffer_len);

    if (gtc->new_type == 0 ||
	gtc->new_type == GTK_OBJECT (gtc->data)->klass->type) {
      valid = TRUE;
    } else {
      valid = guppi_data_validate_by_type (gtc->new_type, buffer, NULL, 0);
    }

    gtk_text_insert (gtc->text, l_f, valid ? v_c : iv_c, NULL, buffer, -1);

    if (!valid && invalid_repl) {

      ++invalid_count;

      gtk_text_insert (gtc->text, l_f, v_c, NULL, " => ", -1);

      if (!repl_tested) {
	repl_valid = guppi_data_validate_by_type (gtc->new_type, invalid_repl,
						  NULL, 0);
	repl_tested = TRUE;
      }

      gtk_text_insert (gtc->text, l_f,
		       repl_valid ? v_c : iv_c, NULL, invalid_repl, -1);


    }

    if (!valid && !repl_valid)
      gtc->fully_valid = FALSE;

  }
  gtk_text_thaw (gtc->text);

  /* Set invalid entry sensitivity based on whether or not there are
     actually any invalid items. */
  gtk_widget_set_sensitive (GTK_WIDGET (gtc->invalid_label),
			    invalid_count > 0);
  gtk_widget_set_sensitive (GTK_WIDGET (gtc->invalid_entry),
			    invalid_count > 0);

  /* Now put in some informational text */
  if (invalid_count > 0) {
    g_snprintf (buffer, buffer_len,
		_("%d lines (%d invalid) range = %d:%d"),
		guppi_data_size (gtc->data),
		invalid_count,
		guppi_data_min_index (gtc->data),
		guppi_data_max_index (gtc->data));
  } else {
    g_snprintf (buffer, buffer_len,
		_("%d lines, range = %d:%d"),
		guppi_data_size (gtc->data),
		guppi_data_min_index (gtc->data),
		guppi_data_max_index (gtc->data));
  }
  gtk_label_set_text (gtc->info_label, buffer);

  /* We emit this signal here.  Sort of a lousy hack. */
  gtk_signal_emit (GTK_OBJECT (gtc), gtc_signals[TYPE_CHANGED]);
}

/**************************************************************************/

static void
on_label_entry_activate (GtkWidget * w, gpointer user_data)
{
  g_return_if_fail (w != NULL);
  g_return_if_fail (user_data != NULL);

  guppi_data_set_label (GUPPI_DATA (user_data),
			gtk_entry_get_text (GTK_ENTRY (w)));
}

static void
on_label_entry_focus_out (GtkWidget * w, GdkEventFocus * foo,
			  gpointer user_data)
{
  on_label_entry_activate (w, user_data);
}

static void
on_ignore_toggle (GtkWidget * w, gpointer user_data)
{
  GuppiTypeChanger *gtc;

  gtc = GUPPI_TYPE_CHANGER (user_data);
  g_return_if_fail (gtc != NULL);

  gtc->ignore = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (w));
  gtk_widget_set_sensitive (gtc->main_box, !gtc->ignore);
  gtk_signal_emit (GTK_OBJECT (gtc), gtc_signals[TYPE_CHANGED]);
}

static void
on_type_menu_activate (GtkWidget * w, gpointer user_data)
{
  GtkType type;
  GuppiTypeChanger *gtc;

  gtc = GUPPI_TYPE_CHANGER (user_data);
  g_return_if_fail (gtc != NULL);

  type = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (w), "type"));

  if (type != gtc->new_type) {
    gtc->new_type = type;
    guppi_type_changer_render (gtc);
  }
}


GtkWidget *
guppi_type_changer_new (GuppiData * data, GtkType * possibles,
			GtkType initial_guess)
{
  GuppiTypeChanger *gtc;

  GtkWidget *column_frame;
  GtkWidget *bonus_vbox;
  GtkWidget *ignore_button;
  GtkWidget *vbox1;
  GtkWidget *table1;
  GtkWidget *label_entry;
  GtkWidget *label1;
  GtkWidget *label2;
  GtkWidget *data_type_optionmenu;
  GtkWidget *data_type_optionmenu_menu;
  GtkWidget *glade_menuitem;
  GtkWidget *hseparator1;
  GtkWidget *scrolledwindow1;
  GtkWidget *text;
  GtkWidget *hseparator2;
  GtkWidget *table2;
  GtkWidget *invalid_items_entry;
  GtkWidget *invalid_items_label;
  GtkWidget *label5;
  gint match;

  g_return_val_if_fail (data != NULL, NULL);
  g_return_val_if_fail (possibles != NULL, NULL);

  gtc = GUPPI_TYPE_CHANGER (guppi_type_new (guppi_type_changer_get_type ()));

  gtc->data = data;
  gtk_object_ref (GTK_OBJECT (data));

  gtc->new_type = initial_guess;
  if (gtc->new_type == 0)
    gtc->new_type = GTK_OBJECT (data)->klass->type;

  column_frame = GTK_WIDGET (gtc);
  gtk_container_set_border_width (GTK_CONTAINER (column_frame), 4);

  bonus_vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (column_frame), bonus_vbox);

  ignore_button = gtk_check_button_new_with_label (_("Ignore This Data"));
  gtk_box_pack_start (GTK_BOX (bonus_vbox), ignore_button, FALSE, TRUE, 4);

  vbox1 = gtk_vbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (bonus_vbox), vbox1, TRUE, TRUE, 0);
  gtc->main_box = vbox1;

  table1 = gtk_table_new (2, 2, FALSE);
  gtk_box_pack_start (GTK_BOX (vbox1), table1, FALSE, FALSE, 4);

  label_entry = gtk_entry_new ();
  gtk_table_attach (GTK_TABLE (table1), label_entry, 1, 2, 0, 1,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  label1 = gtk_label_new (_("Label"));
  gtk_table_attach (GTK_TABLE (table1), label1, 0, 1, 0, 1,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label1), 1, 0.5);
  gtk_misc_set_padding (GTK_MISC (label1), 4, 0);

  label2 = gtk_label_new (_("Data Type"));
  gtk_table_attach (GTK_TABLE (table1), label2, 0, 1, 1, 2,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label2), 1, 0.5);
  gtk_misc_set_padding (GTK_MISC (label2), 4, 0);

  data_type_optionmenu = gtk_option_menu_new ();
  gtk_table_attach (GTK_TABLE (table1), data_type_optionmenu, 1, 2, 1, 2,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  data_type_optionmenu_menu = gtk_menu_new ();

  match = -1;
  {
    gint i = 0;
    const gchar *typename;
    GtkType now;

    /* We allow ourselves a (possible) extra iteration in which we 
       check to make sure that new_type is among the list of possible
       types.  If it isn't, we add it. */
    while (possibles[i] != 0 || match == -1) {

      now = possibles[i];
      if (now == 0)
	now = gtc->new_type;

      typename = guppi_data_type_name_by_type (now);
      if (typename == NULL)
	typename = _("Unknown");

      glade_menuitem = gtk_menu_item_new_with_label (typename);

      gtk_object_set_data (GTK_OBJECT (glade_menuitem),
			   "type", GINT_TO_POINTER (now));

      gtk_signal_connect (GTK_OBJECT (glade_menuitem),
			  "activate",
			  GTK_SIGNAL_FUNC (on_type_menu_activate), gtc);

      gtk_menu_append (GTK_MENU (data_type_optionmenu_menu), glade_menuitem);

      /* Find matching type to be our default value */
      if (gtc->new_type == now)
	match = i;

      if (possibles[i] != 0)
	++i;
    }
    /* This should never happen. */
    g_assert (match != -1);
  }

  gtk_option_menu_set_menu (GTK_OPTION_MENU (data_type_optionmenu),
			    data_type_optionmenu_menu);

  /* Set the proper initial value */
  gtk_option_menu_set_history (GTK_OPTION_MENU (data_type_optionmenu), match);

  hseparator1 = gtk_hseparator_new ();
  gtk_box_pack_start (GTK_BOX (vbox1), hseparator1, FALSE, FALSE, 4);

  scrolledwindow1 = gtk_scrolled_window_new (NULL, NULL);
  gtk_box_pack_start (GTK_BOX (vbox1), scrolledwindow1, TRUE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (scrolledwindow1), 4);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwindow1),
				  GTK_POLICY_NEVER, GTK_POLICY_ALWAYS);

  text = gtk_text_new (NULL, NULL);
  gtk_container_add (GTK_CONTAINER (scrolledwindow1), text);
  gtc->text = GTK_TEXT (text);

  hseparator2 = gtk_hseparator_new ();
  gtk_box_pack_start (GTK_BOX (vbox1), hseparator2, FALSE, FALSE, 4);

  table2 = gtk_table_new (3, 2, FALSE);
  gtk_box_pack_start (GTK_BOX (vbox1), table2, FALSE, FALSE, 4);

  invalid_items_entry = gtk_entry_new ();
  gtk_table_attach (GTK_TABLE (table2), invalid_items_entry, 1, 2, 2, 3,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  gtc->invalid_entry = GTK_ENTRY (invalid_items_entry);

  invalid_items_label = gtk_label_new (_("Invalid Override"));
  gtk_table_attach (GTK_TABLE (table2), invalid_items_label, 0, 1, 2, 3,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (invalid_items_label), 1, 0.5);
  gtk_misc_set_padding (GTK_MISC (invalid_items_label), 4, 0);

  gtc->invalid_label = GTK_LABEL (invalid_items_label);

  label5 = gtk_label_new ("Put info here!");
  gtk_table_attach (GTK_TABLE (table2), label5, 0, 2, 0, 1,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_SHRINK),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (label5), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (label5), 1, 0.5);
  gtk_misc_set_padding (GTK_MISC (label5), 4, 0);
  gtc->info_label = GTK_LABEL (label5);

  /* The above is hacked from Glade-generated code. */

  gtk_signal_connect (GTK_OBJECT (ignore_button),
		      "toggled", GTK_SIGNAL_FUNC (on_ignore_toggle), gtc);

  gtk_signal_connect (GTK_OBJECT (label_entry),
		      "activate",
		      GTK_SIGNAL_FUNC (on_label_entry_activate), gtc->data);
  gtk_signal_connect (GTK_OBJECT (label_entry),
		      "focus_out_event",
		      GTK_SIGNAL_FUNC (on_label_entry_focus_out), gtc->data);


  /* Repaint everything if our invalid replacement changes. */
  gtk_signal_connect_object (GTK_OBJECT (invalid_items_entry),
			     "activate",
			     GTK_SIGNAL_FUNC (guppi_type_changer_render),
			     GTK_OBJECT (gtc));
  gtk_signal_connect_object (GTK_OBJECT (invalid_items_entry),
			     "focus_out_event",
			     GTK_SIGNAL_FUNC (guppi_type_changer_render),
			     GTK_OBJECT (gtc));

  /* Put our data object's label in the label entry. */
  gtk_entry_set_text (GTK_ENTRY (label_entry),
		      guppi_data_label (GUPPI_DATA (gtc->data)));

  /* Draw everything */
  guppi_type_changer_render (gtc);

  return GTK_WIDGET (gtc);
}

gboolean guppi_type_changer_fully_valid (GuppiTypeChanger * gtc)
{
  g_return_val_if_fail (gtc != NULL, FALSE);
  return gtc->fully_valid;
}

GuppiData *
guppi_type_changer_get_converted_data (GuppiTypeChanger * gtc)
{
  gchar *invalid_repl;
  g_return_val_if_fail (gtc != NULL, NULL);

  if (gtc->new_type == 0)
    return NULL;

  if (!guppi_type_changer_fully_valid (gtc))
    return NULL;

  /* If no type conversion is needed, just pass 'er on through... */
  if (gtc->new_type == GTK_OBJECT (gtc->data)->klass->type) {
    gtk_object_ref (GTK_OBJECT (gtc->data));
    return gtc->data;
  }

  invalid_repl = gtk_entry_get_text (gtc->invalid_entry);

  return guppi_data_convert (gtc->data, gtc->new_type, invalid_repl);
}



/* $Id$ */
