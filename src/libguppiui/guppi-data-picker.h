/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-data-picker.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_DATA_PICKER_H
#define _INC_GUPPI_DATA_PICKER_H

#include <config.h>
#include <gnome.h>
#include  "guppi-data-catalog.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS typedef struct _GuppiDataPicker GuppiDataPicker;
typedef struct _GuppiDataPickerClass GuppiDataPickerClass;

struct _GuppiDataPicker {
  GtkOptionMenu parent;

  GuppiDataCatalog *catalog;
  GtkType type;
  gchar *label;
};

struct _GuppiDataPickerClass {
  GtkOptionMenuClass parent_class;

  void (*selected_data) (GuppiDataPicker *, GuppiData *);
};

#define GUPPI_TYPE_DATA_PICKER (guppi_data_picker_get_type())
#define GUPPI_DATA_PICKER(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_DATA_PICKER,GuppiDataPicker))
#define GUPPI_DATA_PICKER_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_DATA_PICKER,GuppiDataPickerClass))
#define GUPPI_IS_DATA_PICKER(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_DATA_PICKER))
#define GUPPI_IS_DATA_PICKER_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_DATA_PICKER))

GtkType guppi_data_picker_get_type (void);

void guppi_data_picker_construct (GuppiDataPicker *, GuppiDataCatalog *,
				  GtkType, const gchar * label);

GtkWidget *guppi_data_picker_new (GuppiDataCatalog *, const gchar * label);
GtkWidget *guppi_data_picker_new_by_type (GuppiDataCatalog *, GtkType,
					  const gchar * label);

GuppiData *guppi_data_picker_get_selected_data (GuppiDataPicker *);
void guppi_data_picker_set_selected_data (GuppiDataPicker *, GuppiData *);

GtkWidget *guppi_data_picker_glade_custom_func (gchar * name,
						gchar * type_restriction,
						gchar * label,
						gint dummy_int1,
						gint dummy_int2);



END_GUPPI_DECLS
#endif /* _INC_GUPPI_DATA_PICKER_H */
/* $Id$ */
