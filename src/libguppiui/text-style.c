/* $Id$ */

/*
 * text-style.c
 *
 * Copyright (C) 1999 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 * and Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "text-style.h"


GdkColor *
style_active_line_color (void)
{
  static GdkColor *c = NULL;

  if (c == NULL) {
    c = guppi_new (GdkColor, 1);
    c->red = 0;
    c->green = 0;
    c->blue = 0;
    gdk_color_alloc (gdk_colormap_get_system (), c);
  }

  return c;
}

GdkColor *
style_inactive_line_color (void)
{
  static GdkColor *c = NULL;

  if (c == NULL) {
    c = guppi_new (GdkColor, 1);
    c->red = 0x8000;
    c->green = 0x8000;
    c->blue = 0x8000;
    gdk_color_alloc (gdk_colormap_get_system (), c);
  }

  return c;
}

GdkColor *
style_active_line_number_color (void)
{
  static GdkColor *c = NULL;

  if (c == NULL) {
    c = guppi_new (GdkColor, 1);
    c->red = 0;
    c->green = 0;
    c->blue = 0xffff;
    gdk_color_alloc (gdk_colormap_get_system (), c);
  }

  return c;
}

GdkColor *
style_inactive_line_number_color (void)
{
  static GdkColor *c = NULL;

  if (c == NULL) {
    c = guppi_new (GdkColor, 1);
    c->red = 0x8000;
    c->green = 0x8000;
    c->blue = 0x6fff;
    gdk_color_alloc (gdk_colormap_get_system (), c);
  }

  return c;
}

GdkColor *
style_title_line_color (void)
{
  static GdkColor *c = NULL;

  if (c == NULL) {
    c = guppi_new (GdkColor, 1);
    c->red = 0xc000;
    c->green = 0;
    c->blue = 0;
    gdk_color_alloc (gdk_colormap_get_system (), c);
  }

  return c;
}

GdkColor *
style_invalid_line_color (void)
{
  static GdkColor *c = NULL;

  if (c == NULL) {
    c = guppi_new (GdkColor, 1);
    c->red = 0xffff;
    c->green = 0;
    c->blue = 0;
    gdk_color_alloc (gdk_colormap_get_system (), c);
  }

  return c;
}

GdkFont *
style_line_font (void)
{
  static GdkFont *f = NULL;

  if (f == NULL) {
    f = gdk_font_load ("-misc-fixed-medium-r-*-*-*-120-*-*-*-*-*-*");
  }

  return f;
}

GdkFont *
style_line_number_font (void)
{

  static GdkFont *f = NULL;
  if (f == NULL) {
    f = gdk_font_load ("-misc-fixed-medium-r-*-*-*-50-*-*-*-*-*-*");
  }

  return f;
}


/* $Id$ */
