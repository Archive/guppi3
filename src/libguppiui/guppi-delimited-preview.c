/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-delimited-preview.c
 *
 * Copyright (C) 1999 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <ctype.h>
#include "guppi-delimited-preview.h"
#include <guppi-stream.h>
#include "guppi-stream-preview.h"
#include "text-style.h"

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0
};

static void
guppi_delimited_preview_get_arg (GtkObject * obj, GtkArg * arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_delimited_preview_set_arg (GtkObject * obj, GtkArg * arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_delimited_preview_destroy (GtkObject * obj)
{
  if (parent_class->destroy)
    parent_class->destroy (obj);
}

static void
guppi_delimited_preview_finalize (GtkObject * obj)
{
  g_slist_free (GUPPI_DELIMITED_PREVIEW (obj)->menu_items);
  gtk_object_unref (GTK_OBJECT (GUPPI_DELIMITED_PREVIEW (obj)->importer));

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_delimited_preview_class_init (GuppiDelimitedPreviewClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;

  parent_class = gtk_type_class (GTK_TYPE_HBOX);

  object_class->get_arg = guppi_delimited_preview_get_arg;
  object_class->set_arg = guppi_delimited_preview_set_arg;
  object_class->destroy = guppi_delimited_preview_destroy;
  object_class->finalize = guppi_delimited_preview_finalize;

}

static void
guppi_delimited_preview_init (GuppiDelimitedPreview * obj)
{

}

GtkType guppi_delimited_preview_get_type (void)
{
  static GtkType guppi_delimited_preview_type = 0;
  if (!guppi_delimited_preview_type) {
    static const GtkTypeInfo guppi_delimited_preview_info = {
      "GuppiDelimitedPreview",
      sizeof (GuppiDelimitedPreview),
      sizeof (GuppiDelimitedPreviewClass),
      (GtkClassInitFunc) guppi_delimited_preview_class_init,
      (GtkObjectInitFunc) guppi_delimited_preview_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_delimited_preview_type = gtk_type_unique (GTK_TYPE_HBOX,
						    &guppi_delimited_preview_info);
  }
  return guppi_delimited_preview_type;
}

/**************************************************************************/

static gboolean
gdp_skip_line_for_stream_preview (GuppiStreamPreview * gsp,
				  const gchar * line, gint line_no,
				  gpointer user_data)
{
  GuppiDelimitedImporter *gdi;

  gdi = GUPPI_DELIMITED_IMPORTER (user_data);
  return guppi_delimited_importer_line_skip (gdi, line, line_no);
}

static GdkColor *
gdp_extra_info_for_stream_preview (GuppiStreamPreview * gsp,
				   const gchar * line, gint line_no,
				   gchar * sbuf, gint sbuf_len,
				   gpointer user_data)
{
  GuppiDelimitedImporter *gdi;
  gint col_count;
  gint l;
  const gchar *p;

  g_return_val_if_fail (gsp != NULL, NULL);
  g_return_val_if_fail (line != NULL, NULL);
  g_return_val_if_fail (sbuf != NULL, NULL);

  g_return_val_if_fail (user_data != NULL, NULL);
  gdi = GUPPI_DELIMITED_IMPORTER (user_data);
  g_return_val_if_fail (gdi != NULL, NULL);

  if (sbuf_len <= 0)
    return NULL;

  if (line_no == guppi_delimited_importer_title_line (gdi)) {
    g_snprintf (sbuf, sbuf_len, _("ttl"));
    return style_title_line_color ();
  }

  l = guppi_delimited_importer_skip_lines_before (gdi);
  if (l >= 0 && line_no < l) {
    g_snprintf (sbuf, sbuf_len, _("pre"));
    return style_inactive_line_number_color ();
  }

  l = guppi_delimited_importer_skip_lines_after (gdi);
  if (l >= 0 && line_no > l) {
    g_snprintf (sbuf, sbuf_len, _("pst"));
    return style_inactive_line_number_color ();
  }

  if (guppi_delimited_importer_skip_alpha (gdi)) {
    p = line;
    while (*p) {
      if (isalpha (*p)) {
	g_snprintf (sbuf, sbuf_len, _("ABC"));
	return style_inactive_line_number_color ();
      }
      ++p;
    }
  }

  guppi_delimited_importer_line_split (gdi, line, &col_count, -1, TRUE);
  if (col_count > 0) {
    g_snprintf (sbuf, sbuf_len, "%-3d", col_count);
    return NULL;
  }

  /* padding */
  g_snprintf (sbuf, sbuf_len, "   ");

  return NULL;
}

static GdkColor *
gdp_line_color_for_stream_preview (GuppiStreamPreview * gsp,
				   const gchar * line, gint line_no,
				   gpointer user_data)
{
  GuppiDelimitedImporter *gdi;

  gdi = GUPPI_DELIMITED_IMPORTER (user_data);

  if (guppi_delimited_importer_title_line (gdi) == line_no)
    return style_title_line_color ();

  return NULL;
}



/**************************************************************************/

static void
guppi_delimited_preview_grab_importer_state (GuppiDelimitedPreview * gp)
{
  gchar buffer[32];
  const gint buffer_len = 32;
  gint x;
  GSList *iter;
  gchar *c;
  gboolean other;

  g_return_if_fail (gp != NULL);
  g_return_if_fail (gp->importer != NULL);

  gtk_spin_button_set_value (gp->col_spinbutton,
			     guppi_delimited_importer_columns (gp->importer));

  /* Handle delim menu */
  other = FALSE;
  iter = gp->menu_items;
  x = 0;
  while (iter != NULL) {
    c = gtk_object_get_data (GTK_OBJECT (iter->data), "char");
    if (*c == guppi_delimited_importer_delimiter (gp->importer) || *c == '\0') {
      gtk_option_menu_set_history (gp->delim_option_menu, x);
      if (*c == '\0')
	other = TRUE;
      iter = NULL;
    } else {
      iter = g_slist_next (iter);
      ++x;
    }
  }

  gtk_widget_set_sensitive (GTK_WIDGET (gp->other_label), other);
  gtk_widget_set_sensitive (GTK_WIDGET (gp->other_entry), other);

  if (other) {
    buffer[0] = guppi_delimited_importer_delimiter (gp->importer);
    buffer[1] = '\0';
    gtk_entry_set_text (gp->other_entry, buffer);
  }

  x = guppi_delimited_importer_title_line (gp->importer);
  if (x >= 0)
    g_snprintf (buffer, buffer_len, "%d", x);
  else
    buffer[0] = '\0';
  gtk_entry_set_text (gp->titleline_entry, buffer);

  x = guppi_delimited_importer_skip_lines_before (gp->importer);
  if (x >= 0)
    g_snprintf (buffer, buffer_len, "%d", x);
  else
    buffer[0] = '\0';
  gtk_entry_set_text (gp->skipbefore_entry, buffer);

  x = guppi_delimited_importer_skip_lines_after (gp->importer);
  if (x >= 0)
    g_snprintf (buffer, buffer_len, "%d", x);
  else
    buffer[0] = '\0';
  gtk_entry_set_text (gp->skipafter_entry, buffer);

  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (gp->noncontig_checkbutton),
				guppi_delimited_importer_skip_noncontiguous
				(gp->importer));

  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (gp->nonalpha_checkbutton),
				guppi_delimited_importer_skip_alpha (gp->
								     importer));

  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
				(gp->autofilter_checkbutton),
				guppi_delimited_importer_autofilter (gp->
								     importer));

}


/**************************************************************************/

/* This can do something more clever later.  In fact, this is
   downright wrong. */
static gint
gdp_text_to_line_number (GuppiDelimitedPreview * gdp, gchar * txt)
{
  gint l = atoi (txt);
  return (l == 0) ? -1 : l;
}

static void
on_col_spinbox_adj_changed (GtkAdjustment * adj, gpointer user_data)
{
  GuppiDelimitedPreview *gdp;

  gdp = GUPPI_DELIMITED_PREVIEW (user_data);
  g_return_if_fail (gdp != NULL);

  guppi_delimited_importer_set_columns (gdp->importer, (gint) (adj->value));
}

static void
on_delim_optionmenu_activate (GtkMenuItem * item, gpointer user_data)
{
  GuppiDelimitedPreview *gdp;
  gchar *c;

  gdp = GUPPI_DELIMITED_PREVIEW (user_data);
  g_return_if_fail (gdp != NULL);

  c = gtk_object_get_data (GTK_OBJECT (item), "char");

  gtk_widget_set_sensitive (GTK_WIDGET (gdp->other_label), *c == '\0');
  gtk_widget_set_sensitive (GTK_WIDGET (gdp->other_entry), *c == '\0');

  if (*c != '\0') {
    guppi_delimited_importer_set_delimiter (gdp->importer, *c);
  } else {
    c = gtk_entry_get_text (gdp->other_entry);
    guppi_delimited_importer_set_delimiter (gdp->importer, c ? *c : '\0');
  }
}

static void
on_other_entry_activate (GtkEntry * entry, gpointer user_data)
{
  GuppiDelimitedPreview *gdp;
  gchar *txt;

  gdp = GUPPI_DELIMITED_PREVIEW (user_data);
  g_return_if_fail (gdp != NULL);

  txt = gtk_entry_get_text (entry);
  guppi_delimited_importer_set_delimiter (gdp->importer, txt ? *txt : '\0');

}

static void
on_other_entry_focus_out (GtkWidget * w, GdkEventFocus * foo,
			  gpointer user_data)
{
  on_other_entry_activate (GTK_ENTRY (w), user_data);
}



static void
on_titleline_entry_activate (GtkEntry * entry, gpointer user_data)
{
  GuppiDelimitedPreview *gdp;
  gint line;

  gdp = GUPPI_DELIMITED_PREVIEW (user_data);
  g_return_if_fail (gdp != NULL);

  line = gdp_text_to_line_number (gdp, gtk_entry_get_text (entry));
  if (line < 0) {
    gtk_entry_set_text (entry, "");
    line = -1;
  }

  guppi_delimited_importer_set_title_line (gdp->importer, line);
}

static void
on_titleline_entry_focus_out (GtkWidget * w, GdkEventFocus * foo,
			      gpointer user_data)
{
  on_titleline_entry_activate (GTK_ENTRY (w), user_data);
}

static void
on_skipbefore_entry_activate (GtkEntry * entry, gpointer user_data)
{
  GuppiDelimitedPreview *gdp;
  gint line;

  gdp = GUPPI_DELIMITED_PREVIEW (user_data);
  g_return_if_fail (gdp != NULL);

  line = gdp_text_to_line_number (gdp, gtk_entry_get_text (entry));
  if (line < 0) {
    gtk_entry_set_text (entry, "");
    line = -1;
  }
  guppi_delimited_importer_set_skip_lines_before (gdp->importer, line);
}

static void
on_skipbefore_entry_focus_out (GtkWidget * w, GdkEventFocus * foo,
			       gpointer user_data)
{
  on_skipbefore_entry_activate (GTK_ENTRY (w), user_data);
}

static void
on_skipafter_entry_activate (GtkEntry * entry, gpointer user_data)
{
  GuppiDelimitedPreview *gdp;
  gint line;

  gdp = GUPPI_DELIMITED_PREVIEW (user_data);
  g_return_if_fail (gdp != NULL);

  line = gdp_text_to_line_number (gdp, gtk_entry_get_text (entry));
  if (line < 0) {
    gtk_entry_set_text (entry, "");
    line = -1;
  }
  guppi_delimited_importer_set_skip_lines_after (gdp->importer, line);
}

static void
on_skipafter_entry_focus_out (GtkWidget * w, GdkEventFocus * foo,
			      gpointer user_data)
{
  on_skipafter_entry_activate (GTK_ENTRY (w), user_data);
}

static void
on_noncontig_button_toggled (GtkWidget * w, gpointer user_data)
{
  GuppiDelimitedPreview *gdp;
  gboolean state;

  gdp = GUPPI_DELIMITED_PREVIEW (user_data);
  g_return_if_fail (gdp != NULL);

  state =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON
				  (gdp->noncontig_checkbutton));
  guppi_delimited_importer_set_skip_noncontiguous (gdp->importer, state);
}

static void
on_nonalpha_button_toggled (GtkWidget * w, gpointer user_data)
{
  GuppiDelimitedPreview *gdp;
  gboolean state;

  gdp = GUPPI_DELIMITED_PREVIEW (user_data);
  g_return_if_fail (gdp != NULL);

  state =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON
				  (gdp->nonalpha_checkbutton));
  guppi_delimited_importer_set_skip_alpha (gdp->importer, state);
}

static void
on_autofilter_button_toggled (GtkWidget * w, gpointer user_data)
{
  GuppiDelimitedPreview *gdp;
  gboolean state;

  gdp = GUPPI_DELIMITED_PREVIEW (user_data);
  g_return_if_fail (gdp != NULL);

  state =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON
				  (gdp->autofilter_checkbutton));
  guppi_delimited_importer_set_autofilter (gdp->importer, state);
}

/**************************************************************************/

GtkWidget *
guppi_delimited_preview_new (GuppiDelimitedImporter * gdi)
{
  GtkWidget *hbox3;
  GtkWidget *vbox2;
  GtkWidget *frame2;
  GtkWidget *table1;
  GtkWidget *label6;
  GtkWidget *label5;
  GtkWidget *label4;
  GtkWidget *label3;
  GtkWidget *label2;
  GtkWidget *optionmenu1;
  GtkWidget *optionmenu1_menu;
  GtkWidget *glade_menuitem;
  GtkObject *spinbutton1_adj;
  GtkWidget *spinbutton1;
  GtkWidget *entry2;
  GtkWidget *entry3;
  GtkWidget *entry4;
  GtkWidget *checkbutton2;
  GtkWidget *checkbutton3;
  GtkWidget *checkbutton4;
  /*  GtkWidget *frame1;
     GtkWidget *info_label; */
  GtkWidget *stream_preview;
  GtkWidget *other_label;
  GtkWidget *other_entry;

  GuppiDelimitedPreview *gp;

  g_return_val_if_fail (gdi != NULL, NULL);

  gp =
    GUPPI_DELIMITED_PREVIEW (guppi_type_new
			     (guppi_delimited_preview_get_type ()));

  gp->importer = gdi;
  gtk_object_ref (GTK_OBJECT (gdi));

  /* Assemble the GUI */

  hbox3 = GTK_WIDGET (gp);

  vbox2 = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (vbox2);
  gtk_box_pack_start (GTK_BOX (hbox3), vbox2, FALSE, TRUE, 0);

  frame2 = gtk_frame_new (_("Settings"));
  gtk_widget_show (frame2);
  gtk_box_pack_start (GTK_BOX (vbox2), frame2, FALSE, FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (frame2), 3);

  table1 = gtk_table_new (9, 2, FALSE);
  gtk_widget_show (table1);
  gtk_container_add (GTK_CONTAINER (frame2), table1);
  gtk_table_set_row_spacings (GTK_TABLE (table1), 2);
  gtk_table_set_col_spacings (GTK_TABLE (table1), 4);

  label2 = gtk_label_new (_("Columns"));
  gtk_widget_show (label2);
  gtk_table_attach (GTK_TABLE (table1), label2, 0, 1, 0, 1,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label2), 1, 0.5);

  label3 = gtk_label_new (_("Delimiter"));
  gtk_widget_show (label3);
  gtk_table_attach (GTK_TABLE (table1), label3, 0, 1, 1, 2,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label3), 1, 0.5);

  other_label = gtk_label_new (_("Other"));
  gtk_widget_show (other_label);
  gtk_table_attach (GTK_TABLE (table1), other_label, 0, 1, 2, 3,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (other_label), 1, 0.5);

  label4 = gtk_label_new (_("Take Titles From"));
  gtk_widget_show (label4);
  gtk_table_attach (GTK_TABLE (table1), label4, 0, 1, 3, 4,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label4), 1, 0.5);

  label5 = gtk_label_new (_("Skip Lines Before"));
  gtk_widget_show (label5);
  gtk_table_attach (GTK_TABLE (table1), label5, 0, 1, 4, 5,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label5), 1, 0.5);

  label6 = gtk_label_new (_("Skip Lines After"));
  gtk_widget_show (label6);
  gtk_table_attach (GTK_TABLE (table1), label6, 0, 1, 5, 6,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label6), 1, 0.5);



  optionmenu1 = gtk_option_menu_new ();
  gtk_widget_show (optionmenu1);
  gtk_table_attach (GTK_TABLE (table1), optionmenu1, 1, 2, 1, 2,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  optionmenu1_menu = gtk_menu_new ();

  {
    gint i = 0;

    static gchar *delim_names[] = { N_("Space"),
      N_("Tab"),
      N_("Comma"),
      N_("Pipe (|)"),
      N_("Colon (:)"),
      N_("Semicolon (;)"),
      N_("Slash (/)"),
      N_("Bang (!)"),
      N_("Hyphen (-)"),
      N_("Other"),
      NULL
    };
    static gchar delim_chars[] = { ' ', '\t', ',',
      '|', ':', ';',
      '/', '!', '-', '\0'
    };

    while (delim_names[i] != NULL) {

      glade_menuitem = gtk_menu_item_new_with_label (_(delim_names[i]));

      gtk_object_set_data (GTK_OBJECT (glade_menuitem),
			   "char", &delim_chars[i]);

      gtk_widget_show (glade_menuitem);
      gtk_menu_append (GTK_MENU (optionmenu1_menu), glade_menuitem);

      gtk_signal_connect (GTK_OBJECT (glade_menuitem),
			  "activate",
			  GTK_SIGNAL_FUNC (on_delim_optionmenu_activate),
			  (gpointer) gp);

      /* We keep our own list of menu items */
      gp->menu_items = g_slist_prepend (gp->menu_items, glade_menuitem);

      ++i;
    }
  }

  /* We want Other to be last */
  gp->menu_items = g_slist_reverse (gp->menu_items);

  gtk_option_menu_set_menu (GTK_OPTION_MENU (optionmenu1), optionmenu1_menu);

  spinbutton1_adj = gtk_adjustment_new (1, 1, 100, 1, 10, 10);
  spinbutton1 = gtk_spin_button_new (GTK_ADJUSTMENT (spinbutton1_adj), 1, 0);
  gtk_widget_show (spinbutton1);
  gtk_table_attach (GTK_TABLE (table1), spinbutton1, 1, 2, 0, 1,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  other_entry = gtk_entry_new_with_max_length (1);
  gtk_widget_show (other_entry);
  gtk_table_attach (GTK_TABLE (table1), other_entry, 1, 2, 2, 3,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_widget_set_usize (other_entry, 50, -2);

  entry2 = gtk_entry_new ();
  gtk_widget_show (entry2);
  gtk_table_attach (GTK_TABLE (table1), entry2, 1, 2, 3, 4,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_widget_set_usize (entry2, 75, -2);

  entry3 = gtk_entry_new ();
  gtk_widget_show (entry3);
  gtk_table_attach (GTK_TABLE (table1), entry3, 1, 2, 4, 5,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_widget_set_usize (entry3, 75, -2);

  entry4 = gtk_entry_new ();
  gtk_widget_show (entry4);
  gtk_table_attach (GTK_TABLE (table1), entry4, 1, 2, 5, 6,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_widget_set_usize (entry4, 75, -2);

  checkbutton2 =
    gtk_check_button_new_with_label (_("Ignore non-contiguous lines."));
  gtk_widget_show (checkbutton2);
  gtk_table_attach (GTK_TABLE (table1), checkbutton2, 0, 2, 6, 7,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  checkbutton3 =
    gtk_check_button_new_with_label (_("Ignore lines containing letters."));
  gtk_widget_show (checkbutton3);
  gtk_table_attach (GTK_TABLE (table1), checkbutton3, 0, 2, 7, 8,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  checkbutton4 =
    gtk_check_button_new_with_label (_("Auto-filter suspicious lines"));
  gtk_widget_show (checkbutton4);
  gtk_table_attach (GTK_TABLE (table1), checkbutton4, 0, 2, 8, 9,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  /*
     I'll add this later...
     frame1 = gtk_frame_new (_("Status"));
     gtk_widget_show (frame1);
     gtk_box_pack_start (GTK_BOX (vbox2), frame1, FALSE, FALSE, 0);
     gtk_container_set_border_width (GTK_CONTAINER (frame1), 3);

     info_label = gtk_label_new (_("Foo Bar"));
     gtk_widget_show (info_label);
     gtk_container_add (GTK_CONTAINER (frame1), info_label);
   */

  /* Build the stream preview widget and declare all of our customizing
     callback functions. */
  stream_preview =
    guppi_stream_preview_new (guppi_importer_stream (GUPPI_IMPORTER (gdi)));
  guppi_stream_preview_set_line_skip_cb (GUPPI_STREAM_PREVIEW
					 (stream_preview),
					 gdp_skip_line_for_stream_preview,
					 gp->importer);
  guppi_stream_preview_set_extra_info_cb (GUPPI_STREAM_PREVIEW
					  (stream_preview),
					  gdp_extra_info_for_stream_preview,
					  gp->importer);
  guppi_stream_preview_set_line_color_cb (GUPPI_STREAM_PREVIEW
					  (stream_preview),
					  gdp_line_color_for_stream_preview,
					  gp->importer);

  gtk_widget_show (stream_preview);
  gtk_box_pack_start (GTK_BOX (hbox3), stream_preview, TRUE, TRUE, 0);


  /* OK, that is just about enough of that. */

  gtk_signal_connect (GTK_OBJECT (spinbutton1_adj),
		      "value_changed",
		      GTK_SIGNAL_FUNC (on_col_spinbox_adj_changed),
		      (gpointer) gp);
  gp->col_spinbutton = GTK_SPIN_BUTTON (spinbutton1);

  gp->delim_option_menu = GTK_OPTION_MENU (optionmenu1);
  gp->other_label = GTK_LABEL (other_label);

  gtk_signal_connect (GTK_OBJECT (other_entry),
		      "activate",
		      GTK_SIGNAL_FUNC (on_other_entry_activate),
		      (gpointer) gp);
  gtk_signal_connect (GTK_OBJECT (other_entry),
		      "focus_out_event",
		      GTK_SIGNAL_FUNC (on_other_entry_focus_out),
		      (gpointer) gp);
  gp->other_entry = GTK_ENTRY (other_entry);

  gtk_signal_connect (GTK_OBJECT (entry2),
		      "activate",
		      GTK_SIGNAL_FUNC (on_titleline_entry_activate),
		      (gpointer) gp);
  gtk_signal_connect (GTK_OBJECT (entry2),
		      "focus_out_event",
		      GTK_SIGNAL_FUNC (on_titleline_entry_focus_out),
		      (gpointer) gp);
  gp->titleline_entry = GTK_ENTRY (entry2);

  gtk_signal_connect (GTK_OBJECT (entry3),
		      "activate",
		      GTK_SIGNAL_FUNC (on_skipbefore_entry_activate),
		      (gpointer) gp);
  gtk_signal_connect (GTK_OBJECT (entry3),
		      "focus_out_event",
		      GTK_SIGNAL_FUNC (on_skipbefore_entry_focus_out),
		      (gpointer) gp);
  gp->skipbefore_entry = GTK_ENTRY (entry3);

  gtk_signal_connect (GTK_OBJECT (entry4),
		      "activate",
		      GTK_SIGNAL_FUNC (on_skipafter_entry_activate),
		      (gpointer) gp);
  gtk_signal_connect (GTK_OBJECT (entry4),
		      "focus_out_event",
		      GTK_SIGNAL_FUNC (on_skipafter_entry_focus_out),
		      (gpointer) gp);
  gp->skipafter_entry = GTK_ENTRY (entry4);

  gtk_signal_connect (GTK_OBJECT (checkbutton2),
		      "toggled",
		      GTK_SIGNAL_FUNC (on_noncontig_button_toggled),
		      (gpointer) gp);
  gp->noncontig_checkbutton = GTK_CHECK_BUTTON (checkbutton2);

  gtk_signal_connect (GTK_OBJECT (checkbutton3),
		      "toggled",
		      GTK_SIGNAL_FUNC (on_nonalpha_button_toggled),
		      (gpointer) gp);
  gp->nonalpha_checkbutton = GTK_CHECK_BUTTON (checkbutton3);

  gtk_signal_connect (GTK_OBJECT (checkbutton4),
		      "toggled",
		      GTK_SIGNAL_FUNC (on_autofilter_button_toggled),
		      (gpointer) gp);
  gp->autofilter_checkbutton = GTK_CHECK_BUTTON (checkbutton4);

  /* Since autofiltering is currently unimplemented, I'm going to turn
     this guy off. */
  gtk_widget_set_sensitive (checkbutton4, FALSE);

  guppi_delimited_preview_grab_importer_state (gp);

  gtk_signal_connect_object (GTK_OBJECT (gp->importer),
			     "changed",
			     GTK_SIGNAL_FUNC
			     (guppi_delimited_preview_grab_importer_state),
			     GTK_OBJECT (gp));

  gtk_signal_connect_object (GTK_OBJECT (gp->importer),
			     "changed",
			     GTK_SIGNAL_FUNC (guppi_stream_preview_refresh),
			     GTK_OBJECT (stream_preview));

  return GTK_WIDGET (gp);
}



/* $Id$ */
