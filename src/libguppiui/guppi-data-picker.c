/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-data-picker.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "guppi-data-picker.h"

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0
};

enum {
  SELECTED_DATA,
  LAST_SIGNAL
};
static guint gdp_signals[LAST_SIGNAL] = { 0 };

static void
guppi_data_picker_get_arg (GtkObject * obj, GtkArg * arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_data_picker_set_arg (GtkObject * obj, GtkArg * arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_data_picker_release_data (GuppiDataPicker * gdp)
{
  GtkWidget *menu;
  gpointer ptr;
  GList *iter;

  g_return_if_fail (gdp != NULL);

  menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (gdp));
  if (menu == NULL)
    return;

  iter = GTK_MENU_SHELL (menu)->children;

  while (iter != NULL) {
    ptr = gtk_object_get_user_data (GTK_OBJECT (iter->data));
    if (ptr != NULL) {
      gtk_object_unref (GTK_OBJECT (ptr));
    }
    iter = g_list_next (iter);
  }
}

static void
guppi_data_picker_destroy (GtkObject * obj)
{
  GuppiDataPicker *gdp = GUPPI_DATA_PICKER (obj);
  if (parent_class->destroy)
    parent_class->destroy (obj);

  gtk_object_unref (GTK_OBJECT (gdp->catalog));
  gtk_signal_disconnect_by_data (GTK_OBJECT (gdp->catalog), gdp);
  gdp->catalog = NULL;

  guppi_data_picker_release_data (gdp);

  guppi_free (gdp->label);
  gdp->label = NULL;

  gtk_option_menu_remove_menu (GTK_OPTION_MENU (gdp));
}

static void
guppi_data_picker_finalize (GtkObject * obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_data_picker_class_init (GuppiDataPickerClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;

  parent_class = gtk_type_class (GTK_TYPE_OPTION_MENU);

  gdp_signals[SELECTED_DATA] =
    gtk_signal_new ("selected_data",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GuppiDataPickerClass, selected_data),
		    gtk_marshal_NONE__POINTER,
		    GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

  gtk_object_class_add_signals (object_class, gdp_signals, LAST_SIGNAL);

  object_class->get_arg = guppi_data_picker_get_arg;
  object_class->set_arg = guppi_data_picker_set_arg;
  object_class->destroy = guppi_data_picker_destroy;
  object_class->finalize = guppi_data_picker_finalize;

}

static void
guppi_data_picker_init (GuppiDataPicker * obj)
{
  obj->catalog = NULL;
  obj->type = (GtkType) 0;
  obj->label = NULL;
}

GtkType guppi_data_picker_get_type (void)
{
  static GtkType guppi_data_picker_type = 0;
  if (!guppi_data_picker_type) {
    static const GtkTypeInfo guppi_data_picker_info = {
      "GuppiDataPicker",
      sizeof (GuppiDataPicker),
      sizeof (GuppiDataPickerClass),
      (GtkClassInitFunc) guppi_data_picker_class_init,
      (GtkObjectInitFunc) guppi_data_picker_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_data_picker_type =
      gtk_type_unique (GTK_TYPE_OPTION_MENU, &guppi_data_picker_info);
  }
  return guppi_data_picker_type;
}

static void
on_menu_item_activate (GtkMenuItem * item, gpointer gdp_ptr)
{
  gpointer data_ptr = gtk_object_get_user_data (GTK_OBJECT (item));
  gtk_signal_emit (GTK_OBJECT (gdp_ptr), gdp_signals[SELECTED_DATA],
		   data_ptr);
}

static void
guppi_data_picker_rebuild (GuppiDataPicker * gdp)
{
  GuppiData *current_pick = NULL;
  GuppiData **vec;
  GtkMenu *menu;
  GtkWidget *w;
  GtkWidget *mi;
  gchar *s;
  gint i, set_pick = -1;

  w = gtk_option_menu_get_menu (GTK_OPTION_MENU (gdp));
  if (w != NULL) {

    /* Find and remember the currently selected item */
    mi = gtk_menu_get_active (GTK_MENU (w));
    if (mi != NULL)
      current_pick = gtk_object_get_user_data (GTK_OBJECT (mi));
  }

  guppi_data_picker_release_data (gdp);

  gtk_option_menu_remove_menu (GTK_OPTION_MENU (gdp));

  vec = gdp->type ? guppi_data_catalog_get_by_type (gdp->catalog, gdp->type) :
    guppi_data_catalog_get_all (gdp->catalog);

  menu = GTK_MENU (gtk_menu_new ());

  mi = gtk_menu_item_new_with_label (gdp->label ?
				     gdp->label : _("Select Data"));
  gtk_object_set_user_data (GTK_OBJECT (mi), NULL);
  gtk_signal_connect (GTK_OBJECT (mi),
		      "activate",
		      GTK_SIGNAL_FUNC (on_menu_item_activate), gdp);
  gtk_widget_show (mi);
  gtk_menu_append (menu, mi);


  if (vec == NULL || vec[0] == NULL) {

    gtk_widget_set_sensitive (GTK_WIDGET (gdp), FALSE);

  } else {

    gtk_widget_set_sensitive (GTK_WIDGET (gdp), TRUE);

    for (i = 0; vec[i] != NULL; ++i) {
      s = guppi_strdup_printf ("%s::%s",
			   guppi_data_dataset_name (vec[i]),
			   guppi_data_label (vec[i]));
      if (vec[i] == current_pick)
	set_pick = i;
      mi = gtk_menu_item_new_with_label (s);
      guppi_free (s);
      gtk_object_set_user_data (GTK_OBJECT (mi), vec[i]);
      gtk_signal_connect (GTK_OBJECT (mi),
			  "activate",
			  GTK_SIGNAL_FUNC (on_menu_item_activate), gdp);

      gtk_object_ref (GTK_OBJECT (vec[i]));

      gtk_menu_append (menu, mi);
      gtk_widget_show (mi);
    }
  }

  gtk_option_menu_set_menu (GTK_OPTION_MENU (gdp), GTK_WIDGET (menu));

  if (set_pick > -1)
    gtk_option_menu_set_history (GTK_OPTION_MENU (gdp), set_pick);

  if (vec != NULL)
    for (i = 0; vec[i] != NULL; ++i)
      gtk_object_unref (GTK_OBJECT (vec[i]));
  guppi_free (vec);
}

void
guppi_data_picker_construct (GuppiDataPicker * gdp,
			     GuppiDataCatalog * gdc,
			     GtkType type, const gchar * label)
{
  g_return_if_fail (gdp != NULL);
  if (gdc == NULL)
    gdc = guppi_data_catalog ();

  gdp->catalog = gdc;
  gtk_object_ref (GTK_OBJECT (gdp->catalog));

  gdp->type = type;

  gdp->label = guppi_strdup (label);

  guppi_data_picker_rebuild (gdp);

  gtk_signal_connect_object (GTK_OBJECT (gdc),
			     "changed",
			     GTK_SIGNAL_FUNC (guppi_data_picker_rebuild),
			     GTK_OBJECT (gdp));
}

GtkWidget *
guppi_data_picker_new (GuppiDataCatalog * gdc, const gchar * label)
{
  return guppi_data_picker_new_by_type (gdc, (GtkType) 0, label);
}

GtkWidget *
guppi_data_picker_new_by_type (GuppiDataCatalog * gdc, GtkType type,
			       const gchar * label)
{
  GuppiDataPicker *gdp;

  gdp = GUPPI_DATA_PICKER (guppi_type_new (guppi_data_picker_get_type ()));
  guppi_data_picker_construct (gdp, gdc, type, label);

  return GTK_WIDGET (gdp);
}

GuppiData *
guppi_data_picker_get_selected_data (GuppiDataPicker * gdp)
{
  GtkWidget *w;
  gpointer udata;

  g_return_val_if_fail (gdp != NULL, NULL);

  w = gtk_option_menu_get_menu (GTK_OPTION_MENU (gdp));
  if (w == NULL)
    return NULL;

  w = gtk_menu_get_active (GTK_MENU (w));
  if (w == NULL)
    return NULL;

  udata = gtk_object_get_user_data (GTK_OBJECT (w));

  return udata ? GUPPI_DATA (udata) : NULL;
}

void
guppi_data_picker_set_selected_data (GuppiDataPicker * gdp, GuppiData * data)
{
  GtkMenu *menu;
  GList *items;
  gint pick = 0;

  g_return_if_fail (gdp != NULL);

  menu = GTK_MENU (gtk_option_menu_get_menu (GTK_OPTION_MENU (gdp)));
  g_return_if_fail (menu != NULL);

  if (data == NULL) {
    gtk_option_menu_set_history (GTK_OPTION_MENU (gdp), 0);
    return;
  }

  items = GTK_MENU_SHELL (menu)->children;
  while (items != NULL) {
    if (items->data &&
	gtk_object_get_user_data (GTK_OBJECT (items->data)) == data) {
      gtk_option_menu_set_history (GTK_OPTION_MENU (gdp), pick);
      return;
    }
    items = g_list_next (items);
    ++pick;
  }
}

GtkWidget *
guppi_data_picker_glade_custom_func (gchar * name,
				     gchar * type_restriction,
				     gchar * label, gint d2, gint d3)
{
  GtkType type = (GtkType) 0;

  if (type_restriction != NULL)
    type = gtk_type_from_name (type_restriction);

  return guppi_data_picker_new_by_type (NULL, type, label);
}



/* $Id$ */
