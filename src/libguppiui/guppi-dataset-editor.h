/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-dataset-editor.h
 *
 * Copyright (C) 1999 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_DATASET_EDITOR_H
#define _INC_GUPPI_DATASET_EDITOR_H

#include <config.h>
#include <gnome.h>
#include  "guppi-dataset.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS typedef struct _GuppiDatasetEditor GuppiDatasetEditor;
typedef struct _GuppiDatasetEditorClass GuppiDatasetEditorClass;

struct _GuppiDatasetEditor {
  GtkFrame parent;

  GuppiDataset *dataset;

  GtkEntry *name_entry;
  GtkEntry *source_entry;
  GtkEntry *reference_entry;
  GtkEntry *contact_entry;
  GtkEntry *URL_entry;
  GtkText *notes_text;
};

struct _GuppiDatasetEditorClass {
  GtkFrameClass parent_class;
};

#define GUPPI_TYPE_DATASET_EDITOR (guppi_dataset_editor_get_type())
#define GUPPI_DATASET_EDITOR(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_DATASET_EDITOR,GuppiDatasetEditor))
#define GUPPI_DATASET_EDITOR_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_DATASET_EDITOR,GuppiDatasetEditorClass))
#define GUPPI_IS_DATASET_EDITOR(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_DATASET_EDITOR))
#define GUPPI_IS_DATASET_EDITOR_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_DATASET_EDITOR))

GtkType guppi_dataset_editor_get_type (void);

GtkWidget *guppi_dataset_editor_new (GuppiDataset *);



END_GUPPI_DECLS
#endif /* _INC_GUPPI_DATASET_EDITOR_H */
/* $Id$ */
