/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-delimited-preview.h
 *
 * Copyright (C) 1999 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_DELIMITED_PREVIEW_H
#define _INC_GUPPI_DELIMITED_PREVIEW_H

#include <config.h>
#include <gnome.h>
#include  "guppi-delimited-importer.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS typedef struct _GuppiDelimitedPreview GuppiDelimitedPreview;
typedef struct _GuppiDelimitedPreviewClass GuppiDelimitedPreviewClass;

struct _GuppiDelimitedPreview {
  GtkHBox parent;

  GuppiDelimitedImporter *importer;

  GtkSpinButton *col_spinbutton;
  GtkOptionMenu *delim_option_menu;
  GtkLabel *other_label;
  GtkEntry *other_entry;
  GtkEntry *titleline_entry;
  GtkEntry *skipbefore_entry;
  GtkEntry *skipafter_entry;
  GtkCheckButton *noncontig_checkbutton;
  GtkCheckButton *nonalpha_checkbutton;
  GtkCheckButton *autofilter_checkbutton;

  GSList *menu_items;

};

struct _GuppiDelimitedPreviewClass {
  GtkHBoxClass parent_class;
};

#define GUPPI_TYPE_DELIMITED_PREVIEW (guppi_delimited_preview_get_type())
#define GUPPI_DELIMITED_PREVIEW(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_DELIMITED_PREVIEW,GuppiDelimitedPreview))
#define GUPPI_DELIMITED_PREVIEW_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_DELIMITED_PREVIEW,GuppiDelimitedPreviewClass))
#define GUPPI_IS_DELIMITED_PREVIEW(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_DELIMITED_PREVIEW))
#define GUPPI_IS_DELIMITED_PREVIEW_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_DELIMITED_PREVIEW))

GtkType guppi_delimited_preview_get_type (void);

GtkWidget *guppi_delimited_preview_new (GuppiDelimitedImporter *);



END_GUPPI_DECLS
#endif /* _INC_GUPPI_DELIMITED_PREVIEW_H */
/* $Id$ */
