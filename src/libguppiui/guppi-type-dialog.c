/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-type-dialog.c
 *
 * Copyright (C) 1999 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "guppi-type-dialog.h"
#include <guppi-data-classify.h>
#include <guppi-scalar-data.h>
#include <guppi-categorical-data.h>
#include <guppi-boolean-data.h>
#include <guppi-string-data.h>

#include "guppi-dataset-editor.h"

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0
};

static void
guppi_type_dialog_get_arg (GtkObject * obj, GtkArg * arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_type_dialog_set_arg (GtkObject * obj, GtkArg * arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_type_dialog_destroy (GtkObject * obj)
{
  if (parent_class->destroy)
    parent_class->destroy (obj);
}

static void
guppi_type_dialog_finalize (GtkObject * obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_type_dialog_class_init (GuppiTypeDialogClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;

  parent_class = gtk_type_class (GTK_TYPE_DIALOG);

  object_class->get_arg = guppi_type_dialog_get_arg;
  object_class->set_arg = guppi_type_dialog_set_arg;
  object_class->destroy = guppi_type_dialog_destroy;
  object_class->finalize = guppi_type_dialog_finalize;

}

static void
guppi_type_dialog_init (GuppiTypeDialog * obj)
{

}

GtkType guppi_type_dialog_get_type (void)
{
  static GtkType guppi_type_dialog_type = 0;
  if (!guppi_type_dialog_type) {
    static const GtkTypeInfo guppi_type_dialog_info = {
      "GuppiTypeDialog",
      sizeof (GuppiTypeDialog),
      sizeof (GuppiTypeDialogClass),
      (GtkClassInitFunc) guppi_type_dialog_class_init,
      (GtkObjectInitFunc) guppi_type_dialog_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_type_dialog_type =
      gtk_type_unique (GTK_TYPE_DIALOG, &guppi_type_dialog_info);
  }
  return guppi_type_dialog_type;
}

static void
guppi_type_dialog_set_ok_sensitivity (GuppiTypeDialog * gtd)
{
  gint i;
  g_return_if_fail (gtd != NULL);
  g_return_if_fail (gtd->changers != NULL);

  for (i = 0; gtd->changers[i] != NULL; ++i) {
    if (!gtd->changers[i]->ignore &&
	!guppi_type_changer_fully_valid (gtd->changers[i])) {
      gtk_widget_set_sensitive (GTK_WIDGET (gtd->ok_button), FALSE);
      return;
    }
  }

  gtk_widget_set_sensitive (GTK_WIDGET (gtd->ok_button), TRUE);
}

GtkWidget *
guppi_type_dialog_new (GuppiData ** datav)
{
  GtkWidget *scrolled_window;
  GtkWidget *type_changer_hbox;
  GtkWidget *dataset_editor;

  GtkWidget *ok_button;
  GtkWidget *cancel_button;

  GtkType guess;
  gint i;

  GuppiTypeDialog *gtd;

  static GtkType pot_types[5] = { 0 };

  if (pot_types[0] == 0) {
    pot_types[0] = GUPPI_TYPE_SCALAR_DATA;
    pot_types[1] = GUPPI_TYPE_BOOLEAN_DATA;
    pot_types[2] = GUPPI_TYPE_CATEGORICAL_DATA;
    pot_types[3] = GUPPI_TYPE_STRING_DATA;
    pot_types[4] = (GtkType) 0;
  }

  gtd = GUPPI_TYPE_DIALOG (guppi_type_new (guppi_type_dialog_get_type ()));

  gtk_window_set_default_size (GTK_WINDOW (gtd), 550, 400);
  gtk_window_set_title (GTK_WINDOW (gtd),
			_("Specify Dataset and Column Types"));

  /* Build typechangers to go with our data */
  for (i = 0; datav[i] != NULL; ++i);
  gtd->changers = guppi_new (GuppiTypeChanger *, i + 1);
  gtd->changers[i] = NULL;

  while (i > 0) {
    --i;
    guess = guppi_data_classify (datav[i]);
    gtd->changers[i] = GUPPI_TYPE_CHANGER (guppi_type_changer_new (datav[i],
								   pot_types,
								   guess));

    gtk_signal_connect_object (GTK_OBJECT (gtd->changers[i]),
			       "type_changed",
			       GTK_SIGNAL_FUNC
			       (guppi_type_dialog_set_ok_sensitivity),
			       GTK_OBJECT (gtd));
  }

  type_changer_hbox = gtk_hbox_new (FALSE, 4);

  /* We assume that the data vector elements all share the same dataset.
     For now, this isn't a bad assumption.  Still, it should be kept in the
     back of our minds... */
  dataset_editor = guppi_dataset_editor_new (guppi_data_dataset (datav[0]));
  gtk_box_pack_start (GTK_BOX (type_changer_hbox),
		      dataset_editor, TRUE, FALSE, 0);

  for (i = 0; datav[i] != NULL; ++i) {
    gtk_box_pack_start (GTK_BOX (type_changer_hbox),
			GTK_WIDGET (gtd->changers[i]), TRUE, FALSE, 0);
    gtk_widget_show (GTK_WIDGET (gtd->changers[i]));
  }

  scrolled_window = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_NEVER);
  gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW
					 (scrolled_window),
					 type_changer_hbox);

  gtk_container_add (GTK_CONTAINER (GTK_DIALOG (gtd)->vbox), scrolled_window);


  ok_button = gnome_stock_button (GNOME_STOCK_BUTTON_OK);
  gtd->ok_button = GTK_BUTTON (ok_button);
  cancel_button = gnome_stock_button (GNOME_STOCK_BUTTON_CANCEL);

  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (gtd)->action_area),
		      ok_button, TRUE, FALSE, 0);

  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (gtd)->action_area),
		      cancel_button, TRUE, FALSE, 0);

  guppi_type_dialog_set_ok_sensitivity (gtd);


  gtk_signal_connect_object (GTK_OBJECT (cancel_button),
			     "clicked",
			     GTK_SIGNAL_FUNC (gtk_widget_destroy),
			     GTK_OBJECT (gtd));

  return GTK_WIDGET (gtd);
}



/* $Id$ */
