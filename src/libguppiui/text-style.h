/* $Id$ */

/*
 * text-style.h
 *
 * Copyright (C) 1999 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 * and Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_TEXT_STYLE_H
#define _INC_TEXT_STYLE_H

#include <config.h>
#include <gnome.h>

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS 

GdkColor * style_active_line_color (void);
GdkColor *style_inactive_line_color (void);

GdkColor *style_active_line_number_color (void);
GdkColor *style_inactive_line_number_color (void);

GdkColor *style_title_line_color (void);
GdkColor *style_invalid_line_color (void);

GdkFont *style_line_font (void);
GdkFont *style_line_number_font (void);



END_GUPPI_DECLS

#endif /* _INC_TEXT_STYLE_H */

/* $Id$ */
