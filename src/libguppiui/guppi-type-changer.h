/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-type-changer.h
 *
 * Copyright (C) 1999 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_TYPE_CHANGER_H
#define _INC_GUPPI_TYPE_CHANGER_H

#include <config.h>
#include <gnome.h>
#include  "guppi-data.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS typedef struct _GuppiTypeChanger GuppiTypeChanger;
typedef struct _GuppiTypeChangerClass GuppiTypeChangerClass;

struct _GuppiTypeChanger {
  GtkFrame parent;

  GuppiData *data;
  GtkType *possible_types;
  GtkType new_type;

  gboolean ignore;

  GtkWidget *main_box;
  GtkText *text;
  GtkLabel *invalid_label;
  GtkEntry *invalid_entry;
  GtkLabel *info_label;

  gboolean fully_valid;
};

struct _GuppiTypeChangerClass {
  GtkFrameClass parent_class;

  void (*type_changed) (GuppiTypeChanger *);
};

#define GUPPI_TYPE_TYPE_CHANGER (guppi_type_changer_get_type())
#define GUPPI_TYPE_CHANGER(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_TYPE_CHANGER,GuppiTypeChanger))
#define GUPPI_TYPE_CHANGER_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_TYPE_CHANGER,GuppiTypeChangerClass))
#define GUPPI_IS_TYPE_CHANGER(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_TYPE_CHANGER))
#define GUPPI_IS_TYPE_CHANGER_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_TYPE_CHANGER))

GtkType guppi_type_changer_get_type (void);

GtkWidget *guppi_type_changer_new (GuppiData *, GtkType * possible_types,
				   GtkType initial_guess);

gboolean guppi_type_changer_fully_valid (GuppiTypeChanger *);
GuppiData *guppi_type_changer_get_converted_data (GuppiTypeChanger *);



END_GUPPI_DECLS
#endif /* _INC_GUPPI_TYPE_CHANGER_H */
/* $Id$ */
