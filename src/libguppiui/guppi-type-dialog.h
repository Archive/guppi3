/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-type-dialog.h
 *
 * Copyright (C) 1999 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_TYPE_DIALOG_H
#define _INC_GUPPI_TYPE_DIALOG_H

#include <config.h>
#include <gnome.h>
#include "guppi-type-changer.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS typedef struct _GuppiTypeDialog GuppiTypeDialog;
typedef struct _GuppiTypeDialogClass GuppiTypeDialogClass;

struct _GuppiTypeDialog {
  GtkDialog parent;

  GtkButton *ok_button;
  GuppiTypeChanger **changers;
};

struct _GuppiTypeDialogClass {
  GtkDialogClass parent_class;
};

#define GUPPI_TYPE_TYPE_DIALOG (guppi_type_dialog_get_type())
#define GUPPI_TYPE_DIALOG(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_TYPE_DIALOG,GuppiTypeDialog))
#define GUPPI_TYPE_DIALOG_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_TYPE_DIALOG,GuppiTypeDialogClass))
#define GUPPI_IS_TYPE_DIALOG(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_TYPE_DIALOG))
#define GUPPI_IS_TYPE_DIALOG_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_TYPE_DIALOG))

GtkType guppi_type_dialog_get_type (void);

GtkWidget *guppi_type_dialog_new (GuppiData ** datav);



END_GUPPI_DECLS
#endif /* _INC_GUPPI_TYPE_DIALOG_H */
/* $Id$ */
