// $Id$

#ifndef _INC_SPECFNS_H
#define _INC_SPECFNS_H

/*
  
  Maybe it is because I'm too young, and missed out on programming in
  the age when Fortran was king, but function names like "gdtr" and
  "chdtri" really leave me cold.  I'd rather just type some more
  characters, which (to me) is well worth not having to remember all
  of those damn abbreviated names.

  (Until I go to my grave, I'm going to remember that character 191 on
  the TRS-80 gave you the solid white rectangle.  And wasn't it "call
  -151" that got you into the Apple II+'s low-level monitor thing?)

 */

#include "specfns_protos.h"
#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS

#define binomial_cdf(k, n, x) bdtr(k,n,x)
#define neg_binomial_cdf(k, n, x) nbdtr(k,n,x)
// Choice is good.
#define chisq_cdf(df, x) chdtr(df,x)
#define chi_square_cdf(df, x) chdtr(df,x)
#define inv_chisq_cdf(df, x) chdtri(df,1-(x))
#define inv_chi_square_cdf(df, x) chdtri(df,1-(x))
#define F_cdf(df1, df2, x) fdtr(df1,df2,x)
#define inv_F_cdf(df1, df2, p) fdtri(df1,df2,1-(p));}
#define log_gamma(x) lgam(x)
#define incomplete_gamma(a, x) igam(a,x)
#define inv_incomplete_gamma(a, p) igami(a,p)
#define gamma_cdf(a, b, x) gdtr(a,b,x)
// Default assumption is mean=0, sdev=1
#define normal_cdf(x) ndtr(x)
#define inv_normal_cdf(p) ndtri(p)
#define poisson_cdf(k, x) pdtr(k,x)
#define inv_poisson_cdf(k, p) pdtri(k,p)
// You can use it either way...
#define students_cdf(df, t) stdtr(df,t)
#define t_cdf(df, t) stdtr(df,t)
#define inv_students_cdf(df, p) stdtri(df,p)
#define inv_t_cdf(df, p) stdtri(df,p)

END_GUPPI_DECLS

#endif // _INC_SPECFNS_H

// $Id$
