/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-equation.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_EQUATION_H
#define _INC_GUPPI_EQUATION_H

/* #include <gtk/gtk.h> */
#include <glib.h>
#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS 

typedef enum _GuppiEquationTermType GuppiEquationTermType;

enum _GuppiEquationTermType {
  EQN_VARIABLE,
  EQN_CONSTANT,
  EQN_SUM,
  EQN_DIFFERENCE,
  EQN_PRODUCT,
  EQN_QUOTIENT,
  EQN_SIN,
  EQN_COS,
  EQN_TAN,
  EQN_EXP,
  EQN_LOG,
  EQN_LOG10,
  EQN_POW,
  LAST_EQN
};

/***********************************************************************/

typedef struct _GuppiEquation GuppiEquation;

struct _GuppiEquation {
  GuppiEquationTermType type;

  guint32 code;
  double constant;

  GuppiEquation *left;
  GuppiEquation *right;
};

#define guppi_equation_is_leaf(x) \
((x) != NULL && (x)->left == NULL && (x)->right == NULL)

GuppiEquation *guppi_equation_new (GuppiEquationTermType);
void guppi_equation_free (GuppiEquation *);

gboolean guppi_equation_equal (GuppiEquation *, GuppiEquation *);
GuppiEquation *guppi_equation_copy (GuppiEquation *);

GuppiEquation *guppi_equation_constant (double value);

GuppiEquation *guppi_equation_variable_coded (guint code);
GuppiEquation *guppi_equation_variable (gchar c);
GuppiEquation *guppi_equation_variable_subscripted (gchar c, gint16 i);

gchar guppi_equation_variable_symbol (GuppiEquation *);
gboolean guppi_equation_variable_has_index (GuppiEquation *);
gint16 guppi_equation_variable_index (GuppiEquation *);


GuppiEquation *guppi_equation_unary (GuppiEquationTermType, GuppiEquation *);
GuppiEquation *guppi_equation_binary (GuppiEquationTermType,
				      GuppiEquation * left,
				      GuppiEquation * right);

#define guppi_equation_sum(f,g) guppi_equation_binary(EQN_SUM,(f),(g))
#define guppi_equation_product(f,g) guppi_equation_binary(EQN_PRODUCT,(f),(g))
#define guppi_equation_difference(f,g) guppi_equation_binary(EQN_DIFFERENCE,(f),(g))
#define guppi_equation_quotient(f,g) guppi_equation_binary(EQN_QUOTIENT,(f),(g))
#define guppi_equation_neg(f) guppi_equation_difference(guppi_equation_constant(0), (f))

#define guppi_equation_pow(f,g) guppi_equation_binary(EQN_POW,(f),(g))
#define guppi_equation_cpow(f,x) guppi_equation_pow((f),guppi_equation_constant(x))

void guppi_equation_dump (GuppiEquation *);

gboolean guppi_equation_eval_simple (GuppiEquation *,
				     guint code,
				     double input, double *output);

GuppiEquation *guppi_equation_derivative (GuppiEquation *, guint);

void guppi_equation_simplify (GuppiEquation *);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_EQUATION_H */

/* $Id$ */
