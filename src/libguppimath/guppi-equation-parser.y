%{
#include <config.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "guppi-equation.h"

extern FILE* yyin;
extern char* yytext;
extern int yylex();
void yyerror(char* s);

#define YYSTYPE GuppiEquation*

extern gchar last_variable;

%}

%token FINISHED
%token OPENPAREN CLOSEPAREN VARIABLE CONSTANT
%token CONSTANT_PI CONSTANT_E
%token SUM DIFFERENCE PRODUCT QUOTIENT
%token SIN COS TAN
%token EXP LOG LOG10 POW
%token SQ SQRT
%token UNARY_MINUS LOWER_THAN_UNARY_MINUS
%token SUBSCRIPT


%left DIFFERENCE SUM
%left PRODUCT QUOTIENT
%right POW
%left SIN COS TAN EXP LOG LOG10 SQ SQRT
%right LOWER_THAN_UNARY_MINUS
%nonassoc SUBSCRIPT
%nonassoc UNARY_MINUS
%nonassoc OPENPAREN CLOSEPAREN

%%

statement: expression FINISHED
{ return $1; } ;

expression: CONSTANT %prec LOWER_THAN_UNARY_MINUS
{
  $$ = guppi_equation_constant(atof(yytext));
}
| DIFFERENCE CONSTANT %prec UNARY_MINUS
{
  $$ = guppi_equation_constant(-atof(yytext));
}
| CONSTANT_PI
{
  $$ = guppi_equation_constant(M_PI);
}
| CONSTANT_E
{
  $$ = guppi_equation_constant(M_E);
}
| VARIABLE SUBSCRIPT
{
  $$ = guppi_equation_variable_subscripted(last_variable,
                                           atoi(yytext+1));
}
| VARIABLE
{
  $$ = guppi_equation_variable(last_variable);
}
| OPENPAREN expression CLOSEPAREN 
{
  $$ = $2;
}
| expression SUM expression 
{
  $$ = guppi_equation_binary(EQN_SUM, $1, $3);
}
| expression DIFFERENCE expression
{
  $$ = guppi_equation_binary(EQN_DIFFERENCE, $1, $3);
}
| expression PRODUCT expression
{
  $$ = guppi_equation_binary(EQN_PRODUCT, $1, $3);
}
| expression QUOTIENT expression
{
  $$ = guppi_equation_binary(EQN_QUOTIENT, $1, $3);
}
| expression POW expression
{
  $$ = guppi_equation_binary(EQN_POW, $1, $3);
}
| SIN expression
{
  $$ = guppi_equation_unary(EQN_SIN, $2);
}
| COS expression
{
  $$ = guppi_equation_unary(EQN_COS, $2);
}
| TAN expression
{
  $$ = guppi_equation_unary(EQN_TAN, $2);
}
| EXP expression
{
  $$ = guppi_equation_unary(EQN_EXP, $2);
}
| LOG expression
{ 
  $$ = guppi_equation_unary(EQN_LOG, $2);
}
| LOG10 expression
{
  $$ = guppi_equation_unary(EQN_LOG10, $2);
}
| SQ expression
{
  $$ = guppi_equation_cpow($2, 2);
}
| SQRT expression
{
  $$ = guppi_equation_cpow($2, 0.5);
}
;


%%

void yyerror(char* s)
{
  g_error(s);
}
