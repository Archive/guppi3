/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-equation.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <math.h>
#include <stdio.h>
#include <guppi-memory.h>
#include "guppi-equation.h"

GuppiEquation *
guppi_equation_new (GuppiEquationTermType type)
{
  GuppiEquation *t = guppi_new0 (GuppiEquation, 1);
  t->type = type;
  return t;
}

void
guppi_equation_free (GuppiEquation * term)
{
  if (term) {
    if (term->left)
      guppi_equation_free (term->left);
    if (term->right)
      guppi_equation_free (term->right);
    guppi_free (term);
  }
}

gboolean guppi_equation_equal (GuppiEquation * a, GuppiEquation * b)
{
  if (a == NULL || b == NULL)
    return a == b;

  if (a->type != b->type)
    return FALSE;

  if (a->type == EQN_VARIABLE)
    return a->code == b->code;

  if (a->type == EQN_CONSTANT)
    return a->constant == b->constant;

  if (guppi_equation_equal (a->left, b->left) &&
      guppi_equation_equal (a->right, b->right))
    return TRUE;

  if ((a->type == EQN_SUM || a->type == EQN_PRODUCT) &&
      guppi_equation_equal (a->left, b->right) &&
      guppi_equation_equal (a->right, b->left))
    return TRUE;

  return FALSE;
}

GuppiEquation *
guppi_equation_copy (GuppiEquation * term)
{
  GuppiEquation *t;

  if (term == NULL)
    return NULL;

  t = guppi_new0 (GuppiEquation, 1);
  t->type = term->type;
  t->code = term->code;
  t->constant = term->constant;
  t->left = term->left ? guppi_equation_copy (term->left) : NULL;
  t->right = term->right ? guppi_equation_copy (term->right) : NULL;

  return t;
}

GuppiEquation *
guppi_equation_constant (double x)
{
  GuppiEquation *t = guppi_new0 (GuppiEquation, 1);
  t->type = EQN_CONSTANT;
  t->constant = x;
  return t;
}

/***************************************************************************/

#define INDEX_FLAG (1<<31)
#define SYMBOL_MASK 0xff
#define INDEX_OFFSET 8
#define INDEX_MASK (0xffff<<INDEX_OFFSET)

GuppiEquation *
guppi_equation_variable_coded (guint code)
{
  GuppiEquation *t = guppi_new0 (GuppiEquation, 1);
  t->type = EQN_VARIABLE;
  t->code = code;
  return t;
}

GuppiEquation *
guppi_equation_variable (gchar c)
{
  return guppi_equation_variable_coded ((guint) c);
}

GuppiEquation *
guppi_equation_variable_subscripted (gchar c, gint16 i)
{
  return guppi_equation_variable_coded (((guchar) c) |
					(i << INDEX_OFFSET) | INDEX_FLAG);
}

gchar guppi_equation_variable_symbol (GuppiEquation * term)
{
  g_return_val_if_fail (term != NULL, '\0');
  return term->type !=
    EQN_VARIABLE ? '\0' : (guchar) (term->code & SYMBOL_MASK);
}

gboolean guppi_equation_variable_has_index (GuppiEquation * term)
{
  g_return_val_if_fail (term != NULL, FALSE);
  g_return_val_if_fail (term->type == EQN_VARIABLE, FALSE);

  return term->code & INDEX_FLAG;
}

gint16 guppi_equation_variable_index (GuppiEquation * term)
{
  g_return_val_if_fail (term != NULL, 0);
  g_return_val_if_fail (term->type == EQN_VARIABLE, 0);

  return (gint16) ((term->code & INDEX_MASK) >> INDEX_OFFSET);
}

/***************************************************************************/

GuppiEquation *
guppi_equation_unary (GuppiEquationTermType type, GuppiEquation * arg)
{
  GuppiEquation *t;
  g_return_val_if_fail (arg != NULL, NULL);
  t = guppi_new0 (GuppiEquation, 1);
  t->type = type;
  t->right = arg;		/* Convention: single args go on the right. */
  return t;
}

GuppiEquation *
guppi_equation_binary (GuppiEquationTermType type,
		       GuppiEquation * arg1, GuppiEquation * arg2)
{
  GuppiEquation *t;
  g_return_val_if_fail (arg1 != NULL, NULL);
  g_return_val_if_fail (arg2 != NULL, NULL);
  t = guppi_new0 (GuppiEquation, 1);
  t->type = type;
  t->left = arg1;
  t->right = arg2;
  return t;
}

static const gchar *
term_type_name (GuppiEquationTermType type)
{
  switch (type) {
  case EQN_SUM:
    return "+";
  case EQN_DIFFERENCE:
    return "-";
  case EQN_PRODUCT:
    return "*";
  case EQN_QUOTIENT:
    return "/";
  case EQN_SIN:
    return "sin";
  case EQN_COS:
    return "cos";
  case EQN_TAN:
    return "tan";
  case EQN_EXP:
    return "exp";
  case EQN_LOG:
    return "log";
  case EQN_LOG10:
    return "log10";
  case EQN_POW:
    return "pow";
  default:
    return "?";
  }
}

void
guppi_equation_dump (GuppiEquation * term)
{
  if (term == NULL) {
    printf ("NULL");
    return;
  }

  if (term->type == EQN_VARIABLE)

    if (guppi_equation_variable_has_index (term))
      printf ("%c_%d",
	      guppi_equation_variable_symbol (term),
	      guppi_equation_variable_index (term));
    else
      printf ("%c", guppi_equation_variable_symbol (term));

  else if (term->type == EQN_CONSTANT)
    printf ("%g", term->constant);
  else {
    printf ("(%s ", term_type_name (term->type));
    if (term->left) {
      guppi_equation_dump (term->left);
      printf (" ");
    }
    if (term->right)
      guppi_equation_dump (term->right);
    printf (")");
  }
}

/* We assume that our GuppiEquation is well-formed: if not, there
   will be trouble... */
gboolean
guppi_equation_eval_simple (GuppiEquation * term,
			    guint code, double input, double *output)
{
  gboolean have_left, have_right;
  double left_val = 0, right_val = 0;

  g_return_val_if_fail (output != NULL, FALSE);
  g_return_val_if_fail (term != NULL, FALSE);

  have_left = term->left != NULL;
  have_right = term->right != NULL;

  if (have_left &&
      !guppi_equation_eval_simple (term->left, code, input, &left_val))
      return FALSE;

  if (have_right &&
      !guppi_equation_eval_simple (term->right, code, input, &right_val))
      return FALSE;

  switch (term->type) {

  case EQN_VARIABLE:
    if (code == term->code) {
      *output = input;
      return TRUE;
    } else
      return FALSE;

  case EQN_CONSTANT:
    *output = term->constant;
    return TRUE;

  case EQN_SUM:
    *output = left_val + right_val;
    return TRUE;

  case EQN_DIFFERENCE:
    *output = left_val - right_val;
    return TRUE;

  case EQN_PRODUCT:
    *output = left_val * right_val;
    return TRUE;

  case EQN_QUOTIENT:
    if (fabs (right_val) < 1e-8)
      return FALSE;		/* A hack... */
    *output = left_val / right_val;
    return TRUE;

  case EQN_SIN:
    *output = sin (right_val);
    return TRUE;

  case EQN_COS:
    *output = cos (right_val);
    return TRUE;

  case EQN_TAN:
    *output = tan (right_val);
    return TRUE;

  case EQN_EXP:
    *output = exp (right_val);
    return TRUE;

  case EQN_LOG:
    if (right_val <= 0)
      return FALSE;
    *output = log (right_val);
    return TRUE;

  case EQN_LOG10:
    if (right_val <= 0)
      return FALSE;
    *output = log10 (right_val);
    return TRUE;

  case EQN_POW:
    *output = pow (left_val, right_val);
    return TRUE;

  default:
    g_error ("Unknown type.");
    return FALSE;
  }
}

GuppiEquation *
guppi_equation_derivative (GuppiEquation * term, guint var_code)
{
  double x;
  GuppiEquation *f;
  GuppiEquation *g;
  GuppiEquation *Df;
  GuppiEquation *Dg;
  GuppiEquation *ff;
  GuppiEquation *gg;

  g_return_val_if_fail (term != NULL, NULL);

  switch (term->type) {
  case EQN_CONSTANT:
    return guppi_equation_constant (0);

  case EQN_VARIABLE:
    return guppi_equation_constant (term->code == var_code ? 1 : 0);

  case EQN_SUM:
    Df = guppi_equation_derivative (term->left, var_code);
    Dg = guppi_equation_derivative (term->right, var_code);
    return guppi_equation_sum (Df, Dg);

  case EQN_DIFFERENCE:
    Df = guppi_equation_derivative (term->left, var_code);
    Dg = guppi_equation_derivative (term->right, var_code);
    return guppi_equation_difference (Df, Dg);

  case EQN_PRODUCT:
    f = guppi_equation_copy (term->left);
    g = guppi_equation_copy (term->right);
    Df = guppi_equation_derivative (f, var_code);
    Dg = guppi_equation_derivative (g, var_code);

    return guppi_equation_sum (guppi_equation_product (f, Dg),
			       guppi_equation_product (Df, g));

  case EQN_QUOTIENT:
    f = guppi_equation_copy (term->left);
    g = guppi_equation_copy (term->right);
    Df = guppi_equation_derivative (f, var_code);
    Dg = guppi_equation_derivative (g, var_code);

    ff = guppi_equation_difference (guppi_equation_product (Df, g),
				    guppi_equation_product (f, Dg));
    gg = guppi_equation_cpow (guppi_equation_copy (g), 2);

    return guppi_equation_quotient (ff, gg);

  case EQN_SIN:
    f = guppi_equation_copy (term->right);
    Df = guppi_equation_derivative (f, var_code);

    return guppi_equation_product (guppi_equation_unary (EQN_COS, f), Df);

  case EQN_COS:
    f = guppi_equation_copy (term->right);
    Df = guppi_equation_derivative (f, var_code);
    ff = guppi_equation_product (guppi_equation_unary (EQN_SIN, f), Df);

    return guppi_equation_neg (ff);

  case EQN_TAN:
    f = guppi_equation_copy (term->right);
    Df = guppi_equation_derivative (f, var_code);
    g = guppi_equation_unary (EQN_COS, f);

    return guppi_equation_quotient (guppi_equation_constant (1),
				    guppi_equation_cpow (g, 2));

  case EQN_EXP:
    f = guppi_equation_copy (term->right);
    Df = guppi_equation_derivative (f, var_code);

    return guppi_equation_product (guppi_equation_unary (EQN_EXP, f), Df);

  case EQN_LOG:
    f = guppi_equation_copy (term->right);
    Df = guppi_equation_derivative (f, var_code);

    return guppi_equation_quotient (Df, f);

  case EQN_POW:

    if (term->right->type == EQN_CONSTANT) {	/* The f^n case */
      x = term->right->constant;
      ff = guppi_equation_cpow (guppi_equation_copy (term->left), x - 1);
      gg = guppi_equation_constant (x);
      Df = guppi_equation_derivative (term->left, var_code);

      return guppi_equation_product (guppi_equation_product (gg, ff), Df);

    } else {			/* The general case */
      f = guppi_equation_copy (term->left);
      g = guppi_equation_copy (term->right);
      Df = guppi_equation_derivative (f, var_code);
      Dg = guppi_equation_derivative (g, var_code);

      ff = guppi_equation_product (g, guppi_equation_quotient (Df, f));

      f = guppi_equation_unary (EQN_LOG, guppi_equation_copy (term->left));
      gg = guppi_equation_product (Dg, f);

      return guppi_equation_product (guppi_equation_copy (term),
				     guppi_equation_sum (ff, gg));
    }

  default:
    g_error ("Unhandled case.");
    return NULL;
  }
}

/**************************************************************************/

#define is_zero(x) ((fabs(x)<1e-8))
#define is_one(x) is_zero((x)-1)
#define term_is_zero(t) \
(((t) != NULL) && ((t)->type == EQN_CONSTANT) && is_zero((t)->constant))
#define term_is_one(t) \
(((t) != NULL) && ((t)->type == EQN_CONSTANT) && is_one((t)->constant))

static void
make_constant (GuppiEquation * t, double x)
{
  guppi_equation_free (t->left);
  guppi_equation_free (t->right);
  t->type = EQN_CONSTANT;
  t->constant = x;
  t->left = t->right = NULL;
}

static void
replace (GuppiEquation * dest, GuppiEquation * src)
{
  dest->type = src->type;
  dest->code = src->code;
  dest->constant = src->constant;
  dest->left = src->left;
  dest->right = src->right;
}

void
guppi_equation_simplify (GuppiEquation * term)
{
  GuppiEquation *tmp;
  double x;

  g_return_if_fail (term != NULL);

  if (term->left)
    guppi_equation_simplify (term->left);
  if (term->right)
    guppi_equation_simplify (term->right);

  /* fold constants */
  if (guppi_equation_eval_simple (term, 0, 0, &x)) {
    make_constant (term, x);
    return;
  }

  /* try to move all constants to the left */
  if (term->left != NULL && term->right != NULL &&
      (term->type == EQN_SUM || term->type == EQN_PRODUCT) &&
      term->left->type != EQN_CONSTANT && term->right->type == EQN_CONSTANT) {
    tmp = term->left;
    term->left = term->right;
    term->right = tmp;
  }

  /* handle 0 * foo, foo * 0, and 0 / foo */
  if ((term->type == EQN_PRODUCT &&
       (term_is_zero (term->left) || term_is_zero (term->right)))
      || (term->type == EQN_QUOTIENT &&
	  term_is_zero (term->left) && !term_is_zero (term->right))) {
    make_constant (term, 0);
    return;
  }

  /* handle 1 * foo and foo * 1 and 0 + foo and foo + 0 */
  if ((term->type == EQN_PRODUCT && term_is_one (term->left)) ||
      (term->type == EQN_SUM && term_is_zero (term->left))) {
    guppi_equation_free (term->left);
    replace (term, tmp = term->right);
    guppi_free (tmp);
  }
  if ((term->type == EQN_PRODUCT && term_is_one (term->right)) ||
      (term->type == EQN_SUM && term_is_zero (term->right))) {
    guppi_equation_free (term->right);
    replace (term, tmp = term->left);
    guppi_free (tmp);
  }

  /* handle foo - 0  && foo / 1 */
  if ((term->type == EQN_DIFFERENCE && term_is_zero (term->right)) ||
      (term->type == EQN_QUOTIENT && term_is_one (term->right))) {
    guppi_equation_free (term->right);
    replace (term, tmp = term->left);
    guppi_free (tmp);
  }

  /* handle foo - foo */
  if (term->type == EQN_DIFFERENCE &&
      guppi_equation_equal (term->left, term->right)) {
    make_constant (term, 0);
    return;
  }

  /* handle foo / foo (but not 0/0) */
  if (term->type == EQN_QUOTIENT &&
      guppi_equation_equal (term->left, term->right) &&
      !term_is_zero (term->left)) {
    make_constant (term, 1);
    return;
  }

  /* In the case of foo + foo, change to 2 * foo and try further
     optimizations */
  if (term->type == EQN_SUM && guppi_equation_equal (term->left, term->right)) {
    guppi_equation_free (term->left);
    term->left = guppi_equation_constant (2);
    term->type = EQN_PRODUCT;
    guppi_equation_simplify (term);
  }

  /* Try to fold (* x (* y foo)) => (* x*y foo) and
     (+ x (+ y foo)) => (+ x+y foo) */
  if ((term->type == EQN_PRODUCT || term->type == EQN_SUM) &&
      term->left->type == EQN_CONSTANT &&
      term->right->type == term->type &&
      term->right->left->type == EQN_CONSTANT) {
    if (term->type == EQN_PRODUCT)
      term->left->constant *= term->right->left->constant;
    else
      term->left->constant += term->right->left->constant;
    guppi_equation_free (term->right->left);
    tmp = term->right;
    term->right = term->right->right;
    guppi_free (term->right);
  }

  /* Try to fold (+ (* c1 foo) (* c2 foo)) => (* c1+c2 foo),
     and try some more. */
  if (term->type == EQN_SUM &&
      term->left->type == EQN_PRODUCT &&
      term->right->type == EQN_PRODUCT &&
      term->left->left->type == EQN_CONSTANT &&
      term->right->left->type == EQN_CONSTANT &&
      guppi_equation_equal (term->left->right, term->right->right)) {
    term->type = EQN_PRODUCT;
    x = term->left->left->constant;
    guppi_equation_free (term->left);
    term->left = guppi_equation_constant (x + term->right->left->constant);
    guppi_equation_free (term->right->left);
    tmp = term->right;
    term->right = term->right->right;
    guppi_free (tmp);
    guppi_equation_simplify (term);
  }

  /* I should also try to fold (+ foo (* c foo)) and (+ (* c foo) foo)
     => (* c+1 foo) */

  /* Fold (pow x 0) => 1, (pow x 1) => x */
  if (term->type == EQN_POW && term->right->type == EQN_CONSTANT) {
    if (is_zero (term->right->constant)) {
      guppi_equation_free (term->left);
      guppi_equation_free (term->right);
      replace (term, guppi_equation_constant (1));
    } else if (is_one (term->right->constant)) {
      guppi_equation_free (term->right);
      tmp = term->left;
      replace (term, term->left);
      guppi_free (tmp);
    }
  }

}


/* $Id$ */
