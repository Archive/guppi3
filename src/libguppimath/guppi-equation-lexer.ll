%{
#include <config.h>
#include <stdio.h>
#include <unistd.h>
#include <guppi-equation.h>
#include <guppi-equation-parser.h>

#define YYDEBUG 1

#if YYDEBUG != 0
extern int yydebug;
#endif

gchar last_variable;

%}

%%

[ \t\n]+   ; /* gobble whitespace */

"("		{ return OPENPAREN; }
")"		{ return CLOSEPAREN; }
"+"		{ return SUM; }
"-"		{ return DIFFERENCE; }
"*"		{ return PRODUCT; }
"/"		{ return QUOTIENT; }
"**"		{ return POW; }
"^"		{ return POW; }
sin|Sin|SIN	{ return SIN; }
cos|Cos|COS	{ return COS; }
tan|Tan|TAN	{ return TAN; }
log10|Log10|LOG10 { return LOG10; }
log|Log|LOG|ln|Ln|LN	{ return LOG; }
exp|Exp|EXP	{ return EXP; }
sq|Sq|SQ	{ return SQ; }
sqrt|Sqrt|SQRT	{ return SQRT; }

\_-?[0-9]+|\_\(-?[0-9]+\)	{ return SUBSCRIPT; }
[0-9]+ | 
[0-9]+"."[0-9]* |
\.[0-9]+	{ return CONSTANT; }
pi|Pi|PI        { return CONSTANT_PI; }
e|E             { return CONSTANT_E; }

[abcxyzABCXYZ]	{
  last_variable = *yytext;
  return VARIABLE; }

<<EOF>> { return FINISHED; }

%%

GuppiEquation*
guppi_equation_parse(const gchar* s)
{
  GuppiEquation* t;
  YY_BUFFER_STATE ybs = yy_scan_string(s);
  t = (GuppiEquation*)yyparse();
  yy_delete_buffer(ybs);
  return t;
}

int
yywrap()
{
  return 1;
}
