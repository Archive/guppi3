/* $Id$ */

/*
 * parse-test.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.

 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <gnome.h>
#include <unistd.h>
#include <signal.h>

#include "guppi-equation.h"

GuppiEquation *guppi_equation_parse (const gchar *);

gint time_limit;
gint count = 0;
gboolean running;


static void
handler (int i)
{
  g_message ("%d evals in %d seconds (%g eps)", count, time_limit,
	     count / (double) time_limit);
  running = FALSE;
}

static void
set_alarm (int tl)
{
  time_limit = tl;
  count = 0;
  running = TRUE;
  signal (SIGALRM, handler);
  alarm (tl);
}

main (int argc, char *argv[])
{
  GuppiEquation *v = guppi_equation_variable ('x');
  GuppiEquation *t;
  GuppiEquation *Dt;
  gint i;
  double x;

  for (i = 1; i < argc; ++i) {
    printf ("\"%s\" => ", argv[i]);

    t = guppi_equation_parse (argv[i]);
    guppi_equation_simplify (t);

    guppi_equation_dump (t);
    printf (" => ");

    if (guppi_equation_eval_simple (t, 0, 0, &x)) {
      printf ("%g\n", x);
    } else {
      printf ("???\n");
    }

    set_alarm (1);
    while (running) {
      guppi_equation_eval_simple (t, 0, 0, &x);
      ++count;
    }

    if (t) {
      Dt = guppi_equation_derivative (t, v->code);
      printf ("Deriv: ");
      guppi_equation_dump (Dt);
      printf ("\n");
      guppi_equation_simplify (Dt);
      printf ("  Opt: ");
      guppi_equation_dump (Dt);
      printf ("\n\n");
    }
  }
}




/* $Id$ */
