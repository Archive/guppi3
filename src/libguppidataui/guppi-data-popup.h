/* This is -*- C -*- */
/* vim: set sw=2: */

/*
 * guppi-data-popup.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_DATA_POPUP_H
#define _INC_GUPPI_DATA_POPUP_H

/* #include <gnome.h> */
#include <gtk/gtkbutton.h>

#include  "guppi-defs.h"
#include  "guppi-data.h"
#include  "guppi-data-tree.h"

BEGIN_GUPPI_DECLS 

typedef struct _GuppiDataPopup GuppiDataPopup;
typedef struct _GuppiDataPopupClass GuppiDataPopupClass;

struct _GuppiDataPopup {
  GtkButton parent;

  gboolean allow_none;
  gchar *none_label;

  GtkType allowed_type;

  gboolean (*type_fn)(GtkType, gpointer);
  gpointer user_data;

  GuppiDataTree *tree;
};

struct _GuppiDataPopupClass {
  GtkButtonClass parent_class;

  void (*selected_data) (GuppiDataPopup * popup, GuppiData * data);
};

#define GUPPI_TYPE_DATA_POPUP (guppi_data_popup_get_type ())
#define GUPPI_DATA_POPUP(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_DATA_POPUP,GuppiDataPopup))
#define GUPPI_DATA_POPUP0(obj) ((obj) ? (GUPPI_DATA_POPUP(obj)) : NULL)
#define GUPPI_DATA_POPUP_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_DATA_POPUP,GuppiDataPopupClass))
#define GUPPI_IS_DATA_POPUP(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_DATA_POPUP))
#define GUPPI_IS_DATA_POPUP0(obj) (((obj) == NULL) || (GUPPI_IS_DATA_POPUP(obj)))
#define GUPPI_IS_DATA_POPUP_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_DATA_POPUP))

GtkType guppi_data_popup_get_type (void);

void guppi_data_popup_construct (GuppiDataPopup * popup);

GtkWidget *guppi_data_popup_new (void);
GtkWidget *guppi_data_popup_new_with_label (const gchar * label);

void guppi_data_popup_disallow_none (GuppiDataPopup * popup);
void guppi_data_popup_allow_none (GuppiDataPopup * popup,
				  const gchar * none_label);

void guppi_data_popup_set_allowed_type (GuppiDataPopup * popup, GtkType type);
void guppi_data_popup_set_allowed_type_fn (GuppiDataPopup * popup,
					   gboolean (*fn)(GtkType, gpointer),
					   gpointer);
void guppi_data_popup_set_allow_all_types (GuppiDataPopup * popup);


gboolean guppi_data_popup_allowed_type (GuppiDataPopup * popup, GtkType type);
gboolean guppi_data_popup_allowed_data (GuppiDataPopup * popup,
					GuppiData * data);




END_GUPPI_DECLS

#endif /* _INC_GUPPI_DATA_POPUP_H */

/* $Id$ */
