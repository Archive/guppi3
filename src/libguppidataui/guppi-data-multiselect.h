/* $Id$ */
/* This is -*- C -*- */
/* vim: set sw=2: */

/*
 * guppi-data-multiselect.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_DATA_MULTISELECT_H
#define _INC_GUPPI_DATA_MULTISELECT_H

/* #include <gnome.h> */
#include <gtk/gtkframe.h>
#include <gtk/gtkclist.h>

#include  "guppi-defs.h"
#include  "guppi-data.h"
#include  "guppi-seq-data.h"

BEGIN_GUPPI_DECLS

typedef struct _GuppiDataMultiselect GuppiDataMultiselect;
typedef struct _GuppiDataMultiselectClass GuppiDataMultiselectClass;

struct _GuppiDataMultiselect {
  GtkFrame parent;

  gboolean uniq;
  GtkType allowed_type;
  gboolean (*type_fn)(GtkType, gpointer);
  gpointer user_data;

  GuppiSeqData *data_seq;

  /* widgets */
  GtkCList *clist;
  GtkWidget *add_button;
  GtkWidget *remove_button;
};

struct _GuppiDataMultiselectClass {
  GtkFrameClass parent_class;

  void (*changed) (GuppiDataMultiselect *);
};

#define GUPPI_TYPE_DATA_MULTISELECT (guppi_data_multiselect_get_type ())
#define GUPPI_DATA_MULTISELECT(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_DATA_MULTISELECT,GuppiDataMultiselect))
#define GUPPI_DATA_MULTISELECT0(obj) ((obj) ? (GUPPI_DATA_MULTISELECT(obj)) : NULL)
#define GUPPI_DATA_MULTISELECT_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_DATA_MULTISELECT,GuppiDataMultiselectClass))
#define GUPPI_IS_DATA_MULTISELECT(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_DATA_MULTISELECT))
#define GUPPI_IS_DATA_MULTISELECT0(obj) (((obj) == NULL) || (GUPPI_IS_DATA_MULTISELECT(obj)))
#define GUPPI_IS_DATA_MULTISELECT_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_DATA_MULTISELECT))

typedef void (*guppi_multiselect_fn) (GuppiData *, gpointer);


GtkType guppi_data_multiselect_get_type (void);

void guppi_data_multiselect_construct (GuppiDataMultiselect * multi);

GtkWidget *guppi_data_multiselect_new (void);
GtkWidget *guppi_data_multiselect_new_by_type (GtkType allowed_type);
GtkWidget *guppi_data_multiselect_new_by_type_fn (gboolean (*fn)(GtkType,
								 gpointer),
						  gpointer user_data);

void guppi_data_multiselect_set_allowed_type (GuppiDataMultiselect * multi,
					      GtkType);
void guppi_data_multiselect_set_allowed_type_fn (GuppiDataMultiselect * multi,
						 gboolean (*fn)(GtkType,
								gpointer),
						 gpointer user_data);
void guppi_data_multiselect_allow_all_types (GuppiDataMultiselect * multi);

void guppi_data_multiselect_connect (GuppiDataMultiselect * multi,
				     GuppiSeqData * seq_data);

gboolean guppi_data_multiselect_allowed_type (GuppiDataMultiselect * multi,
					      GtkType type);
gboolean guppi_data_multiselect_allowed_data (GuppiDataMultiselect * multi,
					      GuppiData * data);

gsize guppi_data_multiselect_size (GuppiDataMultiselect * multi);

GuppiData *guppi_data_multiselect_get (GuppiDataMultiselect * multi, gint i);
gboolean guppi_data_multiselect_contains (GuppiDataMultiselect * multi,
					  GuppiData * data);
void guppi_data_multiselect_foreach (GuppiDataMultiselect * multi,
				     guppi_multiselect_fn fn, gpointer ptr);

gboolean guppi_data_multiselect_unique (GuppiDataMultiselect * multi);
void guppi_data_multiselect_set_unique (GuppiDataMultiselect * multi,
					gboolean uniq_flag);

void guppi_data_multiselect_append (GuppiDataMultiselect * multi,
				    GuppiData * data);
void guppi_data_multiselect_prepend (GuppiDataMultiselect * multi,
				     GuppiData * data);
void guppi_data_multiselect_insert (GuppiDataMultiselect * multi,
				    gint i, GuppiData * data);
void guppi_data_multiselect_remove (GuppiDataMultiselect * multi, gint i);

GtkWidget *guppi_data_multiselect_glade_fn (gchar * name,
					    gchar * type_restriction,
					    gchar * dummy_str,
					    gint dummy_int1, gint dummy_int2);


END_GUPPI_DECLS

#endif /* _INC_GUPPI_DATA_MULTISELECT_H */

/* $Id$ */
