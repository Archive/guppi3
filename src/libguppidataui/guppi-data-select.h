/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-data-select.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_DATA_SELECT_H
#define _INC_GUPPI_DATA_SELECT_H

/* #include <gnome.h> */
#include <gtk/gtkeventbox.h>
#include <gtk/gtklabel.h>

#include  "guppi-data.h"
#include  "guppi-data-tree.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS 

typedef struct _GuppiDataSelect GuppiDataSelect;
typedef struct _GuppiDataSelectClass GuppiDataSelectClass;

struct _GuppiDataSelect {
  GtkEventBox parent;

  GtkLabel *label;
  gchar *empty_label;
  GuppiData *data;

  GtkWidget *popup;

  GtkType allowed_type;
  gboolean (*type_fn)(GtkType, gpointer);
  gpointer user_data;
};

struct _GuppiDataSelectClass {
  GtkEventBoxClass parent_class;

  void (*selected_data) (GuppiDataSelect *, GuppiData *);
};

#define GUPPI_TYPE_DATA_SELECT (guppi_data_select_get_type())
#define GUPPI_DATA_SELECT(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_DATA_SELECT,GuppiDataSelect))
#define GUPPI_DATA_SELECT0(obj) ((obj) ? (GUPPI_DATA_SELECT(obj)) : NULL)
#define GUPPI_DATA_SELECT_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_DATA_SELECT,GuppiDataSelectClass))
#define GUPPI_IS_DATA_SELECT(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_DATA_SELECT))
#define GUPPI_IS_DATA_SELECT0(obj) (((obj) == NULL) || (GUPPI_IS_DATA_SELECT(obj)))
#define GUPPI_IS_DATA_SELECT_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_DATA_SELECT))

GtkType guppi_data_select_get_type (void);

void guppi_data_select_construct (GuppiDataSelect *);

GtkWidget *guppi_data_select_new (void);
GtkWidget *guppi_data_select_new_by_type (GtkType type);
GtkWidget *guppi_data_select_new_by_type_fn (gboolean (*fn)(GtkType, gpointer),
					     gpointer user_data);

void guppi_data_select_set_empty_label (GuppiDataSelect *, const gchar *);

void guppi_data_select_set_allowed_type (GuppiDataSelect *, GtkType);
void guppi_data_select_set_allowed_type_fn (GuppiDataSelect *,
					    gboolean (*fn)(GtkType, gpointer),
					    gpointer user_data);
void guppi_data_select_set_allow_all_types (GuppiDataSelect *);

gboolean guppi_data_select_allowed_type (GuppiDataSelect *, GtkType);
gboolean guppi_data_select_allowed_data (GuppiDataSelect *, GuppiData *);

GuppiData *guppi_data_select_get_selected_data (GuppiDataSelect *);
void guppi_data_select_set_selected_data (GuppiDataSelect *, GuppiData *);

GtkWidget *guppi_data_select_glade_custom_func (gchar * name,
						gchar * type_restriction,
						gchar * label,
						gint dummy_int1,
						gint dummy_int2);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_DATA_SELECT_H */

/* $Id$ */
