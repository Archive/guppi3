/* This is -*- C -*- */
/* vim: set sw=2: */

/*
 * guppi-data-multiselect.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>

#include <gtk/gtkselection.h>
#include <gtk/gtkdnd.h>
#include <gtk/gtkbox.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtkhbbox.h>

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>

#include <libgnomeui/gnome-stock.h>

#include <guppi-convenient.h>
#include "guppi-data-popup.h"
#include "guppi-data-multiselect.h"

static void emit_changed (GuppiDataMultiselect * multi);
static void add_data_to_clist_iter_fn (GuppiData * data, gpointer user_data);
static void push_state_to_clist (GuppiDataMultiselect * multi);
static void drag_data_received_cb (GtkWidget * w,
				   GdkDragContext * context,
				   gint x, gint y,
				   GtkSelectionData * selection_data,
				   guint info, guint time,
				   gpointer user_data);

static void remove_by_click_cb (GuppiDataMultiselect * multi);
static void add_click_cb (GuppiDataPopup * pop, GuppiData * data,
			  gpointer user_data);
static void tree_change_cb (GuppiDataMultiselect * multi);





static GtkObjectClass *parent_class = NULL;

enum {
  CHANGED,
  LAST_SIGNAL
};

guint multi_signals[LAST_SIGNAL] = { 0 };

enum {
  ARG_0
};

static void
guppi_data_multiselect_get_arg (GtkObject * obj, GtkArg * arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_data_multiselect_set_arg (GtkObject * obj, GtkArg * arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_data_multiselect_destroy (GtkObject * obj)
{
  if (parent_class->destroy)
    parent_class->destroy (obj);
}

static void
guppi_data_multiselect_finalize (GtkObject * obj)
{
  GuppiDataMultiselect *multi = GUPPI_DATA_MULTISELECT (obj);
  GuppiDataPopup *popup = GUPPI_DATA_POPUP (multi->add_button);

  guppi_unref0 (multi->data_seq);

  gtk_signal_disconnect_by_data (GTK_OBJECT (popup->tree), multi);

  gtk_signal_disconnect_by_data (GTK_OBJECT (multi->data_seq), multi);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/***************************************************************************/

static void
changed (GuppiDataMultiselect * multi)
{
  push_state_to_clist (multi);
}


static void
guppi_data_multiselect_class_init (GuppiDataMultiselectClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;

  parent_class = gtk_type_class (GTK_TYPE_FRAME);

  object_class->get_arg = guppi_data_multiselect_get_arg;
  object_class->set_arg = guppi_data_multiselect_set_arg;
  object_class->destroy = guppi_data_multiselect_destroy;
  object_class->finalize = guppi_data_multiselect_finalize;

  klass->changed = changed;

  multi_signals[CHANGED] =
    gtk_signal_new ("changed",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GuppiDataMultiselectClass, changed),
		    gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 0);

  gtk_object_class_add_signals (object_class, multi_signals, LAST_SIGNAL);

}

static void
guppi_data_multiselect_init (GuppiDataMultiselect * obj)
{
  GuppiSeqData *seq_data = GUPPI_SEQ_DATA (guppi_seq_data_new ());

  guppi_data_multiselect_connect (obj, seq_data);
}

GtkType guppi_data_multiselect_get_type (void)
{
  static GtkType guppi_data_multiselect_type = 0;
  if (!guppi_data_multiselect_type) {
    static const GtkTypeInfo guppi_data_multiselect_info = {
      "GuppiDataMultiselect",
      sizeof (GuppiDataMultiselect),
      sizeof (GuppiDataMultiselectClass),
      (GtkClassInitFunc) guppi_data_multiselect_class_init,
      (GtkObjectInitFunc) guppi_data_multiselect_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_data_multiselect_type = gtk_type_unique (GTK_TYPE_FRAME,
						   &guppi_data_multiselect_info);
  }
  return guppi_data_multiselect_type;
}

void
guppi_data_multiselect_construct (GuppiDataMultiselect * multi)
{
  static GtkTargetEntry drag_types[] = {
    {"guppi/data", 0, 0}
  };
  static gint n_drag_types = sizeof (drag_types) / sizeof (drag_types[0]);

  GtkBox *box;
  GtkBox *button_box;
  GtkWidget *add_label;
  GtkWidget *add_pixmap;
  GtkWidget *add_box;

  g_return_if_fail (multi && GUPPI_DATA_MULTISELECT (multi));

  /* Initialize the CList */
  multi->clist = GTK_CLIST (gtk_clist_new (1));
  gtk_clist_set_selection_mode (multi->clist, GTK_SELECTION_BROWSE);

  gtk_drag_dest_set (GTK_WIDGET (multi->clist),
		     GTK_DEST_DEFAULT_ALL,
		     drag_types, n_drag_types, GDK_ACTION_COPY);
  gtk_signal_connect (GTK_OBJECT (multi->clist), "drag_data_received",
		      GTK_SIGNAL_FUNC (drag_data_received_cb), multi);

  /* Set up "Add" button. */

  add_label = gtk_label_new (_("Add"));
  add_pixmap = gnome_stock_new_with_icon (GNOME_STOCK_MENU_FORWARD);
  add_box = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (add_box), add_label, TRUE, TRUE, 2);
  gtk_box_pack_end (GTK_BOX (add_box), add_pixmap, TRUE, TRUE, 2);
  gtk_widget_show_all (add_box);

  multi->add_button = guppi_data_popup_new ();
  gtk_container_add (GTK_CONTAINER (multi->add_button), add_box);

  gtk_signal_connect (GTK_OBJECT (multi->add_button),
		      "selected_data",
		      GTK_SIGNAL_FUNC (add_click_cb), GTK_OBJECT (multi));


  /* Set up "Remove" button. */

  multi->remove_button = gtk_button_new_with_label (_("Remove"));
  gtk_signal_connect_object (GTK_OBJECT (multi->remove_button),
			     "clicked",
			     GTK_SIGNAL_FUNC (remove_by_click_cb),
			     GTK_OBJECT (multi));

  /* Build boxes and put stuff in them. */

  button_box = GTK_BOX (gtk_hbutton_box_new ());
  gtk_box_pack_start (button_box, multi->add_button, FALSE, FALSE, 0);
  gtk_box_pack_end (button_box, multi->remove_button, FALSE, FALSE, 0);

  box = GTK_BOX (gtk_vbox_new (FALSE, 2));
  gtk_box_pack_start (box, GTK_WIDGET (multi->clist), TRUE, TRUE, 2);
  gtk_box_pack_end (box, GTK_WIDGET (button_box), FALSE, FALSE, 2);


  gtk_container_add (GTK_CONTAINER (multi), GTK_WIDGET (box));
  gtk_widget_show (GTK_WIDGET (multi->clist));


  /* We need to watch tree change signals to know when to change the
     "Add" button from insensitive to sensitive. */
  gtk_signal_connect_object (GTK_OBJECT
			     (GUPPI_DATA_POPUP (multi->add_button)->tree),
			     "changed", GTK_SIGNAL_FUNC (tree_change_cb),
			     GTK_OBJECT (multi));

  push_state_to_clist (multi);
}

GtkWidget *
guppi_data_multiselect_new (void)
{
  GuppiDataMultiselect *multi;

  multi = GUPPI_DATA_MULTISELECT (guppi_type_new (GUPPI_TYPE_DATA_MULTISELECT));

  guppi_data_multiselect_construct (multi);
  guppi_data_multiselect_allow_all_types (multi);

  return GTK_WIDGET (multi);
}

GtkWidget *
guppi_data_multiselect_new_by_type (GtkType allowed_type)
{
  GuppiDataMultiselect *multi;

  multi = GUPPI_DATA_MULTISELECT (guppi_data_multiselect_new ());
  guppi_data_multiselect_set_allowed_type (multi, allowed_type);

  return GTK_WIDGET (multi);
}

GtkWidget *
guppi_data_multiselect_new_by_type_fn (gboolean (*fn)(GtkType, gpointer),
				       gpointer user_data)
{
  GuppiDataMultiselect *multi;

  multi = GUPPI_DATA_MULTISELECT (guppi_data_multiselect_new ());
  guppi_data_multiselect_set_allowed_type_fn (multi, fn, user_data);

  return GTK_WIDGET (multi);
}

void
guppi_data_multiselect_set_allowed_type (GuppiDataMultiselect * multi,
					 GtkType type)
{
  g_return_if_fail (multi && GUPPI_IS_DATA_MULTISELECT (multi));

  multi->allowed_type = type;
  multi->type_fn = NULL;
  multi->user_data = NULL;
}

void
guppi_data_multiselect_set_allowed_type_fn (GuppiDataMultiselect * multi,
					    gboolean (*fn)(GtkType, gpointer),
					    gpointer user_data)
{
  g_return_if_fail (multi && GUPPI_IS_DATA_MULTISELECT (multi));
  g_return_if_fail (fn);

  multi->allowed_type = 0;
  multi->type_fn = fn;
  multi->user_data = user_data;
}

void
guppi_data_multiselect_allow_all_types (GuppiDataMultiselect * multi)
{
  g_return_if_fail (multi && GUPPI_IS_DATA_MULTISELECT (multi));

  guppi_data_multiselect_set_allowed_type (multi, (GtkType)0);
}
					

void
guppi_data_multiselect_connect (GuppiDataMultiselect * multi,
				GuppiSeqData * seq_data)
{
  g_return_if_fail (multi && GUPPI_IS_DATA_MULTISELECT (multi));
  g_return_if_fail (seq_data && GUPPI_IS_SEQ_DATA (seq_data));

  if (multi->data_seq != seq_data) {
    guppi_refcounting_assign (multi->data_seq, seq_data);
    gtk_signal_connect_object (GTK_OBJECT (multi->data_seq),
			       "changed",
			       GTK_SIGNAL_FUNC (push_state_to_clist),
			       GTK_OBJECT (multi));
    emit_changed (multi);
  }
}

gboolean
guppi_data_multiselect_allowed_type (GuppiDataMultiselect * multi,
				     GtkType type)
{
  g_return_val_if_fail (multi && GUPPI_IS_DATA_MULTISELECT (multi), FALSE);

  if (multi->type_fn)
    return multi->type_fn (type, multi->user_data);
  else if (multi->allowed_type)
    return gtk_type_is_a (type, multi->allowed_type);
  else
    return TRUE;
}

gboolean
guppi_data_multiselect_allowed_data (GuppiDataMultiselect * multi,
				     GuppiData * data)
{
  g_return_val_if_fail (multi && GUPPI_IS_DATA_MULTISELECT (multi), FALSE);
  g_return_val_if_fail (data == NULL || GUPPI_IS_DATA (data), FALSE);

  return data == NULL
    || guppi_data_multiselect_allowed_type (multi, GTK_OBJECT_TYPE (data));
}

gsize guppi_data_multiselect_size (GuppiDataMultiselect * multi)
{
  g_return_val_if_fail (multi && GUPPI_IS_DATA_MULTISELECT (multi), 0);

  return guppi_seq_size (GUPPI_SEQ (multi->data_seq));
}

GuppiData *
guppi_data_multiselect_get (GuppiDataMultiselect * multi, gint i)
{
  g_return_val_if_fail (multi && GUPPI_IS_DATA_MULTISELECT (multi), NULL);
  g_return_val_if_fail (i >= 0, NULL);

  i += guppi_seq_min_index (GUPPI_SEQ (multi->data_seq));

  if (!guppi_seq_in_bounds (GUPPI_SEQ (multi->data_seq), i))
    return NULL;

  return guppi_seq_data_get (multi->data_seq, i);
}

gboolean
guppi_data_multiselect_contains (GuppiDataMultiselect * multi,
				 GuppiData * data)
{
  gint i, N;

  g_return_val_if_fail (multi && GUPPI_IS_DATA_MULTISELECT (multi), FALSE);
  g_return_val_if_fail (data && GUPPI_IS_DATA (data), FALSE);

  N = guppi_data_multiselect_size (multi);
  for (i = 0; i < N; ++i)
    if (guppi_data_multiselect_get (multi, i) == data)
      return TRUE;

  return FALSE;
}

void
guppi_data_multiselect_foreach (GuppiDataMultiselect * multi,
				guppi_multiselect_fn fn, gpointer ptr)
{
  gint i, N;

  g_return_if_fail (multi && GUPPI_IS_DATA_MULTISELECT (multi));
  g_return_if_fail (fn != NULL);

  N = guppi_data_multiselect_size (multi);
  for (i = 0; i < N; ++i)
    fn (guppi_data_multiselect_get (multi, i), ptr);
}

gboolean guppi_data_multiselect_unique (GuppiDataMultiselect * multi)
{
  g_return_val_if_fail (multi && GUPPI_IS_DATA_MULTISELECT (multi), FALSE);

  return multi->uniq;
}

void
guppi_data_multiselect_set_unique (GuppiDataMultiselect * multi,
				   gboolean uniq_flag)
{
  gboolean changed_flag = FALSE;

  g_return_if_fail (multi && GUPPI_IS_DATA_MULTISELECT (multi));

  if (multi->uniq != uniq_flag) {

    multi->uniq = uniq_flag;

#if 0
    /* If we just set the unique flag to be true, go through our data list
       and remove and duplicate elements that may be in there. */
    if (multi->uniq) {
      for (i = multi->data_list; i != NULL; i = g_list_next (i)) {
	j = g_list_next (i);
	while (j != NULL) {
	  GList *k = g_list_next (j);

	  if (j->data == i->data) {
	    guppi_unref0 (j->data);
	    multi->data_list = g_list_remove_link (multi->data_list, j);
	    g_list_free_1 (j);
	    changed_flag = TRUE;
	  }

	  j = k;
	}
      }
    }
#endif

    if (changed_flag)
      emit_changed (multi);
  }
}

void
guppi_data_multiselect_append (GuppiDataMultiselect * multi, GuppiData * data)
{
  g_return_if_fail (multi && GUPPI_IS_DATA_MULTISELECT (multi));
  g_return_if_fail (data && GUPPI_IS_DATA (data));

  if (multi->uniq && guppi_data_multiselect_contains (multi, data))
    return;

  guppi_seq_data_append (multi->data_seq, data);

  emit_changed (multi);
}

void
guppi_data_multiselect_prepend (GuppiDataMultiselect * multi,
				GuppiData * data)
{
  g_return_if_fail (multi && GUPPI_IS_DATA_MULTISELECT (multi));
  g_return_if_fail (data && GUPPI_IS_DATA (data));

  if (multi->uniq && guppi_data_multiselect_contains (multi, data))
    return;

  guppi_seq_data_prepend (multi->data_seq, data);

  emit_changed (multi);
}

void
guppi_data_multiselect_insert (GuppiDataMultiselect * multi,
			       gint i, GuppiData * data)
{
  g_return_if_fail (multi && GUPPI_IS_DATA_MULTISELECT (multi));
  g_return_if_fail (data && GUPPI_IS_DATA (data));

  if (multi->uniq && guppi_data_multiselect_contains (multi, data))
    return;

  i += guppi_seq_min_index (GUPPI_SEQ (multi->data_seq));

  guppi_seq_data_insert (multi->data_seq, i, data);

  emit_changed (multi);
}

void
guppi_data_multiselect_remove (GuppiDataMultiselect * multi, gint i)
{
  g_return_if_fail (multi && GUPPI_IS_DATA_MULTISELECT (multi));
  g_return_if_fail (i >= 0);

  i += guppi_seq_min_index (GUPPI_SEQ (multi->data_seq));

  guppi_seq_delete (GUPPI_SEQ (multi->data_seq), i);

  emit_changed (multi);
}

GtkWidget *
guppi_data_multiselect_glade_fn (gchar * name,
				 gchar * type_restriction,
				 gchar * dummy, gint d2, gint d3)
{
  GtkType type = (GtkType) 0;

  if (type_restriction != NULL) {
    type = gtk_type_from_name (type_restriction);
    if (strcmp (type_restriction, gtk_type_name (type))) {
      g_warning ("Something bad happened: %s / %d / %s",
		 type_restriction, type, gtk_type_name (type));
    }
  }

  return guppi_data_multiselect_new_by_type (type);
}

/**************************************************************************/

static void
emit_changed (GuppiDataMultiselect * multi)
{
  gtk_signal_emit (GTK_OBJECT (multi), multi_signals[CHANGED]);
}

static void
add_data_to_clist_iter_fn (GuppiData * data, gpointer user_data)
{
  GtkCList *clist = GTK_CLIST (user_data);
  gchar *label = (gchar *) guppi_data_get_label (data);

  gtk_clist_append (clist, &label);
}

static void
push_state_to_clist (GuppiDataMultiselect * multi)
{
  gint size;
  gint sel_row = 0;

  size = guppi_data_multiselect_size (multi);

  if (multi->clist->selection)
    sel_row = GPOINTER_TO_INT (multi->clist->selection->data);

  gtk_clist_freeze (multi->clist);

  gtk_clist_clear (multi->clist);

  guppi_data_multiselect_foreach (multi,
				  add_data_to_clist_iter_fn, multi->clist);

  gtk_clist_thaw (multi->clist);

  gtk_clist_select_row (multi->clist, sel_row, 0);

  gtk_clist_set_column_title (multi->clist,
			      0,
			      size ? _("Selected Data") :
			      _("No Data Selected"));
  gtk_clist_column_titles_show (multi->clist);

  gtk_widget_set_sensitive (multi->add_button,
			    guppi_data_tree_size (GUPPI_DATA_POPUP
						  (multi->add_button)->tree) >
			    0);
  gtk_widget_set_sensitive (multi->remove_button, size != 0);
}

static void
drag_data_received_cb (GtkWidget * w,
		       GdkDragContext * context,
		       gint x, gint y,
		       GtkSelectionData * selection_data,
		       guint info, guint time, gpointer user_data)
{
  GuppiDataMultiselect *multi;
  GuppiData *data;

  g_return_if_fail (user_data && GUPPI_IS_DATA_MULTISELECT (user_data));
  g_return_if_fail (selection_data != NULL);

  multi = GUPPI_DATA_MULTISELECT (user_data);

  data = *(GuppiData **) selection_data->data;

  guppi_data_multiselect_append (multi, data);
}

static void
remove_by_click_cb (GuppiDataMultiselect * multi)
{
  gint row;

  g_return_if_fail (multi && GUPPI_IS_DATA_MULTISELECT (multi));

  if (multi->clist->selection == NULL)
    return;

  row = GPOINTER_TO_INT (multi->clist->selection->data);

  guppi_data_multiselect_remove (multi, row);
}

static void
add_click_cb (GuppiDataPopup * pop, GuppiData * data, gpointer user_data)
{
  GuppiDataMultiselect *multi;

  multi = GUPPI_DATA_MULTISELECT (user_data);

  guppi_data_multiselect_append (multi, data);
}

static void
tree_change_cb (GuppiDataMultiselect * multi)
{
  push_state_to_clist (multi);
}


/* $Id$ */
