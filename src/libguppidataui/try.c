/* $Id$ */

/*
 * try.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 * and Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <guppi-seq-scalar.h>
#include <guppi-struct.h>
#include <guppi-data-init.h>
#include <guppi-data-tree-view.h>

gboolean foo (gpointer s)
{
  GuppiStruct *str = GUPPI_STRUCT (s);
  GuppiData *x;
  gchar buffer[32];
  g_snprintf (buffer, 32, "slot %d", random ());

  guppi_struct_add_free_field (str, buffer);
  x = guppi_seq_scalar_new ();
  guppi_data_set_label (x, buffer);
  guppi_struct_set (str, buffer, x);

  return TRUE;
}

int
main (gint argc, char *argv[])
{
  GuppiData *x;
  GuppiData *y;
  GuppiData *s;
  int i;

  gtk_init (&argc, &argv);

  x = guppi_seq_scalar_new ();
  guppi_data_set_label (x, "foo #1");
  guppi_data_tree_add (guppi_data_tree_main (), x);

  y = guppi_seq_scalar_new ();
  guppi_data_set_label (y, "foo sub #1");
  guppi_data_tree_add_below (guppi_data_tree_main (), x, y);

  x = guppi_seq_scalar_new ();
  guppi_data_set_label (x, "foo sub sub #1");
  guppi_data_tree_add_below (guppi_data_tree_main (), y, x);


  x = guppi_seq_scalar_new ();
  guppi_data_set_label (x, "foo #2");
  guppi_data_tree_add (guppi_data_tree_main (), x);

  x = guppi_seq_scalar_new ();
  guppi_data_set_label (x, "foo #3");
  guppi_data_tree_add (guppi_data_tree_main (), x);
  for (i = 0; i < 1000; ++i)
    guppi_seq_scalar_append (GUPPI_SEQ_SCALAR (x), i / 3.14159);

  y = guppi_seq_scalar_new ();
  guppi_data_set_label (y, "foo sub #3");
  guppi_data_tree_add_below (guppi_data_tree_main (), x, y);


  x = guppi_seq_scalar_new ();
  guppi_data_set_label (x, "foo #4");
  guppi_data_tree_add (guppi_data_tree_main (), x);

  x = guppi_seq_scalar_new ();
  guppi_data_set_label (x, "foo #5");
  guppi_data_tree_add (guppi_data_tree_main (), x);

  s = guppi_struct_new ();
  guppi_struct_add_free_field (GUPPI_STRUCT (s), "a");
  guppi_struct_add_free_field (GUPPI_STRUCT (s), "b");
  guppi_struct_add_free_field (GUPPI_STRUCT (s), "c");

  x = guppi_seq_scalar_new ();
  guppi_data_set_label (x, "elt a");
  guppi_struct_set (GUPPI_STRUCT (s), "a", x);

  x = guppi_seq_scalar_new ();
  guppi_data_set_label (x, "elt b");
  guppi_struct_set (GUPPI_STRUCT (s), "b", x);

  x = guppi_seq_scalar_new ();
  guppi_data_set_label (x, "elt c");
  guppi_struct_set (GUPPI_STRUCT (s), "c", x);


  guppi_data_tree_add (guppi_data_tree_main (), s);

  guppi_data_tree_view_popup (guppi_data_tree_main ());

  gtk_main ();
}




/* $Id$ */
