/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-regression2d.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <guppi-memory.h>
#include "guppi-regression2d.h"

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0,
  ARG_X_DATA,
  ARG_Y_DATA,
  ARG_MASK,
  ARG_WEIGHTS
};

enum {
  CHANGED,
  LAST_SIGNAL
};

static guint guppi_regression2d_signals[LAST_SIGNAL] = { 0 };

typedef struct _GuppiRegression2DPrivate GuppiRegression2DPrivate;
struct _GuppiRegression2DPrivate {
  GuppiSeqScalar  *x_data;
  GuppiSeqScalar  *y_data;
  GuppiSeqBoolean *mask;
  GuppiSeqScalar  *weights;

  guint x_data_handle;
  guint y_data_handle;
  guint mask_handle;
  guint weights_handle;

  gint freeze_count;
  guint changed_elements;
};

#define priv(x) ((GuppiRegression2DPrivate*)(GUPPI_REGRESSION2D((x))->opaque_internals))

static void
guppi_regression2d_get_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  GuppiRegression2D *reg = GUPPI_REGRESSION2D (obj);

  switch (arg_id) {

  case ARG_X_DATA:
    GTK_VALUE_POINTER (*arg) = guppi_regression2d_x_data (reg);
    break;

  case ARG_Y_DATA:
    GTK_VALUE_POINTER (*arg) = guppi_regression2d_y_data (reg);
    break;

  case ARG_MASK:
    GTK_VALUE_POINTER (*arg) = guppi_regression2d_mask (reg);
    break;

  case ARG_WEIGHTS:
    GTK_VALUE_POINTER (*arg) = guppi_regression2d_weights (reg);
    break;

  default:
    break;
  };
}

static void
guppi_regression2d_set_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  GuppiRegression2D *reg = GUPPI_REGRESSION2D (obj);
  gpointer ptr = GTK_VALUE_POINTER (*arg);

  switch (arg_id) {

  case ARG_X_DATA:
    guppi_regression2d_set_x_data (reg, GUPPI_SEQ_SCALAR0 (ptr));
    break;

  case ARG_Y_DATA:
    guppi_regression2d_set_y_data (reg, GUPPI_SEQ_SCALAR0 (ptr));
    break;
    
  case ARG_MASK:
    guppi_regression2d_set_mask (reg, GUPPI_SEQ_BOOLEAN0 (ptr));
    break;

  case ARG_WEIGHTS:
    guppi_regression2d_set_weights (reg, GUPPI_SEQ_SCALAR0 (ptr));
    break;

  default:
    break;
  };
}

static void
guppi_regression2d_finalize (GtkObject *obj)
{
  GuppiRegression2D *x = GUPPI_REGRESSION2D(obj);
  GuppiRegression2DPrivate *p = priv (x);

  if (p->freeze_count > 0) 
    g_warning ("finalizing frozen GuppiRegression2D (count=%u)", p->freeze_count);

  if (p->x_data_handle)
    gtk_signal_disconnect (GTK_OBJECT (p->x_data), p->x_data_handle);

  if (p->y_data_handle)
    gtk_signal_disconnect (GTK_OBJECT (p->y_data), p->y_data_handle);

  if (p->mask_handle)
    gtk_signal_disconnect (GTK_OBJECT (p->mask), p->mask_handle);

  if (p->weights_handle)
    gtk_signal_disconnect (GTK_OBJECT (p->weights), p->weights_handle);

  guppi_unref0 (p->x_data);
  guppi_unref0 (p->y_data);
  guppi_unref0 (p->mask);
  guppi_unref0 (p->weights);

  g_free (x->opaque_internals);
  x->opaque_internals = NULL;

  guppi_finalized (obj);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

#define add_arg(str, t, symb) \
gtk_object_add_arg_type ("GuppiRegression2D::" str, t, GTK_ARG_READWRITE, symb)

static void
guppi_regression2d_class_init (GuppiRegression2DClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;

  parent_class = gtk_type_class (GTK_TYPE_OBJECT);

  object_class->get_arg = guppi_regression2d_get_arg;
  object_class->set_arg = guppi_regression2d_set_arg;
  object_class->finalize = guppi_regression2d_finalize;

  klass->allow_mask = TRUE;
  klass->allow_weights = TRUE;

  guppi_regression2d_signals[CHANGED] =
    gtk_signal_new ("changed",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GuppiRegression2DClass, changed),
                    gtk_marshal_NONE__UINT, GTK_TYPE_NONE, 1, GTK_TYPE_UINT);

  gtk_object_class_add_signals (object_class, guppi_regression2d_signals,
                                LAST_SIGNAL);

  add_arg ("x_data",  GTK_TYPE_POINTER, ARG_X_DATA);
  add_arg ("y_data",  GTK_TYPE_POINTER, ARG_Y_DATA);
  add_arg ("mask",    GTK_TYPE_POINTER, ARG_MASK);
  add_arg ("weights", GTK_TYPE_POINTER, ARG_X_DATA);
}

static void
guppi_regression2d_init (GuppiRegression2D *obj)
{
  GuppiRegression2DPrivate *p = g_new0 (GuppiRegression2DPrivate, 1);

  obj->opaque_internals = p;
}

GtkType
guppi_regression2d_get_type (void)
{
  static GtkType guppi_regression2d_type = 0;
  if (!guppi_regression2d_type) {
    static const GtkTypeInfo guppi_regression2d_info = {
      "GuppiRegression2D",
      sizeof (GuppiRegression2D),
      sizeof (GuppiRegression2DClass),
      (GtkClassInitFunc)guppi_regression2d_class_init,
      (GtkObjectInitFunc)guppi_regression2d_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    guppi_regression2d_type = gtk_type_unique (GTK_TYPE_OBJECT, &guppi_regression2d_info);
  }
  return guppi_regression2d_type;
}

static void
guppi_regression2d_changed_inner (GuppiRegression2D *reg, GuppiRegression2DElement el)
{
  GuppiRegression2DPrivate *p;
  g_return_if_fail (GUPPI_IS_REGRESSION2D (reg));
  p = priv (reg);
  
  p->changed_elements |= el;
  
  if (p->freeze_count == 0 && p->changed_elements) {
    gtk_signal_emit (GTK_OBJECT (reg), guppi_regression2d_signals[CHANGED], p->changed_elements);
    p->changed_elements = 0;
  }
}

void
guppi_regression2d_changed (GuppiRegression2D *reg)
{
  g_return_if_fail (GUPPI_IS_REGRESSION2D (reg));
  guppi_regression2d_changed_inner (reg, GUPPI_REGRESSION2D_OTHER);
}

gboolean
guppi_regression2d_allow_mask (GuppiRegression2D *reg)
{
  g_return_val_if_fail (GUPPI_IS_REGRESSION2D (reg), FALSE);
  return GUPPI_REGRESSION2D_CLASS (GTK_OBJECT (reg)->klass)->allow_mask;
}

gboolean
guppi_regression2d_allow_weights (GuppiRegression2D *reg)
{
  g_return_val_if_fail (GUPPI_IS_REGRESSION2D (reg), FALSE);
  return GUPPI_REGRESSION2D_CLASS (GTK_OBJECT (reg)->klass)->allow_weights;
}

GuppiSeqScalar *
guppi_regression2d_x_data (GuppiRegression2D *reg)
{
  g_return_val_if_fail (GUPPI_IS_REGRESSION2D (reg), NULL);
  return priv (reg)->x_data;
}

GuppiSeqScalar *
guppi_regression2d_y_data (GuppiRegression2D *reg)
{
  g_return_val_if_fail (GUPPI_IS_REGRESSION2D (reg), NULL);
  return priv (reg)->y_data;
}

GuppiSeqBoolean *
guppi_regression2d_mask (GuppiRegression2D *reg)
{
  g_return_val_if_fail (GUPPI_IS_REGRESSION2D (reg), NULL);
  return priv (reg)->mask;
}

GuppiSeqScalar *
guppi_regression2d_weights (GuppiRegression2D *reg)
{
  g_return_val_if_fail (GUPPI_IS_REGRESSION2D (reg), NULL);
  return priv (reg)->weights;
}

static void
x_data_changed_inner_proxy (GuppiData *d, GuppiRegression2D *reg)
{
  guppi_regression2d_changed_inner (reg, GUPPI_REGRESSION2D_X_DATA);
}

static void
y_data_changed_inner_proxy (GuppiData *d, GuppiRegression2D *reg)
{
  guppi_regression2d_changed_inner (reg, GUPPI_REGRESSION2D_Y_DATA);
}

static void
mask_changed_inner_proxy (GuppiData *d, GuppiRegression2D *reg)
{
  guppi_regression2d_changed_inner (reg, GUPPI_REGRESSION2D_MASK);
}

static void
weights_changed_inner_proxy (GuppiData *d, GuppiRegression2D *reg)
{
  guppi_regression2d_changed_inner (reg, GUPPI_REGRESSION2D_WEIGHTS);
}

void
guppi_regression2d_set_x_data (GuppiRegression2D *reg, GuppiSeqScalar *seq)
{
  GuppiRegression2DPrivate *p;
  g_return_if_fail (GUPPI_IS_REGRESSION2D (reg));
  g_return_if_fail (seq == NULL || GUPPI_IS_SEQ_SCALAR (seq));
  
  p = priv (reg);

  if (p->x_data != seq) {

    if (p->x_data_handle) {
      gtk_signal_disconnect (GTK_OBJECT (p->x_data), p->x_data_handle);
    }

    guppi_refcounting_assign (p->x_data, seq);

    if (p->x_data) {
      p->x_data_handle = gtk_signal_connect_after (GTK_OBJECT (p->x_data),
						   "changed",
						   GTK_SIGNAL_FUNC (x_data_changed_inner_proxy),
						   reg);
    } else {
      p->x_data_handle = 0;
    }

    guppi_regression2d_changed_inner (reg, GUPPI_REGRESSION2D_X_DATA);
  }
}

void
guppi_regression2d_set_y_data (GuppiRegression2D *reg, GuppiSeqScalar *seq)
{
  GuppiRegression2DPrivate *p;
  g_return_if_fail (GUPPI_IS_REGRESSION2D (reg));
  g_return_if_fail (seq == NULL || GUPPI_IS_SEQ_SCALAR (seq));

  p = priv (reg);

  if (p->y_data != seq) {

    if (p->y_data_handle) {
      gtk_signal_disconnect (GTK_OBJECT (p->y_data), p->y_data_handle);
    }

    guppi_refcounting_assign (p->y_data, seq);

    if (p->y_data) {
      p->y_data_handle = gtk_signal_connect_after (GTK_OBJECT (p->y_data),
						   "changed",
						   GTK_SIGNAL_FUNC (y_data_changed_inner_proxy),
						   reg);
    } else {
      p->y_data_handle = 0;
    }

    guppi_regression2d_changed_inner (reg, GUPPI_REGRESSION2D_Y_DATA);
  }
}

void
guppi_regression2d_set_mask (GuppiRegression2D *reg, GuppiSeqBoolean *seq)
{
  GuppiRegression2DPrivate *p;
  g_return_if_fail (GUPPI_IS_REGRESSION2D (reg));
  g_return_if_fail (seq == NULL || GUPPI_IS_SEQ_BOOLEAN (seq));
  g_return_if_fail (guppi_regression2d_allow_mask (reg));

  p = priv (reg);

  if (p->mask != seq) {

    if (p->mask_handle) {
      gtk_signal_disconnect (GTK_OBJECT (p->mask), p->mask_handle);
    }

    guppi_refcounting_assign (p->mask, seq);

    if (p->mask) {
      p->mask_handle = gtk_signal_connect_after (GTK_OBJECT (p->mask),
						 "changed",
						 GTK_SIGNAL_FUNC (mask_changed_inner_proxy),
						 reg);
    } else {
      p->mask_handle = 0;
    }

    guppi_regression2d_changed_inner (reg, GUPPI_REGRESSION2D_MASK);
  }
}

void
guppi_regression2d_set_weights (GuppiRegression2D *reg, GuppiSeqScalar *seq)
{
  GuppiRegression2DPrivate *p;
  g_return_if_fail (GUPPI_IS_REGRESSION2D (reg));
  g_return_if_fail (seq == NULL || GUPPI_IS_SEQ_SCALAR (seq));
  g_return_if_fail (guppi_regression2d_allow_weights (reg));

  p = priv (reg);

  if (p->weights != seq) {

    if (p->weights_handle) {
      gtk_signal_disconnect (GTK_OBJECT (p->weights), p->weights_handle);
    }

    guppi_refcounting_assign (p->weights, seq);

    if (p->weights) {
      p->weights_handle = gtk_signal_connect_after (GTK_OBJECT (p->weights),
						    "changed",
						    GTK_SIGNAL_FUNC (weights_changed_inner_proxy),
						    reg);
    } else {
      p->weights_handle = 0;
    }

    guppi_regression2d_changed_inner (reg, GUPPI_REGRESSION2D_WEIGHTS);
  }
}

void
guppi_regression2d_freeze (GuppiRegression2D *reg)
{
  g_return_if_fail (GUPPI_IS_REGRESSION2D (reg));

  ++priv (reg)->freeze_count;
}

void
guppi_regression2d_thaw (GuppiRegression2D *reg)
{
  g_return_if_fail (GUPPI_IS_REGRESSION2D (reg));
  g_return_if_fail (priv (reg)->freeze_count > 0);

  --priv (reg)->freeze_count;
  if (priv (reg)->freeze_count == 0)
    guppi_regression2d_changed_inner (reg, 0);
}


