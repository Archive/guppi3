/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-simple-linreg.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <math.h>
#include <gnan.h>

#include <gtk/gtksignal.h>

#include <guppi-convenient.h>
#include <guppi-specfns.h>
#include <guppi-seq.h>
#include "guppi-simple-linreg.h"

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0
};

enum {
  CHANGED,
  LAST_SIGNAL
};
static guint slinreg_signals[LAST_SIGNAL] = { 0 };

static void
guppi_simple_linreg_get_arg (GtkObject * obj, GtkArg * arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_simple_linreg_set_arg (GtkObject * obj, GtkArg * arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_simple_linreg_destroy (GtkObject * obj)
{
  if (parent_class->destroy)
    parent_class->destroy (obj);
}

static void
guppi_simple_linreg_finalize (GtkObject * obj)
{
  GuppiSimpleLinreg *slr = GUPPI_SIMPLE_LINREG (obj);

  guppi_finalized (obj);

  guppi_unref0 (slr->x_data);
  guppi_unref0 (slr->y_data);
  guppi_unref0 (slr->mask_data);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_simple_linreg_class_init (GuppiSimpleLinregClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;

  parent_class = gtk_type_class (GTK_TYPE_OBJECT);

  slinreg_signals[CHANGED] =
    gtk_signal_new ("changed",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GuppiSimpleLinregClass, changed),
		    gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);

  gtk_object_class_add_signals (object_class, slinreg_signals, LAST_SIGNAL);

  object_class->get_arg = guppi_simple_linreg_get_arg;
  object_class->set_arg = guppi_simple_linreg_set_arg;
  object_class->destroy = guppi_simple_linreg_destroy;
  object_class->finalize = guppi_simple_linreg_finalize;
}

static void
guppi_simple_linreg_init (GuppiSimpleLinreg * obj)
{
  obj->valid = FALSE;
}

GtkType guppi_simple_linreg_get_type (void)
{
  static GtkType guppi_simple_linreg_type = 0;
  if (!guppi_simple_linreg_type) {
    static const GtkTypeInfo guppi_simple_linreg_info = {
      "GuppiSimpleLinreg",
      sizeof (GuppiSimpleLinreg),
      sizeof (GuppiSimpleLinregClass),
      (GtkClassInitFunc) guppi_simple_linreg_class_init,
      (GtkObjectInitFunc) guppi_simple_linreg_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_simple_linreg_type =
      gtk_type_unique (GTK_TYPE_OBJECT, &guppi_simple_linreg_info);
  }
  return guppi_simple_linreg_type;
}

GuppiSimpleLinreg *
guppi_simple_linreg_new (void)
{
  return GUPPI_SIMPLE_LINREG (guppi_type_new (guppi_simple_linreg_get_type ()));
}

static void
guppi_simple_linreg_recalc (GuppiSimpleLinreg * slr)
{
  gconstpointer x_raw, y_raw;
  gint x_stride, y_stride;
  double ssq_xy = 0, ssq_x = 0, ssq_y = 0;
  double mean_x = 0, mean_y = 0, omx, omy;
  double x, y, pred_y, t;
  gint i0, i, i1, im0, im1;
  guint count = 0;
  gboolean have_missing;

  g_return_if_fail (slr != NULL);

  slr->valid = FALSE;

  if (slr->x_data != NULL && slr->y_data != NULL) {

    x_raw = guppi_seq_scalar_raw (slr->x_data, &x_stride);
    y_raw = guppi_seq_scalar_raw (slr->y_data, &y_stride);

    have_missing = guppi_seq_has_missing (GUPPI_SEQ (slr->x_data)) ||
      guppi_seq_has_missing (GUPPI_SEQ (slr->y_data));

    guppi_seq_common_bounds (GUPPI_SEQ (slr->x_data),
			     GUPPI_SEQ (slr->y_data), &i0, &i1);

    /* Our first pass --- we calculate the model coefficients */

    if (slr->mask_data) {
      im0 = guppi_seq_boolean_first_true (slr->mask_data);
      im1 = guppi_seq_max_index (GUPPI_SEQ (slr->mask_data));
      i0 = MAX (i0, im0);
      i1 = MIN (i1, im1);
      if (i0 <= i1 && !guppi_seq_boolean_get (slr->mask_data, i0))
	i0 = guppi_seq_boolean_next_true (slr->mask_data, i0);
    }

    i = i0;
    while (i <= i1) {

      if (!have_missing ||
	  (guppi_seq_available (GUPPI_SEQ (slr->x_data), i) &&
	   guppi_seq_available (GUPPI_SEQ (slr->y_data), i))) {

	if (x_raw)
	  x = guppi_seq_scalar_raw_get (x_raw, x_stride, i);
	else
	  x = guppi_seq_scalar_get (slr->x_data, i);

	if (y_raw)
	  y = guppi_seq_scalar_raw_get (y_raw, y_stride, i);
	else
	  y = guppi_seq_scalar_get (slr->y_data, i);

	if (count == 0) {
	  ssq_xy = 0;
	  ssq_x = 0;
	  mean_x = x;
	  mean_y = y;
	} else {
	  omx = mean_x;
	  mean_x += (x - mean_x) / (count + 1);
	  ssq_x += (x - mean_x) * (x - omx);
	  ssq_xy += (x - mean_x) * (y - mean_y);
	  omy = mean_y;
	  mean_y += (y - mean_y) / (count + 1);
	  ssq_y += (y - mean_y) * (y - omy);
	}

	++count;

      }

      if (slr->mask_data)
	i = guppi_seq_boolean_next_true (slr->mask_data, i);
      else
	++i;
    }

    if (count > 2) {

      slr->count = count;
      slr->slope = ssq_xy / ssq_x;
      slr->intercept = mean_y - mean_x * slr->slope;
      slr->tss_x = ssq_x;
      slr->tss_y = ssq_y;
      slr->mean_x = mean_x;
      slr->mean_y = mean_y;
      if (ssq_x * ssq_y > 0)
	slr->R = ssq_xy / sqrt (ssq_x * ssq_y);
      else
	slr->R = G_NAN;

      slr->Rsq = slr->R * slr->R;
      
      if (count > 2)
	slr->adj_Rsq = slr->Rsq - 1 / (count - 2.0) * (1 - slr->Rsq);
      else
	slr->adj_Rsq = G_NAN;

      /* Our second pass --- we calculate info about residuals */

      slr->rss = 0;
      slr->ess = 0;

      i = i0;
      while (i <= i1) {

	if (!have_missing ||
	    (guppi_seq_available (GUPPI_SEQ (slr->x_data), i) &&
	     guppi_seq_available (GUPPI_SEQ (slr->y_data), i))) {

	  if (x_raw)
	    x = guppi_seq_scalar_raw_get (x_raw, x_stride, i);
	  else
	    x = guppi_seq_scalar_get (slr->x_data, i);

	  if (y_raw)
	    y = guppi_seq_scalar_raw_get (y_raw, y_stride, i);
	  else
	    y = guppi_seq_scalar_get (slr->y_data, i);

	  pred_y = slr->slope * x + slr->intercept;

	  t = pred_y - y;
	  slr->rss += t * t;

	  t = pred_y - mean_y;
	  slr->ess += t * t;

	  if (slr->mask_data)
	    i = guppi_seq_boolean_next_true (slr->mask_data, i);
	  else
	    ++i;
	}
      }

      if (slr->rss >= 0 && slr->count > 2)
	slr->residual_sdev = sqrt (slr->rss / (slr->count - 2));
      else
	slr->residual_sdev = G_NAN;

      if (slr->tss_x > 0)
	slr->slope_serr = slr->residual_sdev / sqrt (slr->tss_x);
      else
	slr->slope_serr = G_NAN;
      
      if (slr->count > 0 && slr->tss_x > 0)
	slr->intercept_serr = slr->residual_sdev *
	  sqrt (1.0 / slr->count + mean_x * mean_x / slr->tss_x);
      else
	slr->intercept_serr = G_NAN;

      if (slr->slope_serr > 0)
	slr->slope_t = slr->slope / slr->slope_serr;
      else
	slr->slope_t = G_NAN;

      if (slr->intercept_serr > 0)
	slr->intercept_t = slr->intercept / slr->intercept_serr;
      else
	slr->intercept_t = G_NAN;

      if (!g_isnan (slr->slope_t)) 
	slr->slope_p =
	  1 - (t_cdf (slr->count - 2, fabs (slr->slope_t)) -
	       t_cdf (slr->count - 2, -fabs (slr->slope_t)));
      else
	slr->slope_p = G_NAN;

      if (!g_isnan (slr->intercept_t)) 
	slr->intercept_p =
	  1 - (t_cdf (slr->count - 2, fabs (slr->intercept_t)) -
	       t_cdf (slr->count - 2, -fabs (slr->intercept_t)));
      else
	slr->intercept_t = G_NAN;
	  
      if (slr->rss > 0 && slr->count > 2)
	slr->F = slr->ess / (slr->rss / (slr->count - 2));
      else
	slr->F = G_NAN;

      if (!g_isnan (slr->F))
	slr->p =
	  1 - (t_cdf (slr->count - 2, sqrt (slr->F)) -
	       t_cdf (slr->count - 2, -sqrt (slr->F)));
      else
	slr->p = G_NAN;

      slr->valid = TRUE;

    }
  }

  gtk_signal_emit (GTK_OBJECT (slr), slinreg_signals[CHANGED]);
}

void
guppi_simple_linreg_set_x_data (GuppiSimpleLinreg * slr, GuppiSeqScalar * x)
{
  g_return_if_fail (slr != NULL);

  if (slr->x_data != x) {

    if (slr->x_data) {
      gtk_signal_disconnect_by_data (GTK_OBJECT (slr->x_data), slr);
      guppi_unref (slr->x_data);
    }

    slr->x_data = x;

    if (slr->x_data) {
      gtk_signal_connect_object (GTK_OBJECT (slr->x_data),
				 "changed",
				 GTK_SIGNAL_FUNC (guppi_simple_linreg_recalc),
				 GTK_OBJECT (slr));
      guppi_ref (slr->x_data);
    }

    guppi_simple_linreg_recalc (slr);
  }
}

void
guppi_simple_linreg_set_y_data (GuppiSimpleLinreg * slr, GuppiSeqScalar * y)
{
  g_return_if_fail (slr != NULL);

  if (slr->y_data != y) {

    if (slr->y_data) {
      gtk_signal_disconnect_by_data (GTK_OBJECT (slr->y_data), slr);
      guppi_unref (slr->y_data);
    }

    slr->y_data = y;

    if (slr->y_data) {
      gtk_signal_connect_object (GTK_OBJECT (slr->y_data),
				 "changed",
				 GTK_SIGNAL_FUNC (guppi_simple_linreg_recalc),
				 GTK_OBJECT (slr));
      guppi_ref (slr->y_data);
    }
    
    guppi_simple_linreg_recalc (slr);
  }
}

void
guppi_simple_linreg_set_mask (GuppiSimpleLinreg * slr, GuppiSeqBoolean * m)
{
  g_return_if_fail (slr != NULL);

  if (slr->mask_data != m) {

    if (slr->mask_data) {
      gtk_signal_disconnect_by_data (GTK_OBJECT (slr->mask_data), slr);
      guppi_unref (slr->mask_data);
    }

    slr->mask_data = m;

    if (slr->mask_data) {
      gtk_signal_connect_object (GTK_OBJECT (slr->mask_data),
				 "changed",
				 GTK_SIGNAL_FUNC (guppi_simple_linreg_recalc),
				 GTK_OBJECT (slr));
      guppi_ref (slr->mask_data);
    }

    guppi_simple_linreg_recalc (slr);
  }
}


double
guppi_simple_linreg_slope (const GuppiSimpleLinreg * slr)
{
  g_return_val_if_fail (slr != NULL, 0);
  return slr->slope;
}

double
guppi_simple_linreg_intercept (const GuppiSimpleLinreg * slr)
{
  g_return_val_if_fail (slr != NULL, 0);
  return slr->intercept;
}

double
guppi_simple_linreg_R (const GuppiSimpleLinreg * slr)
{
  g_return_val_if_fail (slr != NULL, 0);
  return slr->R;
}

double
guppi_simple_linreg_R_squared (const GuppiSimpleLinreg * slr)
{
  g_return_val_if_fail (slr != NULL, 0);
  return slr->Rsq;
}

double
guppi_simple_linreg_residual_sdev (const GuppiSimpleLinreg * slr)
{
  g_return_val_if_fail (slr != NULL, 0);
  return slr->residual_sdev;
}

double
guppi_simple_linreg_slope_serr (const GuppiSimpleLinreg * slr)
{
  g_return_val_if_fail (slr != NULL, 0);
  return slr->slope_serr;
}

double
guppi_simple_linreg_intercept_serr (const GuppiSimpleLinreg * slr)
{
  g_return_val_if_fail (slr != NULL, 0);
  return slr->intercept_serr;
}

double
guppi_simple_linreg_slope_t (const GuppiSimpleLinreg * slr)
{
  g_return_val_if_fail (slr != NULL, 0);
  return slr->slope_t;
}

double
guppi_simple_linreg_intercept_t (const GuppiSimpleLinreg * slr)
{
  g_return_val_if_fail (slr != NULL, 0);
  return slr->intercept_t;
}

double
guppi_simple_linreg_slope_p (const GuppiSimpleLinreg * slr)
{
  g_return_val_if_fail (slr != NULL, 0);
  return slr->slope_p;
}

double
guppi_simple_linreg_intercept_p (const GuppiSimpleLinreg * slr)
{
  g_return_val_if_fail (slr != NULL, 0);
  return slr->intercept_p;
}

double
guppi_simple_linreg_F (const GuppiSimpleLinreg * slr)
{
  g_return_val_if_fail (slr != NULL, 0);
  return slr->F;
}

double
guppi_simple_linreg_F_p (const GuppiSimpleLinreg * slr)
{
  g_return_val_if_fail (slr != NULL, 0);
  return slr->p;
}





/* $Id$ */
