/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-regression-linear.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <guppi-memory.h>
#include <gnan.h>
#include <guppi-specfns.h>
#include "guppi-regression-linear.h"

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0
};

typedef struct _GuppiRegressionLinearPrivate GuppiRegressionLinearPrivate;
struct _GuppiRegressionLinearPrivate {
  gboolean valid;
  guint count;
  double slope, intercept;
  double residual_sdev;
  double slope_serr, intercept_serr;
  double slope_t, intercept_t;
  double slope_p, intercept_p;
  double ess, rss, tss_x, tss_y;
  double mean_x, mean_y;
  double F;
  double p;
  double R, Rsq, adj_Rsq;
};

#define priv(x) ((GuppiRegressionLinearPrivate*)(GUPPI_REGRESSION_LINEAR((x))->opaque_internals))

static void guppi_regression_linear_recalc  (GuppiRegression2D *, guint elements);

static void
guppi_regression_linear_get_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_regression_linear_set_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_regression_linear_finalize (GtkObject *obj)
{
  GuppiRegressionLinear *x = GUPPI_REGRESSION_LINEAR(obj);

  g_free (x->opaque_internals);
  x->opaque_internals = NULL;

  guppi_finalized (obj);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_regression_linear_class_init (GuppiRegressionLinearClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;
  GuppiRegression2DClass *twod_class = GUPPI_REGRESSION2D_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_REGRESSION2D);

  twod_class->changed = guppi_regression_linear_recalc;

  object_class->get_arg = guppi_regression_linear_get_arg;
  object_class->set_arg = guppi_regression_linear_set_arg;
  object_class->finalize = guppi_regression_linear_finalize;

}

static void
guppi_regression_linear_init (GuppiRegressionLinear *obj)
{
  GuppiRegressionLinearPrivate *p = g_new0 (GuppiRegressionLinearPrivate, 1);
  obj->opaque_internals = p;

}

GtkType
guppi_regression_linear_get_type (void)
{
  static GtkType guppi_regression_linear_type = 0;
  if (!guppi_regression_linear_type) {
    static const GtkTypeInfo guppi_regression_linear_info = {
      "GuppiRegressionLinear",
      sizeof (GuppiRegressionLinear),
      sizeof (GuppiRegressionLinearClass),
      (GtkClassInitFunc)guppi_regression_linear_class_init,
      (GtkObjectInitFunc)guppi_regression_linear_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    guppi_regression_linear_type = gtk_type_unique (GUPPI_TYPE_REGRESSION2D, &guppi_regression_linear_info);
  }
  return guppi_regression_linear_type;
}

GuppiRegression2D *
guppi_regression_linear_new (void)
{
  return GUPPI_REGRESSION2D (guppi_type_new (guppi_regression_linear_get_type ()));
}

static void
guppi_regression_linear_recalc (GuppiRegression2D *reg, guint elements)
{
  GuppiRegressionLinearPrivate *p;

  GuppiSeqScalar *x_data;
  GuppiSeqScalar *y_data;
  GuppiSeqBoolean *mask;
  GuppiSeqScalar *weights;

  gconstpointer x_raw, y_raw;
  gint x_stride, y_stride;
  double ssq_xy = 0, ssq_x = 0, ssq_y = 0;
  double mean_x = 0, mean_y = 0, omx, omy;
  double x, y, pred_y, t;
  gint i0, i, i1, im0, im1;
  guint count = 0;
  gboolean have_missing;
  
  g_return_if_fail (GUPPI_IS_REGRESSION_LINEAR (reg));
  p = priv (reg);

  x_data  = guppi_regression2d_x_data  (reg);
  y_data  = guppi_regression2d_y_data  (reg);
  mask    = guppi_regression2d_mask    (reg);
  weights = guppi_regression2d_weights (reg);

  p->valid = FALSE;

  if (x_data != NULL && y_data != NULL) {

    x_raw = guppi_seq_scalar_raw (x_data, &x_stride);
    y_raw = guppi_seq_scalar_raw (y_data, &y_stride);

    have_missing = guppi_seq_has_missing (GUPPI_SEQ (x_data)) ||
      guppi_seq_has_missing (GUPPI_SEQ (y_data));

    guppi_seq_common_bounds (GUPPI_SEQ (x_data),
			     GUPPI_SEQ (y_data), &i0, &i1);

    /* Our first pass --- we calculate the model coefficients */

    if (mask) {
      im0 = guppi_seq_boolean_first_true (mask);
      im1 = guppi_seq_max_index (GUPPI_SEQ (mask));
      i0 = MAX (i0, im0);
      i1 = MIN (i1, im1);
      if (i0 <= i1 && !guppi_seq_boolean_get (mask, i0))
	i0 = guppi_seq_boolean_next_true (mask, i0);
    }

    i = i0;
    while (i <= i1) {

      if (!have_missing ||
	  (guppi_seq_available (GUPPI_SEQ (x_data), i) &&
	   guppi_seq_available (GUPPI_SEQ (y_data), i))) {

	if (x_raw)
	  x = guppi_seq_scalar_raw_get (x_raw, x_stride, i);
	else
	  x = guppi_seq_scalar_get (x_data, i);

	if (y_raw)
	  y = guppi_seq_scalar_raw_get (y_raw, y_stride, i);
	else
	  y = guppi_seq_scalar_get (y_data, i);

	if (count == 0) {
	  ssq_xy = 0;
	  ssq_x = 0;
	  mean_x = x;
	  mean_y = y;
	} else {
	  omx = mean_x;
	  mean_x += (x - mean_x) / (count + 1);
	  ssq_x += (x - mean_x) * (x - omx);
	  ssq_xy += (x - mean_x) * (y - mean_y);
	  omy = mean_y;
	  mean_y += (y - mean_y) / (count + 1);
	  ssq_y += (y - mean_y) * (y - omy);
	}

	++count;

      }

      if (mask)
	i = guppi_seq_boolean_next_true (mask, i);
      else
	++i;
    }

    if (count > 2) {

      p->count = count;
      p->slope = ssq_xy / ssq_x;
      p->intercept = mean_y - mean_x * p->slope;
      p->tss_x = ssq_x;
      p->tss_y = ssq_y;
      p->mean_x = mean_x;
      p->mean_y = mean_y;
      if (ssq_x * ssq_y > 0)
	p->R = ssq_xy / sqrt (ssq_x * ssq_y);
      else
	p->R = G_NAN;

      p->Rsq = p->R * p->R;
      
      if (count > 2)
	p->adj_Rsq = p->Rsq - 1 / (count - 2.0) * (1 - p->Rsq);
      else
	p->adj_Rsq = G_NAN;

      /* Our second pass --- we calculate info about residuals */

      p->rss = 0;
      p->ess = 0;

      i = i0;
      while (i <= i1) {

	if (!have_missing ||
	    (guppi_seq_available (GUPPI_SEQ (x_data), i) &&
	     guppi_seq_available (GUPPI_SEQ (y_data), i))) {

	  if (x_raw)
	    x = guppi_seq_scalar_raw_get (x_raw, x_stride, i);
	  else
	    x = guppi_seq_scalar_get (x_data, i);

	  if (y_raw)
	    y = guppi_seq_scalar_raw_get (y_raw, y_stride, i);
	  else
	    y = guppi_seq_scalar_get (y_data, i);

	  pred_y = p->slope * x + p->intercept;

	  t = pred_y - y;
	  p->rss += t * t;

	  t = pred_y - mean_y;
	  p->ess += t * t;

	  if (mask)
	    i = guppi_seq_boolean_next_true (mask, i);
	  else
	    ++i;
	}
      }

      if (p->rss >= 0 && p->count > 2)
	p->residual_sdev = sqrt (p->rss / (p->count - 2));
      else
	p->residual_sdev = G_NAN;

      if (p->tss_x > 0)
	p->slope_serr = p->residual_sdev / sqrt (p->tss_x);
      else
	p->slope_serr = G_NAN;
      
      if (p->count > 0 && p->tss_x > 0)
	p->intercept_serr = p->residual_sdev *
	  sqrt (1.0 / p->count + mean_x * mean_x / p->tss_x);
      else
	p->intercept_serr = G_NAN;

      if (p->slope_serr > 0)
	p->slope_t = p->slope / p->slope_serr;
      else
	p->slope_t = G_NAN;

      if (p->intercept_serr > 0)
	p->intercept_t = p->intercept / p->intercept_serr;
      else
	p->intercept_t = G_NAN;

      if (!g_isnan (p->slope_t)) 
	p->slope_p =
	  1 - (t_cdf (p->count - 2, fabs (p->slope_t)) -
	       t_cdf (p->count - 2, -fabs (p->slope_t)));
      else
	p->slope_p = G_NAN;

      if (!g_isnan (p->intercept_t)) 
	p->intercept_p =
	  1 - (t_cdf (p->count - 2, fabs (p->intercept_t)) -
	       t_cdf (p->count - 2, -fabs (p->intercept_t)));
      else
	p->intercept_t = G_NAN;
	  
      if (p->rss > 0 && p->count > 2)
	p->F = p->ess / (p->rss / (p->count - 2));
      else
	p->F = G_NAN;

      if (!g_isnan (p->F))
	p->p =
	  1 - (t_cdf (p->count - 2, sqrt (p->F)) -
	       t_cdf (p->count - 2, -sqrt (p->F)));
      else
	p->p = G_NAN;

      p->valid = TRUE;

    }
  }
}


/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

gboolean
guppi_regression_linear_valid (GuppiRegressionLinear *reg)
{
  g_return_val_if_fail (GUPPI_IS_REGRESSION_LINEAR (reg), FALSE);
  return priv (reg)->valid;
}

double
guppi_regression_linear_slope (GuppiRegressionLinear *reg)
{
  g_return_val_if_fail (GUPPI_IS_REGRESSION_LINEAR (reg), 0);
  return priv (reg)->slope;
}

double
guppi_regression_linear_intercept (GuppiRegressionLinear *reg)
{
  g_return_val_if_fail (GUPPI_IS_REGRESSION_LINEAR (reg), 0);
  return priv (reg)->intercept;
}

double
guppi_regression_linear_R (GuppiRegressionLinear *reg)
{
  g_return_val_if_fail (GUPPI_IS_REGRESSION_LINEAR (reg), 0);
  return priv (reg)->R;
}

double
guppi_regression_linear_R_squared (GuppiRegressionLinear *reg)
{
  g_return_val_if_fail (GUPPI_IS_REGRESSION_LINEAR (reg), 0);
  return priv (reg)->Rsq;
}

double
guppi_regression_linear_residual_sdev (GuppiRegressionLinear *reg)
{
  g_return_val_if_fail (GUPPI_IS_REGRESSION_LINEAR (reg), 0);
  return priv (reg)->residual_sdev;
}

double
guppi_regression_linear_slope_serr (GuppiRegressionLinear *reg)
{
  g_return_val_if_fail (GUPPI_IS_REGRESSION_LINEAR (reg), 0);
  return priv (reg)->slope_serr;
}

double
guppi_regression_linear_intercept_serr (GuppiRegressionLinear *reg)
{
  g_return_val_if_fail (GUPPI_IS_REGRESSION_LINEAR (reg), 0);
  return priv (reg)->intercept_serr;
}

double
guppi_regression_linear_slope_t (GuppiRegressionLinear *reg)
{
  g_return_val_if_fail (GUPPI_IS_REGRESSION_LINEAR (reg), 0);
  return priv (reg)->slope_t;
}

double
guppi_regression_linear_intercept_t (GuppiRegressionLinear *reg)
{
  g_return_val_if_fail (GUPPI_IS_REGRESSION_LINEAR (reg), 0);
  return priv (reg)->intercept_t;
}

double
guppi_regression_linear_slope_p (GuppiRegressionLinear *reg)
{
  g_return_val_if_fail (GUPPI_IS_REGRESSION_LINEAR (reg), 0);
  return priv (reg)->slope_p;
}

double
guppi_regression_linear_intercept_p (GuppiRegressionLinear *reg)
{
  g_return_val_if_fail (GUPPI_IS_REGRESSION_LINEAR (reg), 0);
  return priv (reg)->intercept_p;
}

double
guppi_regression_linear_F (GuppiRegressionLinear *reg)
{
  g_return_val_if_fail (GUPPI_IS_REGRESSION_LINEAR (reg), 0);
  return priv (reg)->F;
}

double
guppi_regression_linear_F_p (GuppiRegressionLinear *reg)
{
  g_return_val_if_fail (GUPPI_IS_REGRESSION_LINEAR (reg), 0);
  return priv (reg)->p;
}
