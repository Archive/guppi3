/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-regression-polynomial.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_REGRESSION_POLYNOMIAL_H
#define _INC_GUPPI_REGRESSION_POLYNOMIAL_H

#include <gnome.h>
#include  "guppi-defs.h"
#include  "guppi-polynomial.h"
#include "guppi-regression2d.h"


BEGIN_GUPPI_DECLS;

typedef struct _GuppiRegressionPolynomial GuppiRegressionPolynomial;
typedef struct _GuppiRegressionPolynomialClass GuppiRegressionPolynomialClass;

struct _GuppiRegressionPolynomial {
  GuppiRegression2D parent;
  gpointer opaque_internals;
};

struct _GuppiRegressionPolynomialClass {
  GuppiRegression2DClass parent_class;
};

#define GUPPI_TYPE_REGRESSION_POLYNOMIAL (guppi_regression_polynomial_get_type ())
#define GUPPI_REGRESSION_POLYNOMIAL(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_REGRESSION_POLYNOMIAL,GuppiRegressionPolynomial))
#define GUPPI_REGRESSION_POLYNOMIAL0(obj) ((obj) ? (GUPPI_REGRESSION_POLYNOMIAL(obj)) : NULL)
#define GUPPI_REGRESSION_POLYNOMIAL_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_REGRESSION_POLYNOMIAL,GuppiRegressionPolynomialClass))
#define GUPPI_IS_REGRESSION_POLYNOMIAL(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_REGRESSION_POLYNOMIAL))
#define GUPPI_IS_REGRESSION_POLYNOMIAL0(obj) (((obj) == NULL) || (GUPPI_IS_REGRESSION_POLYNOMIAL(obj)))
#define GUPPI_IS_REGRESSION_POLYNOMIAL_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_REGRESSION_POLYNOMIAL))

GtkType guppi_regression_polynomial_get_type (void);

GuppiRegression2D *guppi_regression_polynomial_new (void);

void   guppi_regression_polynomial_set_degree  (GuppiRegressionPolynomial *, gint n);

gint   guppi_regression_polynomial_degree      (GuppiRegressionPolynomial *);
GuppiPolynomial *guppi_regression_polynomial_get_polynomial (GuppiRegressionPolynomial *);



END_GUPPI_DECLS;

#endif /* _INC_GUPPI_REGRESSION_POLYNOMIAL_H */

/* $Id$ */
