/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-regression-linear.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_REGRESSION_LINEAR_H
#define _INC_GUPPI_REGRESSION_LINEAR_H

#include <gnome.h>
#include  "guppi-defs.h"
#include "guppi-regression2d.h"

BEGIN_GUPPI_DECLS;

typedef struct _GuppiRegressionLinear GuppiRegressionLinear;
typedef struct _GuppiRegressionLinearClass GuppiRegressionLinearClass;

struct _GuppiRegressionLinear {
  GuppiRegression2D parent;
  gpointer opaque_internals;
};

struct _GuppiRegressionLinearClass {
  GuppiRegression2DClass parent_class;
};

#define GUPPI_TYPE_REGRESSION_LINEAR (guppi_regression_linear_get_type ())
#define GUPPI_REGRESSION_LINEAR(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_REGRESSION_LINEAR,GuppiRegressionLinear))
#define GUPPI_REGRESSION_LINEAR0(obj) ((obj) ? (GUPPI_REGRESSION_LINEAR(obj)) : NULL)
#define GUPPI_REGRESSION_LINEAR_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_REGRESSION_LINEAR,GuppiRegressionLinearClass))
#define GUPPI_IS_REGRESSION_LINEAR(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_REGRESSION_LINEAR))
#define GUPPI_IS_REGRESSION_LINEAR0(obj) (((obj) == NULL) || (GUPPI_IS_REGRESSION_LINEAR(obj)))
#define GUPPI_IS_REGRESSION_LINEAR_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_REGRESSION_LINEAR))

GtkType guppi_regression_linear_get_type (void);

GuppiRegression2D *guppi_regression_linear_new (void);

gboolean guppi_regression_linear_valid (GuppiRegressionLinear *);

double guppi_regression_linear_slope          (GuppiRegressionLinear *);
double guppi_regression_linear_intercept      (GuppiRegressionLinear *);
double guppi_regression_linear_R              (GuppiRegressionLinear *);
double guppi_regression_linear_R_squared      (GuppiRegressionLinear *);

double guppi_regression_linear_residual_sdev  (GuppiRegressionLinear *);

double guppi_regression_linear_slope_serr     (GuppiRegressionLinear *);
double guppi_regression_linear_intercept_serr (GuppiRegressionLinear *);

double guppi_regression_linear_slope_t        (GuppiRegressionLinear *);
double guppi_regression_linear_intercept_t    (GuppiRegressionLinear *);

double guppi_regression_linear_slope_p        (GuppiRegressionLinear *);
double guppi_regression_linear_intercept_p    (GuppiRegressionLinear *);

double guppi_regression_linear_F              (GuppiRegressionLinear *);
double guppi_regression_linear_F_p            (GuppiRegressionLinear *);


END_GUPPI_DECLS;

#endif /* _INC_GUPPI_REGRESSION_LINEAR_H */

/* $Id$ */
