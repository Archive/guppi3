#!/usr/bin/perl -w

my $is_a_widget = 0;
my $generate_destroy = 1;
my $generate_signals = 1;
my $opaque_internals = 1;

print "Enter Class Name (e.g. GuppiSuperPlotWidget)\n";
print ">>> ";
my $objectname = <STDIN>;
chomp $objectname;

print "\n";
print "Enter Parent Class (e.g. GtkObject)\n";
print ">>> ";
my $parentname = <STDIN>;
chomp $parentname;

my $widgetin = "?";
$widgetin = "n" if $parentname eq "GtkObject";
while ($widgetin ne "y" && $widgetin ne "n") {
    print "\nIs this a widget? (y/n) >>> ";
    $widgetin = <STDIN>;
    chomp $widgetin;
    $widgetin = lc $widgetin;
}
$is_a_widget = ($widgetin eq "y");

if ($is_a_widget) {
    $generate_destroy = 1;
} else {
    $generate_destroy = 0;
}

my $sigin = "?";
while ($sigin ne "y" && $sigin ne "n") {
    print "\nWill $objectname define its own signals? (y/n) >>> ";
    $sigin = <STDIN>;
    chomp $sigin;
    $sigin = lc $sigin;
}
$generate_signals = ($sigin eq "y");

my $opaquein = "?";
while ($opaquein ne "y" && $opaquein ne "n") {
    print "\nWill $objectname be opaque? (y/n) >>> ";
    $opaquein = <STDIN>;
    chomp $opaquein;
    $opaquein = lc $opaquein;
}
$opaque_internals = ($opaquein eq "y");

##############################################################################

my $objectnameprivate = $objectname . "Private";

my ($c_file, $h_file, $blockname, $prefix, $leading_prefix, $blocktype, $blockis);
$c_file = $objectname;
$c_file =~ s/([a-z])([A-Z])/$1\-$2/g;
$blockname = uc $c_file;
$blockname =~ tr/-/_/;
$c_file = lc $c_file;
$h_file = $c_file . ".h";
$c_file .= ".c";

$prefix = lc $blockname;
($leading_prefix) = $prefix =~ /^([^_]+)_/;
$leading_prefix ||= $prefix;

$blocktype = $blockname;
$blocktype =~ s/_/_TYPE_/;

$blockis = $blockname;
$blockis =~ s/_/_IS_/;

my $guard = "_INC_" . $blockname . "_H";

my $parentblocktype = $parentname;
$parentblocktype =~ s/([a-z])([A-Z])/$1_$2/g;
$parentblocktype =~ s/_/_TYPE_/;
$parentblocktype = uc $parentblocktype;

my $signal_array = "";
my $signal_array_decl = "";

if ($generate_signals) {
    $signal_array = $prefix . "_signals";
    $signal_array_decl = $signal_array . "[LAST_SIGNAL]";
}

sub fn { 
    my $x = shift;
    $prefix . "_" . $x . " (" . join(", ", @_), ")";
}

my $yr = 1900 + (localtime)[5];

# I've made myself the default.  Such are the benefits of writing the code...
my $copyright_holder = $ENV{"BOILERMAKER_COPYRIGHT"} || $ENV{"GUPPI_COPYRIGHT"} || "The Free Software Foundation";
my $developer = $ENV{"BOILERMAKER_DEVELOPER"} || $ENV{"GUPPI_DEVELOPER"} || "Jon Trowbridge <trow\@gnu.org>";

my $decl_header = $leading_prefix . "-defs.h";
my $decl_start_macro = uc "BEGIN_" . $leading_prefix . "_DECLS";
my $decl_end_macro = uc "END_" . $leading_prefix . "_DECLS";

#############################################################################

die "Sorry, $h_file already exists" if -e $h_file;
die "Sorry, $c_file already exists" if -e $c_file;

#############################################################################

print STDERR "Writing $h_file...\n";
open(OUT, ">$h_file") || die "Can't open $h_file";
select(OUT);

### .h file

print <<EOF;
/* This is -*- C -*- */
/* vim: set sw=2: */
/* \$Id\$ */

/*
 * $h_file
 *
 * Copyright (C) $yr $copyright_holder
 *
 * Developed by $developer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef $guard
#define $guard

#include <gnome.h>
#include "$decl_header"

$decl_start_macro;

EOF

print "typedef struct _$objectname $objectname;\n";
if ($opaque_internals) {
    print "typedef struct _$objectnameprivate $objectnameprivate;\n";
}
print "typedef struct _$objectname", "Class $objectname", "Class;\n";

print "\n";

if ($opaque_internals) {
    print "struct _$objectnameprivate;\n\n";
}

print "struct _", $objectname, " {\n";
print "  ", $parentname, " parent;\n";
if ($opaque_internals) {
    print "  $objectnameprivate *priv;\n";
}
print "};\n";

print "\n";

print "struct _", $objectname, "Class {\n";
print "  ", $parentname, "Class parent_class;\n";
print "};\n";

print "\n";

print "#define ", $blocktype, " (", fn("get_type"), ")\n";
printf("#define %s(obj) (GTK_CHECK_CAST((obj),%s,%s))\n",
       $blockname, $blocktype, $objectname);
printf("#define %s0(obj) ((obj) ? (%s(obj)) : NULL)\n",
       $blockname, $blockname);
printf("#define %s_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),%s,%sClass))\n",
       $blockname, $blocktype, $objectname);
printf("#define %s(obj) (GTK_CHECK_TYPE((obj), %s))\n", $blockis, $blocktype);
printf("#define %s0(obj) (((obj) == NULL) || (%s(obj)))\n",
       $blockis, $blockis);
printf("#define %s_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), %s))\n",
       $blockis, $blocktype);

print "\n";

print "GtkType ", fn("get_type","void"), ";\n";

print "\n";
if ($is_a_widget) {
    print "GtkWidget *";
} else {
    print "GtkObject *";
}
print fn("new", "void"), ";\n";

print "\n\n\n";

print <<EOF;
$decl_end_macro;

#endif /* $guard */

/* \$Id\$ */
EOF

close(OUT);

#############################################################################

print STDERR "Writing $c_file...\n";
open(OUT, ">$c_file") || die "Can't open $c_file";
select(OUT);

### .c file

print <<EOF;
/* This is -*- C -*- */
/* vim: set sw=2: */
/* \$Id\$ */

/*
 * $c_file
 *
 * Copyright (C) $yr $copyright_holder
 *
 * Developed by $developer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <guppi-memory.h>
#include "$h_file"

static GtkObjectClass *parent_class = NULL;

EOF

if ($generate_signals) {
    print <<EOF;
enum {
  LAST_SIGNAL
};

static guint $signal_array_decl = { 0 };

EOF
}

if ($opaque_internals) {
    print "struct _", $objectnameprivate, " {\n";
    print "\n";
    print "};\n";
    print "\n";
}
	
if ($generate_destroy) {
    print "static void\n";
    print fn("destroy", "GtkObject *obj"), "\n";
    print "{\n";
    print "  if (parent_class->destroy)\n";
    print "    parent_class->destroy (obj);\n";
    print "}\n\n";
}

print "static void\n";
print fn("finalize", "GtkObject *obj"), "\n";
print "{\n";
if ($opaque_internals) {
    print "  $objectname *x = $blockname(obj);\n";
    print "\n";
    print "  g_free (x->priv);\n";
    print "  x->priv = NULL;\n";
}
print "\n";
print "  guppi_finalized (obj);\n";
print "\n";
print "  if (parent_class->finalize)\n";
print "    parent_class->finalize (obj);\n";
print "}\n\n";

print "static void\n";
print fn("class_init", $objectname . "Class *klass"), "\n";
print "{\n";
print "  GtkObjectClass *object_class = (GtkObjectClass *)klass;\n";
print "\n";
print "  parent_class = gtk_type_class ($parentblocktype);\n";
print "\n";
if ($generate_destroy) {
    print "  object_class->destroy = $prefix", "_destroy;\n";
}
print "  object_class->finalize = $prefix", "_finalize;\n";
print "\n";
if ($generate_signals) {
    print "#if 0\n";
    print "  /* Signal definition template */\n";
    print "  ", $signal_array, "[CHANGED] =\n";
    print "    gtk_signal_new (\"changed\",\n";
    print "                    GTK_RUN_FIRST,\n";
    print "                    object_class->type,\n";
    print "                    GTK_SIGNAL_OFFSET (", $objectname, "Class, changed),\n";
    print "                    gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);\n";
    print "#endif\n\n";
    print "  gtk_object_class_add_signals (object_class, $signal_array,\n";
    print "                                LAST_SIGNAL);\n";
}
print "}\n";

print "\n";

print "static void\n";
print fn("init", $objectname . " *obj"), "\n";
print "{\n";
if ($opaque_internals) {
    print "  obj->priv = g_new0 ($objectnameprivate, 1);\n";
}
print "\n";
print "}\n";

print "\n";

print "GtkType\n";
print fn("get_type", "void"), "\n";
print "{\n";
print "  static GtkType $prefix", "_type = 0;\n";
print "  if (!$prefix", "_type) {\n";
print "    static const GtkTypeInfo $prefix", "_info = {\n";
print "      \"$objectname\",\n";
print "      sizeof ($objectname),\n";
print "      sizeof ($objectname", "Class),\n";
print "      (GtkClassInitFunc)$prefix", "_class_init,\n";
print "      (GtkObjectInitFunc)$prefix", "_init,\n";
print "      NULL, NULL, (GtkClassInitFunc)NULL\n";
print "    };\n";
print "    $prefix", "_type = gtk_type_unique ($parentblocktype, &$prefix", "_info);\n";
print "  }\n";
print "  return $prefix", "_type;\n";
print "}\n";

print "\n";

if ($is_a_widget) {
    print "GtkWidget *\n";
    print fn("new", "void"), "\n";
    print "{\n";
    print "  return GTK_WIDGET (guppi_type_new (", fn("get_type"), "));\n";
    print "}\n";

} else {
    print "GtkObject *\n";
    print fn("new", "void"), "\n";
    print "{\n";
    print "  return GTK_OBJECT (guppi_type_new (", fn("get_type"), "));\n";
    print "}\n";
}

close(OUT);

print STDERR "...Done.\n";
