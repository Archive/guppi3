/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-curve-func-impl.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_CURVE_FUNC_H
#define _INC_GUPPI_CURVE_FUNC_H

/* #include <gtk/gtk.h> */
#include <guppi-defs.h>
#include <guppi-data.h>
#include <guppi-curve.h>
#include <guppi-fn-wrapper.h>
#include <guppi-curve.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiCurveFunc GuppiCurveFunc;
typedef struct _GuppiCurveFuncClass GuppiCurveFuncClass;

struct _GuppiCurveFunc {
  GuppiCurve parent;

  gboolean reparameterize;
  double t0, t1;
  GuppiFnWrapper *t_fn_wrapper;

  GuppiFnWrapper *x_fn_wrapper;
  GuppiFnWrapper *y_fn_wrapper;
  GuppiFnWrapper *x_binary_fn_wrapper;
  GuppiFnWrapper *y_binary_fn_wrapper;

  double use_2x2_matrix;
  double a, b, c, d;

  GuppiCurve *base_curve;
};

struct _GuppiCurveFuncClass {
  GuppiCurveClass parent_class;
};

#define GUPPI_TYPE_CURVE_FUNC (guppi_curve_func_get_type())
#define GUPPI_CURVE_FUNC(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_CURVE_FUNC,GuppiCurveFunc))
#define GUPPI_CURVE_FUNC0(obj) ((obj) ? (GUPPI_CURVE_FUNC(obj)) : NULL)
#define GUPPI_CURVE_FUNC_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_CURVE_FUNC,GuppiCurveFuncClass))
#define GUPPI_IS_CURVE_FUNC(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_CURVE_FUNC))
#define GUPPI_IS_CURVE_FUNC0(obj) (((obj) == NULL) || (GUPPI_IS_CURVE_FUNC(obj)))
#define GUPPI_IS_CURVE_FUNC_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_CURVE_FUNC))

GtkType guppi_curve_func_get_type (void);

#if 0
GuppiData *guppi_curve_new_reparameterize_linearly (double t0, double t1,
						    GuppiCurve *c);
GuppiData *guppi_curve_new_reparameterize (double t0, double t1,
					   GuppiFnWrapper *fn,
					   GuppiCurve *c);
#endif

END_GUPPI_DECLS

#endif /* _INC_GUPPI_CURVE_FUNC_H */

/* $Id$ */
