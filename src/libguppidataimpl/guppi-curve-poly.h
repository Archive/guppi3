/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-curve-poly-impl.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_CURVE_POLY_H
#define _INC_GUPPI_CURVE_POLY_H

/* #include <gnome.h> */
#include <guppi-defs.h>
#include <guppi-polynomial.h>
#include <guppi-curve.h>
#include <guppi-data.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiCurvePoly GuppiCurvePoly;
typedef struct _GuppiCurvePolyClass GuppiCurvePolyClass;

struct _GuppiCurvePoly {
  GuppiCurve parent;

  double min, max;

  GuppiPolynomial *poly;
  guint changed_handler;
};

struct _GuppiCurvePolyClass {
  GuppiCurveClass parent_class;
};

#define GUPPI_TYPE_CURVE_POLY (guppi_curve_poly_get_type ())
#define GUPPI_CURVE_POLY(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_CURVE_POLY,GuppiCurvePoly))
#define GUPPI_CURVE_POLY0(obj) ((obj) ? (GUPPI_CURVE_POLY(obj)) : NULL)
#define GUPPI_CURVE_POLY_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_CURVE_POLY,GuppiCurvePolyClass))
#define GUPPI_IS_CURVE_POLY(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_CURVE_POLY))
#define GUPPI_IS_CURVE_POLY0(obj) (((obj) == NULL) || (GUPPI_IS_CURVE_POLY(obj)))
#define GUPPI_IS_CURVE_POLY_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_CURVE_POLY))

GtkType guppi_curve_poly_get_type (void);

GuppiCurve *guppi_curve_poly_new (void);

GuppiPolynomial *guppi_curve_poly_get_polynomial (GuppiCurvePoly *curve);
void guppi_curve_poly_set_polynomial (GuppiCurvePoly *curve, GuppiPolynomial *p);

/* Hack for bindings */
GuppiPolynomial *guppi_curve_get_polynomial (GuppiData *data);

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_CURVE_POLY_H */

/* $Id$ */
