/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-curve-func-impl.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_CURVE_CALC_IMPL_H
#define _INC_GUPPI_CURVE_CALC_IMPL_H

/* #include <gtk/gtk.h> */
#include <guppi-defs.h>
#include <guppi-data.h>
#include <guppi-fn-wrapper.h>
#include <guppi-curve.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiCurveCalc GuppiCurveCalc;
typedef struct _GuppiCurveCalcClass GuppiCurveCalcClass;

struct _GuppiCurveCalc {
  GuppiCurve parent;

  double t0, t1;

  double (*x_func) (double, gpointer);
  double (*y_func) (double, gpointer);
  void (*xy_func) (double, double *, double *, gpointer, gpointer);
  gpointer x_user_data;
  gpointer y_user_data;

  GuppiFnWrapper *x_fn_wrapper;
  GuppiFnWrapper *y_fn_wrapper;
};

struct _GuppiCurveCalcClass {
  GuppiCurveClass parent_class;
};

#define GUPPI_TYPE_CURVE_CALC (guppi_curve_calc_get_type())
#define GUPPI_CURVE_CALC(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_CURVE_CALC,GuppiCurveCalc))
#define GUPPI_CURVE_CALC0(obj) ((obj) ? (GUPPI_CURVE_CALC(obj)) : NULL)
#define GUPPI_CURVE_CALC_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_CURVE_CALC,GuppiCurveCalcClass))
#define GUPPI_IS_CURVE_CALC(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_CURVE_CALC))
#define GUPPI_IS_CURVE_CALC0(obj) (((obj) == NULL) || (GUPPI_IS_CURVE_CALC(obj)))
#define GUPPI_IS_CURVE_CALC_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_CURVE_CALC))

GtkType guppi_curve_calc_get_type (void);

#if 0
GuppiData *guppi_curve_new_calc_parametric (double t0, double t1,
					    GuppiFnWrapper * x_fn,
					    GuppiFnWrapper * y_fn);
GuppiData *guppi_curve_new_calc_function (double t0, double t1,
					  GuppiFnWrapper * fn);
#endif

END_GUPPI_DECLS

#endif /* _INC_GUPPI_CURVE_CALC_H */

/* $Id$ */
