/* This is -*- C -*- */
/* $Id$ */

/*
 * func.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-seq-scalar-func.h"

#include <gtk/gtksignal.h>
#include <guppi-convenient.h>
#include <guppi-debug.h>
#include <guppi-memory.h>

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0,
  ARG_SEQ_SCALAR_ARG1,
  ARG_SEQ_SCALAR_ARG2,
  ARG_BASIC_UNARY_FN,
  ARG_BASIC_BINARY_FN,
  ARG_BASIC_UNARY_FN_C,
  ARG_BASIC_BINARY_FN_C,
  ARG_BASIC_FN_USER_DATA,
};

static void
guppi_seq_scalar_func_get_arg (GtkObject *obj, GtkArg *arg,
				    guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_seq_scalar_func_set_arg (GtkObject *obj, GtkArg *arg,
				    guint arg_id)
{
  GuppiSeqScalarFunc *func = GUPPI_SEQ_SCALAR_FUNC (obj);
  GuppiSeqScalar *ss;
  double (*fn1) (double, gpointer);
  double (*fn2) (double, double, gpointer);
  GuppiFnWrapper *fw;

  switch (arg_id) {

  case ARG_SEQ_SCALAR_ARG1:
    ss = GUPPI_SEQ_SCALAR (GTK_VALUE_POINTER (*arg));
    if (ss != func->arg1) {
      if (func->arg1)
	gtk_signal_disconnect_by_func (GTK_OBJECT (func->arg1),
				       guppi_data_touch, func);
      guppi_refcounting_assign (func->arg1, ss);
      guppi_data_changed (GUPPI_DATA (obj));
      if (func->arg1)
	gtk_signal_connect_object (GTK_OBJECT (func->arg1),
				   "changed",
				   GTK_SIGNAL_FUNC (guppi_data_changed),
				   GTK_OBJECT (func));
    }

    break;

  case ARG_SEQ_SCALAR_ARG2:
    ss = GUPPI_SEQ_SCALAR (GTK_VALUE_POINTER (*arg));
    if (ss != func->arg2) {
      if (func->arg2)
	gtk_signal_disconnect_by_func (GTK_OBJECT (func->arg2),
				       guppi_data_touch, func);
      guppi_refcounting_assign (func->arg2, ss);
      guppi_data_changed (GUPPI_DATA (obj));
      if (func->arg2)
	gtk_signal_connect_object (GTK_OBJECT (func->arg2),
				   "changed",
				   GTK_SIGNAL_FUNC (guppi_data_changed),
				   GTK_OBJECT (func));
    }

    break;

  case ARG_BASIC_UNARY_FN:
    fw = GUPPI_FN_WRAPPER (GTK_VALUE_POINTER (*arg));
    if (func->basic_unary_wrapper != fw) {
      guppi_refcounting_assign (func->basic_unary_wrapper, fw);
      guppi_data_changed (GUPPI_DATA (obj));
    }

    break;

  case ARG_BASIC_UNARY_FN_C:
    fn1 = (double (*)(double, gpointer)) GTK_VALUE_POINTER (*arg);
    if (fn1 != func->basic_unary) {
      func->basic_unary = fn1;
      guppi_data_changed (GUPPI_DATA (obj));
    }

    break;


  case ARG_BASIC_BINARY_FN:
    fw = GUPPI_FN_WRAPPER (GTK_VALUE_POINTER (*arg));
    if (func->basic_binary_wrapper != fw) {
      guppi_refcounting_assign (func->basic_binary_wrapper, fw);
      guppi_data_changed (GUPPI_DATA (obj));
    }

    break;

  case ARG_BASIC_BINARY_FN_C:
    fn2 = (double (*)(double, double, gpointer)) GTK_VALUE_POINTER (*arg);
    if (fn2 != func->basic_binary) {
      func->basic_binary = fn2;
      guppi_data_changed (GUPPI_DATA (obj));
    }

    break;

  case ARG_BASIC_FN_USER_DATA:
    if (func->user_data != GTK_VALUE_POINTER (*arg)) {
      func->user_data = GTK_VALUE_POINTER (*arg);
      guppi_data_changed (GUPPI_DATA (obj));
    }

    break;

  default:
    break;
  };
}

static void
guppi_seq_scalar_func_finalize (GtkObject *obj)
{
  GuppiSeqScalarFunc *func = GUPPI_SEQ_SCALAR_FUNC (obj);

  guppi_unref0 (func->basic_unary_wrapper);
  guppi_unref0 (func->basic_binary_wrapper);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/**************************************************************************/

/* no range */

/* no sum */

/* no var */

static double
v_seq_scalar_get (GuppiSeqScalar *impl, gint i)
{
  GuppiSeqScalarFunc *func = GUPPI_SEQ_SCALAR_FUNC (impl);
  double x, y;

  if (func->basic_unary || func->basic_unary_wrapper) {
    x = guppi_seq_scalar_get (func->arg1, i);
    if (func->basic_unary)
      return func->basic_unary (x, func->user_data);
    else
      return guppi_fn_wrapper_eval_d__d (func->basic_unary_wrapper, x);
  }

  if (func->basic_binary || func->basic_binary_wrapper) {
    x = guppi_seq_scalar_get (func->arg1, i);
    y = guppi_seq_scalar_get (func->arg2, i);
    if (func->basic_binary)
      return func->basic_binary (x, y, func->user_data);
    else
      return guppi_fn_wrapper_eval_d__d_d (func->basic_binary_wrapper, x, y);
  }

  g_assert_not_reached ();
  return 0;
}

/* no set */

/* no insert or insert_many */

/* no raw access */

/* no set */

static void
v_seq_size_hint (GuppiSeq *impl, gsize N)
{
  /* A total no-op */
}

static void
v_seq_get_bounds (GuppiSeq *impl, gint *i0, gint *i1)
{
  GuppiSeqScalarFunc *func = GUPPI_SEQ_SCALAR_FUNC (impl);

  if (func->arg2)
    guppi_seq_common_bounds (GUPPI_SEQ (func->arg1), GUPPI_SEQ (func->arg2),
			     i0, i1);
  else
    guppi_seq_bounds (GUPPI_SEQ (func->arg1), i0, i1);
}

/* no delete or delete many */

/* no grow-to-include */

static gboolean
v_seq_has_missing (GuppiSeq *impl)
{
  GuppiSeqScalarFunc *func = GUPPI_SEQ_SCALAR_FUNC (impl);

  return (func->arg1 && guppi_seq_has_missing (GUPPI_SEQ (func->arg1))) ||
    (func->arg2 && guppi_seq_has_missing (GUPPI_SEQ (func->arg2)));
}

static gboolean
v_seq_missing (GuppiSeq *impl, gint i)
{
  GuppiSeqScalarFunc *func = GUPPI_SEQ_SCALAR_FUNC (impl);

  return (func->arg1 && guppi_seq_missing (GUPPI_SEQ (func->arg1), i)) ||
    (func->arg2 && guppi_seq_missing (GUPPI_SEQ (func->arg2), i));
}

static GuppiData *
v_data_copy (GuppiData *impl)
{
  /* Unimplemented. */
  g_assert_not_reached ();
  return NULL;
}

static gint
v_data_size_in_bytes (GuppiData *impl)
{
  return sizeof (GuppiSeqScalarFunc);
}

#define add_arg(str, t, symb) \
gtk_object_add_arg_type("GuppiSeqScalarFunc::" str, t, GTK_ARG_WRITABLE, symb)

static void
guppi_seq_scalar_func_class_init (GuppiSeqScalarFuncClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiSeqScalarClass *seq_scalar_class =
    GUPPI_SEQ_SCALAR_CLASS (klass);
  GuppiSeqClass *seq_class = GUPPI_SEQ_CLASS (klass);
  GuppiDataClass *data_class = GUPPI_DATA_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_SEQ_SCALAR);

  object_class->get_arg = guppi_seq_scalar_func_get_arg;
  object_class->set_arg = guppi_seq_scalar_func_set_arg;
  object_class->finalize = guppi_seq_scalar_func_finalize;

  seq_scalar_class->get = v_seq_scalar_get;

  seq_class->size_hint = v_seq_size_hint;
  seq_class->get_bounds = v_seq_get_bounds;
  seq_class->has_missing = v_seq_has_missing;
  seq_class->missing = v_seq_missing;

  /* Override default sequence import/export... no replacement yet. */
  data_class->import_xml = NULL;
  data_class->export_xml = NULL;
  guppi_FIXME ();

  data_class->read_only = TRUE;
  data_class->copy = v_data_copy;
  data_class->get_size_in_bytes = v_data_size_in_bytes;

  add_arg ("seq_scalar_arg1", GTK_TYPE_POINTER, ARG_SEQ_SCALAR_ARG1);
  add_arg ("seq_scalar_arg2", GTK_TYPE_POINTER, ARG_SEQ_SCALAR_ARG2);
  add_arg ("unary_function", GTK_TYPE_POINTER, ARG_BASIC_UNARY_FN);
  add_arg ("binary_function", GTK_TYPE_POINTER, ARG_BASIC_BINARY_FN);
  add_arg ("unary_function_C", GTK_TYPE_POINTER, ARG_BASIC_UNARY_FN_C);
  add_arg ("binary_function_C", GTK_TYPE_POINTER, ARG_BASIC_BINARY_FN_C);
  add_arg ("function_data", GTK_TYPE_POINTER, ARG_BASIC_FN_USER_DATA);
}

static void
guppi_seq_scalar_func_init (GuppiSeqScalarFunc *obj)
{

}

GtkType guppi_seq_scalar_func_get_type (void)
{
  static GtkType guppi_seq_scalar_func_type = 0;
  if (!guppi_seq_scalar_func_type) {
    static const GtkTypeInfo guppi_seq_scalar_func_info = {
      "GuppiSeqScalarFunc",
      sizeof (GuppiSeqScalarFunc),
      sizeof (GuppiSeqScalarFuncClass),
      (GtkClassInitFunc) guppi_seq_scalar_func_class_init,
      (GtkObjectInitFunc) guppi_seq_scalar_func_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_seq_scalar_func_type =
      gtk_type_unique (GUPPI_TYPE_SEQ_SCALAR,
		       &guppi_seq_scalar_func_info);
  }
  return guppi_seq_scalar_func_type;
}

#if 0
GuppiData *
guppi_seq_scalar_new_func (GuppiFnWrapper *fn, GuppiSeqScalar *arg)
{
  g_return_val_if_fail (fn != NULL && GUPPI_IS_FN_WRAPPER (fn), NULL);
  g_return_val_if_fail (arg != NULL && GUPPI_IS_SEQ_SCALAR (arg), NULL);

  return guppi_data_new_by_type (GUPPI_TYPE_SEQ_SCALAR,
				 GUPPI_TYPE_SEQ_SCALAR_FUNC,
				 "seq_scalar_arg1", arg,
				 "unary_function", fn, NULL);
}

GuppiData *
guppi_seq_scalar_new_binary_func (GuppiFnWrapper *fn,
				  GuppiSeqScalar *arg1,
				  GuppiSeqScalar *arg2)
{
  g_return_val_if_fail (fn != NULL && GUPPI_IS_FN_WRAPPER (fn), NULL);
  g_return_val_if_fail (arg1 != NULL && GUPPI_IS_SEQ_SCALAR (arg1), NULL);
  g_return_val_if_fail (arg2 != NULL && GUPPI_IS_SEQ_SCALAR (arg2), NULL);

  return guppi_data_new_by_type (GUPPI_TYPE_SEQ_SCALAR,
				 GUPPI_TYPE_SEQ_SCALAR_FUNC,
				 "seq_scalar_arg1", arg1,
				 "seq_scalar_arg2", arg2,
				 "binary_function", fn,
				 NULL);
}
#endif



/* $Id$ */
