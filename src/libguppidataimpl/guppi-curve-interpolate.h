/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-curve-interpolate-impl.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_CURVE_INTERPOLATE_H
#define _INC_GUPPI_CURVE_INTERPOLATE_H

#include <gnome.h>
#include <guppi-defs.h>
#include <guppi-curve.h>
#include <guppi-seq.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiCurveInterpolate GuppiCurveInterpolate;
typedef struct _GuppiCurveInterpolateClass GuppiCurveInterpolateClass;

struct _GuppiCurveInterpolate {
  GuppiCurve parent;

  GuppiSeq *x_data;
  GuppiSeq *y_data;
  guint changed_x_handler;
  guint changed_y_handler;
};

struct _GuppiCurveInterpolateClass {
  GuppiCurveClass parent_class;
};

#define GUPPI_TYPE_CURVE_INTERPOLATE (guppi_curve_interpolate_get_type ())
#define GUPPI_CURVE_INTERPOLATE(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_CURVE_INTERPOLATE,GuppiCurveInterpolate))
#define GUPPI_CURVE_INTERPOLATE0(obj) ((obj) ? (GUPPI_CURVE_INTERPOLATE(obj)) : NULL)
#define GUPPI_CURVE_INTERPOLATE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_CURVE_INTERPOLATE,GuppiCurveInterpolateClass))
#define GUPPI_IS_CURVE_INTERPOLATE(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_CURVE_INTERPOLATE))
#define GUPPI_IS_CURVE_INTERPOLATE0(obj) (((obj) == NULL) || (GUPPI_IS_CURVE_INTERPOLATE(obj)))
#define GUPPI_IS_CURVE_INTERPOLATE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_CURVE_INTERPOLATE))

GtkType guppi_curve_interpolate_get_type (void);


END_GUPPI_DECLS;

#endif /* _INC_GUPPI_CURVE_INTERPOLATE_H */

/* $Id$ */
