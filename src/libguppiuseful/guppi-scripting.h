/* $Id$ */

/*
 * guppi-scripting.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_SCRIPTING_H
#define _INC_GUPPI_SCRIPTING_H

/* #include <gtk/gtk.h> */
#include <glib.h>
#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS

enum {
  GUPPI_SCRIPTING_NONE,
  GUPPI_SCRIPTING_GUILE,
  GUPPI_SCRIPTING_PYTHON,
  GUPPI_LAST_SCRIPTING
};

gboolean guppi_supports_guile (void);
gboolean guppi_supports_python (void);

gboolean guppi_file_is_guile_script (const gchar *);
gboolean guppi_file_is_python_script (const gchar *);
gint guppi_identify_script_file (const gchar *);

gint guppi_execute_script (const gchar *);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_SCRIPTING_H */

/* $Id$ */
