/* $Id$ */

/*
 * guppi-python.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Andrew H. Chatham <andrew.chatham@duke.edu> and
 * Jon Trowbridge <trow@gnu.org>.
 * 
 * Portions adapted from gnome-python, originally written by 
 * James Henstridge <james@daa.com.au>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */

#define IN_GUPPI_PYTHON_C
#include <config.h>
#include "guppi-python.h"

#include <stdio.h>
#include <string.h>

/* #include <gnome.h> */
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>

#include <Python.h>

#include "guppi-memory.h"
#include "guppi-rgb.h"
#include "guppi-plug-in-spec.h"

#define BROKEN_PY_INITIALIZE


static void guppi_python_module_init (void);
static PyObject *py_exec_module = NULL;
static PyObject *py_globals = NULL;

gint
guppi_python_eval_str (const gchar * expression)
{
  gchar *copy = guppi_strdup (expression);
  PyObject *ret;

  ret = PyRun_String (copy, Py_single_input, py_globals, py_globals);
  guppi_free (copy);
  
  if (ret == NULL)
    return -1;

  Py_DECREF (ret);
  return 0;
}

gint
guppi_python_eval_file (const gchar * filename)
{
  gchar *copy;
  PyObject *ret, *module, *namespace;
  FILE *fp = fopen (filename, "r");
  if (fp == NULL)
    return 0;
  copy = guppi_strdup (filename);

  module = PyImport_AddModule ("__main__");
  namespace = PyModule_GetDict (module);

  ret = PyRun_File (fp, copy, Py_file_input, namespace, namespace);
  
  if (ret == NULL)
    PyErr_Print ();

  guppi_free (copy);
  fclose (fp);
  
  if (ret == NULL)
    return -1;
  
  Py_DECREF (ret);
  return 0;
}

gboolean
guppi_python_is_active (void)
{
  return Py_IsInitialized ();
}

void
guppi_python_init (void)
{
  gchar *prog_name = "guppi";
  static gboolean init = FALSE;
#ifdef BROKEN_PY_INITIALIZE
  int i;
  extern char **environ;
#endif

  g_return_if_fail (init == FALSE);
  init = TRUE;

#if 0
  guppi_splash_message (_("Initializing"), _("Python"));
#endif

#ifdef BROKEN_PY_INITIALIZE
  /* Python's convertenviron has gotten into its head that it can
     write to the strings in the environment.  We have little choice
     but to allocate a copy of everything. */
  for (i = 0; environ[i]; i++) {
    environ[i] = guppi_strdup (environ[i]);
    guppi_permanent_alloc (environ[i]);
  }
#endif

  Py_SetProgramName (prog_name);
  Py_Initialize ();

  PySys_SetArgv (1, &prog_name);	/* so Python's sys.argv will exist */

  py_exec_module = PyImport_AddModule ("__main__");
  py_globals = PyModule_GetDict (py_exec_module);
  guppi_python_module_init ();
}

PyObject *
gtk_arg2py (GtkArg * x)
{
  if (x == NULL) {
    PyErr_SetString (PyExc_ValueError, "Attempted to convert NULL pointer");
    return NULL;
  }

  switch (GTK_FUNDAMENTAL_TYPE (x->type)) {
  case GTK_TYPE_CHAR:
    return Py_BuildValue ("c", GTK_VALUE_CHAR (*x));
  case GTK_TYPE_UCHAR:
    return Py_BuildValue ("c", (char) GTK_VALUE_UCHAR (*x));
  case GTK_TYPE_BOOL:
    return Py_BuildValue ("i", GTK_VALUE_BOOL (*x));
  case GTK_TYPE_INT:
    return Py_BuildValue ("i", GTK_VALUE_INT (*x));
  case GTK_TYPE_UINT:
    return Py_BuildValue ("l", GTK_VALUE_UINT (*x));
  case GTK_TYPE_FLOAT:
    return Py_BuildValue ("f", GTK_VALUE_FLOAT (*x));
  case GTK_TYPE_DOUBLE:
    return Py_BuildValue ("d", GTK_VALUE_DOUBLE (*x));
  case GTK_TYPE_STRING:
    return Py_BuildValue ("s", GTK_VALUE_STRING (*x));
  default:
    PyErr_SetString (PyExc_ValueError, "Unknown datatype");
    return NULL;
  }
}

GtkArg *
py2argv (PyObject * dict, guint * N)
{
  PyObject *items;
  PyObject *cur;
  PyObject *name;
  PyObject *val;
  guint len = 0, i;
  GtkArg *args;
  GtkArg *this_arg;

  g_return_val_if_fail (N != NULL, NULL);
  g_return_val_if_fail (PyDict_Check (dict), NULL);

  items = PyDict_Items (dict);
  len = PyList_Size (items);
  *N = len;
  args = guppi_new0 (GtkArg, len);

  for (i = 0; i < len; i++) {
    cur = PyList_GetItem (items, i);
    if (cur == NULL) {
      Py_DECREF (items);
      guppi_free (args);
      return NULL;
    }

    name = PyTuple_GetItem (cur, 0);
    val = PyTuple_GetItem (cur, 1);
    this_arg = py2gtk_arg (name, val);

    if (this_arg) {
      gtk_arg_copy (this_arg, &args[i]);
      args[i].name = this_arg->name;
      this_arg->name = NULL;
      gtk_arg_free (this_arg, TRUE);
    }
  }

  return args;
}

GtkArg *
py2gtk_arg (PyObject * name, PyObject * x)
{
  GtkArg *arg = NULL;
  g_return_val_if_fail (PyString_Check (name), NULL);

  if PyInt_Check
    (x) {
    arg = gtk_arg_new (GTK_TYPE_INT);
    GTK_VALUE_INT (*arg) = PyInt_AsLong (x);
    //      PyArg_ParseTuple(x, "i", & GTK_VALUE_INT(*arg));
  } else if (PyFloat_Check (x)) {
    arg = gtk_arg_new (GTK_TYPE_DOUBLE);
    GTK_VALUE_DOUBLE (*arg) = PyFloat_AsDouble (x);
    //      PyArg_ParseTuple(x, "d", & GTK_VALUE_DOUBLE(*arg));
  } else if (PyString_Check (x)) {
    arg = gtk_arg_new (GTK_TYPE_STRING);
    GTK_VALUE_STRING (*arg) = PyString_AsString (x);
    //      PyArg_ParseTuple(x, "s", & GTK_VALUE_STRING(*arg));
  }

  g_return_val_if_fail (arg != NULL, NULL);

  arg->name = PyString_AsString (name);
  return arg;
}

PyObject *
stringv2py (gchar ** strv)
{
  PyObject *tuple;
  PyObject *new;
  int i, len = 0;
  //    gchar *cur;
  if (strv == NULL) {
    Py_INCREF (Py_None);
    return Py_None;
  }
  while (strv[len] != NULL)
    len++;
  tuple = PyTuple_New (len);
  for (i = 0; i < len; i++) {
    new = PyString_FromString (strv[i]);
    PyTuple_SetItem (tuple, i, new);
  }
  return tuple;
}

gchar **
py2stringv (PyObject * obj)
{
  gchar **val = NULL;
  PyObject *cur;
  gint i, len = 0;

  if (PyList_Check (obj)) {

    len = PyList_Size (obj);
    val = guppi_new (gchar *, len + 1);
    val[len] = NULL;

    for (i = 0; i < len; i++) {
      cur = PyList_GetItem (obj, i);
      PyArg_ParseTuple (obj, "s", &val[i]);
    }

  } else if (PyTuple_Check (obj)) {

    len = PyTuple_Size (obj);
    val = guppi_new (gchar *, len + 1);
    val[len] = NULL;

    for (i = 0; i < len; i++) {
      cur = PyTuple_GetItem (obj, i);
      PyArg_ParseTuple (obj, "s", &val[i]);
    }

  }
  return val;
}

PyObject *
color2py (guint32 color)
{
  gint r, g, b, a;
  PyObject *x;

  return PyInt_FromLong (color);
  UINT_TO_RGBA (color, &r, &g, &b, &a);
  x = Py_BuildValue ("(iiii)", r, g, b, a);
  if (x == NULL) {
    Py_INCREF (Py_None);
    return Py_None;
  }
  return x;
}

/* Writes a color object to addr. Invoked in automated code. Has to fit this prototype. */
int
py2color (PyObject * obj, void *addr)
{
  PyObject *tuple = NULL;
  guint32 *ptr = (guint32 *) addr;

  if (PyInt_Check (obj)) {
    gint c;
    c = (gint) PyInt_AsLong (obj);
    *ptr = (c >= 0) ? c : 0;
    return 1;
  }

  if (PyString_Check (obj)) {
    gchar *s;
    gboolean flag;
    GdkColor col;
    s = PyString_AsString (obj);	//borrowed ref
    flag = gdk_color_parse (s, &col);
    *ptr = RGBA_TO_UINT (col.red >> 8, col.green >> 8, col.blue >> 8, 0xff);
    return 1;
  }

  if (PyList_Check (obj)) {
    tuple = PyList_AsTuple (obj);
    obj = tuple;		// so next test will pass
  }
  if (PyTuple_Check (obj)) {
    gint len = 0;
    gint r, g, b, a;

    len = PyTuple_Size (obj);
    a = 0xff;
    if (len == 3)
      if (!PyArg_ParseTuple (obj, "(iii)", &r, &g, &b)) {
	if (tuple) {
	  Py_DECREF (tuple);
	}
	return 0;
      }
    if (len == 4)
      if (!PyArg_ParseTuple (obj, "(iiii)", &r, &g, &b, &a)) {
	if (tuple) {
	  Py_DECREF (tuple);
	}
	return 0;
      }
    if (tuple) {
      Py_DECREF (tuple);
    }
    *ptr = RGBA_TO_UINT (r, g, b, a);
    return 1;
  }
  if (tuple) {
    Py_DECREF (tuple);
  }
  return 0;
}

gboolean
py_compass_check (PyObject * obj)
{
  const gchar *symb = PyString_AsString (obj);
  gboolean rv = (guppi_str2compass (symb) != GUPPI_COMPASS_INVALID);
  return rv;
}

int
py2compass (PyObject * obj, void *addr)
{
  guppi_compass_t *c = (guppi_compass_t *) addr;
  const gchar *symb = PyString_AsString (obj);
  *c = guppi_str2compass (symb);
  return 1;
}

PyObject *
compass2py (guppi_compass_t c)
{
  return PyString_FromString (guppi_compass2str (c));
}


static PyObject *
PyGuppi_New (GtkObject * go)
{
  PyGuppi_Object *self;
  self = (PyGuppi_Object *) PyObject_NEW (PyGuppi_Object, &PyGuppi_Type);
  if (self == NULL)
    return NULL;
  self->obj = go;
  guppi_ref (self->obj);
  /*  gtk_object_sink(self->obj); */
  return (PyObject *) self;

}

static void
PyGuppi_dealloc (PyGuppi_Object * self)
{
  guppi_unref0 (self->obj);
  PyMem_DEL (self);
}



static int
PyGuppi_compare (PyGuppi_Object * self, PyGuppi_Object * v)
{
  if (self->obj == v->obj)
    return 0;
  if (self->obj > v->obj)
    return -1;
  return 1;
}

static long
PyGuppi_hash (PyGuppi_Object * self)
{
  return (long) self->obj;
}

static PyObject *
PyGuppi_repr (PyGuppi_Object * self)
{
  char buf[100];

  sprintf (buf, "<GuppiObject of type %s at %lx>",
	   gtk_type_name (PyGuppi_Get (self)->klass->type),
	   (long) PyGuppi_Get (self));
  return PyString_FromString (buf);
}

static char PyGuppi_Type__doc__[] = "This is the type of Guppi Objects";

static PyTypeObject PyGuppi_Type = {
  PyObject_HEAD_INIT (&PyType_Type)
    0,				/*ob_size */
  "GuppiObject",		/*tp_name */
  sizeof (PyGuppi_Object),	/*tp_basicsize */
  0,				/*tp_itemsize */
  (destructor) PyGuppi_dealloc,	/*tp_dealloc */
  (printfunc) 0,		/*tp_print */
  (getattrfunc) 0,		/*tp_getattr */
  (setattrfunc) 0,		/*tp_setattr */
  (cmpfunc) PyGuppi_compare,	/*tp_compare */
  (reprfunc) PyGuppi_repr,	/*tp_repr */
  0,				/*tp_as_number */
  0,				/*tp_as_sequence */
  0,				/*tp_as_mapping */
  (hashfunc) PyGuppi_hash,	/*tp_hash */
  (ternaryfunc) 0,		/*tp_call */
  (reprfunc) 0,			/*tp_str */
  0L, 0L, 0L, 0L,
  PyGuppi_Type__doc__
};

static gint
PyGuppiEnum_get_value (GtkType enum_type, PyObject * obj, int *val)
{
  if (PyInt_Check (obj)) {
    *val = PyInt_AsLong (obj);
    return 0;
  } else if (PyString_Check (obj)) {
    GtkEnumValue *info = gtk_type_enum_find_value (enum_type,
						   PyString_AsString (obj));
    if (!info) {
      PyErr_SetString (PyExc_TypeError, "couldn't translate string");
      return 1;
    }
    *val = info->value;
    return 0;
  }
  PyErr_SetString (PyExc_TypeError,
		   "enum values must be integers or strings");
  return 1;
}

/* convert the GtkArg array ARGS to a python tuple */
static PyObject *
GtkArgs_AsTuple (int nparams, GtkArg * args)
{
  PyObject *tuple, *item;
  int i;

  if ((tuple = PyTuple_New (nparams)) == NULL)
    return NULL;
  for (i = 0; i < nparams; i++) {
    item = GtkArg_AsPyObject (&args[i]);
    if (item == NULL) {
      Py_INCREF (Py_None);
      item = Py_None;
    }
    PyTuple_SetItem (tuple, i, item);
  }
  return tuple;
}

/* converts a Python sequence to a GtkArg array.  Returns -1 if the sequence
 * doesn't match the specification in ARGS */
static int
GtkArgs_FromSequence (GtkArg * args, int nparams, PyObject * seq)
{
  PyObject *item;
  int i;
  if (!PySequence_Check (seq))
    return -1;
  for (i = 0; i < nparams; i++) {
    item = PySequence_GetItem (seq, i);
    Py_DECREF (item);
    if (GtkArg_FromPyObject (&args[i], item)) {
      gchar buf[512];
      if (args[i].name == NULL)
	g_snprintf (buf, 511, "argument %d: expected %s, %s found", i + 1,
		    gtk_type_name (args[i].type), item->ob_type->tp_name);
      else
	g_snprintf (buf, 511, "argument %s: expected %s, %s found",
		    args[i].name, gtk_type_name (args[i].type),
		    item->ob_type->tp_name);
      PyErr_SetString (PyExc_TypeError, buf);
      return -1;
    }
  }
  return 0;
}


/* create a GtkArg from a PyObject, using the GTK_VALUE_* routines.
 * returns -1 if it couldn't set the argument. */
static int
GtkArg_FromPyObject (GtkArg * arg, PyObject * obj)
{
  PyObject *tmp;
  switch (GTK_FUNDAMENTAL_TYPE (arg->type)) {
  case GTK_TYPE_NONE:
  case GTK_TYPE_INVALID:
    GTK_VALUE_INT (*arg) = 0;
    break;
  case GTK_TYPE_BOOL:
    if ((tmp = PyNumber_Int (obj)))
      GTK_VALUE_BOOL (*arg) = (PyInt_AsLong (tmp) != 0);
    else {
      PyErr_Clear ();
      return -1;
    }
    Py_DECREF (tmp);
    break;
  case GTK_TYPE_CHAR:
  case GTK_TYPE_UCHAR:
    if ((tmp = PyObject_Str (obj)))
      GTK_VALUE_CHAR (*arg) = PyString_AsString (tmp)[0];
    else {
      PyErr_Clear ();
      return -1;
    }
    Py_DECREF (tmp);
    break;
  case GTK_TYPE_ENUM:
    if (PyGuppiEnum_get_value (arg->type, obj, &(GTK_VALUE_ENUM (*arg))))
      return -1;
    break;
  case GTK_TYPE_INT:
    if ((tmp = PyNumber_Int (obj)))
      GTK_VALUE_INT (*arg) = PyInt_AsLong (tmp);
    else {
      PyErr_Clear ();
      return -1;
    }
    Py_DECREF (tmp);
    break;
  case GTK_TYPE_UINT:
    if ((tmp = PyNumber_Int (obj)))
      GTK_VALUE_UINT (*arg) = PyInt_AsLong (tmp);
    else {
      PyErr_Clear ();
      return -1;
    }
    Py_DECREF (tmp);
    break;
  case GTK_TYPE_LONG:
    if ((tmp = PyNumber_Int (obj)))
      GTK_VALUE_LONG (*arg) = PyInt_AsLong (tmp);
    else {
      PyErr_Clear ();
      return -1;
    }
    Py_DECREF (tmp);
    break;
  case GTK_TYPE_ULONG:
    if ((tmp = PyNumber_Int (obj)))
      GTK_VALUE_ULONG (*arg) = PyInt_AsLong (tmp);
    else {
      PyErr_Clear ();
      return -1;
    }
    Py_DECREF (tmp);
    break;
  case GTK_TYPE_FLOAT:
    if ((tmp = PyNumber_Float (obj)))
      GTK_VALUE_FLOAT (*arg) = PyFloat_AsDouble (tmp);
    else {
      PyErr_Clear ();
      return -1;
    }
    Py_DECREF (tmp);
    break;
  case GTK_TYPE_DOUBLE:
    if ((tmp = PyNumber_Float (obj)))
      GTK_VALUE_DOUBLE (*arg) = PyFloat_AsDouble (tmp);
    else {
      PyErr_Clear ();
      return -1;
    }
    Py_DECREF (tmp);
    break;
  case GTK_TYPE_STRING:
    if ((tmp = PyObject_Str (obj)))
      GTK_VALUE_STRING (*arg) = PyString_AsString (tmp);
    else {
      PyErr_Clear ();
      return -1;
    }
    Py_DECREF (tmp);
    break;
  case GTK_TYPE_OBJECT:
    if (PyGuppi_Check (obj))
      GTK_VALUE_OBJECT (*arg) = PyGuppi_Get (obj);
    else
      return -1;
    break;
  case GTK_TYPE_POINTER:
    if (PyCObject_Check (obj))
      GTK_VALUE_BOXED (*arg) = PyCObject_AsVoidPtr (obj);
    else
      return -1;
    break;
  case GTK_TYPE_SIGNAL:
  case GTK_TYPE_ARGS:
  case GTK_TYPE_C_CALLBACK:
    fprintf (stderr, "unsupported type");
    g_assert_not_reached ();
    return -1;
  default:
    g_assert_not_reached ();
    break;
  }
  return 0;
}

/* convert a GtkArg structure to a PyObject, using the GTK_VALUE_* routines.
 * conversion is always possible. */
static PyObject *
GtkArg_AsPyObject (GtkArg * arg)
{
  switch (GTK_FUNDAMENTAL_TYPE (arg->type)) {
  case GTK_TYPE_INVALID:
  case GTK_TYPE_NONE:
    Py_INCREF (Py_None);
    return Py_None;
  case GTK_TYPE_CHAR:
  case GTK_TYPE_UCHAR:
    return PyString_FromStringAndSize (&GTK_VALUE_CHAR (*arg), 1);
  case GTK_TYPE_BOOL:
    return PyInt_FromLong (GTK_VALUE_BOOL (*arg));
  case GTK_TYPE_ENUM:
  case GTK_TYPE_FLAGS:
  case GTK_TYPE_INT:
    return PyInt_FromLong (GTK_VALUE_INT (*arg));
  case GTK_TYPE_UINT:
    return PyInt_FromLong (GTK_VALUE_UINT (*arg));
  case GTK_TYPE_LONG:
    return PyInt_FromLong (GTK_VALUE_LONG (*arg));
  case GTK_TYPE_ULONG:
    return PyInt_FromLong (GTK_VALUE_ULONG (*arg));
  case GTK_TYPE_FLOAT:
    return PyFloat_FromDouble (GTK_VALUE_FLOAT (*arg));
  case GTK_TYPE_DOUBLE:
    return PyFloat_FromDouble (GTK_VALUE_DOUBLE (*arg));
  case GTK_TYPE_STRING:
    if (GTK_VALUE_STRING (*arg) != NULL)
      return PyString_FromString (GTK_VALUE_STRING (*arg));
    else {
      Py_INCREF (Py_None);
      return Py_None;
    }
  case GTK_TYPE_ARGS:
    return GtkArgs_AsTuple (GTK_VALUE_ARGS (*arg).n_args,
			    GTK_VALUE_ARGS (*arg).args);
  case GTK_TYPE_OBJECT:
    if (GTK_VALUE_OBJECT (*arg) != NULL)
      return PyGuppi_New (GTK_VALUE_OBJECT (*arg));
    else {
      Py_INCREF (Py_None);
      return Py_None;
    }
  case GTK_TYPE_POINTER:
    return PyCObject_FromVoidPtr (GTK_VALUE_POINTER (*arg), NULL);
  case GTK_TYPE_FOREIGN:
    Py_INCREF ((PyObject *) GTK_VALUE_FOREIGN (*arg).data);
    return (PyObject *) GTK_VALUE_FOREIGN (*arg).data;
  case GTK_TYPE_CALLBACK:
    Py_INCREF ((PyObject *) GTK_VALUE_CALLBACK (*arg).data);
    return (PyObject *) GTK_VALUE_CALLBACK (*arg).data;
  case GTK_TYPE_SIGNAL:
    Py_INCREF ((PyObject *) GTK_VALUE_SIGNAL (*arg).d);
    return (PyObject *) GTK_VALUE_SIGNAL (*arg).d;
  default:
    g_assert_not_reached ();
    break;
  }
  return NULL;
}

/* set a GtkArg structure's data from a PyObject, using the GTK_RETLOC_*
 * routines.  If it can't make the conversion, set the return to a zero
 * equivalent. */
static void
GtkRet_FromPyObject (GtkArg * ret, PyObject * py_ret)
{
  PyObject *tmp;
  switch (GTK_FUNDAMENTAL_TYPE (ret->type)) {
  case GTK_TYPE_NONE:
  case GTK_TYPE_INVALID:
    break;
  case GTK_TYPE_BOOL:
    if ((tmp = PyNumber_Int (py_ret))) {
      *GTK_RETLOC_BOOL (*ret) = (PyInt_AsLong (tmp) != 0);
      Py_DECREF (tmp);
    } else {
      PyErr_Clear ();
      *GTK_RETLOC_BOOL (*ret) = FALSE;
    }
    break;
  case GTK_TYPE_CHAR:
    if ((tmp = PyObject_Str (py_ret))) {
      *GTK_RETLOC_CHAR (*ret) = PyString_AsString (tmp)[0];
      Py_DECREF (tmp);
    } else {
      PyErr_Clear ();
      *GTK_RETLOC_CHAR (*ret) = '\0';
    }
    break;
  case GTK_TYPE_ENUM:
    if (PyGuppiEnum_get_value (ret->type, py_ret, GTK_RETLOC_ENUM (*ret))) {
      PyErr_Clear ();
      *GTK_RETLOC_ENUM (*ret) = 0;
    }
    break;
  case GTK_TYPE_INT:
    if ((tmp = PyNumber_Int (py_ret))) {
      *GTK_RETLOC_INT (*ret) = PyInt_AsLong (tmp);
      Py_DECREF (tmp);
    } else {
      PyErr_Clear ();
      *GTK_RETLOC_INT (*ret) = 0;
    }
    break;
  case GTK_TYPE_UINT:
    if ((tmp = PyNumber_Int (py_ret))) {
      *GTK_RETLOC_UINT (*ret) = PyInt_AsLong (tmp);
      Py_DECREF (tmp);
    } else {
      PyErr_Clear ();
      *GTK_RETLOC_UINT (*ret) = 0;
    }
    break;
  case GTK_TYPE_LONG:
    if ((tmp = PyNumber_Int (py_ret))) {
      *GTK_RETLOC_LONG (*ret) = PyInt_AsLong (tmp);
      Py_DECREF (tmp);
    } else {
      PyErr_Clear ();
      *GTK_RETLOC_LONG (*ret) = 0;
    }
    break;
  case GTK_TYPE_ULONG:
    if ((tmp = PyNumber_Int (py_ret))) {
      *GTK_RETLOC_ULONG (*ret) = PyInt_AsLong (tmp);
      Py_DECREF (tmp);
    } else {
      PyErr_Clear ();
      *GTK_RETLOC_ULONG (*ret) = 0;
    }
    break;
  case GTK_TYPE_FLOAT:
    if ((tmp = PyNumber_Float (py_ret))) {
      *GTK_RETLOC_FLOAT (*ret) = PyFloat_AsDouble (tmp);
      Py_DECREF (tmp);
    } else {
      PyErr_Clear ();
      *GTK_RETLOC_FLOAT (*ret) = 0;
    }
    break;
  case GTK_TYPE_DOUBLE:
    if ((tmp = PyNumber_Float (py_ret))) {
      *GTK_RETLOC_DOUBLE (*ret) = PyFloat_AsDouble (tmp);
      Py_DECREF (tmp);
    } else {
      PyErr_Clear ();
      *GTK_RETLOC_DOUBLE (*ret) = 0;
    }
    break;
  case GTK_TYPE_STRING:
    if ((tmp = PyObject_Str (py_ret))) {
      *GTK_RETLOC_STRING (*ret) = PyString_AsString (py_ret);
      Py_DECREF (tmp);
    } else {
      PyErr_Clear ();
      *GTK_RETLOC_STRING (*ret) = NULL;
    }
    break;
  case GTK_TYPE_OBJECT:
    if (PyGuppi_Check (py_ret))
      *GTK_RETLOC_OBJECT (*ret) = PyGuppi_Get (py_ret);
    else
      *GTK_RETLOC_OBJECT (*ret) = NULL;
    break;
  case GTK_TYPE_POINTER:
    if (PyCObject_Check (py_ret))
      *GTK_RETLOC_POINTER (*ret) = PyCObject_AsVoidPtr (py_ret);
    else
      *GTK_RETLOC_POINTER (*ret) = NULL;
    break;
  default:
    g_assert_not_reached ();
    break;
  }
}

/* convert a GtkArg structure to a PyObject, using the GTK_RETLOC_* routines.
 * conversion is always possible. */
static PyObject *
GtkRet_AsPyObject (GtkArg * arg)
{
  switch (GTK_FUNDAMENTAL_TYPE (arg->type)) {
  case GTK_TYPE_INVALID:
  case GTK_TYPE_NONE:
    Py_INCREF (Py_None);
    return Py_None;
  case GTK_TYPE_CHAR:
    return PyString_FromStringAndSize (GTK_RETLOC_CHAR (*arg), 1);
  case GTK_TYPE_BOOL:
    return PyInt_FromLong (*GTK_RETLOC_BOOL (*arg));
  case GTK_TYPE_ENUM:
  case GTK_TYPE_FLAGS:
  case GTK_TYPE_INT:
    return PyInt_FromLong (*GTK_RETLOC_INT (*arg));
  case GTK_TYPE_UINT:
    return PyInt_FromLong (*GTK_RETLOC_UINT (*arg));
  case GTK_TYPE_LONG:
    return PyInt_FromLong (*GTK_RETLOC_LONG (*arg));
  case GTK_TYPE_ULONG:
    return PyInt_FromLong (*GTK_RETLOC_ULONG (*arg));
  case GTK_TYPE_FLOAT:
    return PyFloat_FromDouble (*GTK_RETLOC_FLOAT (*arg));
  case GTK_TYPE_DOUBLE:
    return PyFloat_FromDouble (*GTK_RETLOC_DOUBLE (*arg));
  case GTK_TYPE_STRING:
    if (*GTK_RETLOC_STRING (*arg) != NULL)
      return PyString_FromString (*GTK_RETLOC_STRING (*arg));
    else {
      Py_INCREF (Py_None);
      return Py_None;
    }
  case GTK_TYPE_ARGS:
    break;
  case GTK_TYPE_OBJECT:
    return PyGuppi_New (*GTK_RETLOC_OBJECT (*arg));
  case GTK_TYPE_POINTER:
    return PyCObject_FromVoidPtr (*GTK_RETLOC_POINTER (*arg), NULL);
  default:
    g_assert_not_reached ();
    break;
  }
  return NULL;
}

static GtkArg *
PyDict_AsGtkArgs (PyObject * dict, GtkType type, gint * nargs)
{
  PyObject *key, *item;
  gint i = 0, pos;
  GtkArg *arg;
  GtkArgInfo *info;
  gchar *err, buf[128];

  gtk_type_class (type);	/* initialise the class structure (and setup args) */
  *nargs = PyDict_Size (dict);
  arg = guppi_new (GtkArg, *nargs);
  pos = 0;
  while (PyDict_Next (dict, &i, &key, &item)) {
    if (!PyString_Check (key)) {
      PyErr_SetString (PyExc_TypeError, "dictionary keys must be strings");
      guppi_free (arg);
      return NULL;
    }
    arg[pos].name = PyString_AsString (key);
    err = gtk_object_arg_get_info (type, arg[pos].name, &info);
    if (info == NULL) {
      PyErr_SetString (PyExc_TypeError, err);
      guppi_free (err);
      guppi_free (arg);
      return NULL;
    }
    arg[pos].type = info->type;
    arg[pos].name = info->name;
    if (GtkArg_FromPyObject (&(arg[pos]), item)) {
      g_snprintf (buf, 255, "arg %s: expected type %s, found %s",
		  arg[pos].name, gtk_type_name (arg[pos].type),
		  item->ob_type->tp_name);
      PyErr_SetString (PyExc_TypeError, buf);
      guppi_free (arg);
      return NULL;
    }
    pos++;
  }
  return arg;
}


static struct _PyGuppi_FunctionStruct functions = {
  VERSION,
  FALSE,

  GtkArgs_AsTuple,
  GtkArgs_FromSequence,
  GtkArg_FromPyObject,
  GtkArg_AsPyObject,
  GtkRet_FromPyObject,
  GtkRet_AsPyObject,
  PyDict_AsGtkArgs,

  PyGuppiEnum_get_value,

  &PyGuppi_Type, PyGuppi_New,
};

static PyObject *
guppi_python_type_name (PyObject * self, PyObject * args)
{
  gchar *name;
  PyObject *obj;

  if (!PyArg_ParseTuple
      (args, "O!:guppi_python_type_name", &PyGuppi_Type, &obj)) return NULL;

  if (!PyGuppi_Check (obj)) {
    PyErr_SetString (PyExc_TypeError, "Can only be used on Guppi objects.");
    return NULL;
  }

  name = gtk_type_name (PyGuppi_Get (obj)->klass->type);

  if (name == NULL) {
    PyErr_SetString (PyExc_ValueError, "Not a valid Guppi Type.");
    return NULL;
  }
  obj = PyString_FromString (name);
  return obj;
}

static PyObject *py_registered_classes = NULL;	//set in module_init

/* This basically just does dict2.update(dict2) in C. It's in C so I
   can keep it all in the C-implemented _guppi module. */

static PyObject *
guppi_python_register_classes (PyObject * self, PyObject * args)
{
  PyObject *dict, *items, *tuple, *key, *value;
  gint len, i;

  if (!PyArg_ParseTuple (args, "O:register_classes", &dict))
    return NULL;
  if (!PyDict_Check (dict)) {
    Py_DECREF (dict);
    PyErr_SetString (PyExc_ValueError, "Expected a dictionary");
    return NULL;
  }
  items = PyDict_Items (dict);
  Py_DECREF (dict);
  len = PyList_Size (items);

  for (i = 0; i < len; i++) {
    tuple = PyList_GetItem (items, i);	//borrowed reference
    key = PyTuple_GetItem (tuple, 0);
    value = PyTuple_GetItem (tuple, 1);
    PyDict_SetItem (py_registered_classes, key, value);
  }
  Py_DECREF (items);
  Py_INCREF (Py_None);
  return Py_None;
}

static PyObject *
guppi_python_load_plug_in (PyObject * self, PyObject * args)
{
  char *type, *code;

  if (!PyArg_ParseTuple (args, "ss", &type, &code))
    return NULL;

  if (!guppi_plug_in_exists (type, code)) {
    PyErr_SetString (PyExc_ValueError, "Plug-in does not exist");
    return NULL;
  }

  if (guppi_plug_in_is_loaded (type, code))
    return Py_None;

  guppi_plug_in_force_load (type, code);
  return Py_None;
}

void init_guppi_py (void);

static PyMethodDef _guppimoduleMethods[] = {
  {"type_name", guppi_python_type_name, METH_VARARGS},
  {"register_classes", guppi_python_register_classes, METH_VARARGS},
  {"load_plug_in", guppi_python_load_plug_in, METH_VARARGS},
  {NULL, NULL}
};

static
  void
guppi_python_module_init (void)
{
  PyObject *m, *d, *private;
  PyObject *py_ret;

  _PyGuppi_API = &functions;
  m = Py_InitModule ("_guppi", _guppimoduleMethods);
  d = PyModule_GetDict (m);

  PyDict_SetItemString (d, "GuppiObjectType", (PyObject *) & PyGuppi_Type);
  PyDict_SetItemString (d, "_PyGuppi_API",
			PyCObject_FromVoidPtr (&functions, NULL));
  py_registered_classes = PyDict_New ();
  PyDict_SetItemString (d, "classes", py_registered_classes);

  private = PyDict_New ();
  PyDict_SetItemString (d, "_private", private);
  Py_DECREF (private);
  PyDict_SetItemString (private, "PyGuppi_New",
			d = PyCObject_FromVoidPtr (PyGuppi_New, NULL));
  Py_DECREF (d);
  PyDict_SetItemString (private, "GtkArgs_AsTuple",
			d = PyCObject_FromVoidPtr (GtkArgs_AsTuple, NULL));
  Py_DECREF (d);
  PyDict_SetItemString (private, "GtkArgs_FromSequence",
			d =
			PyCObject_FromVoidPtr (GtkArgs_FromSequence, NULL));
  Py_DECREF (d);
  PyDict_SetItemString (private, "GtkArg_FromPyObject",
			d =
			PyCObject_FromVoidPtr (GtkArg_FromPyObject, NULL));
  Py_DECREF (d);
  PyDict_SetItemString (private, "GtkArg_AsPyObject",
			d = PyCObject_FromVoidPtr (GtkArg_AsPyObject, NULL));
  Py_DECREF (d);
  PyDict_SetItemString (private, "GtkRet_FromPyObject",
			d =
			PyCObject_FromVoidPtr (GtkRet_FromPyObject, NULL));
  Py_DECREF (d);
  PyDict_SetItemString (private, "GtkRet_AsPyObject",
			d = PyCObject_FromVoidPtr (GtkRet_AsPyObject, NULL));
  Py_DECREF (d);
  PyDict_SetItemString (private, "PyGuppiEnum_get_value",
			d =
			PyCObject_FromVoidPtr (PyGuppiEnum_get_value, NULL));
  Py_DECREF (d);
  PyDict_SetItemString (private, "PyDict_AsGtkArgs",
			d = PyCObject_FromVoidPtr (PyDict_AsGtkArgs, NULL));
  Py_DECREF (d);

  m = PyImport_ImportModule ("os");
  if (m == NULL) {
    Py_FatalError ("couldn't import os");
    return;
  }
  d = PyModule_GetDict (m);

  d = PyDict_GetItemString (d, "environ");
  d = PyMapping_GetItemString (d, "PYGUPPI_FATAL_EXCEPTIONS");
  if (d == NULL)
    PyErr_Clear ();
  else {
    functions.fatalExceptions = PyGuppi_FatalExceptions = PyObject_IsTrue (d);
  }

  if (PyErr_Occurred ())
    Py_FatalError ("can't initialise module _guppi");

  py_ret = PyRun_String ("import sys\nsys.path.append(\"" \
			 GUPPI_SCRIPTS "/python\")\n",
			 Py_single_input, py_globals, py_globals);
  if (py_ret == NULL)
    g_warning ("Error setting Python path\n");
  else
    Py_DECREF (py_ret);
}

/**********************************************************************/

static double
guppi_python_safe_apply (PyObject * proc, PyObject * objs)
{
  PyObject *rv;
  double ret;

  if (proc != NULL && PyCallable_Check (proc))
    rv = PyObject_CallObject (proc, objs);
  else {
    PyErr_SetString (PyExc_ValueError, "uncallable object");
    rv = NULL;
  }

  if (rv == NULL) {
    PyErr_Print ();
    PyErr_Clear ();
    ret = 0;
  } else if (!PyFloat_Check (rv)) {
    ret = 0;
  } else {
    ret = PyFloat_AsDouble (rv);
  }

  Py_DECREF (objs);
  Py_XDECREF (rv);
  return ret;
}

static double
py_eval_d__i (gint i, gpointer py_proc)
{
  PyObject *py_args;

  py_args = Py_BuildValue ("i", i);
  return guppi_python_safe_apply ((PyObject *) py_proc, py_args);
}

static double
py_eval_d__i_d (gint i, double x, gpointer py_proc)
{
  PyObject *py_args;

  py_args = Py_BuildValue ("id", i, x);
  return guppi_python_safe_apply ((PyObject *) py_proc, py_args);
}

static double
py_eval_d__d (double x, gpointer py_proc)
{
  PyObject *py_args;

  py_args = Py_BuildValue ("d", x);
  return guppi_python_safe_apply ((PyObject *) py_proc, py_args);
}

static double
py_eval_d__d_d (double x, double y, gpointer py_proc)
{
  PyObject *py_args;

  py_args = Py_BuildValue ("dd", x, y);
  return guppi_python_safe_apply ((PyObject *) py_proc, py_args);
}

/**********************************************************************/

GuppiFnWrapper *
py2fn_d__i (PyObject * proc)
{
  g_return_val_if_fail (PyCallable_Check (proc), NULL);
  Py_INCREF (proc);		// FIXME: When do I do the decref?
  return guppi_fn_wrapper_new_d__i (py_eval_d__i, (gpointer) proc);
}

GuppiFnWrapper *
py2fn_d__i_d (PyObject * proc)
{
  g_return_val_if_fail (PyCallable_Check (proc), NULL);
  Py_INCREF (proc);
  return guppi_fn_wrapper_new_d__i_d (py_eval_d__i_d, (gpointer) proc);
}

GuppiFnWrapper *
py2fn_d__d (PyObject * proc)
{
  g_return_val_if_fail (PyCallable_Check (proc), NULL);
  Py_INCREF (proc);
  return guppi_fn_wrapper_new_d__d (py_eval_d__d, (gpointer) proc);
}

GuppiFnWrapper *
py2fn_d__d_d (PyObject * proc)
{
  g_return_val_if_fail (PyCallable_Check (proc), NULL);
  Py_INCREF (proc);
  return guppi_fn_wrapper_new_d__d_d (py_eval_d__d_d, (gpointer) proc);
}

/* $Id$ */
