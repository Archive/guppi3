/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-config-model.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_CONFIG_MODEL_H
#define _INC_GUPPI_CONFIG_MODEL_H

#include <gnome.h>
#include <glade/glade.h>
#include "guppi-attribute-bag.h"
#include "guppi-defs.h"

BEGIN_GUPPI_DECLS;

typedef struct _GuppiConfigModel GuppiConfigModel;
typedef struct _GuppiConfigModelPrivate GuppiConfigModelPrivate;
typedef struct _GuppiConfigModelClass GuppiConfigModelClass;

typedef enum {
  GUPPI_CONFIG_APPEARANCE  = 1<<0,
  GUPPI_CONFIG_DATA        = 1<<1,
  GUPPI_CONFIG_PREFERENCES = 1<<2,
  GUPPI_CONFIG_LAST        = 1<<3
} GuppiConfigType;


typedef GtkWidget *(*GuppiConfigFn) (gpointer config_closure);
typedef void (*GuppiConfigIteratorFn) (const gchar *major_label, const gchar *minor_label,
				       GuppiConfigType type, GtkWidget *w,
				       gpointer closure);

struct _GuppiConfigModelPrivate;

struct _GuppiConfigModel {
  GtkObject parent;
  GuppiConfigModelPrivate *priv;
};

struct _GuppiConfigModelClass {
  GtkObjectClass parent_class;
};

#define GUPPI_TYPE_CONFIG_MODEL (guppi_config_model_get_type ())
#define GUPPI_CONFIG_MODEL(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_CONFIG_MODEL,GuppiConfigModel))
#define GUPPI_CONFIG_MODEL0(obj) ((obj) ? (GUPPI_CONFIG_MODEL(obj)) : NULL)
#define GUPPI_CONFIG_MODEL_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_CONFIG_MODEL,GuppiConfigModelClass))
#define GUPPI_IS_CONFIG_MODEL(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_CONFIG_MODEL))
#define GUPPI_IS_CONFIG_MODEL0(obj) (((obj) == NULL) || (GUPPI_IS_CONFIG_MODEL(obj)))
#define GUPPI_IS_CONFIG_MODEL_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_CONFIG_MODEL))

GtkType guppi_config_model_get_type (void);

GuppiConfigModel *guppi_config_model_new (void);

void guppi_config_model_add (GuppiConfigModel *,
			     const gchar *major_label, const gchar *minor_label,
			     GuppiConfigType type,
			     GuppiAttributeBag *bag, /* A bag to bind to any attribute widgets */
			     GuppiConfigFn config_fn, gpointer config_closure,
			     GtkDestroyNotify config_closure_destructor);

void guppi_config_model_add_glade_file (GuppiConfigModel *,
					const gchar *major_label, const gchar *minor_label,
					GuppiConfigType type, GuppiAttributeBag *bag,
					const gchar *filename, const gchar *root,
					void (*glade_init) (GladeXML *, gpointer),
					gpointer glade_init_closure,
					GtkDestroyNotify glade_init_closure_destroy);

void guppi_config_model_combine (GuppiConfigModel *, const gchar *major_label_override, GuppiConfigModel *);

void guppi_config_model_foreach (GuppiConfigModel *, GuppiConfigIteratorFn iter_fn, gpointer closure);

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_CONFIG_MODEL_H */

/* $Id$ */
