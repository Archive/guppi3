/* $Id$ */

/*
 * guppi-debug.h
 *
 * Copyright (C) 1999, 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 * and Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_DEBUG_H
#define _INC_GUPPI_DEBUG_H

/* #include <gtk/gtk.h> */

#include  "guppi-defs.h"
#include <glib.h>

BEGIN_GUPPI_DECLS

#undef TRACK_ENTRY_AND_EXIT

enum {
  GUPPI_SILENT = 0,
  GUPPI_NORMAL_VERBOSITY = 1,
  GUPPI_VERBOSE = 2,
  GUPPI_VERY_VERBOSE = 3
};

gint guppi_verbosity (void);
void guppi_set_verbosity (gint);

gboolean guppi_is_not_silent (void);
gboolean guppi_is_verbose (void);
gboolean guppi_is_very_verbose (void);

void _guppi_debug_corev (gint verbosity,
			 const gchar * log_domain, GLogLevelFlags log_level,
			 const gchar * file, gint line, const gchar * func,
			 const gchar * format, va_list args);

void _guppi_debug_core (gint verbosity,
			const gchar * log_domain, GLogLevelFlags log_level,
			const gchar * file, gint line, const gchar * func,
			const gchar * format, ...);


#ifdef __GNUC__

#define guppi_msg(format, args...) \
{ if (guppi_is_not_silent()) { g_message(format, ##args); } }

#define guppi_msg_v(format, args...) \
{ if (guppi_is_verbose()) { g_message(format, ##args); } }

#define guppi_msg_vv(format, args...) \
{ if (guppi_is_very_verbose()) { g_message(format, ##args); } }

#define guppi_debug(format, args...) \
{ if (guppi_is_not_silent()) { _guppi_debug_core(GUPPI_NORMAL_VERBOSITY, G_LOG_DOMAIN, G_LOG_LEVEL_MESSAGE, __FILE__, __LINE__, __PRETTY_FUNCTION__, format, ##args); }}

#define guppi_debug_v(format, args...) \
{ if (guppi_is_verbose()) { _guppi_debug_core(GUPPI_VERBOSE, G_LOG_DOMAIN, G_LOG_LEVEL_MESSAGE, __FILE__, __LINE__, __PRETTY_FUNCTION__, format, ##args); }}

#define guppi_debug_vv(format, args...) \
{ if (guppi_is_very_verbose()) { _guppi_debug_core(GUPPI_VERY_VERBOSE, G_LOG_DOMAIN, G_LOG_LEVEL_MESSAGE, __FILE__, __LINE__, __PRETTY_FUNCTION__, format, ##args); }}

#else /* !__GNUC__ */

/* Make these a no-op for now */
static void
guppi_msg (const gchar * format, ...)
{
}
static void
guppi_msg_v (const gchar * format, ...)
{
}
static void
guppi_msg_vv (const gchar * format, ...)
{
}
static void
guppi_debug (const gchar * format, ...)
{
}
static void
guppi_debug_v (const gchar * format, ...)
{
}
static void
guppi_debug_vv (const gchar * format, ...)
{
}

#endif

void guppi_set_synchronous (void);
gboolean guppi_is_synchronous (void);

void guppi_FIXME(void);

#define guppi_unimplemented_function_dialog(func) \
  _guppi_unimplemented_function_dialog(func, __FILE__, __LINE__)

void _guppi_unimplemented_function_dialog (const gchar * function,
					   const gchar * filename, gint line);


#ifdef TRACK_ENTRY_AND_EXIT

#define GUPPI_ENTER_MESSAGE(msg) \
{ if (guppi_is_very_verbose()) _guppi_track_entry(__FILE__,__PRETTY_FUNCTION__,__LINE__, msg);}
#define GUPPI_EXIT_MESSAGE(msg) \
{ if (guppi_is_very_verbose()) _guppi_track_exit(__FILE__,__PRETTY_FUNCTION__,__LINE__, msg);}

#define GUPPI_ENTER GUPPI_ENTER_MESSAGE(NULL)
#define GUPPI_EXIT GUPPI_EXIT_MESSAGE(NULL)

#else

#define GUPPI_ENTER_MESSAGE(x)
#define GUPPI_EXIT_MESSAGE(x)
#define GUPPI_ENTER
#define GUPPI_EXIT

#endif

void _guppi_track_entry (const gchar * func, const gchar * filename,
			 gint line, const gchar * msg);
void _guppi_track_exit (const gchar * func, const gchar * filename, gint line,
			const gchar * msg);

void guppi_stop_in_debugger (void);
void guppi_make_warnings_and_criticals_stop_in_debugger (const char
							 *first_domain, ...);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_DEBUG_H */

/* $Id$ */
