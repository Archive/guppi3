/* $Id$ */

/*
 * guppi-scripting.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-scripting.h"

/* #include <gnome.h> */
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-util.h>

#include <guppi-guile.h>
#ifdef HAVE_PYTHON
#include <guppi-python.h>
#endif


gboolean
guppi_supports_guile (void)
{
  return guppi_guile_is_active ();
}

gboolean
guppi_supports_python (void)
{
#ifdef HAVE_PYTHON
  return guppi_python_is_active ();
#else
  return FALSE;
#endif
}

gboolean
guppi_file_is_guile_script (const gchar * filename)
{
  const gchar *ext;

  g_return_val_if_fail (filename != NULL, FALSE);

  if (!g_file_exists (filename))
    return FALSE;

  ext = g_extension_pointer (filename);
  if (g_strcasecmp (ext, "scm") == 0 || g_strcasecmp (ext, "guile") == 0)
    return TRUE;

  return FALSE;
}

gboolean
guppi_file_is_python_script (const gchar * filename)
{
  const gchar *ext;

  g_return_val_if_fail (filename != NULL, FALSE);

  if (!g_file_exists (filename))
    return FALSE;

  ext = g_extension_pointer (filename);
  if (g_strcasecmp (ext, "py") == 0)
    return TRUE;

  return FALSE;
}

gint
guppi_identify_script_file (const gchar * filename)
{
  if (guppi_file_is_guile_script (filename))
    return GUPPI_SCRIPTING_GUILE;

  if (guppi_file_is_python_script (filename))
    return GUPPI_SCRIPTING_PYTHON;

  return GUPPI_SCRIPTING_NONE;
}

gint
guppi_execute_script (const gchar * filename)
{
  g_return_val_if_fail (filename != NULL, GUPPI_SCRIPTING_NONE);

  if (!g_file_exists (filename))
    return GUPPI_SCRIPTING_NONE;

  if (guppi_supports_guile () && guppi_file_is_guile_script (filename)) {
    guppi_safe_load ((gchar *) filename);
    return GUPPI_SCRIPTING_GUILE;
  }

  if (guppi_supports_python () && guppi_file_is_python_script (filename)) {
#ifdef HAVE_PYTHON
    guppi_python_eval_file (filename);
#else
    g_assert_not_reached ();
#endif
    return GUPPI_SCRIPTING_PYTHON;
  }

  return GUPPI_SCRIPTING_NONE;
}




/* $Id$ */
