/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-attribute-widget.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_ATTRIBUTE_WIDGET_H
#define _INC_GUPPI_ATTRIBUTE_WIDGET_H

#include <gnome.h>
#include  "guppi-defs.h"

#include "guppi-attribute-flavor.h"
#include "guppi-attribute-bag.h"

BEGIN_GUPPI_DECLS;

typedef struct _GuppiAttributeWidget GuppiAttributeWidget;
typedef struct _GuppiAttributeWidgetPrivate GuppiAttributeWidgetPrivate;
typedef struct _GuppiAttributeWidgetClass GuppiAttributeWidgetClass;

struct _GuppiAttributeWidget {
  GtkTable parent;
  GuppiAttributeWidgetPrivate *priv;
};

struct _GuppiAttributeWidgetClass {
  GtkHBoxClass parent_class;

  void (*bag_to_widget) (GuppiAttributeWidget *);
};

#define GUPPI_TYPE_ATTRIBUTE_WIDGET (guppi_attribute_widget_get_type ())
#define GUPPI_ATTRIBUTE_WIDGET(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_ATTRIBUTE_WIDGET,GuppiAttributeWidget))
#define GUPPI_ATTRIBUTE_WIDGET0(obj) ((obj) ? (GUPPI_ATTRIBUTE_WIDGET(obj)) : NULL)
#define GUPPI_ATTRIBUTE_WIDGET_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_ATTRIBUTE_WIDGET,GuppiAttributeWidgetClass))
#define GUPPI_IS_ATTRIBUTE_WIDGET(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_ATTRIBUTE_WIDGET))
#define GUPPI_IS_ATTRIBUTE_WIDGET0(obj) (((obj) == NULL) || (GUPPI_IS_ATTRIBUTE_WIDGET(obj)))
#define GUPPI_IS_ATTRIBUTE_WIDGET_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_ATTRIBUTE_WIDGET))

GtkType guppi_attribute_widget_get_type (void);

GtkWidget *guppi_attribute_widget_new         (GuppiAttributeFlavor flavor, const gchar *key);
void       guppi_attribute_widget_construct   (GuppiAttributeWidget *gaw, GuppiAttributeFlavor flavor_name, const gchar *key);

void guppi_attribute_widget_attach_bag             (GuppiAttributeWidget *gaw, GuppiAttributeBag *bag);
void guppi_attribute_widget_recursively_attach_bag (GtkWidget *w, GuppiAttributeBag *bag);

void guppi_attribute_widget_bag_get (GuppiAttributeWidget *gaw, const gchar *subkey, gpointer);
void guppi_attribute_widget_bag_set (GuppiAttributeWidget *gaw, const gchar *subkey, ...);

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_ATTRIBUTE_WIDGET_H */

/* $Id$ */
