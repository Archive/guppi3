/* $Id$ */

/*
 * guppi-convenient.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-convenient.h"

#include <gtk/gtkwidget.h>
#include <gtk/gtktooltips.h>

void
guppi_free2 (gpointer x, gpointer ignored)
{
  guppi_free (x);
}

void
guppi_free_hash_key (gpointer k, gpointer v, gpointer d)
{
  guppi_free (k);
}

void
guppi_free_hash_val (gpointer k, gpointer v, gpointer d)
{
  guppi_free (v);
}

void
guppi_free_hash_keyval (gpointer k, gpointer v, gpointer d)
{
  guppi_free (k);
  guppi_free (v);
}

void
guppi_unref2 (gpointer x, gpointer ignored)
{
  guppi_unref (x);
}

void
guppi_unref_hash_key (gpointer k, gpointer v, gpointer d)
{
  guppi_unref (k);
}

void
guppi_unref_hash_val (gpointer k, gpointer v, gpointer d)
{
  guppi_unref (v);
}

void
guppi_unref_hash_keyval (gpointer k, gpointer v, gpointer d)
{
  guppi_unref (k);
  guppi_unref (v);
}


void
guppi_2sort (double *a, double *b)
{
  double t;

  if (a == NULL || b == NULL)
    return;

  if (*a > *b) {
    t = *a;
    *a = *b;
    *b = t;
  }
}

void
guppi_2sort_i (gint * a, gint * b)
{
  gint t;

  if (a == NULL || b == NULL)
    return;

  if (*a > *b) {
    t = *a;
    *a = *b;
    *b = t;
  }
}

void
guppi_2sort_dt (GDate **a, GDate **b)
{
  g_return_if_fail (a && *a && g_date_valid (*a));
  g_return_if_fail (b && *b && g_date_valid (*b));

  if (g_date_gt (*a, *b)) {
    GDate *t = *a;
    *a = *b;
    *b = t;
  }
}

void
guppi_3sort (double *a, double *b, double *c)
{
  double t;

  if (a == NULL || b == NULL || c == NULL)
    return;

  if (*c < *b) {
    t = *b;
    *b = *c;
    *c = t;
  }

  if (*b < *a) {
    t = *b;
    *a = *b;
    *b = t;
  }

  if (*c < *b) {
    t = *b;
    *b = *c;
    *c = t;
  }
}

void
guppi_tooltip (GtkWidget * w, const gchar * tip)
{
  GtkTooltips *t;
  g_return_if_fail (w != NULL && GTK_IS_WIDGET (w));
  g_return_if_fail (tip != NULL);

  t = gtk_tooltips_new ();
  gtk_tooltips_set_tip (t, w, tip, NULL);
}

/* $Id$ */
