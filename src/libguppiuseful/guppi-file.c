/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-file.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-file.h"

#include "guppi-memory.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_file_finalize (GtkObject *obj)
{
  GuppiFile *gf = GUPPI_FILE (obj);

  guppi_finalized (obj);

  guppi_free (gf->filename);
  gf->filename = NULL;

  if (gf->gzfd) {
    gzclose (gf->gzfd);
    gf->gzfd = NULL;
  }
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_file_class_init (GuppiFileClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;

  parent_class = gtk_type_class (GTK_TYPE_OBJECT);

  object_class->finalize = guppi_file_finalize;
}

static void
guppi_file_init (GuppiFile *obj)
{
  obj->base_fd = -1;
}

GtkType
guppi_file_get_type (void)
{
  static GtkType guppi_file_type = 0;
  if (!guppi_file_type) {
    static const GtkTypeInfo guppi_file_info = {
      "GuppiFile",
      sizeof (GuppiFile),
      sizeof (GuppiFileClass),
      (GtkClassInitFunc) guppi_file_class_init,
      (GtkObjectInitFunc) guppi_file_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_file_type = gtk_type_unique (GTK_TYPE_OBJECT, &guppi_file_info);
  }
  return guppi_file_type;
}

/**********************************************************************/

/*
 * The whole point of wrapping stdio is to set things up so that
 * guppi_file_peek() will work on a non-seekable stream by doing some
 * buffering.  That can all be implemented later, though.
 */

GuppiFile *
guppi_file_open (const gchar *path)
{
  GuppiFile *gf;
  gzFile gzfd;

  g_return_val_if_fail (path != NULL, NULL);

  gzfd = gzopen (path, "r");
  if (gzfd == NULL)
    return NULL;

  gf = GUPPI_FILE (guppi_type_new (guppi_file_get_type ()));
  gf->gzfd = gzfd;

  guppi_free (gf->filename);
  gf->filename = guppi_strdup (path);

  return gf;
}

GuppiFile *
guppi_file_fdopen (gint file_des)
{
  GuppiFile *gf;
  gzFile gzfd;

  g_return_val_if_fail (file_des >= 0, NULL);

  gzfd = gzdopen (file_des, "r");
  if (gzfd == NULL)
    return NULL;

  gf = GUPPI_FILE (guppi_type_new (guppi_file_get_type ()));
  gf->base_fd = file_des;
  gf->gzfd = gzfd;

  return gf;
}

void
guppi_file_close (GuppiFile *gf)
{
  g_return_if_fail (gf != NULL);

  if (gf->gzfd != NULL) {
    gzclose (gf->gzfd);
    gf->gzfd = NULL;
  }
}

const gchar *
guppi_file_filename (GuppiFile *gf)
{
  g_return_val_if_fail (gf != NULL, NULL);
  return gf->filename;
}

gsize
guppi_file_read (GuppiFile *gf, gpointer ptr, gsize size, gsize nmemb)
{
  g_return_val_if_fail (gf != NULL, 0);
  g_return_val_if_fail (gf->gzfd != NULL, 0);
  g_return_val_if_fail (ptr != NULL, 0);

  return gzread (gf->gzfd, ptr, size *nmemb);
}

gint
guppi_file_getc (GuppiFile *gf)
{
  g_return_val_if_fail (gf != NULL, EOF);
  g_return_val_if_fail (gf->gzfd != NULL, EOF);

  return gzgetc (gf->gzfd);
}

gchar *
guppi_file_gets (GuppiFile *gf, gchar *s, gsize size)
{
  g_return_val_if_fail (gf != NULL, NULL);
  g_return_val_if_fail (gf->gzfd != NULL, NULL);
  g_return_val_if_fail (s != NULL, NULL);

  return gzgets (gf->gzfd, s, size);
}

gsize
guppi_file_peek (GuppiFile *gf, gpointer ptr, gsize size)
{
  z_off_t where;
  gsize how_many;

  g_return_val_if_fail (gf != NULL, 0);
  g_return_val_if_fail (gf->gzfd != NULL, 0);
  g_return_val_if_fail (ptr != NULL, 0);

  where = gztell (gf->gzfd);
  how_many = gzread (gf->gzfd, ptr, size);
  gzseek (gf->gzfd, where, SEEK_SET);

  return how_many;
}

gint
guppi_file_seek (GuppiFile *gf, glong offset, gint whence)
{
  g_return_val_if_fail (gf != NULL, -1);
  g_return_val_if_fail (gf->gzfd != NULL, -1);

  return gzseek (gf->gzfd, offset, whence);
}

glong
guppi_file_tell (GuppiFile *gf)
{
  g_return_val_if_fail (gf != NULL, -1);
  g_return_val_if_fail (gf->gzfd != NULL, -1);

  return gztell (gf->gzfd);
}

void
guppi_file_rewind (GuppiFile *gf)
{
  g_return_if_fail (gf != NULL);
  g_return_if_fail (gf->gzfd != NULL);

  gzrewind (gf->gzfd);
}

gint
guppi_file_stat (GuppiFile *gf, struct stat *buf)
{
  g_return_val_if_fail (gf != NULL, -1);
  g_return_val_if_fail (buf != NULL, -1);

  if (gf->base_fd >= 0)
    return fstat (gf->base_fd, buf);
  else if (gf->filename)
    return stat (gf->filename, buf);
  else
    return -1;
}

gboolean
guppi_file_eof (GuppiFile *gf)
{
  g_return_val_if_fail (gf != NULL, TRUE);
  g_return_val_if_fail (gf->gzfd != NULL, TRUE);
  return gzeof (gf->gzfd);
}

const gchar *
guppi_file_error (GuppiFile *gf)
{
  gint errnum;

  g_return_val_if_fail (gf != NULL, NULL);
  g_return_val_if_fail (gf->gzfd != NULL, NULL);
  return gzerror (gf->gzfd, &errnum);
}

/* $Id$ */
