/* $Id$ */

/*
 * guppi-text-squeeze.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-text-squeeze.h"

#include "guppi-memory.h"

/* This presents all sorts of i18n issues that I don't want to
   think about right now... */

/*
  SCRABBLE(R) a registered trademark. All intellectual property
  rights in and to the game are owned in the U.S.A. by Hasbro Inc., in
  Canada by Hasbro Canada Inc. and throughout the rest of the world by
  J.W. Spear & Sons, PLC of Enfield, Middlesex, England, a subsidiary of
  Mattel Inc.  Mattel and Spear are not affiliated with Hasbro or Hasbro
  Canada.
*/

static const gint scrabble_tile_values[] = {
  /*  A B C D E F G H I J K L M N O P  Q R S T U V W X Y  Z
   */ 1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8,
  4, 10, -1
};

static const gint english_letter_frequencies[] = {
  /*  A B  C  D   E  F  G  H  I J K  L  M  N  O  P Q  R  S  T  U  V  W X  Y Z
   */ 73, 9, 30, 44, 130, 28, 16, 35, 74, 2, 3, 35, 25, 78, 74, 27, 3, 77, 63,
  93, 27, 13, 16, 5, 19, 1,
  -1
};

static const gchar *english_language_digrams[] = {
  "th", "he", "at", "st", "an", "in", "ea", "nd", "er", "en", "re", "nt",
  "to", "es", "on", "ed", "is", "ti", NULL
};

static const gchar *english_language_trigraphs[] = {
  "the", "and", "tha", "hat", "ent", "ion", "for", "tio", "has", "edt", "tis",
  "ers", "res", "ter", "con", "ing", "men", "tho", NULL
};

gchar *
guppi_text_squeeze (const gchar * txt, gint len)
{
  g_return_val_if_fail (txt != NULL, NULL);
  g_return_val_if_fail (len > 0, NULL);

  if (strlen (txt) <= len)
    return guppi_strdup (txt);

  return NULL;
}


/* $Id$ */
