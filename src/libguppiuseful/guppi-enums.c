/* $Id$ */

/*
 * guppi-enums.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-enums.h"

/* #include <gnome.h> */
#include <glib.h>
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-i18n.h>

/**
 * guppi_compass2str
 * @val: The #guppi_compass_t to convert.
 *
 * Given a #guppi_compass_t, returns a descriptive string.
 *
 * Returns: The string version of @val.
 */
const gchar *
guppi_compass2str (guppi_compass_t val)
{
  switch (val) {

  case GUPPI_NORTH:
    return "north";

  case GUPPI_SOUTH:
    return "south";

  case GUPPI_EAST:
    return "east";

  case GUPPI_WEST:
    return "west";

  default:
    return "invalid-compass";
  }
}

/**
 * guppi_str2compass
 * @str: The string to convert.
 *
 * Convert a string into a #guppi_compass_t.
 *
 * Returns: The #guppi_compass_t represented by @str.
 */
guppi_compass_t
guppi_str2compass (const gchar *str)
{
  g_return_val_if_fail (str != NULL, GUPPI_COMPASS_INVALID);

  if (!g_strcasecmp (str, "north") || !g_strcasecmp (str, "top"))
    return GUPPI_NORTH;
  
  if (!g_strcasecmp (str, "south") || !g_strcasecmp (str, "bottom"))
    return GUPPI_SOUTH;
  
  if (!g_strcasecmp (str, "east") || !g_strcasecmp (str, "right"))
    return GUPPI_EAST;

  if (!g_strcasecmp (str, "west") || !g_strcasecmp (str, "left"))
    return GUPPI_WEST;
  
  return GUPPI_COMPASS_INVALID;
}

const gchar *
guppi_alignment2str (guppi_alignment_t val)
{
  switch (val) {

  case GUPPI_LEFT:
    return "left";

  case GUPPI_RIGHT:
    return "right";

  case GUPPI_TOP:
    return "top";

  case GUPPI_BOTTOM:
    return "bottom";

  case GUPPI_CENTER_X:
    return "center-x";

  case GUPPI_CENTER_Y:
    return "center-y";

  case GUPPI_WIDTH:
    return "width";

  case GUPPI_HEIGHT:
    return "height";

  default:
    return "invalid-alignment";
  }
}

guppi_alignment_t
guppi_str2alignment (const gchar * str)
{
  g_return_val_if_fail (str != NULL, GUPPI_ALIGNMENT_INVALID);

  if (!g_strcasecmp (str, "left"))
    return GUPPI_LEFT;

  if (!g_strcasecmp (str, "right"))
    return GUPPI_RIGHT;

  if (!g_strcasecmp (str, "top"))
    return GUPPI_TOP;

  if (!g_strcasecmp (str, "bottom"))
    return GUPPI_BOTTOM;

  if (!g_strcasecmp (str, "center-x"))
    return GUPPI_CENTER_X;

  if (!g_strcasecmp (str, "center-y"))
    return GUPPI_CENTER_Y;

  if (!g_strcasecmp (str, "width"))
    return GUPPI_WIDTH;
  
  if (!g_strcasecmp (str, "height"))
    return GUPPI_HEIGHT;

    return GUPPI_ALIGNMENT_INVALID;
}

const gchar *
guppi_orientation2str (guppi_orientation_t val)
{
  switch (val) {

  case GUPPI_HORIZONTAL:
    return "horizontal";

  case GUPPI_VERTICAL:
    return "vertical";

  default:
    return "invalid-orientation";

  }
}

guppi_orientation_t
guppi_str2orientation (const gchar * str)
{
  g_return_val_if_fail (str != NULL, GUPPI_ORIENTATION_INVALID);

  if (!g_strcasecmp (str, "horizontal"))
    return GUPPI_HORIZONTAL;
  
  if (!g_strcasecmp (str, "vertical"))
    return GUPPI_VERTICAL;

  return GUPPI_ORIENTATION_INVALID;
}

const gchar *
guppi_axis2str (guppi_axis_t val)
{
  switch (val) {
  case GUPPI_X_AXIS:
    return "x";
  case GUPPI_Y_AXIS:
    return "y";
  case GUPPI_Z_AXIS:
    return "z";
  case GUPPI_T_AXIS:
    return "t";
  case GUPPI_META_AXIS:
    return "meta";
  default:
    return "?";
  }
}

guppi_axis_t
guppi_str2axis (const gchar *str)
{
  g_return_val_if_fail (str != NULL, GUPPI_INVALID_AXIS);

  if (*str == 'x' || *str == 'X')
    return GUPPI_X_AXIS;

  if (*str == 'y' || *str == 'Y')
    return GUPPI_Y_AXIS;

  if (*str == 'z' || *str == 'Z')
    return GUPPI_Z_AXIS;

  if (*str == 't' || *str == 'T')
    return GUPPI_T_AXIS;

  if (*str == 'm' || *str == 'M')
    return GUPPI_META_AXIS;

  return GUPPI_INVALID_AXIS;
}


/* $Id$ */
