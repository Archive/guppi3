/* $Id$ */

/* 
 * guppi-guile.h
 *
 * Copyright (C) 1998, 1999, 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_GUILE_H
#define _INC_GUPPI_GUILE_H

/* #include <gtk/gtk.h> */
#include <stdlib.h>
#include <libguile.h>
#include <guile/gh.h>

#include "guppi-enums.h"
#include "guppi-fn-wrapper.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS

/*
  Our own version of the guile snarfing macros.

  There is nothing like some good preprocessor hacks to get your day
  started off right.
*/

#ifndef SCM_MAGIC_SNARFER
#define GUPPI_PROC(fname, string_name, req, opt, var, ARGLIST) \
static char str_##fname[] = string_name; \
static SCM scm_fn_##fname ARGLIST
#else
#define GUPPI_PROC(fname, string_name, req, opt, var, ARGLIST) \
 %%%start \
   scm_make_gsubr(str_##fname, req, opt, var, scm_fn_##fname); \
%%%end
#endif
/* There are ideas from scwm strewn all through here. */
gboolean guppi_guile_is_active (void);
void guppi_guile_init (void);

void guppi_scm_error (const char *subr, const char *msg);
/* Tagged errors can be caught by catch; (untagged errors can too, but they 
   have the generic tag "guppi-error") */
void guppi_scm_tagged_error (const char *tag, const char *subr,
			     const char *msg);
SCM guppi_eval_str (const char *exp);
SCM guppi_safe_load (char *filename);

SCM guppi_safe_apply (SCM proc, SCM args);
SCM guppi_safe_apply_thunk (SCM proc);
SCM guppi_safe_apply_message_only (SCM proc, SCM args);

SCM guppi_safe_call0 (SCM thunk);
SCM guppi_safe_call1 (SCM proc, SCM arg);
SCM guppi_safe_call2 (SCM proc, SCM arg1, SCM arg2);
SCM guppi_safe_call3 (SCM proc, SCM arg1, SCM arg2, SCM arg3);

typedef struct _ScmEnumVal {
  char *name;
  gint val;
} ScmEnumVal;

typedef struct _ScmEnumInfo {
  char *enum_name;
  const int nvals;
  ScmEnumVal *vals;
} ScmEnumInfo;

/* Return true if the lookup succeeded */
gboolean guppi_string_to_enum_val (const ScmEnumInfo * info, const char *str,
				   gint * retval);
gboolean guppi_enum_val_to_string (const ScmEnumInfo * info, gint val,
				   const char **retval);

void guppi_scm_thunk_cb (gpointer widget, gpointer thunk);
void guppi_scm_eval_cb (gpointer widget, gpointer eval_str);

void guppi_scm_define_autoloaded_symbol (const gchar *fn_name,
					 const gchar *type,
					 const gchar *name);

/* Some convenient type conversion gadgets */

gboolean scm_stringv_p (SCM);
SCM stringv2scm (gchar **);
gchar **scm2stringv (SCM);

gboolean scm_color_p (SCM);
SCM color2scm (guint32);
guint32 scm2color (SCM);

gboolean scm_compass_p (SCM);
SCM compass2scm (guppi_compass_t);
guppi_compass_t scm2compass (SCM);

GtkArg *scm2gtk_arg (SCM name, SCM);
SCM gtk_arg2scm (GtkArg *);
GtkArg *scm2argv (SCM list, guint * length);
void guppi_argv_free (GtkArg *, guint len);

GuppiFnWrapper *scm2fn_d__i (SCM proc);
GuppiFnWrapper *scm2fn_d__i_d (SCM proc);
GuppiFnWrapper *scm2fn_d__d (SCM proc);
GuppiFnWrapper *scm2fn_d__d_d (SCM proc);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_GUILE_H */

/* $Id$ */
