/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-attribute-flavor.h
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __GUPPI_ATTRIBUTE_FLAVOR_H__
#define __GUPPI_ATTRIBUTE_FLAVOR_H__

#include  "guppi-defs.h"
#include <glib.h>
#include <gtk/gtkwidget.h>
#include "guppi-xml.h"

BEGIN_GUPPI_DECLS

typedef gint GuppiAttributeFlavor;

typedef gpointer (*AttributeCreateDefaultFn) (void);
typedef void     (*AttributeDestroyFn)       (gpointer);
typedef gpointer (*AttributeCopyFn)          (gpointer);
typedef gboolean (*AttributeEqualityFn)      (gpointer data_a, gpointer data_b);

typedef gboolean (*AttributeGetFromPointerFn) (const gchar *key, const gchar *subkey, gpointer data, gpointer *storage);
typedef gboolean (*AttributeGetFromVarArgsFn) (const gchar *key, const gchar *subkey, va_list *, gpointer *storage);
typedef void     (*AttributePutToVarArgsFn)   (const gchar *key, const gchar *subkey, gpointer ptr, gpointer dest);

typedef GtkWidget *(*AttributeWidgetFn) (const gchar *key);

typedef gchar   *(*AttributeExportStringFn) (gpointer);
typedef gboolean (*AttributeImportStringFn) (const gchar *, gpointer *);

typedef xmlNodePtr (*AttributeExportXMLFn) (GuppiXMLDocument *, gpointer);
typedef gboolean   (*AttributeImportXMLFn) (GuppiXMLDocument *, xmlNodePtr, gpointer *);

struct _GuppiAttributesFlavorInfo;
typedef struct _GuppiAttributesFlavorInfo GuppiAttributeFlavorInfo;

GuppiAttributeFlavor guppi_attribute_flavor_register (const gchar *name,
						      AttributeCreateDefaultFn create_default,
						      AttributeDestroyFn destroy,
						      AttributeCopyFn copy,
						      AttributeEqualityFn equality,
						      AttributeGetFromPointerFn getp,
						      AttributeGetFromVarArgsFn va2p,
						      AttributePutToVarArgsFn p2va);

GuppiAttributeFlavor guppi_attribute_flavor_register_object_semantics (const gchar *name,
								       AttributeCreateDefaultFn create_default,
								       AttributeCopyFn copy,
								       AttributeEqualityFn equality);

void guppi_attribute_flavor_add_widget (GuppiAttributeFlavor flavor,
					AttributeWidgetFn widget_fn);

void guppi_attribute_flavor_add_string_serialization (GuppiAttributeFlavor flavor,
						      AttributeExportStringFn export_str,
						      AttributeImportStringFn import_str);

void guppi_attribute_flavor_add_xml_serialization (GuppiAttributeFlavor flavor,
						   AttributeExportXMLFn export_xml,
						   AttributeImportXMLFn import_xml);

void guppi_attribute_flavor_add_signal_to_forward (GuppiAttributeFlavor flavor,
						   const gchar *signal_name,
						   gboolean queue_signal_emissions);

GuppiAttributeFlavor      guppi_attribute_flavor_get_from_name (const gchar *name);
const gchar              *guppi_attribute_flavor_get_name (GuppiAttributeFlavor);

GtkWidget                *guppi_attribute_flavor_get_widget (GuppiAttributeFlavor, const gchar *key);
GtkWidget                *guppi_attr_glade (const gchar *label, const gchar *type, const gchar *key);

GuppiAttributeFlavorInfo *guppi_attribute_flavor_get_info (GuppiAttributeFlavor);



#define GUPPI_ATTR_BOOLEAN   (guppi_attribute_flavor_boolean ())
#define GUPPI_ATTR_INT       (guppi_attribute_flavor_int ())
#define GUPPI_ATTR_UINT32    (guppi_attribute_flavor_uint32 ())
#define GUPPI_ATTR_DOUBLE    (guppi_attribute_flavor_double ())
#define GUPPI_ATTR_DIMENSION (guppi_attribute_flavor_dimension ())
#define GUPPI_ATTR_STRING    (guppi_attribute_flavor_string ())
#define GUPPI_ATTR_DATE      (guppi_attribute_flavor_date ())
#define GUPPI_ATTR_RGB       (guppi_attribute_flavor_rgb ())
#define GUPPI_ATTR_RGBA      (guppi_attribute_flavor_rgba ())
#define GUPPI_ATTR_FONT      (guppi_attribute_flavor_font ())

GuppiAttributeFlavor guppi_attribute_flavor_boolean (void);
GuppiAttributeFlavor guppi_attribute_flavor_int (void);
GuppiAttributeFlavor guppi_attribute_flavor_uint32 (void);
GuppiAttributeFlavor guppi_attribute_flavor_string (void);
GuppiAttributeFlavor guppi_attribute_flavor_double (void);
GuppiAttributeFlavor guppi_attribute_flavor_dimension (void);
GuppiAttributeFlavor guppi_attribute_flavor_date (void);
GuppiAttributeFlavor guppi_attribute_flavor_rgb (void);
GuppiAttributeFlavor guppi_attribute_flavor_rgba (void);
GuppiAttributeFlavor guppi_attribute_flavor_font (void);

/* For use in GtkObject-based flavor implementations. */




END_GUPPI_DECLS

#endif /* __GUPPI_ATTRIBUTE_FLAVOR_H__ */

