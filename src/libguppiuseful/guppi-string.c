/* $Id$ */

/*
 * guppi-string.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 * and Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <config.h>
#include "guppi-string.h"
/* #include <gnome.h> */

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "guppi-memory.h"


/* 
   First entry of each array is the preferred form.

   We allow for translations of true, false, yes, no, etc., but we
   also always make sure that the English versions will also work.
   This is just another example of tactics the Anglophone world uses
   to maintains its hegemony.
*/

static const gchar *true_forms[] = { "#t", "1",
  "true", "t", "yes", "y",
  "",
  N_("true"), N_("t"),
  N_("yes"), N_("y"),
  NULL
};

static const gchar *false_forms[] = { "#f", "0",
  "false", "f", "no", "n",
  "",
  N_("false"), N_("f"),
  N_("no"), N_("n"),
  NULL
};

gboolean
guppi_string_is_boolean (const gchar * str)
{
  gint i;
  gboolean trans;

  g_return_val_if_fail (str != NULL, FALSE);

  i = 0;
  trans = FALSE;
  while (true_forms[i]) {
    if (*true_forms[i] == '\0')
      trans = TRUE;
    else if (g_strcasecmp (trans ? _(true_forms[i]) : true_forms[i], str) ==
	     0) return TRUE;
    ++i;
  }

  i = 0;
  trans = FALSE;
  while (false_forms[i]) {
    if (*false_forms[i] == '\0')
      trans = TRUE;
    else if (g_strcasecmp (trans ? _(false_forms[i]) : false_forms[i], str) ==
	     0) return TRUE;
    ++i;
  }

  return FALSE;
}

gboolean
guppi_string2boolean (const gchar * str)
{
  gint i;

  g_return_val_if_fail (str != NULL, FALSE);

  i = 0;
  while (true_forms[i]) {
    if (*true_forms[i] && g_strcasecmp (true_forms[i], str) == 0)
      return TRUE;
    ++i;
  }

  i = 0;
  while (false_forms[i]) {
    if (*false_forms[i] && g_strcasecmp (false_forms[i], str) == 0)
      return FALSE;
    ++i;
  }

  g_warning ("Can't convert \"%s\" to a bool.", str);
  return FALSE;
}

void
guppi_boolean2string (gboolean x, gchar * str, gsize N)
{
  g_return_if_fail (str != NULL);

  if (N == 0)
    return;

  strncpy (str, x ? true_forms[0] : false_forms[0], N);
}

gboolean
guppi_string_is_int (const gchar * str)
{
  gchar *foo;

  g_return_val_if_fail (str != NULL, FALSE);

  strtol (str, &foo, 10);
  /* allow trailing whitespace */
  while (*foo && isspace ((guchar)*foo))
    ++foo;

  return *foo == '\0';

}

gboolean
guppi_string_is_number (const gchar * str)
{
  gchar *foo;

  g_return_val_if_fail (str != NULL, FALSE);

  strtod (str, &foo);
  /* allow trailing whitespace */
  while (*foo && isspace ((guchar)*foo))
    ++foo;

  return *foo == '\0';
}

gsize
guppi_string_noise_count (const gchar * s, gsize N)
{
  gsize i;
  gsize count = 0;
  for (i = 0; i < N; ++i) {
    if (!isprint (s[i]))
      ++count;
  }
  return count;
}

gchar *
guppi_string_canonize_filename (const gchar *s)
{
  const gchar *bad_chars = "|/\\!\"'*`[]<>()";
  gchar *x;
  const gchar *y;
  gchar *c;
    
  if (s == NULL)
    return NULL;

  x = guppi_strdup (s);

  c = x;
  while (*c) {
    
    y = bad_chars;
    while (*y && *c != *y)
      ++y;

    if (*y || isspace ((gint)*c) || iscntrl ((gint)*c))
      *c = '_';

    ++c;
  }
  
  return x;
}

/* $Id$ */
