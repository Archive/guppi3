/* $Id$ */

/*
 * demo.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <gnome.h>
#include <stdio.h>

#include "guppi-attribute-bag.h"

int
main(int argc, char* argv[])
{
  GuppiAttributeBag *bag;
  GuppiXMLDocument *doc;
  xmlNodePtr test_node;
  GDate *dt;

  gnome_init ("foo", "bar", argc, argv);
  
  bag = guppi_attribute_bag_new ();
  guppi_attribute_bag_add (bag, GUPPI_ATTR_BOOLEAN, "frog", "The Frog Attribute");
  guppi_attribute_bag_add (bag, GUPPI_ATTR_INT, "assbarn", "The Assbarn Attribute");
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_STRING, "kick", "Kick!", "default string");
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DOUBLE, "dub", "Testing a double", 17.1717);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGB, "rgb", "Testing a RGB", 0xff0000);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA, "rgba", "Testing a RGBA", 0xff0000ff);

  guppi_attribute_bag_add (bag, GUPPI_ATTR_DATE, "date", "date test");
  guppi_attribute_bag_add (bag, GUPPI_ATTR_FONT, "font", "My font");

  guppi_attribute_bag_set (bag, "frog", TRUE);
  guppi_attribute_bag_set (bag, "assbarn", 103);
  guppi_attribute_bag_set (bag, "kick", "<b>assassin!</b>&gt;");

  dt = g_date_new_dmy (5, 6, 1969);
  guppi_attribute_bag_set (bag, "date", dt);

  guppi_attribute_bag_dump (bag);

  doc = guppi_xml_document_new ();

  test_node = guppi_attribute_bag_export_xml (bag, doc);
  guppi_attribute_bag_import_xml (bag, doc, test_node);
  xmlFreeNode (test_node);

  guppi_attribute_bag_dump (bag);

  g_print ("\n\n");
  guppi_attribute_bag_spew_xml (bag);

  gtk_main ();

  return 0;
}

/* $id$ */






