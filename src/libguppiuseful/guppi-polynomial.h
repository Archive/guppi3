/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-polynomial.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_POLYNOMIAL_H
#define _INC_GUPPI_POLYNOMIAL_H

#include <glib.h>
#include <gtk/gtkobject.h>
#include <libart_lgpl/libart.h>
#include  "guppi-defs.h"
#include "guppi-xml.h"

BEGIN_GUPPI_DECLS;

typedef struct _GuppiPolynomial GuppiPolynomial;
typedef struct _GuppiPolynomialClass GuppiPolynomialClass;

struct _GuppiPolynomial {
  GtkObject parent;
  gpointer opaque_internals;
};

struct _GuppiPolynomialClass {
  GtkObjectClass parent_class;

  void (*changed) (GuppiPolynomial *);
};

#define GUPPI_TYPE_POLYNOMIAL (guppi_polynomial_get_type ())
#define GUPPI_POLYNOMIAL(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_POLYNOMIAL,GuppiPolynomial))
#define GUPPI_POLYNOMIAL0(obj) ((obj) ? (GUPPI_POLYNOMIAL(obj)) : NULL)
#define GUPPI_POLYNOMIAL_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_POLYNOMIAL,GuppiPolynomialClass))
#define GUPPI_IS_POLYNOMIAL(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_POLYNOMIAL))
#define GUPPI_IS_POLYNOMIAL0(obj) (((obj) == NULL) || (GUPPI_IS_POLYNOMIAL(obj)))
#define GUPPI_IS_POLYNOMIAL_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_POLYNOMIAL))

GtkType guppi_polynomial_get_type (void);

GuppiPolynomial *guppi_polynomial_new  (gint degree, ...);
GuppiPolynomial *guppi_polynomial_newv (gint degree, const double *vec);

GuppiPolynomial *guppi_polynomial_new_by_roots  (gint degree, ...);
GuppiPolynomial *guppi_polynomial_new_by_rootsv (gint degree, const double *vec);

GuppiPolynomial *guppi_polynomial_new_constant  (double c0);
GuppiPolynomial *guppi_polynomial_new_linear    (double c0, double c1);
GuppiPolynomial *guppi_polynomial_new_quadratic (double c0, double c1, double c2);

GuppiPolynomial *guppi_polynomial_copy (GuppiPolynomial *p);

void             guppi_polynomial_freeze (GuppiPolynomial *p);
void             guppi_polynomial_thaw   (GuppiPolynomial *p);

gint             guppi_polynomial_degree (GuppiPolynomial *p);
double           guppi_polynomial_coefficient (GuppiPolynomial *p, gint i);
void             guppi_polynomial_set_coefficient (GuppiPolynomial *p, gint i, double x);

double           guppi_polynomial_eval    (GuppiPolynomial *p, double);
double           guppi_polynomial_eval_D  (GuppiPolynomial *p, double);
double           guppi_polynomial_eval_DD (GuppiPolynomial *p, double);

void             guppi_polynomial_sample (GuppiPolynomial *p, gint N,
					  const double *x, gint x_stride,
					  double *y, gint y_stride);
void             guppi_polynomial_sample_uniformly (GuppiPolynomial *p,
						    double a, double b, gsize N,
						    double *x, gint x_stride,
						    double *y, gint y_stride);
ArtVpath        *guppi_polynomial_approximate_path (GuppiPolynomial *p,
						    double a, double b,
						    double min_y, double max_y,
						    double x_error, double y_error,
						    double min_angle,
						    double scale_x, double scale_y);

void             guppi_polynomial_scale (GuppiPolynomial *p, double c);
void             guppi_polynomial_D     (GuppiPolynomial *p);

/* Divide through by coefficient on highest-degree term, resulting in
   a polynomial with leading coefficient 1. */
void             guppi_polynomial_normalize (GuppiPolynomial *p);

/* The remainder of dividing by the polynomial mod */
void             guppi_polynomial_modulo    (GuppiPolynomial *p, GuppiPolynomial *mod);

/* Divide polynomial by (x-x0).  If x0 isn't a root, you have to manage the
   remainder term by hand. */
void             guppi_polynomial_deflate         (GuppiPolynomial *p, double x0);
void             guppi_polynomial_deflate_complex (GuppiPolynomial *p, double re, double im);

/* Multiply the polynomial by (x-x0). */
void             guppi_polynomial_inflate         (GuppiPolynomial *p, double x0);

double           guppi_polynomial_newton_polish   (GuppiPolynomial *p, double x0, gint max_iter, double epsilon);

double           guppi_polynomial_gershgorin_radius (GuppiPolynomial *p);

gint             guppi_polynomial_real_roots             (GuppiPolynomial *p);
gint             guppi_polynomial_real_roots_in_interval (GuppiPolynomial *p, double a, double b);

gboolean         guppi_polynomial_find_one_real_root     (GuppiPolynomial *p, double *x);
gint             guppi_polynomial_find_real_roots        (GuppiPolynomial *p, double *roots);

void             guppi_polynomial_minmax_on_range        (GuppiPolynomial *p, double a, double b, double *min, double *max);
gboolean         guppi_polynomial_find_bounded_range     (GuppiPolynomial *p, double x, double min, double max,
							  double *a, double *b);

void             guppi_polynomial_spew (GuppiPolynomial *p);

xmlNodePtr       guppi_polynomial_export_xml (GuppiPolynomial *p, GuppiXMLDocument *doc);
GuppiPolynomial *guppi_polynomial_import_xml (GuppiXMLDocument *doc, xmlNodePtr node);




END_GUPPI_DECLS;

#endif /* _INC_GUPPI_POLYNOMIAL_H */

/* $Id$ */
