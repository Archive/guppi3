/* $Id$ */

/*
 * guppi-timer.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_TIMER_H
#define _INC_GUPPI_TIMER_H

/* #include <gtk/gtk.h> */
#include <glib.h>

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS 

void guppi_timer_init (void);
void guppi_timer_terminate (void);
void guppi_timer_heartbeat (void);
guint guppi_timer_total_elapsed_time (void);
guint guppi_timer_total_active_time (void);


END_GUPPI_DECLS

#endif /* _INC_GUPPI_TIMER_H */

/* $Id$ */
