/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-garray.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_GARRAY_H
#define _INC_GUPPI_GARRAY_H

/* #include <gtk/gtk.h> */
#include <gtk/gtkobject.h>
#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS

typedef struct _GuppiGArray GuppiGArray;
typedef struct _GuppiGArrayClass GuppiGArrayClass;

/*
  This is just a wrapper around a GArray so that we can get GtkObject-style
  reference counting.
*/

struct _GuppiGArray {
  GtkObject parent;

  gsize size;
  gsize el_size;
  gpointer data;
#if 0
  GArray *array;
#endif
};

struct _GuppiGArrayClass {
  GtkObjectClass parent_class;
};

#define GUPPI_TYPE_GARRAY (guppi_garray_get_type())
#define GUPPI_GARRAY(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_GARRAY,GuppiGArray))
#define GUPPI_GARRAY0(obj) ((obj) ? (GUPPI_GARRAY(obj)) : NULL)
#define GUPPI_GARRAY_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_GARRAY,GuppiGArrayClass))
#define GUPPI_IS_GARRAY(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_GARRAY))
#define GUPPI_IS_GARRAY0(obj) (((obj) == NULL) || (GUPPI_IS_GARRAY(obj)))
#define GUPPI_IS_GARRAY_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_GARRAY))

GtkType guppi_garray_get_type (void);

void guppi_garray_construct (GuppiGArray * ga, guint element_size);
GuppiGArray *guppi_garray_new (guint element_size);

#if 0
#define guppi_garray_data(ga) ((ga)->array->data)
#define guppi_garray_size(ga) ((ga)->array->len)
#define guppi_garray_element_size(ga) (((GRealArray*)((ga)->array))->elt_size)

#define guppi_garray_append_vals(ga, ptr, len) (g_array_append_vals((ga)->array, ptr, len), (ga))
#define guppi_garray_prepend_vals(ga, ptr, len) (g_array_prepend_vals((ga)->array, ptr, len), (ga))
#define guppi_garray_insert_vals(ga, i, ptr, len) (g_array_insert_vals((ga)->array, i, ptr, len), (ga))
#define guppi_garray_set_size(ga, len) (g_array_set_size((ga)->array, len), (ga))
#define guppi_garray_remove_index(ga, i) (g_array_remove_index((ga)->array, i), (ga))
#endif

#define guppi_garray_data(ga) ((ga)->data)
#define guppi_garray_size(ga) ((ga)->size)
#define guppi_garray_element_size(ga) ((ga)->el_size)
void guppi_garray_set_size (GuppiGArray *, gsize);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_GARRAY_H */

/* $Id$ */
