/* $Id$ */

/*
 * guppi-progress.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.

 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-progress.h"

#include "guppi-convenient.h"


static GtkProgress *global_progress = NULL;

void
guppi_progress_register (GtkProgress * prog)
{
  guppi_unref (global_progress);
  global_progress = prog;
  guppi_ref (global_progress);
}

void
guppi_progress_set_percentage (float x)
{
  if (global_progress) {
    x = CLAMP (x, 0, 1);
    gtk_progress_set_activity_mode (global_progress, FALSE);
    gtk_progress_set_percentage (global_progress, x);
  }
}

void
guppi_progress_show_activity (void)
{
  if (global_progress) {
    gtk_progress_set_activity_mode (global_progress, TRUE);
    gtk_progress_set_percentage (global_progress, 0.5);
  }
}

void
guppi_progress_clear (void)
{
  if (global_progress)
    gtk_progress_set_percentage (global_progress, 0);
}

/* $Id$ */
