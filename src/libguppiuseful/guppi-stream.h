/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-stream.h
 *
 * Copyright (C) 1999 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_STREAM_H
#define _INC_GUPPI_STREAM_H

/* #include <gtk/gtk.h> */
#include "guppi-file.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS

typedef struct _GuppiStream GuppiStream;
typedef struct _GuppiStreamClass GuppiStreamClass;

struct _GuppiStream {
  GtkObject parent;

  GuppiFile *in;

  gboolean bad;
  gboolean buffering;
  gboolean eof;

  gint current_line_no;
  gchar *current_line;
  gchar *current_marked_line;
  gchar *current_sani_line;

  gint last_visited_line_no;

  gint buffer_count;
  gint buffer_top;
  gchar **buffer;
  gchar **marked_buffer;
  gchar **sani_buffer;

  gint total_size;		/* total size, in bytes (if available) */
  gint buffered_size;		/* size in bytes of buffered material */

  gboolean in_ml_comment;

  gchar *eol_comment;
  gchar *ml_start_comment;
  gchar *ml_end_comment;
  gchar start_quote;
  gchar end_quote;
  gchar escape;
};

struct _GuppiStreamClass {
  GtkObjectClass parent_class;

  void (*preload) (GuppiStream *);
  void (*fully_preloaded) (GuppiStream *);
  void (*changed_codes) (GuppiStream *);
};

#define GUPPI_TYPE_STREAM (guppi_stream_get_type())
#define GUPPI_STREAM(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_STREAM,GuppiStream))
#define GUPPI_STREAM_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_STREAM,GuppiStreamClass))
#define GUPPI_IS_STREAM(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_STREAM))
#define GUPPI_IS_STREAM_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_STREAM))

GtkType guppi_stream_get_type (void);

GuppiStream *guppi_stream_new (GuppiFile *);

GuppiStream *guppi_stream_open_file (const gchar *);

gboolean guppi_stream_sequential_mode (GuppiStream *);
void guppi_stream_set_sequential_mode (GuppiStream *);

const gchar *guppi_stream_eol_comment (const GuppiStream *);
void guppi_stream_set_eol_comment (GuppiStream *, const gchar *);

const gchar *guppi_stream_ml_comment_start (const GuppiStream *);
const gchar *guppi_stream_ml_comment_end (const GuppiStream *);
void guppi_stream_set_ml_comment_start (GuppiStream *, const gchar *);
void guppi_stream_set_ml_comment_end (GuppiStream *, const gchar *);

gchar guppi_stream_quote_start (const GuppiStream *);
gchar guppi_stream_quote_end (const GuppiStream *);
void guppi_stream_set_quote_start (GuppiStream *, gchar);
void guppi_stream_set_quote_end (GuppiStream *, gchar);

gchar guppi_stream_escape (const GuppiStream *);

const gchar *guppi_stream_source (GuppiStream *);

gint guppi_stream_number_of_lines (GuppiStream *);
gint guppi_stream_estimated_number_of_lines (GuppiStream *);
gint guppi_stream_number_of_preloaded_lines (GuppiStream *);

void guppi_stream_load_some_lines (GuppiStream *);
void guppi_stream_load_more_lines (GuppiStream *);

const gchar *guppi_stream_get_line (GuppiStream *, gint line_no);
const gchar *guppi_stream_get_marked_line (GuppiStream *, gint line_no);
const gchar *guppi_stream_get_sanitized_line (GuppiStream *, gint line_no);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_STREAM_H */

/* $Id$ */
