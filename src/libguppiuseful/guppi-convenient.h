/* $Id$ */

/*
 * guppi-convenient.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_CONVENIENT_H
#define _INC_GUPPI_CONVENIENT_H

/* #include <gtk/gtk.h> */
#include <gtk/gtkwidget.h>
#include "guppi-memory.h"
#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS 

void guppi_free2 (gpointer, gpointer ignored);
void guppi_free_hash_key (gpointer, gpointer, gpointer);
void guppi_free_hash_val (gpointer, gpointer, gpointer);
void guppi_free_hash_keyval (gpointer, gpointer, gpointer);

void guppi_unref2 (gpointer, gpointer ignored);
void guppi_unref_hash_key (gpointer, gpointer, gpointer);
void guppi_unref_hash_val (gpointer, gpointer, gpointer);
void guppi_unref_hash_keyval (gpointer, gpointer, gpointer);

void guppi_2sort (double *, double *);
void guppi_2sort_i (gint *, gint *);
void guppi_2sort_dt (GDate **, GDate **);
void guppi_3sort (double *, double *, double *);


/* Observe that we consider the half-open interval [x,z) */
#define guppi_between(x,y,z) ((x) <= (y) && (y) < (z))


void guppi_tooltip (GtkWidget * w, const gchar * tip);
#define guppi_tooltips(w,t) guppi_tooltip((w),(t))	/* avoid plural typos */


/* Macros for backwards compatibility */

#ifdef USING_OLD_FONT_API
#define gnome_font_get_size(x) ((x)->size)
#endif

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

/* A bunch of GDate convenience functions. */

#define g_date_invalid(a) (!g_date_valid ((a)))

#define g_date_eq(a, b)   (g_date_compare ((a), (b)) == 0)
#define g_date_neq(a, b)  (g_date_compare ((a), (b)) != 0)
#define g_date_lt(a, b)   (g_date_compare ((a), (b)) < 0)
#define g_date_gt(a, b)   (g_date_compare ((a), (b)) > 0)
#define g_date_lteq(a, b) (g_date_compare ((a), (b)) <= 0)
#define g_date_gteq(a,b ) (g_date_compare ((a), (b)) >= 0)

#define g_date_min(a, b) (g_date_lt((a), (b)) ? (a) : (b))
#define g_date_max(a, b) (g_date_gt((a), (b)) ? (a) : (b))
#define g_date_clamp(x, a, b) (g_date_min (g_date_max ((a), (x)), (b)))

END_GUPPI_DECLS

#endif /* _INC_GUPPI_CONVENIENT_H */

/* $Id$ */
