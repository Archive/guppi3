/* $Id$ */

/*
 * guppi-exit.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 * and Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_EXIT_H
#define _INC_GUPPI_EXIT_H

/* #include <gtk/gtk.h> */
#include <glib.h>
#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS 

typedef gboolean (*GuppiApprovalFunc) (gpointer);
typedef void (*GuppiShutdownFunc) (gpointer);

void guppi_exit_connect_approval_func (GuppiApprovalFunc,
				       gpointer user_data,
				       const gchar * label);
void guppi_exit_disconnect_approval_func (GuppiApprovalFunc,
					  gpointer user_data);

void guppi_exit_connect_shutdown_func (GuppiShutdownFunc,
				       gpointer user_data,
				       const gchar * label);
void guppi_exit_disconnect_shutdown_func (GuppiShutdownFunc,
					  gpointer user_data);

gboolean guppi_shutdown (void);
void guppi_exit (void);
void guppi_abort (void);


END_GUPPI_DECLS

#endif /* _INC_GUPPI_EXIT_H */

/* $Id$ */
