/* $Id$ */

/*
 * guppi-timer.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-timer.h"

#include <time.h>
#include <glib.h>

#include <gtk/gtkmain.h>
#include <gtk/gtksignal.h>

#include "guppi-dharma.h"

static time_t init_time;
static time_t active_basis_time;
static time_t last_heartbeat_time;
static guint elapsed_active_time = 0;
static gboolean idle = FALSE;
#define INACTIVITY_CHECK_INTERVAL 2000
#define INACTIVITY_TIMEOUT 20

static gint
activity_monitor (gpointer p)
{
  time_t t;

  if (!idle) {
    time (&t);
    if (t - last_heartbeat_time > INACTIVITY_TIMEOUT) {
      idle = TRUE;
      elapsed_active_time += last_heartbeat_time - active_basis_time;
    }
  }
  return TRUE;
}

static gboolean
emission_monitor (GtkObject * obj, guint sigid, guint n_params,
		  GtkArg * params, gpointer data)
{
  guppi_timer_heartbeat ();
  guppi_dharma_turn_wheel ();
  return TRUE;
}

void
guppi_timer_init (void)
{
  time (&init_time);
  last_heartbeat_time = init_time;
  active_basis_time = init_time;

  gtk_timeout_add (INACTIVITY_CHECK_INTERVAL, activity_monitor, NULL);

  /* Watch for signals, do a "heartbeat" every time a mouse button 
     is clicked or a key is pressed. */

  gtk_signal_add_emission_hook (gtk_signal_lookup ("button-press-event",
						   GTK_TYPE_WIDGET),
				emission_monitor, NULL);
  gtk_signal_add_emission_hook (gtk_signal_lookup ("key-press-event",
						   GTK_TYPE_WIDGET),
				emission_monitor, NULL);
}

void
guppi_timer_terminate (void)
{

}

void
guppi_timer_heartbeat (void)
{
  time (&last_heartbeat_time);
  if (idle) {
    idle = FALSE;
    active_basis_time = last_heartbeat_time;
  }
}

guint
guppi_timer_total_elapsed_time (void)
{
  time_t t;
  time (&t);
  return (guint) (t - init_time);
}

guint
guppi_timer_total_active_time (void)
{
  time_t t;
  guint tot = elapsed_active_time;
  if (!idle) {
    time (&t);
    tot += (guint) (t - active_basis_time);
  }
  return tot;
}


/* $Id$ */
