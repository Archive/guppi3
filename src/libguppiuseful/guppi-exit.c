/* $Id$ */

/*
 * guppi-exit.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 * and Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-exit.h"

#include <gtk/gtkmain.h>
#include <guppi-debug.h>
#include "guppi-memory.h"


typedef struct _GuppiApprovalPair GuppiApprovalPair;
struct _GuppiApprovalPair {
  GuppiApprovalFunc func;
  gpointer user_data;
  gchar *label;
};

static GList *approval_list = NULL;

typedef struct _GuppiShutdownPair GuppiShutdownPair;
struct _GuppiShutdownPair {
  GuppiShutdownFunc func;
  gpointer user_data;
  gchar *label;
};

static GList *shutdown_list = NULL;

/**********************************************************************/

void
guppi_exit_connect_approval_func (GuppiApprovalFunc fn,
				  gpointer data, const gchar * label)
{
  GuppiApprovalPair *pair;
  g_return_if_fail (fn != NULL);

  pair = guppi_new (GuppiApprovalPair, 1);
  pair->func = fn;
  pair->user_data = data;
  pair->label = guppi_strdup (label);

  approval_list = g_list_prepend (approval_list, pair);
}

void
guppi_exit_disconnect_approval_func (GuppiApprovalFunc fn, gpointer data)
{
  GList *iter;
  GList *next;
  GuppiApprovalPair *pair;
  g_return_if_fail (fn != NULL);

  iter = approval_list;
  while (iter != NULL) {
    next = g_list_next (iter);
    pair = (GuppiApprovalPair *) iter->data;
    if (pair->func == fn && pair->user_data == data) {
      guppi_free (pair->label);
      guppi_free (pair);
      g_list_remove_link (approval_list, iter);
      g_list_free_1 (iter);
    }
    iter = next;
  }
}

void
guppi_exit_connect_shutdown_func (GuppiShutdownFunc fn,
				  gpointer data, const gchar * label)
{
  GuppiShutdownPair *pair;
  g_return_if_fail (fn != NULL);

  pair = guppi_new (GuppiShutdownPair, 1);
  pair->func = fn;
  pair->user_data = data;
  pair->label = guppi_strdup (label);

  shutdown_list = g_list_prepend (shutdown_list, pair);
}

void
guppi_exit_disconnect_shutdown_func (GuppiShutdownFunc fn, gpointer data)
{
  GList *iter;
  GList *next;
  GuppiShutdownPair *pair;
  g_return_if_fail (fn != NULL);

  iter = shutdown_list;
  while (iter != NULL) {
    next = g_list_next (iter);
    pair = (GuppiShutdownPair *) iter->data;
    if (pair->func == fn && pair->user_data == data) {
      guppi_free (pair->label);
      guppi_free (pair);
      g_list_remove_link (shutdown_list, iter);
      g_list_free_1 (iter);
    }
    iter = next;
  }
}

/**********************************************************************/

static gboolean
approve_shutdown (void)
{
  GuppiApprovalPair *pair;
  GList *iter = approval_list;

  while (iter != NULL) {
    pair = (GuppiApprovalPair *) iter->data;
    if (pair != NULL) {
      guppi_msg_vv ("Checking approval function: %s", pair->label);
      if (!pair->func (pair->user_data))
	return FALSE;
    }

    iter = g_list_next (iter);
  }

  return TRUE;
}

static void
do_shutdown (void)
{
  GuppiShutdownPair *pair;
  GList *iter = shutdown_list;

  while (iter != NULL) {
    pair = (GuppiShutdownPair *) iter->data;
    if (pair != NULL) {
      guppi_msg_v ("Calling shutdown function: %s", pair->label);
      pair->func (pair->user_data);
      guppi_msg_vv ("Shutdown function \"%s\" complete", pair->label);
    }

    iter = g_list_next (iter);
  }
}

static void
final_phase_shutdown (gint code)
{
  GList *iter;

  /* Clean up our allocated memory. */
  for (iter = approval_list; iter != NULL; iter = g_list_next (iter)) {
    GuppiApprovalPair *pair = (GuppiApprovalPair *)iter->data;
    guppi_free (pair->label);
    guppi_free (pair);
  }
  g_list_free (approval_list);

  for (iter = shutdown_list; iter != NULL; iter = g_list_next (iter)) {
    GuppiShutdownPair *pair = (GuppiShutdownPair *)iter->data;
    guppi_free (pair->label);
    guppi_free (pair);
  }
     
  /* We do some automatically */
  if (gtk_main_level () > 0) {
    guppi_msg_vv ("Calling gtk_main_quit()");
    gtk_main_quit ();
  }

  if (guppi_is_verbose ())
    guppi_msg_vv ("Shutdown complete.");

  exit (code);

}

gboolean
guppi_shutdown (void)
{
  if (guppi_is_verbose ())
    guppi_msg_v ("Exiting Guppi...");

  if (approve_shutdown ()) {
    do_shutdown ();
    return TRUE;
  } else
    guppi_msg_v ("Shutdown cancelled.");

  return FALSE;
}

void
guppi_exit (void)
{
  if (guppi_shutdown ())
    final_phase_shutdown (0);
}

void
guppi_abort (void)
{
  guppi_msg_v ("Aborting Guppi...");
  do_shutdown ();
  final_phase_shutdown (-1);
}


/* $Id$ */
