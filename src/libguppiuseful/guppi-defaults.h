/* $Id$ */

/*
 * guppi-defaults.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.

 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_DEFAULTS_H
#define _INC_GUPPI_DEFAULTS_H

/* #include <gtk/gtk.h> */
#include <libgnomeprint/gnome-font.h>
#include "guppi-metrics.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS

/* Someday I'll use gconf for this.  Until then, I'm just encapsulating
   this information in the simplest way possible... */
GnomeFont * guppi_default_font_large (void);
GnomeFont *guppi_default_font_medium (void);
GnomeFont *guppi_default_font_small (void);

GnomeFont *guppi_default_font (void);	/* same as guppi_default_font_medium() */

/* ugly ugly ugly */
double guppi_default_axis_label_size (void);
double guppi_default_plot_label_size (void);


guppi_metric_t guppi_default_units (void);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_DEFAULTS_H */

/* $Id$ */
