/* $Id$ */

/*
 * guppi-useful.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 * and Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_USEFUL_H
#define _INC_GUPPI_USEFUL_H

#include  "guppi-convenient.h"
#include  "guppi-debug.h"
#include  "guppi-dialogs.h"
#include  "guppi-defaults.h"
#include  "guppi-enums.h"
#include  "guppi-file.h"
#include  "guppi-matrix.h"
#include  "guppi-metrics.h"
#include  "guppi-paths.h"
#include  "guppi-plug-in-spec.h"
#include  "guppi-plug-in.h"
#include  "guppi-rgb.h"
#include  "guppi-stream.h"
#include  "guppi-stream-preview.h"
#include  "guppi-string.h"
#include  "guppi-version.h"

#include  "guppi-useful-init.h"

#endif /* _INC_GUPPI_USEFUL_H */

/* $Id$ */
