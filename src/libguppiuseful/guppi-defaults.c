/* $Id$ */

/*
 * guppi-defaults.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.

 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-defaults.h"

#include <unistd.h>
#include "guppi-memory.h"

static void
cant_find_default_font (void)
{
  g_print ("I can't find the default fonts needed by Guppi.\n");
  g_print ("In 99%% of all cases, this means that there is a problem with\n");
  g_print ("your installed version of gnome-print.\n");
  exit(-1);
}

GnomeFont *
guppi_default_font_large (void)
{
  static GnomeFont *font = NULL;

  if (font == NULL) {
    font = gnome_font_new ("Helvetica", 20.0);
    if (font == NULL)
      cant_find_default_font ();
  }

  guppi_ref (font);
  return font;
}

GnomeFont *
guppi_default_font_medium (void)
{
  static GnomeFont *font = NULL;

  if (font == NULL) {
    font = gnome_font_new ("Helvetica", 12.0);
    if (font == NULL)
      cant_find_default_font ();
  }

  guppi_ref (font);
  return font;
}

GnomeFont *
guppi_default_font_small (void)
{
  static GnomeFont *font = NULL;

  if (font == NULL) {
    font = gnome_font_new ("Helvetica", 9.0);
    if (font == NULL)
      cant_find_default_font ();
  }

  guppi_ref (font);
  return font;
}

GnomeFont *
guppi_default_font (void)
{
  return guppi_default_font_medium ();
}

double
guppi_default_axis_label_size (void)
{
  return 14.0;
}

double
guppi_default_plot_label_size (void)
{
  return 12.0;
}


guppi_metric_t
guppi_default_units (void)
{
  return GUPPI_IN;		/* I'll be Anglocentric for now... */
}


/* $Id$ */
