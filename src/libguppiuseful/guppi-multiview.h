/* $Id$ */

/* guppi-multiview.h
 *
 * Copyright (C) 2000  Jonathan Blandford
 *
 * Modified for use in Guppi by Jon Trowbridge
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GUPPI_MULTIVIEW_H__
#define __GUPPI_MULTIVIEW_H__

/* #include <gtk/gtk.h> */
#include <gtk/gtkcontainer.h>

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS

#define GUPPI_TYPE_MULTIVIEW			(guppi_multiview_get_type ())
#define GUPPI_MULTIVIEW(obj)			(GTK_CHECK_CAST ((obj), GUPPI_TYPE_MULTIVIEW, GuppiMultiview))
#define GUPPI_MULTIVIEW_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), GUPPI_TYPE_MULTIVIEW, GuppiMultiviewClass))
#define GUPPI_IS_MULTIVIEW(obj)			(GTK_CHECK_TYPE ((obj), GUPPI_TYPE_MULTIVIEW))
#define GUPPI_IS_MULTIVIEW_CLASS(klass)		(GTK_CHECK_CLASS_TYPE ((obj), GUPPI_TYPE_MULTIVIEW))
  typedef struct _GuppiMultiview GuppiMultiview;
  typedef struct _GuppiMultiviewClass GuppiMultiviewClass;

  struct _GuppiMultiview {
    GtkContainer parent;

    /*< private > */
    GtkWidget *current;
    GList *children;
  };

  struct _GuppiMultiviewClass {
    GtkContainerClass parent_class;
  };

  GtkType guppi_multiview_get_type (void);
  GtkWidget *guppi_multiview_new (void);
  void guppi_multiview_prepend_child (GuppiMultiview *multiview,
				      GtkWidget *child);
  void guppi_multiview_insert_child (GuppiMultiview *multiview,
				     GtkWidget *back_child,
				     GtkWidget *child);
  void guppi_multiview_append_child (GuppiMultiview *multiview,
				     GtkWidget *child);
  gboolean guppi_multiview_contains (GuppiMultiview *multiview,
				     GtkWidget *child);
  void guppi_multiview_set_current (GuppiMultiview *multiview,
				    GtkWidget *child);


END_GUPPI_DECLS

#endif /* __GUPPI_MULTIVIEW_H__ */

/* $Id$ */
