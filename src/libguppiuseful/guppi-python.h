/* $Id$ */

/*
 * guppi-python.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Andrew H. Chatham <andrew.chatham@duke.edu> and
 * Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_PYTHON_H
#define _INC_GUPPI_PYTHON_H

/* #include <gnome.h> */

/* A hack --- Python does some funky typedefs that cause namespace
   collisions with guile, so we have to make sure to include this first... */
#include "guppi-guile.h"

#include <Python.h>

#include "guppi-enums.h"
#include "guppi-fn-wrapper.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS

gint guppi_python_eval_str (const char *expression);
gint guppi_python_eval_file (const char *filename);

gboolean guppi_python_is_active (void);
void guppi_python_init (void);

int py_stringv_p (PyObject * o);
int py_color_p (PyObject * o);
int py_compass_check (PyObject * o);

gchar **py2stringv (PyObject * o);
int py2color (PyObject * o, void *addr);
int py2compass (PyObject * o, void *addr);
GtkArg *py2gtk_arg (PyObject * name, PyObject * val);
GtkArg *py2argv (PyObject * list, guint * length);

void py_argv_free (GtkArg *, guint len);

PyObject *cstr2py (const gchar *);
PyObject *stringv2py (gchar **);	//returns a tuple
PyObject *color2py (guint32 c);
PyObject *compass2py (guppi_compass_t c);
PyObject *gtk_arg2py (GtkArg *);

#define PY_ASSERT_ARG(_cond, _arg, _argpos, _funcname) \
       if (!(_cond)) {                                     \
           Py_Arg_Exception(_arg, _argpos, _funcname); \
           return NULL;                                    \
       }

/* Taken from PyGTK */

typedef struct {
  PyObject_HEAD GtkObject *obj;
} PyGuppi_Object;

struct _PyGuppi_FunctionStruct {
  char *pygtkVersion;
  gboolean fatalExceptions;

  PyObject *(*argsAsTuple) (int nparams, GtkArg * args);
  int (*argsFromSequence) (GtkArg * args, int nparams, PyObject * seq);
  int (*argFromPyObject) (GtkArg * arg, PyObject * obj);
  PyObject *(*argAsPyObject) (GtkArg * arg);
  void (*retFromPyObject) (GtkArg * ret, PyObject * obj);
  PyObject *(*retAsPyObject) (GtkArg * ret);
  GtkArg *(*dictAsGtkArgs) (PyObject * dict, GtkType type, gint * nargs);

    gint (*enum_get_value) (GtkType enum_type, PyObject * obj, int *val);

  PyTypeObject *gtk_type;
  PyObject *(*gtk_new) (GtkObject * obj);


};


#define PyGuppi_Get(v) (((PyGuppi_Object *)(v))->obj)

// gint PyGuppiEnum_get_value(GtkType enum_type, PyObject *obj, int *val);

#ifdef IN_GUPPI_PYTHON_C
staticforward PyTypeObject PyGuppi_Type;

struct _PyGuppi_FunctionStruct *_PyGuppi_API;

#define PyGuppi_Check(v) ((v)->ob_type == &PyGuppi_Type)
static PyObject *PyGuppi_New (GtkObject * obj);

static PyObject *GtkArgs_AsTuple (int nparams, GtkArg * args);

static int GtkArgs_FromSequence (GtkArg * args, int nparams, PyObject * seq);
static int GtkArg_FromPyObject (GtkArg * arg, PyObject * obj);

static PyObject *GtkArg_AsPyObject (GtkArg * arg);
static void GtkRet_FromPyObject (GtkArg * ret, PyObject * py_ret);
static PyObject *GtkRet_AsPyObject (GtkArg * arg);
static GtkArg *PyDict_AsGtkArgs (PyObject * dict, GtkType type, gint * nargs);


static gboolean PyGuppi_FatalExceptions = FALSE;

#else //IN_GUPPI_PYTHON_C

extern struct _PyGuppi_FunctionStruct *_PyGuppi_API;

#define PyGuppi_Check(v) ((v)->ob_type == _PyGuppi_API->gtk_type)
#define PyGuppi_New              (_PyGuppi_API->gtk_new)
#define GtkArgs_AsTuple (_PyGuppi_API->argsAsTuple)
#define GtkArgs_FromSequence (_PyGuppi_API->argsFromSequence)
#define GtkArg_FromPyObject (_PyGuppi_API->argFromPyObject)
#define GtkArg_AsPyObject (_PyGuppi_API->argAsPyObject)
#define GtkRet_FromPyObject (_PyGuppi_API->retFromPyObject)
#define GtkRet_AsPyObject (_PyGuppi_API->retAsPyObject)
#define PyDict_AsGtkArgs (_PyGuppi_API->dictAsGtkArgs)
#define PyGuppiEnum_get_value (_PyGuppi_API->enum_get_value)
#define PyGuppi_FatalExceptions (_PyGuppi_API->fatalExceptions)

#define init_pyguppi() { \
    PyObject *pyguppi = PyImport_ImportModule("_guppi"); \
    if (pyguppi != NULL) { \
        PyObject *module_dict = PyModule_GetDict(pyguppi); \
        PyObject *cobject = PyDict_GetItemString(module_dict, "_PyGuppi_API"); \
        if (PyCObject_Check(cobject)) \
            _PyGuppi_API = PyCObject_AsVoidPtr(cobject); \
        else { \
            Py_FatalError("could not find _PyGuppi_API object"); \
            return; \
        } \
    } else { \
        Py_FatalError("could not import _guppi"); \
        return; \
    } \
}

#define PyGuppi_Type              *(_PyGuppi_API->gtk_type)

#endif //IN_GUPPI_PYTHON_C

GuppiFnWrapper *py2fn_d__i (PyObject * proc);
GuppiFnWrapper *py2fn_d__i_d (PyObject * proc);
GuppiFnWrapper *py2fn_d__d (PyObject * proc);
GuppiFnWrapper *py2fn_d__d_d (PyObject * proc);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_PYTHON_H */

/* $Id$ */
