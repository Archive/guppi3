/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-file.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_FILE_H
#define _INC_GUPPI_FILE_H

/* #include <gtk/gtk.h> */
#include <gtk/gtkobject.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <zlib.h>

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS

typedef struct _GuppiFile GuppiFile;
typedef struct _GuppiFileClass GuppiFileClass;

struct _GuppiFile {
  GtkObject parent;

  gchar *filename;
  gint base_fd;
  gzFile gzfd;
};

struct _GuppiFileClass {
  GtkObjectClass parent_class;
};

#define GUPPI_TYPE_FILE (guppi_file_get_type())
#define GUPPI_FILE(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_FILE,GuppiFile))
#define GUPPI_FILE0(obj) ((obj) ? (GUPPI_FILE(obj)) : NULL)
#define GUPPI_FILE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_FILE,GuppiFileClass))
#define GUPPI_IS_FILE(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_FILE))
#define GUPPI_IS_FILE0(obj) (((obj) == NULL) || (GUPPI_IS_FILE(obj)))
#define GUPPI_IS_FILE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_FILE))

GtkType      guppi_file_get_type (void);

GuppiFile   *guppi_file_open     (const gchar * path);
GuppiFile   *guppi_file_fdopen   (gint file_des);
void         guppi_file_close    (GuppiFile *);

const gchar *guppi_file_filename (GuppiFile *);

gsize        guppi_file_read     (GuppiFile *gf, gpointer ptr, gsize size, gsize nmemb);
gint         guppi_file_getc     (GuppiFile *gf);
gchar       *guppi_file_gets     (GuppiFile *gf, gchar *s, gsize size);

gsize        guppi_file_peek     (GuppiFile *gf, gpointer ptr, gsize size);

gint         guppi_file_seek     (GuppiFile *gf, glong offset, gint whence);
glong        guppi_file_tell     (GuppiFile *gf);
void         guppi_file_rewind   (GuppiFile *gf);

gint         guppi_file_stat     (GuppiFile *gf, struct stat *buf);

gboolean     guppi_file_eof      (GuppiFile *gf);
const gchar *guppi_file_error    (GuppiFile *gf);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_FILE_H */

/* $Id$ */
