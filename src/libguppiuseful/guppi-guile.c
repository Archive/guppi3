/* This is  -*- C -*- */

/* 
 * guppi-guile.c
 *
 * Copyright (C) 1998, 1999, 2000 EMC Capital Management, Inc.
 * Portions Copyright (C) 1998 Maciej Stachowiak and Greg J. Badros
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <config.h>
#include "guppi-guile.h"

/* #include <gnome.h> */
#include <libguile/fluids.h>
#include <libguile/version.h>
#ifdef USING_GUILE_GNOME_BINDINGS
#include <guile-gtk.h>
#endif

#include "guppi-memory.h"
#include "guppi-rgb.h"
#include "guppi-paths.h"
#include "guppi-plug-in-spec.h"


void
guppi_scm_tagged_error (const char *tag, const char *subr, const char *msg)
{
  scm_error (gh_symbol2scm ((char *) tag), (char *) subr,
	     "%s",
	     gh_list (gh_str02scm ((gchar *) msg), SCM_UNDEFINED),
	     gh_list (gh_str02scm ((gchar *) msg), SCM_UNDEFINED));
}

void
guppi_scm_error (const char *subr, const char *msg)
{
  guppi_scm_tagged_error ("guppi-error", subr, msg);
}

/****************************************************************************/
/* BEGIN Code directly from Guile source tree (internals).  Until we
   work with the Guile developers to figure out what functionality we
   really need and what the public API should be, we're just
   copy/pasting, more or less. */

/*
  NOTES:

  All the below stuff is from SCWM. I don't really understand 
  Guile internals well enough to know what the hell I'm typing
  in, but this feature is needed, so...
  Copyright 1998 Maciej Stachowiak and Greg J. Badros
  I did s/scwm/guppi/g just for looks ;-)

  I hacked on this some to get it working with guile 1.4; the changes
  still seem to work with 1.3.4. -JT

  Simplified to remove unsed code, and add conditional support for
  guile 1.5/1.6. The 1.5/1.6 code is not from SCWM, but rather
  directly from the Guile source tree. (rlb)

  For further documentation/understanding of what this code is doing,
  please see the Guile documentation and source code, esp the C code
  in Guile's libguile/.

  This code basically implements Guile's call-with-dynamic-root, but
  does *not* rewind the dynamic-wind stack, hence our name
  cwdr-no-unwind below.

*/

struct cwdr_no_unwind_handler_data {
  /* Do we need to run the handler? */
  int run_handler;

  /* The tag and args to pass it. */
  SCM tag, args;
};

/* Record the fact that the body of the cwdr has thrown.  Record
   enough information to invoke the handler later when the dynamic
   root has been deestablished.  */

static SCM
cwdr_no_unwind_handler (void *data, SCM tag, SCM args)
{
  struct cwdr_no_unwind_handler_data *c = 
    (struct cwdr_no_unwind_handler_data *) data;

  c->run_handler = 1;
  c->tag = tag;
  c->args = args;
  return SCM_UNSPECIFIED;
}

#if defined(SCM_MAJOR_VERSION) && \
    (SCM_MAJOR_VERSION >= 1) && (SCM_MINOR_VERSION >= 5)
#define GUPPI_USING_NEWER_GUILE
#endif

#ifdef GUPPI_USING_NEWER_GUILE
#define DEREF_LAST_STACK \
  scm_fluid_ref(scm_variable_ref(scm_the_last_stack_fluid_var))
#else
# define DEREF_LAST_STACK scm_fluid_ref(gh_cdr(scm_the_last_stack_fluid))
typedef scm_catch_body_t scm_t_catch_body;
typedef scm_catch_handler_t scm_t_catch_handler;
#endif

#

/* {call-with-dynamic-root (no-unwind)}
 *
 * Suspending the current thread to evaluate a thunk on the
 * same C stack but under a new root.
 *
 * Calls to call-with-dynamic-root return exactly once (unless
 * the process is somehow exited).
 *
 */

/* This is the basic code for new root creation.
 *
 * WARNING!  The order of actions in this routine is in many ways
 * critical.  E. g., it is essential that an error doesn't leave Guile
 * in a messed up state.  */

#ifdef GUPPI_USING_NEWER_GUILE
/* we pick a number here that hopefully won't conflict with guile's. */
static long n_dynamic_roots = 100000;
#endif

static SCM 
scm_internal_cwdr_no_unwind (scm_t_catch_body body, void *body_data,
                             scm_t_catch_handler handler, void *handler_data,
                             SCM_STACKITEM *stack_start)
{
  int old_ints_disabled = scm_ints_disabled;
  SCM old_rootcont;
  struct cwdr_no_unwind_handler_data my_handler_data;

#ifdef GUPPI_USING_NEWER_GUILE
  SCM answer;
#else
  SCM answer = SCM_UNSPECIFIED;
#endif

  /* Create a fresh root continuation.  */
#ifdef GUPPI_USING_NEWER_GUILE
  {
    SCM new_rootcont;

    SCM_REDEFER_INTS;
    {
      scm_t_contregs *contregs = scm_must_malloc (sizeof (scm_t_contregs),
						"inferior root continuation");

      contregs->num_stack_items = 0;
      contregs->dynenv = SCM_EOL;
      contregs->base = stack_start;
      contregs->seq = ++n_dynamic_roots;
      contregs->throw_value = SCM_BOOL_F;
#ifdef DEBUG_EXTENSIONS
      contregs->dframe = 0;
#endif
      SCM_NEWSMOB (new_rootcont, scm_tc16_continuation, contregs);
    }
    old_rootcont = scm_rootcont;
    scm_rootcont = new_rootcont;
    SCM_REALLOW_INTS;
  }
#else /* !GUPPI_USING_NEWER_GUILE */
  {
    SCM new_rootcont;
    SCM_NEWCELL (new_rootcont);
    SCM_REDEFER_INTS;
    SCM_SETJMPBUF (new_rootcont,
		   scm_must_malloc ((long) sizeof (scm_contregs),
				    "inferior root continuation"));
    SCM_SETCAR (new_rootcont, scm_tc7_contin);
    SCM_DYNENV (new_rootcont) = SCM_EOL;
    SCM_BASE (new_rootcont) = stack_start;
    SCM_SEQ (new_rootcont) = (SCM) - 1;	/* warning: neg value in unsigned int */
#ifdef DEBUG_EXTENSIONS
    SCM_DFRAME (new_rootcont) = 0;
#endif
    old_rootcont = scm_rootcont;
    scm_rootcont = new_rootcont;
    SCM_REALLOW_INTS;
  }
#endif /* !GUPPI_USING_NEWER_GUILE */


#ifdef DEBUG_EXTENSIONS
  SCM_DFRAME (old_rootcont) = scm_last_debug_frame;
  scm_last_debug_frame = 0;
#endif

  {
    my_handler_data.run_handler = 0;
    answer = scm_internal_catch (SCM_BOOL_T,
				 body, body_data,
				 cwdr_no_unwind_handler, &my_handler_data);
  }

  SCM_REDEFER_INTS;

#ifndef GUPPI_USING_NEWER_GUILE
#ifdef USE_STACKCJMPBUF
  SCM_SETJMPBUF (scm_rootcont, NULL);
#endif
#endif

#ifdef DEBUG_EXTENSIONS
  scm_last_debug_frame = SCM_DFRAME (old_rootcont);
#endif
  scm_rootcont = old_rootcont;
  SCM_REALLOW_INTS;
  scm_ints_disabled = old_ints_disabled;

  /* Now run the real handler iff the body did a throw. */
  if (my_handler_data.run_handler)
    return handler (handler_data, my_handler_data.tag, my_handler_data.args);
  else
    return answer;
}

/* END Code directly from Guile source tree (internals). */
/****************************************************************************/


/* Stuff from callbacks.c */

struct guppi_body_apply_data {
  SCM proc;
  SCM args;
};

static SCM
guppi_handle_error (void *data, SCM tag, SCM throw_args)
{
  SCM port = scm_def_errp;

  /* FIXME: figure out what this conditional means. */
  if (scm_ilength (throw_args) >= 3) {
    SCM stack = DEREF_LAST_STACK;
    SCM subr = gh_car (throw_args);
    SCM message = SCM_CADR (throw_args);
    SCM args = SCM_CADDR (throw_args);

    scm_newline (port);
    scm_display_backtrace (stack, port, SCM_UNDEFINED, SCM_UNDEFINED);
    scm_newline (port);
    scm_display_error (stack, port, subr, message, args, SCM_EOL);
    return SCM_BOOL_F;
  } else {
    scm_puts ("uncaught throw to ", port);
    scm_prin1 (tag, port, 0);
    scm_puts (": ", port);
    scm_prin1 (throw_args, port, 1);
    scm_putc ('\n', port);
    exit (2);
  }
  return SCM_UNSPECIFIED;	/* guppi returns error hook results */
}

static SCM
guppi_body_apply (void *body_data)
{
  struct guppi_body_apply_data *ad =
    (struct guppi_body_apply_data *) body_data;
  return gh_apply (ad->proc, ad->args);
}


/* Use scm_internal_cwdr to establish a new dynamic root - this causes
   all throws to be caught and prevents continuations from exiting the
   dynamic scope of the callback. This is needed to prevent callbacks
   from disrupting guppi's flow control, which would likely cause a
   crash. Use scm_internal_stack_catch to save the stack so we can
   display a backtrace. scm_internal_stack_cwdr is the combination of
   both. Note that the current implementation causes three(!) distinct
   catch-like constructs to be used; this may have negative, perhaps
   even significantly so, performance implications. */

struct cwssdr_data {
  SCM tag;
  scm_catch_body_t body;
  void *data;
  scm_catch_handler_t handler;
};

static SCM
cwssdr_body (void *data)
{
  struct cwssdr_data *d = (struct cwssdr_data *) data;
  return scm_internal_stack_catch (d->tag, d->body, d->data, d->handler,
				   NULL);
}

static SCM
scm_internal_stack_cwdr (scm_catch_body_t body,
			 void *body_data,
			 scm_catch_handler_t handler,
			 void *handler_data, SCM_STACKITEM * stack_item)
{
  struct cwssdr_data d;
  d.tag = SCM_BOOL_T;
  d.body = body;
  d.data = body_data;
  d.handler = handler;
  return scm_internal_cwdr_no_unwind (cwssdr_body, &d,
				      handler, handler_data, stack_item);
}

SCM
guppi_safe_apply_thunk (SCM proc)
{
  SCM_STACKITEM stack_item;
  struct guppi_body_apply_data apply_data;

  apply_data.proc = proc;
  apply_data.args = SCM_EOL;

  return scm_internal_stack_cwdr (guppi_body_apply, &apply_data,
				  guppi_handle_error, (gpointer) "guppi",
				  &stack_item);
}

SCM
guppi_safe_apply (SCM proc, SCM args)
{
  SCM_STACKITEM stack_item;
  struct guppi_body_apply_data apply_data;

  apply_data.proc = proc;
  apply_data.args = args;

  return scm_internal_stack_cwdr (guppi_body_apply, &apply_data,
				  guppi_handle_error, (gpointer) "guppi",
				  &stack_item);
}


SCM
guppi_safe_apply_message_only (SCM proc, SCM args)
{
  SCM_STACKITEM stack_item;
  struct guppi_body_apply_data apply_data;

  apply_data.proc = proc;
  apply_data.args = args;

  return scm_internal_cwdr_no_unwind (guppi_body_apply, &apply_data,
				      scm_handle_by_message_noexit,
				      (gpointer) "guppi", &stack_item);
}


SCM
guppi_safe_call0 (SCM thunk)
{
  return guppi_safe_apply (thunk, SCM_EOL);
}


SCM
guppi_safe_call1 (SCM proc, SCM arg)
{
  /* This means w must cons (albeit only once) on each callback of
     size one - seems lame. */
  return guppi_safe_apply (proc, gh_list (arg, SCM_UNDEFINED));
}

SCM
guppi_safe_call2 (SCM proc, SCM arg1, SCM arg2)
{
  /* This means w must cons (albeit only once) on each callback of
     size two - seems lame. */
  return guppi_safe_apply (proc, gh_list (arg1, arg2, SCM_UNDEFINED));
}

SCM
guppi_safe_call3 (SCM proc, SCM arg1, SCM arg2, SCM arg3)
{
  /* This means w must cons (albeit only once) on each callback of
     size two - seems lame. */
  return guppi_safe_apply (proc, gh_list (arg1, arg2, arg3, SCM_UNDEFINED));
}

/* Slightly tricky - we want to catch errors per expression, but only
   establish a new dynamic root per load operation, as it's perfectly
   OK for a file to invoke a continuation created by a different
   expression in the file as far as guppi is concerned. So we set a
   dynamic root for the whole load operation, but also catch on each
   eval. */


static SCM
guppi_body_eval_x (void *body_data)
{
  SCM expr = *(SCM *) body_data;
#ifdef GUPPI_USING_NEWER_GUILE
  return scm_eval_x (expr, scm_current_module());
#else
  return scm_eval_x (expr);
#endif
}


inline static SCM
guppi_catching_eval_x (SCM expr)
{
  return scm_internal_stack_catch (SCM_BOOL_T, guppi_body_eval_x, &expr,
				   guppi_handle_error, (gpointer) "guppi");
}

inline static SCM
guppi_catching_load_from_port (SCM port)
{
  SCM expr;
  SCM answer = SCM_UNSPECIFIED;

  while (!SCM_EOF_OBJECT_P (expr = scm_read (port))) {
    answer = guppi_catching_eval_x (expr);
  }
  scm_close_port (port);

  return answer;
}

static SCM
guppi_body_load (void *body_data)
{
  SCM filename = *(SCM *) body_data;
  SCM port = scm_open_file (filename, gh_str02scm ("r"));
  return guppi_catching_load_from_port (port);
}

static SCM
guppi_body_eval_str (void *body_data)
{
  char *string = (char *) body_data;
  SCM port = scm_mkstrport (SCM_MAKINUM (0), gh_str02scm (string),
			    SCM_OPN | SCM_RDNG, "guppi_eval_str");
  return guppi_catching_load_from_port (port);
}

GUPPI_PROC (safe_load, "guppi-safe-load", 1, 0, 0, (SCM fname))
     /* Load file FNAME while trapping and displaying errors.
Each individual top-level-expression is evaluated separately and all
errors are trapped and displayed.  You Should use this procedure if
you need to make sure most of a file loads, even if it may contain
errors. */
{
  SCM_STACKITEM stack_item;
  if (!gh_string_p (fname)) {
    scm_wrong_type_arg (str_safe_load, 1, fname);
  }

  return scm_internal_cwdr_no_unwind (guppi_body_load, &fname,
				      scm_handle_by_message_noexit,
				      (gpointer) "guppi", &stack_item);
}

SCM
guppi_safe_load (char *filename)
{
  return scm_fn_safe_load (gh_str02scm (filename));
}

SCM
guppi_eval_str (const char *exp)
{
  SCM_STACKITEM stack_item;
  return scm_internal_cwdr_no_unwind (guppi_body_eval_str,
				      (char *) exp,
				      scm_handle_by_message_noexit,
				      "guppi", &stack_item);
}


/* These two functions are also in ../corba_guppi.cc, which is lame. */

#ifdef USE_UNUSED
static SCM
make_output_strport (char *fname)
{
  return scm_mkstrport (SCM_INUM0, scm_make_string (SCM_MAKINUM (30),
						    SCM_UNDEFINED),
			SCM_OPN | SCM_WRTNG, fname);
}
#endif

#ifdef USE_UNUSED
static SCM
get_strport_string (SCM port)
{
  /*  SCM answer; */

  return scm_strport_to_string (port);
  /*
     This depended on guile 1.3 internals, and thus became highly broken
     when 1.3.4 rolled around...

     {
     gh_defer_ints();
     answer = scm_makfromstr (SCM_CHARS (gh_cdr (SCM_STREAM (port))),
     SCM_INUM (gh_car (SCM_STREAM (port))),
     0);
     gh_allow_ints();
     }
     return answer;
   */
}
#endif

/* OutputContext is an abstraction for evaling an expression
   and collecting its output.*/

struct OutputContext {
  SCM o_port, e_port;
  SCM saved_def_e_port;
};

#ifdef USE_UNUSED
static void
init_output_context (struct OutputContext *oc)
{
  /* Temporarily redirect output and error to string ports. 
     Note that the port setting functions return the current previous
     port. */
  oc->o_port =
    scm_set_current_output_port (make_output_strport ("guile_eval"));
  oc->e_port =
    scm_set_current_error_port (make_output_strport ("guile_eval"));

  /* Workaround for a problem with older Guiles */
  oc->saved_def_e_port = scm_def_errp;
  scm_def_errp = scm_current_error_port ();
}
#endif

/* These could both be made faster, but this is fine for small enums */
gboolean
guppi_string_to_enum_val (const ScmEnumInfo * info, const char *str,
			  gint * retval)
{
  int i = 0;
  while (i < info->nvals) {
    if (g_strcasecmp (info->vals[i].name, str) == 0) {
      *retval = info->vals[i].val;
      return TRUE;
    }

    ++i;
  }

  return FALSE;
}

gboolean
guppi_enum_val_to_string (const ScmEnumInfo * info, gint val,
			  const char **retval)
{
  int i = 0;
  while (i < info->nvals) {
    if (info->vals[i].val == val) {
      *retval = info->vals[i].name;
      return TRUE;
    }

    ++i;
  }

  return FALSE;
}

void
guppi_scm_thunk_cb (gpointer widget, gpointer thunk)
{
  SCM proc;

  g_return_if_fail (thunk != NULL);

  proc = (SCM) thunk;
  g_return_if_fail (gh_procedure_p (proc));

  guppi_safe_apply_thunk (proc);
}

void
guppi_scm_eval_cb (gpointer widget, gpointer eval_str)
{
  g_return_if_fail (eval_str != NULL);

  guppi_eval_str ((const gchar *) eval_str);
}

/**********************************************************************/

gboolean
scm_stringv_p (SCM x)
{
  if (!gh_list_p (x))
    return FALSE;

  while (!gh_null_p (x)) {
    if (!gh_string_p (gh_car (x)))
      return FALSE;
    x = gh_cdr (x);
  }
  return TRUE;
}

SCM
stringv2scm (gchar ** vec)
{
  SCM x = SCM_EOL;
  gint i = 0;
  if (vec == NULL)
    return SCM_EOL;
  while (vec[i])
    ++i;
  --i;
  while (i >= 0) {
    x = gh_cons (gh_str02scm (vec[i]), x);
    --i;
  }
  return x;
}

gchar **
scm2stringv (SCM x)
{
  SCM y = x;
  gchar **v;
  gint i = 0;
  while (!gh_null_p (y)) {
    ++i;
    y = gh_cdr (y);
  }
  v = guppi_new (gchar *, i + 1);
  v[i] = NULL;
  while (!gh_null_p (x)) {
    gchar *ss;
    --i;
    ss = gh_scm2newstr (x, NULL);
    v[i] = g_strdup (ss);
    free (ss);
    x = gh_cdr (x);
  }
  return v;
}

static SCM
scm_fn_stringv_p (SCM x)
{
  return gh_bool2scm (scm_stringv_p (x));
}

gboolean
scm_color_p (SCM x)
{
  if (gh_exact_p (x)) {
    gint c;
    c = gh_scm2int (x);
    return (c >= 0);
  }

  if (gh_string_p (x)) {
    gchar *s;
    guint32 c;
    s = gh_scm2newstr (x, NULL);
    c = guppi_str2color_rgba (s);
    free (s);
    return c != 0;
  }

  if (gh_list_p (x)) {
    SCM y;
    gint len;
    gint r, g, b, a;
    len = gh_length (x);
    if (len < 3 || len > 4)
      return FALSE;

    y = gh_car (x);
    if (!gh_exact_p (y))
      return FALSE;
    r = gh_scm2int (y);
    if (r < 0 || r > 0xff)
      return FALSE;
    x = gh_cdr (x);

    y = gh_car (x);
    if (!gh_exact_p (y))
      return FALSE;
    g = gh_scm2int (y);
    if (g < 0 || g > 0xff)
      return FALSE;
    x = gh_cdr (x);

    y = gh_car (x);
    if (!gh_exact_p (y))
      return FALSE;
    b = gh_scm2int (y);
    if (b < 0 || b > 0xff)
      return FALSE;
    x = gh_cdr (x);

    if (len == 4) {
      y = gh_car (x);
      if (!gh_exact_p (y))
	return FALSE;
      a = gh_scm2int (y);
      if (a < 0 || a > 0xff)
	return FALSE;
      x = gh_cdr (x);
    }
    return TRUE;
  }

  return FALSE;
}

SCM
color2scm (guint32 color)
{
  gint r, g, b, a;
  SCM x = SCM_EOL;

  UINT_TO_RGBA (color, &r, &g, &b, &a);

  x = gh_cons (gh_int2scm (a), x);
  x = gh_cons (gh_int2scm (b), x);
  x = gh_cons (gh_int2scm (g), x);
  x = gh_cons (gh_int2scm (r), x);

  return x;
}

guint32
scm2color (SCM x)
{
  if (gh_exact_p (x)) {
    gint c;
    c = gh_scm2int (x);
    return (c >= 0) ? c : 0;
  }

  if (gh_string_p (x)) {
    gchar *s;
    guint32 c;
    s = gh_scm2newstr (x, NULL);
    c = guppi_str2color_rgba (s);
    free (s);

    return c;
  }

  if (gh_list_p (x)) {
    SCM y;
    gint len;
    gint r, g, b, a;
    len = gh_length (x);
    if (len < 3 || len > 4)
      return FALSE;

    y = gh_car (x);
    if (!gh_exact_p (y))
      return 0;
    r = gh_scm2int (y);
    if (r < 0 || r > 0xff)
      return 0;
    x = gh_cdr (x);

    y = gh_car (x);
    if (!gh_exact_p (y))
      return 0;
    g = gh_scm2int (y);
    if (g < 0 || g > 0xff)
      return 0;
    x = gh_cdr (x);

    y = gh_car (x);
    if (!gh_exact_p (y))
      return 0;
    b = gh_scm2int (y);
    if (b < 0 || b > 0xff)
      return 0;
    x = gh_cdr (x);

    a = 0xff;
    if (len == 4) {
      y = gh_car (x);
      if (!gh_exact_p (y))
	return 0;
      a = gh_scm2int (y);
      if (a < 0 || a > 0xff)
	return 0;
      x = gh_cdr (x);
    }

    return RGBA_TO_UINT (r, g, b, a);
  }

  return FALSE;

}

static SCM
scm_fn_color_p (SCM x)
{
  return gh_bool2scm (scm_color_p (x));
}


gboolean
scm_compass_p (SCM x)
{
  const gchar *symb = gh_symbol2newstr (x, NULL);
  gboolean rv = (guppi_str2compass (symb) != GUPPI_COMPASS_INVALID);
  free ((gpointer) symb);
  return rv;
}

SCM
compass2scm (guppi_compass_t c)
{
  return gh_symbol2scm ((gpointer) guppi_compass2str (c));
}

guppi_compass_t
scm2compass (SCM x)
{
  const gchar *symb = gh_symbol2newstr (x, NULL);
  guppi_compass_t c = guppi_str2compass (symb);
  free ((gpointer) symb);
  return c;
}

static SCM
scm_fn_compass_p (SCM x)
{
  return gh_bool2scm (scm_compass_p (x));
}


GtkArg *
scm2gtk_arg (SCM name, SCM x)
{
  GtkArg *arg = NULL;

  g_return_val_if_fail (gh_string_p (name) || gh_symbol_p (name), NULL);

  if (gh_boolean_p (x)) {
    arg = gtk_arg_new (GTK_TYPE_BOOL);
    GTK_VALUE_BOOL (*arg) = gh_scm2bool (x);
  } else if (gh_exact_p (x)) {
    arg = gtk_arg_new (GTK_TYPE_INT);
    GTK_VALUE_INT (*arg) = gh_scm2int (x);
  } else if (gh_number_p (x)) {
    arg = gtk_arg_new (GTK_TYPE_DOUBLE);
    GTK_VALUE_DOUBLE (*arg) = gh_scm2double (x);
  } else if (gh_string_p (x)) {
    arg = gtk_arg_new (GTK_TYPE_STRING);
    GTK_VALUE_STRING (*arg) = gh_scm2newstr (x, NULL);
  }

  g_return_val_if_fail (arg != NULL, NULL);

  arg->name = gh_string_p (name) ?
    gh_scm2newstr (name, NULL) : gh_symbol2newstr (name, NULL);

  return arg;
}

SCM
gtk_arg2scm (GtkArg * x)
{
  g_return_val_if_fail (x != NULL, SCM_UNDEFINED);

  switch (GTK_FUNDAMENTAL_TYPE (x->type)) {
  case GTK_TYPE_CHAR:
    return gh_char2scm (GTK_VALUE_CHAR (*x));
  case GTK_TYPE_UCHAR:
    return gh_char2scm ((char) GTK_VALUE_UCHAR (*x));
  case GTK_TYPE_BOOL:
    return gh_bool2scm (GTK_VALUE_BOOL (*x));
  case GTK_TYPE_INT:
    return gh_int2scm (GTK_VALUE_INT (*x));
  case GTK_TYPE_UINT:
    return gh_ulong2scm (GTK_VALUE_UINT (*x));
  case GTK_TYPE_FLOAT:
    return gh_double2scm ((double) GTK_VALUE_FLOAT (*x));
  case GTK_TYPE_DOUBLE:
    return gh_double2scm (GTK_VALUE_DOUBLE (*x));
  case GTK_TYPE_STRING:
    return gh_str02scm (GTK_VALUE_STRING (*x));
  default:
    return SCM_UNDEFINED;
  }
}

GtkArg *
scm2argv (SCM list, guint * N)
{
  SCM name, val, iter;
  guint len = 0, i;
  GtkArg *args;
  GtkArg *this_arg;

  g_return_val_if_fail (N != NULL, NULL);
  g_return_val_if_fail (gh_list_p (list), NULL);

  if (gh_null_p (list)) {
    *N = 0;
    return NULL;
  }

  /* Count list length */
  iter = list;
  while (!gh_null_p (iter)) {
    ++len;
    iter = gh_cdr (iter);
  }
  g_assert (len % 2 == 0);
  len /= 2;

  args = guppi_new0 (GtkArg, len);
  *N = len;

  i = 0;
  iter = list;
  while (!gh_null_p (iter)) {
    name = gh_car (iter);
    iter = gh_cdr (iter);
    val = gh_car (iter);
    iter = gh_cdr (iter);
    this_arg = scm2gtk_arg (name, val);

    gtk_arg_copy (this_arg, &args[i]);
    args[i].name = this_arg->name;
    this_arg->name = NULL;
    gtk_arg_free (this_arg, TRUE);
  }

  return args;
}

void
guppi_argv_free (GtkArg * args, guint N)
{
  guint i;

  if (args == NULL || N == 0)
    return;

  for (i = 0; i < N; ++i) {
    guppi_free (args[i].name);
    if (GTK_FUNDAMENTAL_TYPE (args[i].type) == GTK_TYPE_STRING)
      guppi_free (GTK_VALUE_STRING (args[i]));
  }
  guppi_free (args);
}

/***********************************************************************/

static double
scm_eval_d__i (gint i, gpointer scm_proc)
{
  SCM scm_i, scm_rv;
  g_return_val_if_fail (scm_proc != NULL
			&& gh_procedure_p ((SCM) scm_proc), 0);

  scm_i = gh_int2scm (i);

  scm_rv = guppi_safe_call1 ((SCM) scm_proc, scm_i);
  g_return_val_if_fail (gh_number_p (scm_rv), 0);

  return gh_scm2double (scm_rv);
}

static double
scm_eval_d__i_d (gint i, double x, gpointer scm_proc)
{
  SCM scm_i, scm_x, scm_rv;
  g_return_val_if_fail (scm_proc != NULL
			&& gh_procedure_p ((SCM) scm_proc), 0);

  scm_i = gh_int2scm (i);
  scm_x = gh_double2scm (x);

  scm_rv = guppi_safe_call2 ((SCM) scm_proc, scm_i, scm_x);
  g_return_val_if_fail (gh_number_p (scm_rv), 0);

  return gh_scm2double (scm_rv);
}


static double
scm_eval_d__d (double x, gpointer scm_proc)
{
  SCM scm_x, scm_rv;
  g_return_val_if_fail (scm_proc != NULL
			&& gh_procedure_p ((SCM) scm_proc), 0);

  scm_x = gh_double2scm (x);

  scm_rv = guppi_safe_call1 ((SCM) scm_proc, scm_x);
  g_return_val_if_fail (gh_number_p (scm_rv), 0);

  return gh_scm2double (scm_rv);
}

static double
scm_eval_d__d_d (double x, double y, gpointer scm_proc)
{
  SCM scm_x, scm_y, scm_rv;
  g_return_val_if_fail (scm_proc != NULL
			&& gh_procedure_p ((SCM) scm_proc), 0);

  scm_x = gh_double2scm (x);
  scm_y = gh_double2scm (y);

  scm_rv = guppi_safe_call2 ((SCM) scm_proc, scm_x, scm_y);
  g_return_val_if_fail (gh_number_p (scm_rv), 0);

  return gh_scm2double (scm_rv);
}

/*
 * I hope that casting the SCM into a gpointer isn't going to cause
 * horrible flaming death on some platforms, but I have a strange feeling
 * that it will.  It'll get fixed one of these days...
 */

GuppiFnWrapper *
scm2fn_d__i (SCM proc)
{
  g_return_val_if_fail (gh_procedure_p (proc), NULL);
  return guppi_fn_wrapper_new_d__i (scm_eval_d__i, (gpointer) proc);
}

GuppiFnWrapper *
scm2fn_d__i_d (SCM proc)
{
  g_return_val_if_fail (gh_procedure_p (proc), NULL);
  return guppi_fn_wrapper_new_d__i_d (scm_eval_d__i_d, (gpointer) proc);
}

GuppiFnWrapper *
scm2fn_d__d (SCM proc)
{
  g_return_val_if_fail (gh_procedure_p (proc), NULL);
  return guppi_fn_wrapper_new_d__d (scm_eval_d__d, (gpointer) proc);
}

GuppiFnWrapper *
scm2fn_d__d_d (SCM proc)
{
  g_return_val_if_fail (gh_procedure_p (proc), NULL);
  return guppi_fn_wrapper_new_d__d_d (scm_eval_d__d_d, (gpointer) proc);
}


/**********************************************************************/

void
guppi_scm_define_autoloaded_symbol (const gchar *fn_name,
				    const gchar *type,
				    const gchar *name)
{
  gchar *fn_name_tweaked;
  gchar *code;
  gchar *c;

  fn_name_tweaked = guppi_strdup (fn_name);
  for (c = fn_name_tweaked; *c; ++c) {
    if (*c == '_')
      *c = '-';
  }

  code = guppi_strdup_printf ("(define (%s . rest) (guppi-plug-in-force-load \"%s\" \"%s\") (apply %s rest))", fn_name_tweaked, type, name, fn_name_tweaked);
  guppi_eval_str (code);

  guppi_free (fn_name_tweaked);
  guppi_free (code);
}

/**********************************************************************/

static gboolean is_guile_active = FALSE;

gboolean
guppi_guile_is_active (void)
{
  return is_guile_active;
}

void
guppi_guile_init (void)
{
  gchar *path;

  static gboolean init = FALSE;
  g_return_if_fail (init == FALSE);
  init = TRUE;

#ifdef WORKING_GUILE_READLINE

  /* Readline was on by default in guile 1.3 ... */

#ifndef USING_GUILE_1_3_0
  gh_eval_str ("(use-modules (ice-9 readline) (ice-9 session))");
  gh_eval_str ("(activate-readline)");
#endif

#endif

  scm_make_gsubr ("stringv?", 1, 0, 0, scm_fn_stringv_p);
  scm_make_gsubr ("color?", 1, 0, 0, scm_fn_color_p);
  scm_make_gsubr ("compass?", 1, 0, 0, scm_fn_compass_p);

#ifdef USING_GUILE_GNOME_BINDINGS
#if 0
  guppi_splash_message (_("Initializing"), _("guile-gtk"));
#endif
  sgtk_init ();
  gh_eval_str ("(use-modules (gtk gtk) (gtk gdk))");

#if 0
  guppi_splash_message (_("Initializing"), _("guile-gnome"));
#endif
  gh_eval_str ("(use-modules (gnome gnome))");
#endif

  path = guppi_find_script ("guppi-guile-boot.scm");
  if (path) {
    guppi_safe_load (path);
    guppi_free(path);
    path = NULL;
  } else {
    g_print ("*** Couldn't find guile boot script.\n");
  }

  is_guile_active = TRUE;
}
