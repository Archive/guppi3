/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-unique-id.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_UNIQUE_ID_H
#define _INC_GUPPI_UNIQUE_ID_H

/* #include <gtk/gtk.h> */
#include <glib.h>
#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS

typedef guint64 guppi_uniq_t;

guppi_uniq_t guppi_unique_id (void);

gchar *guppi_uniq2str (guppi_uniq_t id);
guppi_uniq_t guppi_str2uniq (const gchar *str);

#define guppi_uniq_eq(a, b)  ((a) == (b))
#define guppi_uniq_cmp(a, b) (((a) > (b)) - ((a) < (b)))


GHashTable *guppi_uniq_table_new      (void);
void        guppi_uniq_table_destroy  (GHashTable *);

void        guppi_uniq_table_add      (GHashTable *, guppi_uniq_t, gpointer ptr);
gboolean    guppi_uniq_table_contains (GHashTable *, guppi_uniq_t);
gpointer    guppi_uniq_table_lookup   (GHashTable *, guppi_uniq_t);


END_GUPPI_DECLS

#endif /* _INC_GUPPI_UNIQUE-ID_H */

/* $Id$ */
