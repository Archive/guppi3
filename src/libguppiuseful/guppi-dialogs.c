/* $Id$ */

/*
 * guppi-dialogs.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-dialogs.h"

/* #include <gnome.h> */
#include <gtk/gtkwidget.h>
#include <gtk/gtkwindow.h>
#include <libgnomeui/gnome-dialog-util.h>

#include "guppi-memory.h"

void
guppi_ok_dialog (const gchar *str, ...)
{
  gchar *msg;
  va_list args;

  va_start (args, str);
  msg = guppi_strdup_vprintf (str, args);
  gtk_widget_show_all (gnome_ok_dialog (msg));
  guppi_free (msg);
  va_end (args);
}

void 
guppi_error_dialog (const gchar *str, ...)
{
  gchar *msg;
  va_list args;

  va_start (args, str);
  msg = guppi_strdup_vprintf (str, args);
  gtk_widget_show_all (gnome_error_dialog (msg));
  guppi_free (msg);
  va_end (args);
}

void guppi_warning_dialog (const gchar *str, ...)
{
  gchar *msg;
  va_list args;

  va_start (args, str);
  msg = guppi_strdup_vprintf (str, args);
  gtk_widget_show_all (gnome_warning_dialog (msg));
  guppi_free (msg);
  va_end (args);
}

/* $Id$ */

