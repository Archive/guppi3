/* $Id$ */

/*
 * guppi-version.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 * and Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-version.h"

const gchar *
guppi_version (void)
{
  return VERSION;
}

gint
guppi_version_major (void)
{
  return GUPPI_MAJOR_VERSION;
}

gint
guppi_version_minor (void)
{
  return GUPPI_MINOR_VERSION;
}

gint
guppi_version_micro (void)
{
  return GUPPI_MICRO_VERSION;
}

const gchar **
guppi_copyright (void)
{
  static const gchar *copyright[] = {
    "Copyright (C) 1999, 2000 EMC Capital Management, Inc.",
    "Copyright (C) 2001 Free Software Foundation, Inc.",
    NULL
  };

  return copyright;
}

const gchar **
guppi_developers (void)
{
  static const gchar *developers[] = {
    "Jon Trowbridge",
    "Havoc Pennington",
    "Andrew Chatham",
    "Jody Goldberg",
    "Michael Meeks",
    "Morten Welinder",
    NULL
  };

  return developers;
}

const gchar **
guppi_helpers (void)
{
  static const gchar *helpers[] = {
    "Kenny Graunke",
    "Conrad Steenberg",
    "Cesar Talon",
    "Chris Lyttle",
    "Keld Simonsen",
    "Kenneth Christiansen",
    "Christian Meyer",
    "Bernhard Gonaus",
    "Kai Lahmann",
    "Jean Brefort",
    "Martin Quinson",
    "Halld�r �rn �lafsson",
    "Jos Dehaes",
    "Kjartan Maraas",
    "Valek Filippov",
    "Yuri Syrota",
    "Christian Rose",
    "Andreas Hyden",
    "Stephen L. Moshier",
    "James LewisMoss",
    NULL
  };

  return helpers;
}


/* $Id$ */
