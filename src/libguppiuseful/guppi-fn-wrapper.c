/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-fn-wrapper.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-fn-wrapper.h"

#include "guppi-memory.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_fn_wrapper_finalize (GtkObject * obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_fn_wrapper_class_init (GuppiFnWrapperClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;

  parent_class = gtk_type_class (GTK_TYPE_OBJECT);

  object_class->finalize = guppi_fn_wrapper_finalize;
}

static void
guppi_fn_wrapper_init (GuppiFnWrapper * obj)
{

}

GtkType
guppi_fn_wrapper_get_type (void)
{
  static GtkType guppi_fn_wrapper_type = 0;
  if (!guppi_fn_wrapper_type) {
    static const GtkTypeInfo guppi_fn_wrapper_info = {
      "GuppiFnWrapper",
      sizeof (GuppiFnWrapper),
      sizeof (GuppiFnWrapperClass),
      (GtkClassInitFunc) guppi_fn_wrapper_class_init,
      (GtkObjectInitFunc) guppi_fn_wrapper_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_fn_wrapper_type =
      gtk_type_unique (GTK_TYPE_OBJECT, &guppi_fn_wrapper_info);
  }
  return guppi_fn_wrapper_type;
}

static GuppiFnWrapper *
guppi_fn_wrapper_new (void)
{
  return GUPPI_FN_WRAPPER (guppi_type_new (guppi_fn_wrapper_get_type ()));
}

GuppiFnWrapper *
guppi_fn_wrapper_new_d__i (GuppiFn_d__i f, gpointer ud)
{
  GuppiFnWrapper *fw = guppi_fn_wrapper_new ();

  fw->type = GUPPI_FN_D__I;
  fw->function = f;
  fw->user_data = ud;

  return fw;
}

GuppiFnWrapper *
guppi_fn_wrapper_new_d__i_d (GuppiFn_d__i_d f, gpointer ud)
{
  GuppiFnWrapper *fw = guppi_fn_wrapper_new ();

  fw->type = GUPPI_FN_D__I_D;
  fw->function = f;
  fw->user_data = ud;

  return fw;
}

GuppiFnWrapper *
guppi_fn_wrapper_new_d__d (GuppiFn_d__d f, gpointer ud)
{
  GuppiFnWrapper *fw = guppi_fn_wrapper_new ();

  fw->type = GUPPI_FN_D__D;
  fw->function = f;
  fw->user_data = ud;

  return fw;
}

GuppiFnWrapper *
guppi_fn_wrapper_new_d__d_d (GuppiFn_d__d_d f, gpointer ud)
{
  GuppiFnWrapper *fw = guppi_fn_wrapper_new ();

  fw->type = GUPPI_FN_D__D_D;
  fw->function = f;
  fw->user_data = ud;

  return fw;
}

void
guppi_fn_wrapper_set_user_data (GuppiFnWrapper * fw, gpointer user_data)
{
  g_return_if_fail (fw != NULL && GUPPI_IS_FN_WRAPPER (fw));
  fw->user_data = user_data;
}

double
guppi_fn_wrapper_eval_d__i (GuppiFnWrapper * fw, gint i)
{
  g_return_val_if_fail (fw != NULL && GUPPI_IS_FN_WRAPPER (fw), 0);
  g_return_val_if_fail (fw->function != NULL, 0);
  g_return_val_if_fail (fw->type == GUPPI_FN_D__I, 0);

  return ((GuppiFn_d__i) fw->function) (i, fw->user_data);
}

double
guppi_fn_wrapper_eval_d__i_d (GuppiFnWrapper * fw, gint i, double x)
{
  g_return_val_if_fail (fw != NULL && GUPPI_IS_FN_WRAPPER (fw), 0);
  g_return_val_if_fail (fw->function != NULL, 0);
  g_return_val_if_fail (fw->type == GUPPI_FN_D__I_D, 0);

  return ((GuppiFn_d__i_d) fw->function) (i, x, fw->user_data);
}

double
guppi_fn_wrapper_eval_d__d (GuppiFnWrapper * fw, double x)
{
  g_return_val_if_fail (fw != NULL && GUPPI_IS_FN_WRAPPER (fw), 0);
  g_return_val_if_fail (fw->function != NULL, 0);
  g_return_val_if_fail (fw->type == GUPPI_FN_D__D, 0);

  return ((GuppiFn_d__d) fw->function) (x, fw->user_data);
}

double
guppi_fn_wrapper_eval_d__d_d (GuppiFnWrapper * fw, double x, double y)
{
  g_return_val_if_fail (fw != NULL && GUPPI_IS_FN_WRAPPER (fw), 0);
  g_return_val_if_fail (fw->function != NULL, 0);
  g_return_val_if_fail (fw->type == GUPPI_FN_D__D_D, 0);

  return ((GuppiFn_d__d_d) fw->function) (x, y, fw->user_data);
}


void
guppi_fn_wrapper_eval_d__d_bulk (GuppiFnWrapper *fw,
				 double *buf, gsize N)
{
  gsize i;

  g_return_if_fail (fw != NULL && GUPPI_IS_FN_WRAPPER (fw));
  g_return_if_fail (fw->function != NULL);
  g_return_if_fail (fw->type == GUPPI_FN_D__D);

  for (i=0; i<N; ++i)
    buf[i] = ((GuppiFn_d__d) fw->function) (buf[i], fw->user_data);
}

/* $Id$ */
