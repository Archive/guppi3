/* $Id$ */

/*
 * guppi-paths.h
 *
 * Copyright (C) 1999, 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 * and Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_PATHS_H
#define _INC_GUPPI_PATHS_H

/* #include <gtk/gtk.h> */

#include <glib.h>
#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS

void force_development_path_hacks (void);
gboolean development_path_hacks (void);

void guppi_pixmap_path_add (const gchar *path);
gchar *guppi_find_pixmap (const gchar *filename);
const gchar *guppi_logo_graphic_path (void);

void guppi_script_path_add (const gchar *path);
gchar *guppi_find_script (const gchar *filename);
gchar *guppi_find_guile_script (const gchar *filename); /* historical... */

void guppi_glade_path_add (const gchar *filename);
const gchar *guppi_glade_path (const gchar *filename);

const gchar *gnome_guile_repl_path (void);

void guppi_paths_init (void);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_PATHS_H */

/* $Id$ */
