/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-memory.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_MEMORY_H
#define _INC_GUPPI_MEMORY_H

#define USE_MEMORY_DEBUGGING

/* #include <gtk/gtk.h> */
#include <glib.h>
#include <gtk/gtktypeutils.h>
#include "guppi-defs.h"

BEGIN_GUPPI_DECLS;

#ifdef USE_MEMORY_DEBUGGING

#define guppi_malloc(N) (_guppi_malloc((N),__FILE__,__LINE__))
#define guppi_malloc0(N) (_guppi_malloc0((N),__FILE__,__LINE__))
#define guppi_realloc(x,N) (_guppi_realloc((x),(N),__FILE__,__LINE__))
#define guppi_free(p) (_guppi_free((p),__FILE__,__LINE__))

#define guppi_new(type, count) \
  ((type *)_guppi_new(sizeof(type),(count),#type,__FILE__,__LINE__))
#define guppi_new0(type, count) \
  ((type *)_guppi_new0(sizeof(type),(count),#type,__FILE__,__LINE__))
#define guppi_renew(type, mem, count) \
  ((type *)_guppi_renew(sizeof(type),(mem),(count),#type,__FILE__,__LINE__))

#define guppi_ref(p) (_guppi_ref((p),__FILE__,__LINE__))
#define guppi_unref(p) (_guppi_unref((p),__FILE__,__LINE__))
#define guppi_sink(p) (_guppi_sink((p),__FILE__,__LINE__))

#define guppi_type_new(t) (_guppi_type_new((t),__FILE__,__LINE__))

#define guppi_strdup(s) (_guppi_strdup((s),__FILE__,__LINE__))
#define guppi_strndup(s,N) (_guppi_strndup((s),(N),__FILE__,__LINE__))

#define guppi_strdup_printf(format, args...) \
(_guppi_strdup_printf(__FILE__,__LINE__,(format),##args))

#define guppi_strdup_vprintf(format,args) (_guppi_strdup_vprintf(__FILE__,__LINE__,(format),(args)))

#define guppi_strfreev(s) (_guppi_strfreev((s),__FILE__,__LINE__))

#define guppi_free0(x) { guppi_free((x)); (x) = NULL; }


#define guppi_outside_alloc(x) (_guppi_outside_alloc((x),__FILE__,__LINE__))
#define guppi_outside_object(x) (_guppi_outside_object((x),__FILE__,__LINE__))
#define guppi_permanent_alloc(x) (_guppi_permanent_alloc((x),__FILE__,__LINE__))
#define guppi_A_contains_B(A,B) (_guppi_A_contains_B((A),(B),__FILE__,__LINE__))
#define guppi_finalized(x) (_guppi_finalized((x),__FILE__,__LINE__))


gpointer _guppi_malloc (gsize, const gchar *file, gint line);
gpointer _guppi_malloc0 (gsize, const gchar *file, gint line);
gpointer _guppi_realloc (gpointer, gsize, const gchar *file, gint line);
void _guppi_free (gpointer, const gchar *file, gint line);

gpointer _guppi_new (gsize size, gsize count, const gchar *type,
		     const gchar *file, gint line);
gpointer _guppi_new0 (gsize size, gsize count, const gchar *type,
		      const gchar *file, gint line);
gpointer _guppi_renew (gsize size, gpointer, gsize count, const gchar *type,
		       const gchar *file, gint line);

void _guppi_ref (gpointer, const gchar *file, gint line);
void _guppi_unref (gpointer, const gchar *file, gint line);
void _guppi_sink (gpointer, const gchar *file, gint line);

gpointer _guppi_type_new (GtkType, const gchar *file, gint line);

gchar *_guppi_strdup (const gchar *s, const gchar *file, gint line);
gchar *_guppi_strndup (const gchar *s, guint N, const gchar *file, gint line);
gchar *_guppi_strdup_printf (const gchar *file, gint line,
			     const gchar *format, ...);
gchar *_guppi_strdup_vprintf (const gchar *file, gint line,
			      const gchar *format, va_list args);
void _guppi_strfreev (gchar **s, const gchar *file, gint line);

void _guppi_outside_alloc (gconstpointer, const gchar *file, gint line);
void _guppi_outside_object (gconstpointer, const gchar *file, gint line);
void _guppi_permanent_alloc (gconstpointer, const gchar *file, gint line);

void _guppi_A_contains_B (gconstpointer A, gconstpointer B,
			  const gchar *file, gint line);
void _guppi_finalized (gconstpointer, const char *file, gint line);

void guppi_ref_fn (gpointer);
void guppi_unref_fn (gpointer);
void guppi_free_fn (gpointer);

#else 

#define guppi_malloc g_malloc
#define guppi_malloc0 g_malloc0
#define guppi_realloc g_realloc
#define guppi_free g_free
#define guppi_new g_new
#define guppi_new0 g_new0
#define guppi_renew g_renew
#define guppi_type_new gtk_type_new
#define guppi_strdup g_strdup
#define guppi_strndup g_strndup
#define guppi_strdup_printf g_strdup_printf
#define guppi_strdup_vprintf g_strdup_vprintf
#define guppi_strfreev g_strfreev

#define guppi_outside_alloc(x)
#define guppi_outside_object(x)
#define guppi_permanent_alloc(x)

#define guppi_A_contains_B(A,B)
#define guppi_finalized(x)

void guppi_ref (gpointer);
void guppi_unref (gpointer);
void guppi_sink (gpointer);

#endif

#define guppi_unref0(x) { guppi_unref((x)); (x) = NULL; }
#define guppi_free0(x) { guppi_free((x)); (x) = NULL; }
#define guppi_refcounting_assign(x, y) \
{ if ((x) != (y)) { guppi_ref((y)); guppi_unref((x)); (x) = (y); } }

void guppi_memory_trace (gboolean);

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_MEMORY_H */

/* $Id$ */
