/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-garray.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-garray.h"

#include <string.h>
#include "guppi-memory.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_garray_finalize (GtkObject * obj)
{
  GuppiGArray *ga = GUPPI_GARRAY (obj);

  guppi_finalized (obj);

  guppi_free (ga->data);
  ga->data = NULL;

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_garray_class_init (GuppiGArrayClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;

  parent_class = gtk_type_class (GTK_TYPE_OBJECT);

  object_class->finalize = guppi_garray_finalize;
}

static void
guppi_garray_init (GuppiGArray * obj)
{

}

GtkType
guppi_garray_get_type (void)
{
  static GtkType guppi_garray_type = 0;
  if (!guppi_garray_type) {
    static const GtkTypeInfo guppi_garray_info = {
      "GuppiGArray",
      sizeof (GuppiGArray),
      sizeof (GuppiGArrayClass),
      (GtkClassInitFunc) guppi_garray_class_init,
      (GtkObjectInitFunc) guppi_garray_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_garray_type = gtk_type_unique (GTK_TYPE_OBJECT, &guppi_garray_info);
  }
  return guppi_garray_type;
}

void
guppi_garray_construct (GuppiGArray * ga, guint elt_size)
{
  g_return_if_fail (ga != NULL);
  ga->el_size = elt_size;
#if 0
  ga->array = g_array_new (FALSE, FALSE, elt_size);
#endif
}

GuppiGArray *
guppi_garray_new (guint elt_size)
{
  GuppiGArray *ga = GUPPI_GARRAY (guppi_type_new (guppi_garray_get_type ()));
  guppi_garray_construct (ga, elt_size);
  return ga;
}

void
guppi_garray_set_size (GuppiGArray * ga, gsize N)
{
  if (ga->size < N) {
    gpointer new_ptr = guppi_malloc (N * ga->el_size);

    memcpy (new_ptr, ga->data, ga->size * ga->el_size);

    guppi_free (ga->data);
    ga->data = new_ptr;
    ga->size = N;
  }
}



/* $Id$ */
