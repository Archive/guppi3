/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-seq-integer.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_SEQ_INTEGER_H
#define _INC_GUPPI_SEQ_INTEGER_H

/* #include <gtk/gtk.h> */

#include  "guppi-seq.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS 

typedef struct _GuppiSeqInteger GuppiSeqInteger;
typedef struct _GuppiSeqIntegerClass GuppiSeqIntegerClass;

struct _GuppiSeqInteger {
  GuppiSeq parent;
};

struct _GuppiSeqIntegerClass {
  GuppiSeqClass parent_class;

  void (*range)     (GuppiSeqInteger *, gint *min, gint *max);
  gint (*frequency) (GuppiSeqInteger *, gint);
  gint (*get)       (GuppiSeqInteger *, gint);
  void (*set)       (GuppiSeqInteger *, gint i, gint val);
  void (*insert)    (GuppiSeqInteger *, gint i, const gint *ptr, gsize N);
};

#define GUPPI_TYPE_SEQ_INTEGER (guppi_seq_integer_get_type())
#define GUPPI_SEQ_INTEGER(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_SEQ_INTEGER,GuppiSeqInteger))
#define GUPPI_SEQ_INTEGER0(obj) ((obj) ? (GUPPI_SEQ_INTEGER(obj)) : NULL)
#define GUPPI_SEQ_INTEGER_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_SEQ_INTEGER,GuppiSeqIntegerClass))
#define GUPPI_IS_SEQ_INTEGER(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_SEQ_INTEGER))
#define GUPPI_IS_SEQ_INTEGER0(obj) (((obj) == NULL) || (GUPPI_IS_SEQ_INTEGER(obj)))
#define GUPPI_IS_SEQ_INTEGER_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_SEQ_INTEGER))

GtkType guppi_seq_integer_get_type (void);

gint guppi_seq_integer_get (const GuppiSeqInteger *, gint);
void guppi_seq_integer_get_many (const GuppiSeqInteger *, gint first, gint last, gint * dest);

void guppi_seq_integer_set (GuppiSeqInteger *, gint i, gint val);
void guppi_seq_integer_set_many_dup (GuppiSeqInteger *, gint i0, gint i1, gint val);
void guppi_seq_integer_set_all (GuppiSeqInteger *, gint val);

void guppi_seq_integer_prepend (GuppiSeqInteger *, gint);
void guppi_seq_integer_prepend_many (GuppiSeqInteger *, const gint *, gsize N);

void guppi_seq_integer_append (GuppiSeqInteger *, gint);
void guppi_seq_integer_append_many (GuppiSeqInteger *, const gint *, gsize N);

void guppi_seq_integer_insert (GuppiSeqInteger *, gint, gint val);
void guppi_seq_integer_insert_many (GuppiSeqInteger *, gint,
				    const gint *, gsize N);

gint guppi_seq_integer_min (const GuppiSeqInteger *);
gint guppi_seq_integer_max (const GuppiSeqInteger *);

gint guppi_seq_integer_frequency (const GuppiSeqInteger *, gint);


END_GUPPI_DECLS

#endif /* _INC_GUPPI_SEQ_INTEGER_H */

/* $Id$ */
