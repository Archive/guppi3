/* $Id$ */

/*
 * guppi-data-transform.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_DATA_TRANSFORM_H
#define _INC_GUPPI_DATA_TRANSFORM_H

/* #include <gtk/gtk.h> */
#include "guppi-data.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS 

typedef struct _GuppiDataTransform GuppiDataTransform;
struct _GuppiDataTransform {
  gchar *name;
  gchar *product_name_template;

  GtkType supported_type;
    gboolean (*support_check_cb) (GuppiData *);

  GuppiData *(*transform_cb) (GuppiData *);
};

/* This might be a proper object someday, so using these macros will
   make any future transition easier. */
#define GUPPI_DATA_TRANSFORM(x) ((GuppiDataTransform*)(x))
#define GUPPI_IS_DATA_TRANSFORM(x) ((x)!=NULL)

gboolean guppi_data_transform_supported (GuppiDataTransform *, GuppiData *);
GuppiData *guppi_data_transform_process (GuppiDataTransform *, GuppiData *);

void guppi_data_transforms_register (GuppiDataTransform *);
GList *guppi_data_transforms_find_supported (GuppiData *);

void guppi_data_transforms_init (void);



END_GUPPI_DECLS

#endif /* _INC_GUPPI_DATA_TRANSFORM_H */

/* $Id$ */
