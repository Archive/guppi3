/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-seq-date.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-seq-date.h"

/* #include <gnome.h> */
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>

#include <guppi-memory.h>

static GtkObjectClass *parent_class = NULL;

static void
guppi_seq_date_finalize (GtkObject *obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
set (GuppiSeqDate *sd, gint i, GDate *dt)
{
  GUPPI_SEQ_CLASS (GTK_OBJECT (sd)->klass)->set_missing (GUPPI_SEQ (sd), i, FALSE);
}

static void
insert (GuppiSeqDate *sd, gint i, GDate *dt)
{
  GUPPI_SEQ_CLASS (GTK_OBJECT (sd)->klass)->insert_missing (GUPPI_SEQ (sd), i, FALSE, 1);
}

static void
insert_generic (GuppiSeq *sd, gint i, gsize N)
{
  GDate dt;
  gint j;

  g_date_set_dmy (&dt, 1, 1, 1900);

  for (j = 0; j < (gint) N; ++j) 
    guppi_seq_date_insert (GUPPI_SEQ_DATE (sd), i, &dt);

  if (GUPPI_SEQ_CLASS (parent_class)->insert_generic)
    GUPPI_SEQ_CLASS (parent_class)->insert_generic (sd, i, N);
}

static void
guppi_seq_date_class_init (GuppiSeqDateClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiSeqClass *seq_class = GUPPI_SEQ_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_SEQ);

  klass->set = set;
  klass->insert = insert;

  seq_class->insert_generic = insert_generic;

  object_class->finalize = guppi_seq_date_finalize;
}

static void
guppi_seq_date_init (GuppiSeqDate *obj)
{

}

GtkType guppi_seq_date_get_type (void)
{
  static GtkType guppi_seq_date_type = 0;
  if (!guppi_seq_date_type) {
    static const GtkTypeInfo guppi_seq_date_info = {
      "GuppiSeqDate",
      sizeof (GuppiSeqDate),
      sizeof (GuppiSeqDateClass),
      (GtkClassInitFunc) guppi_seq_date_class_init,
      (GtkObjectInitFunc) guppi_seq_date_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_seq_date_type =
      gtk_type_unique (GUPPI_TYPE_SEQ, &guppi_seq_date_info);
  }
  return guppi_seq_date_type;
}

/***************************************************************************/

/* Sequence Operation Stuff */

typedef struct _GuppiDataOp_Date GuppiDataOp_Date;
struct _GuppiDataOp_Date {
  GuppiDataOp op;

  gint i;
  GDate *dt;
};

static void
op_set (GuppiData *d, GuppiDataOp *op)
{
  GuppiDataOp_Date *date_op = (GuppiDataOp_Date *) op;
  GuppiSeqDateClass *klass;

  klass = GUPPI_SEQ_DATE_CLASS (GTK_OBJECT (d)->klass);

  g_assert (klass->set != NULL);
  klass->set (GUPPI_SEQ_DATE (d), date_op->i, date_op->dt);
}

static void
op_insert (GuppiData *d, GuppiDataOp *op)
{
  GuppiDataOp_Date *date_op = (GuppiDataOp_Date *) op;
  GuppiSeqDateClass *klass;

  klass = GUPPI_SEQ_DATE_CLASS (GTK_OBJECT (d)->klass);

  g_assert (klass->insert != NULL);
  klass->insert (GUPPI_SEQ_DATE (d), date_op->i, date_op->dt);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

GDate *
guppi_seq_date_get (GuppiSeqDate *seq, gint i)
{
  GuppiSeqDateClass *klass;

  g_return_val_if_fail (GUPPI_IS_SEQ_DATE (seq), NULL);
  g_return_val_if_fail (guppi_seq_in_bounds (GUPPI_SEQ (seq), i), NULL);

  klass = GUPPI_SEQ_DATE_CLASS (GTK_OBJECT (seq)->klass);

  g_assert (klass->get != NULL);
  return klass->get (seq, i);
}

void
guppi_seq_date_set (GuppiSeqDate *seq, gint i, GDate *dt)
{
  GuppiDataOp_Date op;

  g_return_if_fail (GUPPI_IS_SEQ_DATE (seq));
  g_return_if_fail (dt != NULL);
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (guppi_seq_in_bounds (GUPPI_SEQ (seq), i));

  if (guppi_seq_missing (GUPPI_SEQ (seq), i) ||
      g_date_compare (guppi_seq_date_get (seq, i), dt) != 0) {

    op.op.op = op_set;
    op.i = i;
    op.dt = dt;

    guppi_seq_changed_set (GUPPI_SEQ (seq), i, i, (GuppiDataOp *) & op);
  }
}

void
guppi_seq_date_prepend (GuppiSeqDate *seq, GDate *dt)
{
  gint first;

  g_return_if_fail (GUPPI_IS_SEQ_DATE (seq));
  g_return_if_fail (dt != NULL);

  first = guppi_seq_min_index (GUPPI_SEQ (seq));
  guppi_seq_date_insert (seq, first, dt);
}

void
guppi_seq_date_append (GuppiSeqDate *seq, GDate *dt)
{
  gint last;

  g_return_if_fail (GUPPI_IS_SEQ_DATE (seq));
  g_return_if_fail (dt != NULL);

  last = guppi_seq_max_index (GUPPI_SEQ (seq));
  guppi_seq_date_insert (seq, last + 1, dt);
}

void
guppi_seq_date_insert (GuppiSeqDate *seq, gint i, GDate *dt)
{
  GuppiDataOp_Date op;

  g_return_if_fail (GUPPI_IS_SEQ_DATE (seq));
  g_return_if_fail (dt != NULL);
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));

  op.op.op = op_insert;
  op.i = i;
  op.dt = dt;

  guppi_seq_changed_insert (GUPPI_SEQ (seq), i, 1, (GuppiDataOp *) & op);
}

static GDate bad_date;

GDate *
guppi_seq_date_min (GuppiSeqDate *seq)
{
  GuppiSeqDateClass *klass;

  g_return_val_if_fail (GUPPI_IS_SEQ_DATE (seq), NULL);

  klass = GUPPI_SEQ_DATE_CLASS (GTK_OBJECT (seq)->klass);

  if (klass->min) {

    return klass->min (seq);

  } else {
    GDate *dt = NULL;
    gint i, i0, i1;
    guppi_seq_indices (GUPPI_SEQ (seq), &i0, &i1);

    g_assert (klass->get != NULL);

    for (i = i0; i <= i1; ++i) {
      GDate *a = klass->get (seq, i);

      if (dt == NULL || (g_date_valid (a) && g_date_compare (a, dt) < 1))
	dt = a;
    }

    if (dt == NULL) {
      g_date_clear (&bad_date, 1);
      dt = &bad_date;
    }

    return dt;
  }
}

GDate *
guppi_seq_date_max (GuppiSeqDate *seq)
{
  GuppiSeqDateClass *klass;

  g_return_val_if_fail (GUPPI_IS_SEQ_DATE (seq), NULL);

  klass = GUPPI_SEQ_DATE_CLASS (GTK_OBJECT (seq)->klass);

  if (klass->max) {

    return klass->max (seq);

  } else {
    GDate *dt = NULL;
    gint i, i0, i1;
    guppi_seq_indices (GUPPI_SEQ (seq), &i0, &i1);

    g_assert (klass->get != NULL);

    for (i = i0; i <= i1; ++i) {
      GDate *a = klass->get (seq, i);

      if (dt == NULL || (g_date_valid (a) && g_date_compare (a, dt) > -1))
	dt = a;
    }

    if (dt == NULL) {
      g_date_clear (&bad_date, 1);
      dt = &bad_date;
    }

    return dt;
  }
}

gint guppi_seq_date_lookup (GuppiSeqDate *seq, GDate *dt)
{
  GuppiSeqDateClass *klass;

  g_return_val_if_fail (GUPPI_IS_SEQ_DATE (seq), 0);
  g_return_val_if_fail (dt != NULL, 1 + guppi_seq_max_index (GUPPI_SEQ (seq)));

  klass = GUPPI_SEQ_DATE_CLASS (GTK_OBJECT (seq)->klass);

  if (klass->lookup) {

    return klass->lookup (seq, dt);

  } else {

    gint i, i0, i1;
    guppi_seq_indices (GUPPI_SEQ (seq), &i0, &i1);

    g_assert (klass->get != NULL);

    for (i = i0; i <= i1; ++i) {
      GDate *a = klass->get (seq, i);

      if (g_date_compare (dt, a) == 0)
	return i;
    }

    return i1 + 1;
  }
}


/* $Id$ */
