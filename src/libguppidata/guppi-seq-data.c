/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-seq-data.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-seq-data.h"

#include <gtk/gtksignal.h>

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>

#include <guppi-useful.h>
#include "guppi-seq-object.h"
#include "guppi-seq-object-impl.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_seq_data_finalize (GtkObject * obj)
{
  GuppiData *data;
  gint i, i0, i1;

  guppi_seq_indices (GUPPI_SEQ (obj), &i0, &i1);
  for (i = i0; i <= i1; ++i) {
    data = guppi_seq_data_get (GUPPI_SEQ_DATA (obj), i);
    if (data)
      gtk_signal_disconnect_by_func (GTK_OBJECT (data),
				     guppi_data_touch, obj);
  }

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
changed_delete (GuppiSeq * seq, gint i, gsize N)
{
  GuppiData *data;
  gint j;

  for (j = 0; j < N; ++j) {
    data = guppi_seq_data_get (GUPPI_SEQ_DATA (seq), i + j);
    if (data != NULL)
      gtk_signal_disconnect_by_func (GTK_OBJECT (data),
				     guppi_data_touch, seq);
  }

  if (GUPPI_SEQ_CLASS (parent_class)->changed_delete)
    GUPPI_SEQ_CLASS (parent_class)->changed_delete (seq, i, N);
}

static xmlNodePtr
export_xml_element (GuppiSeq *seq, gint i, GuppiXMLDocument *doc)
{
  GuppiData *d = guppi_seq_data_get (GUPPI_SEQ_DATA (seq), i);
  return guppi_data_export_xml (d, doc);
}

static void
import_xml_element (GuppiSeq *seq, GuppiXMLDocument *doc, xmlNodePtr node)
{
  GuppiData *d = guppi_data_import_xml (doc, node);
  if (d == NULL)
    g_warning ("problematic node: \"%s\"", node->name);
  else {
    guppi_seq_data_append ((GuppiSeqData *) seq, d);
    guppi_unref (d);
  }
}

static void
guppi_seq_data_class_init (GuppiSeqDataClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiDataClass *data_class = GUPPI_DATA_CLASS (klass);
  GuppiSeqClass *seq_class = GUPPI_SEQ_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_SEQ);

  seq_class->changed_delete = changed_delete;
  seq_class->export_xml_element = export_xml_element;
  seq_class->import_xml_element = import_xml_element;

  data_class->type_name = _("Data Sequence");
  data_class->impl_type = GUPPI_TYPE_SEQ_OBJECT_IMPL;
  data_class->type_name_for_impl_lookup = "GuppiSeqObject";

  object_class->finalize = guppi_seq_data_finalize;
}

static void
guppi_seq_data_init (GuppiSeqData * obj)
{

}

GtkType guppi_seq_data_get_type (void)
{
  static GtkType guppi_seq_data_type = 0;
  if (!guppi_seq_data_type) {
    static const GtkTypeInfo guppi_seq_data_info = {
      "GuppiSeqData",
      sizeof (GuppiSeqData),
      sizeof (GuppiSeqDataClass),
      (GtkClassInitFunc) guppi_seq_data_class_init,
      (GtkObjectInitFunc) guppi_seq_data_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_seq_data_type =
      gtk_type_unique (GUPPI_TYPE_SEQ, &guppi_seq_data_info);
  }
  return guppi_seq_data_type;
}

/***************************************************************************/

typedef struct _GuppiDataOp_Data GuppiDataOp_Data;
struct _GuppiDataOp_Data {
  GuppiDataOp op;
  gint i;
  gsize N;
  GuppiData *data;
};

static void
op_set (GuppiData * d, GuppiDataOp * in_op)
{
  GuppiDataOp_Data *op = (GuppiDataOp_Data *) in_op;

  GuppiSeqObjectImpl *impl;
  GuppiSeqObjectImplClass *impl_class;
  GuppiData *old;

  impl = GUPPI_SEQ_OBJECT_IMPL (guppi_data_impl (d));
  impl_class = GUPPI_SEQ_OBJECT_IMPL_CLASS (GTK_OBJECT (impl)->klass);

  old = guppi_seq_data_get (GUPPI_SEQ_DATA (d), op->i);

  g_assert (impl_class->set);
  impl_class->set (impl, op->i, GTK_OBJECT (op->data));

  if (old)
    gtk_signal_disconnect_by_func (GTK_OBJECT (old), guppi_data_touch, d);

  if (op->data)
    gtk_signal_connect_object_after (GTK_OBJECT (op->data),
				     "changed",
				     GTK_SIGNAL_FUNC (guppi_data_touch),
				     GTK_OBJECT (d));

  guppi_unref (old);
  guppi_ref (op->data);
}

static void
op_insert (GuppiData * d, GuppiDataOp * in_op)
{
  GuppiDataOp_Data *op = (GuppiDataOp_Data *) in_op;

  GuppiSeqObjectImpl *impl;
  GuppiSeqObjectImplClass *impl_class;

  impl = GUPPI_SEQ_OBJECT_IMPL (guppi_data_impl (d));
  impl_class = GUPPI_SEQ_OBJECT_IMPL_CLASS (GTK_OBJECT (impl)->klass);

  g_assert (impl_class->insert);
  impl_class->insert (impl, op->i, op->data ? GTK_OBJECT (op->data) : NULL);

  if (op->data) {
    guppi_ref (op->data);
    gtk_signal_connect_object_after (GTK_OBJECT (op->data),
				     "changed",
				     GTK_SIGNAL_FUNC (guppi_data_touch),
				     GTK_OBJECT (d));
  }
}


/***************************************************************************/

GuppiData *
guppi_seq_data_get (const GuppiSeqData * seq, gint i)
{
  const GuppiSeqObjectImpl *impl;
  GuppiSeqObjectImplClass *impl_class;

  g_return_val_if_fail (seq != NULL, NULL);
  g_return_val_if_fail (GUPPI_IS_SEQ_DATA (seq), NULL);
  g_return_val_if_fail (guppi_seq_in_bounds (GUPPI_SEQ (seq), i), NULL);

  impl = GUPPI_SEQ_OBJECT_IMPL (guppi_data_impl (GUPPI_DATA (seq)));
  impl_class = GUPPI_SEQ_OBJECT_IMPL_CLASS (GTK_OBJECT (impl)->klass);

  g_assert (impl_class->get);
  return GUPPI_DATA0 (impl_class->get (impl, i));
}

void
guppi_seq_data_set (GuppiSeqData * seq, gint i, GuppiData * data)
{
  GuppiDataOp_Data op;

  g_return_if_fail (seq != NULL && GUPPI_IS_SEQ_DATA (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (guppi_seq_in_bounds (GUPPI_SEQ (seq), i));
  g_return_if_fail (data == NULL || GUPPI_IS_DATA (data));

  if (guppi_seq_data_get (seq, i) != data) {
    op.op.op = op_set;
    op.i = i;
    op.data = data;
    guppi_seq_changed_set (GUPPI_SEQ (seq), i, i, (GuppiDataOp *) & op);
  }
}

void
guppi_seq_data_prepend (GuppiSeqData * seq, GuppiData * data)
{
  gint first;

  g_return_if_fail (seq != NULL);
  g_return_if_fail (GUPPI_IS_SEQ_DATA (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (data == NULL || GUPPI_IS_DATA (data));

  first = guppi_seq_min_index (GUPPI_SEQ (seq));
  guppi_seq_data_insert (seq, first, data);
}

void
guppi_seq_data_append (GuppiSeqData * seq, GuppiData * data)
{
  gint last;

  g_return_if_fail (seq != NULL);
  g_return_if_fail (GUPPI_IS_SEQ_DATA (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (data == NULL || GUPPI_IS_DATA (data));

  last = guppi_seq_max_index (GUPPI_SEQ (seq));
  guppi_seq_data_insert (seq, last + 1, data);
}

void
guppi_seq_data_insert (GuppiSeqData * seq, gint i, GuppiData * data)
{
  GuppiDataOp_Data op;

  g_return_if_fail (seq != NULL && GUPPI_IS_SEQ_DATA (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (data == NULL || GUPPI_IS_DATA (data));

  op.op.op = op_insert;
  op.i = i;
  op.data = data;

  guppi_seq_changed_insert (GUPPI_SEQ (seq), i, 1, (GuppiDataOp *) & op);
}






/* $Id$ */
