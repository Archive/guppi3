/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-seq-string.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_SEQ_STRING_H
#define _INC_GUPPI_SEQ_STRING_H

/* #include <gtk/gtk.h> */

#include "guppi-seq.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS 

typedef struct _GuppiSeqString GuppiSeqString;
typedef struct _GuppiSeqStringClass GuppiSeqStringClass;

struct _GuppiSeqString {
  GuppiSeq parent;
};

struct _GuppiSeqStringClass {
  GuppiSeqClass parent_class;

  const gchar *(*get)      (GuppiSeqString *, gint);
  void         (*set)      (GuppiSeqString *, gint, gchar *);
  void         (*insert)   (GuppiSeqString *, gint, gchar *);
  gint         (*lookup)   (GuppiSeqString *, const gchar *);
  gboolean     (*contains) (GuppiSeqString *, const gchar *);
  gsize        (*distinct) (GuppiSeqString *);
};

#define GUPPI_TYPE_SEQ_STRING (guppi_seq_string_get_type())
#define GUPPI_SEQ_STRING(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_SEQ_STRING,GuppiSeqString))
#define GUPPI_SEQ_STRING0(obj) ((obj) ? (GUPPI_SEQ_STRING(obj)) : NULL)
#define GUPPI_SEQ_STRING_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_SEQ_STRING,GuppiSeqStringClass))
#define GUPPI_IS_SEQ_STRING(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_SEQ_STRING))
#define GUPPI_IS_SEQ_STRING0(obj) (((obj) == NULL) || (GUPPI_IS_SEQ_STRING(obj)))
#define GUPPI_IS_SEQ_STRING_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_SEQ_STRING))

GtkType guppi_seq_string_get_type (void);

const gchar *guppi_seq_string_get (const GuppiSeqString *, gint);

/*
 _nc == "no-copy" version of function, assumes ownership of pointers
*/

void guppi_seq_string_set_nc (GuppiSeqString *, gint, gchar *);
void guppi_seq_string_set (GuppiSeqString *, gint, const gchar *);

void guppi_seq_string_prepend_nc (GuppiSeqString *, gchar *);
void guppi_seq_string_prepend (GuppiSeqString *, const gchar *);

void guppi_seq_string_append_nc (GuppiSeqString *, gchar *);
void guppi_seq_string_append (GuppiSeqString *, const gchar *);

void guppi_seq_string_insert_nc (GuppiSeqString *, gint, gchar *);
void guppi_seq_string_insert (GuppiSeqString *, gint, const gchar *);

gint guppi_seq_string_lookup (const GuppiSeqString *, const gchar *);
gboolean guppi_seq_string_contains (const GuppiSeqString *, const gchar *);

gsize guppi_seq_string_distinct_values (const GuppiSeqString *);



END_GUPPI_DECLS

#endif /* _INC_GUPPI_SEQ_STRING_H */

/* $Id$ */
