/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-data-tree.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_DATA_TREE_H
#define _INC_GUPPI_DATA_TREE_H

/* #include <gtk/gtk.h> */

#include "guppi-data.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS

/* While the GuppiDataTree does expose its GuppiDataTreeNodes, they
   should be treated as *read-only*.  If you change them, you could
   really fuck things up. */
typedef struct _GuppiDataTreeNode GuppiDataTreeNode;
struct _GuppiDataTreeNode {
  GuppiDataTreeNode *parent;
  GuppiDataTreeNode *sibling_prev;
  GuppiDataTreeNode *sibling_next;
  GuppiDataTreeNode *child;

  GuppiData *data;
  gpointer reserved;		/* hacky */
};

typedef struct _GuppiDataTree GuppiDataTree;
typedef struct _GuppiDataTreeClass GuppiDataTreeClass;

struct _GuppiDataTree {
  GtkObject parent;

  GuppiDataTreeNode *root;
};

struct _GuppiDataTreeClass {
  GtkObjectClass parent_class;

  void (*changed) (GuppiDataTree *);
  void (*added) (GuppiDataTree *, GuppiData *);
  void (*removed) (GuppiDataTree *, GuppiData *);
};

#define GUPPI_TYPE_DATA_TREE (guppi_data_tree_get_type())
#define GUPPI_DATA_TREE(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_DATA_TREE,GuppiDataTree))
#define GUPPI_DATA_TREE0(obj) ((obj) ? (GUPPI_DATA_TREE(obj)) : NULL)
#define GUPPI_DATA_TREE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_DATA_TREE,GuppiDataTreeClass))
#define GUPPI_IS_DATA_TREE(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_DATA_TREE))
#define GUPPI_IS_DATA_TREE0(obj) (((obj) == NULL) || (GUPPI_IS_DATA_TREE(obj)))
#define GUPPI_IS_DATA_TREE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_DATA_TREE))

GtkType guppi_data_tree_get_type (void);

GuppiDataTree *guppi_data_tree_new (void);

void guppi_data_tree_add (GuppiDataTree *, GuppiData *);
void guppi_data_tree_add_beside (GuppiDataTree *, GuppiData *, GuppiData *);
void guppi_data_tree_add_below (GuppiDataTree *, GuppiData *, GuppiData *);

void guppi_data_tree_remove (GuppiDataTree *, GuppiData *);

gsize guppi_data_tree_size (const GuppiDataTree *);

GuppiData **guppi_data_tree_get_all (const GuppiDataTree *);
GuppiData **guppi_data_tree_get_by_type (const GuppiDataTree *, GtkType);

GuppiDataTree *guppi_data_tree_main (void);
void guppi_data_tree_spew (const GuppiDataTree *);


END_GUPPI_DECLS

#endif /* _INC_GUPPI_DATA_TREE_H */

/* $Id$ */
