/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-data-flavor.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include "guppi-data-flavor.h"

#include <string.h>
#include "guppi-data.h"
#include "guppi-data-socket.h"
#include <guppi-memory.h>

static gpointer
data_create_default (void)
{
  return NULL;
}

static void
data_destroy (gpointer ptr)
{
  guppi_unref (ptr);
}

static gpointer
data_copy (gpointer ptr)
{
  guppi_ref (ptr);
  return ptr;
}

static gboolean
data_equality (gpointer a, gpointer b)
{
  return a == b;
}

static gboolean
data_getp (const gchar *key, const gchar *subkey, gpointer data, gpointer *storage)
{
  gboolean changed = data != *storage;
  *storage = data;
  if (subkey == NULL || !strcmp (subkey, "ref") || !strcmp (subkey, "_default")) {
    guppi_ref (data);
  } else if (strcmp (subkey, "adopt")) {
    g_warning ("Unknown subkey '%s::%s'", key, subkey);
    guppi_ref (data); /* We also ref in this case */
  }
  return changed;
}

static gboolean
data_va2p (const gchar *key, const gchar *subkey, va_list *va, gpointer *storage)
{
  GuppiData *d = va_arg (*va, GuppiData *);
  gboolean changed = d != *storage;

  *storage = d;
  if (subkey == NULL || !strcmp (subkey, "ref") || !strcmp (subkey, "_default")) {
    guppi_ref (d);
  } else if (strcmp (subkey, "adopt")) {
    g_warning ("Unknown subkey '%s::%s'", key, subkey);
    guppi_ref (d); /* also ref in this case */
  }
  return changed;
}

static void
data_p2va (const gchar *key, const gchar *subkey, gpointer ptr, gpointer dest)
{
  *(GuppiData **) dest = (GuppiData *) ptr;
  if (subkey == NULL || !strcmp (subkey, "ref") || !strcmp (subkey, "_default")) {
    guppi_ref (ptr);
  } else if (strcmp (subkey, "raw")) {
    g_warning ("Unknown subkey '%s::%s'", key, subkey);
    guppi_ref (ptr); /* also ref in this case */
  }
}
  

static xmlNodePtr
data_exp_xml (GuppiXMLDocument *doc, gpointer ptr)
{
  GuppiData *d = ptr ? GUPPI_DATA (ptr) : NULL;
  if (d)
    return guppi_data_export_xml (d, doc);
  else
    return xmlNewNode (doc->ns, "Data_NULL");
}

static gboolean
data_imp_xml (GuppiXMLDocument *doc, xmlNodePtr node, gpointer *ptr)
{
  if (!strcmp (node->name, "Data_NULL")) {
    *ptr = NULL;
    return TRUE;
  }

  *ptr = guppi_data_import_xml (doc, node);
  return *ptr != NULL;
}

GuppiAttributeFlavor
guppi_attribute_flavor_data (void)
{
  static GuppiAttributeFlavor flavor = -1;
  
  if (flavor < 0) {
    
    flavor = guppi_attribute_flavor_register ("data",
					      data_create_default,
					      data_destroy,
					      data_copy,
					      data_equality,
					      data_getp,
					      data_va2p,
					      data_p2va);

    guppi_attribute_flavor_add_xml_serialization (flavor,
						  data_exp_xml,
						  data_imp_xml);
  }

  return flavor;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static gpointer
data_socket_create_default (void)
{
  return guppi_data_socket_new ();
}

static void
data_socket_destroy (gpointer ptr)
{
  guppi_unref (ptr);
}

static gpointer
data_socket_copy (gpointer ptr)
{
  guppi_ref (ptr);
  return ptr;
}

static gboolean
data_socket_equality (gpointer a, gpointer b)
{
  return a == b;
}

static gboolean
data_socket_getp (const gchar *key, const gchar *subkey, gpointer data, gpointer *storage)
{
  if (subkey == NULL || !strcmp (subkey, "data") || !strcmp (subkey, "data::ref") || !strcmp (subkey, "data::adopt")) {
    GuppiData *d, *old;
    GuppiDataSocket *sock;

    g_assert (*storage != NULL);
    g_assert (GUPPI_IS_DATA_SOCKET (*storage));

    d = data ? GUPPI_DATA (data) : NULL;
    sock = GUPPI_DATA_SOCKET (*storage);
    old = guppi_data_socket_get_data (sock);
    guppi_data_socket_set_data (sock, d);
    if (subkey && !strcmp (subkey, "data::adopt"))
      guppi_unref (d);
    return d != old;
  }
  
  if (!strcmp (subkey, "socket") || !strcmp (subkey, "socket::ref") || !strcmp (subkey, "socket::adopt") || !strcmp (subkey, "_default")) {
    GuppiDataSocket *sock = data ? GUPPI_DATA_SOCKET (data) : NULL;
    gboolean changed = *storage != (gpointer) sock;
    guppi_refcounting_assign (*storage, sock);
    if (!strcmp (subkey, "socket::adopt"))
      guppi_unref (sock);
    return changed;
  }

  g_warning ("Unknown subkey '%s::%s', ignored.", key, subkey);

  return FALSE;
}

static gboolean
data_socket_va2p (const gchar *key, const gchar *subkey, va_list *va, gpointer *storage)
{
  if (subkey == NULL || !strcmp (subkey, "data") || !strcmp (subkey, "data::ref") || !strcmp (subkey, "data::adopt")) {
    GuppiData *d, *old;
    GuppiDataSocket *sock;

    g_assert (*storage != NULL);
    g_assert (GUPPI_IS_DATA_SOCKET (*storage));

    sock = GUPPI_DATA_SOCKET (*storage);
    old = guppi_data_socket_get_data (sock);
    d  = va_arg (*va, GuppiData *);
    guppi_data_socket_set_data (sock, d);
    if (subkey && !strcmp (subkey, "data::adopt"))
      guppi_unref (d);
    return d != old;
  }
  
  if (!strcmp (subkey, "socket") || !strcmp (subkey, "socket::ref") || !strcmp (subkey, "socket::adopt") || !strcmp (subkey, "_default")) {
    GuppiDataSocket *sock = va_arg (*va, GuppiDataSocket *);
    gboolean changed = *storage != (gpointer) sock;
    guppi_refcounting_assign (*storage, sock);
    if (!strcmp (subkey, "socket::adopt"))
      guppi_unref (sock);
    return changed;
  }

  g_warning ("Unknown subkey '%s::%s', ignored.", key, subkey);

  return FALSE;
}

static void
data_socket_p2va (const gchar *key, const gchar *subkey, gpointer ptr, gpointer dest)
{
  GuppiDataSocket *sock = GUPPI_DATA_SOCKET (ptr);

  if (subkey == NULL || !strcmp (subkey, "data") || !strcmp (subkey, "data::ref") || !strcmp (subkey, "data::raw")) {
    GuppiData *d = guppi_data_socket_get_data (sock);
    *(GuppiData **)dest = d;
    if (subkey == NULL || strcmp (subkey, "data::raw"))
      guppi_ref (d);
    return;
  }

  if (!strcmp (subkey, "socket") || !strcmp(subkey, "socket::ref") || !strcmp (subkey, "socket::raw") || !strcmp (subkey, "_default")) {
    *(GuppiDataSocket **) dest = (GuppiDataSocket *) ptr;
    if (strcmp (subkey, "socket::raw")) {
      guppi_ref (ptr);
    }
    return;
  }

  g_warning ("Unknown subkey '%s::%s', ignored.", key, subkey);
  *(gchar **)dest = NULL;
}

static xmlNodePtr
data_socket_exp_xml (GuppiXMLDocument *doc, gpointer ptr)
{
  if (ptr) {
    GuppiDataSocket *sock = GUPPI_DATA_SOCKET (ptr);
    return guppi_data_socket_export_xml (sock, doc);
  }

  return NULL;
}

static gboolean
data_socket_imp_xml (GuppiXMLDocument *doc, xmlNodePtr node, gpointer *ptr)
{
  GuppiDataSocket *sock = guppi_data_socket_new ();
  if (!guppi_data_socket_import_xml (sock, doc, node)) {
    guppi_unref (sock);
    return FALSE;
  }
  *ptr = sock;
  return TRUE;
}

GuppiAttributeFlavor
guppi_attribute_flavor_data_socket (void)
{
  static GuppiAttributeFlavor flavor = -1;
  
  if (flavor < 0) {
    
    flavor = guppi_attribute_flavor_register ("data_socket",
					      data_socket_create_default,
					      data_socket_destroy,
					      data_socket_copy,
					      data_socket_equality,
					      data_socket_getp,
					      data_socket_va2p,
					      data_socket_p2va);

    guppi_attribute_flavor_add_xml_serialization (flavor,
						  data_socket_exp_xml,
						  data_socket_imp_xml);

    guppi_attribute_flavor_add_signal_to_forward (flavor, "changed", TRUE);
  }

  return flavor;
}
