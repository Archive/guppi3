/* This is -*- C -*- */
/* vim: set sw=2: */

/*
 * guppi-seq-categorical.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_SEQ_CATEGORICAL_H
#define _INC_GUPPI_SEQ_CATEGORICAL_H

/* #include <gtk/gtk.h> */
#include  "guppi-defs.h"
#include "guppi-seq-integer.h"
#include "guppi-category.h"

BEGIN_GUPPI_DECLS 

typedef struct _GuppiSeqCategorical GuppiSeqCategorical;
typedef struct _GuppiSeqCategoricalClass GuppiSeqCategoricalClass;
struct _GuppiSeqCategoricalPrivate;

struct _GuppiSeqCategorical {
  GuppiSeqInteger parent;

  struct _GuppiSeqCategoricalPrivate *priv;
};

struct _GuppiSeqCategoricalClass {
  GuppiSeqIntegerClass parent_class;
};

#define GUPPI_TYPE_SEQ_CATEGORICAL (guppi_seq_categorical_get_type ())
#define GUPPI_SEQ_CATEGORICAL(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_SEQ_CATEGORICAL,GuppiSeqCategorical))
#define GUPPI_SEQ_CATEGORICAL0(obj) ((obj) ? (GUPPI_SEQ_CATEGORICAL(obj)) : NULL)
#define GUPPI_SEQ_CATEGORICAL_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_SEQ_CATEGORICAL,GuppiSeqCategoricalClass))
#define GUPPI_IS_SEQ_CATEGORICAL(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_SEQ_CATEGORICAL))
#define GUPPI_IS_SEQ_CATEGORICAL0(obj) (((obj) == NULL) || (GUPPI_IS_SEQ_CATEGORICAL(obj)))
#define GUPPI_IS_SEQ_CATEGORICAL_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_SEQ_CATEGORICAL))

GtkType guppi_seq_categorical_get_type (void);

GuppiCategory *guppi_seq_categorical_category (GuppiSeqCategorical *seq);
void guppi_seq_categorical_set_category (GuppiSeqCategorical *seq, GuppiCategory *cat);

/* In auto-add mode, an attempt to add an unrecognized item to an
   categorical sequence will result in that item being assigned a code
   number and being added.  Otherwise the item will be rejected with
   a warning to stderr. */

gboolean guppi_seq_categorical_auto_add (GuppiSeqCategorical *seq);
void guppi_seq_categorical_set_auto_add (GuppiSeqCategorical *seq, gboolean x);

const gchar *guppi_seq_categorical_get (GuppiSeqCategorical *seq, gint i);

gboolean guppi_seq_categorical_set (GuppiSeqCategorical *seq,
				    gint i, const gchar *val);

gboolean guppi_seq_categorical_prepend (GuppiSeqCategorical *seq,
					const gchar *val);
gboolean guppi_seq_categorical_append (GuppiSeqCategorical *seq,
				       const gchar *val);
gboolean guppi_seq_categorical_insert (GuppiSeqCategorical *seq,
				       gint i, const gchar *val);

gint guppi_seq_categorical_frequency (GuppiSeqCategorical *seq,
				      const gchar *val);
double guppi_seq_categorical_percentage (GuppiSeqCategorical *seq,
					 const gchar *val);


END_GUPPI_DECLS

#endif /* _INC_GUPPI_SEQ_CATEGORICAL_H */

/* $Id$ */
