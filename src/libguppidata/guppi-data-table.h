/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-data-table.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_DATA_TABLE_H
#define _INC_GUPPI_DATA_TABLE_H

#include "guppi-data.h"
#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS;

typedef struct _GuppiDataTable GuppiDataTable;
typedef struct _GuppiDataTableClass GuppiDataTableClass;
typedef struct _GuppiDataTablePrivate GuppiDataTablePrivate;

typedef enum {
  GUPPI_TABLE_ROW,
  GUPPI_TABLE_COL
} GuppiDataTableSpan;

struct _GuppiDataTable {
  GuppiData parent;
  GuppiDataTablePrivate *priv;
};

struct _GuppiDataTableClass {
  GuppiDataClass parent_class;

  /* virtual functions */
  gboolean     (*get_bounds) (GuppiDataTable *, gint *r, gint *c);
  void         (*set_bounds) (GuppiDataTable *, gint r, gint c);

  double       (*get_entry) (GuppiDataTable *, gint r, gint c);
  void         (*set_entry) (GuppiDataTable *, gint r, gint c, double x);

  const gchar *(*get_label) (GuppiDataTable *, GuppiDataTableSpan, gint i);
  void         (*set_label) (GuppiDataTable *, GuppiDataTableSpan, gint i, const gchar *);

  gboolean     (*get_stats) (GuppiDataTable *, GuppiDataTableSpan, gint i,
			     double *sum, double *sum_abs, double *min, double *max);

  /* signals */
  void (*changed_dimensions)    (GuppiDataTable *, gint r, gint c);
  void (*changed_table_entries) (GuppiDataTable *, gint r0, gint c0, gint r1, gint c1);
  void (*changed_table_labels)  (GuppiDataTable *, GuppiDataTableSpan, gint i0, gint i1);
};

#define GUPPI_TYPE_DATA_TABLE (guppi_data_table_get_type ())
#define GUPPI_DATA_TABLE(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_DATA_TABLE,GuppiDataTable))
#define GUPPI_DATA_TABLE0(obj) ((obj) ? (GUPPI_DATA_TABLE(obj)) : NULL)
#define GUPPI_DATA_TABLE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_DATA_TABLE,GuppiDataTableClass))
#define GUPPI_IS_DATA_TABLE(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_DATA_TABLE))
#define GUPPI_IS_DATA_TABLE0(obj) (((obj) == NULL) || (GUPPI_IS_DATA_TABLE(obj)))
#define GUPPI_IS_DATA_TABLE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_DATA_TABLE))

GtkType guppi_data_table_get_type (void);

gboolean guppi_data_table_get_dimensions (GuppiDataTable *, gint *rows, gint *cols);
gint     guppi_data_table_get_span_count (GuppiDataTable *, GuppiDataTableSpan);
gint     guppi_data_table_get_row_count  (GuppiDataTable *);
gint     guppi_data_table_get_col_count  (GuppiDataTable *);

void     guppi_data_table_set_dimensions (GuppiDataTable *, gint r, gint c);

gboolean guppi_data_table_in_bounds      (GuppiDataTable *, gint r, gint c);
gboolean guppi_data_table_in_span_bounds (GuppiDataTable *, GuppiDataTableSpan, gint i);
gboolean guppi_data_table_in_row_bounds  (GuppiDataTable *, gint r);
gboolean guppi_data_table_in_col_bounds  (GuppiDataTable *, gint c);

double   guppi_data_table_get_entry      (GuppiDataTable *, gint r, gint c);
void     guppi_data_table_set_entry      (GuppiDataTable *, gint r, gint c, double x);

const gchar *guppi_data_table_get_label     (GuppiDataTable *, GuppiDataTableSpan, gint i);
const gchar *guppi_data_table_get_row_label (GuppiDataTable *, gint r);
const gchar *guppi_data_table_get_col_label (GuppiDataTable *, gint c);

void         guppi_data_table_set_label     (GuppiDataTable *, GuppiDataTableSpan, gint i, const gchar *);
void         guppi_data_table_set_row_label (GuppiDataTable *, gint r, const gchar *);
void         guppi_data_table_set_col_label (GuppiDataTable *, gint c, const gchar *);

double       guppi_data_table_get_sum       (GuppiDataTable *, GuppiDataTableSpan, gint i);
double       guppi_data_table_get_abs_sum   (GuppiDataTable *, GuppiDataTableSpan, gint i);
double       guppi_data_table_get_min       (GuppiDataTable *, GuppiDataTableSpan, gint i);
double       guppi_data_table_get_max       (GuppiDataTable *, GuppiDataTableSpan, gint i);

double       guppi_data_table_get_range_sum     (GuppiDataTable *, gint r0, gint c0, gint r1, gint c1);
double       guppi_data_table_get_range_abs_sum (GuppiDataTable *, gint r0, gint c0, gint r1, gint c1);
double       guppi_data_table_get_range_min     (GuppiDataTable *, gint r0, gint c0, gint r1, gint c1);
double       guppi_data_table_get_range_max     (GuppiDataTable *, gint r0, gint c0, gint r1, gint c1);


/* Only use these if you know what you are doing. */

void guppi_data_table_changed_dimensions    (GuppiDataTable *, gint r, gint c, GuppiDataOp *op);
void guppi_data_table_changed_table_entries (GuppiDataTable *, gint r0, gint c0, gint r1, gint c1, GuppiDataOp *op);
void guppi_data_table_changed_table_labels  (GuppiDataTable *, GuppiDataTableSpan, gint i0, gint i1, GuppiDataOp *op);

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_DATA_TABLE_H */

/* $Id$ */
