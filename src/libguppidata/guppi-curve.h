/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-curve.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_CURVE_H
#define _INC_GUPPI_CURVE_H

/* #include <gtk/gtk.h> */
#include <libart_lgpl/libart.h>
#include "guppi-data.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS

typedef struct _GuppiCurve GuppiCurve;
typedef struct _GuppiCurveClass GuppiCurveClass;

struct _GuppiCurve {
  GuppiData parent;
};

struct _GuppiCurveClass {
  GuppiDataClass parent_class;

  /* Mandatory functions */

  void (*bounds) (GuppiCurve *, double *, double *);
  void (*get) (GuppiCurve *, double, double *, double *);


  /* Optional functions */

  void (*bbox) (GuppiCurve *, double, double,
		double *, double *, double *, double *);

  gboolean (*clamp) (GuppiCurve *, double *, double *,
		     double, double, double, double);

  void (*sample) (GuppiCurve *,
		  gconstpointer t_vec, gint t_stride, gsize N,
		  gpointer x_vec, gint x_stride,
		  gpointer y_vec, gint y_stride);
  void (*sample_uniformly) (GuppiCurve *,
			    double t0, double t1, gsize N,
			    double *x_vec, gint x_stride,
			    double *y_vec, gint y_stride);

  ArtVpath *(*approx_to_path) (GuppiCurve *, double t0, double t1,
			       double x_error, double y_error,
			       double x0, double y0, double x1, double y1,
			       double scale_x, double scale_y);

};

#define GUPPI_TYPE_CURVE (guppi_curve_get_type())
#define GUPPI_CURVE(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_CURVE,GuppiCurve))
#define GUPPI_CURVE0(obj) ((obj) ? (GUPPI_CURVE(obj)) : NULL)
#define GUPPI_CURVE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_CURVE,GuppiCurveClass))
#define GUPPI_IS_CURVE(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_CURVE))
#define GUPPI_IS_CURVE0(obj) (((obj) == NULL) || (GUPPI_IS_CURVE(obj)))
#define GUPPI_IS_CURVE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_CURVE))

GtkType guppi_curve_get_type (void);

void guppi_curve_parameter_bounds (GuppiCurve *, double *a, double *b);
double guppi_curve_parameter_lower_bound (GuppiCurve *);
double guppi_curve_parameter_upper_bound (GuppiCurve *);

gboolean guppi_curve_parameter_in_bounds (GuppiCurve *, double t);
double guppi_curve_clamp_parameter (GuppiCurve *, double t);

void guppi_curve_get (GuppiCurve *, double t, double *x, double *y);
double guppi_curve_get_x (GuppiCurve *, double t);
double guppi_curve_get_y (GuppiCurve *, double t);

/* Find a bounding box containing the portion of the curve parameterized
   by [t0, t1]. */
void guppi_curve_get_bbox (GuppiCurve *, double t0, double t1,
			   double *x0, double *y0, double *x1, double *y1);

void guppi_curve_clamp_to_bbox (GuppiCurve *, 
				double *t0, double *t1,
				double x0, double y0, double x1, double y1);

void guppi_curve_sample (GuppiCurve *,
			 const double *t_vec, gint t_stride, gsize N,
			 double *x_vec, gint x_stride,
			 double *y_vec, gint y_stride);

void guppi_curve_sample_uniformly (GuppiCurve *,
				   double t0, double t1, gsize N,
				   double *x_vec, gint x_stride,
				   double *y_vec, gint y_stride);

void guppi_curve_sample_uniformly_to_path (GuppiCurve *,
					   double t0, double t1, gsize N,
					   ArtVpath *);

ArtVpath *guppi_curve_approximate_to_path (GuppiCurve *,
					   double t0, double t1,
					   double x_error, double y_error,
					   double x0, double y0, double x1, double y1,
					   double scale_x, double scale_y);


END_GUPPI_DECLS

#endif /* _INC_GUPPI_CURVE_H */

/* $Id$ */
