/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-seq-object.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_SEQ_OBJECT_H
#define _INC_GUPPI_SEQ_OBJECT_H

/* #include <gtk/gtk.h> */
#include  "guppi-seq.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS 

typedef struct _GuppiSeqObject GuppiSeqObject;
typedef struct _GuppiSeqObjectClass GuppiSeqObjectClass;

struct _GuppiSeqObject {
  GuppiSeq parent;
};

struct _GuppiSeqObjectClass {
  GuppiSeqClass parent_class;

  /* Virtual functions */

  GtkObject *(*get)         (GuppiSeqObject *, gint);
  void       (*set)         (GuppiSeqObject *, gint, GtkObject *);
  void       (*insert)      (GuppiSeqObject *, gint, GtkObject *);
  void       (*insert_NULL) (GuppiSeqObject *, gint, gsize);
};

#define GUPPI_TYPE_SEQ_OBJECT (guppi_seq_object_get_type())
#define GUPPI_SEQ_OBJECT(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_SEQ_OBJECT,GuppiSeqObject))
#define GUPPI_SEQ_OBJECT0(obj) ((obj) ? (GUPPI_SEQ_OBJECT(obj)) : NULL)
#define GUPPI_SEQ_OBJECT_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_SEQ_OBJECT,GuppiSeqObjectClass))
#define GUPPI_IS_SEQ_OBJECT(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_SEQ_OBJECT))
#define GUPPI_IS_SEQ_OBJECT0(obj) (((obj) == NULL) || (GUPPI_IS_SEQ_OBJECT(obj)))
#define GUPPI_IS_SEQ_OBJECT_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_SEQ_OBJECT))

GtkType guppi_seq_object_get_type (void);

GtkObject *guppi_seq_object_get (const GuppiSeqObject *, gint);
void guppi_seq_object_set (GuppiSeqObject *, gint, GtkObject *);

void guppi_seq_object_prepend (GuppiSeqObject *, GtkObject *);
void guppi_seq_object_prepend_NULL (GuppiSeqObject *, gsize N);

void guppi_seq_object_append (GuppiSeqObject *, GtkObject *);
void guppi_seq_object_append_NULL (GuppiSeqObject *, gsize N);

void guppi_seq_object_insert (GuppiSeqObject *, gint, GtkObject *);
void guppi_seq_object_insert_NULL (GuppiSeqObject *, gint i, gsize N);


END_GUPPI_DECLS

#endif /* _INC_GUPPI_SEQ_OBJECT_H */

/* $Id$ */
