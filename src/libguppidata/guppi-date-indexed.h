/* This is -*- C -*- */
/* vim: set sw=2: */

/*
 * guppi-date-indexed.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_DATE_INDEXED_H
#define _INC_GUPPI_DATE_INDEXED_H

/* #include <gtk/gtk.h> */
#include  "guppi-defs.h"
#include "guppi-data.h"

BEGIN_GUPPI_DECLS

typedef struct _GuppiDateIndexed GuppiDateIndexed;
typedef struct _GuppiDateIndexedClass GuppiDateIndexedClass;

struct _GuppiDateIndexed {
  GuppiData parent;
  gpointer opaque_internals;
};

struct _GuppiDateIndexedClass {
  GuppiDataClass parent_class;

  void     (*bounds)      (GuppiDateIndexed *di, GDate *start, GDate *end);
  gboolean (*valid)       (GuppiDateIndexed *di, const GDate *dt);
  gboolean (*step)        (GuppiDateIndexed *di, const GDate *orig, gint delta, GDate *modified);
  gint     (*size)        (GuppiDateIndexed *di);
  void     (*bounds_hint) (GuppiDateIndexed *di, const GDate *start, const GDate *end);

  xmlNodePtr (*export_xml_element) (GuppiDateIndexed *, const GDate *, GuppiXMLDocument *);
  gboolean   (*import_xml_element) (GuppiDateIndexed *, const GDate *, GuppiXMLDocument *, xmlNodePtr);
};

#define GUPPI_TYPE_DATE_INDEXED (guppi_date_indexed_get_type ())
#define GUPPI_DATE_INDEXED(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_DATE_INDEXED,GuppiDateIndexed))
#define GUPPI_DATE_INDEXED0(obj) ((obj) ? (GUPPI_DATE_INDEXED(obj)) : NULL)
#define GUPPI_DATE_INDEXED_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_DATE_INDEXED,GuppiDateIndexedClass))
#define GUPPI_IS_DATE_INDEXED(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_DATE_INDEXED))
#define GUPPI_IS_DATE_INDEXED0(obj) (((obj) == NULL) || (GUPPI_IS_DATE_INDEXED(obj)))
#define GUPPI_IS_DATE_INDEXED_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_DATE_INDEXED))

GtkType guppi_date_indexed_get_type (void);

const GDate *guppi_date_indexed_start (GuppiDateIndexed *);
const GDate *guppi_date_indexed_end (GuppiDateIndexed *);
gboolean guppi_date_indexed_in_bounds (GuppiDateIndexed *, const GDate *);
void guppi_date_indexed_clamp (GuppiDateIndexed *, GDate *);

gboolean guppi_date_indexed_valid (GuppiDateIndexed *, const GDate *);

gboolean guppi_date_indexed_step (GuppiDateIndexed *, GDate *, gint delta);
gboolean guppi_date_indexed_incr (GuppiDateIndexed *, GDate *);
gboolean guppi_date_indexed_decr (GuppiDateIndexed *, GDate *);

gint guppi_date_indexed_size (GuppiDateIndexed *);
gboolean guppi_date_indexed_empty (GuppiDateIndexed *);
gboolean guppi_date_indexed_nonempty (GuppiDateIndexed *);

void guppi_date_indexed_bounds_hint (GuppiDateIndexed *,
				     const GDate *start, const GDate *end);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_DATE_INDEXED_H */

/* $Id$ */
