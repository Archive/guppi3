/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-seq-string.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-seq-string.h"

#include <stdlib.h>
#include <string.h>
/* #include <gnome.h> */

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>

#include <guppi-memory.h>

static GtkObjectClass *parent_class = NULL;

static void
guppi_seq_string_finalize (GtkObject *obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static xmlNodePtr
export_xml_element (GuppiSeq *seq, gint i, GuppiXMLDocument *doc)
{
  const gchar *str;
  xmlNodePtr node;

  str = guppi_seq_string_get (GUPPI_SEQ_STRING (seq), i);
  node = xmlNewNode (doc->ns, "string");
  xmlAddChild (node, xmlNewText (str));

  return node;
}

static gboolean
import_xml_element (GuppiSeq *seq, GuppiXMLDocument *doc, xmlNodePtr node)
{
  gchar *str;
  
  g_return_val_if_fail (!strcmp (node->name, "string"), FALSE);
  
  /* I have to copy the string, because GuppiSeqString will later try to g_free it. */
  str = xmlNodeGetContent (node->xmlChildrenNode);
  guppi_seq_string_append ((GuppiSeqString *) seq, str);
  free (str);

  return TRUE;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
set (GuppiSeqString *ss, gint i, gchar *str)
{
  GUPPI_SEQ_CLASS (GTK_OBJECT (ss)->klass)->set_missing (GUPPI_SEQ (ss), i, FALSE); 
}

static void
insert (GuppiSeqString *ss, gint i, gchar *str)
{
  GUPPI_SEQ_CLASS (GTK_OBJECT (ss)->klass)->insert_missing (GUPPI_SEQ (ss), i, FALSE, 1); 
}

static void
insert_generic (GuppiSeq *seq, gint i, gsize N)
{
  gint j;

  for (j = 0; j < (gint) N; ++j)
    guppi_seq_string_insert (GUPPI_SEQ_STRING (seq), i, "");

  if (GUPPI_SEQ_CLASS (parent_class)->insert_generic)
    GUPPI_SEQ_CLASS (parent_class)->insert_generic (seq, i, N);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

typedef struct _GuppiDataOp_String GuppiDataOp_String;
struct _GuppiDataOp_String {
  GuppiDataOp op;
  gint i;
  const gchar *str;
  gboolean copy;
};

static void
op_set (GuppiData *data, GuppiDataOp *op)
{
  GuppiSeqString *ss;
  GuppiSeqStringClass *klass;
  GuppiDataOp_String *sop = (GuppiDataOp_String *) op;

  ss = GUPPI_SEQ_STRING (data);
  klass = GUPPI_SEQ_STRING_CLASS (GTK_OBJECT (data)->klass);

  g_assert (klass->set);
  if (sop->copy)
    klass->set (ss, sop->i, guppi_strdup (sop->str));
  else
    klass->set (ss, sop->i, (gchar *) sop->str);
}

static void
op_insert (GuppiData *data, GuppiDataOp *op)
{
  GuppiSeqString *ss;
  GuppiSeqStringClass *klass;
  GuppiDataOp_String *sop = (GuppiDataOp_String *) op;

  ss = GUPPI_SEQ_STRING (data);
  klass = GUPPI_SEQ_STRING_CLASS (GTK_OBJECT (data)->klass);

  g_assert (klass->insert);
  if (sop->copy)
    klass->insert (ss, sop->i, guppi_strdup (sop->str));
  else
    klass->insert (ss, sop->i, (gchar *) sop->str);
}

/**************************************************************************/

const gchar *
guppi_seq_string_get (const GuppiSeqString *seq, gint i)
{
  GuppiSeqStringClass *klass;

  g_return_val_if_fail (seq != NULL, NULL);
  g_return_val_if_fail (guppi_seq_in_bounds (GUPPI_SEQ (seq), i), NULL);

  klass = GUPPI_SEQ_STRING_CLASS (GTK_OBJECT (seq)->klass);

  g_assert (klass->get);
  return klass->get ((GuppiSeqString *) seq, i);
}

void
guppi_seq_string_set_nc (GuppiSeqString *seq, gint i, gchar *str)
{
  GuppiDataOp_String op;

  g_return_if_fail (seq != NULL && GUPPI_IS_SEQ_STRING (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (guppi_seq_in_bounds (GUPPI_SEQ (seq), i));

  op.op.op = op_set;
  op.i = i;
  op.str = str;
  op.copy = FALSE;

  /* NOTE : The index is ignored */
  guppi_seq_changed_set (GUPPI_SEQ (seq), i, i, (GuppiDataOp *) & op);
}

void
guppi_seq_string_set (GuppiSeqString *seq, gint i, const gchar *str)
{
  GuppiDataOp_String op;

  g_return_if_fail (seq != NULL && GUPPI_IS_SEQ_STRING (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (guppi_seq_in_bounds (GUPPI_SEQ (seq), i));

  op.op.op = op_set;
  op.i = i;
  op.str = str;
  op.copy = TRUE;

  /* NOTE : The index is ignored */
  guppi_seq_changed_set (GUPPI_SEQ (seq), i, i, (GuppiDataOp *) & op);
}

void
guppi_seq_string_prepend_nc (GuppiSeqString *seq, gchar *str)
{
  gint first;
  first = guppi_seq_min_index (GUPPI_SEQ (seq));
  guppi_seq_string_insert_nc (seq, first, str);
}

void
guppi_seq_string_prepend (GuppiSeqString *seq, const gchar *str)
{
  gint first;
  first = guppi_seq_min_index (GUPPI_SEQ (seq));
  guppi_seq_string_insert (seq, first, str);
}

void
guppi_seq_string_append_nc (GuppiSeqString *seq, gchar *str)
{
  gint last;
  last = guppi_seq_max_index (GUPPI_SEQ (seq));
  guppi_seq_string_insert_nc (seq, last + 1, str);
}

void
guppi_seq_string_append (GuppiSeqString *seq, const gchar *str)
{
  gint last;
  last = guppi_seq_max_index (GUPPI_SEQ (seq));
  guppi_seq_string_insert (seq, last + 1, str);
}

void
guppi_seq_string_insert_nc (GuppiSeqString *seq, gint i, gchar *str)
{
  GuppiDataOp_String op;

  g_return_if_fail (seq != NULL && GUPPI_IS_SEQ_STRING (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (str != NULL);

  op.op.op = op_insert;
  op.i = i;
  op.str = str;
  op.copy = FALSE;

  guppi_seq_changed_insert (GUPPI_SEQ (seq), i, 1, (GuppiDataOp *) & op);
}

void
guppi_seq_string_insert (GuppiSeqString *seq, gint i, const gchar *str)
{
  GuppiDataOp_String op;

  g_return_if_fail (seq != NULL && GUPPI_IS_SEQ_STRING (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (str != NULL);

  op.op.op = op_insert;
  op.i = i;
  op.str = str;
  op.copy = TRUE;

  guppi_seq_changed_insert (GUPPI_SEQ (seq), i, 1, (GuppiDataOp *) & op);
}

gint
guppi_seq_string_lookup (const GuppiSeqString *seq, const gchar *str)
{
  GuppiSeqStringClass *klass;

  g_return_val_if_fail (seq != NULL, 0);

  klass = GUPPI_SEQ_STRING_CLASS (GTK_OBJECT (seq)->klass);

  if (klass->lookup) {

    return klass->lookup ((GuppiSeqString *) seq, str);

  } else {
    gint i0 = guppi_seq_min_index (GUPPI_SEQ (seq));
    gint i1 = guppi_seq_max_index (GUPPI_SEQ (seq));
    gint i;

    g_assert (klass->get);

    for (i = i0; i <= i1; ++i) {
      if (!strcmp (str, klass->get ((GuppiSeqString *) seq, i)))
	return i;
    }
    return i1 + 1;
  }
}

gboolean
guppi_seq_string_contains (const GuppiSeqString *seq, const gchar *str)
{
  GuppiSeqStringClass *klass;

  g_return_val_if_fail (GUPPI_IS_SEQ_STRING (seq), FALSE);

  klass = GUPPI_SEQ_STRING_CLASS (GTK_OBJECT (seq)->klass);

  if (klass->contains) {

    return klass->contains ((GuppiSeqString *) seq, str);

  } else {

    return guppi_seq_in_bounds (GUPPI_SEQ (seq), guppi_seq_string_lookup (seq, str));
  }
}

gsize
guppi_seq_string_distinct_values (const GuppiSeqString *seq)
{
  GuppiSeqStringClass *klass;

  g_return_val_if_fail (GUPPI_IS_SEQ_STRING (seq), 0);

  klass = GUPPI_SEQ_STRING_CLASS (GTK_OBJECT (seq)->klass);

  if (klass->distinct) {

    return klass->distinct ((GuppiSeqString *) seq);

  } else {
    gsize count = 0;
    GHashTable *hash = g_hash_table_new (g_str_hash, g_str_equal);
    const gchar *str;
    gpointer data;
    gint i0 = guppi_seq_min_index (GUPPI_SEQ (seq));
    gint i1 = guppi_seq_max_index (GUPPI_SEQ (seq));
    gint i;

    g_assert (klass->get);

    for (i = i0; i <= i1; ++i) {
      str = klass->get ((GuppiSeqString *) seq, i);
      data = g_hash_table_lookup (hash, str);
      if (data == NULL) {
	++count;
	g_hash_table_insert (hash, (gchar *) str, (gpointer) str);
      }
    }

    g_hash_table_destroy (hash);
    return count;
  }
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */


static void
guppi_seq_string_class_init (GuppiSeqStringClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiSeqClass *seq_class = GUPPI_SEQ_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_SEQ);
  
  klass->set = set;
  klass->insert = insert;

  seq_class->export_xml_element = export_xml_element;
  seq_class->import_xml_element = import_xml_element;
  seq_class->insert_generic = insert_generic;

  object_class->finalize = guppi_seq_string_finalize;
}

static void
guppi_seq_string_init (GuppiSeqString *obj)
{

}

GtkType guppi_seq_string_get_type (void)
{
  static GtkType guppi_seq_string_type = 0;
  if (!guppi_seq_string_type) {
    static const GtkTypeInfo guppi_seq_string_info = {
      "GuppiSeqString",
      sizeof (GuppiSeqString),
      sizeof (GuppiSeqStringClass),
      (GtkClassInitFunc) guppi_seq_string_class_init,
      (GtkObjectInitFunc) guppi_seq_string_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_seq_string_type =
      gtk_type_unique (GUPPI_TYPE_SEQ, &guppi_seq_string_info);
  }
  return guppi_seq_string_type;
}

/* $Id$ */
