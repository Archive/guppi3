/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-seq-scalar.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_SEQ_SCALAR_H
#define _INC_GUPPI_SEQ_SCALAR_H

/* #include <gtk/gtk.h> */
#include "guppi-seq.h"
#include "guppi-seq-boolean.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS

typedef struct _GuppiSeqScalar GuppiSeqScalar;
typedef struct _GuppiSeqScalarClass GuppiSeqScalarClass;
struct _GuppiSeqScalarPrivate;

typedef struct _GuppiIndexedPair GuppiIndexedPair;
struct _GuppiIndexedPair {
  gint i;
  double x, y;
};
typedef void (*GuppiIndexedPairFn) (GuppiIndexedPair *, gsize N, gpointer closure);

struct _GuppiSeqScalar {
  GuppiSeq parent;
  struct _GuppiSeqScalarPrivate *priv;
};

struct _GuppiSeqScalarClass {
  GuppiSeqClass parent_class;

  /* virtual functions */
  void     (*range)       (GuppiSeqScalar *, double *min, double *max);
  double   (*sum)         (GuppiSeqScalar *);
  double   (*var)         (GuppiSeqScalar *);
  gboolean (*quartiles)   (GuppiSeqScalar *, double *q1, double *med, double *q3);
  gboolean (*percentile)  (GuppiSeqScalar *, double p, double *x);

  double   (*get)         (GuppiSeqScalar *, gint);
  void     (*set)         (GuppiSeqScalar *, gint, double);
  void     (*set_many)    (GuppiSeqScalar *, gint start, const double *, gint stride, gsize N);
  void     (*insert)      (GuppiSeqScalar *, gint, double);
  void     (*insert_many) (GuppiSeqScalar *, gint, const double *, gint stride, gsize N);

  gint     (*range_query) (GuppiSeqScalar *, GuppiSeqBoolean *, double min, double max, gboolean do_and);

  gint     (*gather_pairs) (GuppiSeqScalar *a, GuppiSeqScalar *b, 
			    double x0, double y0, double x1, double y1, 
			    GuppiIndexedPairFn fn, gpointer closure);

  double  *(*raw_access)  (GuppiSeqScalar *, gint *);

};

#define GUPPI_TYPE_SEQ_SCALAR (guppi_seq_scalar_get_type())
#define GUPPI_SEQ_SCALAR(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_SEQ_SCALAR,GuppiSeqScalar))
#define GUPPI_SEQ_SCALAR0(obj) ((obj) ? (GUPPI_SEQ_SCALAR(obj)) : NULL)
#define GUPPI_SEQ_SCALAR_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_SEQ_SCALAR,GuppiSeqScalarClass))
#define GUPPI_IS_SEQ_SCALAR(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_SEQ_SCALAR))
#define GUPPI_IS_SEQ_SCALAR0(obj) (((obj) == NULL) || (GUPPI_IS_SEQ_SCALAR(obj)))
#define GUPPI_IS_SEQ_SCALAR_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_SEQ_SCALAR))

GtkType guppi_seq_scalar_get_type (void);

double guppi_seq_scalar_get (const GuppiSeqScalar *seq, gint i);
void guppi_seq_scalar_set (GuppiSeqScalar *seq, gint i, double val);
void guppi_seq_scalar_set_many (GuppiSeqScalar *seq, gint start, const double *vals, gint stride, gsize N);

void guppi_seq_scalar_prepend (GuppiSeqScalar *seq, double val);
void guppi_seq_scalar_prepend_many (GuppiSeqScalar *seq, const double *vals, gint stride, gsize N);
void guppi_seq_scalar_prepend_repeating (GuppiSeqScalar *seq, double val, gsize N);

void guppi_seq_scalar_append (GuppiSeqScalar *seq, double val);
void guppi_seq_scalar_append_many (GuppiSeqScalar *seq, const double *vals, gint stride, gsize N);
void guppi_seq_scalar_append_repeating (GuppiSeqScalar *seq, double val, gsize N);

void guppi_seq_scalar_insert (GuppiSeqScalar *seq, gint i, double val);
void guppi_seq_scalar_insert_many (GuppiSeqScalar *seq, gint i, const double *vals, gint stride, gsize N);
void guppi_seq_scalar_insert_repeating (GuppiSeqScalar *seq, gint i, double val, gsize N);

GuppiSeqBoolean *guppi_seq_scalar_range_query (GuppiSeqScalar *seq, double min, double max);
gint guppi_seq_scalar_in_place_range_query (GuppiSeqScalar *seq, GuppiSeqBoolean *seq_bool, double min, double max);
gint guppi_seq_scalar_bitwise_and_range_query (GuppiSeqScalar *seq, GuppiSeqBoolean *seq_bool, double min, double max);

gint guppi_seq_scalar_gather_pairs (GuppiSeqScalar *a, GuppiSeqScalar *b, 
				    double x0, double y0, double x1, double y1,
				    GuppiIndexedPairFn fn, gpointer closure);

/*
  Return +1 if sequence is non-decreasing (or constant).
  Return -1 if sequence is non-increasing.
  Returns 0 otherwise
 */
gint guppi_seq_scalar_ordering (GuppiSeqScalar *seq);

double guppi_seq_scalar_min (GuppiSeqScalar *seq);
double guppi_seq_scalar_max (GuppiSeqScalar *seq);
double guppi_seq_scalar_sum (GuppiSeqScalar *seq);
double guppi_seq_scalar_sum_abs (GuppiSeqScalar *seq);
double guppi_seq_scalar_mean (GuppiSeqScalar *seq);
double guppi_seq_scalar_var (GuppiSeqScalar *seq);
double guppi_seq_scalar_vars (GuppiSeqScalar *seq);
double guppi_seq_scalar_sdev (GuppiSeqScalar *seq);
double guppi_seq_scalar_sdevs (GuppiSeqScalar *seq);
double guppi_seq_scalar_q1 (GuppiSeqScalar *seq);
double guppi_seq_scalar_median (GuppiSeqScalar *seq);
double guppi_seq_scalar_q3 (GuppiSeqScalar *seq);
double guppi_seq_scalar_percentile (GuppiSeqScalar *seq, double p);

const double *guppi_seq_scalar_raw (const GuppiSeqScalar *seq, gint *stride);

#define guppi_seq_scalar_raw_get(ptr, str, i) \
(*(const double *)(((gchar *)ptr)+(str)*(i)))


END_GUPPI_DECLS

#endif /* _INC_GUPPI_SEQ_SCALAR_H */

/* $Id$ */
