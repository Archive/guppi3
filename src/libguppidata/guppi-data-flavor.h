/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-data-flavor.h
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __GUPPI_DATA_FLAVOR_H__
#define __GUPPI_DATA_FLAVOR_H__

#include  "guppi-defs.h"
#include  "guppi-attribute-flavor.h"

BEGIN_GUPPI_DECLS

#define GUPPI_ATTR_DATA         (guppi_attribute_flavor_data ())
#define GUPPI_ATTR_DATA_SOCKET  (guppi_attribute_flavor_data_socket ())

GuppiAttributeFlavor guppi_attribute_flavor_data        (void);
GuppiAttributeFlavor guppi_attribute_flavor_data_socket (void);

END_GUPPI_DECLS

#endif /* __GUPPI_DATA_FLAVOR_H__ */

