/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-seq.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_SEQ_H
#define _INC_GUPPI_SEQ_H

/* #include <gtk/gtk.h> */
#include "guppi-data.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS 

typedef struct _GuppiSeq GuppiSeq;
typedef struct _GuppiSeqClass GuppiSeqClass;

struct _GuppiSeq {
  GuppiData parent;

  gpointer missing_data;
};

struct _GuppiSeqClass {
  GuppiDataClass parent_class;

  /* virtual functions */
  void (*size_hint)       (GuppiSeq *, gsize);
  void (*get_bounds)      (GuppiSeq *, gint *min, gint *max);
  void (*shift_indices)   (GuppiSeq *, gint delta);
  void (*insert_generic)  (GuppiSeq *, gint, gsize);
  void (*delete_many)     (GuppiSeq *, gint, gsize);

  xmlNodePtr (*export_xml_element) (GuppiSeq *seq, gint i, GuppiXMLDocument *);
  gboolean   (*import_xml_element) (GuppiSeq *seq, GuppiXMLDocument *, xmlNodePtr);
  
  gboolean support_missing_values;
  gboolean (*has_missing)       (GuppiSeq *);
  gsize    (*missing_count)     (GuppiSeq *);
  gboolean (*missing)           (GuppiSeq *, gint i);
  void     (*set_missing)       (GuppiSeq *, gint, gboolean);
  void     (*set_range_missing) (GuppiSeq *, gint i0, gint i1, gboolean x);
  void     (*set_many_missing)  (GuppiSeq *, gint *ind, gsize N, gboolean x);
  void     (*set_all_missing)   (GuppiSeq *, gboolean x);
  void     (*insert_missing)    (GuppiSeq *, gint i, gboolean x, gsize N);
  void     (*copy_missing)      (GuppiSeq *dest, GuppiSeq *src);

  /* signals */
  void (*changed_shift_indices) (GuppiSeq *seq, gint);
  void (*changed_set)           (GuppiSeq *seq, gint, gint);
  void (*changed_insert)        (GuppiSeq *seq, gint, gsize);
  void (*changed_grow)          (GuppiSeq *seq, gint i0, gint i1);
  void (*changed_delete)        (GuppiSeq *seq, gint, gsize);
};

#define GUPPI_TYPE_SEQ (guppi_seq_get_type())
#define GUPPI_SEQ(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_SEQ,GuppiSeq))
#define GUPPI_SEQ0(obj) ((obj) ? (GUPPI_SEQ(obj)) : NULL)
#define GUPPI_SEQ_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_SEQ,GuppiSeqClass))
#define GUPPI_IS_SEQ(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_SEQ))
#define GUPPI_IS_SEQ0(obj) (((obj) == NULL) || (GUPPI_IS_SEQ(obj)))
#define GUPPI_IS_SEQ_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_SEQ))

GtkType guppi_seq_get_type (void);

void guppi_seq_size_hint (GuppiSeq *seq, gsize expected_size);

void guppi_seq_indices (const GuppiSeq *seq, gint *min, gint *max);
#define guppi_seq_bounds(seq, min, max) (guppi_seq_indices((seq), (min), (max)))

gint guppi_seq_min_index (const GuppiSeq *seq);
gint guppi_seq_max_index (const GuppiSeq *seq);

/* Size = max index - min index + 1 */
gsize guppi_seq_size (const GuppiSeq *seq);

/* Count = size - # of missing values (so size == count in many cases) */
gsize guppi_seq_count (const GuppiSeq *seq);

gboolean guppi_seq_empty (const GuppiSeq *seq);	        /* size == 0 */
gboolean guppi_seq_nonempty (const GuppiSeq *seq);	/* size != 0 */

gboolean guppi_seq_absent (const GuppiSeq *seq);	/* count == 0 */
gboolean guppi_seq_present (const GuppiSeq *seq);	/* count != 0 */

gboolean guppi_seq_in_bounds (const GuppiSeq *seq, gint i);
gboolean guppi_seq_contains_bounds (const GuppiSeq *seq, const GuppiSeq *seq2);
gboolean guppi_seq_equal_bounds (const GuppiSeq *seq, const GuppiSeq *seq2);
void guppi_seq_common_bounds (const GuppiSeq *seq, const GuppiSeq *seq2,
			      gint *min, gint *max);

void guppi_seq_shift_indices (GuppiSeq *seq, gint delta);
void guppi_seq_set_min_index (GuppiSeq *seq, gint min);
void guppi_seq_set_max_index (GuppiSeq *seq, gint max);

void guppi_seq_delete (GuppiSeq *seq, gint i);
void guppi_seq_delete_many (GuppiSeq *seq, gint i, gsize N);
void guppi_seq_delete_range (GuppiSeq *seq, gint i0, gint i1);

void guppi_seq_grow_to_include (GuppiSeq *seq, gint i);
void guppi_seq_grow_to_include_range (GuppiSeq *seq, gint i0, gint i1);
void guppi_seq_grow_to_overlap (GuppiSeq *seq, const GuppiSeq *seq_to_overlap);


/* Missing values */

gboolean guppi_seq_has_missing (GuppiSeq *seq);
gsize    guppi_seq_missing_count (GuppiSeq *seq);
gboolean guppi_seq_missing (GuppiSeq *seq, gint i);
gboolean guppi_seq_available (GuppiSeq *seq, gint i);
void     guppi_seq_set_missing (GuppiSeq *seq, gint i);
void     guppi_seq_insert_missing (GuppiSeq *seq, gint i);
void     guppi_seq_prepend_missing (GuppiSeq *seq);
void     guppi_seq_append_missing (GuppiSeq *seq);


/* Don't call these unless you know what you are doing... */
void guppi_seq_changed_shift_indices (GuppiSeq *seq, gint i, GuppiDataOp *op);
void guppi_seq_changed_set (GuppiSeq *seq, gint i, gint j, GuppiDataOp *op);
void guppi_seq_changed_insert (GuppiSeq *seq, gint i, gsize N, GuppiDataOp *op);
void guppi_seq_changed_grow (GuppiSeq *seq, gint i0, gint i1, GuppiDataOp *op);
void guppi_seq_changed_delete (GuppiSeq *seq, gint i, gsize N, GuppiDataOp *op);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_SEQ_H */

/* $Id$ */
