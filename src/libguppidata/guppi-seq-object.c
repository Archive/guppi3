/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-seq-object.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-seq-object.h"

/* #include <gnome.h> */
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>

#include <guppi-useful.h>

static GtkObjectClass *parent_class = NULL;

static void
guppi_seq_object_finalize (GtkObject *obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_seq_object_class_init (GuppiSeqObjectClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;

  parent_class = gtk_type_class (GUPPI_TYPE_SEQ);

  object_class->finalize = guppi_seq_object_finalize;
}

static void
guppi_seq_object_init (GuppiSeqObject *obj)
{

}

GtkType guppi_seq_object_get_type (void)
{
  static GtkType guppi_seq_object_type = 0;
  if (!guppi_seq_object_type) {
    static const GtkTypeInfo guppi_seq_object_info = {
      "GuppiSeqObject",
      sizeof (GuppiSeqObject),
      sizeof (GuppiSeqObjectClass),
      (GtkClassInitFunc) guppi_seq_object_class_init,
      (GtkObjectInitFunc) guppi_seq_object_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_seq_object_type =
      gtk_type_unique (GUPPI_TYPE_SEQ, &guppi_seq_object_info);
  }
  return guppi_seq_object_type;
}

/**************************************************************************/

typedef struct _GuppiDataOp_Obj GuppiDataOp_Obj;
struct _GuppiDataOp_Obj {
  GuppiDataOp op;
  gint i;
  gsize N;
  GtkObject *obj;
};

static void
op_set (GuppiData *d, GuppiDataOp *in_op)
{
  GuppiDataOp_Obj *op = (GuppiDataOp_Obj *) in_op;
  GuppiSeqObjectClass *klass;
  GtkObject *old;

  klass = GUPPI_SEQ_OBJECT_CLASS (GTK_OBJECT (d)->klass);

  old = guppi_seq_object_get (GUPPI_SEQ_OBJECT (d), op->i);

  if (old != op->obj) {
    g_assert (klass->set);
    klass->set (GUPPI_SEQ_OBJECT (d), op->i, op->obj);
    
    guppi_unref (old);
    guppi_ref (op->obj);
  }
}

static void
op_insert (GuppiData *d, GuppiDataOp *in_op)
{
  GuppiDataOp_Obj *op = (GuppiDataOp_Obj *) in_op;
  GuppiSeqObjectClass *klass;

  klass = GUPPI_SEQ_OBJECT_CLASS (GTK_OBJECT (d)->klass);

  g_assert (klass->insert);
  klass->insert (GUPPI_SEQ_OBJECT (d), op->i, op->obj);
}

static void
op_insert_NULL (GuppiData *d, GuppiDataOp *in_op)
{
  GuppiDataOp_Obj *op = (GuppiDataOp_Obj *) in_op;
  GuppiSeqObjectClass *klass;

  klass = GUPPI_SEQ_OBJECT_CLASS (GTK_OBJECT (d)->klass);

  g_assert (klass->insert_NULL);
  klass->insert_NULL (GUPPI_SEQ_OBJECT (d), op->i, op->N);
}


/**************************************************************************/

GtkObject *
guppi_seq_object_get (const GuppiSeqObject *seq, gint i)
{
  GuppiSeqObjectClass *klass;

  g_return_val_if_fail (GUPPI_IS_SEQ_OBJECT (seq), NULL);
  g_return_val_if_fail (guppi_seq_in_bounds (GUPPI_SEQ (seq), i), NULL);

  klass = GUPPI_SEQ_OBJECT_CLASS (GTK_OBJECT (seq)->klass);

  g_assert (klass->get);
  return klass->get ((GuppiSeqObject *) seq, i);
}

void
guppi_seq_object_set (GuppiSeqObject *seq, gint i, GtkObject *obj)
{
  GuppiDataOp_Obj op;

  g_return_if_fail (GUPPI_IS_SEQ_OBJECT (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (guppi_seq_in_bounds (GUPPI_SEQ (seq), i));
  g_return_if_fail (obj == NULL || GTK_IS_OBJECT (obj));

  if (guppi_seq_object_get (seq, i) != obj) {

    op.op.op = op_set;
    op.i = i;
    op.obj = obj;

    guppi_seq_changed_set (GUPPI_SEQ (seq), i, i, (GuppiDataOp *) & op);
  }
}

void
guppi_seq_object_prepend (GuppiSeqObject *seq, GtkObject *obj)
{
  gint first;

  g_return_if_fail (GUPPI_IS_SEQ_OBJECT (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (obj == NULL || GTK_IS_OBJECT (obj));

  first = guppi_seq_min_index (GUPPI_SEQ (seq));
  guppi_seq_object_insert (seq, first, obj);
}

void
guppi_seq_object_prepend_NULL (GuppiSeqObject *seq, gsize N)
{
  gint first;

  g_return_if_fail (GUPPI_IS_SEQ_OBJECT (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  if (N == 0)
    return;

  first = guppi_seq_min_index (GUPPI_SEQ (seq));
  guppi_seq_object_insert_NULL (seq, first, N);
}


void
guppi_seq_object_append (GuppiSeqObject *seq, GtkObject *obj)
{
  gint last;

  g_return_if_fail (GUPPI_IS_SEQ_OBJECT (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (obj == NULL || GTK_IS_OBJECT (obj));

  last = guppi_seq_max_index (GUPPI_SEQ (seq));
  guppi_seq_object_insert (seq, last + 1, obj);
}

void
guppi_seq_object_append_NULL (GuppiSeqObject *seq, gsize N)
{
  gint last;

  g_return_if_fail (GUPPI_IS_SEQ_OBJECT (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));

  if (N == 0)
    return;

  last = guppi_seq_max_index (GUPPI_SEQ (seq));
  guppi_seq_object_insert_NULL (seq, last + 1, N);
}

void
guppi_seq_object_insert (GuppiSeqObject *seq, gint i, GtkObject *obj)
{
  GuppiDataOp_Obj op;

  g_return_if_fail (GUPPI_IS_SEQ_OBJECT (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (obj == NULL || GTK_IS_OBJECT (obj));

  op.op.op = op_insert;
  op.i = i;
  op.obj = obj;

  guppi_seq_changed_insert (GUPPI_SEQ (seq), i, 1, (GuppiDataOp *) & op);
}

void
guppi_seq_object_insert_NULL (GuppiSeqObject *seq, gint i, gsize N)
{
  GuppiDataOp_Obj op;

  g_return_if_fail (GUPPI_IS_SEQ_OBJECT (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));

  if (N == 0)
    return;

  op.op.op = op_insert_NULL;
  op.i = i;
  op.N = N;

  guppi_seq_changed_insert (GUPPI_SEQ (seq), i, 1, (GuppiDataOp *) & op);
}



/* $Id$ */
