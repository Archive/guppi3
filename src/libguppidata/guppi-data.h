/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-data.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_DATA_H
#define _INC_GUPPI_DATA_H

#include <gtk/gtk.h>
#include  "guppi-unique-id.h"
#include  "guppi-defs.h"
#include  "guppi-xml.h"

BEGIN_GUPPI_DECLS

typedef struct _GuppiData GuppiData;
typedef struct _GuppiDataClass GuppiDataClass;
typedef struct _GuppiDataOp GuppiDataOp;

struct _GuppiDataOp {
  void (*op) (GuppiData *, GuppiDataOp *);
};

typedef void (*GuppiDataFn) (GuppiData *data, gpointer closure);

struct _GuppiData {
  GtkObject parent;

  guppi_uniq_t id;
  gchar *label;
  gboolean read_only;

  gpointer pending_ops;
};

struct _GuppiDataClass {
  GtkObjectClass parent_class;

  gboolean is_leaf_type;
  gboolean read_only;

  /* virtual functions */
  void       (*build_menu) (GuppiData *, GtkMenu *, gpointer data_tree);
  GtkWidget *(*info_display) (GuppiData *);
  GuppiData *(*copy) (GuppiData *);
  gint       (*get_size_in_bytes) (GuppiData *);
  gchar     *(*get_size_info) (GuppiData *);
  void       (*foreach_subdata) (GuppiData *, GuppiDataFn, gpointer closure);
  void       (*export_xml) (GuppiData *, GuppiXMLDocument *doc, xmlNodePtr node);
  gboolean   (*import_xml) (GuppiData *, GuppiXMLDocument *doc, xmlNodePtr node);

  gboolean class_validated;
  gboolean (*validate_class) (GuppiDataClass *);

  gchar *plug_in_code;

  /* signals */
  void (*changed) (GuppiData *);
  void (*changed_label) (GuppiData *);
  void (*changed_subdata) (GuppiData *);
};

#define GUPPI_TYPE_DATA (guppi_data_get_type())
#define GUPPI_DATA(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_DATA,GuppiData))
#define GUPPI_DATA0(obj) ((obj) ? (GUPPI_DATA(obj)) : NULL)
#define GUPPI_DATA_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_DATA,GuppiDataClass))
#define GUPPI_IS_DATA(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_DATA))
#define GUPPI_IS_DATA0(obj) (((obj) == NULL) || (GUPPI_IS_DATA(obj)))
#define GUPPI_IS_DATA_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_DATA))

GtkType guppi_data_get_type (void);

GuppiData *guppi_data_new (const gchar *type_name);

guppi_uniq_t guppi_data_unique_id (GuppiData *data);

const gchar *guppi_data_get_label(GuppiData *data);
void         guppi_data_set_label (GuppiData *data, const gchar *label);

gboolean guppi_data_is_read_only (GuppiData *data);
gboolean guppi_data_can_change (GuppiData *data);

/*
 * If you just want to get a GuppiData object to emit a generic
 * "changed" signal, call this.
 */
void guppi_data_touch (GuppiData *data);
void guppi_data_touch_subdata (GuppiData *data);

GuppiData *guppi_data_copy              (GuppiData *data);
gint       guppi_data_get_size_in_bytes (GuppiData *data);
gchar     *guppi_data_get_size_info     (GuppiData *data);


xmlNodePtr guppi_data_export_xml (GuppiData *data, GuppiXMLDocument *doc);
GuppiData *guppi_data_import_xml (GuppiXMLDocument *doc, xmlNodePtr node);
void       guppi_data_spew_xml   (GuppiData *data);


gboolean guppi_data_has_subdata     (GuppiData *data);
gint     guppi_data_subdata_count   (GuppiData *data);
void     guppi_data_foreach_subdata (GuppiData *data,
				     void (*fn) (GuppiData *, gpointer),
				     gpointer user_data);

void       guppi_data_build_menu   (GuppiData *data, GtkMenu *menu, gpointer data_tree);
GtkWidget *guppi_data_info_display (GuppiData *data);


/* Don't call these unless you _really_ know what you are doing */
void guppi_data_add_pending_op (GuppiData *data, GuppiDataOp *op);
void guppi_data_changed (GuppiData *data);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_DATA_H */

/* $Id$ */
