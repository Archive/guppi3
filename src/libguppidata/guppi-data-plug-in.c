/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-data-plug-in.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-data-plug-in.h"

#include <guppi-memory.h>

static GtkObjectClass *parent_class = NULL;

static void
guppi_data_plug_in_finalize (GtkObject * obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_data_plug_in_class_init (GuppiDataPlugInClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;

  parent_class = gtk_type_class (GUPPI_TYPE_PLUG_IN);

  object_class->finalize = guppi_data_plug_in_finalize;
}

static void
guppi_data_plug_in_init (GuppiDataPlugIn * obj)
{

}

GtkType guppi_data_plug_in_get_type (void)
{
  static GtkType guppi_data_plug_in_type = 0;
  if (!guppi_data_plug_in_type) {
    static const GtkTypeInfo guppi_data_plug_in_info = {
      "GuppiDataPlugIn",
      sizeof (GuppiDataPlugIn),
      sizeof (GuppiDataPlugInClass),
      (GtkClassInitFunc) guppi_data_plug_in_class_init,
      (GtkObjectInitFunc) guppi_data_plug_in_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_data_plug_in_type = gtk_type_unique (GUPPI_TYPE_PLUG_IN, &guppi_data_plug_in_info);
  }
  return guppi_data_plug_in_type;
}

GuppiPlugIn *
guppi_data_plug_in_new (void)
{
  return GUPPI_PLUG_IN (guppi_type_new (guppi_data_plug_in_get_type ()));
}

GuppiData *
guppi_data_plug_in_create_data (GuppiDataPlugIn *plugin)
{
  g_return_val_if_fail (GUPPI_IS_DATA_PLUG_IN (plugin), NULL);
  g_return_val_if_fail (plugin->constructor, NULL);

  return plugin->constructor ();
}


/* $Id$ */
