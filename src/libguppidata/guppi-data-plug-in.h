/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-data-plug-in.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_DATA_PLUG_IN_H
#define _INC_GUPPI_DATA_PLUG_IN_H

/* #include <gtk/gtk.h> */
#include  "guppi-plug-in.h"
#include "guppi-data.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS 

typedef struct _GuppiDataPlugIn GuppiDataPlugIn;
typedef struct _GuppiDataPlugInClass GuppiDataPlugInClass;

struct _GuppiDataPlugIn {
  GuppiPlugIn parent;

  GuppiData *(*constructor) (void);
};

struct _GuppiDataPlugInClass {
  GuppiPlugInClass parent_class;
};

#define GUPPI_TYPE_DATA_PLUG_IN (guppi_data_plug_in_get_type())
#define GUPPI_DATA_PLUG_IN(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_DATA_PLUG_IN,GuppiDataPlugIn))
#define GUPPI_DATA_PLUG_IN0(obj) ((obj) ? (GUPPI_DATA_PLUG_IN(obj)) : NULL)
#define GUPPI_DATA_PLUG_IN_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_DATA_PLUG_IN,GuppiDataPlugInClass))
#define GUPPI_IS_DATA_PLUG_IN(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_DATA_PLUG_IN))
#define GUPPI_IS_DATA_PLUG_IN0(obj) (((obj) == NULL) || (GUPPI_IS_DATA_PLUG_IN(obj)))
#define GUPPI_IS_DATA_PLUG_IN_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_DATA_PLUG_IN))

GtkType guppi_data_plug_in_get_type (void);

GuppiPlugIn *guppi_data_plug_in_new (void);

GuppiData *guppi_data_plug_in_create_data (GuppiDataPlugIn *);



END_GUPPI_DECLS

#endif /* _INC_GUPPI_DATA_PLUG_IN_H */

/* $Id$ */
