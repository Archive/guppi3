/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-data-socket.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_DATA_SOCKET_H
#define _INC_GUPPI_DATA_SOCKET_H

#include <gtk/gtkobject.h>
#include  "guppi-defs.h"
#include  "guppi-xml.h"
#include "guppi-data.h"

BEGIN_GUPPI_DECLS;

typedef struct _GuppiDataSocket GuppiDataSocket;
typedef struct _GuppiDataSocketClass GuppiDataSocketClass;
struct _GuppiDataSocketPrivate;

typedef gboolean (*GuppiDataSocketTypeFn) (GuppiDataSocket *, GuppiData *, gpointer closure);
typedef void     (*GuppiDataSocketDestroyFn) (gpointer);
typedef gpointer (*GuppiDataSocketDupFn) (gpointer);

struct _GuppiDataSocket {
  GtkObject parent;
  struct _GuppiDataSocketPrivate *priv;
};

struct _GuppiDataSocketClass {
  GtkObjectClass parent_class;

  void (*changed) (GuppiDataSocket *); /* == contained GuppiData emitted "changed" */
  void (*plugged) (GuppiDataSocket *, GuppiData *new_data, GuppiData *old_data);
};

#define GUPPI_TYPE_DATA_SOCKET (guppi_data_socket_get_type ())
#define GUPPI_DATA_SOCKET(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_DATA_SOCKET,GuppiDataSocket))
#define GUPPI_DATA_SOCKET_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_DATA_SOCKET,GuppiDataSocketClass))
#define GUPPI_IS_DATA_SOCKET(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_DATA_SOCKET))
#define GUPPI_IS_DATA_SOCKET_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_DATA_SOCKET))

GtkType guppi_data_socket_get_type (void);

void             guppi_data_socket_construct   (GuppiDataSocket *sock,
						GuppiDataSocketTypeFn fn,
						GuppiDataSocketDestroyFn destroy_closure_fn,
						GuppiDataSocketDupFn dup_closure_fn,
						gpointer closure);

GuppiDataSocket *guppi_data_socket_new         (void);
GuppiDataSocket *guppi_data_socket_new_by_type (GtkType allowed_type);
GuppiDataSocket *guppi_data_socket_new_full    (GuppiDataSocketTypeFn fn,
						GuppiDataSocketDestroyFn destroy_closure_fn,
						GuppiDataSocketDupFn dup_closure_fn,
						gpointer closure);

GuppiData       *guppi_data_socket_get_data    (GuppiDataSocket *sock);
gboolean         guppi_data_socket_try_data    (GuppiDataSocket *sock, GuppiData *data);
void             guppi_data_socket_set_data    (GuppiDataSocket *sock, GuppiData *data);

/* The slave is set equal to the master. */
void             guppi_data_socket_connect     (GuppiDataSocket *master, GuppiDataSocket *slave);

xmlNodePtr       guppi_data_socket_export_xml  (GuppiDataSocket *sock, GuppiXMLDocument *doc);
gboolean         guppi_data_socket_import_xml  (GuppiDataSocket *sock, GuppiXMLDocument *doc, xmlNodePtr node);


END_GUPPI_DECLS;

#endif /* _INC_GUPPI_DATA_SOCKET_H */

/* $Id$ */
