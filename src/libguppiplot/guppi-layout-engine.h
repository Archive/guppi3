/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-layout-engine.h
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __GUPPI_LAYOUT_ENGINE_H__
#define __GUPPI_LAYOUT_ENGINE_H__

#include "guppi-layout-rule.h"
#include  "guppi-defs.h"
#include <gtk/gtkobject.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiLayoutEngine GuppiLayoutEngine;
typedef struct _GuppiLayoutEngineClass GuppiLayoutEngineClass;
struct _GuppiLayoutEnginePrivate;

typedef void (*GuppiLayoutEngineRuleFn) (GuppiLayoutEngine *engine, GuppiLayoutRule *rule, gpointer closure);
typedef void (*GuppiLayoutEngineGeometryFn) (GuppiLayoutEngine *engine, GuppiGeometry *geom, gpointer closure);

struct _GuppiLayoutEngine {
  GtkObject parent;
  struct _GuppiLayoutEnginePrivate *priv;
};

struct _GuppiLayoutEngineClass {
  GtkObjectClass parent_class;

  void (*changed)     (GuppiLayoutEngine *);
  void (*pre_layout)  (GuppiLayoutEngine *);
  void (*post_layout) (GuppiLayoutEngine *);
};

#define GUPPI_TYPE_LAYOUT_ENGINE            (guppi_layout_engine_get_type())
#define GUPPI_LAYOUT_ENGINE(obj)            (GTK_CHECK_CAST((obj),GUPPI_TYPE_LAYOUT_ENGINE,GuppiLayoutEngine))
#define GUPPI_LAYOUT_ENGINE0(obj)           ((obj) ? (GUPPI_LAYOUT_ENGINE(obj)) : NULL)
#define GUPPI_LAYOUT_ENGINE_CLASS(klass)    (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_LAYOUT_ENGINE,GuppiLayoutEngineClass))
#define GUPPI_IS_LAYOUT_ENGINE(obj)         (GTK_CHECK_TYPE((obj), GUPPI_TYPE_LAYOUT_ENGINE))
#define GUPPI_IS_LAYOUT_ENGINE0(obj)        (((obj) == NULL) || (GUPPI_IS_LAYOUT_ENGINE(obj)))
#define GUPPI_IS_LAYOUT_ENGINE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_LAYOUT_ENGINE))

GtkType guppi_layout_engine_get_type (void);

GuppiLayoutEngine *guppi_layout_engine_new (void);

void guppi_layout_engine_add_rule              (GuppiLayoutEngine *engine, GuppiLayoutRule *rule);
void guppi_layout_engine_remove_rule           (GuppiLayoutEngine *engine, GuppiLayoutRule *rule);
void guppi_layout_engine_remove_geometry_rules (GuppiLayoutEngine *engine, GuppiGeometry *geom);
void guppi_layout_engine_remove_geometry       (GuppiLayoutEngine *engine, GuppiGeometry *geom);
void guppi_layout_engine_replace_geometry      (GuppiLayoutEngine *engine, GuppiGeometry *old, GuppiGeometry *nuevo);

void guppi_layout_engine_foreach_rule     (GuppiLayoutEngine *engine, GuppiLayoutEngineRuleFn fn, gpointer closure);
void guppi_layout_engine_foreach_geometry (GuppiLayoutEngine *engine, GuppiLayoutEngineGeometryFn fn, gpointer closure);

void guppi_layout_engine_set_bounds               (GuppiLayoutEngine *engine, double x0, double y0, double x1, double y1);
void guppi_layout_engine_set_bounds_from_geometry (GuppiLayoutEngine *engine, GuppiGeometry *geom);
void guppi_layout_engine_unset_bounds             (GuppiLayoutEngine *engine);

void guppi_layout_engine_freeze (GuppiLayoutEngine *engine);
void guppi_layout_engine_thaw   (GuppiLayoutEngine *engine);
void guppi_layout_engine_flush  (GuppiLayoutEngine *engine);

xmlNodePtr         guppi_layout_engine_export_xml (GuppiLayoutEngine *engine, GuppiXMLDocument *doc);
GuppiLayoutEngine *guppi_layout_engine_import_xml (GuppiXMLDocument *doc, xmlNodePtr node);
void               guppi_layout_engine_spew_xml   (GuppiLayoutEngine *engine);


END_GUPPI_DECLS

#endif /* __GUPPI_LAYOUT_ENGINE_H__ */

