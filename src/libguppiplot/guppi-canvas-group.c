/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-canvas-group.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-canvas-group.h"

#include <guppi-memory.h>
#include <guppi-convenient.h>
#include "guppi-group-view.h"

typedef struct _GuppiCanvasGroupPrivate GuppiCanvasGroupPrivate;
struct _GuppiCanvasGroupPrivate {
};

#define priv(x) ((GuppiCanvasGroupPrivate*)((GUPPI_CANVAS_GROUP(x))->opaque_internals))


static GtkObjectClass *parent_class = NULL;

static void
position_subitem (GuppiCanvasItem *grp, GuppiCanvasItem *subitem)
{
  double x0, y0, x1, y1;
  gint cx0, cy0, cx1, cy1;

  if (guppi_canvas_item_positioned (grp) && guppi_canvas_item_positioned (subitem)) {

    gnome_canvas_item_show (GNOME_CANVAS_ITEM (subitem));
    guppi_canvas_item_get_bbox_pt (subitem, &x0, &y0, &x1, &y1);

    guppi_canvas_item_pt2c (grp, x0, y0, &cx0, &cy0);
    guppi_canvas_item_pt2c (grp, x1, y1, &cx1, &cy1);

    guppi_2sort_i (&cx0, &cx1);
    guppi_2sort_i (&cy0, &cy1);

    guppi_canvas_item_set_bbox_c (subitem, cx0, cy0, cx1+1, cy1+1);
    
  } else {
    gnome_canvas_item_hide (GNOME_CANVAS_ITEM (subitem));
  }
}

static void
changed_position_cb (GuppiElementView *view, gpointer closure)
{
  GuppiCanvasGroup *grp = GUPPI_CANVAS_GROUP (closure);
  GuppiCanvasItem *item;

  /* Yes, this is mildly inefficient since it requires us to do a linear search
     every time we change position. */
  item = guppi_canvas_group_find_by_view (grp, view);
  position_subitem (GUPPI_CANVAS_ITEM (grp), item);
}

static void
disconnect_cb (GuppiCanvasItem *item, gpointer closure)
{
  GuppiElementView *view = guppi_canvas_item_view (item);
  gtk_signal_disconnect_by_func (GTK_OBJECT (view),
				 GTK_SIGNAL_FUNC (changed_position_cb),
				 closure);
}

static void
guppi_canvas_group_destroy (GtkObject *obj)
{
  GuppiCanvasGroup *grp = GUPPI_CANVAS_GROUP (obj);

  guppi_canvas_group_foreach_item (grp, disconnect_cb, grp);

  if (parent_class->destroy)
    parent_class->destroy (obj);
}

static void
guppi_canvas_group_finalize (GtkObject *obj)
{
  GuppiCanvasGroup *grp = GUPPI_CANVAS_GROUP (obj);

  guppi_free (grp->opaque_internals);
  grp->opaque_internals = NULL;

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/***************************************************************************/

static void
remove_if_gone_iter_fn (GuppiCanvasItem *item, gpointer ptr)
{
  GuppiGroupView *grp;
  GuppiElementView *view;

  grp = GUPPI_GROUP_VIEW (guppi_canvas_item_view (GUPPI_CANVAS_ITEM (ptr)));
  view = guppi_canvas_item_view (item);

  if (!guppi_group_view_has (grp, view)) {
    gtk_object_destroy (GTK_OBJECT (item));
  }
}

static void
add_if_missing_iter_fn (GuppiElementView *view, gpointer ptr)
{
  GuppiCanvasGroup *canv_grp;
  GuppiCanvasItem *item;
  canv_grp = GUPPI_CANVAS_GROUP (ptr);

  if (!guppi_canvas_group_find_by_view (canv_grp, view)) {
    item = guppi_element_view_make_canvas_item (view,
						GNOME_CANVAS_ITEM (canv_grp)->
						canvas,
						GNOME_CANVAS_GROUP
						(canv_grp));
  }
}

static void
add_remove_items (GuppiCanvasGroup *grp_item)
{
  GuppiGroupView *grp;

  grp = GUPPI_GROUP_VIEW (guppi_canvas_item_view (GUPPI_CANVAS_ITEM (grp_item)));

  /* First, remove superfluous items. */
  guppi_canvas_group_foreach_item (grp_item, remove_if_gone_iter_fn, grp_item);

  /* Then add in new items. */
  guppi_group_view_foreach (grp, add_if_missing_iter_fn, grp_item);
}

static void
set_scale_cb (GuppiCanvasItem *item, gpointer closure)
{
  double scale = *(double *)closure;
  guppi_canvas_item_set_scale (item, scale);
}

static void
changed_scale (GuppiCanvasItem *item, double scale)
{
  guppi_canvas_group_foreach_item (GUPPI_CANVAS_GROUP (item), set_scale_cb, &scale);

  if (GUPPI_CANVAS_ITEM_CLASS (parent_class)->changed_scale)
    GUPPI_CANVAS_ITEM_CLASS (parent_class)->changed_scale (item, scale);
}

static void
changed_state (GuppiCanvasItem *item)
{
  //position_all_subitems (GUPPI_CANVAS_GROUP (item));

  if (GUPPI_CANVAS_ITEM_CLASS (parent_class)->changed_state)
    GUPPI_CANVAS_ITEM_CLASS (parent_class)->changed_state (item);
}

static void
changed_view (GuppiCanvasItem *item)
{
  add_remove_items (GUPPI_CANVAS_GROUP (item));

  if (GUPPI_CANVAS_ITEM_CLASS (parent_class)->changed_view)
    GUPPI_CANVAS_ITEM_CLASS (parent_class)->changed_view (item);
}

static void
foreach (GuppiCanvasItem *item, GuppiCanvasItemFunc fn, gpointer user_data)
{
  GList *iter;

  /* First, call function on self */
  fn (item, user_data);

  /* Next, call it on all subitems */
  iter = GNOME_CANVAS_GROUP (item)->item_list;
  while (iter != NULL) {
    GuppiCanvasItem *subitem = GUPPI_CANVAS_ITEM (iter->data);
    guppi_canvas_item_foreach (subitem, fn, user_data);
    iter = g_list_next (iter);
  }
}

static void
bbox_changed_hook (GuppiCanvasItem *item, gint cx0, gint cy0, gint cx1, gint cy1)
{
  GList *iter;

  iter = GNOME_CANVAS_GROUP (item)->item_list;
  while (iter != NULL) {
    position_subitem (item, GUPPI_CANVAS_ITEM (iter->data));
    iter = g_list_next (iter);
  }
}

static gboolean
double_click (GuppiCanvasItem *item,
	      guint button, guint state, double pt_x, double pt_y)
{
  GList *iter;

  iter = GNOME_CANVAS_GROUP (item)->item_list;
  while (iter != NULL) {
    GuppiCanvasItem *subitem = GUPPI_CANVAS_ITEM (iter->data);
    if (guppi_canvas_item_double_click (subitem, pt_x, pt_y, button, state))
      return TRUE;
    iter = g_list_next (iter);
  }

  return FALSE;
}

struct foreach_toolkit {
  void (*fn) (GuppiPlotToolkit *, gpointer);
  gpointer user_data;
};

static void
foreach_toolkit_fn (GuppiCanvasItem *item, gpointer ptr)
{
  struct foreach_toolkit *foo = (struct foreach_toolkit *) ptr;
  guppi_canvas_item_foreach_class_toolkit (item, foo->fn, foo->user_data);
}


static void
foreach_class_toolkit (GuppiCanvasItem *item,
		       void (*fn) (GuppiPlotToolkit *, gpointer),
		       gpointer user_data)
{
  struct foreach_toolkit foo = { fn, user_data };

  guppi_canvas_group_foreach_item (GUPPI_CANVAS_GROUP (item),
				   foreach_toolkit_fn, &foo);
}

static void
add_hook (GuppiCanvasGroup *grp, GuppiCanvasItem *item)
{
  GuppiElementView *view = guppi_canvas_item_view (item);
  
  gtk_signal_connect (GTK_OBJECT (view),
		      "changed_position",
		      GTK_SIGNAL_FUNC (changed_position_cb),
		      grp);
}

static void
remove_hook (GuppiCanvasGroup *grp, GuppiCanvasItem *item)
{
  GuppiElementView *view = guppi_canvas_item_view (item);
  gtk_signal_disconnect_by_func (GTK_OBJECT (view), 
				 GTK_SIGNAL_FUNC (changed_position_cb),
				 grp);
}


/***************************************************************************/

static void
guppi_canvas_group_class_init (GuppiCanvasGroupClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiCanvasItemClass *item_class = GUPPI_CANVAS_ITEM_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_CANVAS_ITEM);

  object_class->destroy = guppi_canvas_group_destroy;
  object_class->finalize = guppi_canvas_group_finalize;

  klass->add_hook = add_hook;
  klass->remove_hook = remove_hook;

  item_class->changed_scale = changed_scale;
  item_class->changed_state = changed_state;
  item_class->changed_view  = changed_view;

  item_class->foreach = foreach;
  item_class->bbox_changed_hook = bbox_changed_hook;
  item_class->double_click = double_click;
  item_class->foreach_class_toolkit = foreach_class_toolkit;
}

static void
guppi_canvas_group_init (GuppiCanvasGroup *obj)
{
  GuppiCanvasGroupPrivate *p;

  p = guppi_new0 (GuppiCanvasGroupPrivate, 1);
  obj->opaque_internals = p;
}

GtkType guppi_canvas_group_get_type (void)
{
  static GtkType guppi_canvas_group_type = 0;
  if (!guppi_canvas_group_type) {
    static const GtkTypeInfo guppi_canvas_group_info = {
      "GuppiCanvasGroup",
      sizeof (GuppiCanvasGroup),
      sizeof (GuppiCanvasGroupClass),
      (GtkClassInitFunc) guppi_canvas_group_class_init,
      (GtkObjectInitFunc) guppi_canvas_group_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_canvas_group_type = gtk_type_unique (GUPPI_TYPE_CANVAS_ITEM,
					       &guppi_canvas_group_info);
  }
  return guppi_canvas_group_type;
}

/***************************************************************************/

GuppiCanvasItem *
guppi_canvas_group_find_by_view (GuppiCanvasGroup *cgrp,
				 GuppiElementView *view)
{
  GList *iter;

  g_return_val_if_fail (cgrp && GUPPI_IS_CANVAS_GROUP (cgrp), NULL);
  g_return_val_if_fail (view && GUPPI_IS_ELEMENT_VIEW (view), NULL);

  iter = GNOME_CANVAS_GROUP (cgrp)->item_list;
  while (iter != NULL) {
    GuppiCanvasItem *item;

    item = GUPPI_CANVAS_ITEM (iter->data);
    if (view == guppi_canvas_item_view (item))
      return item;

    /* Descend into any "subgroups" */
    if (GUPPI_IS_CANVAS_GROUP (item)) {
      item = guppi_canvas_group_find_by_view (GUPPI_CANVAS_GROUP (item), view);
      if (item)
	return item;
    }

    iter = g_list_next (iter);
  }

  return NULL;
}

void
guppi_canvas_group_foreach_item (GuppiCanvasGroup *cgrp,
				 GuppiCanvasItemFunc fn, gpointer user_data)
{
  GList *iter;

  g_return_if_fail (cgrp && GUPPI_IS_CANVAS_GROUP (cgrp));
  g_return_if_fail (fn != NULL);

  iter = GNOME_CANVAS_GROUP (cgrp)->item_list;
  while (iter != NULL) {
    GuppiCanvasItem *item = GUPPI_CANVAS_ITEM (iter->data);
    GList *next = g_list_next (iter);
    fn (item, user_data);
    iter = next;
  }
}
