#!/usr/bin/perl -w

print <<EOF;
/* This code is automatically generated.  Edit at your own risk! */

#include <config.h>
#include "guppi-group-view-layout.h"

#include "guppi-layout-rule-predef.h"

EOF

while (defined (my $line = <>)) {

    chomp $line;
    $line =~ s/\#.*//g;
    next unless $line =~ /^([a-zA-Z0-9_]+):\s*(.*)/;

    my $name = $1;
    my @rest = map { $_ =~ s/geom/view/; $_ } split (/\s*,\s*/, $2);

    print "void\n";
    print "guppi_group_view_layout_$name (GuppiGroupView *group, ";

    my $first = 1;
    foreach my $var (@rest) {

	print ", " unless $first;
	$first = 0;

	if ($var =~ /view/) {
	    print "GuppiElementView *$var";
	} else {
	    print "double $var";
	}
    }
    print ")\n";

    print "{\n";
    print "  GuppiLayoutRule *rule;\n\n";
    print "  g_return_if_fail (group && GUPPI_IS_GROUP_VIEW (group));\n";
    foreach my $var (@rest) {
	if ($var =~ /view/) {
	    print "  g_return_if_fail ($var && GUPPI_IS_ELEMENT_VIEW ($var));\n";
	} else {
	    print "  g_return_if_fail ($var >= 0);\n";
	}
    }

    print "\n";

    print "  /* multiple adds are harmless */\n";
    foreach my $var (@rest) {
	if ($var =~ /view/) {
	    print "  guppi_group_view_add (group, $var);\n";
	}
    }

    print "\n";

    print "  rule = guppi_layout_rule_new_$name (", join (", ", map { /view/ ? "guppi_element_view_geometry ($_)" : $_ } @rest), ");\n";
    print "  g_return_if_fail (rule != NULL);\n";
    print "  guppi_layout_engine_add_rule (guppi_group_view_layout (group), rule);\n";
    print "  guppi_layout_rule_unref (rule);\n";

    print "}\n\n";
}

print <<EOF;
/* This code is automatically generated.  Edit at your own risk! */
EOF
