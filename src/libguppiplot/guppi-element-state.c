/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-element-state.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-element-state.h"

#include <gnome-xml/xmlmemory.h>
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>

#include <guppi-debug.h>
#include <guppi-i18n.h>
#include <guppi-convenient.h>
#include <guppi-memory.h>
#include <guppi-unique-id.h>
#include <guppi-plug-in-spec.h>
#include "guppi-plot-plug-in.h"
#include "guppi-element-view.h"
#include "guppi-root-group-state.h"

typedef struct _GuppiElementStatePrivate GuppiElementStatePrivate;
struct _GuppiElementStatePrivate {
  guppi_uniq_t id;
  GuppiAttributeBag *attr_bag;
  guint pending_change;

  gboolean doing_attr_change;
  GList *changed_attr;

  double cached_w, cached_h;
};

#define priv(x) ((x)->priv)


static GtkObjectClass *parent_class = NULL;

enum {
  CHANGED,
  CHANGED_SIZE,
  LAST_SIGNAL
};

static guint ges_signals[LAST_SIGNAL] = { 0 };

static void
guppi_element_state_finalize (GtkObject *obj)
{
  GuppiElementState *ges = GUPPI_ELEMENT_STATE (obj);
  GuppiElementStatePrivate *p = priv (ges);

  guppi_finalized (obj);

  guppi_unref0 (p->attr_bag);

  if (p->pending_change) {
    gtk_idle_remove (p->pending_change);
    p->pending_change = 0;
  }

  guppi_free0 (ges->priv);

  if (p->changed_attr) {
    g_list_free (p->changed_attr);
  }

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

typedef void (*GuppiSignal_NONE__DOUBLE_DOUBLE) (GtkObject *, double, double, gpointer user_data);

static void
guppi_marshal_NONE__DOUBLE_DOUBLE (GtkObject *object, GtkSignalFunc func, gpointer func_data, GtkArg *args)
{
  GuppiSignal_NONE__DOUBLE_DOUBLE rfunc;

  rfunc = (GuppiSignal_NONE__DOUBLE_DOUBLE) func;
  
  (*rfunc) (object, GTK_VALUE_DOUBLE (args[0]), GTK_VALUE_DOUBLE (args[1]), func_data);
}


static void
guppi_element_state_class_init (GuppiElementStateClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;

  parent_class = gtk_type_class (GTK_TYPE_OBJECT);

  object_class->finalize = guppi_element_state_finalize;

  ges_signals[CHANGED] =
    gtk_signal_new ("changed",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GuppiElementStateClass, changed),
		    gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);

  ges_signals[CHANGED_SIZE] =
    gtk_signal_new ("changed_size",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GuppiElementStateClass, changed_size),
		    guppi_marshal_NONE__DOUBLE_DOUBLE,
		    GTK_TYPE_NONE, 2, GTK_TYPE_DOUBLE, GTK_TYPE_DOUBLE);

  gtk_object_class_add_signals (object_class, ges_signals, LAST_SIGNAL);
}

static void push_changed_attr (GuppiElementState *, const gchar *str);

static void
bag_changed_cb (GuppiAttributeBag *bag, const gchar *name, gpointer closure)
{
  GuppiElementState *state = GUPPI_ELEMENT_STATE (closure);
  push_changed_attr (state, name);
  state->priv->doing_attr_change = TRUE;
  guppi_element_state_changed (state);
}

static void
guppi_element_state_init (GuppiElementState *obj)
{
  obj->priv = guppi_new0 (GuppiElementStatePrivate, 1);

  obj->priv->id       = guppi_unique_id ();

  obj->priv->attr_bag = guppi_attribute_bag_new ();
  guppi_attribute_bag_add_with_default (obj->priv->attr_bag, GUPPI_ATTR_STRING, "label", NULL, _("Unlabelled"));

  gtk_signal_connect (GTK_OBJECT (obj->priv->attr_bag),
		      "changed",
		      GTK_SIGNAL_FUNC (bag_changed_cb),
		      GTK_OBJECT (obj));

  obj->priv->cached_w = -1;
  obj->priv->cached_h = -1;
}

GtkType guppi_element_state_get_type (void)
{
  static GtkType guppi_element_state_type = 0;
  if (!guppi_element_state_type) {
    static const GtkTypeInfo guppi_element_state_info = {
      "GuppiElementState",
      sizeof (GuppiElementState),
      sizeof (GuppiElementStateClass),
      (GtkClassInitFunc) guppi_element_state_class_init,
      (GtkObjectInitFunc) guppi_element_state_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_element_state_type =
      gtk_type_unique (GTK_TYPE_OBJECT, &guppi_element_state_info);
  }
  return guppi_element_state_type;
}

/**************************************************************************/

GuppiElementState *
guppi_element_state_new (const gchar *type, ...)
{
  GuppiPlugIn *plug_in;
  GuppiPlotPlugIn *plot_plug_in;
  GuppiElementState *state;
  GuppiElementStateClass *klass;
  va_list args;

  g_return_val_if_fail (type && *type, NULL);

  if (!strcmp (type, "GuppiRootGroupState")) {

    state = guppi_root_group_state_new ();

  } else {
    /* Find our plug-in. */
    plug_in = guppi_plug_in_lookup ("plot", type);
    if (plug_in == NULL) {
      g_warning ("Unknown plot plug-in: \"%s\"", type);
      return NULL;
    }
    g_return_val_if_fail (GUPPI_IS_PLOT_PLUG_IN (plug_in), NULL);
    plot_plug_in = GUPPI_PLOT_PLUG_IN (plug_in);
    
    /* Build our plot element. */
    g_return_val_if_fail (plot_plug_in->element_constructor != NULL, NULL);
    state = plot_plug_in->element_constructor ();
    g_return_val_if_fail (state != NULL, NULL);

    /* Remember the plug in that created us, as necessary... */
    klass = GUPPI_ELEMENT_STATE_CLASS (GTK_OBJECT (state)->klass);
    if (klass->plug_in_code == NULL) {
      klass->plug_in_code = guppi_strdup (guppi_plug_in_code (plug_in));
      guppi_permanent_alloc (klass->plug_in_code);
    }
  }

  /* Apply our args */
  va_start (args, type);
  guppi_attribute_bag_vset (priv (state)->attr_bag, &args);
  va_end (args);

  return state;
}

/**************************************************************************/

guppi_uniq_t
guppi_element_state_unique_id (GuppiElementState *state)
{
  g_return_val_if_fail (GUPPI_IS_ELEMENT_STATE (state), 0);
  return priv (state)->id;
}

GuppiAttributeBag *
guppi_element_state_attribute_bag (GuppiElementState *state)
{
  g_return_val_if_fail (GUPPI_IS_ELEMENT_STATE (state), NULL);
  return priv (state)->attr_bag;
}

const gchar *
guppi_element_state_get_label (GuppiElementState *state)
{
  const gchar *label = NULL;
  g_return_val_if_fail (GUPPI_IS_ELEMENT_STATE (state), NULL);

  guppi_attribute_bag_get1 (state->priv->attr_bag, "label::raw", &label);
  return label;
}

void
guppi_element_state_set_label (GuppiElementState *state, const gchar *label)
{
  g_return_if_fail (GUPPI_IS_ELEMENT_STATE (state));
  guppi_attribute_bag_set (state->priv->attr_bag, "label", label, NULL);
}

gboolean
guppi_element_state_get (GuppiElementState *state, ...)
{
  va_list args;
  const gchar *key;
  gboolean rv = TRUE;

  g_return_val_if_fail (GUPPI_IS_ELEMENT_STATE (state), FALSE);

  va_start (args, state);
  do {
    key = va_arg (args, const gchar *);

    if (key != NULL) {

      if (!strcmp (key, "label")) {
	gchar **dest = va_arg (args, gchar **);
	*dest = g_strdup (guppi_element_state_get_label (state));
      } else {
	gpointer ptr = va_arg (args, gpointer);
	if (ptr != NULL)
	  guppi_attribute_bag_get1 (priv (state)->attr_bag, key, ptr);
      }
    }

  } while (key != NULL);

  return rv;
}

gboolean
guppi_element_state_set (GuppiElementState *state, ...)
{
  va_list args;
  const gchar *key;
  gboolean rv = TRUE;

  g_return_val_if_fail (GUPPI_IS_ELEMENT_STATE (state), FALSE);

  va_start (args, state);
  do {
    key = va_arg (args, const gchar *);

    if (key != NULL) {

      if (!strcmp (key, "label")) {
	guppi_element_state_set_label (state, va_arg (args, const gchar *));
      } else {
	guppi_attribute_bag_vset1 (priv (state)->attr_bag, key, &args);
      }
    }
  } while (key != NULL);
  va_end (args);

  return rv;
}

/**************************************************************************/

GuppiElementView *
guppi_element_state_make_view (GuppiElementState *ges)
{
  GuppiElementStateClass *klass;
  GuppiElementView *view = NULL;

  g_return_val_if_fail (ges != NULL, NULL);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_STATE (ges), NULL);

  klass = GUPPI_ELEMENT_STATE_CLASS (GTK_OBJECT (ges)->klass);

  if (klass->view_type && klass->make_view) {
    g_warning ("For %s, both a view type and a view constructor are defined.",
	       gtk_type_name (GTK_OBJECT_TYPE (ges)));
  }

  if (klass->view_type) {

    view = GUPPI_ELEMENT_VIEW (guppi_type_new (klass->view_type));

  } else if (klass->make_view) {

    view = klass->make_view (ges);

  }

  /* Tweak the insides of our view object. */
  if (view)
    guppi_element_view_set_state (view, ges);

  /* If our state has a size associated with it already, pass it onto the
     view so that the size calculation doesn't need to be repeated. */
  if (ges->priv->cached_w >= 0 || ges->priv->cached_h >= 0) {
    guppi_element_view_changed_size (view, ges->priv->cached_w, ges->priv->cached_h);
  }

  return view;
}

/***************************************************************************/

static void
push_changed_attr (GuppiElementState *ges, const gchar *str)
{
  ges->priv->changed_attr = g_list_prepend (ges->priv->changed_attr, (gpointer) str);
}

static void
pop_changed_attr (GuppiElementState *ges)
{
  GList *node = ges->priv->changed_attr;
  ges->priv->changed_attr = g_list_remove_link (ges->priv->changed_attr, ges->priv->changed_attr);
  g_list_free_1 (node);
}

void
guppi_element_state_changed (GuppiElementState *ges)
{
  GuppiElementStatePrivate *p;

  g_return_if_fail (ges != NULL);
  g_return_if_fail (GUPPI_IS_ELEMENT_STATE (ges));

  p = priv (ges);

  if (p->pending_change) {
    gtk_idle_remove (p->pending_change);
    p->pending_change = 0;
  }

  if (! ges->priv->doing_attr_change) {
    push_changed_attr (ges, NULL);
    ges->priv->doing_attr_change = FALSE;
  }
  gtk_signal_emit (GTK_OBJECT (ges), ges_signals[CHANGED]);
  pop_changed_attr (ges);
}

static gint
delayed_changer (gpointer foo)
{
  GuppiElementStatePrivate *p;
  GuppiElementState *ges;

  g_return_val_if_fail (foo != NULL && GUPPI_IS_ELEMENT_STATE (foo), FALSE);

  ges = GUPPI_ELEMENT_STATE (foo);
  p = priv (ges);

  p->pending_change = 0;
  guppi_element_state_changed (ges);

  return FALSE;
}

void
guppi_element_state_changed_delayed (GuppiElementState *ges)
{
  GuppiElementStatePrivate *p;

  g_return_if_fail (ges != NULL && GUPPI_IS_ELEMENT_STATE (ges));

  p = priv (ges);

  if (p->pending_change == 0)
    p->pending_change = gtk_idle_add_priority (G_PRIORITY_HIGH_IDLE, delayed_changer, ges);
}

void
guppi_element_state_flush_changes (GuppiElementState *ges)
{
  g_return_if_fail (GUPPI_IS_ELEMENT_STATE (ges));

  if (ges->priv->pending_change != 0) {
    guppi_element_state_changed (ges);
  }
}

const gchar *
guppi_element_state_get_changed_attribute (GuppiElementState *ges)
{
  g_return_val_if_fail (GUPPI_IS_ELEMENT_STATE (ges), NULL);

  return ges->priv->changed_attr ? (const gchar *) ges->priv->changed_attr->data : NULL;
}

void
guppi_element_state_changed_size (GuppiElementState *ges, double width, double height)
{
  g_return_if_fail (ges != NULL);
  g_return_if_fail (GUPPI_IS_ELEMENT_STATE (ges));

  /* Store the width and height so that they can be passed on to any views we might
     create later. */
  ges->priv->cached_w = width;
  ges->priv->cached_h = height;

  gtk_signal_emit (GTK_OBJECT (ges), ges_signals[CHANGED_SIZE], width, height);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

GuppiConfigModel *
guppi_element_state_make_config_model (GuppiElementState *state)
{
  GuppiElementStateClass *klass;
  GuppiConfigModel *model;

  g_return_val_if_fail (GUPPI_IS_ELEMENT_STATE (state), NULL);

  model = guppi_config_model_new ();

  klass = GUPPI_ELEMENT_STATE_CLASS (GTK_OBJECT (state)->klass);
  if (klass->make_config_model)
    klass->make_config_model (state, model);

  return model;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

xmlNodePtr
guppi_element_state_export_xml (GuppiElementState *state, GuppiXMLDocument *doc)
{
  GuppiElementStateClass *klass;
  xmlNodePtr state_node = NULL;
  xmlNodePtr attr_node = NULL;
  gchar *uid_str;

  g_return_val_if_fail (state && GUPPI_IS_ELEMENT_STATE (state), NULL);
  g_return_val_if_fail (doc != NULL, NULL);

  klass = GUPPI_ELEMENT_STATE_CLASS (GTK_OBJECT (state)->klass);

  state_node = xmlNewNode (doc->ns, "ElementState");

  if (klass->plug_in_code) {
    xmlNewProp (state_node, "Type", klass->plug_in_code);
  }
  
  uid_str = guppi_uniq2str (priv (state)->id);
  xmlNewProp (state_node, "UID", uid_str);
  guppi_free (uid_str);

  attr_node = guppi_attribute_bag_export_xml (priv (state)->attr_bag, doc);  
  if (attr_node) {
    if (attr_node->xmlChildrenNode != NULL) {
      xmlAddChild (state_node, attr_node);
    } else {
      xmlFreeNode (attr_node);
    }
  }

  if (klass->xml_export) {
    klass->xml_export (state, doc, state_node);
  }

  return state_node;
}

GuppiElementState *
guppi_element_state_import_xml (GuppiXMLDocument *doc, xmlNodePtr node)
{
  GuppiElementState *state = NULL;
  GuppiElementStateClass *klass;
  gchar *type_str = NULL, *uid_str = NULL;
  gboolean loaded_attr_bag = FALSE;

  g_return_val_if_fail (doc != NULL, NULL);
  g_return_val_if_fail (node != NULL, NULL);

  if (strcmp (node->name, "ElementState"))
    return NULL;

  type_str = xmlGetProp (node, "Type");
  state = guppi_element_state_new (type_str, NULL);
  if (state == NULL) {
    g_warning ("Unknown GuppiElementState type '%s'", type_str);
    goto finished;
  }

  klass = GUPPI_ELEMENT_STATE_CLASS (GTK_OBJECT (state)->klass);

  uid_str = xmlGetProp (node, "UID");
  priv (state)->id = guppi_str2uniq (uid_str);

  node = node->xmlChildrenNode;

  while (node != NULL) {

    if (!loaded_attr_bag && guppi_attribute_bag_import_xml (priv (state)->attr_bag, doc, node)) {

      loaded_attr_bag = TRUE;

    } else if (klass->xml_import) {

      klass->xml_import (state, doc, node);

    }

    node = node->next;
  }
  
 finished:
  if (type_str)
    xmlFree (type_str);
  if (uid_str)
    xmlFree (uid_str);

  return state;
}

void
guppi_element_state_spew_xml (GuppiElementState *state)
{
  GuppiXMLDocument *doc;
  g_return_if_fail (state && GUPPI_IS_ELEMENT_STATE (state));

  doc = guppi_xml_document_new ();
  guppi_xml_document_set_root (doc, guppi_element_state_export_xml (state, doc));
  guppi_xml_document_spew (doc);
  guppi_xml_document_free (doc);
}

/* $Id$ */
