/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-group-view.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-group-view.h"

#include <guppi-debug.h>
#include <guppi-memory.h>
#include <guppi-convenient.h>
#include "guppi-canvas-item.h"
#include "guppi-canvas-group.h"
#include "guppi-group-state.h"

enum {
  VIEW_ADD,
  VIEW_REMOVE,
  VIEW_REPLACE,
  LAST_SIGNAL
};

guint guppi_group_view_signals[LAST_SIGNAL] = { 0 };


typedef struct _GuppiGroupViewPrivate GuppiGroupViewPrivate;
struct _GuppiGroupViewPrivate {
  GList *elements;
  GuppiLayoutEngine *layout;

  gboolean touched;
};

#define priv(x) ((GuppiGroupViewPrivate*)((GUPPI_GROUP_VIEW(x))->opaque_internals))

static GtkObjectClass *parent_class = NULL;

static void
guppi_group_view_finalize (GtkObject *obj)
{
  GuppiGroupView *grp = GUPPI_GROUP_VIEW (obj);
  GuppiGroupViewPrivate *p = priv (grp);

  guppi_unref0 (p->layout);
  g_list_foreach (p->elements, guppi_unref2, NULL);
  g_list_free (p->elements);

  guppi_free0 (grp->opaque_internals);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/**************************************************************************/

static void
make_item_iter_fn (GuppiElementView *view, gpointer user_data)
{
  GnomeCanvasGroup *group = GNOME_CANVAS_GROUP (user_data);
  GuppiCanvasGroupClass *group_klass;
  GnomeCanvas *canvas = GNOME_CANVAS_ITEM (group)->canvas;
  GuppiCanvasItem *item;
  double scale;

  /* Create our item w/ the correct initial scale */
  item = guppi_element_view_make_canvas_item (view, canvas, group);
  scale = guppi_canvas_item_scale (GUPPI_CANVAS_ITEM (group));
  guppi_canvas_item_set_scale (item, scale);

  group_klass = GUPPI_CANVAS_GROUP_CLASS (GTK_OBJECT (group)->klass);
  if (group_klass->add_hook)
    group_klass->add_hook (GUPPI_CANVAS_GROUP (group), item);
}

static void
group_add_cb (GuppiGroupView *group, GuppiElementView *view, gpointer user_data)
{
  make_item_iter_fn (view, user_data);
}

static void
group_remove_cb (GuppiGroupView *group, GuppiElementView *view, gpointer user_data)
{
  /* GuppiCanvasGroup *group_item = GUPPI_CANVAS_GROUP (user_data); */
  
  guppi_FIXME ();
}

static void
group_replace_cb (GuppiGroupView *group, GuppiElementView *old, GuppiElementView *nuevo, gpointer user_data)
{
  /* GuppiCanvasGroup *group_item = GUPPI_CANVAS_GROUP (user_data); */

  guppi_FIXME ();
}

static GuppiCanvasItem *
make_canvas_item (GuppiElementView *view, GnomeCanvas *canvas, GnomeCanvasGroup *canvas_group)
{
  GuppiElementViewClass *klass;
  GnomeCanvasItem *gnoitem;

  klass = GUPPI_ELEMENT_VIEW_CLASS (GTK_OBJECT (view)->klass);
  g_assert (klass->canvas_item_type);
  gnoitem = gnome_canvas_item_new (canvas_group, klass->canvas_item_type, NULL);

  gtk_signal_connect (GTK_OBJECT (view),
		      "view_add",
		      GTK_SIGNAL_FUNC (group_add_cb),
		      gnoitem);

  gtk_signal_connect (GTK_OBJECT (view),
		      "view_remove",
		      GTK_SIGNAL_FUNC (group_remove_cb),
		      gnoitem);

  gtk_signal_connect (GTK_OBJECT (view),
		      "view_replace",
		      GTK_SIGNAL_FUNC (group_replace_cb),
		      gnoitem);

  return GUPPI_CANVAS_ITEM (gnoitem);
}

static void
item_post_creation_init (GuppiElementView *view, GuppiCanvasItem *item)
{
  guppi_group_view_foreach (GUPPI_GROUP_VIEW (view), make_item_iter_fn, item);
}

static void
print (GuppiElementView *view, GnomePrintContext *pc)
{
  GuppiGroupViewPrivate *p = priv (GUPPI_GROUP_VIEW (view));

  guppi_layout_engine_flush (p->layout);

  /* Chain our call */
  if (GUPPI_ELEMENT_VIEW_CLASS (parent_class)->print)
    GUPPI_ELEMENT_VIEW_CLASS (parent_class)->print (view, pc);

  guppi_group_view_foreach (GUPPI_GROUP_VIEW (view),
			    (void (*)(GuppiElementView *, gpointer)) guppi_element_view_print,
			    pc);
}

static gboolean
find (GuppiElementView *view, const gchar *label, GuppiElementState **es, GuppiElementView **ev)
{
  GuppiElementViewClass *klass;
  GuppiGroupView *grp = GUPPI_GROUP_VIEW (view);
  GList *iter;

  for (iter = priv (grp)->elements; iter != NULL; iter = g_list_next (iter)) {
    GuppiElementView *subview = iter->data;
    klass = GUPPI_ELEMENT_VIEW_CLASS (GTK_OBJECT (subview)->klass);
    if (klass->find (subview, label, es, ev))
      return TRUE;
  }

  if (GUPPI_ELEMENT_VIEW_CLASS (parent_class)->find)
    return GUPPI_ELEMENT_VIEW_CLASS (parent_class)->find (view, label, es, ev);

  return FALSE;
}

/**************************************************************************/

static void
config_model_iter_cb (GuppiElementView *view, gpointer closure)
{
  GuppiConfigModel *final_cm = closure;
  GuppiConfigModel *cm = guppi_element_view_make_config_model (view);

  guppi_config_model_combine (final_cm, NULL, cm);
  guppi_unref (cm);
}

static void
make_config_model (GuppiElementView *view, GuppiConfigModel *model)
{
  guppi_group_view_foreach (GUPPI_GROUP_VIEW (view),
			    config_model_iter_cb,
			    model);

}

/**************************************************************************/

typedef struct _ExportInfo ExportInfo;
struct _ExportInfo {
  GuppiXMLDocument *doc;
  xmlNodePtr views_node;
};

static void
xml_export_cb (GuppiElementView *view, gpointer ptr)
{
  ExportInfo *info = (ExportInfo *) ptr;
  xmlAddChild (info->views_node, guppi_element_view_export_xml (view, info->doc));
}

static void
xml_export (GuppiElementView *view, GuppiXMLDocument *doc, xmlNodePtr root_node)
{
  GuppiGroupView *grp = GUPPI_GROUP_VIEW (view);
  ExportInfo *info = g_new0 (ExportInfo, 1);

  info->doc         = doc;
  info->views_node  = xmlNewNode (doc->ns, "Views");

  /* Flush our layout calculation early. */
  guppi_layout_engine_flush (priv (grp)->layout);

  guppi_group_view_foreach (grp, xml_export_cb, info);
  xmlAddChild (root_node, info->views_node);

  xmlAddChild (root_node, guppi_layout_engine_export_xml (priv (grp)->layout, doc));

  g_free (info);
}

static gboolean
xml_import (GuppiElementView *view, GuppiXMLDocument *doc, xmlNodePtr node)
{
  GuppiGroupView *grp = GUPPI_GROUP_VIEW (view);

  if (!strcmp (node->name, "Views")) {

    node = node->xmlChildrenNode;
    while (node != NULL) {
      GuppiElementView *subview;

      subview = guppi_element_view_import_xml (doc, node);
      if (subview)
	guppi_group_view_add (grp, subview);
      guppi_unref (subview);

      node = node->next;
    }

    return TRUE;

  } else {
    GuppiLayoutEngine *layout = guppi_layout_engine_import_xml (doc, node);
    if (layout) {
      guppi_refcounting_assign (priv (grp)->layout, layout);
      guppi_unref (layout);
      return TRUE;
    }
  }
  return FALSE;
}

/**************************************************************************/

static void
freeze (GuppiElementView *view)
{
  GuppiGroupView *grp = GUPPI_GROUP_VIEW (view);

  guppi_layout_engine_freeze (priv (grp)->layout);
}

static void
thaw (GuppiElementView *view)
{
  GuppiGroupView *grp = GUPPI_GROUP_VIEW (view);

  guppi_layout_engine_thaw (priv (grp)->layout);
}

/**************************************************************************/

static void
changed (GuppiElementView *view)
{
  guppi_group_view_foreach (GUPPI_GROUP_VIEW (view),
			    (void (*)(GuppiElementView *, gpointer)) guppi_element_view_changed,
			    NULL);

  if (GUPPI_ELEMENT_VIEW_CLASS (parent_class)->changed)
    GUPPI_ELEMENT_VIEW_CLASS (parent_class)->changed (view);
}

static void
changed_size (GuppiElementView *view, double w, double h)
{
  if (GUPPI_ELEMENT_VIEW_CLASS (parent_class)->changed_size)
    GUPPI_ELEMENT_VIEW_CLASS (parent_class)->changed_size (view, w, h);

#if 0
  /* Signal each child that we've changed, in case they need to resize, etc. */
  guppi_group_view_foreach (GUPPI_GROUP_VIEW (view),
			    (void (*)(GuppiElementView *, gpointer)) guppi_element_view_changed,
			    NULL);
#endif
}

static void
changed_position (GuppiElementView *view)
{
  GuppiGroupView *grp = GUPPI_GROUP_VIEW (view);
  double x0, y0, x1, y1;

  guppi_element_view_get_bbox_pt (view, &x0, &y0, &x1, &y1);
  guppi_layout_engine_set_bounds (priv (grp)->layout, x0, y0, x1, y1);

  if (GUPPI_ELEMENT_VIEW_CLASS (parent_class)->changed_position)
    GUPPI_ELEMENT_VIEW_CLASS (parent_class)->changed_position (view);
}

/**************************************************************************/

static void
guppi_group_view_class_init (GuppiGroupViewClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementViewClass *view_class = GUPPI_ELEMENT_VIEW_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_VIEW);

  object_class->finalize = guppi_group_view_finalize;

  view_class->canvas_item_type  = GUPPI_TYPE_CANVAS_GROUP;
  view_class->make_canvas_item  = make_canvas_item;
  view_class->item_post_creation_init = item_post_creation_init;
  view_class->print             = print;
  view_class->make_config_model = make_config_model;
  view_class->xml_export        = xml_export;
  view_class->xml_import        = xml_import;
  view_class->freeze            = freeze;
  view_class->thaw              = thaw;
  view_class->find              = find;
  view_class->changed           = changed;
  view_class->changed_size      = changed_size;
  view_class->changed_position  = changed_position;

  guppi_group_view_signals[VIEW_ADD] = 
    gtk_signal_new ("view_add",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GuppiGroupViewClass, view_add),
		    gtk_marshal_NONE__POINTER,
		    GTK_TYPE_NONE,
		    1, GTK_TYPE_POINTER);

  guppi_group_view_signals[VIEW_REMOVE] = 
    gtk_signal_new ("view_remove",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GuppiGroupViewClass, view_remove),
		    gtk_marshal_NONE__POINTER,
		    GTK_TYPE_NONE,
		    1, GTK_TYPE_POINTER);

  guppi_group_view_signals[VIEW_REPLACE] = 
    gtk_signal_new ("view_replace",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GuppiGroupViewClass, view_replace),
		    gtk_marshal_NONE__POINTER_POINTER,
		    GTK_TYPE_NONE,
		    1, GTK_TYPE_POINTER, GTK_TYPE_POINTER);

  gtk_object_class_add_signals (object_class, guppi_group_view_signals, LAST_SIGNAL);
}

static void
guppi_group_view_init (GuppiGroupView *obj)
{
  GuppiGroupViewPrivate *p;
  p = guppi_new0 (GuppiGroupViewPrivate, 1);
  obj->opaque_internals = p;
  p->layout = guppi_layout_engine_new ();
}

GtkType
guppi_group_view_get_type (void)
{
  static GtkType guppi_group_view_type = 0;
  if (!guppi_group_view_type) {
    static const GtkTypeInfo guppi_group_view_info = {
      "GuppiGroupView",
      sizeof (GuppiGroupView),
      sizeof (GuppiGroupViewClass),
      (GtkClassInitFunc) guppi_group_view_class_init,
      (GtkObjectInitFunc) guppi_group_view_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_group_view_type =
      gtk_type_unique (GUPPI_TYPE_ELEMENT_VIEW, &guppi_group_view_info);
  }
  return guppi_group_view_type;
}

GuppiGroupView *
guppi_group_view_new (void)
{
  GuppiElementState *state = guppi_group_state_new ();
  GuppiElementView *view = guppi_element_view_new (state, NULL);
  guppi_unref (state);
  return GUPPI_GROUP_VIEW (view);
}

GuppiLayoutEngine *
guppi_group_view_layout (GuppiGroupView *view)
{
  g_return_val_if_fail (view && GUPPI_IS_GROUP_VIEW (view), NULL);
  return priv (view)->layout;
}

/**************************************************************************/

gboolean 
guppi_group_view_has (GuppiGroupView *grp, GuppiElementView *view)
{
  g_return_val_if_fail (grp != NULL && GUPPI_IS_GROUP_VIEW (grp), FALSE);
  g_return_val_if_fail (view != NULL && GUPPI_IS_ELEMENT_VIEW (view), FALSE);

  return g_list_find (priv (grp)->elements, view) != NULL;
}

void
guppi_group_view_add (GuppiGroupView *grp, GuppiElementView *view)
{
  GuppiGroupViewPrivate *p;

  g_return_if_fail (grp != NULL && GUPPI_IS_GROUP_VIEW (grp));
  g_return_if_fail (view != NULL && GUPPI_IS_ELEMENT_VIEW (view));

  p = priv (grp);

  if (!guppi_group_view_has (grp, view)) {
    GuppiGroupViewClass *klass;
    klass = GUPPI_GROUP_VIEW_CLASS (GTK_OBJECT (grp)->klass);

    p->elements = g_list_append (p->elements, view);

    if (klass->add_hook)
      klass->add_hook (grp, view);

    gtk_signal_connect_object (GTK_OBJECT (view),
			       "changed_structure",
			       GTK_SIGNAL_FUNC (guppi_element_view_changed_structure),
			       GTK_OBJECT (grp));

    guppi_ref (view);

    gtk_signal_emit (GTK_OBJECT (grp), guppi_group_view_signals[VIEW_ADD], view);
    guppi_element_view_changed_structure (GUPPI_ELEMENT_VIEW (grp));
  }
}

void
guppi_group_view_remove (GuppiGroupView *grp, GuppiElementView *view)
{
  GuppiGroupViewPrivate *p;
  GList *node;

  g_return_if_fail (grp != NULL && GUPPI_IS_GROUP_VIEW (grp));
  g_return_if_fail (view != NULL && GUPPI_IS_ELEMENT_VIEW (view));

  p = priv (grp);

  node = g_list_find (p->elements, view);
  g_return_if_fail (node != NULL);

  guppi_layout_engine_remove_geometry (p->layout, guppi_element_view_geometry (view));

  gtk_signal_disconnect_by_func (GTK_OBJECT (view),
				 GTK_SIGNAL_FUNC (guppi_element_view_changed_structure),
				 grp);

  guppi_unref (view);
  p->elements = g_list_remove_link (p->elements, node);
  g_list_free_1 (node);

  gtk_signal_emit (GTK_OBJECT (grp), guppi_group_view_signals[VIEW_REMOVE], view);
  guppi_element_view_changed_structure (GUPPI_ELEMENT_VIEW (grp));
}

void
guppi_group_view_replace (GuppiGroupView *grp,
			  GuppiElementView *old, GuppiElementView *nuevo)
{
  GuppiGroupViewPrivate *p;
  GuppiGeometry *old_geom;
  GuppiGeometry *nuevo_geom;
  GList *old_node;

  g_return_if_fail (grp != NULL && GUPPI_IS_GROUP_VIEW (grp));
  g_return_if_fail (old != NULL && GUPPI_IS_ELEMENT_VIEW (old));
  g_return_if_fail (nuevo != NULL && GUPPI_IS_ELEMENT_VIEW (nuevo));
  
  if (old == nuevo)
    return;

  p = priv (grp);

  old_node = g_list_find (p->elements, old);
  g_return_if_fail (old_node != NULL);
  g_return_if_fail (g_list_find (p->elements, nuevo) == NULL);

  old_geom = guppi_element_view_geometry (old);
  nuevo_geom = guppi_element_view_geometry (nuevo);

  guppi_layout_engine_replace_geometry (p->layout, old_geom, nuevo_geom);

  guppi_ref (old);
  guppi_refcounting_assign (old_node->data, nuevo);
  gtk_signal_emit (GTK_OBJECT (grp), guppi_group_view_signals[VIEW_REPLACE], old);
  guppi_unref (old);

  guppi_element_view_changed_structure (GUPPI_ELEMENT_VIEW (grp));
}

void
guppi_group_view_foreach (GuppiGroupView *grp,
			  void (*fn) (GuppiElementView *, gpointer),
			  gpointer user_data)
{
  GuppiGroupViewPrivate *p;
  GList *iter;

  g_return_if_fail (grp != NULL && GUPPI_IS_GROUP_VIEW (grp));
  g_return_if_fail (fn != NULL);

  p = priv (grp);

  for (iter = p->elements; iter != NULL; iter = g_list_next (iter))
    fn (GUPPI_ELEMENT_VIEW (iter->data), user_data);
}

gint
guppi_group_view_compare_z (GuppiGroupView *grp, GuppiElementView *a, GuppiElementView *b)
{
  GList *iter;
  gint seen_a, seen_b, count;

  g_return_val_if_fail (GUPPI_IS_GROUP_VIEW (grp), 0);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (a), 0);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (b), 0);

  seen_a = -1;
  seen_b = -1;
  count  = 0;
  
  iter = priv (grp)->elements;
  while (iter != NULL && (seen_a == -1 || seen_b == -1)) {
    if (seen_a == -1 && iter->data == a)
      seen_a = count;
    if (seen_b == -1 && iter->data == b)
      seen_b = count;
    ++count;
    iter = g_list_next (iter);
  }

  if (seen_a == -1 || seen_b == -1)
    return 0;

  return seen_a - seen_b;
}

void
guppi_group_view_raise (GuppiGroupView *grp, GuppiElementView *a, GuppiElementView *b)
{
  GList *iter, *node_a = NULL, *node_b = NULL;

  g_return_if_fail (GUPPI_IS_GROUP_VIEW (grp));
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (a));
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (b));

  iter = priv (grp)->elements;
  while (iter && !(node_a && node_b)) {
    if (!node_a && iter->data == a) {
      node_a = iter;
    }
    if (!node_b && iter->data == b) {
      if (node_a == NULL)
	return;
      node_b = iter;
    }
    iter = g_list_next (iter);
  }

  if (!(node_a && node_b))
    return;

  priv (grp)->elements = g_list_remove_link (priv (grp)->elements, node_b);
  if (node_a->prev)
    node_a->prev->next = node_b;
  node_b->prev = node_a->prev;  
  node_b->next = node_a;
  node_a->prev = node_b;
}

void
guppi_group_view_raise_to_top (GuppiGroupView *grp, GuppiElementView *a)
{
  GList *iter;
  GuppiGroupViewPrivate *p;

  g_return_if_fail (GUPPI_IS_GROUP_VIEW (grp));
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (a));

  p = priv (grp);

  iter = p->elements;
  while (iter) {
    if (iter->data == a)
      break;
    iter = g_list_next (iter);
  }

  if (iter) {
    p->elements = g_list_remove_link (p->elements, iter);
    p->elements = g_list_append (p->elements, iter->data);
    g_list_free_1 (iter);
  }
}

void
guppi_group_view_sink_to_bottom (GuppiGroupView *grp, GuppiElementView *a)
{
  GList *iter;
  GuppiGroupViewPrivate *p;

  g_return_if_fail (GUPPI_IS_GROUP_VIEW (grp));
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (a));

  p = priv (grp);

  iter = p->elements;
  while (iter) {
    if (iter->data == a)
      break;
    iter = g_list_next (iter);
  }

  if (iter) {
    p->elements = g_list_remove_link (p->elements, iter);
    p->elements = g_list_prepend (p->elements, iter->data);
    g_list_free_1 (iter);
  }
}

/* $Id$ */
