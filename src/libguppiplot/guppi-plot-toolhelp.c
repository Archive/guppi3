/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-plot-toolhelp.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-plot-toolhelp.h"

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>

#include <guppi-memory.h>


static GtkObjectClass *parent_class = NULL;

static void
guppi_plot_toolhelp_destroy (GtkObject * obj)
{
  if (parent_class->destroy)
    parent_class->destroy (obj);
}

static void
guppi_plot_toolhelp_finalize (GtkObject * obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_plot_toolhelp_class_init (GuppiPlotToolhelpClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;

  parent_class = gtk_type_class (GTK_TYPE_TABLE);

  object_class->destroy = guppi_plot_toolhelp_destroy;
  object_class->finalize = guppi_plot_toolhelp_finalize;

}

static void
guppi_plot_toolhelp_init (GuppiPlotToolhelp * obj)
{

}

GtkType guppi_plot_toolhelp_get_type (void)
{
  static GtkType guppi_plot_toolhelp_type = 0;
  if (!guppi_plot_toolhelp_type) {
    static const GtkTypeInfo guppi_plot_toolhelp_info = {
      "GuppiPlotToolhelp",
      sizeof (GuppiPlotToolhelp),
      sizeof (GuppiPlotToolhelpClass),
      (GtkClassInitFunc) guppi_plot_toolhelp_class_init,
      (GtkObjectInitFunc) guppi_plot_toolhelp_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_plot_toolhelp_type =
      gtk_type_unique (GTK_TYPE_TABLE, &guppi_plot_toolhelp_info);
  }
  return guppi_plot_toolhelp_type;
}

GtkWidget *
guppi_plot_toolhelp_new (GuppiCanvasItem * item, double pt_x, double pt_y)
{
  GtkWidget *w;
  g_return_val_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item), NULL);

  w = GTK_WIDGET (guppi_type_new (guppi_plot_toolhelp_get_type ()));

  guppi_plot_toolhelp_construct (GUPPI_PLOT_TOOLHELP (w), item, pt_x, pt_y);

  return w;
}

/**************************************************************************/

void
guppi_plot_toolhelp_construct (GuppiPlotToolhelp * help,
			       GuppiCanvasItem * item,
			       double pt_x, double pt_y)
{
  GtkTable *table;
#if 0
  GList *key_list;
  GList *iter;
#endif
  guint button_count = 0, key_count = 0;
  guint r, c, i, j, k;
  const guint opts = GTK_EXPAND | GTK_FILL;
  const gint label_xpad = 3;
  const gint label_ypad = 1;

  const gchar *modifier[4] = { "",
    N_("s-"),
    N_("C-"),
    N_("C-s-")
  };
  const gint mask[4] = { 0,
    GDK_SHIFT_MASK,
    GDK_CONTROL_MASK,
    GDK_SHIFT_MASK | GDK_CONTROL_MASK
  };

  GuppiPlotTool *tools[GUPPI_PLOT_TOOLKIT_BUTTON_MAX * 4];

  g_return_if_fail (help != NULL && GUPPI_IS_PLOT_TOOLHELP (help));
  g_return_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item));

  /* We ignore key-bindings for now. */

  /* Look up all button tools and count them as we go. */
  k = 0;
  for (i = 0; i < 4; ++i)
    for (j = 1; j <= GUPPI_PLOT_TOOLKIT_BUTTON_MAX; ++j) {
      tools[k] = NULL;
      guppi_canvas_item_locate_button_tool (item, pt_x, pt_y,
					    j, mask[i], NULL, &tools[k]);
      if (tools[k] != NULL)
	++button_count;
      ++k;
    }

  table = GTK_TABLE (help);
  gtk_table_set_homogeneous (table, FALSE);

  r = MAX (button_count, key_count) + 4;
  c = 0;
  if (button_count > 0)
    c += 3;
  if (key_count > 0)
    c += 3;
  if (c == 6)
    c += 2;

  gtk_table_resize (table, r, c);

  gtk_table_attach (table, gtk_hseparator_new (), 0, c, 1, 2, opts, opts, 0,
		    0);

  gtk_table_attach (table, gtk_hseparator_new (), 0, c, 3, 4, opts, opts, 0,
		    0);

  if (button_count > 0) {

    gtk_table_attach (table, gtk_label_new (_("Mouse Bindings")),
		      0, 3, 0, 1, opts, opts, label_xpad, label_ypad);

    gtk_table_attach (table, gtk_label_new (_("button")),
		      0, 1, 2, 3, opts, opts, label_xpad, label_ypad);

    gtk_table_attach (table, gtk_label_new (_("action")),
		      2, 3, 2, 3, opts, opts, label_xpad, label_ypad);

    gtk_table_attach (table, gtk_vseparator_new (),
		      1, 2, 1, r, opts, opts, 0, 0);
  }


  if (key_count > 0) {

    gtk_table_attach (table, gtk_label_new (_("Key Bindings")),
		      c - 3, c, 0, 1, opts, opts, label_xpad, label_ypad);

    gtk_table_attach (table, gtk_label_new (_("key")),
		      c - 3, c - 2, 2, 3, opts, opts, label_xpad, label_ypad);

    gtk_table_attach (table, gtk_label_new (_("action")),
		      c - 1, c, 2, 3, opts, opts, label_xpad, label_ypad);

    gtk_table_attach (table, gtk_vseparator_new (),
		      c - 2, c - 1, 1, r, opts, opts, 0, 0);
  }

  if (button_count > 0 && key_count > 0) {

    gtk_table_attach (table, gtk_vseparator_new (),
		      3, 4, 0, r, opts, opts, 0, 0);

    gtk_table_attach (table, gtk_vseparator_new (),
		      4, 5, 0, r, opts, opts, 0, 0);
  }


  /* Attach mouse clicks */
  r = 4;
  k = 0;
  for (i = 0; i < 4; ++i) {
    for (j = 1; j <= GUPPI_PLOT_TOOLKIT_BUTTON_MAX; ++j) {
      GuppiPlotTool *tool = tools[k];
      ++k;

      if (tool) {
	gchar *key_name;
	GtkWidget *key;
	GtkWidget *binding;

	key_name = guppi_strdup_printf ("%s%d", _(modifier[i]), j);
	key = gtk_label_new (key_name);
	guppi_free (key_name);
	gtk_misc_set_alignment (GTK_MISC (key), 1, 0.5);


	binding = gtk_label_new (guppi_plot_tool_name (tool));
	gtk_misc_set_alignment (GTK_MISC (binding), 0, 0.5);


	gtk_table_attach (table, key, 0, 1, r, r + 1,
			  opts, opts, label_xpad, label_ypad);
	gtk_table_attach (table, binding, 2, 3, r, r + 1,
			  opts, opts, label_xpad, label_ypad);

	gtk_widget_show (key);
	gtk_widget_show (binding);

	++r;
      }
    }
  }

#if 0
  /* Attach key codes */
  r = 4;
  key_list = iter = guppi_plot_toolkit_get_all_keys (tk);
  while (iter != NULL) {
    GuppiPlotKeystroke *ks = (GuppiPlotKeystroke *) iter->data;
    gchar *key_name;
    GtkWidget *key;
    GtkWidget *binding;

    key_name = guppi_strdup_printf ("%s%s", _(modifier[ks->state]),
				gdk_keyval_name (ks->key_code));
    key = gtk_label_new (key_name);
    guppi_free (key_name);
    gtk_misc_set_alignment (GTK_MISC (key), 1, 0.5);

    binding = gtk_label_new (guppi_plot_tool_name (ks->tool));
    gtk_misc_set_alignment (GTK_MISC (binding), 0, 0.5);


    gtk_table_attach (table, key, c - 3, c - 2, r, r + 1,
		      opts, opts, label_xpad, label_ypad);

    gtk_table_attach (table, binding, c - 1, c, r, r + 1,
		      opts, opts, label_xpad, label_ypad);

    gtk_widget_show (key);
    gtk_widget_show (binding);

    ++r;

    guppi_free (ks);
    iter = g_list_next (iter);
  }
  g_list_free (key_list);
#endif
}



/* $Id$ */
