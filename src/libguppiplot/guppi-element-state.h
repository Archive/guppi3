/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-element-state.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_ELEMENT_STATE_H
#define _INC_GUPPI_ELEMENT_STATE_H

/* #include <gnome.h> */
#include "guppi-unique-id.h"
#include "guppi-attribute-bag.h"
#include "guppi-config-model.h"
#include "guppi-xml.h"
#include "guppi-defs.h"

BEGIN_GUPPI_DECLS

/* forward defs */
struct _GuppiElementView;

typedef struct _GuppiElementState GuppiElementState;
typedef struct _GuppiElementStateClass GuppiElementStateClass;
struct _GuppiElementStatePrivate;

struct _GuppiElementState {
  GtkObject parent;
  struct _GuppiElementStatePrivate *priv;
};

struct _GuppiElementStateClass {
  GtkObjectClass parent_class;

  const gchar *name;
  const gchar *plug_in_code;

  /* --- VTable --- */

  GtkType view_type;
  struct _GuppiElementView *(*make_view) (GuppiElementState *);

  void (*make_config_model) (GuppiElementState *, GuppiConfigModel *);

  /* XML input & output */
  void     (*xml_export)  (GuppiElementState *, GuppiXMLDocument *, xmlNodePtr root_node);
  gboolean (*xml_import)  (GuppiElementState *, GuppiXMLDocument *, xmlNodePtr);

  /* Signals */
  void (*changed)      (GuppiElementState *);
  void (*changed_size) (GuppiElementState *, double width, double height);
};

#define GUPPI_TYPE_ELEMENT_STATE (guppi_element_state_get_type())
#define GUPPI_ELEMENT_STATE(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_ELEMENT_STATE,GuppiElementState))
#define GUPPI_ELEMENT_STATE0(obj) ((obj) ? (GUPPI_ELEMENT_STATE(obj)) : NULL)
#define GUPPI_ELEMENT_STATE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_ELEMENT_STATE,GuppiElementStateClass))
#define GUPPI_IS_ELEMENT_STATE(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_ELEMENT_STATE))
#define GUPPI_IS_ELEMENT_STATE0(obj) (((obj) == NULL) || (GUPPI_IS_ELEMENT_STATE(obj)))
#define GUPPI_IS_ELEMENT_STATE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_ELEMENT_STATE))

GtkType guppi_element_state_get_type (void);

GuppiElementState *guppi_element_state_new (const gchar *type, ...);

guppi_uniq_t       guppi_element_state_unique_id     (GuppiElementState *);
GuppiAttributeBag *guppi_element_state_attribute_bag (GuppiElementState *);

const gchar *guppi_element_state_get_label (GuppiElementState *);
void         guppi_element_state_set_label (GuppiElementState *, const gchar *label);

gboolean guppi_element_state_get (GuppiElementState *, ...);
gboolean guppi_element_state_set (GuppiElementState *, ...);

struct _GuppiElementView *guppi_element_state_make_view (GuppiElementState *);

void         guppi_element_state_changed (GuppiElementState *);
void         guppi_element_state_changed_delayed (GuppiElementState *);
void         guppi_element_state_flush_changes (GuppiElementState *);
const gchar *guppi_element_state_get_changed_attribute (GuppiElementState *);

void guppi_element_state_changed_size (GuppiElementState *, double width, double height);

GuppiConfigModel *guppi_element_state_make_config_model (GuppiElementState *);

xmlNodePtr         guppi_element_state_export_xml (GuppiElementState *, GuppiXMLDocument *);
GuppiElementState *guppi_element_state_import_xml (GuppiXMLDocument *, xmlNodePtr);

void               guppi_element_state_spew_xml   (GuppiElementState *);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_ELEMENT_STATE_H */

/* $Id$ */
