/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-config-dialog.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include "guppi-config-dialog.h"

#include "guppi-memory.h"
#include "guppi-multiview.h"

typedef struct _PageInfo PageInfo;
struct _PageInfo {
  gchar *major_label;
  gchar *minor_label;
  GtkWidget *w;
};

static void
dialog_button_cb (GtkWidget *w, gint button_num, gpointer closure)
{
  gtk_widget_destroy (w);
}

static void
config_iter_cb (const gchar *major_label, const gchar *minor_label,
		GuppiConfigType type, GtkWidget *w, gpointer closure)
{
  GHashTable *hash = closure;
  PageInfo *info = guppi_new0 (PageInfo, 1);
  GList *items;
  gboolean first_item;
  
  info->major_label = guppi_strdup (major_label);
  info->minor_label = guppi_strdup (minor_label);
  info->w           = w;

  if (info->w == NULL)
    info->w = gtk_label_new ("Missing Widget");

  items = g_hash_table_lookup (hash, info->major_label);
  first_item = items == NULL;
  items = g_list_append (items, info);
  if (first_item) {
    g_hash_table_insert (hash, info->major_label, items);
  }
}

static void
menu_activate_cb (GtkWidget *w, gpointer closure)
{
  GtkWidget *outer = closure;
  GuppiMultiview *multiview = gtk_object_get_data (GTK_OBJECT (outer), "multiview");

  guppi_multiview_set_current (multiview, outer);
}

static void
info_hash_iter_cb (gpointer key, gpointer val, gpointer closure)
{
  GList *items = (GList *) val;
  GuppiMultiview *multiview = closure;
  GtkMenu *menu = gtk_object_get_data (GTK_OBJECT (multiview), "menu");
  GtkWidget *mi;
  GtkWidget *outer;

  if (g_list_length (items) == 1) {
    PageInfo *info = items->data;
    outer = gtk_frame_new (info->minor_label);

    gtk_container_add (GTK_CONTAINER (outer), info->w);
    gtk_widget_show (info->w);
  } else {
    outer = gtk_notebook_new ();
    while (items != NULL) {
      PageInfo *info = items->data;
      GtkWidget *label = gtk_label_new (info->minor_label);
      gtk_notebook_append_page (GTK_NOTEBOOK (outer), info->w, label);
      gtk_widget_show (info->w);
      items = g_list_next (items);
    }
  }
  gtk_object_set_data (GTK_OBJECT (outer), "multiview", multiview);

  mi = gtk_menu_item_new_with_label ((gchar *) key);
  gtk_menu_append (menu, mi);
  gtk_signal_connect (GTK_OBJECT (mi),
		      "activate",
		      GTK_SIGNAL_FUNC (menu_activate_cb),
		      outer);


  guppi_multiview_append_child (multiview, outer);
  gtk_widget_show (outer);
  gtk_widget_show_all (mi);
}

static void
info_hash_free_cb (gpointer key, gpointer val, gpointer closure)
{
  GList *items = (GList *) val;
  
  while (items != NULL) {
    PageInfo *info = items->data;
    guppi_free (info->major_label);
    guppi_free (info->minor_label);
    guppi_free (info);
    items = g_list_next (items);
  }

  g_list_free ((GList *) val);
}

GtkWidget *
guppi_config_dialog_new (GuppiConfigModel *model, GuppiRootGroupView *rgv)
{
  GtkWidget *dialog;
  GHashTable *info_hash;
  GtkWidget *table;
  GtkWidget *option_menu;
  GtkWidget *menu;
  GtkWidget *multiview;

  g_return_val_if_fail (GUPPI_IS_CONFIG_MODEL (model), NULL);
  g_return_val_if_fail (rgv == NULL || GUPPI_IS_ROOT_GROUP_VIEW (rgv), NULL);

  dialog = gnome_dialog_new ("Configuration",
			     GNOME_STOCK_BUTTON_OK,
			     GNOME_STOCK_BUTTON_CANCEL,
			     NULL);

  gtk_signal_connect (GTK_OBJECT (dialog),
		      "clicked",
		      GTK_SIGNAL_FUNC (dialog_button_cb),
		      model);

  info_hash = g_hash_table_new (g_str_hash, g_str_equal);
  guppi_config_model_foreach (model, config_iter_cb, info_hash);

  table = gtk_table_new (2, 2, FALSE);

  option_menu = gtk_option_menu_new ();
  menu = gtk_menu_new ();
  gtk_option_menu_set_menu (GTK_OPTION_MENU (option_menu), menu);
  gtk_widget_show (menu);

  multiview = guppi_multiview_new ();
  gtk_object_set_data (GTK_OBJECT (multiview), "menu", menu);
  g_hash_table_foreach (info_hash, info_hash_iter_cb, multiview);
  gtk_option_menu_set_history (GTK_OPTION_MENU (option_menu), 0);

  g_hash_table_foreach (info_hash, info_hash_free_cb, NULL);
  g_hash_table_destroy (info_hash);
  info_hash = NULL;

  gtk_table_attach (GTK_TABLE (table), option_menu,
		    0, 1, 0, 1,
		    GTK_EXPAND | GTK_FILL,
		    0,
		    0, 0);
  gtk_widget_show_all (option_menu);
  
  if (rgv) {
    GtkWidget *w = (GtkWidget *) guppi_root_group_view_make_canvas (rgv, NULL);
    if (w) {
      GtkWidget *shell = gtk_hbox_new (FALSE, 0);
      gtk_container_add (GTK_CONTAINER (shell), w);
      gtk_widget_show (w);
      gtk_widget_show (shell);
      gtk_widget_set_usize (shell, 300, 200);

      gtk_table_attach (GTK_TABLE (table), shell,
			1, 2, 0, 2,
			GTK_EXPAND | GTK_FILL,
			GTK_EXPAND | GTK_FILL,
			0, 0);
    }
  }

  gtk_table_attach (GTK_TABLE (table), multiview,
		    0, 1, 1, 2,
		    GTK_EXPAND | GTK_FILL,
		    GTK_EXPAND | GTK_FILL,
		    0, 0);
  gtk_widget_show (multiview);

  gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox), table, TRUE, TRUE, 0);
  gtk_widget_show (table);
  
  gtk_window_set_policy (GTK_WINDOW (dialog), FALSE, TRUE, TRUE);

  return dialog;
}
