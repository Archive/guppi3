/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-root-group-view.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-root-group-view.h"

#include <stdlib.h>
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>
#include <libgnomeui/gnome-stock.h>

#include <guppi-debug.h>
#include <guppi-memory.h>
#include <guppi-convenient.h>
#include <guppi-metrics.h>
#include "guppi-root-group-state.h"
#include "guppi-root-group-item.h"

/* Harmonious defaults */
#define DEFAULT_WIDTH  (6*72)
#define DEFAULT_HEIGHT (6*72/1.61803398874989484820)

static GtkObjectClass *parent_class = NULL;

static void
guppi_root_group_view_destroy (GtkObject *obj)
{
  if (parent_class->destroy)
    parent_class->destroy (obj);
}

static void
guppi_root_group_view_finalize (GtkObject *obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/***************************************************************************/

static void
changed_state (GuppiElementView *view)
{
  if (GUPPI_ELEMENT_VIEW_CLASS (parent_class)->changed_state)
    (GUPPI_ELEMENT_VIEW_CLASS (parent_class)->changed_state) (view);
}

static void
view_init (GuppiElementView *view)
{

}

static void
xml_export (GuppiElementView *view, GuppiXMLDocument *doc, xmlNodePtr root_node)
{
  GuppiRootGroupView *rgv = GUPPI_ROOT_GROUP_VIEW (view);
  xmlNodePtr rgv_node;
  gchar buf[64];

  rgv_node = xmlNewNode (doc->ns, "RootGroupView_size");
  
  g_snprintf (buf, 64, "%g", rgv->width);
  xmlNewProp (rgv_node, "width", buf);

  g_snprintf (buf, 64, "%g", rgv->height);
  xmlNewProp (rgv_node, "height", buf);

  g_snprintf (buf, 64, "%g", rgv->scale);
  xmlNewProp (rgv_node, "scale", buf);

  xmlAddChild (root_node, rgv_node);

  if (GUPPI_ELEMENT_VIEW_CLASS (parent_class)->xml_export)
    GUPPI_ELEMENT_VIEW_CLASS (parent_class)->xml_export (view, doc, root_node);
}

static gboolean
xml_import (GuppiElementView *view, GuppiXMLDocument *doc, xmlNodePtr node)
{
  GuppiRootGroupView *rgv = GUPPI_ROOT_GROUP_VIEW (view);
  gchar *s;

  if (!strcmp (node->name, "RootGroupView_size")) {
    s = xmlGetProp (node, "width");
    rgv->width = s ? atof (s) : DEFAULT_WIDTH;
    xmlFree (s);

    s = xmlGetProp (node, "height");
    rgv->height = s ? atof (s) : DEFAULT_HEIGHT;
    xmlFree (s);

    s = xmlGetProp (node, "scale");
    rgv->scale = s ? atof (s) : 1.0;
    xmlFree (s);
  }

  if (GUPPI_ELEMENT_VIEW_CLASS (parent_class)->xml_import)
    return GUPPI_ELEMENT_VIEW_CLASS (parent_class)->xml_import (view, doc, node);

  return TRUE;
}

static void
guppi_root_group_view_class_init (GuppiRootGroupViewClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementViewClass *view_class = GUPPI_ELEMENT_VIEW_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_GROUP_VIEW);

  object_class->destroy = guppi_root_group_view_destroy;
  object_class->finalize = guppi_root_group_view_finalize;

  view_class->canvas_item_type = GUPPI_TYPE_ROOT_GROUP_ITEM;
  view_class->changed_state = changed_state;
  view_class->view_init = view_init;

  view_class->xml_export = xml_export;
  view_class->xml_import = xml_import;
}

/***************************************************************************/

static void
guppi_root_group_view_init (GuppiRootGroupView *obj)
{
  GuppiGeometry *geom = guppi_element_view_geometry (GUPPI_ELEMENT_VIEW (obj));

  obj->width  = DEFAULT_WIDTH;
  obj->height = DEFAULT_HEIGHT;

  guppi_geometry_set_position (geom, 0, obj->width, 0, obj->height);
}

GtkType 
guppi_root_group_view_get_type (void)
{
  static GtkType guppi_root_group_view_type = 0;
  if (!guppi_root_group_view_type) {
    static const GtkTypeInfo guppi_root_group_view_info = {
      "GuppiRootGroupView",
      sizeof (GuppiRootGroupView),
      sizeof (GuppiRootGroupViewClass),
      (GtkClassInitFunc) guppi_root_group_view_class_init,
      (GtkObjectInitFunc) guppi_root_group_view_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_root_group_view_type =
      gtk_type_unique (GUPPI_TYPE_GROUP_VIEW, &guppi_root_group_view_info);
  }
  return guppi_root_group_view_type;
}

GuppiRootGroupView *
guppi_root_group_view_new (void)
{
  GuppiElementState *state;
  GuppiElementView *view;

  state = guppi_root_group_state_new ();
  view = guppi_element_state_make_view (state);
  guppi_unref (state);

  return GUPPI_ROOT_GROUP_VIEW (view);
}

GuppiCanvasItem *
guppi_root_group_view_make_canvas_item (GuppiRootGroupView *root_view, GnomeCanvas *canvas)
{
  GuppiCanvasItem *item;

  g_return_val_if_fail (root_view && GUPPI_IS_ROOT_GROUP_VIEW (root_view), NULL);
  g_return_val_if_fail (canvas && GNOME_IS_CANVAS (canvas), NULL);

  item = guppi_element_view_make_canvas_item (GUPPI_ELEMENT_VIEW (root_view),
					      canvas,
					      gnome_canvas_root (canvas));

  return item;
}

GnomeCanvas *
guppi_root_group_view_make_canvas (GuppiRootGroupView *root_view, GuppiCanvasItem **root_item)
{
  GnomeCanvas *canvas;
  GuppiCanvasItem *item;

  g_return_val_if_fail (root_view && GUPPI_IS_ROOT_GROUP_VIEW (root_view), NULL);

  gtk_widget_push_visual (gdk_rgb_get_visual ());
  gtk_widget_push_colormap (gdk_rgb_get_cmap ());
  canvas = GNOME_CANVAS (gnome_canvas_new_aa ());
  gtk_widget_pop_colormap ();
  gtk_widget_pop_visual ();

  item = guppi_root_group_view_make_canvas_item (root_view, canvas);
  if (root_item)
    *root_item = item;

  return canvas;
}

/***************************************************************************/

void
guppi_root_group_view_set_size (GuppiRootGroupView *view, double w, double h)
{
  g_return_if_fail (view != NULL && GUPPI_IS_ROOT_GROUP_VIEW (view));
  g_return_if_fail (w >= 0 && h >= 0);

  if (view->width != w || view->height != h) {

    GuppiGeometry *geom = guppi_element_view_geometry (GUPPI_ELEMENT_VIEW (view));

    view->width = w;
    view->height = h;

    guppi_element_view_changed_size (GUPPI_ELEMENT_VIEW (view), view->width, view->height);
    guppi_geometry_set_position (geom, 0, view->width, 0, view->height);
  }
}

double
guppi_root_group_view_width (GuppiRootGroupView *view)
{
  g_return_val_if_fail (view != NULL && GUPPI_IS_ROOT_GROUP_VIEW (view), 0);
  return view->width;
}

double
guppi_root_group_view_height (GuppiRootGroupView *view)
{
  g_return_val_if_fail (view != NULL && GUPPI_IS_ROOT_GROUP_VIEW (view), 0);
  return view->height;
}

/* $Id$ */
