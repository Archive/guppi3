/* $Id$ */

/*
 * guppi-marker.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_MARKER_H
#define _INC_GUPPI_MARKER_H

/* #include <gnome.h> */
#include <libgnomeprint/gnome-print.h>
#include  "guppi-element-print.h"
#include "guppi-alpha-template.h"
#include "guppi-pixbuf.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS 

typedef struct _GuppiMarkerInfo GuppiMarkerInfo;

typedef enum {
  GUPPI_MARKER_NONE,
  GUPPI_MARKER_CIRCLE,
  GUPPI_MARKER_DIAMOND,
  GUPPI_MARKER_CROSS,
  GUPPI_MARKER_X,
  GUPPI_MARKER_SQUARE,
  GUPPI_MARKER_AST,
  GUPPI_MARKER_TRIANGLE,
  GUPPI_MARKER_BAR,
  GUPPI_MARKER_HALF_BAR,
  GUPPI_MARKER_FILLED_CIRCLE,
  GUPPI_MARKER_FILLED_SQUARE,
  GUPPI_MARKER_FILLED_DIAMOND,
  GUPPI_MARKER_FILLED_TRIANGLE,
  GUPPI_MARKER_LAST,
  GUPPI_MARKER_UNKNOWN
} GuppiMarker;

/* This information can be used to automate the construction of
   configuration dialogs, etc. */
struct _GuppiMarkerInfo {
  GuppiMarker marker;

  const gchar *code;		/* a "canonical" name, independent of i18n */
  const gchar *name;

  const gchar *size1_desc;
  gdouble size1_min, size1_max, size1_default;

  const gchar *size2_desc;
  gdouble size2_min, size2_max, size2_default;
};

const GuppiMarkerInfo *guppi_marker_info (GuppiMarker);
GuppiMarker guppi_str2marker (const gchar *);

GuppiAlphaTemplate *guppi_marker_alpha_template (GuppiMarker m,
						 double sz1, double sz2,
						 double scale_factor);
GuppiPixbuf *guppi_marker_pixbuf (GuppiMarker m,
				  double sz1, double sz2,
				  double scale_factor);

void guppi_marker_print (GuppiMarker m, double sz1, double sz2,
			 GuppiElementPrint *, double x, double y);



END_GUPPI_DECLS

#endif /* _INC_GUPPI_MARKER_H */

/* $Id$ */
