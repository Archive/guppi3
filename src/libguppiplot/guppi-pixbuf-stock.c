/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-pixbuf-stock.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include "guppi-pixbuf-stock.h"

#include <math.h>
#include <string.h>
#include <guppi-memory.h>
#include <guppi-hash.h>

/* Assumes no alpha */
#define PIXBUF_SET(pixels, stride, i, j, r, g, b) \
{ guchar *foofoofoo = (pixels)+(j)*(stride)+3*(i); foofoofoo[0] = (r); foofoofoo[1] = (g); foofoofoo[2] = (b); }

/*
 * A framework for cacheing pixbufs
 */

typedef struct _GuppiPixbufStockItem GuppiPixbufStockItem;
struct _GuppiPixbufStockItem {
  guint typeid;
  double p1, p2, p3, p4;
  gint n1, n2, n3;
  guint b1 : 1;
  guint b2 : 1;
  guint b3 : 1;
  GuppiPixbuf *pixbuf;
  GuppiPixbufStockItem *next;
};

static GuppiPixbufStockItem *
guppi_pixbuf_stock_item_new (guint typeid)
{
  GuppiPixbufStockItem *item = guppi_new0 (GuppiPixbufStockItem, 1);
  item->typeid = typeid;
  return item;
}

static void
guppi_pixbuf_stock_item_free (GuppiPixbufStockItem *item)
{
  guppi_free (item);
}

static gboolean
guppi_pixbuf_stock_item_eq (GuppiPixbufStockItem *a, GuppiPixbufStockItem *b)
{
  g_return_val_if_fail (a != NULL && b != NULL, FALSE);

  return a->typeid == b->typeid
    && a->p1 == b->p1
    && a->p2 == b->p2
    && a->p3 == b->p3
    && a->p4 == b->p4
    && a->n1 == b->n1
    && a->n2 == b->n2
    && a->n3 == b->n3
    && a->b1 == b->b1
    && a->b2 == b->b2
    && a->b3 == b->b3;
}

static guint
guppi_pixbuf_stock_item_get_hash (GuppiPixbufStockItem *item)
{
  GuppiHash *hash;
  guint x;

  g_return_val_if_fail (item != NULL, 0);

  hash = guppi_hash_new ();

  guppi_hash_raw (hash, item, sizeof (GuppiPixbufStockItem));

  x = guppi_hash_to_uint (hash);
  guppi_hash_free (hash);
  return x;
}

static GuppiPixbuf *
guppi_pixbuf_stock_item_lookup (GuppiPixbufStockItem *item)
{
  static GHashTable *cache = NULL;
  guint hash;
  GuppiPixbufStockItem *lookup;

  if (cache == NULL) {
    cache = g_hash_table_new (NULL, NULL);
  }

  hash = guppi_pixbuf_stock_item_get_hash (item);
  lookup = g_hash_table_lookup (cache, GUINT_TO_POINTER (hash));

  while (lookup != NULL) {
    if (guppi_pixbuf_stock_item_eq (lookup, item)) {
      guppi_pixbuf_stock_item_free (item);
      guppi_pixbuf_ref (lookup->pixbuf);
      return lookup->pixbuf;
    }

    /* If we walk to the end, append our new item. */
    if (lookup->next == NULL) {
      lookup->next = item;
      return NULL;
    }
    
    lookup = lookup->next;
  }

  /* Insert our new item into the cache. */
  g_hash_table_insert (cache, GUINT_TO_POINTER (hash), item);

  return NULL;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

GuppiPixbuf *
guppi_pixbuf_stock_new_circle (double r, double w)
{
  GuppiPixbufStockItem *item;
  GuppiPixbuf *gp;
  GdkPixbuf *pixbuf;
  guchar *pixels, *run;
  gint rowstride;
  const gint slice = 3;
  const gint slice2 = slice * slice;
  const double side = 1 / (double)slice;
  gint span = (gint) ceil (2 * r + 1);
  double cent = span / 2.0;
  double rr = r*r;
  double rw;
  double ip, jp, rr1, D_est, D;
  gint i, j, ii, jj, count1, count2;
  guint val1, val2;
  
  g_return_val_if_fail (r > 0, NULL);

  if (w > r)
    w = r;

  if (w < 1e-6)
    w = 0;

  item = guppi_pixbuf_stock_item_new (0x9a931e83);
  item->p1 = r;
  item->p2 = w;

  gp = guppi_pixbuf_stock_item_lookup (item);
  if (gp) 
    return gp;

  rw = (r - w) * (r - w);
  rr1 = (r + M_SQRT2) * (r + M_SQRT2);

  pixbuf = gdk_pixbuf_new (GDK_COLORSPACE_RGB, FALSE, 8, span, span);
  pixels = gdk_pixbuf_get_pixels (pixbuf);
  rowstride = gdk_pixbuf_get_rowstride (pixbuf);

  /* Zero out memory */
  run = pixels;
  for (j = 0; j < span; ++j) {
    memset (run, 0, 3 * span);
    run += rowstride;
  }

  for (i = 0; i <= span/2; ++i) {
    for (j = i; j <= span/2; ++j) {
      count1 = 0;
      count2 = 0;
      D_est = (i - cent) * (i - cent) + (j - cent) * (j - cent);

      if (D_est < rr1) {

	for (ii = 0; ii < slice; ++ii) {
	  for (jj = 0; jj < slice; ++jj) {
	    ip = i + (ii + 0.5) * side - cent;
	    jp = j + (jj + 0.5) * side - cent;
	    D = ip * ip + jp * jp;

	    if (D <= rr) {
	      if (D <= rw)
		++count1;
	      else
		++count2;
	    }
	  }
	}
      }

      val1 = count1 ? 0xff * count1 / slice2 : 0;
      val2 = count2 ? 0xff * count2 / slice2 : 0;

      /* Exploit the eightfold symmetry of the situation. */
      if (val1 > 0 || val2 > 0) {
	PIXBUF_SET (pixels, rowstride,          i,          j, val1, 0, val2);
	PIXBUF_SET (pixels, rowstride,          j,          i, val1, 0, val2);
	PIXBUF_SET (pixels, rowstride,          i, span-1 - j, val1, 0, val2);
	PIXBUF_SET (pixels, rowstride,          j, span-1 - i, val1, 0, val2);
	PIXBUF_SET (pixels, rowstride, span-1 - i,          j, val1, 0, val2);
	PIXBUF_SET (pixels, rowstride, span-1 - j,          i, val1, 0, val2);
	PIXBUF_SET (pixels, rowstride, span-1 - i, span-1 - j, val1, 0, val2);
	PIXBUF_SET (pixels, rowstride, span-1 - j, span-1 - i, val1, 0, val2);
      }
    }
  }

  gp = guppi_pixbuf_new (pixbuf);
  gdk_pixbuf_unref (pixbuf);

  gp->x_base_point = span / 2;
  gp->y_base_point = span / 2;
  gp->color_mappable = TRUE;

  item->pixbuf = gp;
  guppi_pixbuf_ref (gp);

  return gp;
}

GuppiPixbuf *
guppi_pixbuf_stock_new_square (double r, double w, double th)
{
  GuppiPixbufStockItem *item;
  GuppiPixbuf *gp;
  GdkPixbuf *pixbuf;
  guchar *pixels, *run;
  gint rowstride;
  const gint slice = 3;
  const gint slice2 = slice * slice;
  const double side = 1 / (double)slice;
  gint span = (gint) ceil (2*r+1);
  double cent = span / 2.0;
  double len, sth, cth, ip, jp, tx, ty;
  gint i, j, ii, jj, count1, count2;

  g_return_val_if_fail (r > 0, NULL);
  
  if (w > r) 
    w = r;
  
  if (w < 1e-6)
    w = 0;

  item = guppi_pixbuf_stock_item_new (0x31412721);
  item->p1 = r;
  item->p2 = w;
  item->p3 = th;

  gp = guppi_pixbuf_stock_item_lookup (item);
  if (gp)
    return gp;

  sth = sin (-th);
  cth = cos (-th);

  pixbuf = gdk_pixbuf_new (GDK_COLORSPACE_RGB, FALSE, 8, span, span);
  pixels = gdk_pixbuf_get_pixels (pixbuf);
  rowstride = gdk_pixbuf_get_rowstride (pixbuf);

  len = r / sqrt (2);

  for (j = 0; j < span; ++j) {
    run = pixels;
    for (i = 0; i < span; ++i) {
      count1 = 0;
      count2 = 0;
      for (ii = 0; ii < slice; ++ii) {
	for (jj = 0; jj < slice; ++jj) {
	  ip = i + (ii + 0.5) * side - cent;
	  jp = j + (jj + 0.5) * side - cent;
	  tx = fabs ( cth * ip + sth * jp);
	  ty = fabs (-sth * ip + cth * jp);
  
	  if (tx <= len && ty <= len) {
	    if (w > 0 && (tx > len-w || ty > len-w))
	      ++count2;
	    else
	      ++count1;
	  }
	}
      }

      run[0] = count1 ? 0xff * count1 / slice2 : 0;
      run[1] = 0;
      run[2] = count2 ? 0xff * count2 / slice2 : 0;

      run += 3;
    }
    pixels += rowstride;
  }

  gp = guppi_pixbuf_new (pixbuf);
  gdk_pixbuf_unref (pixbuf);

  gp->x_base_point = span / 2;
  gp->y_base_point = span / 2;
  gp->color_mappable = TRUE;

  item->pixbuf = gp;
  guppi_pixbuf_ref (gp);

  return gp;
}

static gint
half_plane (double x, double y,
	    double x0, double y0, double x1, double y1)
{
  const double epsilon = 1e-8;
  double t = - (x - x0) * (y1 - y0) + (y - y0) * (x1 - x0);
  
  if (t > epsilon)
    return +1;
  if (t < -epsilon)
    return -1;
  return 0;
}

static double
distsq_to_segment (double x, double y,
		   double x0, double y0, double x1, double y1)
{
  double dx = x1 - x0;
  double dy = y1 - y0;
  double t;
  double ax, ay;

  t = (dx * (x - x0) + dy * (y - y0)) / (dx * dx + dy * dy);
  t = CLAMP (t, 0, 1);

  ax = x0 + dx * t - x;
  ay = y0 + dy * t - y;

  return ax * ax + ay * ay;
}

GuppiPixbuf *
guppi_pixbuf_stock_new_triangle (double r, double w, double th)
{
  GuppiPixbufStockItem *item;
  GuppiPixbuf *gp;
  GdkPixbuf *pixbuf;
  guchar *pixels, *run;
  gint rowstride;
  const gint slice = 3;
  const gint slice2 = slice * slice;
  const double side = 1/(double)slice;
  gint span = (gint) ceil (2*r + 1);
  double cent = span / 2.0;
  gint i, j, ii, jj, count1, count2;
  double ip, jp, rr, ww;
  double x0, y0, x1, y1, x2, y2;

  g_return_val_if_fail (r > 0, NULL);

  if (w > r)
    w = r;

  if (w < 1e-6)
    w = 0;

  item = guppi_pixbuf_stock_item_new (0xabcd8273);
  item->p1 = r;
  item->p2 = w;
  item->p3 = th;

  gp = guppi_pixbuf_stock_item_lookup (item);
  if (gp) 
    return gp;
  
  rr = r * r;
  ww = w * w;

  x0 =  r * cos (M_PI/2 + th);
  y0 = -r * sin (M_PI/2 + th);

  x1 =  r * cos (M_PI/2 + 2*M_PI/3 + th);
  y1 = -r * sin (M_PI/2 + 2*M_PI/3 + th);
  
  x2 =  r * cos (M_PI/2 + 4*M_PI/3 + th);
  y2 = -r * sin (M_PI/2 + 4*M_PI/3 + th);

  pixbuf = gdk_pixbuf_new (GDK_COLORSPACE_RGB, FALSE, 8, span, span);
  pixels = gdk_pixbuf_get_pixels (pixbuf);
  rowstride = gdk_pixbuf_get_rowstride (pixbuf);

  for (j = 0; j < span; ++j) {
    run = pixels;
    for (i = 0; i < span; ++i) {

      count1 = 0;
      count2 = 0;

      for (ii = 0; ii < slice; ++ii) {
	for (jj = 0; jj < slice; ++jj) {
	  ip = i + (ii + 0.5) * side - cent;
	  jp = j + (jj + 0.5) * side - cent;

	  if (half_plane (ip, jp, x0, y0, x1, y1) <= 0
	      && half_plane (ip, jp, x1, y1, x2, y2) <= 0
	      && half_plane (ip, jp, x2, y2, x0, y0) <= 0) {

	    if (ww > 0 && (distsq_to_segment (ip, jp, x0, y0, x1, y1) < ww
			   || distsq_to_segment (ip, jp, x1, y1, x2, y2) < ww
			   || distsq_to_segment (ip, jp, x0, y0, x2, y2) < ww))
	      ++count2;
	    else
	      ++count1;
	  }
	}
      }

      run[0] = count1 > 0 ? count1 * 0xff / slice2 : 0;
      run[1] = 0;
      run[2] = count2 > 0 ? count2 * 0xff / slice2 : 0;

      run += 3;
    }
    pixels += rowstride;
  }

  gp = guppi_pixbuf_new (pixbuf);
  gdk_pixbuf_unref (pixbuf);

  gp->x_base_point = span / 2;
  gp->y_base_point = span / 2;
  gp->color_mappable = TRUE;

  item->pixbuf = gp;
  guppi_pixbuf_ref (gp);

  return gp;
}

GuppiPixbuf *
guppi_pixbuf_stock_new_starburst (double radius, double width, double edge, gint N, double theta)
{
  GuppiPixbufStockItem *item;
  GuppiPixbuf *gp;
  GdkPixbuf *pixbuf;
  guchar *pixels, *run;
  gint rowstride;
  double *x, *y;
  const gint slice = 3;
  const gint slice2 = slice * slice;
  const double side = 1/(double)slice;
  gint span = (gint)ceil (2 * radius + 1);
  double cent = span / 2.0;
  gint i, j, k, ii, jj, count1, count2;
  double ip, jp, rr, D, D0, D1;
  gboolean hit_inner, hit_edge;

  g_return_val_if_fail (radius > 0, NULL);
  g_return_val_if_fail (width > 0, NULL);
  g_return_val_if_fail (N > 0, NULL);

  if (edge < 1e-6)
    edge = 0;

  item = guppi_pixbuf_stock_item_new (0x83819eee);
  item->p1 = radius;
  item->p2 = width;
  item->p3 = edge;
  item->p4 = theta;
  item->n1 = N;

  gp = guppi_pixbuf_stock_item_lookup (item);
  if (gp)
    return gp;

  rr = radius * radius;
  D1 = width * width / 4;
  D0 = (width - edge) * (width - edge) / 4;

  x = guppi_new (double, N);
  y = guppi_new (double, N);

  for (k = 0; k < N; ++k) {
    x[k] = (radius - width) * cos (2*M_PI*k/(double)N+theta);
    y[k] = (radius - width) * sin (2*M_PI*k/(double)N+theta);
    g_print ("%d %g %g\n", k, x[k], y[k]);
  }

  pixbuf = gdk_pixbuf_new (GDK_COLORSPACE_RGB, FALSE, 8, span, span);
  pixels = gdk_pixbuf_get_pixels (pixbuf);
  rowstride = gdk_pixbuf_get_rowstride (pixbuf);

  for (j = 0; j < span; ++j) {
    run = pixels;
    for (i = 0; i < span; ++i) {

      count1 = 0;
      count2 = 0;

      for (ii = 0; ii < slice; ++ii) {
	for (jj = 0; jj < slice; ++jj) {
	  ip = i + (ii + 0.5) * side - cent;
	  jp = j + (jj + 0.5) * side - cent;

	  if (ip * ip + jp * jp <= rr) {
	    hit_inner = hit_edge = FALSE;
	    for (k = 0; k < N && !hit_inner; ++k) {
	      D = distsq_to_segment (ip, jp, 0, 0, x[k], y[k]);
	      if (D <= D0)
		hit_inner = TRUE;
	      else if (D <= D1)
		hit_edge = TRUE;
	    }
	    
	    if (hit_inner)
	      ++count1;
	    else if (hit_edge)
	      ++count2;
	  }
	}
      }

      run[0] = count1 > 0 ? count1 * 0xff / slice2 : 0;
      run[1] = 0;
      run[2] = count2 > 0 ? count2 * 0xff / slice2 : 0;

      run += 3;
    }
    pixels += rowstride;
  }

  gp = guppi_pixbuf_new (pixbuf);
  gdk_pixbuf_unref (pixbuf);

  gp->x_base_point = span / 2;
  gp->y_base_point = span / 2;
  gp->color_mappable = TRUE;

  item->pixbuf = gp;
  guppi_pixbuf_ref (gp);

  guppi_free (x);
  guppi_free (y);

  return gp;
}
