#!/usr/bin/perl -w

print <<EOF;
/* This code is automatically generated.  Edit at your own risk! */

#ifndef __GUPPI_LAYOUT_RULE_PREDEF_H__
#define __GUPPI_LAYOUT_RULE_PREDEF_H__

#include "guppi-layout-rule.h"
#include "guppi-defs.h"

BEGIN_GUPPI_DECLS

EOF

while (defined (my $line = <>)) {

    chomp $line;
    $line =~ s/\#.*//g;
    next unless $line =~ /^([a-zA-Z0-9_]+):\s*(.*)/;

    my $name = $1;
    my @rest = split (/\s*,\s*/, $2);

    print "GuppiLayoutRule *guppi_layout_rule_new_$name (";
    my $first = 1;
    foreach my $var (@rest) {


	print ", " unless $first;
	$first = 0;

	if ($var =~ /geom/) {
	    print "GuppiGeometry *$var";
	} else {
	    print "double $var";
	}
    }
    print ");\n";
}

print <<EOF;

END_GUPPI_DECLS

#endif /* __GUPPI_LAYOUT_RULE_PREDEF_H__ */

/* This code is automatically generated.  Edit at your own risk! */
EOF
