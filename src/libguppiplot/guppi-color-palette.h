/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-color-palette.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_COLOR_PALETTE_H
#define _INC_GUPPI_COLOR_PALETTE_H

#include <gnome.h>
#include  "guppi-defs.h"
#include  "guppi-xml.h"
#include  "guppi-attribute-flavor.h"

BEGIN_GUPPI_DECLS;

typedef struct _GuppiColorPalette GuppiColorPalette;
typedef struct _GuppiColorPaletteClass GuppiColorPaletteClass;

struct _GuppiColorPalette {
  GtkObject parent;

  gchar *meta;
  gint N;
  guint32 *nodes;
  gint offset, intensity, alpha;
  gboolean flip, own_nodes;
};

struct _GuppiColorPaletteClass {
  GtkObjectClass parent_class;
  
  void (*changed) (GuppiColorPalette *);
};

#define GUPPI_TYPE_COLOR_PALETTE (guppi_color_palette_get_type ())
#define GUPPI_COLOR_PALETTE(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_COLOR_PALETTE,GuppiColorPalette))
#define GUPPI_COLOR_PALETTE0(obj) ((obj) ? (GUPPI_COLOR_PALETTE(obj)) : NULL)
#define GUPPI_COLOR_PALETTE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_COLOR_PALETTE,GuppiColorPaletteClass))
#define GUPPI_IS_COLOR_PALETTE(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_COLOR_PALETTE))
#define GUPPI_IS_COLOR_PALETTE0(obj) (((obj) == NULL) || (GUPPI_IS_COLOR_PALETTE(obj)))
#define GUPPI_IS_COLOR_PALETTE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_COLOR_PALETTE))

GtkType guppi_color_palette_get_type (void);

GuppiColorPalette *guppi_color_palette_new  (void);
GuppiColorPalette *guppi_color_palette_copy (GuppiColorPalette *);

gint     guppi_color_palette_size            (GuppiColorPalette *pal);
guint32  guppi_color_palette_get             (GuppiColorPalette *pal, gint i);
guint32  guppi_color_palette_interpolate     (GuppiColorPalette *pal, double t);

void     guppi_color_palette_set             (GuppiColorPalette *pal, gint i, guint32 c);

gint     guppi_color_palette_get_offset      (GuppiColorPalette *);
void     guppi_color_palette_set_offset      (GuppiColorPalette *, gint);

gint     guppi_color_palette_get_alpha       (GuppiColorPalette *);
void     guppi_color_palette_set_alpha       (GuppiColorPalette *, gint);

gint     guppi_color_palette_get_intensity   (GuppiColorPalette *);
void     guppi_color_palette_set_intensity   (GuppiColorPalette *, gint);

gboolean guppi_color_palette_get_flipped     (GuppiColorPalette *);
void     guppi_color_palette_set_flipped     (GuppiColorPalette *, gboolean);

void     guppi_color_palette_set_stock       (GuppiColorPalette *);
void     guppi_color_palette_set_alien_stock (GuppiColorPalette *);
void     guppi_color_palette_set_transition  (GuppiColorPalette *, guint32 c1, guint32 c2);
void     guppi_color_palette_set_fade        (GuppiColorPalette *, guint32 c);
void     guppi_color_palette_set_fire        (GuppiColorPalette *);
void     guppi_color_palette_set_ice         (GuppiColorPalette *);
void     guppi_color_palette_set_thermal     (GuppiColorPalette *);
void     guppi_color_palette_set_spectrum    (GuppiColorPalette *);
void     guppi_color_palette_set_monochrome  (GuppiColorPalette *, guint32 c);

void     guppi_color_palette_set_custom      (GuppiColorPalette *, gint N, guint32 *color);
void     guppi_color_palette_set_vcustom     (GuppiColorPalette *, gint N, guint32 first_color, ...);


xmlNodePtr         guppi_color_palette_export_xml (GuppiColorPalette *, GuppiXMLDocument *);
GuppiColorPalette *guppi_color_palette_import_xml (GuppiXMLDocument *, xmlNodePtr);



/* A GuppiAttributeBag flavor */

#define GUPPI_ATTR_COLOR_PALETTE (guppi_attribute_flavor_color_palette ())
GuppiAttributeFlavor guppi_attribute_flavor_color_palette (void);

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_COLOR_PALETTE_H */

/* $Id$ */
