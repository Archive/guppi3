/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-text-tokens.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_TEXT_TOKENS_H
#define _INC_GUPPI_TEXT_TOKENS_H

#include <gnome.h>
#include <libgnomeprint/gnome-font.h>
#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS;

enum {
  TEXT_TOKEN_ERROR=0,
  TEXT_TOKEN_WORD,
  TEXT_TOKEN_SPACE,
  TEXT_TOKEN_NOBREAK,
  TEXT_TOKEN_PUSH,
  TEXT_TOKEN_POP,
  TEXT_TOKEN_SET_FONT,
  TEXT_TOKEN_RESIZE_FONT,
  TEXT_TOKEN_RAISE_LOWER,
  TEXT_TOKEN_HARD_BREAK,
  TEXT_TOKEN_SOFT_BREAK,
  TEXT_TOKEN_JUSTIFY,
  TEXT_TOKEN_LAST
};

enum {
  RESIZE_FONT_ABSOLUTE=0,
  RESIZE_FONT_ADDITIVE,
  RESIZE_FONT_SCALED,
  RESIZE_FONT_LAST
};

typedef struct _GuppiTextToken GuppiTextToken;
struct _GuppiTextToken {
  gpointer opaque_internals;
};

#define GUPPI_TEXT_TOKEN(x) ((GuppiTextToken *)(x))

void guppi_text_token_free (GuppiTextToken *);

GuppiTextToken *guppi_text_token_new_word (const gchar *str);
GuppiTextToken *guppi_text_token_new_space (double space_size);
GuppiTextToken *guppi_text_token_new_nobreak (void);
GuppiTextToken *guppi_text_token_new_push (void);
GuppiTextToken *guppi_text_token_new_pop (void);
GuppiTextToken *guppi_text_token_new_set_font (GnomeFont *);
GuppiTextToken *guppi_text_token_new_resize_font (gint resize_type, double);
GuppiTextToken *guppi_text_token_new_raise_lower (double dist);
GuppiTextToken *guppi_text_token_new_justify (GtkJustification);

GuppiTextToken *guppi_text_token_new_hard_break (void);
GuppiTextToken *guppi_text_token_new_soft_break (double line_size,
						 double line_ascender,
						 double line_descender,
						 gboolean from_hard);

gint guppi_text_token_type (GuppiTextToken *);

gboolean guppi_text_token_is_break (GuppiTextToken *);

/* For TEXT_TOKEN_WORD */
const gchar *guppi_text_token_word (GuppiTextToken *);

/* For TEXT_TOKEN_SPACE */
double guppi_text_token_space_size (GuppiTextToken *);
gboolean guppi_text_token_space_is_breakable (GuppiTextToken *);

/* For TEXT_TOKEN_*_FONT */
GnomeFont *guppi_text_token_evolve_font (GuppiTextToken *, GnomeFont *prior);

/* For TEXT_TOKEN_RAISE_LOWER */
double guppi_text_token_raise_lower_distance (GuppiTextToken *);

GtkJustification guppi_text_token_justification (GuppiTextToken *);

/* For TEXT_TOKEN_SOFT_BREAK */
void guppi_text_token_soft_break_line_dimensions (GuppiTextToken *,
						  double *line_length,
						  double *line_ascender,
						  double *line_descender);
gboolean guppi_text_token_soft_break_from_hard (GuppiTextToken *);



END_GUPPI_DECLS;


#endif /* _INC_GUPPI_TEXT-TOKENS_H */

/* $Id$ */

