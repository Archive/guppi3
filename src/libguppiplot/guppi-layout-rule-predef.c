/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-layout-rule-predef.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include "guppi-layout-rule-predef.h"

#include <guppi-i18n.h>

static void
merge_cb (GuppiLayoutConstraint *cnt, gpointer closure)
{
  GuppiLayoutRule *dest = closure;
  guppi_layout_rule_add_constraint (dest, cnt);
}

static void
rule_merge (GuppiLayoutRule *dest, GuppiLayoutRule *src)
{
  guppi_layout_rule_foreach (src, merge_cb, dest);
  guppi_layout_rule_unref (src);
}

GuppiLayoutRule *
guppi_layout_rule_new_flush_left (GuppiGeometry *geom, double margin)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom), NULL);
  g_return_val_if_fail (margin >= 0, NULL);

  rule = guppi_layout_rule_new (_("Flush Left"));

  guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),
				     GLC_LEFT, -1.0, geom,
				     GLC_REGION_LEFT, 1.0,
				     GLC_FIXED, margin,
				     GLC_LAST);

  guppi_layout_rule_lock (rule);

  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_flush_right (GuppiGeometry *geom, double margin)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom), NULL);
  g_return_val_if_fail (margin >= 0, NULL);

  rule = guppi_layout_rule_new (_("Flush Right"));

  guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),
				     GLC_RIGHT, -1.0, geom,
				     GLC_REGION_RIGHT, 1.0,
				     GLC_FIXED, -margin,
				     GLC_LAST);

  guppi_layout_rule_lock (rule);

  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_flush_top (GuppiGeometry *geom, double margin)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom), NULL);
  g_return_val_if_fail (margin >= 0, NULL);

  rule = guppi_layout_rule_new (_("Flush Top"));

  guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),
				     GLC_TOP, -1.0, geom,
				     GLC_REGION_TOP, 1.0,
				     GLC_FIXED, -margin,
				     GLC_LAST);

  guppi_layout_rule_lock (rule);

  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_flush_bottom (GuppiGeometry *geom, double margin)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom), NULL);
  g_return_val_if_fail (margin >= 0, NULL);

  rule = guppi_layout_rule_new (_("Flush Bottom"));

  guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),
				     GLC_BOTTOM, -1.0, geom,
				     GLC_REGION_BOTTOM, 1.0,
				     GLC_FIXED, margin,
				     GLC_LAST);

  guppi_layout_rule_lock (rule);

  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_center_horizontally (GuppiGeometry *geom)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom), NULL);

  rule = guppi_layout_rule_new (_("Center Horizontally"));

  guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),
				     GLC_HORIZONTAL_CENTER, -1.0, geom,
				     GLC_REGION_HORIZONTAL_CENTER, 1.0,
				     GLC_LAST);

  guppi_layout_rule_lock (rule);

  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_center_vertically (GuppiGeometry *geom)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom), NULL);

  rule = guppi_layout_rule_new (_("Center Vertically"));

  guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),
				     GLC_VERTICAL_CENTER, -1.0, geom,
				     GLC_REGION_VERTICAL_CENTER, 1.0,
				     GLC_LAST);

  guppi_layout_rule_lock (rule);

  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_center (GuppiGeometry *geom)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom), NULL);

  rule = guppi_layout_rule_new (_("Center"));

  rule_merge (rule, guppi_layout_rule_new_center_horizontally (geom));
  rule_merge (rule, guppi_layout_rule_new_center_vertically (geom));

  guppi_layout_rule_lock (rule);
  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_fill_horizontally (GuppiGeometry *geom, double l_m, double r_m)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom), NULL);
  g_return_val_if_fail (l_m >= 0, NULL);
  g_return_val_if_fail (r_m >= 0, NULL);

  rule = guppi_layout_rule_new (_("Fill Horizontally"));

  rule_merge (rule, guppi_layout_rule_new_flush_left (geom, l_m));
  rule_merge (rule, guppi_layout_rule_new_flush_right (geom, r_m));
  
  guppi_layout_rule_lock (rule);
  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_fill_vertically (GuppiGeometry *geom, double t_m, double b_m)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom), NULL);
  g_return_val_if_fail (t_m >= 0, NULL);
  g_return_val_if_fail (b_m >= 0, NULL);

  rule = guppi_layout_rule_new (_("Fill Vertically"));

  rule_merge (rule, guppi_layout_rule_new_flush_top (geom, t_m));
  rule_merge (rule, guppi_layout_rule_new_flush_bottom (geom, b_m));
  
  guppi_layout_rule_lock (rule);
  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_fill (GuppiGeometry *geom, double l_m, double r_m, double t_m, double b_m)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom), NULL);
  g_return_val_if_fail (l_m >= 0, NULL);
  g_return_val_if_fail (r_m >= 0, NULL);
  g_return_val_if_fail (t_m >= 0, NULL);
  g_return_val_if_fail (b_m >= 0, NULL);

  rule = guppi_layout_rule_new (_("Fill"));

  rule_merge (rule, guppi_layout_rule_new_fill_horizontally (geom, l_m, r_m));
  rule_merge (rule, guppi_layout_rule_new_fill_vertically (geom, t_m, b_m));

  guppi_layout_rule_lock (rule);
  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_same_left (GuppiGeometry *geom1, GuppiGeometry *geom2)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom1), NULL);
  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom2), NULL);

  rule = guppi_layout_rule_new (_("Same Left"));

  guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),
				     GLC_LEFT, -1.0, geom1,
				     GLC_LEFT,  1.0, geom2,
				     GLC_LAST);

  guppi_layout_rule_lock (rule);
  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_same_right (GuppiGeometry *geom1, GuppiGeometry *geom2)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom1), NULL);
  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom2), NULL);

  rule = guppi_layout_rule_new (_("Same Right"));

  guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),
				     GLC_RIGHT, -1.0, geom1,
				     GLC_RIGHT,  1.0, geom2,
				     GLC_LAST);

  guppi_layout_rule_lock (rule);
  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_same_top (GuppiGeometry *geom1, GuppiGeometry *geom2)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom1), NULL);
  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom2), NULL);

  rule = guppi_layout_rule_new (_("Same Top"));

  guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),
				     GLC_TOP, -1.0, geom1,
				     GLC_TOP,  1.0, geom2,
				     GLC_LAST);

  guppi_layout_rule_lock (rule);
  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_same_bottom (GuppiGeometry *geom1, GuppiGeometry *geom2)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom1), NULL);
  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom2), NULL);

  rule = guppi_layout_rule_new (_("Same Bottom"));

  guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),
				     GLC_BOTTOM, -1.0, geom1,
				     GLC_BOTTOM,  1.0, geom2,
				     GLC_LAST);

  guppi_layout_rule_lock (rule);
  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_same_horizontal_center (GuppiGeometry *geom1, GuppiGeometry *geom2)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom1), NULL);
  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom2), NULL);

  rule = guppi_layout_rule_new (_("Same Horizontal Center"));

  guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),
				     GLC_LEFT,   1.0, geom1,
				     GLC_RIGHT,  1.0, geom1,
				     GLC_LEFT,  -1.0, geom2,
				     GLC_RIGHT, -1.0, geom2,
				     GLC_LAST);

  guppi_layout_rule_lock (rule);
  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_same_vertical_center (GuppiGeometry *geom1, GuppiGeometry *geom2)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom1), NULL);
  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom2), NULL);

  rule = guppi_layout_rule_new (_("Same Vertical Center"));

  guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),
				     GLC_TOP,     1.0, geom1,
				     GLC_BOTTOM,  1.0, geom1,
				     GLC_TOP,    -1.0, geom2,
				     GLC_BOTTOM, -1.0, geom2,
				     GLC_LAST);

  guppi_layout_rule_lock (rule);
  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_same_center (GuppiGeometry *geom1, GuppiGeometry *geom2)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom1), NULL);
  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom2), NULL);

  rule = guppi_layout_rule_new (_("Same Vertical Center"));

  rule_merge (rule, guppi_layout_rule_new_same_horizontal_center (geom1, geom2));
  rule_merge (rule, guppi_layout_rule_new_same_vertical_center (geom1, geom2));

  guppi_layout_rule_lock (rule);
  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_left_aligned (GuppiGeometry *t_geom, GuppiGeometry *b_geom, double gap)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (t_geom), NULL);
  g_return_val_if_fail (GUPPI_IS_GEOMETRY (b_geom), NULL);
  g_return_val_if_fail (gap >= 0, NULL);

  rule = guppi_layout_rule_new (_("Left-Aligned"));
  
  guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),
				     GLC_TOP,    -1.0, b_geom,
				     GLC_BOTTOM,  1.0, t_geom,
				     GLC_FIXED,   gap,
				     GLC_LAST);

  rule_merge (rule, guppi_layout_rule_new_same_left (t_geom, b_geom));

  guppi_layout_rule_lock (rule);
  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_right_aligned (GuppiGeometry *t_geom, GuppiGeometry *b_geom, double gap)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (t_geom), NULL);
  g_return_val_if_fail (GUPPI_IS_GEOMETRY (b_geom), NULL);
  g_return_val_if_fail (gap >= 0, NULL);

  rule = guppi_layout_rule_new (_("Right-Aligned"));
  
  guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),
				     GLC_TOP,    -1.0, b_geom,
				     GLC_BOTTOM,  1.0, t_geom,
				     GLC_FIXED,   gap,
				     GLC_LAST);

  rule_merge (rule, guppi_layout_rule_new_same_right (t_geom, b_geom));

  guppi_layout_rule_lock (rule);
  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_top_aligned (GuppiGeometry *l_geom, GuppiGeometry *r_geom, double gap)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (l_geom), NULL);
  g_return_val_if_fail (GUPPI_IS_GEOMETRY (r_geom), NULL);
  g_return_val_if_fail (gap >= 0, NULL);

  rule = guppi_layout_rule_new (_("Top-Aligned"));
  
  guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),
				     GLC_LEFT,  -1.0, r_geom,
				     GLC_RIGHT,  1.0, l_geom,
				     GLC_FIXED,  gap,
				     GLC_LAST);

  rule_merge (rule, guppi_layout_rule_new_same_top (r_geom, l_geom));


  guppi_layout_rule_lock (rule);
  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_bottom_aligned (GuppiGeometry *l_geom, GuppiGeometry *r_geom, double gap)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (l_geom), NULL);
  g_return_val_if_fail (GUPPI_IS_GEOMETRY (r_geom), NULL);
  g_return_val_if_fail (gap >= 0, NULL);

  rule = guppi_layout_rule_new (_("Bottom-Aligned"));
  
  guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),
				     GLC_LEFT,  -1.0, r_geom,
				     GLC_RIGHT,  1.0, l_geom,
				     GLC_FIXED,  gap,
				     GLC_LAST);

  rule_merge (rule, guppi_layout_rule_new_same_bottom (r_geom, l_geom));

  guppi_layout_rule_lock (rule);
  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_horizontally_adjacent (GuppiGeometry *l_geom, GuppiGeometry *r_geom, double gap)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (l_geom), NULL);
  g_return_val_if_fail (GUPPI_IS_GEOMETRY (r_geom), NULL);
  g_return_val_if_fail (gap >= 0, NULL);

  rule = guppi_layout_rule_new (_("Horizontally Adjacent"));

  guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),
				     GLC_LEFT,  -1.0, r_geom,
				     GLC_RIGHT,  1.0, l_geom,
				     GLC_FIXED,  gap,
				     GLC_LAST);

  guppi_layout_rule_lock (rule);
  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_horizontally_aligned (GuppiGeometry *l_geom, GuppiGeometry *r_geom, double gap)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (l_geom), NULL);
  g_return_val_if_fail (GUPPI_IS_GEOMETRY (r_geom), NULL);
  g_return_val_if_fail (gap >= 0, NULL);

  rule = guppi_layout_rule_new (_("Horizontally Aligned"));

  rule_merge (rule, guppi_layout_rule_new_horizontally_adjacent (l_geom, r_geom, gap));
  rule_merge (rule, guppi_layout_rule_new_same_top (l_geom, r_geom));
  rule_merge (rule, guppi_layout_rule_new_same_bottom (l_geom, r_geom));

  guppi_layout_rule_lock (rule);
  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_vertically_adjacent (GuppiGeometry *t_geom, GuppiGeometry *b_geom, double gap)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (t_geom), NULL);
  g_return_val_if_fail (GUPPI_IS_GEOMETRY (b_geom), NULL);
  g_return_val_if_fail (gap >= 0, NULL);

  rule = guppi_layout_rule_new (_("Vertically Adjacent"));

  guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),
				     GLC_TOP,   -1.0, b_geom,
				     GLC_BOTTOM, 1.0, t_geom,
				     GLC_FIXED,  -gap,
				     GLC_LAST);

  guppi_layout_rule_lock (rule);
  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_vertically_aligned (GuppiGeometry *t_geom, GuppiGeometry *b_geom, double gap)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (t_geom), NULL);
  g_return_val_if_fail (GUPPI_IS_GEOMETRY (b_geom), NULL);
  g_return_val_if_fail (gap >= 0, NULL);

  rule = guppi_layout_rule_new (_("Vertically Aligned"));

  rule_merge (rule, guppi_layout_rule_new_vertically_adjacent (t_geom, b_geom, gap));
  rule_merge (rule, guppi_layout_rule_new_same_left (t_geom, b_geom));
  rule_merge (rule, guppi_layout_rule_new_same_right (t_geom, b_geom));

  guppi_layout_rule_lock (rule);
  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_width_ratio (GuppiGeometry *geom1, GuppiGeometry *geom2, double ratio)
{
  GuppiLayoutRule *rule;
  
  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom1), NULL);
  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom2), NULL);
  g_return_val_if_fail (ratio > 0, NULL);

  rule = guppi_layout_rule_new (_("Width Ratio"));
  guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),
				     GLC_WIDTH, -1.0, geom1,
				     GLC_WIDTH, ratio, geom2,
				     GLC_LAST);

  guppi_layout_rule_lock (rule);
  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_height_ratio (GuppiGeometry *geom1, GuppiGeometry *geom2, double ratio)
{
  GuppiLayoutRule *rule;
  
  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom1), NULL);
  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom2), NULL);
  g_return_val_if_fail (ratio > 0, NULL);

  rule = guppi_layout_rule_new (_("Height Ratio"));
  guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),
				     GLC_HEIGHT, -1.0, geom1,
				     GLC_HEIGHT, ratio, geom2,
				     GLC_LAST);

  guppi_layout_rule_lock (rule);
  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_aspect_ratio (GuppiGeometry *geom, double ratio)
{
  GuppiLayoutRule *rule;
  
  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom), NULL);
  g_return_val_if_fail (ratio > 0, NULL);

  rule = guppi_layout_rule_new (_("Aspect Ratio"));
  guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),
				     GLC_WIDTH, -1.0, geom,
				     GLC_HEIGHT, ratio, geom,
				     GLC_LAST);

  guppi_layout_rule_lock (rule);
  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_same_width (GuppiGeometry *geom1, GuppiGeometry *geom2)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom1), NULL);
  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom2), NULL);
  
  rule = guppi_layout_rule_new (_("Same Width"));
  guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),
				     GLC_WIDTH, -1.0, geom1,
				     GLC_WIDTH,  1.0, geom2,
				     GLC_LAST);

  guppi_layout_rule_lock (rule);
  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_same_height (GuppiGeometry *geom1, GuppiGeometry *geom2)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom1), NULL);
  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom2), NULL);
  
  rule = guppi_layout_rule_new (_("Same Height"));
  guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),
				     GLC_HEIGHT, -1.0, geom1,
				     GLC_HEIGHT,  1.0, geom2,
				     GLC_LAST);

  guppi_layout_rule_lock (rule);
  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_same_size (GuppiGeometry *geom1, GuppiGeometry *geom2)
{
  GuppiLayoutRule *rule;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom1), NULL);
  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom2), NULL);
  
  rule = guppi_layout_rule_new (_("Same Size"));
  guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),
				     GLC_WIDTH, -1.0, geom1,
				     GLC_WIDTH,  1.0, geom2,
				     GLC_LAST);
  guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),
				     GLC_HEIGHT, -1.0, geom1,
				     GLC_HEIGHT,  1.0, geom2,
				     GLC_LAST);

  guppi_layout_rule_lock (rule);
  return rule;
}

GuppiLayoutRule *
guppi_layout_rule_new_same_place (GuppiGeometry *geom1, GuppiGeometry *geom2)
{
  GuppiLayoutRule *rule;
  GuppiLayoutConstraintTermType tt[4] = { GLC_TOP, GLC_BOTTOM, GLC_LEFT, GLC_RIGHT };
  gint i;

  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom1), NULL);
  g_return_val_if_fail (GUPPI_IS_GEOMETRY (geom2), NULL);

  rule = guppi_layout_rule_new (_("Same Place"));
  for (i=0; i<4; ++i) {
    guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),
				       tt[i], -1.0, geom1,
				       tt[i],  1.0, geom2,
				       GLC_LAST);
  }

  guppi_layout_rule_lock (rule);
  return rule;
}
