/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-text-tokens.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-text-tokens.h"

#include <guppi-defaults.h>
#include <guppi-memory.h>

/* I guess that this should be a union, but for now I'll just bundle
   everything up in one struct. */
typedef struct _GuppiTextTokenPrivate GuppiTextTokenPrivate;
struct _GuppiTextTokenPrivate {
  gint type;

  /* For TEXT_TOKEN_WORD */
  gchar *str;

  /* For TEXT_TOKEN_SPACE */
  double space_size;
  gboolean breakable;

  /* for TEXT_TOKEN_SOFT_BREAK */
  double line_len, line_asc, line_desc;
  gboolean from_hard;

  /* For TEXT_TOKEN_SET_FONT */
  GnomeFont *font;

  /* For TEXT_TOKEN_RESIZE_FONT */
  gint resize_type;
  double resize_quant;

  /* For TEXT_TOKEN_RAISE_LOWER */
  double raise_lower;

  /* For TEXT_TOKEN_JUSTIFY */
  GtkJustification  justify;
};

#define priv(x) ((GuppiTextTokenPrivate *)((x)->opaque_internals))


static GuppiTextToken *
guppi_text_token_new (gint type)
{
  GuppiTextToken *tt;
  GuppiTextTokenPrivate *p;

  g_return_val_if_fail (type != TEXT_TOKEN_ERROR, NULL);
  g_return_val_if_fail (0 < type && type < TEXT_TOKEN_LAST, NULL);

  tt = guppi_new0 (GuppiTextToken, 1);
  p = guppi_new0 (GuppiTextTokenPrivate, 1);
  tt->opaque_internals = p;

  p->type = type;

  return tt;
}

void
guppi_text_token_free (GuppiTextToken *tt)
{
  if (tt) {
    GuppiTextTokenPrivate *p = priv (tt);

    guppi_free0 (p->str);
    guppi_unref0 (p->font);
    
    guppi_free0 (tt->opaque_internals);
    guppi_free (tt);
  }
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

/* Constructors */

GuppiTextToken *
guppi_text_token_new_word (const gchar *str)
{
  GuppiTextToken *tt;
  GuppiTextTokenPrivate *p;

  g_return_val_if_fail (str, NULL);

  tt = guppi_text_token_new (TEXT_TOKEN_WORD);
  p = priv (tt);
  p->str = guppi_strdup (str);

  return tt;
}

GuppiTextToken *
guppi_text_token_new_space (double space_size)
{
  GuppiTextToken *tt;
  GuppiTextTokenPrivate *p;

  tt = guppi_text_token_new (TEXT_TOKEN_SPACE);
  p = priv (tt);
  p->space_size = space_size;
  p->breakable = TRUE;

  return tt;
}

GuppiTextToken *
guppi_text_token_new_nobreak (void)
{
  return guppi_text_token_new (TEXT_TOKEN_NOBREAK);
}

GuppiTextToken *
guppi_text_token_new_push (void)
{
  return guppi_text_token_new (TEXT_TOKEN_PUSH);

}

GuppiTextToken *
guppi_text_token_new_pop (void)
{
  return guppi_text_token_new (TEXT_TOKEN_POP);
}

GuppiTextToken *
guppi_text_token_new_set_font (GnomeFont *font)
{
  GuppiTextToken *tt;
  GuppiTextTokenPrivate *p;

  g_return_val_if_fail (font && GNOME_IS_FONT (font), NULL);

  tt = guppi_text_token_new (TEXT_TOKEN_SET_FONT);
  p = priv (tt);

  guppi_refcounting_assign (p->font, font);

  return tt;
}

GuppiTextToken *
guppi_text_token_new_resize_font (gint resize_type, double resize_quant)
{
  GuppiTextToken *tt;
  GuppiTextTokenPrivate *p;

  g_return_val_if_fail (0 <= resize_type && resize_type < RESIZE_FONT_LAST,
			NULL);

  if (resize_type == RESIZE_FONT_ABSOLUTE
      || resize_type == RESIZE_FONT_SCALED)
    g_return_val_if_fail (resize_type >= 0, NULL);
    
  tt = guppi_text_token_new (TEXT_TOKEN_RESIZE_FONT);
  p = priv (tt);

  p->resize_type = resize_type;
  p->resize_quant = resize_quant;
  
  return tt;
}

GuppiTextToken *
guppi_text_token_new_raise_lower (double dist)
{
  GuppiTextToken *tt;
  GuppiTextTokenPrivate *p;
    
  tt = guppi_text_token_new (TEXT_TOKEN_RAISE_LOWER);
  p = priv (tt);
  p->raise_lower = dist;
  
  return tt;
}

GuppiTextToken *
guppi_text_token_new_justify (GtkJustification j)
{
  GuppiTextToken *tt;
    
  tt = guppi_text_token_new (TEXT_TOKEN_JUSTIFY);
  priv (tt)->justify = j;
  
  return tt;
}

GuppiTextToken *
guppi_text_token_new_hard_break (void)
{
  return guppi_text_token_new (TEXT_TOKEN_HARD_BREAK);
}

GuppiTextToken *
guppi_text_token_new_soft_break (double line_length, 
				 double line_ascender,
				 double line_descender,
				 gboolean from_hard)
{
  GuppiTextToken *tt;
  GuppiTextTokenPrivate *p;

  tt = guppi_text_token_new (TEXT_TOKEN_SOFT_BREAK);
  p = priv (tt);
  p->line_len = line_length;
  p->line_asc = line_ascender;
  p->line_desc = line_descender;
  p->from_hard = from_hard;

  return tt;
}


/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

gint
guppi_text_token_type (GuppiTextToken *tt)
{
  if (tt == NULL)
    return TEXT_TOKEN_ERROR;

  return priv (tt)->type;
}

gboolean
guppi_text_token_is_break (GuppiTextToken *tt)
{
  GuppiTextTokenPrivate *p;

  g_return_val_if_fail (tt, FALSE);
  p = priv (tt);

  return p->type == TEXT_TOKEN_SOFT_BREAK || p->type == TEXT_TOKEN_HARD_BREAK;
}

const gchar *
guppi_text_token_word (GuppiTextToken *tt)
{
  g_return_val_if_fail (tt, NULL);
  g_return_val_if_fail (guppi_text_token_type (tt) == TEXT_TOKEN_WORD, NULL);

  return priv (tt)->str;
}

double
guppi_text_token_space_size (GuppiTextToken *tt)
{
  g_return_val_if_fail (tt, 0.0);
  g_return_val_if_fail (guppi_text_token_type (tt) == TEXT_TOKEN_SPACE, 0.0);

  return priv (tt)->space_size;
}

gboolean
guppi_text_token_space_is_breakable (GuppiTextToken *tt)
{
  g_return_val_if_fail (tt, FALSE);
  g_return_val_if_fail (guppi_text_token_type (tt) == TEXT_TOKEN_SPACE, FALSE);

  return priv (tt)->breakable;
}

double
guppi_text_token_raise_lower_distance (GuppiTextToken *tt)
{
  g_return_val_if_fail (tt, 0.0);

  if (guppi_text_token_type (tt) == TEXT_TOKEN_RAISE_LOWER)
    return priv (tt)->raise_lower;

  return 0;
}

GtkJustification
guppi_text_token_justification (GuppiTextToken *tt)
{
  g_return_val_if_fail (tt, GTK_JUSTIFY_LEFT);
  
  if (guppi_text_token_type (tt) == TEXT_TOKEN_JUSTIFY)
    return priv (tt)->justify;

  return GTK_JUSTIFY_LEFT;
}

void
guppi_text_token_soft_break_line_dimensions (GuppiTextToken *tt,
					     double *line_length,
					     double *line_ascender,
					     double *line_descender)
{
  g_return_if_fail (tt);
  g_return_if_fail (guppi_text_token_type (tt)==TEXT_TOKEN_SOFT_BREAK);
  
  if (line_length) *line_length = priv (tt)->line_len;
  if (line_ascender) *line_ascender = priv (tt)->line_asc;
  if (line_descender) *line_descender = priv (tt)->line_desc;
}

gboolean
guppi_text_token_soft_break_from_hard (GuppiTextToken *tt)
{
  g_return_val_if_fail (tt, FALSE);
  g_return_val_if_fail (guppi_text_token_type (tt)==TEXT_TOKEN_SOFT_BREAK,
			FALSE);
  
  return priv (tt)->from_hard;
}

#define MIN_FONT_SIZE 0.125
GnomeFont *
guppi_text_token_evolve_font (GuppiTextToken *tt, GnomeFont *font)
{
  GuppiTextTokenPrivate *p;
  gint type;
  GnomeFont *new_font = NULL;
  double sz, new_sz = 0;

  g_return_val_if_fail (tt, NULL);
  
  if (font == NULL)
    font = guppi_default_font ();
  type = guppi_text_token_type (tt);

  p = priv (tt);

  if (type == TEXT_TOKEN_SET_FONT) 
    return p->font;

  if (type == TEXT_TOKEN_RESIZE_FONT) {

    sz = gnome_font_get_size (font);

    switch (p->resize_type) {
      
    case RESIZE_FONT_ABSOLUTE:
      new_sz = p->resize_quant;
      break;
      
    case RESIZE_FONT_ADDITIVE:
      new_sz = sz + p->resize_quant;
      break;
      
    case RESIZE_FONT_SCALED:
      new_sz = sz * p->resize_quant;
      break;
      
    default:
      g_assert_not_reached ();
    }
    
    if (new_sz < MIN_FONT_SIZE)
      new_sz = MIN_FONT_SIZE;
    
    if (sz != new_sz)
      new_font = gnome_font_new (gnome_font_get_name (font), new_sz);
  }

  return new_font;
}

/* $Id$ */
