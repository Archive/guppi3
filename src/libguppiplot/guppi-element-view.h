
/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-element-view.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_ELEMENT_VIEW_H
#define _INC_GUPPI_ELEMENT_VIEW_H

/* #include <gnome.h> */
#include <libgnomeprint/gnome-print.h>
#include <libgnomeui/gnome-canvas.h>

#include "guppi-enums.h"
#include "guppi-element-state.h"
#include "guppi-geometry.h"
#include "guppi-view-interval.h"
#include "guppi-axis-markers.h"

#include "guppi-defs.h"

#define DEPRECATED_GUPPI_ELEMENT_VIEW 0

BEGIN_GUPPI_DECLS 

struct _GuppiStyle;
struct _GuppiCanvasItem;
struct _GuppiElementPrint;

typedef struct _GuppiElementView GuppiElementView;
typedef struct _GuppiElementViewClass GuppiElementViewClass;
struct _GuppiElementViewPrivate;

struct _GuppiElementView {
  GtkObject parent;
  struct _GuppiElementViewPrivate *priv;
};

struct _GuppiElementViewClass {
  GtkObjectClass parent_class;

  const gchar *plug_in_code;

  /* VTable */
  GtkType canvas_item_type;
  struct _GuppiCanvasItem *(*make_canvas_item) (GuppiElementView *,
						GnomeCanvas *,
						GnomeCanvasGroup *);
  void (*item_post_creation_init) (GuppiElementView *view, struct _GuppiCanvasItem *item);

  GtkType print_type;
  struct _GuppiElementPrint *(*make_print) (GuppiElementView *,
					    GnomePrintContext *);

  /* Init function called after view is constructed and has parent state
     attached. */
  void (*view_init) (GuppiElementView *);

  void (*update_axis_markers) (GuppiElementView *, guppi_axis_t, GuppiAxisMarkers *, double x0, double x1);

  gboolean (*preferred_range) (GuppiElementView *, guppi_axis_t, double *, double *);

  void (*print) (GuppiElementView *, GnomePrintContext *);

  /* Freeze/thaw */

  void (*freeze)       (GuppiElementView *);
  void (*thaw)         (GuppiElementView *);
  void (*fully_thawed) (GuppiElementView *);

  gboolean (*find) (GuppiElementView *view, const gchar *label, 
		    GuppiElementState **stateptr, GuppiElementView **viewptr);

  void (*make_config_model) (GuppiElementView *, GuppiConfigModel *);

  /* XML input & output */
  void     (*xml_export)  (GuppiElementView *, GuppiXMLDocument *, xmlNodePtr root_node);
  gboolean (*xml_import)  (GuppiElementView *, GuppiXMLDocument *, xmlNodePtr);

  /* Signals */
  void (*changed)           (GuppiElementView *);
  void (*changed_state)     (GuppiElementView *); /* the underlying state object emitted 'changed' */
  void (*changed_size)      (GuppiElementView *, double w, double h);
  void (*changed_position)  (GuppiElementView *);
  void (*changed_structure) (GuppiElementView *);
};

#define GUPPI_TYPE_ELEMENT_VIEW (guppi_element_view_get_type())
#define GUPPI_ELEMENT_VIEW(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_ELEMENT_VIEW,GuppiElementView))
#define GUPPI_ELEMENT_VIEW0(obj) ((obj) ? (GUPPI_ELEMENT_VIEW(obj)) : NULL)
#define GUPPI_ELEMENT_VIEW_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_ELEMENT_VIEW,GuppiElementViewClass))
#define GUPPI_IS_ELEMENT_VIEW(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_ELEMENT_VIEW))
#define GUPPI_IS_ELEMENT_VIEW0(obj) (((obj) == NULL) || (GUPPI_IS_ELEMENT_VIEW(obj)))
#define GUPPI_IS_ELEMENT_VIEW_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_ELEMENT_VIEW))

GtkType guppi_element_view_get_type (void);

GuppiElementView *guppi_element_view_new (GuppiElementState *, ...);

GuppiElementState *guppi_element_view_state (GuppiElementView *);
GuppiGeometry *guppi_element_view_geometry (GuppiElementView *);

guppi_uniq_t guppi_element_view_unique_id (GuppiElementView *);

const gchar *guppi_element_view_get_label (GuppiElementView *);
void         guppi_element_view_set_label (GuppiElementView *, const gchar *);

gboolean guppi_element_view_get (GuppiElementView *, ...);
gboolean guppi_element_view_set (GuppiElementView *, ...);

GuppiElementState *guppi_element_view_find_state (GuppiElementView *, const gchar *label);
GuppiElementView  *guppi_element_view_find_view  (GuppiElementView *, const gchar *label);


/* View Intervals */

GuppiViewInterval *guppi_element_view_axis_view_interval (GuppiElementView *, guppi_axis_t);
void               guppi_element_view_connect_view_intervals (GuppiElementView *view1, guppi_axis_t axis1,
							      GuppiElementView *view2, guppi_axis_t axis2);

void guppi_element_view_set_preferred_view (GuppiElementView *, guppi_axis_t);
void guppi_element_view_set_preferred_view_all (GuppiElementView *);
void guppi_element_view_force_preferred_view (GuppiElementView *, guppi_axis_t, gboolean);


/* Axis Markers */

void              guppi_element_view_add_axis_markers     (GuppiElementView *, guppi_axis_t);
gint              guppi_element_view_axis_marker_type     (GuppiElementView *, guppi_axis_t);
void              guppi_element_view_set_axis_marker_type (GuppiElementView *, guppi_axis_t, gint code);
GuppiAxisMarkers *guppi_element_view_axis_markers         (GuppiElementView *, guppi_axis_t);

void              guppi_element_view_connect_axis_markers (GuppiElementView *view1, guppi_axis_t axis1,
							   GuppiElementView *view2, guppi_axis_t axis2);


void guppi_element_view_set_state (GuppiElementView *, GuppiElementState *);

void guppi_element_view_get_bbox_vp (GuppiElementView *,
				     double *x0, double *y0,
				     double *x1, double *y1);
void guppi_element_view_get_bbox_pt (GuppiElementView *,
				     double *x0, double *y0,
				     double *x1, double *y1);

#define guppi_element_view_valid_x_vp(gev, x) \
(guppi_view_interval_valid(guppi_element_view_axis_view_interval((gev), GUPPI_X_AXIS), (x)))

#define guppi_element_view_valid_y_vp(gev, y) \
(guppi_view_interval_valid(guppi_element_view_axis_view_interval((gev), GUPPI_Y_AXIS), (y)))

#define guppi_element_view_valid_vp(gev, x, y) \
(guppi_element_view_valid_x_vp((gev), (x)) && \
 guppi_element_view_valid_y_vp((gev), (y)))

void guppi_element_view_vp2pt (GuppiElementView *,
			       double, double, double *, double *);
void guppi_element_view_pt2vp (GuppiElementView *,
			       double, double, double *, double *);

struct _GuppiCanvasItem *guppi_element_view_make_canvas_item (GuppiElementView *,
							      GnomeCanvas *,
							      GnomeCanvasGroup *);
struct _GuppiElementPrint *guppi_element_view_make_print (GuppiElementView *,
							  GnomePrintContext *);

gboolean guppi_element_view_visible (GuppiElementView *);
void guppi_element_view_show (GuppiElementView *);
void guppi_element_view_hide (GuppiElementView *);
void guppi_element_view_set_visibility (GuppiElementView *, gboolean);

gboolean guppi_element_view_tools_are_blocked (GuppiElementView *);
void guppi_element_view_set_tool_blocking (GuppiElementView *, gboolean);

void guppi_element_view_print (GuppiElementView *, GnomePrintContext *);
void guppi_element_view_print_to_bbox (GuppiElementView *,
				       GnomePrintContext *,
				       double x0, double y0,
				       double x1, double y1);
void guppi_element_view_print_ps_to_file (GuppiElementView *, const gchar *filename);

void guppi_element_view_changed (GuppiElementView *);
void guppi_element_view_changed_delayed (GuppiElementView *);
void guppi_element_view_flush_changes (GuppiElementView *);

void guppi_element_view_changed_size (GuppiElementView *, double width, double height);
void guppi_element_view_changed_structure (GuppiElementView *);

gboolean guppi_element_view_frozen (GuppiElementView *);
void     guppi_element_view_freeze (GuppiElementView *);
void     guppi_element_view_thaw   (GuppiElementView *);

GuppiConfigModel *guppi_element_view_make_config_model (GuppiElementView *view);

xmlNodePtr        guppi_element_view_export_xml (GuppiElementView *, GuppiXMLDocument *);
GuppiElementView *guppi_element_view_import_xml (GuppiXMLDocument *, xmlNodePtr);
void              guppi_element_view_spew_xml   (GuppiElementView *);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_ELEMENT_VIEW_H */

/* $Id$ */
