/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-element-print.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-element-print.h"

#include <guppi-convenient.h>
#include <guppi-rgb.h>

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0
};

static void
guppi_element_print_finalize (GtkObject *obj)
{
  GuppiElementPrint *ep = GUPPI_ELEMENT_PRINT (obj);

  guppi_unref0 (ep->view);
  guppi_unref0 (ep->context);

  guppi_finalized (obj);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_element_print_class_init (GuppiElementPrintClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;

  parent_class = gtk_type_class (GTK_TYPE_OBJECT);

  object_class->finalize = guppi_element_print_finalize;

}

static void
guppi_element_print_init (GuppiElementPrint *obj)
{

}

GtkType guppi_element_print_get_type (void)
{
  static GtkType guppi_element_print_type = 0;
  if (!guppi_element_print_type) {
    static const GtkTypeInfo guppi_element_print_info = {
      "GuppiElementPrint",
      sizeof (GuppiElementPrint),
      sizeof (GuppiElementPrintClass),
      (GtkClassInitFunc) guppi_element_print_class_init,
      (GtkObjectInitFunc) guppi_element_print_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_element_print_type =
      gtk_type_unique (GTK_TYPE_OBJECT, &guppi_element_print_info);
  }
  return guppi_element_print_type;
}

/***************************************************************************/

void
guppi_element_print_set_context (GuppiElementPrint *ep,
				 GnomePrintContext *pc)
{
  g_return_if_fail (ep != NULL);
  g_return_if_fail (GUPPI_IS_ELEMENT_PRINT (ep));
  g_return_if_fail (pc != NULL);
  g_return_if_fail (GNOME_IS_PRINT_CONTEXT (pc));

  if (pc == ep->context)
    return;

  guppi_unref (ep->context);
  ep->context = pc;

  guppi_ref (ep->context);
}

void
guppi_element_print_set_bbox (GuppiElementPrint *ep,
			      double x0, double y0, double x1, double y1)
{
  g_return_if_fail (ep != NULL);
  g_return_if_fail (GUPPI_IS_ELEMENT_PRINT (ep));

  ep->pt_x0 = MIN (x0, x1);
  ep->pt_y0 = MIN (y0, y1);
  ep->pt_x1 = MAX (x0, x1);
  ep->pt_y1 = MAX (y0, y1);
}

void
guppi_element_print_set_bbox_corner (GuppiElementPrint *ep,
				     double x, double y)
{
  g_return_if_fail (ep != NULL);
  g_return_if_fail (GUPPI_IS_ELEMENT_PRINT (ep));

  g_assert_not_reached ();
}

void
guppi_element_print_get_bbox (GuppiElementPrint *ep,
			      double *x0, double *y0, double *x1, double *y1)
{
  g_return_if_fail (ep != NULL);
  g_return_if_fail (GUPPI_IS_ELEMENT_PRINT (ep));

  if (x0)
    *x0 = ep->pt_x0;
  if (y0)
    *y0 = ep->pt_y0;
  if (x1)
    *x1 = ep->pt_x1;
  if (y1)
    *y1 = ep->pt_y1;
}

void
guppi_element_print_get_bbox_vp (GuppiElementPrint *ep,
				 double *x0, double *y0,
				 double *x1, double *y1)
{
  GuppiViewInterval *vxi;
  GuppiViewInterval *vyi;

  g_return_if_fail (ep != NULL);
  g_return_if_fail (GUPPI_IS_ELEMENT_PRINT (ep));

  vxi = guppi_element_view_axis_view_interval (guppi_element_print_view (ep), GUPPI_X_AXIS);
  vyi = guppi_element_view_axis_view_interval (guppi_element_print_view (ep), GUPPI_Y_AXIS);

  guppi_view_interval_range (vxi, x0, x1);
  guppi_view_interval_range (vyi, y0, y1);
}


void
guppi_element_print_print (GuppiElementPrint *ep)
{
  GuppiElementPrintClass *klass;
  GuppiElementView *view;

  g_return_if_fail (ep != NULL);
  g_return_if_fail (GUPPI_IS_ELEMENT_PRINT (ep));

  klass = GUPPI_ELEMENT_PRINT_CLASS (GTK_OBJECT (ep)->klass);
  view = guppi_element_print_view (ep);

  g_return_if_fail (klass->print != NULL);

  if (!guppi_element_view_visible (view))
    return;

  guppi_element_print_gsave (ep);

#if 0
  {
    /* A frame around our print region.  Useful for debugging placement. */
    double mx = (ep->pt_x0 + ep->pt_x1)/2;
    double my = (ep->pt_y0 + ep->pt_y1)/2;

    guppi_element_print_setlinewidth(ep, 72/64.0);
    guppi_element_print_newpath (ep);
    guppi_element_print_moveto (ep, ep->pt_x0, ep->pt_y0);
    guppi_element_print_lineto (ep, ep->pt_x1, ep->pt_y0);
    guppi_element_print_lineto (ep, ep->pt_x1, ep->pt_y1);
    guppi_element_print_lineto (ep, ep->pt_x0, ep->pt_y1);
    guppi_element_print_closepath (ep);
    guppi_element_print_stroke (ep);

    guppi_element_print_setlinewidth (ep, 72/128.0);
    guppi_element_print_newpath (ep);
    guppi_element_print_moveto (ep, ep->pt_x0, my);
    guppi_element_print_lineto (ep, ep->pt_x1, my);
    guppi_element_print_stroke (ep);
    guppi_element_print_newpath (ep);
    guppi_element_print_moveto (ep, mx, ep->pt_y0);
    guppi_element_print_lineto (ep, mx, ep->pt_y1);
    guppi_element_print_stroke (ep);
  }
#endif

  /* Clip to our bounding box */
  guppi_element_print_newpath (ep);
  guppi_element_print_moveto (ep, ep->pt_x0, ep->pt_y0);
  guppi_element_print_lineto (ep, ep->pt_x1, ep->pt_y0);
  guppi_element_print_lineto (ep, ep->pt_x1, ep->pt_y1);
  guppi_element_print_lineto (ep, ep->pt_x0, ep->pt_y1);
  guppi_element_print_closepath (ep);
  guppi_element_print_clip (ep);

  klass->print (ep);

  guppi_element_print_grestore (ep);
}

void
guppi_element_print_vp2pt (GuppiElementPrint *ep,
			   double x, double y, double *pt_x, double *pt_y)
{
  GuppiViewInterval *vi;
  GuppiElementView *view;
  double t;

  g_return_if_fail (ep != NULL);

  view = guppi_element_print_view (ep);

  if (pt_x) {
    vi = guppi_element_view_axis_view_interval (view, GUPPI_X_AXIS);
    t = guppi_view_interval_conv (vi, x);
    *pt_x = ep->pt_x0 + t * (ep->pt_x1 - ep->pt_x0);
  }

  if (pt_y) {
    vi = guppi_element_view_axis_view_interval (view, GUPPI_Y_AXIS);
    t = guppi_view_interval_conv (vi, y);
    *pt_y = ep->pt_y0 + t * (ep->pt_y1 - ep->pt_y0);
  }
}

void
guppi_element_print_vp2pt_auto (GuppiElementPrint *ep, double *x, double *y)
{
  guppi_element_print_vp2pt (ep, x ? *x : 0, y ? *y : 0, x, y);
}

/***************************************************************************/

gint guppi_element_print_newpath (GuppiElementPrint *ep)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  return gnome_print_newpath (guppi_element_print_context (ep));
}

gint guppi_element_print_moveto (GuppiElementPrint *ep, double x, double y)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  return gnome_print_moveto (guppi_element_print_context (ep), x, y);
}

gint
guppi_element_print_moveto_vp (GuppiElementPrint *ep, double x, double y)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  guppi_element_print_vp2pt_auto (ep, &x, &y);

  return gnome_print_moveto (guppi_element_print_context (ep), x, y);
}

gint guppi_element_print_lineto (GuppiElementPrint *ep, double x, double y)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  return gnome_print_lineto (guppi_element_print_context (ep), x, y);
}

gint
guppi_element_print_lineto_vp (GuppiElementPrint *ep, double x, double y)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  guppi_element_print_vp2pt_auto (ep, &x, &y);

  return gnome_print_lineto (guppi_element_print_context (ep), x, y);
}

gint
guppi_element_print_curveto (GuppiElementPrint *ep,
			     double x1, double y1,
			     double x2, double y2, double x3, double y3)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  return gnome_print_curveto (guppi_element_print_context (ep),
			      x1, y1, x2, y2, x3, y3);
}

gint
guppi_element_print_curveto_vp (GuppiElementPrint *ep,
				double x1, double y1,
				double x2, double y2, double x3, double y3)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  guppi_element_print_vp2pt_auto (ep, &x1, &y1);
  guppi_element_print_vp2pt_auto (ep, &x2, &y2);
  guppi_element_print_vp2pt_auto (ep, &x3, &y3);

  return gnome_print_curveto (guppi_element_print_context (ep),
			      x1, y1, x2, y2, x3, y3);
}

gint guppi_element_print_closepath (GuppiElementPrint *ep)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  return gnome_print_closepath (guppi_element_print_context (ep));
}

gint
guppi_element_print_setrgbcolor (GuppiElementPrint *ep,
				 double r, double g, double b)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  return gnome_print_setrgbcolor (guppi_element_print_context (ep), r, g, b);
}

gint
guppi_element_print_setrgbcolor_uint (GuppiElementPrint *ep, guint32 color)
{
  guint r, g, b;
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  UINT_TO_RGB (color, &r, &g, &b);

  return gnome_print_setrgbcolor (guppi_element_print_context (ep),
				  r / 255.0, g / 255.0, b / 255.0);
}

gint
guppi_element_print_setrgbacolor (GuppiElementPrint *ep,
				  double r, double g, double b, double a)
{
  gint rv1, rv2;

  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  rv1 = gnome_print_setrgbcolor (guppi_element_print_context (ep), r, g, b);

  rv2 = 0;
  if (a != 1.0)
    rv2 = gnome_print_setopacity (guppi_element_print_context (ep), a);

  return (rv1 == -1 || rv2 == -1) ? -1 : 0;
}

gint
guppi_element_print_setrgbacolor_uint (GuppiElementPrint *ep, guint32 color)
{
  guint r, g, b, a;
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  UINT_TO_RGBA (color, &r, &g, &b, &a);

  if (a == 0xff)
    return guppi_element_print_setrgbcolor (ep, r / 255.0, g / 255.0,
					    b / 255.0);
  else
    return guppi_element_print_setrgbacolor (ep, r / 255.0, g / 255.0,
					     b / 255.0, a / 255.0);
}


gint guppi_element_print_fill (GuppiElementPrint *ep)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  return gnome_print_fill (guppi_element_print_context (ep));
}

gint guppi_element_print_eofill (GuppiElementPrint *ep)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  return gnome_print_eofill (guppi_element_print_context (ep));
}


gint guppi_element_print_setlinewidth (GuppiElementPrint *ep, double w)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  return gnome_print_setlinewidth (guppi_element_print_context (ep), w);
}

gint guppi_element_print_setmiterlimit (GuppiElementPrint *ep, double lim)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  return gnome_print_setmiterlimit (guppi_element_print_context (ep), lim);
}

gint guppi_element_print_setlinejoin (GuppiElementPrint *ep, gint linejoin)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  return gnome_print_setlinejoin (guppi_element_print_context (ep), linejoin);
}

gint guppi_element_print_setlinecap (GuppiElementPrint *ep, gint linecap)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  return gnome_print_setlinecap (guppi_element_print_context (ep), linecap);
}

gint
guppi_element_print_setdash (GuppiElementPrint *ep, gint N, double *dash,
			     double offset)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  return gnome_print_setdash (guppi_element_print_context (ep), N, dash,
			      offset);
}

gint guppi_element_print_strokepath (GuppiElementPrint *ep)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  return gnome_print_strokepath (guppi_element_print_context (ep));
}

gint guppi_element_print_stroke (GuppiElementPrint *ep)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  return gnome_print_stroke (guppi_element_print_context (ep));
}

gint guppi_element_print_setfont (GuppiElementPrint *ep, GnomeFont *font)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);
  g_return_val_if_fail (font != NULL, -1);

  return gnome_print_setfont (guppi_element_print_context (ep), font);
}

gint guppi_element_print_show (GuppiElementPrint *ep, const gchar *text)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);
  g_return_val_if_fail (text != NULL, -1);

  return gnome_print_show (guppi_element_print_context (ep), text);
}

gint
guppi_element_print_concat (GuppiElementPrint *ep, const double matrix[6])
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  return gnome_print_concat (guppi_element_print_context (ep), matrix);
}

gint guppi_element_print_gsave (GuppiElementPrint *ep)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  return gnome_print_gsave (guppi_element_print_context (ep));
}

gint guppi_element_print_grestore (GuppiElementPrint *ep)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  return gnome_print_grestore (guppi_element_print_context (ep));
}

gint guppi_element_print_clip (GuppiElementPrint *ep)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  return gnome_print_clip (guppi_element_print_context (ep));
}

gint guppi_element_print_eoclip (GuppiElementPrint *ep)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  return gnome_print_eoclip (guppi_element_print_context (ep));
}

gint guppi_element_print_showpage (GuppiElementPrint *ep)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  return gnome_print_showpage (guppi_element_print_context (ep));
}

gint
guppi_element_print_beginpage (GuppiElementPrint *ep,
			       const gchar *page_name)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  return gnome_print_beginpage (guppi_element_print_context (ep), page_name);
}

gint guppi_element_print_setopacity (GuppiElementPrint *ep, double x)
{
  g_return_val_if_fail (ep != NULL, -1);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_PRINT (ep), -1);
  g_return_val_if_fail (guppi_element_print_context (ep) != NULL, -1);

  return gnome_print_setopacity (guppi_element_print_context (ep), x);
}

void
guppi_element_print_vpath (GuppiElementPrint *ep, ArtVpath *vpath,
			   gboolean app)
{
  g_return_if_fail (ep != NULL);
  g_return_if_fail (GUPPI_IS_ELEMENT_PRINT (ep));
  g_return_if_fail (guppi_element_print_context (ep) != NULL);
  g_return_if_fail (vpath != NULL);

  gnome_print_vpath (guppi_element_print_context (ep), vpath, app);
}

void
guppi_element_print_vpath_vp (GuppiElementPrint *ep, ArtVpath *vpath,
			      gboolean app)
{
  ArtVpath *vpath_copy;
  gint i, N;

  g_return_if_fail (ep != NULL);
  g_return_if_fail (GUPPI_IS_ELEMENT_PRINT (ep));
  g_return_if_fail (guppi_element_print_context (ep) != NULL);
  g_return_if_fail (vpath != NULL);

  N = 0;
  while (vpath[N].code != ART_END)
    ++N;
  ++N;
  if (N == 1)
    return;

  vpath_copy = guppi_new (ArtVpath, N);
  for (i = 0; i < N; ++i) {
    vpath_copy[i].code = vpath[i].code;
    guppi_element_print_vp2pt (ep, vpath[i].x, vpath[i].y,
			       &(vpath_copy[i].x), &(vpath_copy[i].y));
  }

  gnome_print_vpath (guppi_element_print_context (ep), vpath_copy, app);

  guppi_free (vpath_copy);
}

void
guppi_element_print_bpath (GuppiElementPrint *ep, ArtBpath *bpath,
			   gboolean app)
{
  g_return_if_fail (ep != NULL);
  g_return_if_fail (GUPPI_IS_ELEMENT_PRINT (ep));
  g_return_if_fail (guppi_element_print_context (ep) != NULL);
  g_return_if_fail (bpath != NULL);

  gnome_print_bpath (guppi_element_print_context (ep), bpath, app);
}

void
guppi_element_print_bpath_vp (GuppiElementPrint *ep, ArtBpath *bpath,
			      gboolean app)
{
  ArtBpath *bpath_copy;
  gint i, N;

  g_return_if_fail (ep != NULL);
  g_return_if_fail (GUPPI_IS_ELEMENT_PRINT (ep));
  g_return_if_fail (guppi_element_print_context (ep) != NULL);
  g_return_if_fail (bpath != NULL);

  N = 0;
  while (bpath[N].code != ART_END)
    ++N;
  ++N;
  if (N == 1)
    return;

  bpath_copy = guppi_new (ArtBpath, N);
  for (i = 0; i < N; ++i) {
    bpath_copy[i].code = bpath[i].code;
    guppi_element_print_vp2pt (ep, bpath[i].x1, bpath[i].y1,
			       &(bpath_copy[i].x1), &(bpath_copy[i].y1));
    guppi_element_print_vp2pt (ep, bpath[i].x2, bpath[i].y2,
			       &(bpath_copy[i].x2), &(bpath_copy[i].y2));
    guppi_element_print_vp2pt (ep, bpath[i].x3, bpath[i].y3,
			       &(bpath_copy[i].x3), &(bpath_copy[i].y3));
  }

  gnome_print_bpath (guppi_element_print_context (ep), bpath, app);

  guppi_free (bpath_copy);
}

/* $Id$ */
