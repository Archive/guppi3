/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-plot-tool.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_PLOT_TOOL_H
#define _INC_GUPPI_PLOT_TOOL_H

/* #include <gnome.h> */
#include <glib.h>

#include <gdk/gdk.h>

#include <gtk/gtkobject.h>

#include <libgnomeui/gnome-canvas.h>

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS 

struct _GuppiCanvasItem;

typedef enum {
  GPTPC_NONE,
  GPTPC_FRAME,
  GPTPC_FRAME_BY_CENTER,
  GPTPC_CIRCLE
} GuppiPlotToolPositionCue;

typedef struct _GuppiPlotTool GuppiPlotTool;
typedef struct _GuppiPlotToolClass GuppiPlotToolClass;

struct _GuppiPlotTool {
  GtkObject parent;

  gchar *name;
  GtkType supported_type;

  gint button;

  GdkCursor *cursor;

  gboolean tracks_motion;
  GuppiPlotToolPositionCue cue_type;
  guint32 cue_fill_color;
  double cue_arg;
  GnomeCanvasItem *cue_item;

  gboolean repeating;
  gint repeat_interval;
  gboolean repeat_lock;

  /* useful places to temporarily stuff data... */
  gint int_arg1, int_arg2, int_arg3;
  double arg1, arg2, arg3;
  gpointer ptr_arg1, ptr_arg2, ptr_arg3;

  /* Canvas coordinates */
  double raw_start_x, raw_start_y;
  double raw_prev_x, raw_prev_y;
  double raw_x, raw_y;

  /* vp coordinates */
  double start_x, start_y;
  double prev_x, prev_y;
  double x, y;			/* current x, y */

  /* pt (device indep) coordinates */
  double start_pt_x, start_pt_y;
  double prev_pt_x, prev_pt_y;
  double pt_x, pt_y;		/* current x, y */

  guint repeater_tag;

  gboolean terminated;

  struct _GuppiCanvasItem *canvas_item;

    gboolean (*supports) (GuppiPlotTool *, struct _GuppiCanvasItem *);
  void (*first) (GuppiPlotTool *, struct _GuppiCanvasItem *);
  void (*middle) (GuppiPlotTool *, struct _GuppiCanvasItem *);
  void (*repeat) (GuppiPlotTool *, struct _GuppiCanvasItem *);
  void (*last) (GuppiPlotTool *, struct _GuppiCanvasItem *);

  void (*finalize) (GuppiPlotTool *);
};

struct _GuppiPlotToolClass {
  GtkObjectClass parent_class;
};

#define GUPPI_TYPE_PLOT_TOOL (guppi_plot_tool_get_type())
#define GUPPI_PLOT_TOOL(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_PLOT_TOOL,GuppiPlotTool))
#define GUPPI_PLOT_TOOL0(obj) ((obj) ? (GUPPI_PLOT_TOOL(obj)) : NULL)
#define GUPPI_PLOT_TOOL_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_PLOT_TOOL,GuppiPlotToolClass))
#define GUPPI_IS_PLOT_TOOL(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_PLOT_TOOL))
#define GUPPI_IS_PLOT_TOOL0(obj) (((obj) == NULL) || (GUPPI_IS_PLOT_TOOL(obj)))
#define GUPPI_IS_PLOT_TOOL_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_PLOT_TOOL))

GtkType guppi_plot_tool_get_type (void);

GuppiPlotTool *guppi_plot_tool_new (void);

const gchar *guppi_plot_tool_name (GuppiPlotTool *);
void guppi_plot_tool_set_name (GuppiPlotTool * tool, const gchar *);

gboolean guppi_plot_tool_supports_item (GuppiPlotTool *,
					struct _GuppiCanvasItem *);

void guppi_plot_tool_first (GuppiPlotTool *,
			    struct _GuppiCanvasItem *,
			    double c_x, double c_y);
void guppi_plot_tool_middle (GuppiPlotTool *, double c_x, double c_y);
void guppi_plot_tool_end (GuppiPlotTool *, double c_x, double c_y);

/* Return an (allegedly) unique id for each type of tool. */
guint guppi_plot_tool_signature (GuppiPlotTool *);


END_GUPPI_DECLS

#endif /* _INC_GUPPI_PLOT_TOOL_H */

/* $Id$ */
