/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-pixbuf-stock.h
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __GUPPI_PIXBUF_STOCK_H__
#define __GUPPI_PIXBUF_STOCK_H__

#include "guppi-pixbuf.h"

GuppiPixbuf *guppi_pixbuf_stock_new_circle   (double r, double w);
GuppiPixbuf *guppi_pixbuf_stock_new_square   (double r, double w, double th);
GuppiPixbuf *guppi_pixbuf_stock_new_triangle (double r, double w, double th);
GuppiPixbuf *guppi_pixbuf_stock_new_starburst (double radius, double width, double edge, gint N, double th);


#endif /* __GUPPI_PIXBUF_STOCK_H__ */

