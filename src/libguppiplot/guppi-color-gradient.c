/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-color-gradient.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-color-gradient.h"

#include <gtk/gtksignal.h>

#include <math.h>
#include <guppi-rgb.h>
#include <guppi-memory.h>


static GtkObjectClass *parent_class = NULL;

enum {
  CHANGED,
  LAST_SIGNAL
};

guint cg_signals[LAST_SIGNAL] = { 0 };

static void
color_gradient_free (GuppiColorGradient * cg)
{
  guppi_free (cg->nodes);
  cg->nodes = NULL;
  cg->N = 0;
}

static void
guppi_color_gradient_finalize (GtkObject * obj)
{
  GuppiColorGradient *cg = GUPPI_COLOR_GRADIENT (obj);

  guppi_finalized (obj);

  color_gradient_free (cg);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_color_gradient_class_init (GuppiColorGradientClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;

  parent_class = gtk_type_class (GTK_TYPE_OBJECT);

  object_class->finalize = guppi_color_gradient_finalize;

  cg_signals[CHANGED] =
    gtk_signal_new ("changed",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GuppiColorGradientClass, changed),
		    gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);

  gtk_object_class_add_signals (object_class, cg_signals, LAST_SIGNAL);
}

static void
guppi_color_gradient_init (GuppiColorGradient * obj)
{
  obj->type = GUPPI_COLOR_GRADIENT_NONE;
  obj->v = 1.0;
  obj->a = 1.0;
}

GtkType guppi_color_gradient_get_type (void)
{
  static GtkType guppi_color_gradient_type = 0;
  if (!guppi_color_gradient_type) {
    static const GtkTypeInfo guppi_color_gradient_info = {
      "GuppiColorGradient",
      sizeof (GuppiColorGradient),
      sizeof (GuppiColorGradientClass),
      (GtkClassInitFunc) guppi_color_gradient_class_init,
      (GtkObjectInitFunc) guppi_color_gradient_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_color_gradient_type =
      gtk_type_unique (GTK_TYPE_OBJECT, &guppi_color_gradient_info);
  }
  return guppi_color_gradient_type;
}

GuppiColorGradient *
guppi_color_gradient_new (void)
{
  return
    GUPPI_COLOR_GRADIENT (guppi_type_new (guppi_color_gradient_get_type ()));
}

guint32 guppi_color_gradient_value (const GuppiColorGradient * cg, double t)
{
  gint i;
  double f;

  g_return_val_if_fail (cg != NULL, 0);
  g_return_val_if_fail (GUPPI_IS_COLOR_GRADIENT (cg), 0);
  g_return_val_if_fail (0 <= t && t <= 1, 0);

  if (cg->nodes == NULL || cg->N == 0)
    return 0;

  t *= cg->N - 1;
  i = (gint) floor (t);
  f = t - i;

  if (i == cg->N - 1)
    return cg->nodes[i];
  else
    return UINT_INTERPOLATE (cg->nodes[i], cg->nodes[i + 1], f);
}

guint32 guppi_color_gradient_get_node (const GuppiColorGradient * cg, gint i)
{
  g_return_val_if_fail (cg != NULL, 0);
  g_return_val_if_fail (GUPPI_IS_COLOR_GRADIENT (cg), 0);
  g_return_val_if_fail (i >= 0, 0);
  g_return_val_if_fail (i < cg->N, 0);

  return cg->nodes[i];
}

static void
changed (GuppiColorGradient * cg)
{
  if (cg->freeze_count == 0) {
    gtk_signal_emit (GTK_OBJECT (cg), cg_signals[CHANGED]);
    cg->pending_change = FALSE;
  } else
    cg->pending_change = TRUE;
}

void
guppi_color_gradient_reset (GuppiColorGradient * cg)
{
  g_return_if_fail (cg != NULL);
  g_return_if_fail (GUPPI_IS_COLOR_GRADIENT (cg));

  color_gradient_free (cg);
  cg->type = GUPPI_COLOR_GRADIENT_NONE;

  changed (cg);
}

void
guppi_color_gradient_set (GuppiColorGradient * cg, const guint32 * nodes,
			  gint N)
{
  gint i;

  g_return_if_fail (cg != NULL);
  g_return_if_fail (GUPPI_IS_COLOR_GRADIENT (cg));
  g_return_if_fail (nodes != NULL);
  g_return_if_fail (N > 0);

  color_gradient_free (cg);

  cg->N = N;
  cg->nodes = guppi_new (guint32, N);
  for (i = 0; i < N; ++i)
    cg->nodes[i] = nodes[i];

  cg->type = GUPPI_COLOR_GRADIENT_CUSTOM;

  changed (cg);
}

void
guppi_color_gradient_set2 (GuppiColorGradient * cg, guint32 a, guint32 b)
{
  guint32 pal[2];

  g_return_if_fail (cg != NULL);
  g_return_if_fail (GUPPI_IS_COLOR_GRADIENT (cg));

  if (cg->type == GUPPI_COLOR_GRADIENT_TWO &&
      cg->N == 2 && cg->nodes[0] == a && cg->nodes[1] == b)
    return;

  pal[0] = a;
  pal[1] = b;

  guppi_color_gradient_freeze (cg);
  guppi_color_gradient_set (cg, pal, 2);
  cg->type = GUPPI_COLOR_GRADIENT_TWO;
  guppi_color_gradient_thaw (cg);
}

void
guppi_color_gradient_set_fade_in (GuppiColorGradient * cg, guint32 c)
{
  guint32 fc;

  g_return_if_fail (cg != NULL);
  g_return_if_fail (GUPPI_IS_COLOR_GRADIENT (cg));

  fc = UINT_RGBA_CHANGE_A (c, 0);

  if (cg->type == GUPPI_COLOR_GRADIENT_FADE_IN &&
      cg->N == 2 && cg->nodes[0] == fc && cg->nodes[1] == c)
    return;

  guppi_color_gradient_freeze (cg);
  guppi_color_gradient_set2 (cg, fc, c);
  cg->type = GUPPI_COLOR_GRADIENT_FADE_IN;
  guppi_color_gradient_thaw (cg);
}

void
guppi_color_gradient_set_fade_out (GuppiColorGradient * cg, guint32 c)
{
  guint32 fc;

  g_return_if_fail (cg != NULL);
  g_return_if_fail (GUPPI_IS_COLOR_GRADIENT (cg));

  fc = UINT_RGBA_CHANGE_A (c, 0);

  if (cg->type == GUPPI_COLOR_GRADIENT_FADE_OUT &&
      cg->N == 2 && cg->nodes[0] == c && cg->nodes[1] == fc)
    return;


  guppi_color_gradient_freeze (cg);
  guppi_color_gradient_set2 (cg, c, fc);
  cg->type = GUPPI_COLOR_GRADIENT_FADE_OUT;
  guppi_color_gradient_thaw (cg);
}

void
guppi_color_gradient_set_fire (GuppiColorGradient * cg, double vv, double aa)
{
  guint32 x[3];
  guint v, a;

  g_return_if_fail (cg != NULL);
  g_return_if_fail (GUPPI_IS_COLOR_GRADIENT (cg));

  if (cg->type == GUPPI_COLOR_GRADIENT_FIRE && cg->v == vv && cg->a == aa)
    return;

  v = (gint) rint (CLAMP (vv, 0, 1) * 255);
  a = (gint) rint (CLAMP (aa, 0, 1) * 255);

  x[0] = RGBA_TO_UINT (0, 0, 0, a);
  x[1] = RGBA_TO_UINT (v, 0, 0, a);
  x[2] = RGBA_TO_UINT (v, v, 0, a);

  guppi_color_gradient_freeze (cg);
  guppi_color_gradient_set (cg, x, 3);
  cg->type = GUPPI_COLOR_GRADIENT_FIRE;
  cg->v = vv;
  cg->a = aa;
  guppi_color_gradient_thaw (cg);
}

void
guppi_color_gradient_set_ice (GuppiColorGradient * cg, double vv, double aa)
{
  guint32 x[3];
  guint v, a;

  g_return_if_fail (cg != NULL);
  g_return_if_fail (GUPPI_IS_COLOR_GRADIENT (cg));

  v = (gint) rint (CLAMP (vv, 0, 1) * 255);
  a = (gint) rint (CLAMP (aa, 0, 1) * 255);

  if (cg->type == GUPPI_COLOR_GRADIENT_ICE && cg->v == vv && cg->a == aa)
    return;

  x[0] = RGBA_TO_UINT (0, 0, 0, a);
  x[1] = RGBA_TO_UINT (0, 0, v, a);
  x[2] = RGBA_TO_UINT (0, v, v, a);

  guppi_color_gradient_freeze (cg);
  guppi_color_gradient_set (cg, x, 3);
  cg->type = GUPPI_COLOR_GRADIENT_ICE;
  cg->v = vv;
  cg->a = aa;
  guppi_color_gradient_thaw (cg);
}

void
guppi_color_gradient_set_thermal (GuppiColorGradient * cg, double vv,
				  double aa)
{
  guint32 x[5];
  guint v, a;

  g_return_if_fail (cg != NULL);
  g_return_if_fail (GUPPI_IS_COLOR_GRADIENT (cg));

  if (cg->type == GUPPI_COLOR_GRADIENT_THERMAL && cg->v == vv && cg->a == aa)
    return;

  v = (gint) rint (CLAMP (vv, 0, 1) * 255);
  a = (gint) rint (CLAMP (aa, 0, 1) * 255);

  x[0] = RGBA_TO_UINT (0, v, v, a);
  x[1] = RGBA_TO_UINT (0, 0, v, a);
  x[2] = RGBA_TO_UINT (v / 2, 0, v / 2, a);
  x[3] = RGBA_TO_UINT (v, 0, 0, a);
  x[4] = RGBA_TO_UINT (v, v, 0, a);

  guppi_color_gradient_freeze (cg);
  guppi_color_gradient_set (cg, x, 5);
  cg->type = GUPPI_COLOR_GRADIENT_THERMAL;
  cg->v = vv;
  cg->a = aa;
  guppi_color_gradient_thaw (cg);
}

void
guppi_color_gradient_set_spectrum (GuppiColorGradient * cg, double vv,
				   double aa)
{
  guint32 x[6];
  guint v, a;

  g_return_if_fail (cg != NULL);
  g_return_if_fail (GUPPI_IS_COLOR_GRADIENT (cg));

  if (cg->type == GUPPI_COLOR_GRADIENT_SPECTRUM && cg->v == vv && cg->a == aa)
    return;

  v = (gint) rint (CLAMP (vv, 0, 1) * 255);
  a = (gint) rint (CLAMP (aa, 0, 1) * 255);

  x[0] = RGBA_TO_UINT (v, 0, 0, a);	/* red */
  x[1] = RGBA_TO_UINT (v, v / 2, 0, a);	/* orange */
  x[2] = RGBA_TO_UINT (v, v, 0, a);	/* yellow */
  x[3] = RGBA_TO_UINT (v / 5, 4 * v / 5, v / 5, a);	/* green */
  x[4] = RGBA_TO_UINT (0, v / 2, v, a);	/* blue */
  x[5] = RGBA_TO_UINT (v, v / 2, v, a);	/* violet */


  guppi_color_gradient_freeze (cg);
  guppi_color_gradient_set (cg, x, 6);
  cg->type = GUPPI_COLOR_GRADIENT_SPECTRUM;
  cg->v = vv;
  cg->a = aa;
  guppi_color_gradient_thaw (cg);
}

/***************************************************************************/

static void
set (GuppiColorGradient * cg, gint t, double v, double a,
     guint32 c1, guint32 c2)
{
  switch (t) {
  case GUPPI_COLOR_GRADIENT_NONE:
    guppi_color_gradient_reset (cg);
    break;

  case GUPPI_COLOR_GRADIENT_TWO:
    guppi_color_gradient_set2 (cg, c1, c2);
    break;

  case GUPPI_COLOR_GRADIENT_FADE_IN:
    guppi_color_gradient_set_fade_in (cg, c1);
    break;

  case GUPPI_COLOR_GRADIENT_FADE_OUT:
    guppi_color_gradient_set_fade_out (cg, c1);
    break;

  case GUPPI_COLOR_GRADIENT_FIRE:
    guppi_color_gradient_set_fire (cg, v, a);
    break;

  case GUPPI_COLOR_GRADIENT_ICE:
    guppi_color_gradient_set_ice (cg, v, a);
    break;

  case GUPPI_COLOR_GRADIENT_THERMAL:
    guppi_color_gradient_set_thermal (cg, v, a);
    break;

  case GUPPI_COLOR_GRADIENT_SPECTRUM:
    guppi_color_gradient_set_spectrum (cg, v, a);
    break;

  default:
    g_assert_not_reached ();
  }
}

static gboolean
uses_v (gint t)
{
  return t == GUPPI_COLOR_GRADIENT_FIRE ||
    t == GUPPI_COLOR_GRADIENT_ICE ||
    t == GUPPI_COLOR_GRADIENT_THERMAL || t == GUPPI_COLOR_GRADIENT_SPECTRUM;
}

static gboolean
uses_a (gint t)
{
  return t == GUPPI_COLOR_GRADIENT_FIRE ||
    t == GUPPI_COLOR_GRADIENT_ICE ||
    t == GUPPI_COLOR_GRADIENT_THERMAL || t == GUPPI_COLOR_GRADIENT_SPECTRUM;
}

void
guppi_color_gradient_set_intensity (GuppiColorGradient * cg, double v)
{
  g_return_if_fail (cg != NULL);
  g_return_if_fail (GUPPI_IS_COLOR_GRADIENT (cg));

  if (uses_v (cg->type))
    set (cg, cg->type, v, cg->a, 0, 0);
}

void
guppi_color_gradient_set_alpha (GuppiColorGradient * cg, double a)
{
  g_return_if_fail (cg != NULL);
  g_return_if_fail (GUPPI_IS_COLOR_GRADIENT (cg));

  if (uses_a (cg->type))
    set (cg, cg->type, cg->v, a, 0, 0);
}

/***************************************************************************/

void
guppi_color_gradient_freeze (GuppiColorGradient * cg)
{
  g_return_if_fail (cg != NULL);
  g_return_if_fail (GUPPI_IS_COLOR_GRADIENT (cg));
  ++cg->freeze_count;
}

void
guppi_color_gradient_thaw (GuppiColorGradient * cg)
{
  g_return_if_fail (cg != NULL);
  g_return_if_fail (GUPPI_IS_COLOR_GRADIENT (cg));
  g_return_if_fail (cg->freeze_count > 0);
  --cg->freeze_count;
  if (cg->freeze_count == 0 && cg->pending_change)
    changed (cg);
}

/* $Id$ */
