/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-root-group-state.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-root-group-state.h"

#include <guppi-memory.h>
#include "guppi-root-group-view.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_root_group_state_finalize (GtkObject *obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_root_group_state_class_init (GuppiRootGroupStateClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementStateClass *state_class = GUPPI_ELEMENT_STATE_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_GROUP_STATE);

  object_class->finalize = guppi_root_group_state_finalize;

  state_class->view_type = GUPPI_TYPE_ROOT_GROUP_VIEW;
  state_class->plug_in_code = "GuppiRootGroupState";
}

static void
guppi_root_group_state_init (GuppiRootGroupState * obj)
{
}

GtkType guppi_root_group_state_get_type (void)
{
  static GtkType guppi_root_group_state_type = 0;
  if (!guppi_root_group_state_type) {
    static const GtkTypeInfo guppi_root_group_state_info = {
      "GuppiRootGroupState",
      sizeof (GuppiRootGroupState),
      sizeof (GuppiRootGroupStateClass),
      (GtkClassInitFunc) guppi_root_group_state_class_init,
      (GtkObjectInitFunc) guppi_root_group_state_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_root_group_state_type =
      gtk_type_unique (GUPPI_TYPE_GROUP_STATE, &guppi_root_group_state_info);
  }
  return guppi_root_group_state_type;
}

GuppiElementState *
guppi_root_group_state_new (void)
{
  return GUPPI_ELEMENT_STATE (guppi_type_new (guppi_root_group_state_get_type ()));
}

/* $Id$ */
