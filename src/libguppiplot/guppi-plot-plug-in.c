/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-plot-plug-in.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-plot-plug-in.h"

#include <guppi-memory.h>


static GtkObjectClass *parent_class = NULL;

static void
guppi_plot_plug_in_destroy (GtkObject * obj)
{
  if (parent_class->destroy)
    parent_class->destroy (obj);
}

static void
guppi_plot_plug_in_finalize (GtkObject * obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_plot_plug_in_class_init (GuppiPlotPlugInClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;

  parent_class = gtk_type_class (GUPPI_TYPE_PLUG_IN);

  object_class->destroy = guppi_plot_plug_in_destroy;
  object_class->finalize = guppi_plot_plug_in_finalize;

}

static void
guppi_plot_plug_in_init (GuppiPlotPlugIn * obj)
{

}

GtkType guppi_plot_plug_in_get_type (void)
{
  static GtkType guppi_plot_plug_in_type = 0;
  if (!guppi_plot_plug_in_type) {
    static const GtkTypeInfo guppi_plot_plug_in_info = {
      "GuppiPlotPlugIn",
      sizeof (GuppiPlotPlugIn),
      sizeof (GuppiPlotPlugInClass),
      (GtkClassInitFunc) guppi_plot_plug_in_class_init,
      (GtkObjectInitFunc) guppi_plot_plug_in_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_plot_plug_in_type =
      gtk_type_unique (GUPPI_TYPE_PLUG_IN, &guppi_plot_plug_in_info);
  }
  return guppi_plot_plug_in_type;
}

GuppiPlugIn *
guppi_plot_plug_in_new (void)
{
  return GUPPI_PLUG_IN (guppi_type_new (guppi_plot_plug_in_get_type ()));
}



/* $Id$ */
