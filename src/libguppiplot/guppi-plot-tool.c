/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-plot-tool.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-plot-tool.h"

#include <libgnomeui/gnome-canvas-rect-ellipse.h>

#include <guppi-useful.h>

#include "guppi-canvas-item.h"
#include "guppi-view-interval.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_plot_tool_destroy (GtkObject * obj)
{
  GuppiPlotTool *tool;

  tool = GUPPI_PLOT_TOOL (obj);

  if (tool->cursor) {
    gdk_cursor_destroy (tool->cursor);
    tool->cursor = NULL;
  }

  if (parent_class->destroy)
    parent_class->destroy (obj);
}

static void
guppi_plot_tool_finalize (GtkObject * obj)
{
  GuppiPlotTool *tool = GUPPI_PLOT_TOOL (obj);

  guppi_finalized (obj);

  guppi_free (tool->name);
  tool->name = NULL;

  if (tool->finalize)
    tool->finalize (tool);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_plot_tool_class_init (GuppiPlotToolClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;

  parent_class = gtk_type_class (GTK_TYPE_OBJECT);

  object_class->destroy = guppi_plot_tool_destroy;
  object_class->finalize = guppi_plot_tool_finalize;

}

static void
guppi_plot_tool_init (GuppiPlotTool * obj)
{
  obj->supported_type = (GtkType) 0;

  obj->cue_type = GPTPC_NONE;
  obj->cue_fill_color = 0x0000ff30;
}

GtkType guppi_plot_tool_get_type (void)
{
  static GtkType guppi_plot_tool_type = 0;
  if (!guppi_plot_tool_type) {
    static const GtkTypeInfo guppi_plot_tool_info = {
      "GuppiPlotTool",
      sizeof (GuppiPlotTool),
      sizeof (GuppiPlotToolClass),
      (GtkClassInitFunc) guppi_plot_tool_class_init,
      (GtkObjectInitFunc) guppi_plot_tool_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_plot_tool_type =
      gtk_type_unique (GTK_TYPE_OBJECT, &guppi_plot_tool_info);
  }
  return guppi_plot_tool_type;
}

GuppiPlotTool *
guppi_plot_tool_new (void)
{
  return GUPPI_PLOT_TOOL (guppi_type_new (guppi_plot_tool_get_type ()));
}

const gchar *
guppi_plot_tool_name (GuppiPlotTool * tool)
{
  g_return_val_if_fail (tool != NULL && GUPPI_IS_PLOT_TOOL (tool), NULL);

  return tool->name;
}

void
guppi_plot_tool_set_name (GuppiPlotTool * tool, const gchar * name)
{
  g_return_if_fail (tool != NULL && GUPPI_IS_PLOT_TOOL (tool));

  guppi_free (tool->name);
  tool->name = guppi_strdup (name);
}

gboolean
guppi_plot_tool_supports_item (GuppiPlotTool * tool, GuppiCanvasItem * item)
{
  g_return_val_if_fail (tool != NULL, FALSE);
  g_return_val_if_fail (GUPPI_IS_PLOT_TOOL (tool), FALSE);
  g_return_val_if_fail (item != NULL, FALSE);
  g_return_val_if_fail (GUPPI_IS_CANVAS_ITEM (item), FALSE);

  if (tool->supported_type == 0) {
    g_warning ("Tool \"%s\"'s supported type is undefined.", tool->name);
    return FALSE;
  }

  return gtk_type_is_a (GTK_OBJECT_TYPE (item), tool->supported_type) &&
    (tool->supports == NULL || tool->supports (tool, item));
}

/**************************************************************************/

static gint
tool_repeat_handler (gpointer data)
{
  GuppiPlotTool *tool;

  g_return_val_if_fail (data != NULL, FALSE);
  g_return_val_if_fail (GUPPI_IS_PLOT_TOOL (data), FALSE);

  tool = GUPPI_PLOT_TOOL (data);

  if (tool->repeater_tag == 0)
    return FALSE;

  if (tool->repeat_lock)
    return TRUE;

  g_return_val_if_fail (tool->repeat != NULL, FALSE);
  g_return_val_if_fail (tool->canvas_item != NULL, FALSE);

  tool->repeat_lock = TRUE;
  tool->repeat (tool, tool->canvas_item);
  tool->repeat_lock = FALSE;

  return TRUE;
}

void
guppi_plot_tool_first (GuppiPlotTool * tool,
		       GuppiCanvasItem * item, double c_x, double c_y)
{
  GnomeCanvas *gnome_canvas;

  g_return_if_fail (tool != NULL && GUPPI_IS_PLOT_TOOL (tool));
  g_return_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item));

  g_return_if_fail (tool->canvas_item == NULL);
  tool->canvas_item = item;

  gnome_canvas = GNOME_CANVAS_ITEM (item)->canvas;

  tool->raw_x = tool->raw_start_x = c_x;
  tool->raw_y = tool->raw_start_y = c_y;

  guppi_canvas_item_c2vp (item, c_x, c_y, &tool->x, &tool->y);
  tool->start_x = tool->x;
  tool->start_y = tool->y;

  guppi_canvas_item_c2pt (item, c_x, c_y, &tool->pt_x, &tool->pt_y);
  tool->start_pt_x = tool->pt_x;
  tool->start_pt_y = tool->pt_y;

  tool->terminated = FALSE;

  if (tool->first)
    tool->first (tool, item);

  if (tool->terminated)
    return;

  if (tool->repeating && tool->repeat)
    tool->repeater_tag = gtk_timeout_add (tool->repeat_interval,
					  tool_repeat_handler, tool);

  if (tool->cue_type == GPTPC_NONE) {

    /* No position cue: do nothing */

  } else if (tool->cue_type == GPTPC_FRAME ||
	     tool->cue_type == GPTPC_FRAME_BY_CENTER) {
    tool->cue_item =
      gnome_canvas_item_new (gnome_canvas_root (gnome_canvas),
			     gnome_canvas_rect_get_type (),
			     "outline_color", "black",
			     "fill_color_rgba", tool->cue_fill_color,
			     "width_pixels", (guint) 1,
			     "x1", tool->raw_start_x,
			     "y1", tool->raw_start_y,
			     "x2", tool->raw_start_x,
			     "y2", tool->raw_start_y, NULL);
  } else if (tool->cue_type == GPTPC_CIRCLE) {
    double px_r = guppi_pt2px (tool->cue_arg);

    tool->cue_item =
      gnome_canvas_item_new (gnome_canvas_root (gnome_canvas),
			     gnome_canvas_ellipse_get_type (),
			     "outline_color", "black",
			     "fill_color_rgba", tool->cue_fill_color,
			     "width_pixels", (guint) 1,
			     "x1", tool->raw_start_x - px_r,
			     "y1", tool->raw_start_y - px_r,
			     "x2", tool->raw_start_x + px_r,
			     "y2", tool->raw_start_y + px_r, NULL);
  } else {

    /* tool->cue_type is invalid */
    g_assert_not_reached ();

  }

}

void
guppi_plot_tool_middle (GuppiPlotTool * tool, double c_x, double c_y)
{
  GuppiCanvasItem *item;

  g_return_if_fail (tool != NULL && GUPPI_IS_PLOT_TOOL (tool));
  g_return_if_fail (tool->canvas_item != NULL);

  item = tool->canvas_item;

  tool->raw_prev_x = tool->raw_x;
  tool->raw_prev_y = tool->raw_y;
  tool->prev_x = tool->x;
  tool->prev_y = tool->y;
  tool->prev_pt_x = tool->pt_x;
  tool->prev_pt_y = tool->pt_y;

  tool->raw_x = c_x;
  tool->raw_y = c_y;
  guppi_canvas_item_c2vp (item, c_x, c_y, &tool->x, &tool->y);
  guppi_canvas_item_c2pt (item, c_x, c_y, &tool->pt_x, &tool->pt_y);

  if (tool->cue_item) {

    if (tool->cue_type == GPTPC_FRAME) {

      gnome_canvas_item_set (tool->cue_item,
			     "x1", MIN (tool->raw_start_x, tool->raw_x),
			     "y1", MIN (tool->raw_start_y, tool->raw_y),
			     "x2", MAX (tool->raw_start_x, tool->raw_x),
			     "y2", MAX (tool->raw_start_y, tool->raw_y),
			     NULL);

    } else if (tool->cue_type == GPTPC_FRAME_BY_CENTER) {

      double w = fabs (tool->raw_start_x - tool->raw_x);
      double h = fabs (tool->raw_start_y - tool->raw_y);

      gnome_canvas_item_set (tool->cue_item,
			     "x1", tool->raw_start_x - w,
			     "y1", tool->raw_start_y - h,
			     "x2", tool->raw_start_x + w,
			     "y2", tool->raw_start_y + h, NULL);


    } else if (tool->cue_type == GPTPC_CIRCLE) {
      double px_r = guppi_pt2px (tool->cue_arg);

      gnome_canvas_item_set (tool->cue_item,
			     "x1", tool->raw_x - px_r,
			     "y1", tool->raw_y - px_r,
			     "x2", tool->raw_x + px_r,
			     "y2", tool->raw_y + px_r, NULL);

    } else {
      g_assert_not_reached ();
    }
  }

  if (tool->middle)
    tool->middle (tool, item);

}

void
guppi_plot_tool_end (GuppiPlotTool * tool, double c_x, double c_y)
{
  g_return_if_fail (tool != NULL && GUPPI_IS_PLOT_TOOL (tool));
  g_return_if_fail (tool->canvas_item != NULL);

  if (tool->cue_item) {
    gtk_object_destroy (GTK_OBJECT (tool->cue_item));
    tool->cue_item = NULL;
  }

  if (tool->last) {

    tool->raw_prev_x = tool->raw_x;
    tool->raw_prev_y = tool->raw_y;
    tool->prev_x = tool->x;
    tool->prev_y = tool->y;
    tool->prev_pt_x = tool->pt_x;
    tool->prev_pt_y = tool->pt_y;

    tool->raw_x = c_x;
    tool->raw_x = c_y;
    guppi_canvas_item_c2vp (tool->canvas_item, c_x, c_y, &tool->x, &tool->y);
    guppi_canvas_item_c2pt (tool->canvas_item,
			    c_x, c_y, &tool->pt_x, &tool->pt_y);

    tool->last (tool, tool->canvas_item);
  }

  if (tool->repeating && tool->repeater_tag) {
    gtk_timeout_remove (tool->repeater_tag);
    tool->repeater_tag = 0;
  }

  tool->canvas_item = NULL;
}

guint guppi_plot_tool_signature (GuppiPlotTool * tool)
{
  guint s, f, m, l, z;
  guint seed = 0xdeadbeef;

  g_return_val_if_fail (tool != NULL && GUPPI_IS_PLOT_TOOL (tool), 0);

  s = GPOINTER_TO_UINT (tool->supports);
  f = GPOINTER_TO_UINT (tool->first);
  m = GPOINTER_TO_UINT (tool->middle);
  l = GPOINTER_TO_UINT (tool->last);
  z = GPOINTER_TO_UINT (tool->finalize);

  return 17 * (17 * (17 * (17 * (17 * seed + s) + f) + m) + l) + z;
}


/* $Id$ */
