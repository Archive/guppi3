/* $Id$ */

/*
 * demo.c
 *
 * Copyright (C) 1999 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <math.h>
#include <gnome.h>
#include "guppi-root-group-view.h"
#include "guppi-group-view-layout.h"
#include <guppi-seq-scalar.h>
#include <guppi-memory.h>
#include <guppi-data-socket.h>
#include <guppi-useful.h>
#include <guppi-data-init.h>
#include <guppi-plot-init.h>
#include <guppi-color-palette.h>

GtkWidget *window;
GtkWidget *canvas;
GuppiRootGroupView *root_view;

GuppiElementState *g1_state, *g2_state, *text_state, *bp_state, *ax_state;
GuppiElementView *g1_view, *g2_view, *text_view, *bp_view, *ax_view;

GuppiData *d1, *d2, *d3;

GuppiColorPalette *pal;

const gchar *labels[] = { "This is a demo program for Guppi",
			  "<center>This text element can handle<br/>two lines of text</center>",
			  "A fun number: <rat integer_part=\"112\" numerator=\"10\" denominator=\"32\"/>",
			  "Another fun number: <sci mantissa=\"6.02\" exponent=\"24\"/>",
			  "Fun <scale factor=\"2\">Large</scale> Text!",
			  "<center>Three lines<br/>of text is also<br/>not a problem</center>",
			  "<center>It<br/>is<br/>possible<br/>to<br/>go<br/>a<br/>bit<br/>too<br/>far<br/>though</center>",
			  NULL
};
gint label_index = 0;

guint timeout;
  

static void
init (gint argc, gchar *argv[])
{
  gnome_init ("demo", "0.0", argc, argv);

  guppi_useful_init_without_guile ();
  guppi_data_init ();
  guppi_plot_init ();

  guppi_plug_in_path_set ("../../plug-ins");
  guppi_plug_in_spec_find_all ();

}

static gboolean
label_timeout (gpointer foo)
{
  double angle;
  gint i, i0, i1;
  static gboolean done_xml = FALSE;

#if 0
  if (!done_xml) {
    guppi_element_view_spew_xml (GUPPI_ELEMENT_VIEW (root_view));
    done_xml = TRUE;
  }
#endif
  
  guppi_element_state_set (text_state,
			   "text", labels[label_index],
			   NULL);
  ++label_index;
  if (labels[label_index] == NULL)
    label_index = 0;

  guppi_color_palette_set_offset (pal, guppi_color_palette_get_offset (pal) + 1);

#if 0
  guppi_element_state_get (g2_state,
			   "base_angle", &angle,
			   NULL);
  angle += 2*M_PI/32;
  guppi_element_state_set (g2_state,
			   "base_angle", angle,
			   NULL);
#endif

#if 0
  guppi_seq_bounds (GUPPI_SEQ (d2), &i0, &i1);
  for (i=i0; i<=i1; ++i) {
    double x = guppi_seq_scalar_get (GUPPI_SEQ_SCALAR (d2), i);
    x *= 0.98;
    guppi_seq_scalar_set (GUPPI_SEQ_SCALAR (d2), i, x);
  }
#endif

  return TRUE;
}

static void
quit (GtkWidget *w, GdkEventAny *ev, gpointer closure)
{
  gtk_timeout_remove (timeout);
  guppi_unref (root_view);
  guppi_unref (g1_state);
  guppi_unref (g2_state);
  guppi_unref (text_state);
  guppi_unref (ax_state);
  guppi_unref (bp_state);
  guppi_unref (g1_view);
  guppi_unref (g2_view);
  guppi_unref (text_view);
  guppi_unref (bp_view);
  guppi_unref (ax_view);
  guppi_unref (d1);
  guppi_unref (d2);
  guppi_unref (d3);
  guppi_unref (pal);
  g_message ("bang! bang! bang!");
  gtk_widget_destroy (window);

  gtk_main_quit ();
}

static void
build_gui (void)
{
  canvas = guppi_root_group_view_make_canvas (root_view, NULL);

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_widget_set_usize (window, 800, 600);

  gtk_signal_connect (GTK_OBJECT (window),
		      "delete_event",
		      GTK_SIGNAL_FUNC (quit),
		      NULL);

  gtk_container_add (GTK_CONTAINER (window), GTK_WIDGET (canvas));
  gtk_widget_show_all (window);
}

#define DATA_COUNT 100
static void
build_data (void)
{
  gint i;
  double t, x, y;

  d1 = guppi_seq_scalar_new ();
  d2 = guppi_seq_scalar_new ();
  d3 = guppi_seq_scalar_new ();

  for (i=0; i<DATA_COUNT; ++i) {
    t = 2*M_PI*i/(double)DATA_COUNT;
    x = sin (4*t);
    y = cos (3*t);
    guppi_seq_scalar_append (GUPPI_SEQ_SCALAR (d1), x);
    guppi_seq_scalar_append (GUPPI_SEQ_SCALAR (d2), y);
  }

  for (i = 0; i < 10; ++i) {
    guppi_seq_scalar_append (GUPPI_SEQ_SCALAR (d3), i+1);
  }
}

static void
build_elements (void)
{
  g1_state = guppi_element_state_new ("scatter",
				      "label", "scatter_state",
				      NULL);
  //guppi_element_state_spew_xml (g1_state);

  pal = guppi_color_palette_new ();

  g2_state = guppi_element_state_new ("pie",
				      "label", "pie_state",
				      "use_stock_colors", FALSE,
				      "slice_colors", pal,
				      NULL);

  text_state = guppi_element_state_new ("text",
					"label", "text_state",
					"text", "Initial Text",
					NULL);

  bp_state = guppi_element_state_new ("boxplot",
				      "label", "boxplot_state",
				      "horizontal", FALSE,
				      NULL);

  ax_state = guppi_element_state_new ("axis",
				      "label", "axis_state",
				      "position", GUPPI_WEST,
				      NULL);

  g1_view = guppi_element_view_new (g1_state, NULL);
  g2_view = guppi_element_view_new (g2_state, NULL);
  text_view = guppi_element_view_new (text_state, NULL);
  bp_view = guppi_element_view_new (bp_state, NULL);
  ax_view = guppi_element_view_new (ax_state, NULL);

  guppi_element_view_connect_view_intervals (g1_view, GUPPI_Y_AXIS,
					     bp_view, GUPPI_Y_AXIS);

  guppi_element_view_connect_view_intervals (g1_view, GUPPI_Y_AXIS,
					     ax_view, GUPPI_Y_AXIS);
					     

  root_view = GUPPI_ROOT_GROUP_VIEW (guppi_root_group_view_new ());
  guppi_root_group_view_set_size (root_view, 72*7, 72*5);
}

static void
attach_data (GuppiElementView *view)
{
  guppi_element_view_set (view,
			  "scatter_state.x_data", d1,
			  "scatter_state.y_data", d2,
			  "pie_state.data", d3,
			  "boxplot_state.data", d2,
			  "axis_state.data", d2,
			  NULL);
}

static void
layout_elements (void)
{
  GuppiGroupView *group = GUPPI_GROUP_VIEW (root_view);

  guppi_group_view_layout_flush_left (group, ax_view, 0);      
  guppi_group_view_layout_horizontally_aligned (group, ax_view, g1_view, 0);
  guppi_group_view_layout_horizontally_aligned (group, g1_view, bp_view, 0);
  guppi_group_view_layout_flush_right (group, g2_view, 0);     
  guppi_group_view_layout_flush_bottom (group, g1_view, 0);    
  guppi_group_view_layout_flush_bottom (group, g2_view, 0);    
  guppi_group_view_layout_flush_bottom (group, bp_view, 0);    
  guppi_group_view_layout_horizontally_aligned (group, bp_view, g2_view, 0); 
  guppi_group_view_layout_same_width (group, g1_view, g2_view); 
  
  guppi_group_view_layout_flush_top (group, text_view, 0); 
  guppi_group_view_layout_vertically_adjacent (group, text_view, g1_view, 0); 
  guppi_group_view_layout_center_horizontally (group, text_view);

}

int
main (int argc, char *argv[])
{

  GuppiXMLDocument *doc;
  xmlNodePtr node;
  
  init (argc, argv);

  guppi_memory_trace (TRUE);

  g_message ("building data");
  build_data ();

  g_message ("building elements");
  build_elements ();

  g_message ("laying out elements");
  layout_elements ();

#if 0
  doc = guppi_xml_document_new ();
  guppi_xml_document_set_root (doc, guppi_element_view_export_xml (root_view, doc));
  guppi_xml_document_write_file (doc, "/tmp/guppi.foo");
  guppi_xml_document_free (doc);
  guppi_unref (root_view);

  doc = guppi_xml_document_read_file ("/tmp/guppi.foo");
  root_view = guppi_element_view_import_xml (doc, guppi_xml_document_get_root (doc));
  guppi_xml_document_free (doc);

  doc = guppi_xml_document_new ();
  guppi_xml_document_set_root (doc, guppi_element_view_export_xml (root_view, doc));
  guppi_xml_document_write_file (doc, "/tmp/guppi.bar");
  guppi_xml_document_free (doc);
#endif

  g_message ("attaching data");
  attach_data (root_view);

  g_message ("building gui");
  build_gui ();

  g_message ("printing");
  guppi_element_view_print_ps_to_file (root_view, "demo.ps");
  g_message ("done printing");
  
  timeout = gtk_timeout_add (1000, label_timeout, NULL);

  gtk_main ();

  return 0;
}




/* $Id$ */
