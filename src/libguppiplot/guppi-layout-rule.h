/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-layout-rule.h
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __GUPPI_LAYOUT_RULE_H__
#define __GUPPI_LAYOUT_RULE_H__

#include <glib.h>
#include  "guppi-xml.h"
#include "guppi-layout-constraint.h"
#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS

struct _GuppiLayoutRule;
typedef struct _GuppiLayoutRule GuppiLayoutRule;

typedef void (*GuppiLayoutRuleConstraintFn) (GuppiLayoutConstraint *cst, gpointer closure);

GuppiLayoutRule *guppi_layout_rule_new   (const gchar *name);
void             guppi_layout_rule_ref   (GuppiLayoutRule *rule);
void             guppi_layout_rule_unref (GuppiLayoutRule *rule);
const gchar     *guppi_layout_rule_name  (GuppiLayoutRule *rule);
void             guppi_layout_rule_lock  (GuppiLayoutRule *rule);

void                   guppi_layout_rule_add_constraint (GuppiLayoutRule *rule, GuppiLayoutConstraint *cst);
GuppiLayoutConstraint *guppi_layout_rule_new_constraint (GuppiLayoutRule *rule);

gint             guppi_layout_rule_constraint_count (GuppiLayoutRule *rule);

void             guppi_layout_rule_foreach (GuppiLayoutRule *rule,
					    GuppiLayoutRuleConstraintFn fn,
					    gpointer closure);

gboolean         guppi_layout_rule_contains (GuppiLayoutRule *rule, GuppiGeometry *geom);
gboolean         guppi_layout_rule_replace  (GuppiLayoutRule *rule, GuppiGeometry *old, GuppiGeometry *nuevo);

xmlNodePtr       guppi_layout_rule_export_xml (GuppiLayoutRule *rule, GuppiXMLDocument *doc);
GuppiLayoutRule *guppi_layout_rule_import_xml (GuppiXMLDocument *doc, xmlNodePtr node);

BEGIN_GUPPI_DECLS

#endif /* __GUPPI_LAYOUT_RULE_H__ */

