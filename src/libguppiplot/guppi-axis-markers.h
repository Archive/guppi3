/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-axis-markers.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_AXIS_MARKERS_H
#define _INC_GUPPI_AXIS_MARKERS_H

/* #include <gnome.h> */
#include <libgnomeprint/gnome-font.h>

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS 

enum {
  GUPPI_TICK_NONE,
  GUPPI_TICK_MAJOR,
  GUPPI_TICK_MINOR,
  GUPPI_TICK_MICRO,
  GUPPI_TICK_MAJOR_RULE,
  GUPPI_TICK_MINOR_RULE,
  GUPPI_TICK_MICRO_RULE
};

enum {
  GUPPI_AXIS_NONE=0,
  GUPPI_AXIS_SCALAR=1,
  GUPPI_AXIS_SCALAR_LOG2=2,
  GUPPI_AXIS_SCALAR_LOG10=3,
  GUPPI_AXIS_PERCENTAGE=4,
  GUPPI_AXIS_DATE=5,
  GUPPI_AXIS_XL_DATE=6, /* Accept the butchered MS Excel (tm) date serials */
  GUPPI_AXIS_LAST=7
};

typedef struct _GuppiTick GuppiTick;

struct _GuppiTick {
  double position;
  gint type;
  gchar *label;
  gboolean critical_label;
};

#define guppi_tick_position(x) ((x)->position)
#define guppi_tick_type(x) ((x)->type)
#define guppi_tick_has_label_only(x) ((x)->type == GUPPI_TICK_NONE)
#define guppi_tick_is_major(x) \
  ((x)->type == GUPPI_TICK_MAJOR || (x)->type == GUPPI_TICK_MAJOR_RULE)
#define guppi_tick_is_minor(x) \
  ((x)->type == GUPPI_TICK_MINOR || (x)->type == GUPPI_TICK_MINOR_RULE)
#define guppi_tick_is_micro(x) \
  ((x)->type == GUPPI_TICK_MICRO || (x)->type == GUPPI_TICK_MICRO_RULE)
#define guppi_tick_is_rule(x) \
  ((x)->type == GUPPI_TICK_MAJOR_RULE || (x)->type == GUPPI_TICK_MINOR_RULE ||\
   (x)->type == GUPPI_TICK_MICRO_RULE)
#define guppi_tick_is_labelled(x) ((x)->label != NULL)
#define guppi_tick_label(x) ((x)->label)

typedef struct _GuppiAxisMarkers GuppiAxisMarkers;
typedef struct _GuppiAxisMarkersClass GuppiAxisMarkersClass;

struct _GuppiAxisMarkers {
  GtkObject parent;

  gint N, pool;
  GuppiTick *ticks;

  gboolean sorted;

  gint freeze_count;
  gboolean pending;

  /* A hack to prevent multiple recalculation. */
  double pos_min, pos_max;
  gint goal, radix;
};

struct _GuppiAxisMarkersClass {
  GtkObjectClass parent_class;

  void (*changed) (GuppiAxisMarkers *);
};

#define GUPPI_TYPE_AXIS_MARKERS (guppi_axis_markers_get_type())
#define GUPPI_AXIS_MARKERS(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_AXIS_MARKERS,GuppiAxisMarkers))
#define GUPPI_AXIS_MARKERS0(obj) ((obj) ? (GUPPI_AXIS_MARKERS(obj)) : NULL)
#define GUPPI_AXIS_MARKERS_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_AXIS_MARKERS,GuppiAxisMarkersClass))
#define GUPPI_IS_AXIS_MARKERS(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_AXIS_MARKERS))
#define GUPPI_IS_AXIS_MARKERS0(obj) (((obj) == NULL) || (GUPPI_IS_AXIS_MARKERS(obj)))
#define GUPPI_IS_AXIS_MARKERS_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_AXIS_MARKERS))

GtkType guppi_axis_markers_get_type (void);

GuppiAxisMarkers *guppi_axis_markers_new (void);

void guppi_axis_markers_freeze (GuppiAxisMarkers *);
void guppi_axis_markers_thaw (GuppiAxisMarkers *);

gint guppi_axis_markers_size (GuppiAxisMarkers *);

const GuppiTick *guppi_axis_markers_get (GuppiAxisMarkers *, gint i);

void guppi_axis_markers_clear (GuppiAxisMarkers *);

void guppi_axis_markers_add (GuppiAxisMarkers *, double pos, gint type, const gchar *label);
void guppi_axis_markers_add_critical (GuppiAxisMarkers *, double pos, gint type, const gchar *label);
void guppi_axis_markers_sort (GuppiAxisMarkers *);

void guppi_axis_markers_populate_scalar (GuppiAxisMarkers *,
					 double min, double max,
					 gint goal, gint radix,
					 gboolean percentage);

void guppi_axis_markers_populate_scalar_log (GuppiAxisMarkers *,
					     double min, double max,
					     gint goal, double base);

void guppi_axis_markers_populate_dates (GuppiAxisMarkers *,
					GDate *min, GDate *max,
					gboolean do_excel_adjustment);

void guppi_axis_markers_populate_generic (GuppiAxisMarkers *,
					  gint type,
					  double min, double max);

void guppi_axis_markers_max_label_size (GuppiAxisMarkers *, GnomeFont *,
					gboolean consider_major,
					gboolean consider_minor,
					gboolean consider_micro,
					double *w, double *h);



END_GUPPI_DECLS

#endif /* _INC_GUPPI_AXIS_MARKERS_H */

/* $Id$ */
