/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-layout-constraint.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include "guppi-layout-constraint.h"

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <gnome-xml/xmlmemory.h>
#include <guppi-debug.h>
#include <guppi-memory.h>

typedef struct _TermInfo TermInfo;
struct _TermInfo {
  GuppiLayoutConstraintTermType type;
  double factor;
  GuppiGeometry *geom;
};

struct _GuppiLayoutConstraint {
  gint ref;
  GList *terms;
  gboolean locked;
};

static const gchar *term_type_names[GLC_LAST] = {
  "left", "right", "top", "bottom", "width", "height", "horizontal_center", "vertical_center",
  "region_left", "region_right", "region_top", "region_bottom", "region_width", "region_height",
  "region_horizontal_center", "region_vertical_center", "fixed"
};

static TermInfo *
term_info_new (GuppiLayoutConstraintTermType type, double factor, GuppiGeometry *geom)
{
  TermInfo *ti;

  g_return_val_if_fail (type >= GLC_REGION_LEFT ? geom == NULL : geom != NULL, NULL);
  g_return_val_if_fail (geom == NULL || GUPPI_IS_GEOMETRY (geom), NULL);

  ti = guppi_new (TermInfo, 1);
  ti->type = type;
  ti->factor = factor;
  ti->geom = geom;
  guppi_ref (ti->geom);

  return ti;
}

static void
term_info_free (TermInfo *ti)
{
  if (ti) {
    guppi_unref (ti->geom);
    guppi_free (ti);
  }
}

GuppiLayoutConstraint *
guppi_layout_constraint_new (void)
{
  GuppiLayoutConstraint *glc = guppi_new (GuppiLayoutConstraint, 1);
  glc->ref = 1;
  glc->terms = NULL;
  glc->locked = FALSE;
  
  return glc;
}

void
guppi_layout_constraint_ref (GuppiLayoutConstraint *glc)
{
  if (glc) {
    g_assert (glc->ref > 0);
    ++glc->ref;
  }
}

void
guppi_layout_constraint_unref (GuppiLayoutConstraint *glc)
{
  if (glc) {
    g_assert (glc->ref > 0);
    --glc->ref;

    if (glc->ref == 0) {
      g_list_foreach (glc->terms, (GFunc) term_info_free, NULL);
      g_list_free (glc->terms);
      glc->terms = NULL;

      guppi_free (glc);
    }

  }
}

void
guppi_layout_constraint_lock (GuppiLayoutConstraint *glc)
{
  g_return_if_fail (glc != NULL);

  glc->locked = TRUE;
}

void
guppi_layout_constraint_add_term (GuppiLayoutConstraint *glc,
				  GuppiLayoutConstraintTermType type,
				  double factor,
				  GuppiGeometry *geom)
{
  TermInfo *ti;
  g_return_if_fail (glc != NULL);
  g_assert (! glc->locked);

  if (fabs (factor) < 1e-12)
    return;

  switch (type) {
    
  case GLC_HORIZONTAL_CENTER:
    guppi_layout_constraint_add_term (glc, GLC_RIGHT, factor/2, geom);
    guppi_layout_constraint_add_term (glc, GLC_LEFT,  factor/2, geom);
    return;

  case GLC_VERTICAL_CENTER:
    guppi_layout_constraint_add_term (glc, GLC_TOP,    factor/2, geom);
    guppi_layout_constraint_add_term (glc, GLC_BOTTOM, factor/2, geom);
    return;

  case GLC_REGION_WIDTH:
    guppi_layout_constraint_add_term (glc, GLC_REGION_RIGHT,  factor, geom);
    guppi_layout_constraint_add_term (glc, GLC_REGION_LEFT,  -factor, geom);
    return;
    
  case GLC_REGION_HEIGHT:
    guppi_layout_constraint_add_term (glc, GLC_REGION_TOP,     factor, geom);
    guppi_layout_constraint_add_term (glc, GLC_REGION_BOTTOM, -factor, geom);
    return;

  default:
    /* Fall through, do nothing. */
  }

  ti = term_info_new (type, factor, geom);
  g_return_if_fail (ti != NULL);

  glc->terms = g_list_append (glc->terms, ti);
}

void
guppi_layout_constraint_add_terms (GuppiLayoutConstraint *glc, ...)
{
  va_list args;
  GuppiLayoutConstraintTermType type;
  double factor;
  GuppiGeometry *geom;

  va_start (args, glc);

  while ( (type = (GuppiLayoutConstraintTermType) va_arg (args, gint)) != GLC_LAST ) {
    factor = va_arg (args, double);
    geom = type >= GLC_REGION_LEFT ? NULL : va_arg (args, GuppiGeometry *);
    guppi_layout_constraint_add_term (glc, type, factor, geom);
  }
  
  va_end (args);
}

void
guppi_layout_constraint_foreach (GuppiLayoutConstraint *glc,
				 GuppiLayoutConstraintTermFn fn,
				 gpointer closure)
{
  GList *iter;

  g_return_if_fail (glc != NULL);
  g_return_if_fail (fn != NULL);

  for (iter = glc->terms; iter != NULL; iter = g_list_next (iter)) {
    TermInfo *ti = iter->data;
    fn (ti->type, ti->factor, ti->geom, closure);
  }
}

void
guppi_layout_constraint_foreach_with_region (GuppiLayoutConstraint *glc,
					     double x0, double y0, double x1, double y1,
					     GuppiLayoutConstraintTermFn fn,
					     gpointer closure)
{
  GList *iter;

  g_return_if_fail (glc != NULL);
  g_return_if_fail (fn != NULL);

  for (iter = glc->terms; iter != NULL; iter = g_list_next (iter)) {
    TermInfo *ti = iter->data;

    switch (ti->type) {

    case GLC_WIDTH:
      if (guppi_geometry_has_natural_width (ti->geom)) {

	double natural = guppi_geometry_get_natural_width (ti->geom);
	fn (GLC_FIXED, ti->factor * natural, NULL, closure);

      } else {

	fn (GLC_RIGHT,  ti->factor, ti->geom, closure);
	fn (GLC_LEFT,  -ti->factor, ti->geom, closure);

      }
      break;

    case GLC_HEIGHT:
      if (guppi_geometry_has_natural_height (ti->geom)) {

	double natural = guppi_geometry_get_natural_height (ti->geom);
	fn (GLC_FIXED, ti->factor * natural, NULL, closure);

      } else {

	fn (GLC_TOP,     ti->factor, ti->geom, closure);
	fn (GLC_BOTTOM, -ti->factor, ti->geom, closure);

      }
      break;

    case GLC_HORIZONTAL_CENTER:
      fn (GLC_RIGHT, ti->factor / 2, ti->geom, closure);
      fn (GLC_LEFT,  ti->factor / 2, ti->geom, closure);
      break;

    case GLC_VERTICAL_CENTER:
      fn (GLC_BOTTOM, ti->factor / 2, ti->geom, closure);
      fn (GLC_TOP,    ti->factor / 2, ti->geom, closure);
      break;

    case GLC_REGION_LEFT:
      fn (GLC_FIXED, x0 * ti->factor, NULL, closure);
      break;

    case GLC_REGION_RIGHT:
      fn (GLC_FIXED, x1 * ti->factor, NULL, closure);
      break;

    case GLC_REGION_BOTTOM:
      fn (GLC_FIXED, y0 * ti->factor, NULL, closure);
      break;
      
    case GLC_REGION_TOP:
      fn (GLC_FIXED, y1 * ti->factor, NULL, closure);
      break;

    case GLC_REGION_WIDTH:
      fn (GLC_FIXED, (x1-x0) * ti->factor, NULL, closure);
      break;

    case GLC_REGION_HEIGHT:
      fn (GLC_FIXED, (y1-y0) * ti->factor, NULL, closure);
      break;

    case GLC_REGION_HORIZONTAL_CENTER:
      fn (GLC_FIXED, (x0+x1)/2 * ti->factor, NULL, closure);
      break;

    case GLC_REGION_VERTICAL_CENTER:
      fn (GLC_FIXED, (y0+y1)/2 * ti->factor, NULL, closure);
      break;
   
    default:

      fn (ti->type, ti->factor, ti->geom, closure);
    }

  }
}

gboolean
guppi_layout_constraint_contains (GuppiLayoutConstraint *glc, GuppiGeometry *geom)
{
  GList *iter;

  g_return_val_if_fail (glc != NULL, FALSE);
  g_return_val_if_fail (geom && GUPPI_IS_GEOMETRY (geom), FALSE);

  for (iter = glc->terms; iter != NULL; iter = g_list_next (iter)) {
    TermInfo *term = iter->data;
    if (term->geom == geom)
      return TRUE;
  }
  return FALSE;
}

gboolean
guppi_layout_constraint_replace (GuppiLayoutConstraint *glc, GuppiGeometry *old, GuppiGeometry *nuevo)
{
  GList *iter;
  gboolean did_something = FALSE;

  g_return_val_if_fail (glc != NULL, FALSE);
  g_return_val_if_fail (old && GUPPI_IS_GEOMETRY (old), FALSE);
  g_return_val_if_fail (nuevo && GUPPI_IS_GEOMETRY (nuevo), FALSE);

  if (old == nuevo)
    return FALSE;

  for (iter = glc->terms; iter != NULL; iter = g_list_next (iter)) {
    TermInfo *term = iter->data;
    
    if (term->geom == old) {
      guppi_refcounting_assign (term->geom, nuevo);
      did_something = TRUE;
    }
  }
  
  return did_something;
}

static void
export_xml_cb (GuppiLayoutConstraintTermType type, double factor, GuppiGeometry *geom, gpointer closure)
{
  GuppiXMLDocumentWithNode *doc_with_node = closure;
  GuppiXMLDocument *doc = doc_with_node->doc;
  xmlNodePtr root_node = doc_with_node->node;
  xmlNodePtr term_node = xmlNewNode (root_node->ns, "Term");
  xmlNodePtr geom_node;
  gchar *str;

  xmlAddChild (root_node, term_node);

  str = guppi_strdup_printf ("%f", factor);
  xmlNewProp (term_node, "factor", str);
  guppi_free (str);

  xmlNewProp (term_node, "type", term_type_names[(gint) type]);

  if (geom != NULL) {
    geom_node = guppi_geometry_export_xml (geom, doc);
    xmlAddChild (term_node, geom_node);
  }
}

xmlNodePtr
guppi_layout_constraint_export_xml (GuppiLayoutConstraint *glc, GuppiXMLDocument *doc)
{
  GuppiXMLDocumentWithNode doc_with_node;
  xmlNodePtr root_node;

  g_return_val_if_fail (glc != NULL, NULL);
  g_return_val_if_fail (doc != NULL, NULL);

  root_node = xmlNewNode (doc->ns, "LayoutConstraint");

  doc_with_node.doc = doc;
  doc_with_node.node = root_node;
  
  guppi_layout_constraint_foreach (glc, export_xml_cb, &doc_with_node);
  
  return root_node;
}

GuppiLayoutConstraint *
guppi_layout_constraint_import_xml (GuppiXMLDocument *doc, xmlNodePtr node)
{
  xmlNodePtr subnode;
  GuppiLayoutConstraint *glc;
  gchar *s;

  g_return_val_if_fail (doc != NULL, NULL);
  g_return_val_if_fail (node != NULL, NULL);

  if (strcmp (node->name, "LayoutConstraint"))
    return NULL;

  glc = guppi_layout_constraint_new ();
  
  node = node->xmlChildrenNode;

  while (node != NULL) {

    if (!strcmp (node->name, "Term")) {
      GuppiLayoutConstraintTermType type;
      double factor;
      GuppiGeometry *geom = NULL;

      s = xmlGetProp (node, "type");
      for (type = 0; type < GLC_LAST && strcmp (term_type_names[type], s); ++type);
      xmlFree (s);

      s = xmlGetProp (node, "factor");
      factor = atof (s);
      xmlFree (s);

      subnode = node->xmlChildrenNode;
      if (subnode)
	geom = guppi_geometry_import_xml (doc, subnode);

      guppi_layout_constraint_add_term (glc, type, factor, geom);
    }

    node = node->next;
  }

  return glc;
}
