/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-color-gradient.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_COLOR_GRADIENT_H
#define _INC_GUPPI_COLOR_GRADIENT_H

/* #include <gtk/gtk.h> */

#include  "guppi-defs.h"
#include <gtk/gtkobject.h>

BEGIN_GUPPI_DECLS 

enum {
  GUPPI_COLOR_GRADIENT_NONE,
  GUPPI_COLOR_GRADIENT_TWO,
  GUPPI_COLOR_GRADIENT_FADE_IN,
  GUPPI_COLOR_GRADIENT_FADE_OUT,
  GUPPI_COLOR_GRADIENT_FIRE,
  GUPPI_COLOR_GRADIENT_ICE,
  GUPPI_COLOR_GRADIENT_THERMAL,
  GUPPI_COLOR_GRADIENT_SPECTRUM,
  GUPPI_COLOR_GRADIENT_CUSTOM,
  GUPPI_COLOR_GRADIENT_LAST
};

typedef struct _GuppiColorGradient GuppiColorGradient;
typedef struct _GuppiColorGradientClass GuppiColorGradientClass;

struct _GuppiColorGradient {
  GtkObject parent;

  gint N;
  guint32 *nodes;

  gint type;
  double v, a;
  gint freeze_count;
  gboolean pending_change;
};

struct _GuppiColorGradientClass {
  GtkObjectClass parent_class;

  void (*changed) (GuppiColorGradient *);
};

#define GUPPI_TYPE_COLOR_GRADIENT (guppi_color_gradient_get_type())
#define GUPPI_COLOR_GRADIENT(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_COLOR_GRADIENT,GuppiColorGradient))
#define GUPPI_COLOR_GRADIENT0(obj) ((obj) ? (GUPPI_COLOR_GRADIENT(obj)) : NULL)
#define GUPPI_COLOR_GRADIENT_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_COLOR_GRADIENT,GuppiColorGradientClass))
#define GUPPI_IS_COLOR_GRADIENT(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_COLOR_GRADIENT))
#define GUPPI_IS_COLOR_GRADIENT0(obj) (((obj) == NULL) || (GUPPI_IS_COLOR_GRADIENT(obj)))
#define GUPPI_IS_COLOR_GRADIENT_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_COLOR_GRADIENT))

GtkType guppi_color_gradient_get_type (void);

GuppiColorGradient *guppi_color_gradient_new (void);

guint32 guppi_color_gradient_value (const GuppiColorGradient *, double t);
#define guppi_color_gradient_size(x) ((x)->N)
guint32 guppi_color_gradient_get_node (const GuppiColorGradient *, gint i);
#define guppi_color_gradient_type(x) ((x)->type)
#define guppi_color_gradient_intensity(x) ((x)->v)
#define guppi_color_gradient_alpha(x) ((x)->a)


void guppi_color_gradient_reset (GuppiColorGradient *);

void guppi_color_gradient_set (GuppiColorGradient *, const guint32 *, gint N);
void guppi_color_gradient_set2 (GuppiColorGradient *, guint32, guint32);

void guppi_color_gradient_set_fade_in (GuppiColorGradient *, guint32);
void guppi_color_gradient_set_fade_out (GuppiColorGradient *, guint32);
void guppi_color_gradient_set_fire (GuppiColorGradient *, double v, double a);
void guppi_color_gradient_set_ice (GuppiColorGradient *, double v, double a);
void guppi_color_gradient_set_thermal (GuppiColorGradient *, double v,
				       double a);
void guppi_color_gradient_set_spectrum (GuppiColorGradient *, double v,
					double a);

void guppi_color_gradient_set_intensity (GuppiColorGradient *, double v);
void guppi_color_gradient_set_alpha (GuppiColorGradient *, double v);

void guppi_color_gradient_freeze (GuppiColorGradient *);
void guppi_color_gradient_thaw (GuppiColorGradient *);


END_GUPPI_DECLS

#endif /* _INC_GUPPI_COLOR_GRADIENT_H */

/* $Id$ */
