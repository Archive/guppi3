
/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-axis-markers.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-axis-markers.h"

#include <stdlib.h>
#include <math.h>
#include <string.h>

#include <gtk/gtksignal.h>

#include <guppi-convenient.h>

static GtkObjectClass *parent_class = NULL;

enum {
  CHANGED,
  LAST_SIGNAL
};

static guint gam_signals[LAST_SIGNAL] = { 0 };

static void
guppi_axis_markers_finalize (GtkObject * obj)
{
  static void clear (GuppiAxisMarkers *);
  GuppiAxisMarkers *gal = GUPPI_AXIS_MARKERS (obj);

  guppi_finalized (obj);

  clear (gal);
  guppi_free (gal->ticks);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_axis_markers_class_init (GuppiAxisMarkersClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;

  parent_class = gtk_type_class (GTK_TYPE_OBJECT);

  object_class->finalize = guppi_axis_markers_finalize;

  gam_signals[CHANGED] =
    gtk_signal_new ("changed",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GuppiAxisMarkersClass, changed),
		    gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);
  gtk_object_class_add_signals (object_class, gam_signals, LAST_SIGNAL);

}

static void
guppi_axis_markers_init (GuppiAxisMarkers * obj)
{

}

GtkType guppi_axis_markers_get_type (void)
{
  static GtkType guppi_axis_markers_type = 0;
  if (!guppi_axis_markers_type) {
    static const GtkTypeInfo guppi_axis_markers_info = {
      "GuppiAxisMarkers",
      sizeof (GuppiAxisMarkers),
      sizeof (GuppiAxisMarkersClass),
      (GtkClassInitFunc) guppi_axis_markers_class_init,
      (GtkObjectInitFunc) guppi_axis_markers_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_axis_markers_type =
      gtk_type_unique (GTK_TYPE_OBJECT, &guppi_axis_markers_info);
  }
  return guppi_axis_markers_type;
}

GuppiAxisMarkers *
guppi_axis_markers_new (void)
{
  return GUPPI_AXIS_MARKERS (guppi_type_new (guppi_axis_markers_get_type ()));
}

/**************************************************************************/

static void
changed (GuppiAxisMarkers * gam)
{
  g_return_if_fail (gam != NULL);

  if (gam->freeze_count)
    gam->pending = TRUE;
  else
    gtk_signal_emit (GTK_OBJECT (gam), gam_signals[CHANGED]);
}

static void
clear (GuppiAxisMarkers * gam)
{
  gint i;

  g_return_if_fail (gam != NULL);

  for (i = 0; i < gam->N; ++i) {
    guppi_free (gam->ticks[i].label);
    gam->ticks[i].label = NULL;
  }
  gam->N = 0;
}

void
guppi_axis_markers_freeze (GuppiAxisMarkers * gam)
{
  g_return_if_fail (gam != NULL);
  ++gam->freeze_count;
}

void
guppi_axis_markers_thaw (GuppiAxisMarkers * gam)
{
  g_return_if_fail (gam != NULL);
  g_return_if_fail (gam->freeze_count > 0);
  --gam->freeze_count;
  if (gam->freeze_count == 0 && gam->pending) {
    changed (gam);
    gam->pending = FALSE;
  }
}

gint guppi_axis_markers_size (GuppiAxisMarkers * gal)
{
  g_return_val_if_fail (gal != NULL, 0);

  return gal->N;
}

const GuppiTick *
guppi_axis_markers_get (GuppiAxisMarkers * gal, gint i)
{
  g_return_val_if_fail (gal != NULL, NULL);
  g_return_val_if_fail (i >= 0, NULL);
  g_return_val_if_fail (i < gal->N, NULL);

  return &gal->ticks[i];
}

void
guppi_axis_markers_clear (GuppiAxisMarkers * gam)
{
  g_return_if_fail (gam != NULL);
  clear (gam);
  changed (gam);
}

void
guppi_axis_markers_add (GuppiAxisMarkers * gam,
			double pos, gint type, const gchar *label)
{
  g_return_if_fail (gam != NULL);

  if (gam->N == gam->pool) {
    gint new_size = MAX (2 * gam->pool, 32);
    GuppiTick *tmp = guppi_new0 (GuppiTick, new_size);
    if (gam->ticks)
      memcpy (tmp, gam->ticks, sizeof (GuppiTick) * gam->N);
    guppi_free (gam->ticks);
    gam->ticks = tmp;
    gam->pool = new_size;
  }

  gam->ticks[gam->N].position = pos;
  gam->ticks[gam->N].type = type;
  gam->ticks[gam->N].label = guppi_strdup (label);

  ++gam->N;

  gam->sorted = FALSE;

  changed (gam);
}

/* Stupid copy & modify */
void
guppi_axis_markers_add_critical (GuppiAxisMarkers * gam,
				 double pos, gint type, const gchar *label)
{
  g_return_if_fail (gam != NULL);

  if (gam->N == gam->pool) {
    gint new_size = MAX (2 * gam->pool, 32);
    GuppiTick *tmp = guppi_new0 (GuppiTick, new_size);
    if (gam->ticks)
      memcpy (tmp, gam->ticks, sizeof (GuppiTick) * gam->N);
    guppi_free (gam->ticks);
    gam->ticks = tmp;
    gam->pool = new_size;
  }

  gam->ticks[gam->N].position = pos;
  gam->ticks[gam->N].type = type;
  gam->ticks[gam->N].label = guppi_strdup (label);
  gam->ticks[gam->N].critical_label = TRUE;

  ++gam->N;

  gam->sorted = FALSE;

  changed (gam);
}

static gint
guppi_tick_compare (const void *a, const void *b)
{
  const GuppiTick *ta = a;
  const GuppiTick *tb = b;
  return (ta->position > tb->position) - (ta->position < tb->position);
}


void
guppi_axis_markers_sort (GuppiAxisMarkers *gam)
{
  g_return_if_fail (GUPPI_IS_AXIS_MARKERS (gam));

  if (gam->sorted)
    return;

  qsort (gam->ticks, gam->N, sizeof (GuppiTick), guppi_tick_compare);

  gam->sorted = TRUE;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static const double base4_divisors[] = { 4, 2, 1, -1 };
static const double base8_divisors[] = { 8, 4, 2, 1, -1 };
static const double base10_divisors[] = { 10, 5, 4, 2, 1, -1 };
static const double base16_divisors[] = { 16, 8, 4, 2, 1, -1 };
static const double base32_divisors[] = { 32, 16, 8, 4, 2, 1, -1 };
static const double base64_divisors[] = { 64, 32, 16, 8, 4, 2, 1, -1 };

void
guppi_axis_markers_populate_scalar (GuppiAxisMarkers * gam,
				    double pos_min, double pos_max,
				    gint goal, gint radix,
				    gboolean percentage)
{
  double width, mag, div, step, start, count, t;
  double delta_best = 1e+8, step_best = 0, start_best = 0;
  gint i, count_best = 0;
  const double *divisors = NULL;
  gchar labelbuf[64];

  g_return_if_fail (gam != NULL);
  g_return_if_fail (goal > 1);

  /* Avoid redundant recalculations. */
  if (gam->N &&
      pos_min == gam->pos_min &&
      pos_max == gam->pos_max && goal == gam->goal && radix == gam->radix)
    return;

  gam->pos_min = pos_min;
  gam->pos_max = pos_max;
  gam->goal = goal;
  gam->radix = radix;

  guppi_axis_markers_freeze (gam);

  guppi_axis_markers_clear (gam);

  if (fabs (pos_min - pos_max) < 1e-10) {
    guppi_axis_markers_thaw (gam);
    return;
  }

  if (pos_min > pos_max) {
    t = pos_min;
    pos_min = pos_max;
    pos_max = t;
  }

  width = fabs (pos_max - pos_min);
  mag = ceil (log (width / goal) / log (radix));

  switch (radix) {
  case 4:
    divisors = base4_divisors;
    break;
  case 8:
    divisors = base8_divisors;
    break;
  case 10:
    divisors = base10_divisors;
    break;
  case 16:
    divisors = base16_divisors;
    break;
  case 32:
    divisors = base32_divisors;
    break;
  case 64:
    divisors = base64_divisors;
    break;
  default:
    g_assert_not_reached ();
  }

  g_assert (divisors != NULL);

  for (i = 0; divisors[i] > 0; ++i) {
    div = divisors[i];
    step = pow (radix, mag) / div;
    start = ceil (pos_min / step) * step;
    count = floor (width / step);
    if (pos_min <= start && start <= pos_max)
      ++count;

    if (fabs (count - goal) < delta_best) {
      delta_best = fabs (count - goal);
      step_best = step;
      start_best = start;
      count_best = (gint) count;
    }
  }

  if (step_best <= 0) {
    g_error ("Search for nice axis points failed.  This shouldn't happen.");
  }

  for (i = -1; i <= count_best; ++i) {
    double x;
    t = start_best + i * step_best;
    if (fabs (t / step_best) < 1e-12)
      t = 0;

    if (percentage) {
      g_snprintf (labelbuf, 64, "%g%%", t * 100);
    } else {
      g_snprintf (labelbuf, 64, "%g", t);
    }

    if (pos_min <= t && t <= pos_max) {
      guppi_axis_markers_add (gam, t, GUPPI_TICK_MAJOR, labelbuf);
      guppi_axis_markers_add (gam, t,
			      t ==
			      0 ? GUPPI_TICK_MAJOR_RULE :
			      GUPPI_TICK_MINOR_RULE, NULL);
    }

    /* Add some minor/micro ticks & rules just for fun... */
    x = t + step_best / 4;
    if (pos_min <= x && x <= pos_max)
      guppi_axis_markers_add (gam, x, GUPPI_TICK_MICRO, NULL);

    x = t + step_best / 2;
    if (pos_min <= x && x <= pos_max) {
      guppi_axis_markers_add (gam, x, GUPPI_TICK_MINOR, NULL);
      guppi_axis_markers_add (gam, x, GUPPI_TICK_MICRO_RULE, NULL);
    }

    x = t + 3 * step_best / 4;
    if (pos_min <= x && x <= pos_max)
      guppi_axis_markers_add (gam, x, GUPPI_TICK_MICRO, NULL);

  }

  guppi_axis_markers_thaw (gam);
}

void
guppi_axis_markers_populate_scalar_log (GuppiAxisMarkers * gam,
					double min, double max,
					gint goal, double base)
{
  double minexp, maxexp;
  gint g, i, botexp, topexp, expstep, count = 0;
  gchar labelbuf[64];

  g_return_if_fail (gam != NULL);
  g_return_if_fail (GUPPI_IS_AXIS_MARKERS (gam));
  g_return_if_fail (min < max);
  g_return_if_fail (goal > 0);
  g_return_if_fail (base > 0);

  if (max / min < base) {
    guppi_axis_markers_populate_scalar (gam, min, max, goal, base, FALSE);
    return;
  }

  guppi_axis_markers_freeze (gam);
  guppi_axis_markers_clear (gam);

  minexp = log (min) / log (base);
  maxexp = log (max) / log (base);

  botexp = floor (minexp);
  topexp = ceil (maxexp);

  expstep = 0;
  g = goal;
  while (g > 0 && expstep == 0) {
    expstep = (gint) rint ((maxexp - minexp) / g);
    --g;
  }

  if (expstep == 0)
    expstep = 1;

  if (expstep) {
    for (i = topexp; i >= botexp - 2; i -= expstep) {
      double t = pow (base, i);
      double ta = pow (base, i + expstep);
      double x;

      if (min <= t && t <= max) {
	g_snprintf (labelbuf, 64, "%g", t);
	guppi_axis_markers_add (gam, t, GUPPI_TICK_MAJOR, labelbuf);
	guppi_axis_markers_add (gam, t, GUPPI_TICK_MINOR_RULE, NULL);
	++count;
      }

      x = (ta + t) / 2;
      if (min <= x && x <= max) {
	guppi_axis_markers_add (gam, x, GUPPI_TICK_MINOR, NULL);
	guppi_axis_markers_add (gam, x, GUPPI_TICK_MICRO_RULE, NULL);
      }

      x = ta / 4 + 3 * t / 4;
      if (min <= x && x <= max)
	guppi_axis_markers_add (gam, x, GUPPI_TICK_MICRO, NULL);

      x = 3 * ta / 4 + t / 4;
      if (min <= x && x <= max)
	guppi_axis_markers_add (gam, x, GUPPI_TICK_MICRO, NULL);
    }
  }

  if (count < 2) {
    guppi_axis_markers_populate_scalar (gam, min, max,
					goal > 4 ? goal - 2 : 3, 10, FALSE);
  }

  guppi_axis_markers_thaw (gam);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

/* One less that the Julian day number of 19000101.  */
static int excel_date_origin = 0;

/*
 * The serial number of 19000228.  Excel allocates a serial number for
 * the non-existing date 19000229.
 */
static const int excel_date_serial_19000228 = 59;

static void
excel_date_init (void)
{
  /* Day 1 means 1st of January of 1900 */
  GDate date;
  g_date_set_dmy (&date, 1, 1, 1900);
  excel_date_origin = g_date_julian (&date) - 1;
}

static gboolean
guppi_date_set_excel (GDate *dst, int serial)
{
  if (excel_date_origin == 0)
    excel_date_init ();

  if (serial > excel_date_serial_19000228) {
    if (serial == excel_date_serial_19000228 + 1)
      g_warning ("Request for date 19000229.");
    serial--;
  }
  serial += excel_date_origin;
  if (! g_date_valid_julian (serial))
    return FALSE;
  g_date_set_julian (dst, serial);
  return TRUE;
}

static void
guppi_axis_markers_adjust_excel (GuppiAxisMarkers * gam)
{
  int i;
  double julian;
  g_return_if_fail (gam != NULL);

  if (!excel_date_origin)
    excel_date_init ();

  for (i = 0 ; i < gam->N ; ++i) {
    julian = gam->ticks[i].position - excel_date_origin;
    gam->ticks[i].position = julian + (julian > excel_date_serial_19000228 ? 1 : 0);
  }

  changed (gam);
}

static void
populate_dates_daily (GuppiAxisMarkers *gam,
		      GDate *min, GDate *max)
{
  gchar buf[32];
  GDate dt = *min;

  while (g_date_lteq (&dt, max)) {

    g_date_strftime (buf, 32, "%d %b %y", &dt);

    guppi_axis_markers_add (gam, g_date_julian (&dt), GUPPI_TICK_MAJOR, buf);

    g_date_add_days (&dt, 1);
  }
}

static void
populate_dates_weekly (GuppiAxisMarkers *gam,
		       GDate *min, GDate *max)
{
  gchar buf[32];
  GDate dt = *min;

  while (g_date_weekday (&dt) != G_DATE_MONDAY)
    g_date_add_days (&dt, 1);

  while (g_date_lteq (&dt, max)) {

    if (g_date_weekday (&dt) == G_DATE_MONDAY) {
      g_date_strftime (buf, 32, "%d %b %y", &dt);
      guppi_axis_markers_add (gam, g_date_julian (&dt), GUPPI_TICK_MAJOR, buf);
    } else {
      guppi_axis_markers_add (gam, g_date_julian (&dt), GUPPI_TICK_MICRO, "");
    }

    g_date_add_days (&dt, 1);
  }

}

static void
populate_dates_monthly (GuppiAxisMarkers *gam,
			GDate *min, GDate *max)
{
  gchar buf[32];
  GDate dt, dt2;
  gint j,j2;

  g_date_set_dmy (&dt, 1, g_date_month (min), g_date_year (min));

  while (g_date_lteq (&dt, max)) {
    dt2 = dt;
    g_date_add_months (&dt2, 1);
    j = g_date_julian (&dt);
    j2 = g_date_julian (&dt2);

    g_date_strftime (buf, 32, "%b-%y", &dt);
    
    guppi_axis_markers_add (gam, j, GUPPI_TICK_MAJOR, "");
    guppi_axis_markers_add (gam, (j+j2)/2.0, GUPPI_TICK_NONE, buf);
    
    dt = dt2;
  }
}

static void
populate_dates_quarterly (GuppiAxisMarkers *gam,
			  GDate *min, GDate *max)
{
  gchar monthname[32], buf[32];
  GDate dt, dt2;
  gint j, j2;

  g_date_set_dmy (&dt, 1, g_date_month (min), g_date_year (min));

  while (g_date_lteq (&dt, max)) {

    dt2 = dt;
    g_date_add_months (&dt2, 1);
    j = g_date_julian (&dt);
    j2 = g_date_julian (&dt2);

    g_date_strftime (monthname, 32, "%b", &dt);
    g_snprintf (buf, 32, "%c-%02d", *monthname, g_date_year (&dt) % 100);

    if (g_date_month (&dt) % 3 == 1)
      guppi_axis_markers_add (gam, j, GUPPI_TICK_MAJOR, "");
    else
      guppi_axis_markers_add (gam, j, GUPPI_TICK_MINOR, "");

    guppi_axis_markers_add (gam, (j+j2)/2.0, GUPPI_TICK_NONE, buf);

    dt = dt2;
  }

}

static void
populate_dates_yearly (GuppiAxisMarkers *gam,
		       GDate *min, GDate *max)
{
  gchar buf[32];
  GDate dt, dt2;
  gint j, j2, y;
  gint count = 0, step = 1;
  gboolean two_digit_years;

  g_date_set_dmy (&dt, 1, 1, g_date_year (min));
  while (g_date_lteq (&dt, max)) {
    g_date_add_years (&dt, 1);
    ++count;
  }

  two_digit_years = count > 5;
  if (count > 10)
    step = 2;
  if (count > 20)
    step = 5;

  g_date_set_dmy (&dt, 1, 1, g_date_year (min));
  while (g_date_lteq (&dt, max)) {
    dt2 = dt;
    g_date_add_years (&dt2, 1);
    j = g_date_julian (&dt);
    j2 = g_date_julian (&dt2);
    y = g_date_year (&dt);

    if (two_digit_years) {
      if (step == 1 || y % step == 0)
	g_snprintf (buf, 32, "%02d", y % 100);
      else
	*buf = '\0';
    } else {
      g_snprintf (buf, 32, "%d", y);
    }
    
    guppi_axis_markers_add (gam, j, GUPPI_TICK_MAJOR, "");
    if (*buf)
      guppi_axis_markers_add (gam, (j+j2)/2.0, GUPPI_TICK_NONE, buf);

    if (step == 1) {
      guppi_axis_markers_add (gam, j+0.25*(j2-j), GUPPI_TICK_MICRO, "");
      guppi_axis_markers_add (gam, (j+j2)/2.0, GUPPI_TICK_MICRO, "");
      guppi_axis_markers_add (gam, j+0.75*(j2-j), GUPPI_TICK_MICRO, "");
    }
    
    dt = dt2;
  }
}

void
guppi_axis_markers_populate_dates (GuppiAxisMarkers *gam,
				   GDate *min, GDate *max,
				   gboolean do_excel_adjustment)
{
  gint jspan;

  g_return_if_fail (gam && GUPPI_IS_AXIS_MARKERS (gam));
  g_return_if_fail (min && g_date_valid (min));
  g_return_if_fail (max && g_date_valid (max));

  jspan = g_date_julian (max) - g_date_julian (min);

  guppi_axis_markers_freeze (gam);
  guppi_axis_markers_clear (gam);

  if (jspan < 2*7)
    populate_dates_daily (gam, min, max);
  else if (jspan < 8*7)
    populate_dates_weekly (gam, min, max);
  else if (jspan < 8*30)
    populate_dates_monthly (gam, min, max);
  else if (jspan < 6*90)
    populate_dates_quarterly (gam, min, max); 
  else
    populate_dates_yearly (gam, min, max);

  if (do_excel_adjustment)
    guppi_axis_markers_adjust_excel (gam);
  guppi_axis_markers_thaw (gam);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

void
guppi_axis_markers_populate_generic (GuppiAxisMarkers *gam,
				     gint type,
				     double a, double b)
{
  g_return_if_fail (gam && GUPPI_IS_AXIS_MARKERS (gam));

  guppi_2sort (&a, &b);

  switch (type) {
    
  case GUPPI_AXIS_SCALAR: 
    guppi_axis_markers_populate_scalar (gam, a, b, 6, 10, FALSE);
    break;
					
  case GUPPI_AXIS_SCALAR_LOG2: 
    guppi_axis_markers_populate_scalar_log (gam, a, b, 6, 2.0);
    break;

  case GUPPI_AXIS_SCALAR_LOG10: 
    guppi_axis_markers_populate_scalar_log (gam, a, b, 6, 10);
    break;
    
  case GUPPI_AXIS_PERCENTAGE: 
    guppi_axis_markers_populate_scalar (gam, a, b, 6, 10, TRUE);
    break;

  case GUPPI_AXIS_DATE: 
    {
      gint ja, jb;
      GDate dt_a, dt_b;

      ja = (gint)floor (a + DBL_EPSILON);
      jb = (gint)ceil (b - DBL_EPSILON);

      if (ja <= 0 || jb <= 0)
	return;

      if (! (g_date_valid_julian (ja) && g_date_valid_julian (jb)))
	return;

      g_date_set_julian (&dt_a, ja);
      g_date_set_julian (&dt_b, jb);
            
      guppi_axis_markers_populate_dates (gam, &dt_a, &dt_b, FALSE);
    }
    break;

  case GUPPI_AXIS_XL_DATE: 
    {
      GDate dt_a, dt_b;
      gint ja = (gint)floor (a + DBL_EPSILON);
      gint jb = (gint)ceil (b - DBL_EPSILON);

      if (!guppi_date_set_excel (&dt_a, ja) || !guppi_date_set_excel (&dt_b, jb))
	return;
      guppi_axis_markers_populate_dates (gam, &dt_a, &dt_b, TRUE);
    }
    break;

  default:
    g_assert_not_reached ();

  }
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

void
guppi_axis_markers_max_label_size (GuppiAxisMarkers * gam, GnomeFont * f,
				   gboolean consider_major,
				   gboolean consider_minor,
				   gboolean consider_micro,
				   double *max_w, double *max_h)
{
  gint i;
  const GuppiTick *tick;

  g_return_if_fail (gam != NULL);
  g_return_if_fail (f != NULL);

  if (max_w == NULL && max_h == NULL)
    return;

  /* Font height is independent of the string */
  if (max_h)
    *max_h = gnome_font_get_ascender (f) + gnome_font_get_descender (f);


  if (max_w) {
    *max_w = 0;

    for (i = 0; i < guppi_axis_markers_size (gam); ++i) {

      tick = guppi_axis_markers_get (gam, i);

      if (guppi_tick_is_labelled (tick) &&
	  ((consider_major && guppi_tick_is_major (tick)) ||
	   (consider_minor && guppi_tick_is_minor (tick)) ||
	   (consider_micro && guppi_tick_is_micro (tick)))) {

	if (max_w) {
	  double w = gnome_font_get_width_string (f, guppi_tick_label (tick));
	  *max_w = MAX (*max_w, w);
	}
      }
    }
  }
}


/* $Id$ */
