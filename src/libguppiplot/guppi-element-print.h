/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-element-print.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_ELEMENT_PRINT_H
#define _INC_GUPPI_ELEMENT_PRINT_H

/* #include <gnome.h> */
#include <libart_lgpl/libart.h>
#include <libgnomeprint/gnome-print.h>
#include "guppi-element-view.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS 

typedef struct _GuppiElementPrint GuppiElementPrint;
typedef struct _GuppiElementPrintClass GuppiElementPrintClass;

struct _GuppiElementPrint {
  GtkObject parent;

  GuppiElementView *view;
  GnomePrintContext *context;

  double pt_x0, pt_y0, pt_x1, pt_y1;
};

struct _GuppiElementPrintClass {
  GtkObjectClass parent_class;

  void (*print) (GuppiElementPrint *);
};

#define GUPPI_TYPE_ELEMENT_PRINT (guppi_element_print_get_type())
#define GUPPI_ELEMENT_PRINT(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_ELEMENT_PRINT,GuppiElementPrint))
#define GUPPI_ELEMENT_PRINT0(obj) ((obj) ? (GUPPI_ELEMENT_PRINT(obj)) : NULL)
#define GUPPI_ELEMENT_PRINT_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_ELEMENT_PRINT,GuppiElementPrintClass))
#define GUPPI_IS_ELEMENT_PRINT(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_ELEMENT_PRINT))
#define GUPPI_IS_ELEMENT_PRINT0(obj) (((obj) == NULL) || (GUPPI_IS_ELEMENT_PRINT(obj)))
#define GUPPI_IS_ELEMENT_PRINT_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_ELEMENT_PRINT))

GtkType guppi_element_print_get_type (void);

void guppi_element_print_set_context (GuppiElementPrint *,
				      GnomePrintContext *);
void guppi_element_print_set_bbox (GuppiElementPrint *, double x0, double y0,
				   double x1, double y1);
void guppi_element_print_set_bbox_corner (GuppiElementPrint *, double x,
					  double y);

void guppi_element_print_get_bbox (GuppiElementPrint *,
				   double *x0, double *y0,
				   double *x1, double *y1);

void guppi_element_print_get_bbox_vp (GuppiElementPrint *,
				      double *x0, double *y0,
				      double *x1, double *y1);

void guppi_element_print_print (GuppiElementPrint *);

#define guppi_element_print_view(x) ((x)->view)
#define guppi_element_print_state(x) (guppi_element_view_state((x)->view))
#define guppi_element_print_context(x) ((x)->context)

void guppi_element_print_vp2pt (GuppiElementPrint *,
				double x, double y, double *pt_x,
				double *pt_y);

/* Convert coordinates "in place" */
void guppi_element_print_vp2pt_auto (GuppiElementPrint *, double *x,
				     double *y);



/* A bunch of convenience functions. */

gint guppi_element_print_newpath (GuppiElementPrint *);

gint guppi_element_print_moveto (GuppiElementPrint *, double x, double y);
gint guppi_element_print_moveto_vp (GuppiElementPrint *, double x, double y);

gint guppi_element_print_lineto (GuppiElementPrint *, double x, double y);
gint guppi_element_print_lineto_vp (GuppiElementPrint *, double x, double y);

gint guppi_element_print_curveto (GuppiElementPrint *,
				  double x1, double y1,
				  double x2, double y2, double x3, double y3);
gint guppi_element_print_curveto_vp (GuppiElementPrint *,
				     double x1, double y1,
				     double x2, double y2,
				     double x3, double y3);

gint guppi_element_print_closepath (GuppiElementPrint *);

gint guppi_element_print_setrgbcolor (GuppiElementPrint *,
				      double r, double g, double b);
gint guppi_element_print_setrgbcolor_uint (GuppiElementPrint *,
					   guint32 color);

gint guppi_element_print_setrgbacolor (GuppiElementPrint *,
				       double r, double g, double b,
				       double a);
gint guppi_element_print_setrgbacolor_uint (GuppiElementPrint *,
					    guint32 color);

gint guppi_element_print_fill (GuppiElementPrint *);
gint guppi_element_print_eofill (GuppiElementPrint *);

gint guppi_element_print_setlinewidth (GuppiElementPrint *, double);
gint guppi_element_print_setmiterlimit (GuppiElementPrint *, double);
gint guppi_element_print_setlinejoin (GuppiElementPrint *, gint);
gint guppi_element_print_setlinecap (GuppiElementPrint *, gint);
gint guppi_element_print_setdash (GuppiElementPrint *, gint, double *,
				  double);

gint guppi_element_print_strokepath (GuppiElementPrint *);
gint guppi_element_print_stroke (GuppiElementPrint *);

gint guppi_element_print_setfont (GuppiElementPrint *, GnomeFont *);
gint guppi_element_print_show (GuppiElementPrint *, const gchar * text);

gint guppi_element_print_concat (GuppiElementPrint *, const double matrix[6]);

gint guppi_element_print_gsave (GuppiElementPrint *);
gint guppi_element_print_grestore (GuppiElementPrint *);

gint guppi_element_print_clip (GuppiElementPrint *);
gint guppi_element_print_eoclip (GuppiElementPrint *);

/* skip greyimage, rgbimage, rgbaimage for now */
/* skip textline for now */

gint guppi_element_print_showpage (GuppiElementPrint *);
gint guppi_element_print_beginpage (GuppiElementPrint *,
				    const gchar * page_name);

gint guppi_element_print_setopacity (GuppiElementPrint *, double);

void guppi_element_print_vpath (GuppiElementPrint *, ArtVpath *,
				gboolean append);
void guppi_element_print_vpath_vp (GuppiElementPrint *, ArtVpath *,
				   gboolean app);


void guppi_element_print_bpath (GuppiElementPrint *, ArtBpath *,
				gboolean append);
void guppi_element_print_bpath_vp (GuppiElementPrint *, ArtBpath *,
				   gboolean app);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_ELEMENT_PRINT_H */

/* $Id$ */
