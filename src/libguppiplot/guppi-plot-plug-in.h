/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-plot-plug-in.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_PLOT_PLUG_IN_H
#define _INC_GUPPI_PLOT_PLUG_IN_H

/* #include <gnome.h> */

#include  "guppi-plug-in.h"
#include "guppi-element-state.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS 

typedef struct _GuppiPlotPlugIn GuppiPlotPlugIn;
typedef struct _GuppiPlotPlugInClass GuppiPlotPlugInClass;

struct _GuppiPlotPlugIn {
  GuppiPlugIn parent;

  GuppiElementState *(*element_constructor) (void);
};

struct _GuppiPlotPlugInClass {
  GuppiPlugInClass parent_class;
};

#define GUPPI_TYPE_PLOT_PLUG_IN (guppi_plot_plug_in_get_type())
#define GUPPI_PLOT_PLUG_IN(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_PLOT_PLUG_IN,GuppiPlotPlugIn))
#define GUPPI_PLOT_PLUG_IN0(obj) ((obj) ? (GUPPI_PLOT_PLUG_IN(obj)) : NULL)
#define GUPPI_PLOT_PLUG_IN_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_PLOT_PLUG_IN,GuppiPlotPlugInClass))
#define GUPPI_IS_PLOT_PLUG_IN(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_PLOT_PLUG_IN))
#define GUPPI_IS_PLOT_PLUG_IN0(obj) (((obj) == NULL) || (GUPPI_IS_PLOT_PLUG_IN(obj)))
#define GUPPI_IS_PLOT_PLUG_IN_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_PLOT_PLUG_IN))

GtkType guppi_plot_plug_in_get_type (void);

GuppiPlugIn *guppi_plot_plug_in_new (void);



END_GUPPI_DECLS

#endif /* _INC_GUPPI_PLOT_PLUG_IN_H */

/* $Id$ */
