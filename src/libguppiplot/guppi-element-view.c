/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-element-view.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-element-view.h"

#include <math.h>
#include <string.h>
#include <glade/glade.h>
#include <libart_lgpl/libart.h>
#include <gnome-xml/xmlmemory.h>

#include <guppi-debug.h>
#include <guppi-i18n.h>
#include <guppi-convenient.h>
#include <guppi-memory.h>
#include <guppi-attribute-bag.h>
#include <guppi-metrics.h>
#include <guppi-paths.h>
#include <guppi-unique-id.h>
#include "guppi-canvas-item.h"
#include "guppi-element-print.h"

typedef struct _ViewAxisPair ViewAxisPair;
struct _ViewAxisPair {
  GuppiElementView *view;
  guppi_axis_t axis;
};

#define x_view_interval view_interval[GUPPI_X_AXIS]
#define y_view_interval view_interval[GUPPI_Y_AXIS]

typedef struct _GuppiElementViewPrivate GuppiElementViewPrivate;
struct _GuppiElementViewPrivate {

  guppi_uniq_t id;
  gchar *label;

  GuppiElementState *state;
  guint state_changed_handler;
  guint state_changed_size_handler;
  
  GuppiGeometry *geometry;

  GuppiAttributeBag *attr_bag;

  GuppiViewInterval *view_interval[GUPPI_LAST_AXIS];
  guint vi_changed_handler[GUPPI_LAST_AXIS];
  guint vi_prefrange_handler[GUPPI_LAST_AXIS];
  gboolean vi_force_preferred[GUPPI_LAST_AXIS];
  ViewAxisPair *vi_closure[GUPPI_LAST_AXIS];

  gboolean forcing_preferred;
  guint pending_force_tag;

  gint axis_marker_type[GUPPI_LAST_AXIS];
  GuppiAxisMarkers *axis_markers[GUPPI_LAST_AXIS];
  guint am_changed_handler[GUPPI_LAST_AXIS];
  
  gboolean hide;
  gboolean block_tools;

  gint freeze_count;
  gboolean pending_change;
  guint pending_delayed_change;
};

#define priv(x) ((x)->priv)

static GtkObjectClass *parent_class = NULL;

enum {
  CHANGED,
  CHANGED_STATE,
  CHANGED_SIZE,
  CHANGED_POSITION,
  CHANGED_STRUCTURE,
  LAST_SIGNAL
};

static guint view_signals[LAST_SIGNAL] = { 0 };

static void
guppi_element_view_finalize (GtkObject *obj)
{
  GuppiElementView *view = GUPPI_ELEMENT_VIEW (obj);
  GuppiElementViewPrivate *p = priv (view);
  gint i;

  guppi_finalized (obj);

  /* clean up from state */

  if (p->state_changed_handler)
    gtk_signal_disconnect (GTK_OBJECT (p->state), p->state_changed_handler);
  if (p->state_changed_size_handler)
    gtk_signal_disconnect (GTK_OBJECT (p->state), p->state_changed_size_handler);
  guppi_unref0 (p->state);

  /* clean up from axis markers */
  
  for (i = 0; i < GUPPI_LAST_AXIS; ++i) {
    if (p->am_changed_handler[i])
      gtk_signal_disconnect (GTK_OBJECT (p->axis_markers[i]), p->am_changed_handler[i]);
    guppi_unref0 (p->axis_markers[i]);
  }

  /* clean up from view intervals */

  for (i=0; i<GUPPI_LAST_AXIS; ++i) {
    if (p->view_interval[i]) {
      if (p->vi_changed_handler[i])
	gtk_signal_disconnect (GTK_OBJECT (p->view_interval[i]), p->vi_changed_handler[i]);
      if (p->vi_prefrange_handler[i])
	gtk_signal_disconnect (GTK_OBJECT (p->view_interval[i]), p->vi_prefrange_handler[i]);
      guppi_unref0 (p->view_interval[i]);
    }
    guppi_free0 (p->vi_closure[i]);
  }
  
  guppi_unref0 (p->attr_bag);
  guppi_unref0 (p->geometry);
  guppi_unref0 (p->attr_bag);

  if (p->pending_delayed_change) {
    gtk_idle_remove (p->pending_delayed_change);
    p->pending_delayed_change = 0;
  }

  if (p->pending_force_tag) {
    gtk_idle_remove (p->pending_force_tag);
    p->pending_force_tag = 0;
  }

  guppi_free0 (view->priv);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void compute_markers (GuppiElementView *, guppi_axis_t);

static void
print (GuppiElementView *view, GnomePrintContext *pc)
{
  GuppiElementPrint *ep;
  GuppiGeometry *geom;
  double x0, y0, x1, y1;
  gint i;

  /* Make sure everything is in sync. */
  guppi_element_state_flush_changes (guppi_element_view_state (view));
  guppi_element_view_flush_changes (view);
  for (i = 0; i < GUPPI_LAST_AXIS; ++i)
    compute_markers (view, i);

  geom = guppi_element_view_geometry (view);

  x0 = guppi_geometry_left (geom);
  x1 = guppi_geometry_right (geom);

  y0 = guppi_geometry_bottom (geom);
  y1 = guppi_geometry_top (geom);

  g_message ("printing %s[%s] %g:%g %g:%g",
	     gtk_type_name (GTK_OBJECT_TYPE (view)),
	     guppi_element_view_get_label (view),
	     x0, x1, y0, y1);
			    
  ep = guppi_element_view_make_print (view, pc);

  if (ep) {
    guppi_element_print_set_bbox (ep, x0, y0, x1, y1);
    guppi_element_print_print (ep);
    guppi_unref (ep);
  }
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

GuppiElementView *
guppi_element_view_new (GuppiElementState *state, ...)
{
  GuppiElementView *view;
  va_list args;

  g_return_val_if_fail (GUPPI_IS_ELEMENT_STATE (state), NULL);

  view = guppi_element_state_make_view (state);
  va_start (args, state);
  guppi_attribute_bag_vset (priv (view)->attr_bag, &args);
  va_end (args);

  return view;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

/*
 *
 * GuppiAxisMarkers gadgetry
 *
 */

static void
update_axis_markers (GuppiElementView *view, guppi_axis_t ax, GuppiAxisMarkers *marks, double a, double b)
{
  g_assert (0 <= ax && ax < GUPPI_LAST_AXIS);
  if (marks && priv (view)->axis_marker_type[ax] != GUPPI_AXIS_NONE)
    guppi_axis_markers_populate_generic (marks, priv (view)->axis_marker_type[ax], a, b);
}

static void
compute_markers (GuppiElementView *view, guppi_axis_t ax)
{
  GuppiElementViewClass *klass;
  double a, b;

  g_assert (0 <= ax && ax < GUPPI_LAST_AXIS);

  klass = GUPPI_ELEMENT_VIEW_CLASS (GTK_OBJECT (view)->klass);

  if (view->priv->axis_markers[ax] != NULL && klass->update_axis_markers != NULL) {

    GuppiViewInterval *vi = guppi_element_view_axis_view_interval (view, ax);
    GuppiAxisMarkers *am = view->priv->axis_markers[ax];

    if (am) {
      guppi_view_interval_range (vi, &a, &b);
      klass->update_axis_markers (view, ax, am, a, b);
    }
  }
}

static void
am_changed (GuppiAxisMarkers *gam, ViewAxisPair *pair)
{
  GuppiElementView *view = GUPPI_ELEMENT_VIEW (pair->view);

  guppi_element_view_changed (view);
}

static void
set_axis_markers (GuppiElementView *view, guppi_axis_t ax, GuppiAxisMarkers *mark)
{
  GuppiElementViewPrivate *p;

  g_assert (0 <= ax && ax < GUPPI_LAST_AXIS);

  p = priv (view);

  if (p->axis_markers[ax] != NULL) {
    gtk_signal_disconnect (GTK_OBJECT (p->axis_markers[ax]), p->am_changed_handler[ax]);
    p->am_changed_handler[ax] = 0;
  }

  guppi_refcounting_assign (p->axis_markers[ax], mark);

  if (mark != NULL) {

    if (p->vi_closure[ax] == NULL) {
      p->vi_closure[ax] = guppi_new (ViewAxisPair, 1);
      p->vi_closure[ax]->view = view;
      p->vi_closure[ax]->axis = ax;
    }

    p->am_changed_handler[ax] = gtk_signal_connect (GTK_OBJECT (p->axis_markers[ax]),
						    "changed",
						    GTK_SIGNAL_FUNC (am_changed),
						    p->vi_closure[ax]);
  }
}

/*** GuppiAxisMarker-related API calls */

void
guppi_element_view_add_axis_markers (GuppiElementView *view, guppi_axis_t ax)
{
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));
  g_assert (0 <= ax && ax < GUPPI_LAST_AXIS);
 
  if (view->priv->axis_markers[ax] == NULL) {
    GuppiAxisMarkers *am = guppi_axis_markers_new ();
    set_axis_markers (view, ax, am);
    guppi_unref (am);
  }
}

gint
guppi_element_view_axis_marker_type (GuppiElementView *view, guppi_axis_t ax)
{
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), -1);
  g_assert (0 <= ax && ax < GUPPI_LAST_AXIS);

  return view->priv->axis_marker_type[ax];
}

void
guppi_element_view_set_axis_marker_type (GuppiElementView *view, guppi_axis_t ax, gint code)
{
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));
  g_assert (0 <= ax && ax < GUPPI_LAST_AXIS);
  
  view->priv->axis_marker_type[ax] = code;
  guppi_element_view_add_axis_markers (view, ax);
  compute_markers (view, ax);
}

GuppiAxisMarkers *
guppi_element_view_axis_markers (GuppiElementView *view, guppi_axis_t ax)
{
  GuppiAxisMarkers *gam;
  
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), NULL);
  g_assert (0 <= ax && ax < GUPPI_LAST_AXIS);

  gam = view->priv->axis_markers[ax];
  if (gam)
    guppi_axis_markers_sort (gam);
  
  return gam;
}

void
guppi_element_view_connect_axis_markers (GuppiElementView *view1, guppi_axis_t ax1,
					 GuppiElementView *view2, guppi_axis_t ax2)
{
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view1));
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view2));
  g_assert (0 <= ax1 && ax1 < GUPPI_LAST_AXIS);
  g_assert (0 <= ax2 && ax2 < GUPPI_LAST_AXIS);

  guppi_element_view_freeze (view2);
  set_axis_markers (view2, ax2, guppi_element_view_axis_markers (view1, ax1));
  guppi_element_view_connect_view_intervals (view1, ax1, view2, ax2);
  guppi_element_view_changed (view2);
  guppi_element_view_thaw (view2);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

/* 
 *
 * GuppiViewInterval gadgetry
 *
 */

static gboolean
force_all_preferred_idle (gpointer ptr)
{
  GuppiElementView *view = ptr;
  gint i;

  for (i = 0; i < GUPPI_LAST_AXIS; ++i) {
    if (view->priv->view_interval[i] && view->priv->vi_force_preferred[i])
      guppi_element_view_set_preferred_view (view, i);
  }
  view->priv->pending_force_tag = 0;

  return FALSE;
}

static void
vi_changed (GuppiViewInterval *vi, ViewAxisPair *pair)
{
  GuppiElementViewClass *klass;
  GuppiElementViewPrivate *p;
  GuppiElementView *view = pair->view;
  guppi_axis_t ax = pair->axis;

  p = priv (view);
  klass = GUPPI_ELEMENT_VIEW_CLASS (GTK_OBJECT (view)->klass);

  if (p->vi_force_preferred[ax]) {

    if (p->vi_changed_handler[ax])
      gtk_signal_handler_block (GTK_OBJECT (vi), p->vi_changed_handler[ax]);

    guppi_element_view_set_preferred_view (view, ax);

    if (p->vi_changed_handler[ax])
      gtk_signal_handler_unblock (GTK_OBJECT (vi), p->vi_changed_handler[ax]);
    
  } else if (p->forcing_preferred && p->pending_force_tag == 0) {
    
    p->pending_force_tag = gtk_idle_add (force_all_preferred_idle, view);

  }
  
  if (p->axis_markers[ax] != NULL)
    compute_markers (view, ax);

  guppi_element_view_changed_delayed (view);
}

static void
vi_preferred (GuppiViewInterval *vi, ViewAxisPair *pair)
{
  GuppiElementViewClass *klass;
  GuppiElementView *view = pair->view;
  guppi_axis_t ax = pair->axis;
  double a, b;
  
  klass = GUPPI_ELEMENT_VIEW_CLASS (GTK_OBJECT (view)->klass);

  /* Our 'changed' signals are automatically blocked during view
     interval range requests, so we don't need to worry about
     freeze/thaw. */

  if (klass->preferred_range && klass->preferred_range (view, ax, &a, &b)) {
      guppi_view_interval_grow_to (vi, a, b);
  }
}

static void
set_view_interval (GuppiElementView *view, guppi_axis_t ax, GuppiViewInterval *vi)
{
  GuppiElementViewPrivate *p = priv (view);
  gint i = (int) ax;

  g_assert (0 <= i && i < GUPPI_LAST_AXIS);
  
  if (p->view_interval[i] == vi)
    return;
  
  if (p->view_interval[i] && p->vi_changed_handler[i]) {
    gtk_signal_disconnect (GTK_OBJECT (p->view_interval[i]), p->vi_changed_handler[i]);
    p->vi_changed_handler[i] = 0;
  }

  if (p->vi_prefrange_handler[i]) {
    gtk_signal_disconnect (GTK_OBJECT (p->view_interval[i]), p->vi_prefrange_handler[i]);
    p->vi_prefrange_handler[i] = 0;
  }

  guppi_refcounting_assign (p->view_interval[i], vi);

  if (vi != NULL) {
    
    if (p->vi_closure[i] == NULL) {
      p->vi_closure[i] = guppi_new (ViewAxisPair, GUPPI_LAST_AXIS);
      p->vi_closure[i]->view = view;
      p->vi_closure[i]->axis = ax;
    }

    p->vi_changed_handler[i] = gtk_signal_connect (GTK_OBJECT (p->view_interval[i]),
						   "changed",
						   GTK_SIGNAL_FUNC (vi_changed),
						   p->vi_closure[i]);
    p->vi_prefrange_handler[i] = gtk_signal_connect (GTK_OBJECT (p->view_interval[i]),
						     "preferred_range_request",
						     GTK_SIGNAL_FUNC (vi_preferred),
						     p->vi_closure[i]);

    compute_markers (view, ax);
  }
}

/*** ViewInterval-related API calls ***/

GuppiViewInterval *
guppi_element_view_axis_view_interval (GuppiElementView *view, guppi_axis_t ax)
{
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), NULL);
  g_assert (0 <= ax && ax < GUPPI_LAST_AXIS);

  if (view->priv->view_interval[ax] == NULL) {
    GuppiViewInterval *vi = guppi_view_interval_new ();
    set_view_interval (view, ax, vi);
    guppi_view_interval_request_preferred_range (vi);
    guppi_unref (vi);
  }

  return view->priv->view_interval[ax];
}

void
guppi_element_view_connect_view_intervals (GuppiElementView *view1, guppi_axis_t axis1,
					   GuppiElementView *view2, guppi_axis_t axis2)
{
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view1));
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view2));

  if (axis1 == axis2 && view1 == view2)
    return;

  set_view_interval (view2, axis2, guppi_element_view_axis_view_interval (view1, axis1));
  guppi_element_view_changed (view2);
}


/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

/*
 *
 * Geometry stuff.  Very simple.
 *
 */

GuppiGeometry *
guppi_element_view_geometry (GuppiElementView *view)
{
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), NULL);
  return priv (view)->geometry;
}

static void
changed_position_proxy_fn (GuppiGeometry *geom, gpointer view)
{
  gtk_signal_emit (GTK_OBJECT (view), view_signals[CHANGED_POSITION]);
}

static void
guppi_element_view_set_geometry (GuppiElementView *view, GuppiGeometry *geom)
{
  if (view->priv->geometry == geom)
    return;

  if (view->priv->geometry != NULL) {
    gtk_signal_disconnect_by_func (GTK_OBJECT (view->priv->geometry),
				   GTK_SIGNAL_FUNC (changed_position_proxy_fn),
				   view);
  }
  
  guppi_refcounting_assign (view->priv->geometry, geom);

  gtk_signal_connect (GTK_OBJECT (view->priv->geometry),
		      "changed_position",
		      GTK_SIGNAL_FUNC (changed_position_proxy_fn),
		      GTK_OBJECT (view));

}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

const gchar *
guppi_element_view_get_label (GuppiElementView *view)
{
  const gchar *label = NULL;
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), NULL);

  guppi_attribute_bag_get1 (view->priv->attr_bag, "label::raw", &label);
  return label;
}

void
guppi_element_view_set_label (GuppiElementView *view, const gchar *label)
{
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));
  guppi_attribute_bag_set (view->priv->attr_bag, "label", label, NULL);
}


guppi_uniq_t
guppi_element_view_unique_id (GuppiElementView *view)
{
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), 0);
  return priv (view)->id;
}

static GuppiAttributeBag *
resolve_dot (GuppiElementView *view, const gchar *key, gchar **true_key)
{
  GuppiElementViewClass *klass;
  GuppiElementState *es = NULL;
  GuppiElementView *ev = NULL;
  GuppiAttributeBag *ret_bag = NULL;
  gchar *target_label;
  gchar *dot = strchr (key, '.');

  if (dot == NULL) {
    *true_key = (gchar *) key;
    return view->priv->attr_bag;
  }

  target_label = guppi_strndup (key, dot-key);
  *true_key = (gchar *) dot+1;

  klass = GUPPI_ELEMENT_VIEW_CLASS (GTK_OBJECT (view)->klass);
  if (klass->find (view, target_label, &es, &ev)) {
    if (es != NULL) {
      ret_bag = guppi_element_state_attribute_bag (es);
    } else {
      ret_bag = ev->priv->attr_bag;
    }
  }

  guppi_free (target_label);
  return ret_bag;
}

gboolean
guppi_element_view_get (GuppiElementView *view, ...)
{
  va_list args;
  GuppiAttributeBag *bag;
  const gchar *key, *true_key;
  gboolean rv = TRUE;

  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), FALSE);

  va_start (args, view);
  do {
    key = va_arg (args, const gchar *);

    if (key != NULL) {
      bag = resolve_dot (view, key, (gchar **) &true_key);
      if (bag) {
	guppi_attribute_bag_get1 (bag, true_key, va_arg (args, gpointer));
      } else {
	g_warning ("problem! (%s)", key);
	key = NULL;
	rv = FALSE;
      }
    }
  } while (key != NULL);
  va_end (args);

  return rv;
}

gboolean
guppi_element_view_set (GuppiElementView *view, ...)
{
  va_list args;
  GuppiAttributeBag *bag;
  const gchar *key, *true_key;
  gboolean rv = TRUE;

  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), FALSE);

  va_start (args, view);
  do {
    key = va_arg (args, const gchar *);
    
    if (key != NULL) {
      bag = resolve_dot (view, key, (gchar **) &true_key);
      if (bag) {
	guppi_attribute_bag_vset1 (bag, true_key, &args);
      } else {
	g_warning ("problem setting (%s)", key);
	key = NULL;
	rv = FALSE;
      }
    }
  } while (key != NULL);
  va_end (args);

  return rv;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

/*
 *
 * State-related operations
 *
 */

/* Default handler for changed_state signal. */
static void
changed_state (GuppiElementView *view)
{
  GuppiElementViewClass *klass;
  GuppiElementViewPrivate *p;
  gint i;

  klass = GUPPI_ELEMENT_VIEW_CLASS (GTK_OBJECT (view)->klass);
  p = priv (view);

  for (i = 0; i < GUPPI_LAST_AXIS; ++i) {

    if (p->vi_force_preferred[i]) {
      guppi_element_view_set_preferred_view (view, i);
    } else if (p->view_interval[i] ) {
      double x0, x1;
	/* If one of our axes is really screwed up, try to un-screw it. */
      guppi_view_interval_range (p->view_interval[i], &x0, &x1);
      if (x0 >= x1)
	guppi_element_view_set_preferred_view (view, i);
    }

    if (p->axis_markers[i] != NULL) {
      compute_markers (view, i);
    }
  }
}

static void
state_changed_proxy_fn (GuppiElementState *state, gpointer view)
{
  gtk_signal_emit (GTK_OBJECT (view), view_signals[CHANGED_STATE]);
}

static void
state_changed_size_proxy_fn (GuppiElementState *state, double width, double height, gpointer closure)
{
  guppi_element_view_changed_size (GUPPI_ELEMENT_VIEW (closure), width, height);
}

void
guppi_element_view_set_state (GuppiElementView *view,
			      GuppiElementState *state)
{
  GuppiElementViewClass *klass;
  GuppiElementViewPrivate *p;

  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));
  g_return_if_fail (GUPPI_IS_ELEMENT_STATE (state));

  p = priv (view);
  g_return_if_fail (p->state == NULL);

  p->state = state;
  guppi_ref (p->state);

  /* Forward state 'changed' signals as 'changed_state' */
  p->state_changed_handler = gtk_signal_connect (GTK_OBJECT (state),
						 "changed",
						 GTK_SIGNAL_FUNC (state_changed_proxy_fn),
						 view);

  /* Forward the state's 'changed_size' signals to the view. */
  p->state_changed_size_handler = gtk_signal_connect (GTK_OBJECT (state),
						      "changed_size",
						      GTK_SIGNAL_FUNC (state_changed_size_proxy_fn),
						      GTK_OBJECT (view));

  klass = GUPPI_ELEMENT_VIEW_CLASS (GTK_OBJECT (view)->klass);
  if (klass->view_init)
    klass->view_init (view);
}

GuppiElementState *
guppi_element_view_state (GuppiElementView *view)
{
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), NULL);
  return priv (view)->state;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

void
guppi_element_view_get_bbox_vp (GuppiElementView *view,
				double *x0, double *y0,
				double *x1, double *y1)
{
  GuppiViewInterval *view_x;
  GuppiViewInterval *view_y;

  g_return_if_fail (view != NULL);
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));

  view_x = guppi_element_view_axis_view_interval (view, GUPPI_X_AXIS);
  view_y = guppi_element_view_axis_view_interval (view, GUPPI_Y_AXIS);

  if (x0)
    *x0 = view_x->t0;
  if (x1)
    *x1 = view_x->t1;

  if (y0)
    *y0 = view_y->t0;
  if (y1)
    *y1 = view_y->t1;
}

void
guppi_element_view_get_bbox_pt (GuppiElementView *view,
				double *x0, double *y0,
				double *x1, double *y1)
{
  GuppiGeometry *geom;

  g_return_if_fail (view != NULL);
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));

  geom = guppi_element_view_geometry (view);

  guppi_geometry_get_bbox (geom, x0, y0, x1, y1);
}

void
guppi_element_view_vp2pt (GuppiElementView *view,
			  double vp_x, double vp_y, double *pt_x,
			  double *pt_y)
{
  GuppiGeometry *geom;
  GuppiViewInterval *vix;
  GuppiViewInterval *viy;
  double tx, ty;

  g_return_if_fail (view != NULL);
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));

  geom = guppi_element_view_geometry (view);

  vix = guppi_element_view_axis_view_interval (view, GUPPI_X_AXIS);
  viy = guppi_element_view_axis_view_interval (view, GUPPI_Y_AXIS);

  tx = guppi_view_interval_conv (vix, vp_x);
  ty = guppi_view_interval_conv (viy, vp_y);

  guppi_geometry_unconv (geom, tx, ty, pt_x, pt_y);
}

void
guppi_element_view_pt2vp (GuppiElementView *view,
			  double pt_x, double pt_y, double *vp_x,
			  double *vp_y)
{
  GuppiGeometry *geom;
  double tx, ty;

  g_return_if_fail (view != NULL);
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));

  geom = guppi_element_view_geometry (view);

  guppi_geometry_conv (geom, pt_x, pt_y, &tx, &ty);

  if (vp_x) {
    GuppiViewInterval *vi = guppi_element_view_axis_view_interval (view, GUPPI_X_AXIS);
    *vp_x = guppi_view_interval_unconv (vi, tx);
  }

  if (vp_y) {
    GuppiViewInterval *vi = guppi_element_view_axis_view_interval (view, GUPPI_Y_AXIS);
    *vp_y = guppi_view_interval_unconv (vi, ty);
  }
}

/***************************************************************************/

GuppiCanvasItem *
guppi_element_view_make_canvas_item (GuppiElementView *view,
				     GnomeCanvas *canvas,
				     GnomeCanvasGroup *group)
{
  GuppiElementViewPrivate *p;
  GuppiElementViewClass *klass;
  GuppiCanvasItem *item = NULL;
  GnomeCanvasItem *gnoitem;

  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), NULL);
  g_return_val_if_fail (canvas != NULL && GNOME_IS_CANVAS (canvas), NULL);

  p = priv (view);

  klass = GUPPI_ELEMENT_VIEW_CLASS (GTK_OBJECT (view)->klass);

  /* Default to the root group. */
  if (group == NULL)
    group = gnome_canvas_root (canvas);

  if (klass->make_canvas_item) {

    item = klass->make_canvas_item (view, canvas, group);
    
  } else if (klass->canvas_item_type) {

    gnoitem = gnome_canvas_item_new (group, klass->canvas_item_type, NULL);
    item = GUPPI_CANVAS_ITEM (gnoitem);

  } else {

    g_warning ("no make_canvas_item function or canvas_item_type defined for '%s'",
	       gtk_type_name (GTK_OBJECT_TYPE (view)));

    return NULL;

  }

  g_assert (item != NULL);
  guppi_outside_object (item);

  guppi_canvas_item_set_view (item, view);

  if (klass->item_post_creation_init)
    klass->item_post_creation_init (view, item);
  
  if (GUPPI_CANVAS_ITEM_CLASS (GTK_OBJECT (item)->klass)->post_creation_init)
    GUPPI_CANVAS_ITEM_CLASS (GTK_OBJECT (item)->klass)->post_creation_init (item);

  return item;
}

GuppiElementPrint *
guppi_element_view_make_print (GuppiElementView *view,
			       GnomePrintContext *pc)
{
  GuppiElementViewClass *klass;
  GuppiElementPrint *ep = NULL;

  g_return_val_if_fail (view != NULL, NULL);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), NULL);

  g_return_val_if_fail (pc != NULL, NULL);
  g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (pc), NULL);

  klass = GUPPI_ELEMENT_VIEW_CLASS (GTK_OBJECT (view)->klass);

  if (klass->print_type && klass->make_print) {
    g_warning
      ("For %s, both a print type and a print constructor are defined.",
       gtk_type_name (GTK_OBJECT_TYPE (view)));
  }

  if (klass->print_type) {

    ep = GUPPI_ELEMENT_PRINT (guppi_type_new (klass->print_type));

    if (ep)
      guppi_element_print_set_context (ep, pc);

  } else if (klass->make_print != NULL) {

    ep = klass->make_print (view, pc);

  }

  if (ep) {
    ep->view = view;
    guppi_ref (view);
  }

  return ep;
}

/***************************************************************************/

void
guppi_element_view_set_preferred_view (GuppiElementView *view, guppi_axis_t ax)
{
  GuppiViewInterval *vi;

  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));
  g_assert (0 <= ax && ax < GUPPI_LAST_AXIS);

  vi = guppi_element_view_axis_view_interval (view, ax);
  guppi_view_interval_request_preferred_range (vi);
}

void
guppi_element_view_set_preferred_view_all (GuppiElementView *view)
{
  gint i;
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));

  /* Grossly inefficient, and wrong */

  for (i = 0; i < GUPPI_LAST_AXIS; ++i) {
    guppi_element_view_set_preferred_view (view, (guppi_axis_t) i);
  }
}

void
guppi_element_view_force_preferred_view (GuppiElementView *view, guppi_axis_t ax, gboolean q)
{
  gint i;

  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));
  g_assert (0 <= ax && ax < GUPPI_LAST_AXIS);

  view->priv->vi_force_preferred[ax] = q;

  if (q) {
    view->priv->forcing_preferred = TRUE;
  } else {
    view->priv->forcing_preferred = FALSE;
    for (i = 0; i < GUPPI_LAST_AXIS && !view->priv->forcing_preferred; ++i) {
      if (view->priv->vi_force_preferred[ax])
	view->priv->forcing_preferred = TRUE;
    }
  }

  if (q)
    guppi_element_view_set_preferred_view (view, ax);
}

/***************************************************************************/

gboolean guppi_element_view_visible (GuppiElementView *view)
{
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), FALSE);

  return !(priv (view)->hide);
}

void
guppi_element_view_show (GuppiElementView *view)
{
  guppi_element_view_set_visibility (view, TRUE);
}

void
guppi_element_view_hide (GuppiElementView *view)
{
  guppi_element_view_set_visibility (view, TRUE);
}

void
guppi_element_view_set_visibility (GuppiElementView *view, gboolean x)
{
  GuppiElementViewPrivate *p;

  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));

  p = priv (view);

  if (p->hide != !x) {
    p->hide = !x;
    guppi_element_view_changed (view);
  }
}

/***************************************************************************/

gboolean guppi_element_view_tools_are_blocked (GuppiElementView *view)
{
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), FALSE);
  return priv (view)->block_tools;
}

void
guppi_element_view_set_tool_blocking (GuppiElementView *view, gboolean x)
{
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));
  priv (view)->block_tools = x;
}

/***************************************************************************/

void
guppi_element_view_print (GuppiElementView *view, GnomePrintContext *pc)
{
  GuppiElementViewClass *klass;

  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));
  g_return_if_fail (pc != NULL && GNOME_IS_PRINT_CONTEXT (pc));

  klass = GUPPI_ELEMENT_VIEW_CLASS (GTK_OBJECT (view)->klass);
  if (klass->print
      && view->priv->geometry
      && guppi_geometry_positioned (view->priv->geometry))
    klass->print (view, pc);
}

void
guppi_element_view_print_to_bbox (GuppiElementView *view,
				  GnomePrintContext *pc,
				  double x0, double y0, double x1, double y1)
{
  GuppiGeometry *geom;
  double scale_w, scale_h, scale;
  double cx, cy;		/* corner x and y */
  double mat_scale[6];
  double mat_translate[6];

  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));
  g_return_if_fail (pc != NULL && GNOME_IS_PRINT_CONTEXT (pc));

  guppi_2sort (&x0, &x1);
  guppi_2sort (&y0, &y1);

  geom = guppi_element_view_geometry (view);

  scale_w = (x1 - x0) / guppi_geometry_width (geom);
  scale_h = (y1 - y0) / guppi_geometry_height (geom);
  scale = MIN (scale_w, scale_h);

  cx = (x0 + x1) / 2 - scale * guppi_geometry_width (geom) / 2;
  cy = (y0 + y1) / 2 - scale * guppi_geometry_height (geom) / 2;

  art_affine_scale (mat_scale, scale, scale);
  art_affine_translate (mat_translate, cx, cy);

  gnome_print_gsave (pc);

  gnome_print_concat (pc, mat_translate);
  gnome_print_concat (pc, mat_scale);

  guppi_element_view_print (view, pc);

  gnome_print_grestore (pc);
}

void
guppi_element_view_print_ps_to_file (GuppiElementView *view,
				     const gchar *filename)
{
  GnomePrinter *gprinter;
  GnomePrintContext *pc;

  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));
  g_return_if_fail (filename != NULL);

  gprinter = gnome_printer_new_generic_ps (filename);
  g_return_if_fail (gprinter != NULL);

  pc = gnome_print_context_new (gprinter);
  g_return_if_fail (pc != NULL);

  guppi_element_view_print (view, pc);
  gnome_print_showpage (pc);

  gnome_print_context_close (pc);

  guppi_unref (pc);
  guppi_unref (gprinter);
}

/***************************************************************************/

void
guppi_element_view_changed (GuppiElementView *view)
{
  GuppiElementViewPrivate *p;

  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));

  p = priv (view);

  if (p->pending_delayed_change) {
    gtk_idle_remove (p->pending_delayed_change);
    p->pending_delayed_change = 0;
  }

  if (p->freeze_count > 0)
    p->pending_change = 1;
  else {
    gtk_signal_emit (GTK_OBJECT (view), view_signals[CHANGED]);
    p->pending_change = 0;
  }
}

static gint
delayed_changer (gpointer foo)
{
  GuppiElementView *view = GUPPI_ELEMENT_VIEW (foo);
  priv (view)->pending_delayed_change = 0;
  guppi_element_view_changed (view);
  return FALSE;
}

void
guppi_element_view_changed_delayed (GuppiElementView *view)
{
  GuppiElementViewPrivate *p;

  g_return_if_fail (view != NULL);
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));

  p = priv (view);

  if (p->freeze_count > 0)
    p->pending_change = TRUE;
  else if (p->pending_delayed_change == 0) {
    p->pending_delayed_change = gtk_idle_add_priority (G_PRIORITY_HIGH_IDLE, delayed_changer, view);
  }
}

void
guppi_element_view_flush_changes (GuppiElementView *view)
{
  g_return_if_fail (view != NULL);
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));

  if (priv (view)->pending_delayed_change != 0) {
    guppi_element_view_changed (view);
  }
}

static void
changed_size (GuppiElementView *view, double width, double height)
{
  GuppiGeometry *gg;

  gg = priv (view)->geometry;

  if (width >= 0 && height >= 0) {
    guppi_geometry_set_natural_size (gg, width, height);
  } else if (width >= 0) {
    guppi_geometry_set_natural_width (gg, width);
    guppi_geometry_unset_natural_height (gg);
  } else if (height >= 0) {
    guppi_geometry_unset_natural_width (gg);
    guppi_geometry_set_natural_height (gg, height);
  } else {
    guppi_geometry_unset_natural_width (gg);
    guppi_geometry_unset_natural_height (gg);
  }
}

void
guppi_element_view_changed_size (GuppiElementView *view, double width, double height)
{
  GuppiGeometry *geom;
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));

  if (width < 0)
    width = -1;
  if (height < 0)
    height = -1;

  geom = guppi_element_view_geometry (view);

  if (fabs (guppi_geometry_get_natural_width (geom) - width) > 1e-4
      || fabs (guppi_geometry_get_natural_height (geom) - height) > 1e-4) {
    gtk_signal_emit (GTK_OBJECT (view), view_signals[CHANGED_SIZE], width, height);
  }
}

void
guppi_element_view_changed_structure (GuppiElementView *view)
{
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));
  gtk_signal_emit (GTK_OBJECT (view), view_signals[CHANGED_STRUCTURE]);
}

gboolean
guppi_element_view_frozen (GuppiElementView *view)
{
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), FALSE);

  return priv (view)->freeze_count > 0;
}

void
guppi_element_view_freeze (GuppiElementView *view)
{
  GuppiElementViewClass *klass;
  GuppiElementViewPrivate *p;

  g_return_if_fail (view != NULL);
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));

  p = priv (view);
  ++p->freeze_count;

  klass = GUPPI_ELEMENT_VIEW_CLASS (GTK_OBJECT (view)->klass);
  if (klass->freeze)
    klass->freeze (view);
}

void
guppi_element_view_thaw (GuppiElementView *view)
{
  GuppiElementViewClass *klass;
  GuppiElementViewPrivate *p;

  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));

  p = priv (view);
  g_return_if_fail (p->freeze_count > 0);

  --p->freeze_count;

  klass = GUPPI_ELEMENT_VIEW_CLASS (GTK_OBJECT (view)->klass);
  if (klass->thaw)
    klass->thaw (view);

  if (p->freeze_count == 0) {

    if (klass->fully_thawed)
      klass->fully_thawed (view);

    if (p->pending_change)
      guppi_element_view_changed (view);
  }
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

GuppiConfigModel *
guppi_element_view_make_config_model (GuppiElementView *view)
{
  GuppiElementState *state;
  GuppiElementViewClass *view_class;
  GuppiElementStateClass *state_class;
  GuppiConfigModel *model;

  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), NULL);

  state = guppi_element_view_state (view);

  model = guppi_config_model_new ();

  view_class = GUPPI_ELEMENT_VIEW_CLASS (GTK_OBJECT (view)->klass);
  state_class = GUPPI_ELEMENT_STATE_CLASS (GTK_OBJECT (guppi_element_view_state (view))->klass);

  if (view_class->make_config_model)
    view_class->make_config_model (view, model);
  if (state_class->make_config_model)
    state_class->make_config_model (state, model);

  return model;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

xmlNodePtr
guppi_element_view_export_xml (GuppiElementView *view, GuppiXMLDocument *doc)
{
  GuppiElementViewClass *klass;
  xmlNodePtr view_node = NULL;
  xmlNodePtr state_node = NULL;
  xmlNodePtr attr_node = NULL;
  xmlNodePtr geom_node = NULL;
  gint i;
  gchar *uid_str;

  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), NULL);
  g_return_val_if_fail (doc != NULL, NULL);

  klass = GUPPI_ELEMENT_VIEW_CLASS (GTK_OBJECT (view)->klass);

  view_node = xmlNewNode (doc->ns, "ElementView");

  uid_str = guppi_uniq2str (priv (view)->id);
  xmlNewProp (view_node, "UID", uid_str);
  guppi_free (uid_str);

  /* It is very important that the first child of the view is the state! */
  state_node = guppi_element_state_export_xml (guppi_element_view_state (view), doc);
  if (state_node)
    xmlAddChild (view_node, state_node);

  geom_node = guppi_geometry_export_xml (view->priv->geometry, doc);
  if (geom_node)
    xmlAddChild (view_node, geom_node);

  for (i = 0; i < GUPPI_LAST_AXIS; ++i) {
    if (view->priv->view_interval[i] != NULL || view->priv->axis_markers[i] != NULL) {
      xmlNodePtr axis_node = xmlNewNode (doc->ns, "Axis");
      xmlNewProp (axis_node, "Dim", guppi_axis2str (i));

      if (view->priv->view_interval[i]) {
	xmlNodePtr vi_node = guppi_view_interval_export_xml (view->priv->view_interval[i], doc);
	if (vi_node)
	  xmlAddChild (axis_node, vi_node);
      }

      xmlAddChild (view_node, axis_node);
    }
  }

  attr_node = guppi_attribute_bag_export_xml (priv (view)->attr_bag, doc);
  if (attr_node) {
    if (attr_node->xmlChildrenNode != NULL) {
      xmlAddChild (view_node, attr_node);
    } else {
      xmlFreeNode (attr_node);
    }
  }

  if (klass->xml_export) {
    klass->xml_export (view, doc, view_node);
  }

  return view_node;
}

GuppiElementView *
guppi_element_view_import_xml (GuppiXMLDocument *doc, xmlNodePtr node)
{
  GuppiElementState *state;
  GuppiElementView *view;
  GuppiElementViewClass *klass;
  gchar *uid_str = NULL;
  gboolean loaded_attr_bag = FALSE, loaded_geom = FALSE;
  GuppiGeometry *geom;

  g_return_val_if_fail (doc != NULL, NULL);
  g_return_val_if_fail (node != NULL, NULL);

  if (strcmp (node->name, "ElementView"))
    return NULL;

  uid_str = xmlGetProp (node, "UID");

  node = node->xmlChildrenNode;

  state = guppi_element_state_import_xml (doc, node);
  if (state == NULL)
    return NULL;
  view = guppi_element_view_new (state, NULL);
  guppi_unref0 (state);

  view->priv->id = guppi_str2uniq (uid_str);
  xmlFree (uid_str);
  
  klass = GUPPI_ELEMENT_VIEW_CLASS (GTK_OBJECT (view)->klass);

  node = node->next;

  while (node != NULL) {

    if (!strcmp (node->name, "Axis")) {

      gchar *str = xmlGetProp (node, "Dim");
      guppi_axis_t ax = guppi_str2axis (str);
      xmlFree (str);

      if (ax != GUPPI_INVALID_AXIS) {
	GuppiViewInterval *vi;
	xmlNodePtr subnode = node->xmlChildrenNode;

	while (subnode) {
	  if ( (vi = guppi_view_interval_import_xml (doc, subnode)) != NULL) {
	    set_view_interval (view, ax, vi);
	  } /* else is axismarker... */

	  subnode = subnode->next;
	}

      }

    } else if (!loaded_attr_bag && guppi_attribute_bag_import_xml (view->priv->attr_bag, doc, node)) {

      loaded_attr_bag = TRUE;

    } else if (!loaded_geom && (geom = guppi_geometry_import_xml (doc, node))) {

      guppi_element_view_set_geometry (view, geom);

    } else if (klass->xml_import) {

      klass->xml_import (view, doc, node);

    }
    
    node = node->next;

  }

  return view;
}

void
guppi_element_view_spew_xml (GuppiElementView *view)
{
  GuppiXMLDocument *doc;
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));

  doc = guppi_xml_document_new ();
  guppi_xml_document_set_root (doc, guppi_element_view_export_xml (view, doc));
  guppi_xml_document_spew (doc);
  guppi_xml_document_free (doc);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

/* Default handler for find method */

static gboolean
find (GuppiElementView *view, const gchar *label, GuppiElementState **stateptr, GuppiElementView **viewptr)
{
  const gchar *our_label;

  if ((stateptr == NULL && viewptr == NULL) || label == NULL)
    return FALSE;

  if (viewptr) {
    our_label = guppi_element_view_get_label (view);

    if (our_label && !strcmp (label, our_label)) {
      *viewptr = view;
      return TRUE;
    }
  }

  if (stateptr) {
    GuppiElementState *state;
  
    state = guppi_element_view_state (view);
    our_label = guppi_element_state_get_label (state);
    
    if (our_label && !strcmp (label, our_label)) {
      *stateptr = state;
      return TRUE;
    }
  }

  return FALSE;
}

GuppiElementState *
guppi_element_view_find_state (GuppiElementView *view, const gchar *label)
{
  GuppiElementViewClass *klass;
  GuppiElementState *state = NULL;
  
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), NULL);
  g_return_val_if_fail (label != NULL, NULL);

  klass = GUPPI_ELEMENT_VIEW_CLASS (GTK_OBJECT (view)->klass);

  if (klass->find) {
    klass->find (view, label, &state, NULL);
  }

  return state;
}

GuppiElementView *
guppi_element_view_find_view (GuppiElementView *view, const gchar *label)
{
  GuppiElementViewClass *klass;
  GuppiElementView *subview = NULL;
  
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), NULL);
  g_return_val_if_fail (label != NULL, NULL);

  klass = GUPPI_ELEMENT_VIEW_CLASS (GTK_OBJECT (view)->klass);

  if (klass->find) {
    klass->find (view, label, NULL, &subview);
  }

  return subview;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
view_init (GuppiElementView *view)
{
  guppi_geometry_set_debug_label (view->priv->geometry, gtk_type_name (GTK_OBJECT_TYPE (view)));
#if 0
  gint i;
  /* Initially compute markers */
  for (i = 0; i < GUPPI_LAST_AXIS; ++i)
    compute_markers (view, i);
#endif
}

typedef void (*GuppiSignal_NONE__DOUBLE_DOUBLE) (GtkObject *, double, double, gpointer user_data);

static void
guppi_marshal_NONE__DOUBLE_DOUBLE (GtkObject *object, GtkSignalFunc func, gpointer func_data, GtkArg *args)
{
  GuppiSignal_NONE__DOUBLE_DOUBLE rfunc;

  rfunc = (GuppiSignal_NONE__DOUBLE_DOUBLE) func;
  
  (*rfunc) (object, GTK_VALUE_DOUBLE (args[0]), GTK_VALUE_DOUBLE (args[1]), func_data);
}

static void
guppi_element_view_class_init (GuppiElementViewClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;

  parent_class = gtk_type_class (GTK_TYPE_OBJECT);

  object_class->finalize = guppi_element_view_finalize;

  klass->print = print;
  klass->find = find;

  klass->update_axis_markers = update_axis_markers;
  klass->changed_state = changed_state;
  klass->changed_size = changed_size;
  klass->view_init = view_init;

  view_signals[CHANGED] =
    gtk_signal_new ("changed",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GuppiElementViewClass, changed),
		    gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);

  view_signals[CHANGED_STATE] =
    gtk_signal_new ("changed_state",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GuppiElementViewClass, changed_state),
		    gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);

  view_signals[CHANGED_SIZE] =
    gtk_signal_new ("changed_size",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GuppiElementViewClass, changed_size),
		    guppi_marshal_NONE__DOUBLE_DOUBLE,
		    GTK_TYPE_NONE, 2, GTK_TYPE_DOUBLE, GTK_TYPE_DOUBLE);

  view_signals[CHANGED_POSITION] =
    gtk_signal_new ("changed_position",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GuppiElementViewClass, changed_position),
		    gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);

  view_signals[CHANGED_STRUCTURE] =
    gtk_signal_new ("changed_structure",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GuppiElementViewClass, changed_structure),
		    gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);

  gtk_object_class_add_signals (object_class, view_signals, LAST_SIGNAL);

}

static void
guppi_element_view_init (GuppiElementView *view,
			 GuppiElementViewClass *klass)
{
  GuppiElementViewPrivate *p;
  GuppiGeometry *geom;
  gint i;

  p = view->priv = guppi_new0 (GuppiElementViewPrivate, 1);

  p->id = guppi_unique_id ();

  p->attr_bag = guppi_attribute_bag_new ();
  guppi_attribute_bag_add_with_default (p->attr_bag, GUPPI_ATTR_STRING, "label", NULL, _("Unlabelled"));

  /* Axis Markets */

  for (i = 0; i < GUPPI_LAST_AXIS; ++i) {
    p->axis_markers[i] = NULL;
    p->axis_marker_type[i] = GUPPI_AXIS_NONE;
  }

  /* Geometry */
  
  geom = guppi_geometry_new ();
  guppi_element_view_set_geometry (view, geom);
  guppi_unref (geom); /* drop extra ref */
}

GtkType guppi_element_view_get_type (void)
{
  static GtkType guppi_element_view_type = 0;
  if (!guppi_element_view_type) {
    static const GtkTypeInfo guppi_element_view_info = {
      "GuppiElementView",
      sizeof (GuppiElementView),
      sizeof (GuppiElementViewClass),
      (GtkClassInitFunc) guppi_element_view_class_init,
      (GtkObjectInitFunc) guppi_element_view_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_element_view_type = gtk_type_unique (GTK_TYPE_OBJECT,
					       &guppi_element_view_info);
  }
  return guppi_element_view_type;
}


/* $Id$ */
