/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-geometry.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_GEOMETRY_H
#define _INC_GUPPI_GEOMETRY_H

/* #include <gnome.h> */

#include <gtk/gtkobject.h>
#include  "guppi-defs.h"
#include  "guppi-xml.h"
#include  "guppi-unique-id.h"

BEGIN_GUPPI_DECLS 

struct _GuppiLayout;

typedef struct _GuppiGeometry GuppiGeometry;
typedef struct _GuppiGeometryClass GuppiGeometryClass;
struct _GuppiGeometryPrivate;

struct _GuppiGeometry {
  GtkObject parent;
  struct _GuppiGeometryPrivate *priv;
};

struct _GuppiGeometryClass {
  GtkObjectClass parent_class;

  /* Signals */
  void (*changed_position) (GuppiGeometry *);
  void (*changed_size) (GuppiGeometry *);
};

#define GUPPI_TYPE_GEOMETRY (guppi_geometry_get_type())
#define GUPPI_GEOMETRY(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_GEOMETRY,GuppiGeometry))
#define GUPPI_GEOMETRY0(obj) ((obj) ? (GUPPI_GEOMETRY(obj)) : NULL)
#define GUPPI_GEOMETRY_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_GEOMETRY,GuppiGeometryClass))
#define GUPPI_IS_GEOMETRY(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_GEOMETRY))
#define GUPPI_IS_GEOMETRY0(obj) (((obj) == NULL) || (GUPPI_IS_GEOMETRY(obj)))
#define GUPPI_IS_GEOMETRY_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_GEOMETRY))

GtkType guppi_geometry_get_type (void);

GuppiGeometry *guppi_geometry_new (void);

const gchar *guppi_geometry_get_debug_label (GuppiGeometry *);
void         guppi_geometry_set_debug_label (GuppiGeometry *, const gchar *str);

guppi_uniq_t guppi_geometry_get_unique_id (GuppiGeometry *gg);

void     guppi_geometry_unposition (GuppiGeometry *gg);
gboolean guppi_geometry_positioned (GuppiGeometry *gg);

double guppi_geometry_left (GuppiGeometry *gg);
double guppi_geometry_right (GuppiGeometry *gg);
double guppi_geometry_top (GuppiGeometry *gg);
double guppi_geometry_bottom (GuppiGeometry *gg);
double guppi_geometry_width (GuppiGeometry *gg);
double guppi_geometry_height (GuppiGeometry *gg);
gboolean guppi_geometry_get_bbox (GuppiGeometry *gg, double *x0, double *y0, double *x1, double *y1);

void guppi_geometry_conv (GuppiGeometry *gg,
			  double x, double y, double *t_x, double *t_y);

void guppi_geometry_unconv (GuppiGeometry *gg,
			    double t_x, double t_y, double *x, double *y);

void guppi_geometry_set_position (GuppiGeometry *gg, double left, double right,
				  double top, double bottom);

gboolean guppi_geometry_contains (GuppiGeometry *gg, double x, double y);

void guppi_geometry_set_natural_width  (GuppiGeometry *gg, double width);
void guppi_geometry_set_natural_height (GuppiGeometry *gg, double height);
void guppi_geometry_set_natural_size   (GuppiGeometry *gg, double width, double height);

void guppi_geometry_unset_natural_width  (GuppiGeometry *gg);
void guppi_geometry_unset_natural_height (GuppiGeometry *gg);
void guppi_geometry_unset_natural_size   (GuppiGeometry *gg);

gboolean guppi_geometry_has_natural_width  (GuppiGeometry *gg);
gboolean guppi_geometry_has_natural_height (GuppiGeometry *gg);

double guppi_geometry_get_natural_width  (GuppiGeometry *gg);
double guppi_geometry_get_natural_height (GuppiGeometry *gg);

xmlNodePtr     guppi_geometry_export_xml (GuppiGeometry *gg, GuppiXMLDocument *doc);
GuppiGeometry *guppi_geometry_import_xml (GuppiXMLDocument *doc, xmlNodePtr node);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_GEOMETRY_H */

/* $Id$ */
