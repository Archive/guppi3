/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-raster-text.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-raster-text.h"

#include <math.h>
#include <libart_lgpl/libart.h>
#include <guppi-convenient.h>
#include <guppi-metrics.h>


typedef struct _GuppiRasterTextPrivate GuppiRasterTextPrivate;
struct _GuppiRasterTextPrivate {

  GuppiTextBlock *block;

  double scale;

  GuppiAlphaTemplate *template;
  gint x, y;
};

#define priv(x) ((GuppiRasterTextPrivate *)(GUPPI_RASTER_TEXT(x)->opaque_internals))

static GtkObjectClass *parent_class = NULL;

static void
guppi_raster_text_finalize (GtkObject * obj)
{
  GuppiRasterText *grt = GUPPI_RASTER_TEXT (obj);
  GuppiRasterTextPrivate *p = priv (grt);

  guppi_finalized (obj);

  gtk_signal_disconnect_by_data (GTK_OBJECT (p->block), obj);
  guppi_unref0 (p->block);
  guppi_unref0 (p->template);

  guppi_free0 (grt->opaque_internals);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_raster_text_class_init (GuppiRasterTextClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;

  parent_class = gtk_type_class (GTK_TYPE_OBJECT);

  object_class->finalize = guppi_raster_text_finalize;
}

static void
guppi_raster_text_init (GuppiRasterText *obj)
{
  GuppiRasterTextPrivate *p = guppi_new0 (GuppiRasterTextPrivate, 1);

  p->scale = -1;

  obj->opaque_internals = p;
}

GtkType guppi_raster_text_get_type (void)
{
  static GtkType guppi_raster_text_type = 0;
  if (!guppi_raster_text_type) {
    static const GtkTypeInfo guppi_raster_text_info = {
      "GuppiRasterText",
      sizeof (GuppiRasterText),
      sizeof (GuppiRasterTextClass),
      (GtkClassInitFunc) guppi_raster_text_class_init,
      (GtkObjectInitFunc) guppi_raster_text_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_raster_text_type =
      gtk_type_unique (GTK_TYPE_OBJECT, &guppi_raster_text_info);
  }
  return guppi_raster_text_type;
}

static void
underlying_text_changed (GuppiTextBlock *text, GuppiRasterText *raster)
{
  g_return_if_fail (GUPPI_IS_TEXT_BLOCK (text));
  g_return_if_fail (GUPPI_IS_RASTER_TEXT (raster));
  guppi_unref0 (priv (raster)->template);
}

GuppiRasterText *
guppi_raster_text_new (GuppiTextBlock *text)
{
  GuppiRasterText *raster;
  GuppiRasterTextPrivate *p;

  g_return_val_if_fail (text == NULL || GUPPI_IS_TEXT_BLOCK (text), NULL);

  raster = GUPPI_RASTER_TEXT (guppi_type_new (guppi_raster_text_get_type ()));
  p = priv (raster);

  if (text) {
    p->block = text;
    guppi_ref (text);
  } else {
    p->block = guppi_text_block_new ();
  }

  gtk_signal_connect (GTK_OBJECT (p->block),
		      "changed",
		      GTK_SIGNAL_FUNC (underlying_text_changed),
		      raster);

  return raster;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

double
guppi_raster_text_scale (GuppiRasterText *rt)
{
  g_return_val_if_fail (rt && GUPPI_IS_RASTER_TEXT (rt), -1);

  return priv (rt)->scale;
}

void
guppi_raster_text_set_scale (GuppiRasterText *rt, double sc)
{
  GuppiRasterTextPrivate *p;

  g_return_if_fail (rt && GUPPI_IS_RASTER_TEXT (rt));
  p = priv (rt);

  if (p->scale != sc) {
    p->scale = sc;
    guppi_unref0 (p->template);
  }
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

typedef struct _RasterInfo RasterInfo;
struct _RasterInfo {
  GnomeCanvasBuf buf;
  double scale;

  gboolean filled;
  double outline_width;
  ArtPathStrokeJoinType join_type;
  ArtPathStrokeCapType cap_type;
};

static void
rasterize_cb (gint glyph, const GnomeFontFace *face,
	      double affine[6], gpointer user_data)
{
  RasterInfo *info = (RasterInfo *)user_data;
  const ArtBpath *path;
  ArtBpath *bpath;
  ArtVpath *vpath;
  ArtSVP *svp;

  double scale_affine[6];

  /* We use the fact that the pt2px transformation is just
     multiplication by a constant. */
  art_affine_scale (scale_affine, 
		    guppi_x_pt2px (info->scale),
		    guppi_y_pt2px (info->scale));
  art_affine_multiply (affine, affine, scale_affine);

  path = gnome_font_face_get_glyph_stdoutline (face, glyph);

  bpath = art_bpath_affine_transform (path, affine);
  vpath = art_bez_path_to_vec (bpath, 0.25);
  art_free (bpath);

  if (info->filled) {

    svp = art_svp_from_vpath (vpath);

  } else {

    svp = art_svp_vpath_stroke (vpath, info->join_type, info->cap_type,
				info->outline_width, 4, 0.25);
  }

  gnome_canvas_render_svp (&info->buf, svp, 0xffffffff);
  
  art_svp_free (svp);
  art_free (vpath);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

GuppiAlphaTemplate *
guppi_raster_text_template (GuppiRasterText *rt)
{
  GuppiRasterTextPrivate *p;
  g_return_val_if_fail (rt && GUPPI_IS_RASTER_TEXT (rt), NULL);
  p = priv (rt);

  guppi_text_block_flush_changes (p->block);

  if (p->template == NULL && p->scale > 0) {
    gint i, j;
    ArtDRect bbox;
    gint w, h;
    RasterInfo info;
    guchar *ptr;
    double affine_scale[6];

    guppi_text_block_bbox (p->block, &bbox);

    art_affine_scale (affine_scale, p->scale, p->scale);
    art_drect_affine_transform (&bbox, &bbox, affine_scale);

    w = (gint)ceil (guppi_pt2px (bbox.x1 - bbox.x0));
    h = (gint)ceil (guppi_pt2px (bbox.y1 - bbox.y0));
    
    if (w <= 0 || h <= 0)
      return NULL;

    info.buf.buf = guppi_new0 (guchar, w*h*3);
    info.buf.buf_rowstride = w*3;
    info.buf.rect.x0 = (gint)floor (guppi_pt2px (bbox.x0));
    info.buf.rect.y0 = (gint)floor (guppi_pt2px (bbox.y0));
    info.buf.rect.x1 = info.buf.rect.x0 + w;
    info.buf.rect.y1 = info.buf.rect.y0 + h;
    info.buf.bg_color = 0;
    info.buf.is_bg = TRUE;
    info.buf.is_buf = FALSE;

    info.scale = p->scale;
    
    info.filled = TRUE;
    info.join_type = gnome_canvas_join_gdk_to_art (GDK_JOIN_MITER);
    info.cap_type = gnome_canvas_cap_gdk_to_art (GDK_CAP_BUTT);

    guppi_text_block_foreach_char (p->block, rasterize_cb, &info);

    /* Copy our buffer into an alpha template object */
    guppi_unref (p->template);
    p->template = guppi_alpha_template_new (w, h);

    ptr = info.buf.buf;
    for (j=0; j < h; ++j) {
      for (i=0; i < w; ++i) {
	guppi_alpha_template_set_unsafe (p->template, i, j, *ptr);
	ptr += 3;
      }
    }

    guppi_free0 (info.buf.buf);
  }

  return p->template;
}

GuppiTextBlock *
guppi_raster_text_block (GuppiRasterText *rt)
{
  g_return_val_if_fail (rt && GUPPI_IS_RASTER_TEXT (rt), NULL);

  return priv (rt)->block;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

void
guppi_raster_text_set_position (GuppiRasterText *rt, gint x, gint y)
{
  GuppiRasterTextPrivate *p;

  g_return_if_fail (rt && GUPPI_IS_RASTER_TEXT (rt));
  p = priv (rt);

  p->x = x;
  p->y = y;
}

void
guppi_raster_text_position (GuppiRasterText *rt, gint *x, gint *y)
{
  GuppiRasterTextPrivate *p;

  g_return_if_fail (rt && GUPPI_IS_RASTER_TEXT (rt));
  p = priv (rt);

  if (x) *x = p->x;
  if (y) *y = p->y;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

gchar *
guppi_raster_text_text (GuppiRasterText *rt)
{
  g_return_val_if_fail (rt && GUPPI_IS_RASTER_TEXT (rt), NULL);

  return guppi_text_block_text (guppi_raster_text_block (rt));
}

void
guppi_raster_text_set_text (GuppiRasterText *rt, const gchar *str)
{
  g_return_if_fail (rt && GUPPI_IS_RASTER_TEXT (rt));
  g_return_if_fail (str);

  guppi_text_block_set_text (guppi_raster_text_block (rt), str);
}

void
guppi_raster_text_set_font (GuppiRasterText *rt, GnomeFont *font)
{
  g_return_if_fail (rt && GUPPI_IS_RASTER_TEXT (rt));
  g_return_if_fail (font==NULL || GNOME_IS_FONT (font));

  guppi_text_block_set_font (guppi_raster_text_block (rt), font);
}

void
guppi_raster_text_set_angle (GuppiRasterText *rt, double angle)
{
  g_return_if_fail (rt && GUPPI_IS_RASTER_TEXT (rt));
  guppi_text_block_set_angle (guppi_raster_text_block (rt), angle);
}



/* $Id$ */
