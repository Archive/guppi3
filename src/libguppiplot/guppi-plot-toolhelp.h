/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-plot-toolhelp.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_PLOT_TOOLHELP_H
#define _INC_GUPPI_PLOT_TOOLHELP_H

/* #include <gnome.h> */
#include "guppi-canvas-item.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS 

typedef struct _GuppiPlotToolhelp GuppiPlotToolhelp;
typedef struct _GuppiPlotToolhelpClass GuppiPlotToolhelpClass;

struct _GuppiPlotToolhelp {
  GtkTable parent;
};

struct _GuppiPlotToolhelpClass {
  GtkTableClass parent_class;
};

#define GUPPI_TYPE_PLOT_TOOLHELP (guppi_plot_toolhelp_get_type())
#define GUPPI_PLOT_TOOLHELP(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_PLOT_TOOLHELP,GuppiPlotToolhelp))
#define GUPPI_PLOT_TOOLHELP0(obj) ((obj) ? (GUPPI_PLOT_TOOLHELP(obj)) : NULL)
#define GUPPI_PLOT_TOOLHELP_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_PLOT_TOOLHELP,GuppiPlotToolhelpClass))
#define GUPPI_IS_PLOT_TOOLHELP(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_PLOT_TOOLHELP))
#define GUPPI_IS_PLOT_TOOLHELP0(obj) (((obj) == NULL) || (GUPPI_IS_PLOT_TOOLHELP(obj)))
#define GUPPI_IS_PLOT_TOOLHELP_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_PLOT_TOOLHELP))

GtkType guppi_plot_toolhelp_get_type (void);

GtkWidget *guppi_plot_toolhelp_new (GuppiCanvasItem *, double pt_x,
				    double pt_y);

void guppi_plot_toolhelp_construct (GuppiPlotToolhelp *,
				    GuppiCanvasItem *,
				    double pt_x, double pt_y);



END_GUPPI_DECLS

#endif /* _INC_GUPPI_PLOT_TOOLHELP_H */

/* $Id$ */
