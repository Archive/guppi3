/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-alpha-template.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-alpha-template.h"

#include <math.h>

#include <libgnomeui/gnome-canvas.h>
#include <libgnomeui/gnome-canvas-util.h>

#include <guppi-convenient.h>
#include <guppi-useful.h>


#include <libart_lgpl/libart.h>

static GtkObjectClass *parent_class = NULL;

static void
guppi_alpha_template_finalize (GtkObject *obj)
{
  GuppiAlphaTemplate *atemp = GUPPI_ALPHA_TEMPLATE (obj);

  guppi_finalized (obj);

  if (atemp->data == NULL)
    g_warning ("Re-finalizing alpha template?");
  guppi_free (atemp->data);
  atemp->data = NULL;

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_alpha_template_class_init (GuppiAlphaTemplateClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;

  parent_class = gtk_type_class (GTK_TYPE_OBJECT);

  object_class->finalize = guppi_alpha_template_finalize;

}

static void
guppi_alpha_template_init (GuppiAlphaTemplate *obj)
{

}

GtkType guppi_alpha_template_get_type (void)
{
  static GtkType guppi_alpha_template_type = 0;
  if (!guppi_alpha_template_type) {
    static const GtkTypeInfo guppi_alpha_template_info = {
      "GuppiAlphaTemplate",
      sizeof (GuppiAlphaTemplate),
      sizeof (GuppiAlphaTemplateClass),
      (GtkClassInitFunc) guppi_alpha_template_class_init,
      (GtkObjectInitFunc) guppi_alpha_template_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_alpha_template_type =
      gtk_type_unique (GTK_TYPE_OBJECT, &guppi_alpha_template_info);
  }
  return guppi_alpha_template_type;
}

GuppiAlphaTemplate *
guppi_alpha_template_new (gint w, gint h)
{
  GuppiAlphaTemplate *atemp;

  g_return_val_if_fail (w > 0, NULL);
  g_return_val_if_fail (h > 0, NULL);

  atemp = GUPPI_ALPHA_TEMPLATE (guppi_type_new (guppi_alpha_template_get_type ()));

  atemp->width = w;
  atemp->height = h;
  atemp->data = guppi_new0 (guchar, w * h);

  return atemp;
}

guchar guppi_alpha_template_get (GuppiAlphaTemplate *atemp, gint x, gint y)
{
  g_return_val_if_fail (atemp != NULL, '\0');
  g_return_val_if_fail (x >= 0 && x < atemp->width, '\0');
  g_return_val_if_fail (y >= 0 && y < atemp->height, '\0');

  return guppi_alpha_template_get_unsafe (atemp, x, y);
}

void
guppi_alpha_template_set (GuppiAlphaTemplate *atemp, gint x, gint y,
			  guchar a)
{
  g_return_if_fail (atemp != NULL);
  g_return_if_fail (x >= 0 && x < atemp->width);
  g_return_if_fail (y >= 0 && y < atemp->height);

  guppi_alpha_template_set_unsafe (atemp, x, y, a);
}

GuppiAlphaTemplate *
guppi_alpha_template_copy_rectangle (GuppiAlphaTemplate *atemp,
				     gint x, gint y, gint w, gint h)
{
  GuppiAlphaTemplate *acopy;
  gint x1, y1, i, j;
  guchar val;

  g_return_val_if_fail (atemp != NULL, NULL);
  g_return_val_if_fail (w > 0 && h > 0, NULL);

  x1 = x + w - 1;
  y1 = y + h - 1;

  x = MAX (x, 0);
  y = MAX (y, 0);
  x1 = MIN (x1, atemp->width - 1);
  y1 = MIN (y1, atemp->height - 1);

  if (x1 < x || y1 < y)
    return NULL;

  acopy = guppi_alpha_template_new (x1 - x + 1, y1 - y + 1);

  for (i = x; i <= x1; ++i) {
    for (j = y; j <= y1; ++j) {
      val = guppi_alpha_template_get_unsafe (atemp, i, j);
      guppi_alpha_template_set_unsafe (acopy, i - x, j - y, val);
    }
  }

  return acopy;
}

void
guppi_alpha_template_auto_crop (GuppiAlphaTemplate *atemp)
{
  GuppiAlphaTemplate *acopy;
  gint x0, y0, x1, y1, i, j;
  gboolean empty_line;
  guchar *temp;

  g_return_if_fail (atemp != NULL);

  /* First, we find a bounding box */

  empty_line = TRUE;
  for (x0 = 0; x0 < atemp->width && empty_line; ++x0) {
    for (j = 0; j < atemp->height && empty_line; ++j) {
      if (guppi_alpha_template_get_unsafe (atemp, x0, j) != 0)
	empty_line = FALSE;
    }
  }
  --x0;
  atemp->x_base_point -= x0;

  empty_line = TRUE;
  for (x1 = atemp->width - 1; x1 >= x0 && empty_line; --x1) {
    for (j = 0; j < atemp->height && empty_line; ++j) {
      if (guppi_alpha_template_get_unsafe (atemp, x1, j) != 0)
	empty_line = FALSE;
    }
  }
  ++x1;

  empty_line = TRUE;
  for (y0 = 0; y0 < atemp->height && empty_line; ++y0) {
    for (i = x0; i <= x1 && empty_line; ++i) {
      if (guppi_alpha_template_get_unsafe (atemp, i, y0) != 0)
	empty_line = FALSE;
    }
  }
  --y0;
  atemp->y_base_point -= y0;


  empty_line = TRUE;
  for (y1 = atemp->height - 1; y1 >= y0 && empty_line; --y1) {
    for (i = x0; i <= x1 && empty_line; ++i) {
      if (guppi_alpha_template_get_unsafe (atemp, i, y1) != 0)
	empty_line = FALSE;
    }
  }
  ++y1;

  if (x0 == 0 && y0 == 0 && x1 == atemp->width - 1 && y1 == atemp->height - 1)
    return;

  acopy =
    guppi_alpha_template_copy_rectangle (atemp, x0, y0, x1 - x0 + 1,
					 y1 - y0 + 1);

  /* This isn't very polite */
  temp = atemp->data;
  atemp->data = acopy->data;
  acopy->data = temp;
  atemp->width = acopy->width;
  atemp->height = acopy->height;
  guppi_unref (acopy);
}

void
guppi_alpha_template_print (GuppiAlphaTemplate *atemp,
			    gint x, gint y,
			    guint rnow, guint gnow, guint bnow, guint anow,
			    GnomeCanvasBuf *buf)
{
  gint w, h;

  /* Trying to print the NULL alpha template is just a no-op */
  if (atemp == NULL)
    return;

  g_return_if_fail (buf != NULL);

  if (anow == 0)
    return;

  /* We use the funny variable names (and the declarations inside the if()
     block rather than at the top) so that this chunk of code can be pasted
     into guppi_scatter_style_paint() w/o changes. */

  w = buf->rect.x1 - buf->rect.x0;
  h = buf->rect.y1 - buf->rect.y0;

  x -= buf->rect.x0;
  y -= buf->rect.y0;

  x -= atemp->x_base_point;
  y -= atemp->y_base_point;

  if (x + atemp->width >= 0 && x < w && y + atemp->height >= 0 && y < h) {

    gint atpr_x0, atpr_y0, atpr_x1, atpr_y1;
    gint atpr_i, atpr_j;
    guchar *atpr_ap;
    guchar *atpr_arun;
    guchar *atpr_rgbp;
    gchar *atpr_rgbrun;

    /* Clip to our viewing area */
    atpr_x0 = MAX (x, 0);
    atpr_y0 = MAX (y, 0);
    atpr_x1 = MIN (x + atemp->width - 1, w - 1);
    atpr_y1 = MIN (y + atemp->height - 1, h - 1);

    atpr_ap = atemp->data + atemp->width * (atpr_y0 - y) + (atpr_x0 - x);
    atpr_rgbp = buf->buf + buf->buf_rowstride * atpr_y0 + 3 * atpr_x0;
    for (atpr_j = atpr_y0; atpr_j <= atpr_y1; ++atpr_j) {
      atpr_arun = atpr_ap;
      atpr_rgbrun = atpr_rgbp;
      for (atpr_i = atpr_x0; atpr_i <= atpr_x1; ++atpr_i) {
	if (*atpr_arun > 0) {
	  PIXEL_RGBA (atpr_rgbrun, rnow, gnow, bnow,
		      ((1 + anow) * (1 + *atpr_arun)) >> 8);
	}
	++atpr_arun;
	atpr_rgbrun += 3;
      }
      atpr_ap += atemp->width;
      atpr_rgbp += buf->buf_rowstride;
    }
  }
}

void
guppi_alpha_template_gradient_print (GuppiAlphaTemplate *atemp,
				     gint x, gint y,
				     guint rgba1, guint rgba2,
				     guppi_compass_t start,
				     GnomeCanvasBuf *buf)
{
  gint w, h;
  guint c, rnow, gnow, bnow, anow;
  double t;

  /* Trying to print the NULL alpha template is just a no-op */
  if (atemp == NULL)
    return;

  g_return_if_fail (buf != NULL);

  /* We use the funny variable names (and the declarations inside the if()
     block rather than at the top) so that this chunk of code can be pasted
     into guppi_scatter_style_paint() w/o changes. */

  w = buf->rect.x1 - buf->rect.x0;
  h = buf->rect.y1 - buf->rect.y0;

  x -= buf->rect.x0;
  y -= buf->rect.y0;

  x -= atemp->x_base_point;
  y -= atemp->y_base_point;

  if (x + atemp->width >= 0 && x < w && y + atemp->height >= 0 && y < h) {

    gint atpr_x0, atpr_y0, atpr_x1, atpr_y1;
    gint atpr_i, atpr_j;
    guchar *atpr_ap;
    guchar *atpr_arun;
    guchar *atpr_rgbp;
    gchar *atpr_rgbrun;

    /* Clip to our viewing area */
    atpr_x0 = MAX (x, 0);
    atpr_y0 = MAX (y, 0);
    atpr_x1 = MIN (x + atemp->width - 1, w - 1);
    atpr_y1 = MIN (y + atemp->height - 1, h - 1);

    atpr_ap = atemp->data + atemp->width * (atpr_y0 - y) + (atpr_x0 - x);
    atpr_rgbp = buf->buf + buf->buf_rowstride * atpr_y0 + 3 * atpr_x0;
    for (atpr_j = atpr_y0; atpr_j <= atpr_y1; ++atpr_j) {
      atpr_arun = atpr_ap;
      atpr_rgbrun = atpr_rgbp;

      t = (atpr_j-y) / (double) (atemp->height-1);
      c = UINT_INTERPOLATE (rgba1, rgba2, t);
      UINT_TO_RGBA (c, &rnow, &gnow, &bnow, &anow);

      for (atpr_i = atpr_x0; atpr_i <= atpr_x1; ++atpr_i) {
	if (*atpr_arun > 0) {
	  PIXEL_RGBA (atpr_rgbrun, rnow, gnow, bnow,
		      ((1 + anow) * (1 + *atpr_arun)) >> 8);
	}
	++atpr_arun;
	atpr_rgbrun += 3;
      }
      atpr_ap += atemp->width;
      atpr_rgbp += buf->buf_rowstride;
    }
  }
}

/**************************************************************************/

GuppiAlphaTemplate *
guppi_alpha_template_new_frame (gint w, gint h)
{
  GuppiAlphaTemplate *at;
  gint i, j;

  at = guppi_alpha_template_new (w, h);

  for (i = 0; i < w; ++i)
    for (j = 0; j < h; ++j)
      guppi_alpha_template_set (at, i, j, 0x80);

  for (i = 0; i < w; ++i) {
    guppi_alpha_template_set (at, i, 0, 0xff);
    guppi_alpha_template_set (at, i, h - 1, 0xff);
  }
  for (i = 0; i < h; ++i) {
    guppi_alpha_template_set (at, 0, i, 0xff);
    guppi_alpha_template_set (at, w - 1, i, 0xff);
  }

  return at;
}

/* A small experiment in buffering... */
#define CIRCLE_MIN 0
#define CIRCLE_MAX 50
#define CIRCLE_STEP 0.25
GuppiAlphaTemplate *
guppi_alpha_template_new_circle (double radius)
{
  static GuppiAlphaTemplate **buffer = NULL;
  const gint slice = 3;
  const gdouble side = 1.0 / slice;
  gint span = (gint) ceil (2 * radius + 1);
  double cent;
  GuppiAlphaTemplate *atemp;
  gint buffer_i = -1;
  gint i, j, ii, jj, count;
  double ip, jp, rr, rt0, rt1, D;
  guchar val;

  g_return_val_if_fail (radius > 0, NULL);

  if (buffer == NULL) {
    buffer = guppi_new0 (GuppiAlphaTemplate *,
		     (gint) ceil ((CIRCLE_MAX - CIRCLE_MIN) / CIRCLE_STEP));
    guppi_permanent_alloc (buffer);
  }
  if (CIRCLE_MIN <= radius && radius <= CIRCLE_MAX) {
    buffer_i = (radius - CIRCLE_MIN) / CIRCLE_STEP;
    if ((atemp = buffer[buffer_i])) {
      guppi_ref (atemp);
      return atemp;
    }
  }

  cent = span / 2.0;
  rr = radius * radius;

  rt0 = 0;
  if (radius > M_SQRT2)
    rt0 = (radius - M_SQRT2) * (radius - M_SQRT2);

  rt1 = (radius + M_SQRT2) * (radius + M_SQRT2);

  atemp = guppi_alpha_template_new (span, span);
  atemp->x_base_point = span / 2;
  atemp->y_base_point = span / 2;

  for (i = 0; i <= span / 2; ++i) {
    for (j = i; j <= span / 2; ++j) {
      count = 0;
      D = (i - cent) * (i - cent) + (j - cent) * (j - cent);
      if (D < rt0) {
	count = slice * slice;
      } else if (D < rt1) {
	for (ii = 0; ii < slice; ++ii)
	  for (jj = 0; jj < slice; ++jj) {
	    ip = i + (ii + 0.5) * side - cent;
	    jp = j + (jj + 0.5) * side - cent;
	    if (ip * ip + jp * jp <= rr)
	      ++count;
	  }
      }
      val = count * 0xff / (slice * slice);
      guppi_alpha_template_set_unsafe (atemp, i, j, val);
      guppi_alpha_template_set_unsafe (atemp, j, i, val);
      guppi_alpha_template_set_unsafe (atemp, i, span - 1 - j, val);
      guppi_alpha_template_set_unsafe (atemp, j, span - 1 - i, val);
      guppi_alpha_template_set_unsafe (atemp, span - 1 - i, j, val);
      guppi_alpha_template_set_unsafe (atemp, span - 1 - j, i, val);
      guppi_alpha_template_set_unsafe (atemp, span - 1 - i, span - 1 - j,
				       val);
      guppi_alpha_template_set_unsafe (atemp, span - 1 - j, span - 1 - i,
				       val);
    }
  }

  guppi_alpha_template_auto_crop (atemp);

  if (buffer_i >= 0) {
    buffer[buffer_i] = atemp;
    guppi_permanent_alloc (atemp);
    guppi_permanent_alloc (atemp->data);
    guppi_ref (atemp);
  }

  return atemp;
}


GuppiAlphaTemplate *
guppi_alpha_template_new_ring (double radius, double width)
{
  const gint slice = 3;
  const gdouble side = 1.0 / slice;
  gint span = (gint) ceil (2 * radius + width + 1);
  double cent;
  GuppiAlphaTemplate *atemp;
  gint i, j, ii, jj, count;
  double ip, jp, rr0, rr1, dd, rt0, rt1;
  guchar val;

  g_return_val_if_fail (radius > 0, NULL);
  g_return_val_if_fail (width > 0, NULL);

  cent = span / 2.0;

  rr0 = (radius - width / 2) * (radius - width / 2);
  rr1 = (radius + width / 2) * (radius + width / 2);
  rt0 = 0;
  if (radius - width / 2 - M_SQRT2 > 0) {
    rt0 = (radius - width / 2 - M_SQRT2) * (radius - width / 2 - M_SQRT2);
  }
  rt1 = (radius + width / 2 + M_SQRT2) * (radius + width / 2 + M_SQRT2);

  atemp = guppi_alpha_template_new (span, span);
  atemp->x_base_point = span / 2;
  atemp->y_base_point = span / 2;

  for (i = 0; i <= span / 2; ++i) {
    for (j = i; j <= span / 2; ++j) {
      count = 0;
      dd = (i - cent) * (i - cent) + (j - cent) * (j - cent);
      if (rt0 < dd && dd < rt1) {
	for (ii = 0; ii < slice; ++ii)
	  for (jj = 0; jj < slice; ++jj) {
	    ip = i + (ii + 0.5) * side - cent;
	    jp = j + (jj + 0.5) * side - cent;
	    dd = ip * ip + jp * jp;
	    if (rr0 <= dd && dd <= rr1)
	      ++count;
	  }
      }
      val = count * 0xff / (slice * slice);
      guppi_alpha_template_set_unsafe (atemp, i, j, val);
      guppi_alpha_template_set_unsafe (atemp, j, i, val);
      guppi_alpha_template_set_unsafe (atemp, i, span - 1 - j, val);
      guppi_alpha_template_set_unsafe (atemp, j, span - 1 - i, val);
      guppi_alpha_template_set_unsafe (atemp, span - 1 - i, j, val);
      guppi_alpha_template_set_unsafe (atemp, span - 1 - j, i, val);
      guppi_alpha_template_set_unsafe (atemp, span - 1 - i, span - 1 - j,
				       val);
      guppi_alpha_template_set_unsafe (atemp, span - 1 - j, span - 1 - i,
				       val);
    }
  }

  guppi_alpha_template_auto_crop (atemp);

  return atemp;
}

static double
distsq_to_segment (double x, double y,
		   double x0, double y0, double x1, double y1)
{
  double dx = x1 - x0;
  double dy = y1 - y0;
  double t;
  double ax, ay;

  t = (dx * (x - x0) + dy * (y - y0)) / (dx * dx + dy * dy);
  t = CLAMP (t, 0, 1);

  ax = x0 + dx * t - x;
  ay = y0 + dy * t - y;

  return ax * ax + ay * ay;
}

GuppiAlphaTemplate *
guppi_alpha_template_new_cross (double radius, double width, double theta)
{
  const gint slice = 3;
  const gdouble side = 1.0 / slice;
  gint span = (gint) ceil (2 * radius + width + 1);
  double cent;
  GuppiAlphaTemplate *atemp;
  gint i, j, ii, jj, count, top;
  double im, jm, ww, sth, cth, tx, ty;
  guchar val;
  gboolean symmetry;

  g_return_val_if_fail (radius > 0, NULL);
  g_return_val_if_fail (width > 0, NULL);

  cent = span / 2.0;
  sth = sin (-theta);
  cth = cos (-theta);
  ww = width / 2;

  symmetry = FALSE;
  if (fabs (fmod (4 * theta / M_PI, 1)) < 1e-8)
    symmetry = TRUE;

  atemp = guppi_alpha_template_new (span, span);
  atemp->x_base_point = span / 2;
  atemp->y_base_point = span / 2;

  top = symmetry ? span / 2 : span - 1;

  for (i = 0; i <= top; ++i) {
    for (j = symmetry ? i : 0; j <= top; ++j) {
      count = 0;
      for (ii = 0; ii < slice; ++ii)
	for (jj = 0; jj < slice; ++jj) {
	  im = i + (ii + 0.5) * side - cent;
	  jm = j + (jj + 0.5) * side - cent;
	  tx = cth * im + sth * jm;
	  ty = -sth * im + cth * jm;
	  if ((fabs (tx) <= 2 * radius && fabs (ty) <= ww) ||
	      (fabs (ty) <= 2 * radius && fabs (tx) <= ww))
	    ++count;
	}
      val = count * 0xff / (slice * slice);
      guppi_alpha_template_set_unsafe (atemp, i, j, val);
      if (symmetry) {
	guppi_alpha_template_set_unsafe (atemp, j, i, val);
	guppi_alpha_template_set_unsafe (atemp, span - 1 - i, j, val);
	guppi_alpha_template_set_unsafe (atemp, span - 1 - j, i, val);
	guppi_alpha_template_set_unsafe (atemp, i, span - 1 - j, val);
	guppi_alpha_template_set_unsafe (atemp, j, span - 1 - i, val);
	guppi_alpha_template_set_unsafe (atemp, span - 1 - i, span - 1 - j,
					 val);
	guppi_alpha_template_set_unsafe (atemp, span - 1 - j, span - 1 - i,
					 val);
      }
    }
  }

  guppi_alpha_template_auto_crop (atemp);

  return atemp;
}

GuppiAlphaTemplate *
guppi_alpha_template_new_ast (double radius, double width, double theta)
{
  const gint slice = 3;
  const gdouble side = 1.0 / slice;
  gint span = (gint) ceil (2 * radius + width + 0.5);
  double cent, px, py, px2, py2;
  GuppiAlphaTemplate *atemp;
  gint i, j, ii, jj, count, top;
  double im, jm, ww;
  guchar val;
  gboolean symmetry;

  g_return_val_if_fail (radius > 0, NULL);
  g_return_val_if_fail (width > 0, NULL);

  symmetry = FALSE;
  if (fabs (fmod (4 * theta / M_PI, 1)) < 1e-8)
    symmetry = TRUE;

  cent = span / 2.0;
  px = radius * cos (theta);
  py = radius * sin (theta);
  px2 = 0.75 * radius * cos (theta + M_PI / 4);
  py2 = 0.75 * radius * sin (theta + M_PI / 4);
  ww = width * width / 4;

  atemp = guppi_alpha_template_new (span, span);
  atemp->x_base_point = span / 2;
  atemp->y_base_point = span / 2;

  top = symmetry ? span / 2 : span - 1;

  for (i = 0; i <= top; ++i) {
    for (j = symmetry ? i : 0; j <= top; ++j) {
      count = 0;
      for (ii = 0; ii < slice; ++ii)
	for (jj = 0; jj < slice; ++jj) {
	  im = i + (ii + 0.5) * side;
	  jm = j + (jj + 0.5) * side;
	  if (distsq_to_segment (im, jm,
				 cent + px, cent + py,
				 cent - px, cent - py) <= ww ||
	      distsq_to_segment (im, jm,
				 cent - py, cent + px,
				 cent + py, cent - px) <= ww ||
	      distsq_to_segment (im, jm,
				 cent + px2, cent + py2,
				 cent - px2, cent - py2) <= ww ||
	      distsq_to_segment (im, jm,
				 cent - py2, cent + px2,
				 cent + py2, cent - px2) <= ww)
	    ++count;
	}
      val = count * 0xff / (slice * slice);
      guppi_alpha_template_set_unsafe (atemp, i, j, val);
      if (symmetry) {
	guppi_alpha_template_set_unsafe (atemp, j, i, val);
	guppi_alpha_template_set_unsafe (atemp, span - 1 - i, j, val);
	guppi_alpha_template_set_unsafe (atemp, span - 1 - j, i, val);
	guppi_alpha_template_set_unsafe (atemp, i, span - 1 - j, val);
	guppi_alpha_template_set_unsafe (atemp, j, span - 1 - i, val);
	guppi_alpha_template_set_unsafe (atemp, span - 1 - i, span - 1 - j,
					 val);
	guppi_alpha_template_set_unsafe (atemp, span - 1 - j, span - 1 - i,
					 val);
      }
    }
  }

  guppi_alpha_template_auto_crop (atemp);

  return atemp;
}

GuppiAlphaTemplate *
guppi_alpha_template_new_rectangle (double radius, double width, double theta)
{
  const gint slice = 3;
  const gdouble side = 1.0 / slice;
  gint span = (gint) ceil (2 * radius + 4 * width + 1);
  double cent, px, py;
  GuppiAlphaTemplate *atemp;
  gint i, j, ii, jj, count, top;
  double im, jm, sth, cth, tx, ty, ww, len;
  guchar val;
  gboolean symmetry;

  g_return_val_if_fail (radius > 0, NULL);
  g_return_val_if_fail (width > 0, NULL);

  cent = span / 2.0;
  sth = sin (-theta);
  cth = cos (-theta);
  px = radius * cos (theta);
  py = radius * sin (theta);
  ww = width / 2 + 1e-8;
  len = radius / sqrt (2) + ww;

  symmetry = FALSE;
  if (fabs (fmod (4 * theta / M_PI, 1)) < 1e-8)
    symmetry = TRUE;

  atemp = guppi_alpha_template_new (span, span);
  atemp->x_base_point = span / 2;
  atemp->y_base_point = span / 2;

  top = symmetry ? span / 2 : span - 1;

  for (i = 0; i <= top; ++i) {
    for (j = symmetry ? i : 0; j <= top; ++j) {
      count = 0;
      for (ii = 0; ii < slice; ++ii)
	for (jj = 0; jj < slice; ++jj) {
	  im = i + (ii + 0.5) * side - cent;
	  jm = j + (jj + 0.5) * side - cent;
	  tx = cth * im + sth * jm;
	  ty = -sth * im + cth * jm;

	  if (
	      (fabs (tx) <= len + ww
	       && (fabs (len - ty) <= ww || fabs (-len - ty) <= ww))
	      || (fabs (ty) <= len + ww
		  && (fabs (len - tx) <= ww || fabs (-len - tx) <= ww)))
	    ++count;
	}
      val = count * 0xff / (slice * slice);
      guppi_alpha_template_set_unsafe (atemp, i, j, val);
      if (symmetry) {
	guppi_alpha_template_set_unsafe (atemp, j, i, val);
	guppi_alpha_template_set_unsafe (atemp, span - 1 - i, j, val);
	guppi_alpha_template_set_unsafe (atemp, span - 1 - j, i, val);
	guppi_alpha_template_set_unsafe (atemp, i, span - 1 - j, val);
	guppi_alpha_template_set_unsafe (atemp, j, span - 1 - i, val);
	guppi_alpha_template_set_unsafe (atemp, span - 1 - i, span - 1 - j,
					 val);
	guppi_alpha_template_set_unsafe (atemp, span - 1 - j, span - 1 - i,
					 val);
      }
    }
  }

  guppi_alpha_template_auto_crop (atemp);

  return atemp;
}

GuppiAlphaTemplate *
guppi_alpha_template_new_box (double radius, double theta)
{
  const gint slice = 3;
  const gdouble side = 1.0 / slice;
  gint span = (gint) ceil (2 * radius + 1), top;
  double cent;
  GuppiAlphaTemplate *atemp;
  gint i, j, ii, jj, count;
  double im, jm;
  guchar val;
  double sth, cth, rsq2, tx, ty;
  gboolean symmetry;

  g_return_val_if_fail (radius > 0, NULL);

  cent = span / 2.0;
  sth = sin (-theta);
  cth = cos (-theta);
  rsq2 = radius / sqrt (2);

  symmetry = FALSE;
  if (fabs (fmod (4 * theta / M_PI, 1)) < 1e-8)
    symmetry = TRUE;

  atemp = guppi_alpha_template_new (span, span);
  atemp->x_base_point = span / 2;
  atemp->y_base_point = span / 2;

  top = symmetry ? span / 2 : span - 1;

  for (i = 0; i <= top; ++i) {
    for (j = symmetry ? i : 0; j <= top; ++j) {
      count = 0;
      for (ii = 0; ii < slice; ++ii)
	for (jj = 0; jj < slice; ++jj) {
	  im = i + (ii + 0.5) * side - cent;
	  jm = j + (jj + 0.5) * side - cent;

	  tx = cth * im + sth * jm;
	  ty = -sth * im + cth * jm;

	  if (-rsq2 <= tx && tx <= rsq2 && -rsq2 <= ty && ty <= rsq2)
	    ++count;
	}
      val = count * 0xff / (slice * slice);
      guppi_alpha_template_set_unsafe (atemp, i, j, val);
      if (symmetry) {
	guppi_alpha_template_set_unsafe (atemp, j, i, val);
	guppi_alpha_template_set_unsafe (atemp, span - 1 - i, j, val);
	guppi_alpha_template_set_unsafe (atemp, span - 1 - j, i, val);
	guppi_alpha_template_set_unsafe (atemp, i, span - 1 - j, val);
	guppi_alpha_template_set_unsafe (atemp, j, span - 1 - i, val);
	guppi_alpha_template_set_unsafe (atemp, span - 1 - i, span - 1 - j,
					 val);
	guppi_alpha_template_set_unsafe (atemp, span - 1 - j, span - 1 - i,
					 val);
      }
    }
  }

  guppi_alpha_template_auto_crop (atemp);

  return atemp;
}

GuppiAlphaTemplate *
guppi_alpha_template_new_triangle (double radius, double width, double theta)
{
  GuppiAlphaTemplate *atemp;
  const gint slice = 3;
  const double side = 1.0 / slice;
  gint span = (gint) ceil (2 * radius + width + 1);
  double cent = span / 2.0;
  gint i, j, ii, jj, count, val;
  double ip, jp;
  double x0, y0, x1, y1, x2, y2, d, r, RR;

  x0 =  radius * cos (M_PI/2 + theta);
  y0 = -radius * sin (M_PI/2 + theta);

  x1 = radius * cos (M_PI/2 + 2*M_PI/3 + theta);
  y1 = -radius * sin (M_PI/2 + 2*M_PI/3 + theta);
  
  x2 = radius * cos (M_PI/2 + 4*M_PI/3 + theta);
  y2 = -radius * sin (M_PI/2 + 4*M_PI/3 + theta);

  RR = (radius + width) * (radius + width);

  atemp = guppi_alpha_template_new (span, span);
  atemp->x_base_point = span / 2;
  atemp->y_base_point = span / 2;
    
  for (i = 0; i < span; ++i) {
    for (j = 0; j < span; ++j) {

      count = 0;

      for (ii = 0; ii < slice; ++ii) {
	for (jj = 0; jj < slice; ++jj) {
	  ip = i + (ii + 0.5) * side - cent;
	  jp = j + (jj + 0.5) * side - cent;

	  r = ip * ip + jp * jp;

	  if (r < RR) {

	    d = distsq_to_segment (ip, jp, x0, y0, x1, y1);
	    if (d > width / 2)
	      d = distsq_to_segment (ip, jp, x1, y1, x2, y2);
	    if (d > width / 2)
	      d = distsq_to_segment (ip, jp, x0, y0, x2, y2);
	    
	    if (d <= width / 2)
	      ++count;

	  }

	}
      }

      val = count * 0xff / (slice * slice);
      guppi_alpha_template_set_unsafe (atemp, i, j, val);
    }
  }

  guppi_alpha_template_auto_crop (atemp);

  return atemp;
}

static gint
half_plane (double x, double y,
	    double x0, double y0, double x1, double y1)
{
  const double epsilon = 1e-8;

  double dx = x1 - x0;
  double dy = y1 - y0;
  double t = - (x - x0) * dy + (y - y0) * dx;

  if (t > epsilon)
    return +1;
  if (t < -epsilon)
    return -1;
  return 0;
}

GuppiAlphaTemplate *
guppi_alpha_template_new_filled_triangle (double radius, double theta)
{
  GuppiAlphaTemplate *atemp;
  const gint slice = 3;
  const double side = 1.0 / slice;
  gint span = (gint) ceil (2 * radius + 1);
  double cent = span / 2.0;
  gint i, j, ii, jj, count, val;
  double ip, jp;
  double x0, y0, x1, y1, x2, y2, r, R_lower, R_upper;

  x0 =  radius * cos (M_PI/2 + theta);
  y0 = -radius * sin (M_PI/2 + theta);

  x1 = radius * cos (M_PI/2 + 2*M_PI/3 + theta);
  y1 = -radius * sin (M_PI/2 + 2*M_PI/3 + theta);
  
  x2 = radius * cos (M_PI/2 + 4*M_PI/3 + theta);
  y2 = -radius * sin (M_PI/2 + 4*M_PI/3 + theta);

  R_lower = radius * radius / 4;
  R_upper = (radius + 1) * (radius + 1);

  atemp = guppi_alpha_template_new (span, span);
  atemp->x_base_point = span / 2;
  atemp->y_base_point = span / 2;
    
  for (i = 0; i < span; ++i) {
    for (j = 0; j < span; ++j) {

      count = 0;

      for (ii = 0; ii < slice; ++ii) {
	for (jj = 0; jj < slice; ++jj) {
	  ip = i + (ii + 0.5) * side - cent;
	  jp = j + (jj + 0.5) * side - cent;

	  r = ip * ip + jp * jp;

	  if (r < R_lower) {
	    ++count;
	  } else if (r < R_upper) {

	    if (half_plane (ip, jp, x0, y0, x1, y1) <= 0
		&& half_plane (ip, jp, x1, y1, x2, y2) <= 0
		&& half_plane (ip, jp, x2, y2, x0, y0) <= 0)
	      ++count;

	  }

	}
      }

      val = count * 0xff / (slice * slice);
      guppi_alpha_template_set_unsafe (atemp, i, j, val);
    }
  }

  guppi_alpha_template_auto_crop (atemp);

  return atemp;
}


GuppiAlphaTemplate *
guppi_alpha_template_new_bar (double r_forward, double r_back, double width, double theta)
{
  GuppiAlphaTemplate *atemp;
  const gint slice = 3;
  const double side = 1.0 / slice;
  double r_max = MAX (r_forward, r_back);
  gint span = (gint) ceil (2 * r_max + width + 1);
  double cent = span / 2;
  gint i, j, ii, jj, count, val;
  double ip, jp, x0, y0, x1, y1;
  double r, RR, d;

  x0 = r_forward * cos (theta);
  y0 = r_forward * sin (theta);
  
  x1 = -r_back * cos (theta);
  y1 = -r_back * sin (theta);

  RR = (r_max + width) * (r_max + width);

  atemp = guppi_alpha_template_new (span, span);
  atemp->x_base_point = span / 2;
  atemp->y_base_point = span / 2;

  for (i = 0; i < span; ++i) {
    for (j = 0; j < span; ++j) {

      count = 0;

      for (ii = 0; ii < slice; ++ii) {
	for (jj = 0; jj < slice; ++jj) {
	  ip = i + (ii + 0.5) * side - cent;
	  jp = j + (jj + 0.5) * side - cent;

	  r = ip * ip + jp * jp;

	  if (r < RR) {
	    d = distsq_to_segment (ip, jp, x0, y0, x1, y1);
	    if (d <= width / 2)
	      ++count;
	  }
	}
      }

      val = count * 0xff / (slice * slice);
      guppi_alpha_template_set_unsafe (atemp, i, j, val);
    }
  }

  guppi_alpha_template_auto_crop (atemp);

  return atemp;
}


/**************************************************************************/

GuppiAlphaTemplate *
guppi_alpha_template_text_general (GnomeFont *font, double scale,
				   const gchar *text,
				   double angle,
				   gboolean filled, double outline_width)
{
  gint w, h;
  double affine[6];
  double rot[6];
  GnomeCanvasBuf *buf;
  GuppiAlphaTemplate *atemp;
  const GnomeFontFace *face;
  guchar *p;
  ArtBpath *bpath;
  ArtVpath *vpath;
  ArtSVP *svp;
  ArtIRect bbox;
  gint i, j;
  double size;

  g_return_val_if_fail (font != NULL, NULL);
  g_return_val_if_fail (scale > 0, NULL);

  if (text == NULL || !text[0])
    return NULL;

  face = gnome_font_get_face (font);
  g_return_val_if_fail (face != NULL, NULL);

  size = guppi_pt2px (gnome_font_get_size (font) * scale);

  /* First, compute bounding box for the text */

  art_affine_scale (affine, size * 0.001, size * -0.001);
  art_affine_rotate (rot, angle);
  art_affine_multiply (affine, affine, rot);

  for (i = 0; text[i]; ++i) {
    ArtPoint adv;
    ArtDRect w_bbox, gbbox;
    ArtIRect wbi;
    gint glyph = gnome_font_face_lookup_default (face, text[i]);

    gnome_font_face_get_glyph_stdbbox (face, glyph, &gbbox);
    gnome_font_face_get_glyph_stdadvance (face, glyph, &adv);

    art_drect_affine_transform (&w_bbox, &gbbox, affine);
    art_drect_to_irect (&wbi, &w_bbox);
    if (i == 0)
      bbox = wbi;
    else
      art_irect_union (&bbox, &bbox, &wbi);
    affine[4] += adv.x * affine[0];
    affine[5] += adv.x * affine[1];
  }

  w = bbox.x1 - bbox.x0;
  h = bbox.y1 - bbox.y0;

  /* Next, create a buffer to render the text into */
  buf = guppi_new0 (GnomeCanvasBuf, 1);
  buf->buf = guppi_new0 (guchar, w * h * 3);
  buf->buf_rowstride = w * 3;
  buf->rect = bbox;
  buf->bg_color = 0;
  buf->is_bg = TRUE;
  buf->is_buf = FALSE;

  /* Punch the text info our buffer */

  art_affine_scale (affine, size * 0.001, size * -0.001);
  art_affine_rotate (rot, angle);
  art_affine_multiply (affine, affine, rot);

  if (outline_width < 0.5)
    outline_width = 0.5;

  for (i = 0; text[i]; ++i) {
    ArtPoint adv;
    const ArtBpath *path;
    gint glyph = gnome_font_face_lookup_default (face, text[i]);

    gnome_font_face_get_glyph_stdadvance (face, glyph, &adv);
    path = gnome_font_face_get_glyph_stdoutline (face, glyph);

    bpath = art_bpath_affine_transform (path, affine);
    vpath = art_bez_path_to_vec (bpath, 0.25);
    art_free (bpath);

    if (filled) {
      svp = art_svp_from_vpath (vpath);
    } else {
      svp = art_svp_vpath_stroke (vpath,
				  gnome_canvas_join_gdk_to_art
				  (GDK_JOIN_MITER),
				  gnome_canvas_cap_gdk_to_art (GDK_CAP_BUTT),
				  outline_width, 4, 0.25);
    }

    gnome_canvas_render_svp (buf, svp, 0xffffffff);
    art_svp_free (svp);

    art_free (vpath);

    affine[4] += adv.x * affine[0];
    affine[5] += adv.x * affine[1];
  }

  /* Copy our buffer into an alpha template object */
  atemp = guppi_alpha_template_new (w, h);
  p = buf->buf;
  for (j = 0; j < h; ++j) {
    for (i = 0; i < w; ++i) {
      guppi_alpha_template_set_unsafe (atemp, i, j, *p);
      p += 3;
    }
  }

  /* Free our buffer */
  guppi_free (buf->buf);
  guppi_free (buf);

  return atemp;
}

/* $Id$ */
