/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-alpha-template.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_ALPHA_TEMPLATE_H
#define _INC_GUPPI_ALPHA_TEMPLATE_H

/* #include <gnome.h> */
#include <libgnomeui/gnome-canvas.h>
#include <libgnomeprint/gnome-font.h>
#include <guppi-enums.h>

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS

typedef struct _GuppiAlphaTemplate GuppiAlphaTemplate;
typedef struct _GuppiAlphaTemplateClass GuppiAlphaTemplateClass;

struct _GuppiAlphaTemplate {
  GtkObject parent;

  gint x_base_point, y_base_point;
  gint width, height;
  guchar *data;
};

struct _GuppiAlphaTemplateClass {
  GtkObjectClass parent_class;
};

#define GUPPI_TYPE_ALPHA_TEMPLATE (guppi_alpha_template_get_type())
#define GUPPI_ALPHA_TEMPLATE(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_ALPHA_TEMPLATE,GuppiAlphaTemplate))
#define GUPPI_ALPHA_TEMPLATE0(obj) ((obj) ? (GUPPI_ALPHA_TEMPLATE(obj)) : NULL)
#define GUPPI_ALPHA_TEMPLATE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_ALPHA_TEMPLATE,GuppiAlphaTemplateClass))
#define GUPPI_IS_ALPHA_TEMPLATE(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_ALPHA_TEMPLATE))
#define GUPPI_IS_ALPHA_TEMPLATE0(obj) (((obj) == NULL) || (GUPPI_IS_ALPHA_TEMPLATE(obj)))
#define GUPPI_IS_ALPHA_TEMPLATE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_ALPHA_TEMPLATE))

GtkType guppi_alpha_template_get_type (void);

GuppiAlphaTemplate *guppi_alpha_template_new (gint w, gint h);

guchar guppi_alpha_template_get (GuppiAlphaTemplate *, gint x, gint y);
void guppi_alpha_template_set (GuppiAlphaTemplate *, gint x, gint y,
			       guchar a);

void guppi_alpha_template_print (GuppiAlphaTemplate *,
				 gint x, gint y,
				 guint r, guint g, guint b, guint a,
				 GnomeCanvasBuf *);
void guppi_alpha_template_gradient_print (GuppiAlphaTemplate *,
					  gint x, gint y,
					  guint32 rgba1, guint32 rgba2,
					  guppi_compass_t start,
					  GnomeCanvasBuf *);

GuppiAlphaTemplate *guppi_alpha_template_copy_rectangle (GuppiAlphaTemplate *,
							 gint x, gint y,
							 gint w, gint h);
void guppi_alpha_template_auto_crop (GuppiAlphaTemplate *);

#define guppi_alpha_template_get_unsafe(atemp, x, y) \
((atemp)->data[(atemp)->width * (y) + (x)])

#define guppi_alpha_template_set_unsafe(atemp, x, y, val) \
((atemp)->data[(atemp)->width * (y) + (x)] = val)

GuppiAlphaTemplate *guppi_alpha_template_new_frame (gint w, gint h);
GuppiAlphaTemplate *guppi_alpha_template_new_circle (double r);
GuppiAlphaTemplate *guppi_alpha_template_new_ring (double r, double w);
GuppiAlphaTemplate *guppi_alpha_template_new_cross (double r, double w,
						    double theta);
GuppiAlphaTemplate *guppi_alpha_template_new_ast (double r, double w,
						  double theta);
GuppiAlphaTemplate *guppi_alpha_template_new_rectangle (double r, double w,
							double theta);
GuppiAlphaTemplate *guppi_alpha_template_new_box (double r, double th);

GuppiAlphaTemplate *guppi_alpha_template_new_triangle (double r, double w, double th);
GuppiAlphaTemplate *guppi_alpha_template_new_filled_triangle (double r, double th);

GuppiAlphaTemplate *guppi_alpha_template_new_bar (double r_forward, double r_back, double w, double theta);

GuppiAlphaTemplate *guppi_alpha_template_text_general (GnomeFont * font,
						       double scale,
						       const gchar * text,
						       double rot_angle,
						       gboolean filled,
						       double outline_width);
#define guppi_alpha_template_text(font,text) \
  guppi_alpha_template_text_general((font),1.0,(text),0,TRUE,0)

#define guppi_alpha_template_outline_text(font,text,width) \
  guppi_alpha_template_text_general((font),1.0,(text),0,FALSE,(width))




END_GUPPI_DECLS

#endif /* _INC_GUPPI_ALPHA_TEMPLATE_H */

/* $Id$ */
