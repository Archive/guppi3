/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-root-group-view.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_ROOT_GROUP_VIEW_H
#define _INC_GUPPI_ROOT_GROUP_VIEW_H

/* #include <gnome.h> */
#include  "guppi-group-view.h"
#include  "guppi-canvas-item.h"
#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS 

typedef struct _GuppiRootGroupView GuppiRootGroupView;
typedef struct _GuppiRootGroupViewClass GuppiRootGroupViewClass;

struct _GuppiRootGroupView {
  GuppiGroupView parent;
  double width, height, scale;
};

struct _GuppiRootGroupViewClass {
  GuppiGroupViewClass parent_class;
};

#define GUPPI_TYPE_ROOT_GROUP_VIEW (guppi_root_group_view_get_type())
#define GUPPI_ROOT_GROUP_VIEW(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_ROOT_GROUP_VIEW,GuppiRootGroupView))
#define GUPPI_ROOT_GROUP_VIEW0(obj) ((obj) ? (GUPPI_ROOT_GROUP_VIEW(obj)) : NULL)
#define GUPPI_ROOT_GROUP_VIEW_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_ROOT_GROUP_VIEW,GuppiRootGroupViewClass))
#define GUPPI_IS_ROOT_GROUP_VIEW(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_ROOT_GROUP_VIEW))
#define GUPPI_IS_ROOT_GROUP_VIEW0(obj) (((obj) == NULL) || (GUPPI_IS_ROOT_GROUP_VIEW(obj)))
#define GUPPI_IS_ROOT_GROUP_VIEW_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_ROOT_GROUP_VIEW))

GtkType guppi_root_group_view_get_type (void);

void guppi_root_group_view_set_size (GuppiRootGroupView *, double w, double h);
double guppi_root_group_view_width (GuppiRootGroupView *);
double guppi_root_group_view_height (GuppiRootGroupView *);

GuppiRootGroupView *guppi_root_group_view_new (void);

/* Convenience functions */
GuppiCanvasItem    *guppi_root_group_view_make_canvas_item (GuppiRootGroupView *, GnomeCanvas *);
GnomeCanvas        *guppi_root_group_view_make_canvas (GuppiRootGroupView *, GuppiCanvasItem **);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_ROOT_GROUP_VIEW_H */

/* $Id$ */
