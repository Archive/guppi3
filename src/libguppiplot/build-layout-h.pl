#!/usr/bin/perl -w

print <<EOF;
/* This code is automatically generated.  Edit at your own risk! */

#ifndef __GUPPI_GROUP_VIEW_LAYOUT_H__
#define __GUPPI_GROUP_VIEW_LAYOUT_H__

#include "guppi-group-view.h"
#include "guppi-defs.h"

BEGIN_GUPPI_DECLS

EOF

while (defined (my $line = <>)) {

    chomp $line;
    $line =~ s/\#.*//g;
    next unless $line =~ /^([a-zA-Z0-9_]+):\s*(.*)/;

    my $name = $1;
    my @rest = map { $_ =~ s/geom/view/; $_ } split (/\s*,\s*/, $2);

    print "void guppi_group_view_layout_$name (GuppiGroupView *group, ";

    my $first = 1;
    foreach my $var (@rest) {

	print ", " unless $first;
	$first = 0;

	if ($var =~ /view/) {
	    print "GuppiElementView *$var";
	} else {
	    print "double $var";
	}
    }
    print ");\n";
}

print <<EOF;
END_GUPPI_DECLS

#endif /* __GUPPI_GROUP_VIEW_LAYOUT_H__ */

/* This code is automatically generated.  Edit at your own risk! */
EOF
