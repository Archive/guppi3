/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-layout-constraint.h
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __GUPPI_LAYOUT_CONSTRAINT_H__
#define __GUPPI_LAYOUT_CONSTRAINT_H__

#include <glib.h>
#include  "guppi-xml.h"
#include "guppi-geometry.h"

typedef enum {
  GLC_LEFT,
  GLC_RIGHT,
  GLC_TOP,
  GLC_BOTTOM,
  GLC_WIDTH,
  GLC_HEIGHT,
  GLC_HORIZONTAL_CENTER,
  GLC_VERTICAL_CENTER,
  GLC_REGION_LEFT,
  GLC_REGION_RIGHT,
  GLC_REGION_TOP,
  GLC_REGION_BOTTOM,
  GLC_REGION_WIDTH,
  GLC_REGION_HEIGHT,
  GLC_REGION_HORIZONTAL_CENTER,
  GLC_REGION_VERTICAL_CENTER,
  GLC_FIXED,
  GLC_LAST
} GuppiLayoutConstraintTermType;

struct _GuppiLayoutConstraint;
typedef struct _GuppiLayoutConstraint GuppiLayoutConstraint;

typedef void (*GuppiLayoutConstraintTermFn) (GuppiLayoutConstraintTermType term_type,
					     double factor, GuppiGeometry *geom,
					     gpointer closure);

GuppiLayoutConstraint *guppi_layout_constraint_new   (void);
void                   guppi_layout_constraint_ref   (GuppiLayoutConstraint *glc);
void                   guppi_layout_constraint_unref (GuppiLayoutConstraint *glc);
void                   guppi_layout_constraint_lock  (GuppiLayoutConstraint *glc);

void                   guppi_layout_constraint_add_term (GuppiLayoutConstraint *glc,
							 GuppiLayoutConstraintTermType term_type,
							 double factor,
							 GuppiGeometry *geom);
void                   guppi_layout_constraint_add_terms (GuppiLayoutConstraint *glc, ...);
void                   guppi_layout_constraint_foreach (GuppiLayoutConstraint *glc,
							GuppiLayoutConstraintTermFn fn,
							gpointer closure);
void                   guppi_layout_constraint_foreach_with_region (GuppiLayoutConstraint *glc,
								    double x0, double y0, double x1, double y1,
								    GuppiLayoutConstraintTermFn fn,
								    gpointer closure);

gboolean               guppi_layout_constraint_contains (GuppiLayoutConstraint *glc, GuppiGeometry *geom);
gboolean               guppi_layout_constraint_replace  (GuppiLayoutConstraint *glc, GuppiGeometry *old, GuppiGeometry *nuevo);

xmlNodePtr             guppi_layout_constraint_export_xml (GuppiLayoutConstraint *glc, GuppiXMLDocument *doc);
GuppiLayoutConstraint *guppi_layout_constraint_import_xml (GuppiXMLDocument *doc, xmlNodePtr node);

							 

#endif /* __GUPPI_LAYOUT_CONSTRAINT_H__ */

