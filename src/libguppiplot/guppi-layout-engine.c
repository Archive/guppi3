/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-layout-engine.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include "guppi-layout-engine.h"

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtkmain.h>
#include <gtk/gtksignal.h>
#include <gnome-xml/xmlmemory.h>
#include <guppi-memory.h>
#include <guppi-convenient.h>
#include <guppi-debug.h>
#include <guppi-matrix.h>

static GtkObjectClass *parent_class = NULL;

enum {
  CHANGED,
  PRE_LAYOUT,
  POST_LAYOUT,
  LAST_SIGNAL
};

guint guppi_layout_engine_signals[LAST_SIGNAL] = { 0 };

typedef struct _GeometryInfo GeometryInfo;
struct _GeometryInfo {
  GuppiGeometry *geom;
  guint changed_size_handler;
  gint id;
  gint refs; /* number of times this geometry appears in all rules */
};

typedef struct _GuppiLayoutEnginePrivate GuppiLayoutEnginePrivate;
struct _GuppiLayoutEnginePrivate {
  GList *geometries;
  GeometryInfo *last_geom_info;

  GList *rules;

  gboolean have_bounds;
  double x0, y0, x1, y1;

  gint freeze_count;
  gboolean dirty, resolved;
  guint pending_layout;

  gint reentry_count;
};

static GeometryInfo *geometry_info_new  (GuppiLayoutEngine *engine, GuppiGeometry *geom);
static void          geometry_info_free (GeometryInfo *info);
static gint          get_geometry_id    (GuppiLayoutEngine *, GuppiGeometry *);


static void
guppi_layout_engine_finalize (GtkObject *obj)
{
  GuppiLayoutEngine *engine = GUPPI_LAYOUT_ENGINE (obj);
  GuppiLayoutEnginePrivate *p = engine->priv;

  guppi_finalized (obj);

  g_list_foreach (p->geometries, (GFunc) geometry_info_free, NULL);
  g_list_free (p->geometries);

  g_list_foreach (p->rules, (GFunc) guppi_layout_rule_unref, NULL);
  g_list_free (p->rules);

  if (p->pending_layout) {
    gtk_idle_remove (p->pending_layout);
    p->pending_layout = 0;
  }

  guppi_free0 (engine->priv);
}

static void
guppi_layout_engine_class_init (GuppiLayoutEngineClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;

  parent_class = gtk_type_class (GTK_TYPE_OBJECT);

  object_class->finalize = guppi_layout_engine_finalize;

  guppi_layout_engine_signals[CHANGED] = 
    gtk_signal_new ("changed",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GuppiLayoutEngineClass, changed),
		    gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);

  guppi_layout_engine_signals[PRE_LAYOUT] = 
    gtk_signal_new ("pre_layout",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GuppiLayoutEngineClass, pre_layout),
		    gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);

  guppi_layout_engine_signals[POST_LAYOUT] =
    gtk_signal_new ("post_layout",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GuppiLayoutEngineClass, post_layout),
		    gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);

  gtk_object_class_add_signals (object_class, guppi_layout_engine_signals, LAST_SIGNAL);

}

static void
guppi_layout_engine_init (GuppiLayoutEngine *engine)
{
  engine->priv = guppi_new0 (GuppiLayoutEnginePrivate, 1);
}

GtkType
guppi_layout_engine_get_type (void)
{
  static GtkType guppi_layout_engine_type = 0;
  if (!guppi_layout_engine_type) {
    static const GtkTypeInfo guppi_layout_engine_info = {
      "GuppiLayoutEngine",
      sizeof (GuppiLayoutEngine),
      sizeof (GuppiLayoutEngineClass),
      (GtkClassInitFunc) guppi_layout_engine_class_init,
      (GtkObjectInitFunc) guppi_layout_engine_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_layout_engine_type = gtk_type_unique (GTK_TYPE_OBJECT, &guppi_layout_engine_info);
  }
  return guppi_layout_engine_type;
}

GuppiLayoutEngine *
guppi_layout_engine_new (void)
{
  return GUPPI_LAYOUT_ENGINE (guppi_type_new (guppi_layout_engine_get_type ()));
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static gint
constraint_count (GuppiLayoutEngine *engine)
{
  GList *iter;
  gint count = 0;
  for (iter = engine->priv->rules; iter != NULL; iter = g_list_next (iter)) {
    count += guppi_layout_rule_constraint_count ((GuppiLayoutRule *) iter->data);
  }

  return count;
}

typedef struct _BuildMatrixInfo BuildMatrixInfo;
struct _BuildMatrixInfo {
  GuppiLayoutEngine *engine;
  GuppiMatrix *m;
  gint r;
};

static void
build_matrix_term_fn (GuppiLayoutConstraintTermType tt, double factor, GuppiGeometry *geom, gpointer closure)
{
  BuildMatrixInfo *info = closure;
  gint c = 0;

  if (geom) {
    c = 4 * get_geometry_id (info->engine, geom);
    switch (tt) {
    case GLC_LEFT:
      /* c += 0; */
      break;
    case GLC_RIGHT:
      c += 1;
      break;
    case GLC_TOP:
      c += 2;
      break;
    case GLC_BOTTOM:
      c += 3;
      break;
    default:
      g_message ("Uh oh %d", (gint)tt);
      g_assert_not_reached ();
    }
    ++c; /* constaints are in column zero */
  }

  guppi_matrix_entry (info->m, info->r, c) += factor;
}

static void
build_matrix_constraint_fn (GuppiLayoutConstraint *ctn, gpointer closure)
{
  BuildMatrixInfo *info = closure;
  GuppiLayoutEnginePrivate *p = info->engine->priv;

  guppi_layout_constraint_foreach_with_region (ctn,
					       p->x0, p->y0, p->x1, p->y1,
					       build_matrix_term_fn, info);

  ++info->r;
}

static void
natural_size_contraint_fn (GuppiLayoutEngine *engine, GuppiGeometry *geom, gpointer closure)
{
  BuildMatrixInfo *info = closure;

  if (guppi_geometry_has_natural_width (geom)) {
    GuppiLayoutConstraint *ctn = guppi_layout_constraint_new ();

#if 0
    g_message ("using natural width! %s", guppi_geometry_get_debug_label (geom));
#endif

    guppi_layout_constraint_add_terms (ctn,
				       GLC_RIGHT, 1.0, geom,
				       GLC_LEFT, -1.0, geom,
				       GLC_WIDTH, -1.0, geom,
				       GLC_LAST);

    build_matrix_constraint_fn (ctn, info);

    guppi_layout_constraint_unref (ctn);
  }

  if (guppi_geometry_has_natural_height (geom)) {
    GuppiLayoutConstraint *ctn = guppi_layout_constraint_new ();

#if 0
    g_message ("using natural height! %s", guppi_geometry_get_debug_label (geom));
#endif

    guppi_layout_constraint_add_terms (ctn, 
				       GLC_TOP, 1.0, geom,
				       GLC_BOTTOM, -1.0, geom,
				       GLC_HEIGHT, -1.0, geom,
				       GLC_LAST);

    build_matrix_constraint_fn (ctn, info);

    guppi_layout_constraint_unref (ctn);
  }
}

static void
build_simplified_rule_system (GuppiLayoutEngine *engine, GuppiMatrix **matrix, GuppiVector **vector)
{
  gint rows = constraint_count (engine) + 2 * g_list_length (engine->priv->geometries);
  gint cols = 4 * g_list_length (engine->priv->geometries) + 1;
  GuppiMatrix *m = guppi_matrix_new (rows, cols);
  BuildMatrixInfo info;
  GList *iter;
  gint r, i, j, nonzero_rows;

  if (rows <= 0 || cols <= 0) {
    g_message ("layout %p is empty", engine);
    return;
  }

  info.engine = engine;
  info.m = m;
  info.r = 0;

  for (iter = engine->priv->rules; iter != NULL; iter = g_list_next (iter)) {
    GuppiLayoutRule *rule = iter->data;
    guppi_layout_rule_foreach (rule, build_matrix_constraint_fn, &info);
  }

  /* Handle elements w/ natural width/height */
  guppi_layout_engine_foreach_geometry (engine, natural_size_contraint_fn, &info);

  /* Now we use a Grahm-Schmidt style process to simplify the matrix. */
  nonzero_rows = 0;
  for (r = 0; r < info.r; ++r) {
    for (j=0; j < r; ++j) {
      double sc = guppi_matrix_row_dot (m, r, j);
      guppi_matrix_subtract_scaled_row_from_row (m, sc, j, r);
    }
    if (guppi_matrix_row_is_nonzero (m, r)) {
      guppi_matrix_normalize_row (m, r);
      ++nonzero_rows;
    }
  }

  *matrix = guppi_matrix_new (nonzero_rows, cols-1);
  *vector = guppi_vector_new (nonzero_rows);
  
  j = 0;
  for (r = 0; r < info.r; ++r) {
    if (guppi_matrix_row_is_nonzero (m, r)) {
      double *m_p;
      double *matrix_p;

      guppi_vector_entry (*vector, j) = - guppi_matrix_entry (m, r, 0);

      m_p = guppi_matrix_ptr (m, r, 1);
      matrix_p = guppi_matrix_ptr (*matrix, j, 0);
      for (i = 0; i < cols-1; ++i) {
	*matrix_p = *m_p;
	m_p = guppi_matrix_ptr_col_incr (m, m_p);
	matrix_p = guppi_matrix_ptr_col_incr (*matrix, matrix_p);
      }
      ++j;
    }
  }
  g_assert (nonzero_rows == j);

  guppi_matrix_free (m);
}

struct CustomLUInfo {
  double x0, y0, x1, y1;
};

static gboolean
custom_solve_fallback (GuppiMatrix *m, GuppiVector *vec, gint i, gpointer user_data)
{
  struct CustomLUInfo *info = user_data;
  double q = 0;

  switch (i % 4) {
  case 0:
    q = info->x0;
    break;
  case 1:
    q = info->x1;
    break;
  case 2:
    q = info->y1;
    break;
  case 3:
    q = info->y0;
    break;
  }

  guppi_vector_entry (vec, i) = q;
  
  return TRUE;
}

static double
evil_clean (double x)
{
  const double evil_clean_constant = 1e-10;
  double x0, x1;

  x0 = floor (x);
  x1 = ceil (x);

  if (fabs (x - x0) < evil_clean_constant)
    return x0;
  if (fabs (x - x1) < evil_clean_constant)
    return x1;
  return x;
}

static gboolean
do_layout (gpointer closure)
{
  GuppiLayoutEngine *engine = closure;

  GuppiMatrix *layout_matrix;
  GuppiVector *layout_vector;
  GuppiVector *soln_vector;
  struct CustomLUInfo custom_info;

#if 0
  g_message ("doing layout %p", engine);
#endif

  gtk_signal_emit (GTK_OBJECT (engine), guppi_layout_engine_signals[PRE_LAYOUT]);

  build_simplified_rule_system (engine, &layout_matrix, &layout_vector);

  custom_info.x0 = engine->priv->x0;
  custom_info.y0 = engine->priv->y0;
  custom_info.x1 = engine->priv->x1;
  custom_info.y1 = engine->priv->y1;

  guppi_debug_v ("bounds: %g:%g %g:%g", engine->priv->x0, engine->priv->x1, engine->priv->y0, engine->priv->y1);
  guppi_debug_v ("collapsed system: rows=%d cols=%d", guppi_matrix_rows (layout_matrix), guppi_matrix_cols (layout_matrix));

#if 0
  {
    gint i, j;
    for (i = 0; i < guppi_vector_dim (layout_vector); ++i)
      g_message ("  %2d %g", i, guppi_vector_entry (layout_vector, i));

    for (i = 0; i < guppi_matrix_rows (layout_matrix); ++i) {
      for (j = 0; j < guppi_matrix_cols (layout_matrix); ++j) {
	g_print ("%9.5f ", guppi_matrix_entry (layout_matrix, i, j));
      }
      g_print ("\n");
    }
  }
#endif

  soln_vector = guppi_matrix_solve_with_fallback (layout_matrix,
						  layout_vector,
						  custom_solve_fallback,
						  &custom_info);
#if 0
  if (soln_vector) {
    gint i;
    for (i = 0; i < guppi_vector_dim (soln_vector); ++i)
      g_message ("soln  %2d %g", i, guppi_vector_entry (soln_vector, i));
  }
#endif
  
  if (soln_vector) {
    GList *iter;
    gint i = 0;

    for (iter = engine->priv->geometries; iter != NULL; iter = g_list_next (iter)) {
      GeometryInfo *info = iter->data;
      double x0 = guppi_vector_entry (soln_vector, i);
      double x1 = guppi_vector_entry (soln_vector, i+1);
      double y0 = guppi_vector_entry (soln_vector, i+2);
      double y1 = guppi_vector_entry (soln_vector, i+3);

      x0 = evil_clean (x0);
      x1 = evil_clean (x1);
      y0 = evil_clean (y0);
      y1 = evil_clean (y1);

      guppi_geometry_set_position (info->geom,
				   MAX (x0, engine->priv->x0),
				   MIN (x1, engine->priv->x1),
				   MAX (y0, engine->priv->y0),
				   MIN (y1, engine->priv->y1));

      guppi_debug_v ("Setting to %g:%g %g:%g", x0, x1, y0, y1);
      
      i += 4;
    }

    engine->priv->resolved = TRUE;

  } else {

    g_message ("layout failed");
    engine->priv->resolved = FALSE;

  }

  guppi_matrix_free (layout_matrix);
  guppi_vector_free (layout_vector);
  guppi_vector_free (soln_vector);

  engine->priv->dirty = FALSE;
  engine->priv->pending_layout = 0;

  gtk_signal_emit (GTK_OBJECT (engine), guppi_layout_engine_signals[POST_LAYOUT]);

  return FALSE;
}

static void
schedule_layout (GuppiLayoutEngine *engine)
{
  g_return_if_fail (engine && GUPPI_IS_LAYOUT_ENGINE (engine));

  if (! engine->priv->dirty)
    return;
  
  if (! engine->priv->have_bounds) 
    return;

  if (engine->priv->freeze_count > 0)
    return;

  if (engine->priv->pending_layout != 0)
    return;

  guppi_debug_v ("layout scheduled");
  engine->priv->pending_layout = gtk_idle_add_priority (G_PRIORITY_HIGH_IDLE, do_layout, engine);
}

static void
cancel_layout (GuppiLayoutEngine *engine)
{
  if (engine->priv->pending_layout) {
    gtk_idle_remove (engine->priv->pending_layout);
    engine->priv->pending_layout = 0;
  }
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
changed_size_cb (GuppiGeometry *geom, gpointer closure)
{
  GuppiLayoutEngine *engine = GUPPI_LAYOUT_ENGINE (closure);
  engine->priv->dirty = TRUE;
  schedule_layout (engine);
}

static GeometryInfo *
geometry_info_new (GuppiLayoutEngine *engine, GuppiGeometry *geom)
{
  GeometryInfo *info;

  info = guppi_new0 (GeometryInfo, 1);
  info->geom = geom;
  guppi_ref (info->geom);
  info->refs = 1;

  info->changed_size_handler = gtk_signal_connect (GTK_OBJECT (geom),
						   "changed_size",
						   GTK_SIGNAL_FUNC (changed_size_cb),
						   GTK_OBJECT (engine));
  return info;
}

static void
geometry_info_free (GeometryInfo *info)
{
  if (info == NULL)
    return;

  gtk_signal_disconnect (GTK_OBJECT (info->geom), info->changed_size_handler);
  guppi_unref (info->geom);
  guppi_free (info);
}

static GeometryInfo *
find_geometry_info (GuppiLayoutEngine *engine, GuppiGeometry *geom)
{
  GList *iter;

  if (engine->priv->last_geom_info && engine->priv->last_geom_info->geom == geom)
    return engine->priv->last_geom_info;

  /* Sucky linear search. */
  for (iter = engine->priv->geometries; iter != NULL; iter = g_list_next (iter)) {
    if ( ((GeometryInfo *) iter->data)->geom == geom ) {
      engine->priv->last_geom_info = iter->data;
      return iter->data;
    }
  }
  return NULL;
}

static void
assign_ids (GuppiLayoutEngine *engine)
{
  GList *iter;
  gint id = 0;

  for (iter = engine->priv->geometries; iter != NULL; iter = g_list_next (iter)) {
    ((GeometryInfo *) iter->data)->id = id;
    ++id;
  }
}

static gint
get_geometry_id (GuppiLayoutEngine *engine, GuppiGeometry *geom)
{
  GeometryInfo *info = find_geometry_info (engine, geom);
  return info ? info->id : -1;
}

static void
add_geometry (GuppiLayoutEngine *engine, GuppiGeometry *geom)
{
  GeometryInfo *info;
  
  info = find_geometry_info (engine, geom);
  if (info != NULL) {
    ++info->refs;
    return;
  }

  info = geometry_info_new (engine, geom);

  engine->priv->geometries = g_list_append (engine->priv->geometries, info);
  engine->priv->last_geom_info = info;

  assign_ids (engine);
}

static void
remove_geometry (GuppiLayoutEngine *engine, GuppiGeometry *geom)
{
  GeometryInfo *info;
  GList *node;

  info = find_geometry_info (engine, geom);
  g_return_if_fail (info != NULL);

  g_assert (info->refs > 0);
  --info->refs;
  if (info->refs > 0)
    return;

  if (engine->priv->last_geom_info == info)
    engine->priv->last_geom_info = NULL;

  node = g_list_find (engine->priv->geometries, info);
  engine->priv->geometries = g_list_remove_link (engine->priv->geometries, node);
  g_list_free_1 (node);

  geometry_info_free (info);
  
  assign_ids (engine);
}

static void
add_rule_foreach_term_fn (GuppiLayoutConstraintTermType term_type,
			  double factor, GuppiGeometry *geom,
			  gpointer closure)
{
  if (geom != NULL)
    add_geometry ((GuppiLayoutEngine *) closure, geom);
}

static void
add_rule_foreach_constraint_fn (GuppiLayoutConstraint *cnt, gpointer closure)
{
  guppi_layout_constraint_foreach (cnt, add_rule_foreach_term_fn, closure);
}

void
guppi_layout_engine_add_rule (GuppiLayoutEngine *engine, GuppiLayoutRule *rule)
{
  g_return_if_fail (engine && GUPPI_IS_LAYOUT_ENGINE (engine));
  g_return_if_fail (rule != NULL);

  g_assert (engine->priv->reentry_count == 0);

  guppi_layout_rule_ref (rule);
  engine->priv->rules = g_list_append (engine->priv->rules, rule);
  guppi_layout_rule_foreach (rule, add_rule_foreach_constraint_fn, engine);

  engine->priv->dirty = TRUE;
  schedule_layout (engine);
  gtk_signal_emit (GTK_OBJECT (engine), guppi_layout_engine_signals[CHANGED]);
}

static void
remove_rule_foreach_term_fn (GuppiLayoutConstraintTermType term_type,
			     double factor, GuppiGeometry *geom,
			     gpointer closure)
{
  if (geom != NULL)
    remove_geometry ((GuppiLayoutEngine *) closure, geom);
}

static void
remove_rule_foreach_constraint_fn (GuppiLayoutConstraint *cnt, gpointer closure)
{
  guppi_layout_constraint_foreach (cnt, remove_rule_foreach_term_fn, closure);
}

void
guppi_layout_engine_remove_rule (GuppiLayoutEngine *engine, GuppiLayoutRule *rule)
{
  GList *node;

  g_return_if_fail (engine && GUPPI_IS_LAYOUT_ENGINE (engine));
  g_return_if_fail (rule != NULL);

  g_assert (engine->priv->reentry_count == 0);

  node = g_list_find (engine->priv->rules, rule);
  if (node == NULL)
    return;

  engine->priv->rules = g_list_remove_link (engine->priv->rules, node);
  g_list_free_1 (node);

  guppi_layout_rule_foreach (rule, remove_rule_foreach_constraint_fn, engine);
  guppi_layout_rule_unref (rule);

  schedule_layout (engine);
  gtk_signal_emit (GTK_OBJECT (engine), guppi_layout_engine_signals[CHANGED]);
}

void
guppi_layout_engine_remove_geometry_rules (GuppiLayoutEngine *engine, GuppiGeometry *geom)
{
  GList *rules_copy, *iter;

  g_return_if_fail (engine && GUPPI_IS_LAYOUT_ENGINE (engine));
  g_return_if_fail (geom && GUPPI_IS_GEOMETRY (geom));

  g_assert (engine->priv->reentry_count == 0);

  /* Find and remove any rules that mention geom */

  rules_copy = g_list_copy (engine->priv->rules);

  for (iter = rules_copy; iter != NULL; iter = g_list_next (iter)) {
    GuppiLayoutRule *rule = iter->data;
    if (guppi_layout_rule_contains (rule, geom))
      guppi_layout_engine_remove_rule (engine, rule);
  }

  g_list_free (rules_copy);

  schedule_layout (engine);
  gtk_signal_emit (GTK_OBJECT (engine), guppi_layout_engine_signals[CHANGED]);
}

void
guppi_layout_engine_remove_geometry (GuppiLayoutEngine *engine, GuppiGeometry *geom)
{
  GeometryInfo *info;

  g_return_if_fail (engine && GUPPI_IS_LAYOUT_ENGINE (engine));
  g_return_if_fail (geom && GUPPI_IS_GEOMETRY (geom));

  g_assert (engine->priv->reentry_count == 0);

  guppi_layout_engine_remove_geometry_rules (engine, geom);

  /* Remove our main reference */
  remove_geometry (engine,geom);

  info = find_geometry_info (engine, geom);
  g_assert (info == NULL);

  schedule_layout (engine);
  gtk_signal_emit (GTK_OBJECT (engine), guppi_layout_engine_signals[CHANGED]);
}

void
guppi_layout_engine_replace_geometry (GuppiLayoutEngine *engine, GuppiGeometry *old, GuppiGeometry *nuevo)
{
  GList *iter;
  GeometryInfo *info;

  g_return_if_fail (engine && GUPPI_IS_LAYOUT_ENGINE (engine));
  g_return_if_fail (old && GUPPI_IS_GEOMETRY (old));
  g_return_if_fail (nuevo && GUPPI_IS_GEOMETRY (nuevo));

  g_assert (engine->priv->reentry_count == 0);

  if (old == nuevo)
    return;

  for (iter = engine->priv->rules; iter != NULL; iter = g_list_next (iter)) {
    GuppiLayoutRule *rule = iter->data;
    
    guppi_layout_rule_replace (rule, old, nuevo);
  }

  info = find_geometry_info (engine, old);
  if (info) {
    gtk_signal_disconnect (GTK_OBJECT (info->geom), info->changed_size_handler);
    guppi_refcounting_assign (info->geom, nuevo);
    info->changed_size_handler = gtk_signal_connect_object (GTK_OBJECT (nuevo),
							    "changed_size",
							    GTK_SIGNAL_FUNC (schedule_layout),
							    GTK_OBJECT (engine));
  }

  schedule_layout (engine);
  gtk_signal_emit (GTK_OBJECT (engine), guppi_layout_engine_signals[CHANGED]);
}

void
guppi_layout_engine_foreach_rule (GuppiLayoutEngine *engine, GuppiLayoutEngineRuleFn fn, gpointer closure)
{
  GList *iter;

  g_return_if_fail (engine && GUPPI_IS_LAYOUT_ENGINE (engine));
  g_return_if_fail (fn != NULL);

  ++engine->priv->reentry_count;

  for (iter = engine->priv->rules; iter != NULL; iter = g_list_next (iter)) {
    fn (engine, (GuppiLayoutRule *) iter->data, closure);
  }

  --engine->priv->reentry_count;
}

void
guppi_layout_engine_foreach_geometry (GuppiLayoutEngine *engine, GuppiLayoutEngineGeometryFn fn, gpointer closure)
{
  GList *iter;

  g_return_if_fail (engine && GUPPI_IS_LAYOUT_ENGINE (engine));
  g_return_if_fail (fn != NULL);

  ++engine->priv->reentry_count;

  for (iter = engine->priv->geometries; iter != NULL; iter = g_list_next (iter)) {
    fn (engine, ((GeometryInfo *) iter->data)->geom, closure);
  }

  --engine->priv->reentry_count;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

void
guppi_layout_engine_set_bounds (GuppiLayoutEngine *engine,
				double x0, double y0, double x1, double y1)
{
  GuppiLayoutEnginePrivate *p;

  g_return_if_fail (engine && GUPPI_IS_LAYOUT_ENGINE (engine));
  p = engine->priv;

  guppi_2sort (&x0, &x1);
  guppi_2sort (&y0, &y1);

  if (!p->have_bounds
      || p->x0 != x0
      || p->y0 != y0
      || p->x1 != x1
      || p->y1 != y1) {

    p->have_bounds = TRUE;
    p->x0 = x0;
    p->x1 = x1;
    p->y0 = y0;
    p->y1 = y1;

    engine->priv->dirty = TRUE;

    schedule_layout (engine);
    gtk_signal_emit (GTK_OBJECT (engine), guppi_layout_engine_signals[CHANGED]);

  }
}

void
guppi_layout_engine_set_bounds_from_geometry (GuppiLayoutEngine *engine, GuppiGeometry *geom)
{
  g_return_if_fail (engine && GUPPI_IS_LAYOUT_ENGINE (engine));
  g_return_if_fail (geom && GUPPI_IS_GEOMETRY (geom));

  guppi_layout_engine_set_bounds (engine,
				  guppi_geometry_left (geom),
				  guppi_geometry_bottom (geom),
				  guppi_geometry_right (geom),
				  guppi_geometry_top (geom));
}

void
guppi_layout_engine_unset_bounds (GuppiLayoutEngine *engine)
{
  g_return_if_fail (engine && GUPPI_IS_LAYOUT_ENGINE (engine));

  engine->priv->have_bounds = FALSE;
  cancel_layout (engine);
  gtk_signal_emit (GTK_OBJECT (engine), guppi_layout_engine_signals[CHANGED]);
}

void
guppi_layout_engine_freeze (GuppiLayoutEngine *engine)
{
  g_return_if_fail (engine && GUPPI_IS_LAYOUT_ENGINE (engine));
  g_return_if_fail (engine->priv->freeze_count >= 0);
  ++engine->priv->freeze_count;
}

void
guppi_layout_engine_thaw (GuppiLayoutEngine *engine)
{
  g_return_if_fail (engine && GUPPI_IS_LAYOUT_ENGINE (engine));
  g_return_if_fail (engine->priv->freeze_count > 0);

  --engine->priv->freeze_count;
  if (engine->priv->freeze_count == 0 && engine->priv->dirty)
    schedule_layout (engine);
}

void
guppi_layout_engine_flush (GuppiLayoutEngine *engine)
{
  g_return_if_fail (engine && GUPPI_IS_LAYOUT_ENGINE (engine));

  if (engine->priv->pending_layout != 0) {
    guppi_debug_v ("layout flushed");
    gtk_idle_remove (engine->priv->pending_layout);
    engine->priv->pending_layout = 0;
    do_layout (engine);
  }
}

xmlNodePtr
guppi_layout_engine_export_xml (GuppiLayoutEngine *engine, GuppiXMLDocument *doc)
{
  xmlNodePtr root_node, bounds_node, rule_node, geom_node;
  GList *iter;

  g_return_val_if_fail (GUPPI_IS_LAYOUT_ENGINE (engine),NULL);
  g_return_val_if_fail (doc != NULL, NULL);

  /* Compute any pending layout changes before we export. */
  guppi_layout_engine_flush (engine);

  root_node = xmlNewNode (doc->ns, "Layout");
  xmlNewProp (root_node, "status", engine->priv->dirty ? "dirty" : "clean");
  xmlNewProp (root_node, "resolved", engine->priv->resolved ? "yes" : "no");

  if (engine->priv->have_bounds) {
    gchar buf[64];
    bounds_node = xmlNewNode (doc->ns, "Bounds");
    
    g_snprintf (buf, 64, "%g", engine->priv->x0);
    xmlNewProp (bounds_node, "x0", buf);

    g_snprintf (buf, 64, "%g", engine->priv->y0);
    xmlNewProp (bounds_node, "y0", buf);

    g_snprintf (buf, 64, "%g", engine->priv->x1);
    xmlNewProp (bounds_node, "x1", buf);

    g_snprintf (buf, 64, "%g", engine->priv->y1);
    xmlNewProp (bounds_node, "y1", buf);

    xmlAddChild (root_node, bounds_node);
  }

  geom_node = xmlNewNode (doc->ns, "Geometries");
  rule_node = xmlNewNode (doc->ns, "Rules");

  xmlAddChild (root_node, geom_node);
  xmlAddChild (root_node, rule_node);

  for (iter = engine->priv->geometries; iter != NULL; iter = g_list_next (iter)) {
    xmlAddChild (geom_node, guppi_geometry_export_xml (((GeometryInfo *) iter->data)->geom, doc));
  }

  for (iter = engine->priv->rules; iter != NULL; iter = g_list_next (iter)) {
    GuppiLayoutRule *rule = iter->data;
    if (rule)
      xmlAddChild (rule_node, guppi_layout_rule_export_xml (rule, doc));
  }

  return root_node;
}

GuppiLayoutEngine *
guppi_layout_engine_import_xml (GuppiXMLDocument *doc, xmlNodePtr node)
{
  GuppiLayoutEngine *engine;
  gchar *s;
  gboolean have_bounds=FALSE, have_geoms=FALSE, have_rules=FALSE;

  g_return_val_if_fail (doc != NULL, NULL);
  g_return_val_if_fail (node != NULL, NULL);

  if (strcmp (node->name, "Layout"))
    return NULL;

  engine = guppi_layout_engine_new ();

  s = xmlGetProp (node, "status");
  engine->priv->dirty = s && !strcmp (s, "dirty");
  xmlFree (s);

  s = xmlGetProp (node, "resolved");
  engine->priv->resolved = s && !strcmp (s, "yes");
  xmlFree (s);

  node = node->xmlChildrenNode;

  while (node != NULL) {

    if (!have_bounds && !strcmp (node->name, "Bounds")) {

      s = xmlGetProp (node, "x0");
      engine->priv->x0 = s ? atof (s) : 0;
      xmlFree (s);

      s = xmlGetProp (node, "x1");
      engine->priv->x1 = s ? atof (s) : 1;
      xmlFree (s);

      s = xmlGetProp (node, "y0");
      engine->priv->y0 = s ? atof (s) : 0;
      xmlFree (s);

      s = xmlGetProp (node, "y1");
      engine->priv->y1 = s ? atof (s) : 1;
      xmlFree (s);

      guppi_2sort (&engine->priv->x0, &engine->priv->x1);
      guppi_2sort (&engine->priv->y0, &engine->priv->y1);

      engine->priv->have_bounds = TRUE;
      have_bounds = TRUE;

    } else if (!have_geoms && !strcmp (node->name, "Geometries")) {
      GuppiGeometry *gg;
      xmlNodePtr subnode = node->xmlChildrenNode;

      while (subnode) {
	gg = guppi_geometry_import_xml (doc, subnode);
	if (gg)
	  add_geometry (engine, gg);
	subnode = subnode->next;
      }

      have_geoms = TRUE;

    } else if (!have_rules && !strcmp (node->name, "Rules")) {
      GuppiLayoutRule *rule;
      xmlNodePtr subnode = node->xmlChildrenNode;

      while (subnode) {
	rule = guppi_layout_rule_import_xml (doc,subnode);
	if (rule)
	  engine->priv->rules = g_list_append (engine->priv->rules, rule);
	subnode = subnode->next;
      }

      have_rules = TRUE;

    }

    node = node->next;
  }

  return engine;
}

void
guppi_layout_engine_spew_xml (GuppiLayoutEngine *engine)
{
  GuppiXMLDocument *doc = guppi_xml_document_new ();
  guppi_xml_document_set_root (doc, guppi_layout_engine_export_xml (engine, doc));
  guppi_xml_document_spew (doc);
  guppi_xml_document_free (doc);
}
