/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-raster-text.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_RASTER_TEXT_H
#define _INC_GUPPI_RASTER_TEXT_H

/* #include <gnome.h> */

#include <libgnomeprint/gnome-font.h>
#include "guppi-text-block.h"
#include "guppi-alpha-template.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS 

typedef struct _GuppiRasterText GuppiRasterText;
typedef struct _GuppiRasterTextClass GuppiRasterTextClass;

struct _GuppiRasterText {
  GtkObject parent;

  gpointer opaque_internals;
};

struct _GuppiRasterTextClass {
  GtkObjectClass parent_class;
};

#define GUPPI_TYPE_RASTER_TEXT (guppi_raster_text_get_type())
#define GUPPI_RASTER_TEXT(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_RASTER_TEXT,GuppiRasterText))
#define GUPPI_RASTER_TEXT0(obj) ((obj) ? (GUPPI_RASTER_TEXT(obj)) : NULL)
#define GUPPI_RASTER_TEXT_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_RASTER_TEXT,GuppiRasterTextClass))
#define GUPPI_IS_RASTER_TEXT(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_RASTER_TEXT))
#define GUPPI_IS_RASTER_TEXT0(obj) (((obj) == NULL) || (GUPPI_IS_RASTER_TEXT(obj)))
#define GUPPI_IS_RASTER_TEXT_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_RASTER_TEXT))

GtkType guppi_raster_text_get_type (void);

GuppiRasterText *guppi_raster_text_new (GuppiTextBlock *);

double guppi_raster_text_scale (GuppiRasterText *text);
void guppi_raster_text_set_scale (GuppiRasterText *text, double scale);

GuppiAlphaTemplate *guppi_raster_text_template (GuppiRasterText *);
GuppiTextBlock *guppi_raster_text_block (GuppiRasterText *);

void guppi_raster_text_set_position (GuppiRasterText *, gint, gint);
void guppi_raster_text_position (GuppiRasterText *, gint *, gint *);

/* Convenience functions */
gchar *guppi_raster_text_text (GuppiRasterText *);
void guppi_raster_text_set_text (GuppiRasterText *, const gchar *);
void guppi_raster_text_set_font (GuppiRasterText *, GnomeFont *);
void guppi_raster_text_set_angle (GuppiRasterText *, double);


END_GUPPI_DECLS

#endif /* _INC_GUPPI_RASTER_TEXT_H */

/* $Id$ */
