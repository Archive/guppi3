/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-text-block.c
 *
 * Copyright (C) 2001 Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-text-block.h"

#include <math.h>
#include <guppi-memory.h>
#include <guppi-defaults.h>
#include "guppi-gsml.h"

static GtkObjectClass *parent_class = NULL;

enum {
  CHANGED,
  LAST_SIGNAL
};

static guint guppi_text_block_signals[LAST_SIGNAL] = { 0 };

enum {
  PENDING_NONE,
  PENDING_IMMEDIATE,
  PENDING_DELAYED
};

typedef struct _GuppiTextBlockPrivate GuppiTextBlockPrivate;
struct _GuppiTextBlockPrivate {

  GList *tokens;
  GList *broken;

  GnomeFont *font;

  double angle;

  double max_width;

  gboolean needs_soft_breaks;
  guint pending_changed_signal;

  gint freeze_count;
  gint freeze_type;
};

#define priv(x) ((GuppiTextBlockPrivate*)(GUPPI_TEXT_BLOCK((x))->opaque_internals))

static void
guppi_text_block_finalize (GtkObject *obj)
{
  GuppiTextBlock *x = GUPPI_TEXT_BLOCK(obj);
  GuppiTextBlockPrivate *p = priv (x);
  GList *i;

  /* Free the broken version of our token stream. */
  for (i = p->broken; i != NULL; i = g_list_next (i)) {
    GuppiTextToken *tt = GUPPI_TEXT_TOKEN (i->data);
    if (guppi_text_token_type (tt) == TEXT_TOKEN_SOFT_BREAK)
      guppi_text_token_free (tt);
  }
  g_list_free (p->broken);
  p->broken = NULL;

  for (i = p->tokens; i != NULL; i = g_list_next (i))
    guppi_text_token_free (GUPPI_TEXT_TOKEN (i->data));
  g_list_free (p->tokens);
  p->tokens = NULL;

  if (p->pending_changed_signal)
    gtk_idle_remove (p->pending_changed_signal);
  p->pending_changed_signal = 0;

  guppi_unref0 (p->font);

  guppi_free (x->opaque_internals);
  x->opaque_internals = NULL;

  guppi_finalized (obj);

  if (parent_class->finalize)
    parent_class->finalize (obj);
  }

static void
guppi_text_block_class_init (GuppiTextBlockClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;

  parent_class = gtk_type_class (GTK_TYPE_OBJECT);

  object_class->finalize = guppi_text_block_finalize;

  /* Signal definition template */
  guppi_text_block_signals[CHANGED] =
    gtk_signal_new ("changed",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GuppiTextBlockClass, changed),
                    gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);

  gtk_object_class_add_signals (object_class, guppi_text_block_signals,
                                LAST_SIGNAL);
}

static void
guppi_text_block_init (GuppiTextBlock *obj)
{
  GuppiTextBlockPrivate *p = guppi_new0 (GuppiTextBlockPrivate, 1);

  p->angle = 0;

  obj->opaque_internals = p;
}

GtkType
guppi_text_block_get_type (void)
{
  static GtkType guppi_text_block_type = 0;
  if (!guppi_text_block_type) {
    static const GtkTypeInfo guppi_text_block_info = {
      "GuppiTextBlock",
      sizeof (GuppiTextBlock),
      sizeof (GuppiTextBlockClass),
      (GtkClassInitFunc)guppi_text_block_class_init,
      (GtkObjectInitFunc)guppi_text_block_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    guppi_text_block_type = gtk_type_unique (GTK_TYPE_OBJECT,
					     &guppi_text_block_info);
  }
  return guppi_text_block_type;
}

GuppiTextBlock *
guppi_text_block_new (void)
{
  return GUPPI_TEXT_BLOCK (guppi_type_new (guppi_text_block_get_type ()));
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

/* Operations on tokens */

void
guppi_text_block_add (GuppiTextBlock *text, GuppiTextToken *tt)
{
  GuppiTextBlockPrivate *p;

  g_return_if_fail (GUPPI_IS_TEXT_BLOCK (text));
  p = priv (text);

  g_return_if_fail (tt);
  g_return_if_fail (guppi_text_token_type (tt) != TEXT_TOKEN_ERROR);

  p->tokens = g_list_append (p->tokens, tt);
  p->needs_soft_breaks = TRUE;

  guppi_text_block_changed_delayed (text);
}

void
guppi_text_block_clear (GuppiTextBlock *text)
{
  GuppiTextBlockPrivate *p;
  GList *iter;

  g_return_if_fail (GUPPI_IS_TEXT_BLOCK (text));
  p = priv (text);

  for (iter = p->broken; iter; iter = g_list_next (iter)) {
    GuppiTextToken *tt = GUPPI_TEXT_TOKEN (iter->data);
    if (guppi_text_token_type (tt) == TEXT_TOKEN_SOFT_BREAK)
      guppi_text_token_free (tt);
  }
  g_list_free (p->broken);
  p->broken = NULL;

  for (iter = p->tokens; iter; iter = g_list_next (iter))
    guppi_text_token_free ((GuppiTextToken *) iter->data);

  g_list_free (p->tokens);
  p->tokens = NULL;

  p->needs_soft_breaks = TRUE;

  guppi_text_block_changed_delayed (text);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
build_text_cb (const gchar *str, GnomeFont *font,
	       double off_x, double off_y,
	       gpointer user_data)
{
  gchar **build = (gchar **)user_data;
  gchar *tmp;

  if (*build == NULL) {
    *build = g_strdup (str);
    return;
  }

  tmp = *build;
  *build = g_strconcat (*build, " ", str, NULL);
  g_free (tmp);
}

gchar *
guppi_text_block_text (GuppiTextBlock *text)
{
  gchar *str = NULL;

  g_return_val_if_fail (GUPPI_IS_TEXT_BLOCK (text), NULL);

  guppi_text_block_foreach_word (text, build_text_cb, &str);
  guppi_outside_alloc (str);
  return str;
}

void
guppi_text_block_set_text (GuppiTextBlock *text, const gchar *str)
{
  g_return_if_fail (GUPPI_IS_TEXT_BLOCK (text));
  g_return_if_fail (str);

  guppi_text_block_parse_gsml (text, str);
}

GnomeFont *
guppi_text_block_font (GuppiTextBlock *text)
{
  GuppiTextBlockPrivate *p;

  g_return_val_if_fail (GUPPI_IS_TEXT_BLOCK (text), NULL);
  p = priv (text);

  return p->font ? p->font : guppi_default_font ();
}

void
guppi_text_block_set_font (GuppiTextBlock *text, GnomeFont *font)
{
  GuppiTextBlockPrivate *p;

  g_return_if_fail (GUPPI_IS_TEXT_BLOCK (text));
  g_return_if_fail (font == NULL || GNOME_IS_FONT (font));
  p = priv (text);

  if (p->font != font) {
    guppi_refcounting_assign (p->font, font);
    p->max_width = -1;
    guppi_text_block_changed_delayed (text);
  }
}

double
guppi_text_block_angle (GuppiTextBlock *text)
{
  g_return_val_if_fail (GUPPI_IS_TEXT_BLOCK (text), 0);

  return priv (text)->angle;
}

void
guppi_text_block_set_angle (GuppiTextBlock *text, double angle)
{
  GuppiTextBlockPrivate *p;

  g_return_if_fail (GUPPI_IS_TEXT_BLOCK (text));
  p = priv (text);

  if (p->angle != angle) {
    p->angle = angle;
    guppi_text_block_changed_delayed (text);
  }
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
size_fn (gint glyph,
	 const GnomeFontFace *face,
	 double affine[6],
	 gpointer user_data)
{
  ArtDRect glyph_bbox;
  ArtDRect *bbox = (ArtDRect *)user_data;

  gnome_font_face_get_glyph_stdbbox (face, glyph, &glyph_bbox);
  art_drect_affine_transform (&glyph_bbox, &glyph_bbox, affine);
  art_drect_union (bbox, bbox, &glyph_bbox);
}

void
guppi_text_block_bbox (GuppiTextBlock *text, ArtDRect *bbox)
{
  g_return_if_fail (GUPPI_IS_TEXT_BLOCK (text));
  g_return_if_fail (bbox);

  bbox->x0 = bbox->y0 = bbox->x1 = bbox->y1 = 0;

  guppi_text_block_foreach_char (text, size_fn, bbox);

  /* Tweak the bounding box to avoid an annoying "cut-off" effect.
     Probably related to rounding. */
  bbox->x1 += 1.0;
  bbox->y1 += 1.0;
}

double
guppi_text_block_width (GuppiTextBlock *text)
{
  ArtDRect bbox;

  g_return_val_if_fail (GUPPI_IS_TEXT_BLOCK (text), -1);

  if (priv (text)->max_width > 0)
    return priv (text)->max_width;

  guppi_text_block_bbox (text, &bbox);
  return bbox.x1 - bbox.x0;
}

double
guppi_text_block_height (GuppiTextBlock *text)
{
  ArtDRect bbox;
  g_return_val_if_fail (GUPPI_IS_TEXT_BLOCK (text), -1);
  guppi_text_block_bbox (text, &bbox);
  return bbox.y1 - bbox.y0;
}

double
guppi_text_block_max_width (GuppiTextBlock *text)
{
  g_return_val_if_fail (GUPPI_IS_TEXT_BLOCK (text), 0);
  return priv (text)->max_width;
}

void
guppi_text_block_set_max_width (GuppiTextBlock *text, double w)
{
  g_return_if_fail (GUPPI_IS_TEXT_BLOCK (text));

  if (fabs (priv (text)->max_width - w) > 1) {
    priv (text)->max_width = w;    
    priv (text)->needs_soft_breaks = TRUE;
    guppi_text_block_changed_delayed (text);
  }
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

typedef struct _RenderState RenderState;
struct _RenderState {
  GnomeFont *font;
  double raise_lower;
  GtkJustification justify;
  gboolean allow_break;
};

static void
render_state_init (RenderState *rs)
{
  rs->font = guppi_default_font ();
  guppi_ref (rs->font);

  rs->raise_lower = 0.0;
  rs->justify = GTK_JUSTIFY_LEFT;
  rs->allow_break = TRUE;
}

static RenderState *
render_state_new (void)
{
  RenderState *rs = guppi_new0 (RenderState, 1);
  render_state_init (rs);
  return rs;
}

static void
render_state_free (RenderState *rs)
{
  if (rs) {
    guppi_unref0 (rs->font);
    guppi_free (rs);
  }
}

static RenderState *
render_state_copy (RenderState *rs)
{
  RenderState *cpy = render_state_new ();

  guppi_refcounting_assign (cpy->font, rs->font);

  cpy->raise_lower = rs->raise_lower;
  cpy->justify = rs->justify;
  cpy->allow_break = rs->allow_break;

  return cpy;
}

typedef struct _RenderStack RenderStack;
struct _RenderStack {
  RenderState *default_state;
  GList *stack;
};

static RenderStack *
render_stack_new (GuppiTextBlock *text)
{
  GuppiTextBlockPrivate *p = priv (text);
  RenderStack *ss = guppi_new0 (RenderStack, 1);

  ss->default_state = render_state_new ();
  ss->stack = NULL;

  /* Set default values for top frame from text. */
  if (p->font) {
    guppi_refcounting_assign (ss->default_state->font, p->font);
  }

  return ss;
}

static void
render_stack_free (RenderStack *ss)
{
  if (ss) {
    GList *iter;

    render_state_free (ss->default_state);
    ss->default_state = NULL;

    for (iter=ss->stack; iter; iter = g_list_next (iter))
      render_state_free ((RenderState *)iter->data);
    g_list_free (ss->stack);

    render_state_free (ss->default_state);

    guppi_free (ss);
  }
}

static RenderState *
render_stack_state (RenderStack *ss)
{
  g_return_val_if_fail (ss, NULL);

  if (ss->stack == NULL)
    ss->stack = g_list_prepend (ss->stack,
				render_state_copy (ss->default_state));
				
  return (RenderState *)ss->stack->data;
}

static void
render_stack_push (RenderStack *ss)
{
  g_return_if_fail (ss);

  ss->stack = g_list_prepend (ss->stack, 
			      render_state_copy (render_stack_state (ss)));
}

static void
render_stack_pop (RenderStack *ss)
{
  g_return_if_fail (ss);

  if (ss->stack) {
    render_state_free ((RenderState *)ss->stack->data);
    ss->stack = g_list_remove_link (ss->stack, ss->stack);
  }
}

static void
render_stack_evolve (RenderStack *stack, GuppiTextToken *token)
{
  GnomeFont *new_font;
  RenderState *state;
  gint type;

  g_return_if_fail (stack);
  g_return_if_fail (token);
  
  type = guppi_text_token_type (token);

  state = render_stack_state (stack);
  
  /* Handle our basic operations first. */
  switch (type) {
  case TEXT_TOKEN_PUSH:
    render_stack_push (stack);
    return;

  case TEXT_TOKEN_POP:
    render_stack_pop (stack);
    return;

  case TEXT_TOKEN_JUSTIFY:
    state->justify = guppi_text_token_justification (token);
    return;

  case TEXT_TOKEN_NOBREAK:
    state->allow_break = FALSE;
    return;
  }

  new_font = guppi_text_token_evolve_font (token, state->font);
  if (new_font) {
    guppi_ref (new_font);
    guppi_unref (state->font);
    state->font = new_font;
  }

  if (state->font)
    state->raise_lower -= guppi_text_token_raise_lower_distance (token) *
      gnome_font_get_size (state->font);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
token_size (GuppiTextToken *tt, RenderState *state,
	    double *width, double *ht_asc, double *ht_desc)
{
  gint type;
  const GnomeFontFace *face;
  double w=0, ha=0, hd=0;

  g_return_if_fail (tt);
  g_return_if_fail (state);

  type = guppi_text_token_type (tt);
  face = gnome_font_get_face (state->font);

  if (type == TEXT_TOKEN_WORD) {
    const gchar *str;
    gint i;
    gint glyph, next_glyph;

    str = guppi_text_token_word (tt);
    if (str) {

      next_glyph = gnome_font_face_lookup_default (face, str[0]);

      for (i=0; str[i]; ++i) {

	glyph = next_glyph;
	w += gnome_font_get_glyph_width (state->font, glyph);
	if (str[i+1]) {
	  next_glyph = gnome_font_face_lookup_default (face, str[i+1]);
	  w += gnome_font_get_glyph_kerning (state->font, glyph, next_glyph);
	}
      }

      ha = gnome_font_get_ascender (state->font);
      hd = gnome_font_get_descender (state->font);

      ha += state->raise_lower;
      hd -= state->raise_lower;
      if (ha < 0) ha = 0;
      if (hd < 0) hd = 0;
    }

  } else if (type == TEXT_TOKEN_SPACE) {

    w = gnome_font_get_width_string (state->font, " ");
    w *= guppi_text_token_space_size (tt);

  }

  /* Default: a zero-size token (usually a control token) */
  if (width) *width = w;
  if (ht_asc) *ht_asc = ha;
  if (ht_desc) *ht_desc = hd;
}

/* Line breaking for rotated lines is a bit fucked up right now. */

static void
insert_soft_breaks (GuppiTextBlock *text)
{
  GuppiTextBlockPrivate *p;
  RenderStack *stack;
  GList *i;
  GList *build;
  double max_width, run_width = 0, max_ha=0, max_hd=0;
  double last_space_size = 0;
  gboolean last_was_break = TRUE;
  double longest_line = 0;

  g_return_if_fail (GUPPI_IS_TEXT_BLOCK (text));

  p = priv (text);

  if (! p->needs_soft_breaks)
    return;

  p->needs_soft_breaks = FALSE;

  max_width = p->max_width;
  stack = render_stack_new (text);

  /* First, clean up old broken token list */

  for (i=p->broken; i; i=g_list_next (i)) {
    GuppiTextToken *tt;

    g_assert (i->data);
    tt = GUPPI_TEXT_TOKEN (i->data);
        
    if (guppi_text_token_type (tt) == TEXT_TOKEN_SOFT_BREAK)
      guppi_text_token_free (tt);
  }
  g_list_free (p->broken);
  p->broken = NULL;

  /*
    Now inject new soft breaks, based on our max width.
    Spaces immediately before or after breaks are dropped.
  */
  
  for (i=p->tokens; i; i=g_list_next (i)) {
    GuppiTextToken *tt = GUPPI_TEXT_TOKEN (i->data);
    gint type = guppi_text_token_type (tt);
    gboolean is_space = (type == TEXT_TOKEN_SPACE);
    gboolean is_hard = (type == TEXT_TOKEN_HARD_BREAK);
    RenderState *state;
    double w, ha, hd;

    if (is_space && last_was_break)
      continue;

    if (last_was_break) {
      max_ha = max_hd = 0;
    }

    state = render_stack_state (stack);

    token_size (tt, state, &w, &ha, &hd);

    max_ha = MAX (max_ha, ha);
    max_hd = MAX (max_hd, fabs (hd));
  
    if ((max_width > 0 && state->allow_break && run_width + w > max_width)
	|| is_hard) {
      GuppiTextToken *sbrk;

      run_width -= last_space_size;

      sbrk = guppi_text_token_new_soft_break (run_width, max_ha, max_hd,
					      is_hard);
      p->broken = g_list_append (p->broken, sbrk);
      longest_line = MAX (longest_line, run_width);

      run_width = 0;
      last_space_size = 0;
      last_was_break = TRUE;

    } else {
      last_was_break = FALSE;
    }

    if (!(last_was_break && is_space)) {
      run_width += w;
      p->broken = g_list_append (p->broken, tt);
    }

    last_was_break = guppi_text_token_is_break (tt);
    
    if (is_space) 
      last_space_size += w;
    else if (type == TEXT_TOKEN_WORD)
      last_space_size = 0;
    
    render_stack_evolve (stack, tt);
  }

  /* Add a final soft break w/ the length of the bottom line. */
  if (run_width > 0) {
    GuppiTextToken *sbrk = guppi_text_token_new_soft_break (run_width, 
							    max_ha, max_hd,
							    TRUE);
    p->broken = g_list_append (p->broken, sbrk);
    longest_line = MAX (longest_line, run_width);
  }

  
  /* Strip out the space tokens preceeding the breaks */
  p->broken = g_list_reverse (p->broken);
  build = NULL;
  last_was_break = FALSE;
  for (i=p->broken; i; i=g_list_next (i)) {
    GuppiTextToken *tt = GUPPI_TEXT_TOKEN (i->data);
    if (!(last_was_break && guppi_text_token_type (tt) == TEXT_TOKEN_SPACE)) {
      build = g_list_append (build, tt);
      last_was_break = guppi_text_token_is_break (tt);
    }
  }
  g_list_free (p->broken);
  p->broken = g_list_reverse (build);

  render_stack_free (stack);

  if (p->max_width < 1e-8)
    p->max_width = longest_line;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

void
guppi_text_block_foreach_word (GuppiTextBlock *text,
			       text_word_fn fn,
			       gpointer user_data)
{
  GuppiTextBlockPrivate *p;
  RenderStack *stack;
  double linestart_off_x, linestart_off_y, off_x, off_y;
  double line_length = 0, line_ascender = 0, line_descender = 0;
  double space_padding = 0;
  gint space_count = 0;
  GList *first_token, *iter;
  gboolean last_was_break;
  gboolean first_word_of_line = TRUE;
  GuppiTextToken *brk_tt = NULL;
  double max_width;

  g_return_if_fail (GUPPI_IS_TEXT_BLOCK (text));
  g_return_if_fail (fn != NULL);

  p = priv (text);

  if (p->needs_soft_breaks)
    insert_soft_breaks (text);

  stack = render_stack_new (text);

  off_x = off_y = linestart_off_x = linestart_off_y = 0;
  
  first_token = p->broken ? p->broken : p->tokens;

  max_width = p->max_width;
  /* If we don't have a max width set, find the longest line and use that
     quantity for our justification calculations. */
  if (max_width <= 0) {
    iter = first_token;
    while (iter) {
      GuppiTextToken *tt = GUPPI_TEXT_TOKEN (iter->data);
      double w = 0;
      if (guppi_text_token_type (tt) == TEXT_TOKEN_SOFT_BREAK) {
	guppi_text_token_soft_break_line_dimensions (tt, &w, NULL, NULL);
	max_width = MAX (max_width, w);
      }
      iter = g_list_next (iter);
    }
  }


  /* We want to call the beginning-of-line code on our first pass
     through the the loop. */
  last_was_break = TRUE;

  for (iter = first_token; iter != NULL; iter = g_list_next (iter)) {
    GuppiTextToken *tt = GUPPI_TEXT_TOKEN (iter->data);
    RenderState *rs = render_stack_state (stack);

    if (last_was_break) {
      GList *j;

      space_count = 0;
      space_padding = 0;

      if (iter != first_token)
	linestart_off_y += line_ascender;
      
      j = iter;
      brk_tt = NULL;
      while (j) {
	gint toktype;

	brk_tt = GUPPI_TEXT_TOKEN(j->data);
	toktype = guppi_text_token_type (brk_tt);
	
	if (toktype == TEXT_TOKEN_SPACE)
	  ++space_count;

	j = (toktype != TEXT_TOKEN_SOFT_BREAK) ? g_list_next (j) : NULL;
      }

      if (brk_tt && guppi_text_token_type (brk_tt) != TEXT_TOKEN_SOFT_BREAK)
	brk_tt = NULL;

      if (brk_tt) {
	guppi_text_token_soft_break_line_dimensions (brk_tt,
						     &line_length,
						     &line_ascender,
						     &line_descender);
      } else {
	line_length = 0;
	line_ascender = 0;
	line_descender = 0;
      }

      if (iter != first_token)
	linestart_off_y += line_descender;

      off_x = linestart_off_x;
      off_y = linestart_off_y;

      first_word_of_line = TRUE;
      last_was_break = FALSE;
    }

    
    switch (guppi_text_token_type (tt)) {

    case TEXT_TOKEN_WORD:
      {
	const gchar *str = guppi_text_token_word (tt);

	if (first_word_of_line && brk_tt) {

	  switch (rs->justify) {
	  case GTK_JUSTIFY_LEFT:
	    /* This one is easy... just do nothing. */
	    break;

	  case GTK_JUSTIFY_RIGHT:
	    off_x += max_width - line_length;
	    break;

	  case GTK_JUSTIFY_CENTER:
	    off_x += (max_width - line_length)/2;
	    break;

	  case GTK_JUSTIFY_FILL:
	    if (space_count > 0
		&& !guppi_text_token_soft_break_from_hard (brk_tt))
	      space_padding = (max_width - line_length)/space_count;
	    else
	      space_padding = 0;
	    break;
	    
	  default:
	    g_assert_not_reached ();
	  }
	  
	}

	fn (str, rs->font, off_x, off_y + rs->raise_lower, user_data);
	off_x += gnome_font_get_width_string (rs->font, str);
	first_word_of_line = FALSE;
	break;
      }

    case TEXT_TOKEN_SPACE:
      {
	double w = gnome_font_get_width_string (rs->font, " ");
	w *= guppi_text_token_space_size (tt);
	off_x += w + space_padding;
	break;
      }

    case TEXT_TOKEN_SOFT_BREAK:
      last_was_break = TRUE;
      break;

    case TEXT_TOKEN_HARD_BREAK:
      /* Each hard break gets a corresponding soft break --- so do
	 nothing. */
      break;

    default:
      /* Do nothing. */
    }

    render_stack_evolve (stack, tt);
  }

  render_stack_free (stack);
}

typedef struct _w2c_info w2c_info;
struct _w2c_info {
  GuppiTextBlock *text;
  text_char_fn fn;
  gpointer user_data;
};

static void
word_to_char_cb (const gchar *word,
		 GnomeFont *font,
		 double off_x, double off_y,
		 gpointer user_data)
{
  w2c_info *info = (w2c_info *) user_data;

  const GnomeFontFace *face;
  gint i, glyph, next_glyph;
  double sz, szaff[6], rtaff[6], aff[6];

  if (word == NULL)
    return;

  face = gnome_font_get_face (font);

  sz = gnome_font_get_size (font) * 0.001;
  art_affine_scale (szaff, sz, -sz);

  art_affine_rotate (rtaff, guppi_text_block_angle (info->text));

  next_glyph = gnome_font_face_lookup_default (face, word[0]);

  for (i=0; word[i]; ++i) {

    glyph = next_glyph;

    if (word[i+1])
      next_glyph = gnome_font_face_lookup_default (face, word[i+1]);

    if (glyph > 0) {
    
      art_affine_translate (aff, off_x, off_y);

      art_affine_multiply (aff, aff, rtaff);
      art_affine_multiply (aff, szaff, aff);

      info->fn (glyph, face, aff, info->user_data);
      
      off_x += gnome_font_get_glyph_width (font, glyph);

      /* Kerning!  We rule! */
      if (word[i+1])
	off_x += gnome_font_get_glyph_kerning (font, glyph, next_glyph);
    }
  }
}

void
guppi_text_block_foreach_char (GuppiTextBlock *text,
			       text_char_fn fn,
			       gpointer user_data)
{

  w2c_info info;

  g_return_if_fail (text && GUPPI_IS_TEXT_BLOCK (text));
  g_return_if_fail (fn);

  info.text = text;
  info.fn = fn;
  info.user_data = user_data;

  guppi_text_block_foreach_word (text, word_to_char_cb, &info); 
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

typedef struct _print_info print_info;
struct _print_info {
  GuppiElementPrint *ep;
  GnomeFont *last_font;
};

static void
word_cb (const gchar *str,
	 GnomeFont *font,
	 double off_x, double off_y,
	 gpointer user_data)
{
  print_info *info = (print_info *) user_data;

  if (font != info->last_font) {
    guppi_element_print_setfont (info->ep, font);
    info->last_font = font;
  }

  guppi_element_print_moveto (info->ep, off_x, -off_y);
  guppi_element_print_show (info->ep, str);
}

void
guppi_text_block_print (GuppiTextBlock *text,
			GuppiElementPrint *ep,
			double x, double y,
			GtkAnchorType anchor)
{
  print_info info;
  ArtDRect bbox;
  double aff[6], w, h, tx, ty, saved_angle;

  g_return_if_fail (text && GUPPI_IS_TEXT_BLOCK (text));
  g_return_if_fail (ep && GUPPI_IS_ELEMENT_PRINT (ep));

  info.ep = ep;
  info.last_font = NULL;

  guppi_element_print_gsave (ep);

  saved_angle = guppi_text_block_angle (text);
  guppi_text_block_set_angle (text, 0);
  guppi_text_block_bbox (text, &bbox);
  h = bbox.y1 - bbox.y0;
  w = bbox.x1 - bbox.x0;
  guppi_text_block_set_angle (text, saved_angle);

  art_affine_translate (aff, x, y);
  guppi_element_print_concat (ep, aff);

  art_affine_rotate (aff, -guppi_text_block_angle (text));
  guppi_element_print_concat (ep, aff);
  
  tx = 0;
  ty = 0;

  switch (anchor) {
  case GTK_ANCHOR_NORTH_WEST:
  case GTK_ANCHOR_WEST:
  case GTK_ANCHOR_SOUTH_WEST:
    tx = 0;
    break;
    
  case GTK_ANCHOR_NORTH:
  case GTK_ANCHOR_CENTER:
  case GTK_ANCHOR_SOUTH:
    tx = -w/2;
    break;

  case GTK_ANCHOR_NORTH_EAST:
  case GTK_ANCHOR_EAST:
  case GTK_ANCHOR_SOUTH_EAST:
    tx = -w;
    break;

  default:
    g_assert_not_reached ();
  }

  switch (anchor) {
  case GTK_ANCHOR_NORTH_WEST:
  case GTK_ANCHOR_NORTH:
  case GTK_ANCHOR_NORTH_EAST:
    ty = 0;
    break;

  case GTK_ANCHOR_WEST:
  case GTK_ANCHOR_CENTER:
  case GTK_ANCHOR_EAST:
    ty = h/2;
    break;

  case GTK_ANCHOR_SOUTH_WEST:
  case GTK_ANCHOR_SOUTH:
  case GTK_ANCHOR_SOUTH_EAST:
    ty = h;
    break;

  default:
    g_assert_not_reached ();
  }

  art_affine_translate (aff, tx, ty);
  guppi_element_print_concat (ep, aff);

  guppi_text_block_foreach_word (text, word_cb, &info);

  guppi_element_print_grestore (ep);
}

gboolean
guppi_text_block_line_dimensions (GuppiTextBlock *text,
				  gint line_no,
				  double *length,
				  double *height_ascend,
				  double *height_descend)
{
  GuppiTextBlockPrivate *p;
  GList *i;

  g_return_val_if_fail (GUPPI_IS_TEXT_BLOCK (text), FALSE);
  g_return_val_if_fail (line_no >= 0, FALSE);

  insert_soft_breaks (text);

  p = priv (text);

  if (p->broken == NULL)
    return FALSE;

  for (i = p->broken; i != NULL; i = g_list_next (i)) {
    GuppiTextToken *tt = GUPPI_TEXT_TOKEN (i->data);

    if (guppi_text_token_type (tt) == TEXT_TOKEN_SOFT_BREAK) {

      if (line_no == 0) {
	
	guppi_text_token_soft_break_line_dimensions (tt,
						     length,
						     height_ascend,
						     height_descend);
	return TRUE;

      }

      --line_no;

    }
  }

  return FALSE;
  
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

void
guppi_text_block_changed (GuppiTextBlock *text)
{
  GuppiTextBlockPrivate *p;

  g_return_if_fail (GUPPI_IS_TEXT_BLOCK (text));
  p = priv (text);

  if (p->freeze_count > 0) {

    p->freeze_type = PENDING_IMMEDIATE;

  } else {

    if (p->pending_changed_signal) {
      gtk_idle_remove (p->pending_changed_signal);
      p->pending_changed_signal = 0;
    }
    
    gtk_signal_emit (GTK_OBJECT (text), guppi_text_block_signals[CHANGED]);
  }
}

static gint
idle_cb (gpointer user_data)
{
  GuppiTextBlock *text = GUPPI_TEXT_BLOCK (user_data);
  GuppiTextBlockPrivate *p = priv (text);

  gtk_signal_emit (GTK_OBJECT (text), guppi_text_block_signals[CHANGED]);
  p->pending_changed_signal = 0;

  return 0;
}

void
guppi_text_block_changed_delayed (GuppiTextBlock *text)
{
  GuppiTextBlockPrivate *p;

  g_return_if_fail (GUPPI_IS_TEXT_BLOCK (text));
  p = priv (text);

  if (p->freeze_count > 0) {

    if (p->freeze_type == PENDING_NONE)
      p->freeze_type = PENDING_DELAYED;

  } else {
    
    if (!p->pending_changed_signal)
      p->pending_changed_signal = gtk_idle_add (idle_cb, text);

  }
}

void
guppi_text_block_flush_changes (GuppiTextBlock *text)
{
  GuppiTextBlockPrivate *p;

  g_return_if_fail (GUPPI_IS_TEXT_BLOCK (text));
  p = priv (text);
  
  if (p->pending_changed_signal)
    guppi_text_block_changed (text);
}

void
guppi_text_block_freeze (GuppiTextBlock *text)
{
  GuppiTextBlockPrivate *p;

  g_return_if_fail (GUPPI_IS_TEXT_BLOCK (text));
  p = priv (text);

  ++p->freeze_count;

  if (p->pending_changed_signal) {
    gtk_idle_remove (p->pending_changed_signal);
    p->pending_changed_signal = 0;
    p->freeze_type = PENDING_DELAYED;
  }
}

void
guppi_text_block_thaw (GuppiTextBlock *text)
{
  GuppiTextBlockPrivate *p;

  g_return_if_fail (GUPPI_IS_TEXT_BLOCK (text));
  p = priv (text);

  g_return_if_fail (p->freeze_count > 0);

  --p->freeze_count;
  if (p->freeze_count == 0) {

    switch (p->freeze_type) {
    case PENDING_NONE:
      break;
    case PENDING_IMMEDIATE:
      guppi_text_block_changed (text);
      break;
    case PENDING_DELAYED:
      guppi_text_block_changed_delayed (text);
      break;
    default:
      g_assert_not_reached ();
    }
   
    p->freeze_type = PENDING_NONE;
  }
}
