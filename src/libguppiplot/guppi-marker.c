/* $Id$ */

/*
 * guppi-marker.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-marker.h"

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>

#include <math.h>
#include <ctype.h>
#include <guppi-metrics.h>
#include <guppi-memory.h>
#include "guppi-pixbuf-stock.h"

#define CIRCLE_AREA_FACTOR   1.0
#define SQUARE_AREA_FACTOR   1.2533141373155002512  /* sqrt(pi / 2) */
#define TRIANGLE_AREA_FACTOR 1.55512030155621416074 /* sqrt(4 pi / (3 sqrt 3)) */

#define CIRCLE_DEFAULT_RADIUS   (4.0 * CIRCLE_AREA_FACTOR)
#define SQUARE_DEFAULT_RADIUS   (CIRCLE_DEFAULT_RADIUS * SQUARE_AREA_FACTOR)
#define TRIANGLE_DEFAULT_RADIUS (CIRCLE_DEFAULT_RADIUS * TRIANGLE_AREA_FACTOR)
#define STARBURST_DEFAULT_RADIUS 7.0

#define CIRCLE_DEFAULT_WIDTH   (0.20 * CIRCLE_DEFAULT_RADIUS)
#define SQUARE_DEFAULT_WIDTH   CIRCLE_DEFAULT_WIDTH
#define TRIANGLE_DEFAULT_WIDTH CIRCLE_DEFAULT_WIDTH
#define STARBURST_DEFAULT_WIDTH 2.5

#define STARBURST_EDGE_FACTOR 0.45

#define MIN_SIZE_FACTOR 0.75
#define MAX_SIZE_FACTOR 4.0

typedef struct _GuppiMarkerInfoPrivate GuppiMarkerInfoPrivate;

struct _GuppiMarkerInfoPrivate {
  GuppiMarkerInfo info;

  GuppiAlphaTemplate *(*make_alpha_template) (double sz1, double sz2);
  GuppiPixbuf        *(*make_pixbuf)         (double sz1, double sz2);

  void (*print) (GuppiElementPrint *, double x, double y,
		 double sz1, double sz2);
};

/**************************************************************************/

/*** Marker Alpha Template Constructors */

static GuppiPixbuf *
pixbuf_circle (double r, double w)
{
  r = guppi_pt2px (r);
  w = guppi_pt2px (w);

  return guppi_pixbuf_stock_new_circle (r, w);
}

static GuppiPixbuf *
pixbuf_diamond (double r, double w)
{
  r = guppi_pt2px (r);
  w = guppi_pt2px (w);

  return guppi_pixbuf_stock_new_square (r, w, M_PI / 4);
}

static GuppiPixbuf *
pixbuf_cross (double r, double w)
{
  r = guppi_pt2px (r);
  w = guppi_pt2px (w);

  return guppi_pixbuf_stock_new_starburst (r, w, STARBURST_EDGE_FACTOR * w, 4, 0);
}

static GuppiPixbuf *
pixbuf_x (double r, double w)
{
  r = guppi_pt2px (r);
  w = guppi_pt2px (w);

  return guppi_pixbuf_stock_new_starburst (r, w, STARBURST_EDGE_FACTOR * w, 4, M_PI / 4);
}

static GuppiPixbuf *
pixbuf_square (double r, double w)
{
  r = guppi_pt2px (r);
  w = guppi_pt2px (w);

  return guppi_pixbuf_stock_new_square (r, w, 0);
}

static GuppiPixbuf *
pixbuf_ast (double r, double w)
{
  r = guppi_pt2px (r);
  w = guppi_pt2px (w);

  return guppi_pixbuf_stock_new_starburst (r, w, STARBURST_EDGE_FACTOR * w, 5, -M_PI/2);
}

static GuppiPixbuf *
pixbuf_triangle (double r, double w)
{
  r = guppi_pt2px (r);
  w = guppi_pt2px (w);

  return guppi_pixbuf_stock_new_triangle (r, w, 0);
}

static GuppiAlphaTemplate *
template_bar (double r, double w)
{
  r = guppi_pt2px (r);
  w = guppi_pt2px (w);

  return guppi_alpha_template_new_bar (r, r, w, 0);
}

static GuppiAlphaTemplate *
template_half_bar (double r, double w)
{
  r = guppi_pt2px (r);
  w = guppi_pt2px (w);

  return guppi_alpha_template_new_bar (r, 0, w, 0);
}

#if 0
static GuppiAlphaTemplate *
template_pixel (double sz1, double sz2)
{
  g_assert_not_reached ();
  return NULL;
}

static GuppiAlphaTemplate *
template_dot (double sz1, double sz2)
{
  g_assert_not_reached ();
  return NULL;
}
#endif

static GuppiPixbuf *
pixbuf_filled_circle (double r, double unused)
{
  r = guppi_pt2px (r);
  return guppi_pixbuf_stock_new_circle (r, 0);
}

static GuppiPixbuf *
pixbuf_filled_square (double r, double unused)
{
  r = guppi_pt2px (r);
  return guppi_pixbuf_stock_new_square (r, 0, 0);
}

static GuppiPixbuf *
pixbuf_filled_diamond (double r, double unused)
{
  r = guppi_pt2px (r);
  return guppi_pixbuf_stock_new_square (r, 0, M_PI / 4);
}

static GuppiPixbuf *
pixbuf_filled_triangle (double r, double unused)
{
  r = guppi_pt2px (r);
  return guppi_pixbuf_stock_new_triangle (r, 0, 0);
}

/**************************************************************************/

/*** Marker Print Routines */

static void
generic_print_circle (GuppiElementPrint * ep,
		      double x, double y,
		      double r, double w,
		      gboolean filled, guint32 fill_color, guint32 edge_color)
{
  double max_error;
  double max_theta;
  gint N;
  gint i, j;

  g_return_if_fail (ep != NULL);
  g_return_if_fail (GUPPI_IS_ELEMENT_PRINT (ep));

  if (r <= 0)
    return;

  /* Max error is 5% of radius or 1/16" of an inch, whichever is smaller */
  max_error = MIN (0.05 * r, 72 / 16.0);

  max_theta = 2 * acos (1 - max_error / r);
  N = MAX ((gint) ceil (2 * M_PI / max_theta), 16);

  for (j = 0; j < 2; ++j) {

    if ((j == 0 && filled) || (j == 1 && w > 0)) {

      guppi_element_print_newpath (ep);

      for (i = 0; i < N; ++i) {
	double theta = 2 * M_PI * i / (double) N;
	double xx = x + r * cos (theta);
	double yy = y + r * sin (theta);
	if (i == 0)
	  guppi_element_print_moveto (ep, xx, yy);
	else
	  guppi_element_print_lineto (ep, xx, yy);
      }

      guppi_element_print_closepath (ep);

      if (j == 0 && filled) {

	if (fill_color != 0)
	  guppi_element_print_setrgbacolor_uint (ep, fill_color);

	guppi_element_print_fill (ep);

      } else if (j == 1 && w > 0) {

	if (edge_color != 0)
	  guppi_element_print_setrgbacolor_uint (ep, edge_color);

	guppi_element_print_setlinewidth (ep, w);
	guppi_element_print_stroke (ep);
      }
    }
  }
}

static void
generic_print_diamond (GuppiElementPrint * ep,
		       double x, double y,
		       double sz, double w,
		       gboolean filled,
		       guint32 fill_color, guint32 edge_color)
{
  gint j;
  g_return_if_fail (ep != NULL);
  g_return_if_fail (GUPPI_IS_ELEMENT_PRINT (ep));

  for (j = 0; j < 2; ++j) {

    if ((j == 0 && filled) || (j == 1 && w > 0)) {

      guppi_element_print_newpath (ep);
      guppi_element_print_moveto (ep, x + sz, y);
      guppi_element_print_lineto (ep, x, y + sz);
      guppi_element_print_lineto (ep, x - sz, y);
      guppi_element_print_lineto (ep, x - sz, y);
      guppi_element_print_closepath (ep);

      if (j == 0 && filled) {

	if (fill_color != 0)
	  guppi_element_print_setrgbacolor_uint (ep, fill_color);
	guppi_element_print_fill (ep);

      } else if (j == 1 && w > 0) {

	guppi_element_print_setlinewidth (ep, w);
	guppi_element_print_stroke (ep);

      }
    }
  }
}

static void
generic_print_square (GuppiElementPrint * ep,
		      double x, double y,
		      double sz, double w,
		      gboolean filled, guint32 fill_color, guint32 edge_color)
{
  gint j;
  g_return_if_fail (ep != NULL);
  g_return_if_fail (GUPPI_IS_ELEMENT_PRINT (ep));

  for (j = 0; j < 2; ++j) {

    if ((j == 0 && filled) || (j == 1 && w > 0)) {

      guppi_element_print_newpath (ep);
      guppi_element_print_moveto (ep, x + sz, y + sz);
      guppi_element_print_lineto (ep, x - sz, y + sz);
      guppi_element_print_lineto (ep, x - sz, y - sz);
      guppi_element_print_lineto (ep, x + sz, y - sz);
      guppi_element_print_closepath (ep);

      if (j == 0 && filled) {

	if (fill_color != 0)
	  guppi_element_print_setrgbacolor_uint (ep, fill_color);
	guppi_element_print_fill (ep);

      } else if (j == 1 && w > 0) {

	guppi_element_print_setlinewidth (ep, w);
	guppi_element_print_stroke (ep);

      }
    }
  }
}

static void
generic_print_triangle (GuppiElementPrint * ep,
			double x, double y,
			double sz, double w,
			gboolean filled,
			guint32 fill_color, guint32 edge_color)
{
  gint j;
  g_return_if_fail (ep != NULL);
  g_return_if_fail (GUPPI_IS_ELEMENT_PRINT (ep));

  for (j = 0; j < 2; ++j) {

    if ((j == 0 && filled) || (j == 1 && w > 0)) {

      guppi_element_print_newpath (ep);
      guppi_element_print_moveto (ep, x - sz, y - sz);
      guppi_element_print_lineto (ep, x, y + sz);
      guppi_element_print_lineto (ep, x + sz, y - sz);
      guppi_element_print_closepath (ep);

      if (j == 0 && filled) {

	if (fill_color != 0)
	  guppi_element_print_setrgbacolor_uint (ep, fill_color);
	guppi_element_print_fill (ep);

      } else if (j == 1 && w > 0) {

	guppi_element_print_setlinewidth (ep, w);
	guppi_element_print_stroke (ep);

      }
    }
  }
}

static void
generic_print_bar (GuppiElementPrint * ep,
		   double x, double y,
		   double sz, gboolean half,
		   guint32 edge_color)
{
  g_return_if_fail (ep != NULL);
  g_return_if_fail (GUPPI_IS_ELEMENT_PRINT (ep));

  guppi_element_print_newpath (ep);
  guppi_element_print_moveto (ep, half ? x : x - sz, y);
  guppi_element_print_lineto (ep, x + sz, y);
  guppi_element_print_closepath (ep);
  guppi_element_print_stroke (ep);
}

static void
print_circle (GuppiElementPrint * ep, double x, double y, double r, double w)
{
  generic_print_circle (ep, x, y, r, w, FALSE, 0, 0);
}

static void
print_diamond (GuppiElementPrint * ep, double x, double y,
	       double sz, double w)
{
  generic_print_diamond (ep, x, y, sz, w, FALSE, 0, 0);
}

static void
print_cross (GuppiElementPrint * ep, double x, double y, double sz, double w)
{
  guppi_element_print_setlinewidth (ep, w);

  guppi_element_print_moveto (ep, x + sz, y);
  guppi_element_print_lineto (ep, x - sz, y);
  guppi_element_print_stroke (ep);

  guppi_element_print_moveto (ep, x, y + sz);
  guppi_element_print_lineto (ep, x, y - sz);
  guppi_element_print_stroke (ep);
}

static void
print_x (GuppiElementPrint * ep, double x, double y, double sz, double w)
{
  guppi_element_print_setlinewidth (ep, w);

  guppi_element_print_moveto (ep, x + sz, y + sz);
  guppi_element_print_lineto (ep, x - sz, y - sz);
  guppi_element_print_stroke (ep);

  guppi_element_print_moveto (ep, x + sz, y - sz);
  guppi_element_print_lineto (ep, x - sz, y + sz);
  guppi_element_print_stroke (ep);
}

static void
print_square (GuppiElementPrint * ep, double x, double y, double sz, double w)
{
  generic_print_square (ep, x, y, sz, w, FALSE, 0, 0);
}

static void
print_ast (GuppiElementPrint * ep, double x, double y, double sz, double w)
{
  double sz1 = sz;
  double sz2 = sz / M_SQRT2;

  guppi_element_print_setlinewidth (ep, w);

  /* This is basically an X and a Cross superimposed */

  /* The Cross */
  guppi_element_print_moveto (ep, x + sz1, y);
  guppi_element_print_lineto (ep, x - sz1, y);
  guppi_element_print_stroke (ep);

  guppi_element_print_moveto (ep, x, y + sz1);
  guppi_element_print_lineto (ep, x, y - sz1);
  guppi_element_print_stroke (ep);

  /* The X */
  guppi_element_print_moveto (ep, x + sz2, y + sz2);
  guppi_element_print_lineto (ep, x - sz2, y - sz2);
  guppi_element_print_stroke (ep);

  guppi_element_print_moveto (ep, x + sz2, y - sz2);
  guppi_element_print_lineto (ep, x - sz2, y + sz2);
  guppi_element_print_stroke (ep);
}

static void
print_triangle (GuppiElementPrint * ep, double x, double y,
		double sz, double unused)
{
  generic_print_triangle (ep, x, y, sz, 0, FALSE, 0, 0);
}

static void
print_bar (GuppiElementPrint * ep, double x, double y,
	   double sz, double unused)
{
  generic_print_bar (ep, x, y, sz, FALSE, 0);
}

static void
print_half_bar (GuppiElementPrint * ep, double x, double y,
		double sz, double unused)
{
  generic_print_bar (ep, x, y, sz, TRUE, 0);
}

static void
print_filled_circle (GuppiElementPrint * ep, double x, double y,
		     double r, double unused)
{
  generic_print_circle (ep, x, y, r, 0, TRUE, 0, 0);
}

static void
print_filled_square (GuppiElementPrint * ep, double x, double y,
		     double sz, double unused)
{
  generic_print_diamond (ep, x, y, sz, 0, TRUE, 0, 0);
}

static void
print_filled_diamond (GuppiElementPrint * ep, double x, double y,
		      double sz, double unused)
{
  generic_print_diamond (ep, x, y, sz, 0, TRUE, 0, 0);
}

static void
print_filled_triangle (GuppiElementPrint * ep, double x, double y,
		      double sz, double unused)
{
  generic_print_triangle (ep, x, y, sz, 0, TRUE, 0, 0);
}

/**************************************************************************/

static GuppiMarkerInfoPrivate guppi_marker_info_array[] = {

  {{GUPPI_MARKER_NONE, "none", N_("None"),
    NULL, 0, 0, 0,
    NULL, 0, 0, 0},
   NULL, NULL, NULL},

  {{GUPPI_MARKER_CIRCLE, "circle", N_("Circle"),
    N_("Radius"),
    MIN_SIZE_FACTOR * CIRCLE_DEFAULT_RADIUS,
    MAX_SIZE_FACTOR * CIRCLE_DEFAULT_RADIUS,
    CIRCLE_DEFAULT_RADIUS,
    N_("Thickness"), 
    MIN_SIZE_FACTOR * CIRCLE_DEFAULT_WIDTH,
    MAX_SIZE_FACTOR * CIRCLE_DEFAULT_WIDTH,
    CIRCLE_DEFAULT_WIDTH},
   NULL,
   pixbuf_circle,
   print_circle},

  {{GUPPI_MARKER_DIAMOND, "diamond", N_("Diamond"),
    N_("Radius"),
    MIN_SIZE_FACTOR * SQUARE_DEFAULT_RADIUS,
    MAX_SIZE_FACTOR * SQUARE_DEFAULT_RADIUS,
    SQUARE_DEFAULT_RADIUS,
    N_("Thickness"),
    MIN_SIZE_FACTOR * SQUARE_DEFAULT_WIDTH, 
    MAX_SIZE_FACTOR * SQUARE_DEFAULT_WIDTH,
    SQUARE_DEFAULT_WIDTH},
   NULL,
   pixbuf_diamond,
   print_diamond},

  {{GUPPI_MARKER_CROSS, "cross", N_("Cross"),
    N_("Radius"), 
    MIN_SIZE_FACTOR * STARBURST_DEFAULT_RADIUS,
    MAX_SIZE_FACTOR * STARBURST_DEFAULT_RADIUS,
    STARBURST_DEFAULT_RADIUS,
    N_("Thickness"), 
    MIN_SIZE_FACTOR * STARBURST_DEFAULT_WIDTH,
    MAX_SIZE_FACTOR * STARBURST_DEFAULT_WIDTH,
    STARBURST_DEFAULT_WIDTH},
   NULL,
   pixbuf_cross,
   print_cross},

  {{GUPPI_MARKER_X, "x", N_("X"),
    N_("Radius"), 
    MIN_SIZE_FACTOR * STARBURST_DEFAULT_RADIUS,
    MAX_SIZE_FACTOR * STARBURST_DEFAULT_RADIUS,
    STARBURST_DEFAULT_RADIUS,
    N_("Thickness"),
    MIN_SIZE_FACTOR * STARBURST_DEFAULT_WIDTH,
    MAX_SIZE_FACTOR * STARBURST_DEFAULT_WIDTH,
    STARBURST_DEFAULT_WIDTH},
   NULL,
   pixbuf_x,
   print_x},

  {{GUPPI_MARKER_SQUARE, "square", N_("Square"),
    N_("Radius"),
    MIN_SIZE_FACTOR * SQUARE_DEFAULT_RADIUS,
    MAX_SIZE_FACTOR * SQUARE_DEFAULT_RADIUS,
    SQUARE_DEFAULT_RADIUS,
    N_("Thickness"),
    MIN_SIZE_FACTOR * SQUARE_DEFAULT_WIDTH,
    MAX_SIZE_FACTOR * SQUARE_DEFAULT_WIDTH,
    SQUARE_DEFAULT_WIDTH},
   NULL,
   pixbuf_square,
   print_square},

  {{GUPPI_MARKER_AST, "asterisk", N_("Asterisk"),
    N_("Radius"),
    MIN_SIZE_FACTOR * STARBURST_DEFAULT_RADIUS,
    MAX_SIZE_FACTOR * STARBURST_DEFAULT_RADIUS,
    STARBURST_DEFAULT_RADIUS,
    N_("Thickness"),
    MIN_SIZE_FACTOR * STARBURST_DEFAULT_WIDTH,
    MAX_SIZE_FACTOR * STARBURST_DEFAULT_WIDTH,
    STARBURST_DEFAULT_WIDTH},
   NULL,
   pixbuf_ast,
   print_ast},

  {{GUPPI_MARKER_TRIANGLE, "triangle", N_("Triangle"),
    N_("Radius"), 
    MIN_SIZE_FACTOR * TRIANGLE_DEFAULT_RADIUS,
    MAX_SIZE_FACTOR * TRIANGLE_DEFAULT_RADIUS,
    TRIANGLE_DEFAULT_RADIUS,
    N_("Thickness"),
    MIN_SIZE_FACTOR * TRIANGLE_DEFAULT_WIDTH,
    MAX_SIZE_FACTOR * TRIANGLE_DEFAULT_WIDTH,
    TRIANGLE_DEFAULT_WIDTH},
   NULL,
   pixbuf_triangle,
   print_triangle},

  {{GUPPI_MARKER_BAR, "bar", N_("Bar"),
    N_("Length"), 1, 15, 4,
    NULL, 0.5, 12, 1},
   template_bar,
   NULL,
   print_bar},

  {{GUPPI_MARKER_HALF_BAR, "half bar", N_("Half Bar"),
    N_("Length"), 1, 15, 4,
    NULL, 0.5, 12, 1},
   template_half_bar,
   NULL,
   print_half_bar},

  {{GUPPI_MARKER_FILLED_CIRCLE, "filled circle", N_("Filled Circle"),
    N_("Radius"), 
    MIN_SIZE_FACTOR * CIRCLE_DEFAULT_RADIUS, 
    MAX_SIZE_FACTOR * CIRCLE_DEFAULT_RADIUS,
    CIRCLE_DEFAULT_RADIUS,
    NULL, 0, 0, 0},
   NULL,
   pixbuf_filled_circle,
   print_filled_circle},

  {{GUPPI_MARKER_FILLED_SQUARE, "filled square", N_("Filled Square"),
    N_("Radius"), 
    MIN_SIZE_FACTOR * SQUARE_DEFAULT_RADIUS, 
    MAX_SIZE_FACTOR * SQUARE_DEFAULT_RADIUS,
    SQUARE_DEFAULT_RADIUS,
    NULL, 0, 0, 0},
   NULL,
   pixbuf_filled_square,
   print_filled_square},

  {{GUPPI_MARKER_FILLED_DIAMOND, "filled diamond", N_("Filled Diamond"),
    N_("Radius"), 
    MIN_SIZE_FACTOR * SQUARE_DEFAULT_RADIUS,
    MAX_SIZE_FACTOR * SQUARE_DEFAULT_RADIUS,
    SQUARE_DEFAULT_RADIUS,
    NULL, 0.5, 12, 1},
   NULL,
   pixbuf_filled_diamond,
   print_filled_diamond},

  {{GUPPI_MARKER_FILLED_TRIANGLE, "filled triangle", N_("Filled Triangle"),
    N_("Radius"), 
    MIN_SIZE_FACTOR * TRIANGLE_DEFAULT_RADIUS, 
    MAX_SIZE_FACTOR * TRIANGLE_DEFAULT_RADIUS,
    TRIANGLE_DEFAULT_RADIUS,
    NULL, 0.5, 12, 1},
   NULL,
   pixbuf_filled_triangle,
   print_filled_triangle}
};

static gboolean
guppi_marker_valid (GuppiMarker m)
{
  return (gint) m >= 0 && (gint) m < (gint) GUPPI_MARKER_LAST;
}

const GuppiMarkerInfo *
guppi_marker_info (GuppiMarker m)
{
  gint i;
  GuppiMarkerInfo *mi;

  g_return_val_if_fail (guppi_marker_valid (m), NULL);

  i = (gint) m;
  mi = (GuppiMarkerInfo *) & guppi_marker_info_array[i];
  if (mi->marker == m)
    return mi;

  /* The classic useful warning message. */
  g_message ("Uh oh... something is wrong!");

  i = 0;
  while (i < (gint) GUPPI_MARKER_LAST) {
    if (guppi_marker_info_array[i].info.marker == m)
      return (GuppiMarkerInfo *) & guppi_marker_info_array[i];
    ++i;
  }

  g_assert_not_reached ();
  return NULL;
}

static gboolean
loose_match (const gchar * a, const gchar * b)
{
  gchar aa, bb;
  while (*a != '\0' && *b != '\0') {
    aa = *a;
    bb = *b;

    if (isupper ((guchar)aa))
      aa = tolower ((guchar)aa);
    if (isupper ((guchar)bb))
      bb = tolower ((guchar)bb);

    if (isspace ((guchar)aa))
      ++a;
    else if (isspace ((guchar)bb))
      ++b;
    else if (aa != bb)
      return FALSE;
    else {
      if (*a)
	++a;
      if (*b)
	++b;
    }
  }
  return TRUE;
}

GuppiMarker guppi_str2marker (const gchar * s)
{
  gint i = 0;
  const GuppiMarkerInfo *mi;

  g_return_val_if_fail (s != NULL, GUPPI_MARKER_UNKNOWN);

  while (i < (gint) GUPPI_MARKER_LAST) {
    mi = guppi_marker_info ((GuppiMarker) i);
    if (loose_match (mi->code, s) || loose_match (mi->name, s)) {
      return mi->marker;
    }
    ++i;
  }
  return GUPPI_MARKER_UNKNOWN;
}

GuppiAlphaTemplate *
guppi_marker_alpha_template (GuppiMarker m,
			     double sz1, double sz2, double scale_factor)
{
  GuppiMarkerInfoPrivate *mipriv;

  g_return_val_if_fail (guppi_marker_valid (m), NULL);
  g_return_val_if_fail (sz1 >= 0, NULL);
  g_return_val_if_fail (sz2 >= 0, NULL);
  g_return_val_if_fail (scale_factor >= 0, NULL);

  if (m == GUPPI_MARKER_NONE)
    return NULL;

  mipriv = (GuppiMarkerInfoPrivate *) guppi_marker_info (m);
  g_return_val_if_fail (mipriv != NULL, NULL);

  if (mipriv->make_alpha_template)
    return mipriv->make_alpha_template (sz1 * scale_factor,
					sz2 * scale_factor);

  g_warning ("No alpha template available for marker %s", mipriv->info.name);

  return NULL;
}

GuppiPixbuf *
guppi_marker_pixbuf (GuppiMarker m,
		     double sz1, double sz2,
		     double scale_factor)
{
  GuppiMarkerInfoPrivate *mipriv;

  g_return_val_if_fail (guppi_marker_valid (m), NULL);
  g_return_val_if_fail (sz1 >= 0, NULL);
  g_return_val_if_fail (sz2 >= 0, NULL);
  g_return_val_if_fail (scale_factor >= 0, NULL);

  if (m == GUPPI_MARKER_NONE)
    return NULL;

  mipriv = (GuppiMarkerInfoPrivate *) guppi_marker_info (m);
  g_return_val_if_fail (mipriv != NULL, NULL);

  if (mipriv->make_pixbuf) {

    return mipriv->make_pixbuf (sz1 * scale_factor, sz2 * scale_factor);

  } else if (mipriv->make_alpha_template) {

    GuppiAlphaTemplate *template;
    GuppiPixbuf *pixbuf;
    template = mipriv->make_alpha_template (sz1 * scale_factor, sz2 * scale_factor);
    pixbuf = guppi_pixbuf_new_from_alpha_template (template, 0xff, 0, 0);
    guppi_unref (template);
    return pixbuf;

  } 

  g_warning ("No alpha template available for marker %s", mipriv->info.name);
  return NULL;
}

void
guppi_marker_print (GuppiMarker m,
		    double sz1, double sz2,
		    GuppiElementPrint * ep, double x, double y)
{
  GuppiMarkerInfoPrivate *mipriv;

  g_return_if_fail (guppi_marker_valid (m));
  g_return_if_fail (sz1 >= 0);
  g_return_if_fail (sz2 >= 0);
  g_return_if_fail (ep != NULL);
  g_return_if_fail (GUPPI_IS_ELEMENT_PRINT (ep));

  if (m == GUPPI_MARKER_NONE)
    return;

  mipriv = (GuppiMarkerInfoPrivate *) guppi_marker_info (m);
  g_return_if_fail (mipriv != NULL);

  if (mipriv->print) {
    guppi_element_print_gsave (ep);
    mipriv->print (ep, x, y, sz1, sz2);
    guppi_element_print_grestore (ep);
  } else
    g_warning ("No print method available for marker %s", mipriv->info.name);
}






/* $Id$ */
