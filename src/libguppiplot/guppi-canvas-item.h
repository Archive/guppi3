/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-canvas-item.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_CANVAS_ITEM_H
#define _INC_GUPPI_CANVAS_ITEM_H

/* #include <gnome.h> */
#include  "guppi-data.h"
#include "guppi-element-view.h"
#include "guppi-plot-toolkit.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS 

typedef struct _GuppiCanvasItem GuppiCanvasItem;
typedef struct _GuppiCanvasItemClass GuppiCanvasItemClass;
typedef struct _GuppiCanvasItemPrivate GuppiCanvasItemPrivate;

typedef void (*GuppiCanvasItemFunc) (GuppiCanvasItem *, gpointer);
typedef gboolean (*GuppiCanvasItemTest) (GuppiCanvasItem *, gpointer);

/* We derive from GnomeCanvasGroup rather than GnomeCanvasItem to get around
   the lack of multiple inheritance.  A GuppiElementCanvasGroup needs to be
   both a GnomeCanvasGroup and a GuppiCanvasItem.  If we derived
   from GnomeCanvasItem here, there would be no way to do that w/o M.I.

   Of course, this doesn't really matter, since GnomeCanvasGroup is-a
   GnomeCanvasItem.
*/

struct _GuppiCanvasItem {
  GnomeCanvasGroup parent;
  GuppiCanvasItemPrivate *priv;
};

struct _GuppiCanvasItemClass {
  GnomeCanvasGroupClass parent_class;

  /* Signals */
  void (*changed_scale) (GuppiCanvasItem *, double scale);
  void (*changed_state) (GuppiCanvasItem *); /* state item emitted 'changed' */
  void (*changed_view)  (GuppiCanvasItem *); /* view item emitted 'changed' */

  /* VTable */
  void (*bbox_changed_hook) (GuppiCanvasItem *, gint cx0, gint cy0, gint cx1, gint cy1);
  void (*guppi_update) (GuppiCanvasItem *, double aff[6], ArtSVP *, gint);
  void (*guppi_render) (GuppiCanvasItem *, GnomeCanvasBuf *);

  void (*foreach) (GuppiCanvasItem *, GuppiCanvasItemFunc, gpointer);

  /* Return a NULL-terminated vector of applicable toolkits */
  void (*foreach_class_toolkit) (GuppiCanvasItem *,
				 void (*)(GuppiPlotToolkit *, gpointer),
				 gpointer);
  gboolean uses_vp_coordinates;

  /* Some fallback event handling stuff */
  /* Return TRUE if we have handled the event. */
  gboolean (*double_click) (GuppiCanvasItem *,
			    guint button, guint state,
			    double pt_x, double pt_y);

  gboolean (*data_drop) (GuppiCanvasItem *, GuppiData *);

  void (*post_creation_init) (GuppiCanvasItem *);
  void (*post_realization_init) (GuppiCanvasItem *);

  GuppiPlotToolkit *item_class_toolkit;
};

#define GUPPI_TYPE_CANVAS_ITEM (guppi_canvas_item_get_type())
#define GUPPI_CANVAS_ITEM(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_CANVAS_ITEM,GuppiCanvasItem))
#define GUPPI_CANVAS_ITEM0(obj) ((obj) ? (GUPPI_CANVAS_ITEM(obj)) : NULL)
#define GUPPI_CANVAS_ITEM_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_CANVAS_ITEM,GuppiCanvasItemClass))
#define GUPPI_IS_CANVAS_ITEM(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_CANVAS_ITEM))
#define GUPPI_IS_CANVAS_ITEM0(obj) (((obj) == NULL) || (GUPPI_IS_CANVAS_ITEM(obj)))
#define GUPPI_IS_CANVAS_ITEM_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_CANVAS_ITEM))

GtkType guppi_canvas_item_get_type (void);

GuppiElementState *guppi_canvas_item_state (GuppiCanvasItem *item);
GuppiElementView *guppi_canvas_item_view (GuppiCanvasItem *item);
guppi_uniq_t guppi_canvas_item_unique_id (GuppiCanvasItem *item);
double guppi_canvas_item_scale (GuppiCanvasItem *item);

GuppiGeometry *guppi_canvas_item_geometry (GuppiCanvasItem *item);
gboolean guppi_canvas_item_positioned (GuppiCanvasItem *item);

void guppi_canvas_item_set_view (GuppiCanvasItem *item, GuppiElementView *view);
void guppi_canvas_item_set_scale (GuppiCanvasItem *item, double scale);

/* Set bounding box in either canvas pixel or world coordinates. */

void guppi_canvas_item_set_bbox_c (GuppiCanvasItem *item,
				   gint cx0, gint cy0, gint cx1, gint cy1);

void guppi_canvas_item_get_bbox_c (GuppiCanvasItem *item,
				   gint *cx0, gint *cy0,
				   gint *cx1, gint *cy1);

void guppi_canvas_item_get_bbox_vp (GuppiCanvasItem *item,
				    double *x0, double *y0,
				    double *x1, double *y1);

void guppi_canvas_item_get_bbox_pt (GuppiCanvasItem *item,
				    double *x0, double *y0,
				    double *x1, double *y1);


/** Coordinate transforms galore **/

void guppi_canvas_item_conv (GuppiCanvasItem *item,
			     double c_x, double c_y,
			     double *conv_x, double *conv_y);

void guppi_canvas_item_unconv (GuppiCanvasItem *item,
			       double conv_x, double conv_y,
			       double *c_x, double *c_y);


/* Convert to and from canvas coordinates. */

void guppi_canvas_item_vp2c (GuppiCanvasItem *item,
			     double vp_x, double vp_y,
			     gint *c_x, gint *c_y);

void guppi_canvas_item_vp2c_d (GuppiCanvasItem *item,
			       double vp_x, double vp_y,
			       double *c_x, double *c_y);

void guppi_canvas_item_x_vp2c_d_bulk (GuppiCanvasItem *item,
				      const double *vp_x,
				      double *c_x, gsize N);

void guppi_canvas_item_y_vp2c_d_bulk (GuppiCanvasItem *item,
				      const double *vp_y,
				      double *c_y, gsize N);


void guppi_canvas_item_vp2c_d_bulk (GuppiCanvasItem *item,
				    const double *vp_x,
				    const double *vp_y,
				    double *c_x, double *c_y, gsize N);

void guppi_canvas_item_c2vp (GuppiCanvasItem *item,
			     double c_x, double c_y,
			     double *vp_x, double *vp_y);

void guppi_canvas_item_pt2c (GuppiCanvasItem *item,
			     double pt_x, double pt_y,
			     gint * c_x, gint * c_y);

void guppi_canvas_item_pt2c_d (GuppiCanvasItem *item,
			       double pt_x, double pt_y,
			       double *c_x, double *c_y);

void guppi_canvas_item_c2pt (GuppiCanvasItem *item,
			     double c_x, double c_y,
			     double *pt_x, double *pt_y);

void guppi_canvas_item_vpath_vp2c (GuppiCanvasItem *item, ArtVpath *path);
void guppi_canvas_item_vpath_pt2c (GuppiCanvasItem *item, ArtVpath *path);



/* Request redraw in canvas pixel or viewport coordinate systems. */

void guppi_canvas_item_request_redraw_c (GuppiCanvasItem *item,
					 gint cx0, gint cy0,
					 gint cx1, gint cy1);

void guppi_canvas_item_request_redraw_vp (GuppiCanvasItem *item,
					  double vx0, double vy0,
					  double vx1, double vy1);

/* Request redraw of entire canvas item. */
void guppi_canvas_item_request_total_redraw (GuppiCanvasItem *item);
void guppi_canvas_item_request_update (GuppiCanvasItem *item);


/* Layer operations. */
void guppi_canvas_item_foreach (GuppiCanvasItem *item,
				GuppiCanvasItemFunc func,
				gpointer user_data);

void guppi_canvas_item_foreach_at (GuppiCanvasItem *item,
				   double pt_x, double pt_y,
				   GuppiCanvasItemFunc func,
				   gpointer user_data);

GuppiCanvasItem *guppi_canvas_item_first_matching_at (GuppiCanvasItem *item,
						      double pt_x,
						      double pt_y,
						      GuppiCanvasItemTest test_func,
						      gpointer user_data);

/* Drag & Drop */

GuppiCanvasItem *guppi_canvas_item_data_drop_candidate (GuppiCanvasItem *item,
							double pt_x,
							double pt_y);

gboolean guppi_canvas_item_data_drop (GuppiCanvasItem *item, GuppiData *data);

/* Tool-related stuff. */

gboolean guppi_canvas_item_locate_button_tool (GuppiCanvasItem *item,
					       double pt_x, double pt_y,
					       guint button, guint state,
					       GuppiCanvasItem **,
					       GuppiPlotTool **);

gboolean guppi_canvas_item_locate_key_tool (GuppiCanvasItem *item,
					    double pt_x, double pt_y,
					    guint key_code, guint state,
					    GuppiCanvasItem **,
					    GuppiPlotTool **);

gboolean guppi_canvas_item_double_click (GuppiCanvasItem *item,
					 double pt_x, double pt_y,
					 guint button, guint state);

void guppi_canvas_item_set_local_toolkit (GuppiCanvasItem *item,
					  GuppiPlotToolkit *toolkit);
void guppi_canvas_item_enable_local_toolkit (GuppiCanvasItem *item, gboolean flag);
void guppi_canvas_item_enable_class_toolkit (GuppiCanvasItem *item, gboolean flag);

gboolean guppi_canvas_item_nonlocal_tools_disabled (GuppiCanvasItem *item);

GuppiPlotTool *guppi_canvas_item_local_button_tool (GuppiCanvasItem *item,
						    guint button,
						    guint state);
GuppiPlotTool *guppi_canvas_item_local_key_tool (GuppiCanvasItem *item,
						 guint key_code,
						 guint state);

/* Class-level Tool Information */

void guppi_canvas_item_foreach_class_toolkit (GuppiCanvasItem *item,
					      void (*callback)(GuppiPlotToolkit *,
							       gpointer),
					      gpointer user_data);


/* Some mysterious convenience functions */

void guppi_canvas_item_class_set_item_class_toolkit (GuppiCanvasItemClass *item,
						     GuppiPlotToolkit *toolkit);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_CANVAS_ITEM_H */

/* $Id$ */
