/* $Id$ */

/*
 * guppi-basic-tools.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-basic-tools.h"

#include <gdk/gdkkeysyms.h>

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>

#include <math.h>
#include <guppi-memory.h>
#include "guppi-canvas-item.h"


/* A general check for use of vp coordinates */
static gboolean
uses_vp_cb (GuppiPlotTool * tool, GuppiCanvasItem * item)
{
  return GUPPI_CANVAS_ITEM_CLASS (GTK_OBJECT (item)->klass)->
    uses_vp_coordinates;
}

/* Some generic tools */

static void
rescale_cb (GuppiPlotTool * tool, GuppiCanvasItem * item)
{
  GuppiElementView *view = guppi_canvas_item_view (item);
  GuppiViewInterval *iv;

  guppi_element_view_freeze (view);

  iv = guppi_element_view_axis_view_interval (view, GUPPI_X_AXIS);
  guppi_view_interval_rescale_around_point (iv, tool->x, tool->arg1);

  iv = guppi_element_view_axis_view_interval (view, GUPPI_Y_AXIS);
  guppi_view_interval_rescale_around_point (iv, tool->y, tool->arg2);

  guppi_element_view_thaw (view);

}

GuppiPlotTool *
guppi_basic_tool_new_rescale (double scale)
{
  GuppiPlotTool *tool;
  gchar *name;

  g_return_val_if_fail (scale > 0, NULL);

  tool = guppi_plot_tool_new ();
  tool->supported_type = GUPPI_TYPE_CANVAS_ITEM;
  if (scale >= 1)
    name = guppi_strdup_printf (_("Zoom Out %g%%"), 100 * (scale - 1));
  else
    name = guppi_strdup_printf (_("Zoom In %g%%"), 100 * (1 / scale - 1));

  guppi_plot_tool_set_name (tool, name);
  guppi_free (name);

  tool->cursor = gdk_cursor_new (GDK_CROSSHAIR);
  tool->tracks_motion = TRUE;
  tool->repeating = TRUE;
  tool->repeat_interval = 100;
  tool->arg1 = scale;
  tool->arg2 = scale;
  tool->supports = uses_vp_cb;
  tool->first = tool->repeat = rescale_cb;

  return tool;
}

static void
recenter_cb (GuppiPlotTool * tool, GuppiCanvasItem * item)
{
  GuppiElementView *view = guppi_canvas_item_view (item);
  GuppiViewInterval *iv;

  guppi_element_view_freeze (view);

  iv = guppi_element_view_axis_view_interval (view, GUPPI_X_AXIS);
  guppi_view_interval_recenter_around_point (iv, tool->x);

  iv = guppi_element_view_axis_view_interval (view, GUPPI_Y_AXIS);
  guppi_view_interval_recenter_around_point (iv, tool->y);

  guppi_element_view_thaw (view);
}

GuppiPlotTool *
guppi_basic_tool_new_recenter (void)
{
  GuppiPlotTool *tool;

  tool = guppi_plot_tool_new ();
  tool->supported_type = GUPPI_TYPE_CANVAS_ITEM;

  guppi_plot_tool_set_name (tool, _("Recenter View"));

  tool->tracks_motion = FALSE;
  tool->repeating = FALSE;
  tool->supports = uses_vp_cb;
  tool->first = recenter_cb;

  return tool;
}

static void
drag_cb (GuppiPlotTool * tool, GuppiCanvasItem * item)
{
  GuppiElementView *view = guppi_canvas_item_view (item);
  GuppiViewInterval *iv;
  double x0, x1, y0, y1;
  double vx0, vx1, vy0, vy1;

  /* We need to do it this way to avoid a jittery effect. */
  guppi_canvas_item_c2vp (item, tool->raw_prev_x, tool->raw_prev_y, &x0, &y0);

  guppi_canvas_item_c2vp (item, tool->raw_x, tool->raw_y, &x1, &y1);


  guppi_element_view_freeze (view);

  iv = guppi_element_view_axis_view_interval (view, GUPPI_X_AXIS);
  vx0 = guppi_view_interval_conv (iv, x0);
  vx1 = guppi_view_interval_conv (iv, x1);
  guppi_view_interval_conv_translate (iv, vx0 - vx1);

  iv = guppi_element_view_axis_view_interval (view, GUPPI_Y_AXIS);
  vy0 = guppi_view_interval_conv (iv, y0);
  vy1 = guppi_view_interval_conv (iv, y1);
  guppi_view_interval_conv_translate (iv, vy0 - vy1);

  guppi_element_view_thaw (view);
}

GuppiPlotTool *
guppi_basic_tool_new_drag (void)
{
  GuppiPlotTool *tool;

  tool = guppi_plot_tool_new ();
  tool->supported_type = GUPPI_TYPE_CANVAS_ITEM;

  guppi_plot_tool_set_name (tool, _("Drag View"));

  tool->cursor = gdk_cursor_new (GDK_FLEUR);
  tool->tracks_motion = TRUE;
  tool->supports = uses_vp_cb;
  tool->repeating = FALSE;
  tool->middle = drag_cb;

  return tool;
}

static void
reframe_cb (GuppiPlotTool * tool, GuppiCanvasItem * item)
{
  GuppiElementView *view = guppi_canvas_item_view (item);
  GuppiViewInterval *iv;

  if (tool->cue_type == GPTPC_FRAME_BY_CENTER) {
    double x = tool->start_x;
    double y = tool->start_y;
    double w = fabs (tool->x - x);
    double h = fabs (tool->y - y);

    tool->start_x = x - w;
    tool->start_y = y - h;
    tool->x = x + w;
    tool->y = y + h;
  }

  /*
     Reframe only if our selected rectangle is sufficiently large.
     (We do this to avoid zooming in really, really close due to a
     stray click.
   */
  if (fabs (tool->raw_start_x - tool->raw_x) > 3 &&
      fabs (tool->raw_start_y - tool->raw_y) > 3) {

    guppi_element_view_freeze (view);

    iv = guppi_element_view_axis_view_interval (view, GUPPI_X_AXIS);
    guppi_view_interval_set (iv,
			     MIN (tool->start_x, tool->x),
			     MAX (tool->start_x, tool->x));

    iv = guppi_element_view_axis_view_interval (view, GUPPI_Y_AXIS);
    guppi_view_interval_set (iv,
			     MIN (tool->start_y, tool->y),
			     MAX (tool->start_y, tool->y));

    guppi_element_view_thaw (view);
  }
}

GuppiPlotTool *
guppi_basic_tool_new_reframe (gboolean by_edges)
{
  GuppiPlotTool *tool;

  tool = guppi_plot_tool_new ();
  tool->supported_type = GUPPI_TYPE_CANVAS_ITEM;


  guppi_plot_tool_set_name (tool,
			    by_edges ?
			    _("Reframe (Select Two Corners)") :
			    _("Reframe (Select Center and Corner)"));

  tool->cursor = gdk_cursor_new (GDK_SIZING);
  tool->tracks_motion = FALSE;
  tool->cue_type = by_edges ? GPTPC_FRAME : GPTPC_FRAME_BY_CENTER;
  tool->repeating = FALSE;
  tool->supports = uses_vp_cb;
  tool->last = reframe_cb;

  return tool;
}

static void
preferred_view_cb (GuppiPlotTool * tool, GuppiCanvasItem * item)
{
  GuppiElementView *view = guppi_canvas_item_view (item);

  guppi_element_view_set_preferred_view_all (view);
}

GuppiPlotTool *
guppi_basic_tool_new_preferred_view (void)
{
  GuppiPlotTool *tool;

  tool = guppi_plot_tool_new ();
  tool->supported_type = GUPPI_TYPE_CANVAS_ITEM;

  guppi_plot_tool_set_name (tool, _("Preferred View"));

  tool->tracks_motion = FALSE;
  tool->repeating = FALSE;
  tool->supports = uses_vp_cb;
  tool->first = preferred_view_cb;

  return tool;
}

static void
translate_cb (GuppiPlotTool * cb, GuppiCanvasItem * item)
{
  GuppiElementView *view = guppi_canvas_item_view (item);
  GuppiViewInterval *vi;
  double delta_x, delta_y;

  delta_x = cb->arg1;
  delta_y = cb->arg2;

  if (delta_x) {
    vi = guppi_element_view_axis_view_interval (view, GUPPI_X_AXIS);
    guppi_view_interval_conv_translate (vi, delta_x);
  }

  if (delta_y) {
    vi = guppi_element_view_axis_view_interval (view, GUPPI_Y_AXIS);
    guppi_view_interval_conv_translate (vi, delta_y);
  }
}

GuppiPlotTool *
guppi_basic_tool_new_translate (double x, double y)
{
  GuppiPlotTool *tool;
  gchar *name_x = NULL;
  gchar *name_y = NULL;
  gchar *name;

  tool = guppi_plot_tool_new ();
  tool->supported_type = GUPPI_TYPE_CANVAS_ITEM;

  if (x > 0)
    name_x = guppi_strdup_printf (_("Left %g%%"), 100 * x);
  else if (x < 0)
    name_x = guppi_strdup_printf (_("Right %g%%"), 100 * (-x));

  if (y > 0)
    name_y = guppi_strdup_printf (_("Down %g%%"), 100 * y);
  else if (y < 0)
    name_y = guppi_strdup_printf (_("Up %g%%"), 100 * (-y));

  if (name_x && name_y) {
    name = guppi_strdup_printf (_("Move %s, %s"), name_x, name_y);
  } else if (name_x || name_y) {
    name = guppi_strdup_printf (_("Move %s"), name_x ? name_x : name_y);
  } else {
    name = guppi_strdup (_("<null move>"));
  }

  guppi_plot_tool_set_name (tool, name);

  guppi_free (name_x);
  guppi_free (name_y);
  guppi_free (name);

  tool->tracks_motion = FALSE;
  tool->repeating = FALSE;
  tool->supports = uses_vp_cb;
  tool->first = translate_cb;

  tool->arg1 = x;
  tool->arg2 = y;

  return tool;
}

/***************************************************************************/

#define MV_SMALL 0.1
#define MV_MED   0.2
#define MV_LARGE 0.5

void
guppi_add_keyboard_navigation_to_toolkit (GuppiPlotToolkit * tk)
{
  guppi_plot_toolkit_set_key_tool (tk, GDK_Left, 0,
				   guppi_basic_tool_new_translate (MV_MED,
								   0));

  guppi_plot_toolkit_set_key_tool (tk, GDK_Left, GDK_SHIFT_MASK,
				   guppi_basic_tool_new_translate (MV_LARGE,
								   0));


  guppi_plot_toolkit_set_key_tool (tk, GDK_Right, 0,
				   guppi_basic_tool_new_translate (-MV_MED,
								   0));

  guppi_plot_toolkit_set_key_tool (tk, GDK_Right, GDK_SHIFT_MASK,
				   guppi_basic_tool_new_translate (-MV_LARGE,
								   0));


  guppi_plot_toolkit_set_key_tool (tk, GDK_Up, 0,
				   guppi_basic_tool_new_translate (0,
								   -MV_MED));

  guppi_plot_toolkit_set_key_tool (tk, GDK_Up, GDK_SHIFT_MASK,
				   guppi_basic_tool_new_translate (0,
								   -MV_LARGE));


  guppi_plot_toolkit_set_key_tool (tk, GDK_Down, 0,
				   guppi_basic_tool_new_translate (0,
								   MV_MED));

  guppi_plot_toolkit_set_key_tool (tk, GDK_Down, GDK_SHIFT_MASK,
				   guppi_basic_tool_new_translate (0,
								   MV_LARGE));


  guppi_plot_toolkit_set_key_tool (tk, GDK_Home, 0,
				   guppi_basic_tool_new_preferred_view ());


  guppi_plot_toolkit_set_key_tool (tk, GDK_plus, GDK_SHIFT_MASK,
				   guppi_basic_tool_new_rescale (1 / 1.1));

  guppi_plot_toolkit_set_key_tool (tk, GDK_minus, 0,
				   guppi_basic_tool_new_rescale (1.1));

  guppi_plot_toolkit_set_key_tool (tk, GDK_space, 0,
				   guppi_basic_tool_new_recenter ());

}

/***************************************************************************/

GuppiPlotToolkit *
guppi_basic_toolkit_zoom (void)
{

  GuppiPlotToolkit *tk = guppi_plot_toolkit_new (_("Zoom"));
  tk->toolbar_button_image = "basic-zoom.png";

  guppi_plot_toolkit_set_button_tool (tk, 1, 0,
				      guppi_basic_tool_new_rescale (1 / 1.1));


  guppi_plot_toolkit_set_button_tool (tk, 3, 0,
				      guppi_basic_tool_new_rescale (1.1));


  guppi_plot_toolkit_set_button_tool (tk, 1, GDK_SHIFT_MASK,
				      guppi_basic_tool_new_rescale (1 /
								    1.03));


  guppi_plot_toolkit_set_button_tool (tk, 3, GDK_SHIFT_MASK,
				      guppi_basic_tool_new_rescale (1.03));


  guppi_plot_toolkit_set_button_tool (tk, 2, GDK_SHIFT_MASK,
				      guppi_basic_tool_new_preferred_view ());

  guppi_add_keyboard_navigation_to_toolkit (tk);

  return tk;
}

GuppiPlotToolkit *
guppi_basic_toolkit_move (void)
{
  GuppiPlotToolkit *tk = guppi_plot_toolkit_new (_("Move"));
  tk->toolbar_button_image = "basic-move.png";

  guppi_plot_toolkit_set_button_tool (tk, 1, 0, guppi_basic_tool_new_drag ());

  guppi_plot_toolkit_set_button_tool (tk, 1, GDK_SHIFT_MASK,
				      guppi_basic_tool_new_recenter ());

  guppi_plot_toolkit_set_button_tool (tk, 2, GDK_SHIFT_MASK,
				      guppi_basic_tool_new_preferred_view ());

  guppi_add_keyboard_navigation_to_toolkit (tk);

  return tk;
}

GuppiPlotToolkit *
guppi_basic_toolkit_reframe (void)
{
  GuppiPlotToolkit *tk = guppi_plot_toolkit_new (_("Reframe"));
  tk->toolbar_button_image = "basic-reframe.png";

  guppi_plot_toolkit_set_button_tool (tk, 1, 0,
				      guppi_basic_tool_new_reframe (TRUE));

  guppi_plot_toolkit_set_button_tool (tk, 1, GDK_SHIFT_MASK,
				      guppi_basic_tool_new_reframe (FALSE));

  guppi_plot_toolkit_set_button_tool (tk, 2, GDK_SHIFT_MASK,
				      guppi_basic_tool_new_preferred_view ());

  guppi_add_keyboard_navigation_to_toolkit (tk);

  return tk;
}

GuppiPlotToolkit *
guppi_basic_toolkit_home (void)
{
  GuppiPlotToolkit *tk = guppi_plot_toolkit_new (_("Home"));
  tk->toolbar_button_image = "basic-home.png";

  guppi_plot_toolkit_set_button_tool (tk, 1, 0,
				      guppi_basic_tool_new_preferred_view ());

  guppi_add_keyboard_navigation_to_toolkit (tk);

  return tk;
}


/* $Id$ */
