/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-plot-toolkit.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-plot-toolkit.h"

#include <gtk/gtktogglebutton.h>

#include <libgnomeui/gnome-pixmap.h>

#include <guppi-useful.h>


static GtkObjectClass *parent_class = NULL;

static void
hash_remove (gpointer key, gpointer val, gpointer data)
{
  guppi_unref (val);
}

static void
guppi_plot_toolkit_finalize (GtkObject * obj)
{
  GuppiPlotToolkit *tk = GUPPI_PLOT_TOOLKIT (obj);
  gint i;

  guppi_finalized (obj);

  guppi_free (tk->name);
  tk->name = NULL;

  for (i = 0; i < GUPPI_PLOT_TOOLKIT_BUTTON_MAX; ++i) {
    guppi_unref (tk->button_tool[i]);
    guppi_unref (tk->shift_button_tool[i]);
    guppi_unref (tk->ctrl_button_tool[i]);
    guppi_unref (tk->ctrl_shift_button_tool[i]);
  }

  if (tk->key_tools) {
    g_hash_table_foreach (tk->key_tools, hash_remove, NULL);
    g_hash_table_destroy (tk->key_tools);
  }

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_plot_toolkit_class_init (GuppiPlotToolkitClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;

  parent_class = gtk_type_class (GTK_TYPE_OBJECT);

  object_class->finalize = guppi_plot_toolkit_finalize;
}

static void
guppi_plot_toolkit_init (GuppiPlotToolkit * obj)
{

}

GtkType guppi_plot_toolkit_get_type (void)
{
  static GtkType guppi_plot_toolkit_type = 0;
  if (!guppi_plot_toolkit_type) {
    static const GtkTypeInfo guppi_plot_toolkit_info = {
      "GuppiPlotToolkit",
      sizeof (GuppiPlotToolkit),
      sizeof (GuppiPlotToolkitClass),
      (GtkClassInitFunc) guppi_plot_toolkit_class_init,
      (GtkObjectInitFunc) guppi_plot_toolkit_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_plot_toolkit_type =
      gtk_type_unique (GTK_TYPE_OBJECT, &guppi_plot_toolkit_info);
  }
  return guppi_plot_toolkit_type;
}

GuppiPlotToolkit *
guppi_plot_toolkit_new (const gchar * name)
{
  GuppiPlotToolkit *tk =
    GUPPI_PLOT_TOOLKIT (guppi_type_new (guppi_plot_toolkit_get_type ()));

  tk->name = guppi_strdup (name);

  return tk;
}

const gchar *
guppi_plot_toolkit_name (GuppiPlotToolkit * tk)
{
  g_return_val_if_fail (tk != NULL, NULL);
  g_return_val_if_fail (GUPPI_IS_PLOT_TOOLKIT (tk), NULL);

  return tk->name;
}

GuppiPlotTool *
guppi_plot_toolkit_get_button_tool (GuppiPlotToolkit * tk,
				    guint button, guint state)
{
  GuppiPlotTool **tools = NULL;

  g_return_val_if_fail (tk != NULL, NULL);
  g_return_val_if_fail (GUPPI_IS_PLOT_TOOLKIT (tk), NULL);
  g_return_val_if_fail (0 < button &&
			button <= GUPPI_PLOT_TOOLKIT_BUTTON_MAX, NULL);

  if ((state & GDK_SHIFT_MASK) && (state & GDK_CONTROL_MASK))
    tools = tk->ctrl_shift_button_tool;
  else if (state & GDK_SHIFT_MASK)
    tools = tk->shift_button_tool;
  else if (state & GDK_CONTROL_MASK)
    tools = tk->ctrl_button_tool;
  else
    tools = tk->button_tool;

  g_assert (tools != NULL);

  return tools[button - 1];
}

void
guppi_plot_toolkit_set_button_tool (GuppiPlotToolkit * tk,
				    guint button, guint state,
				    GuppiPlotTool * tool)
{
  GuppiPlotTool **tools = NULL;

  g_return_if_fail (tk != NULL);
  g_return_if_fail (GUPPI_IS_PLOT_TOOLKIT (tk));
  g_return_if_fail (0 < button && button <= GUPPI_PLOT_TOOLKIT_BUTTON_MAX);
  g_return_if_fail (tool != NULL);
  g_return_if_fail (GUPPI_IS_PLOT_TOOL (tool));

  if ((state & GDK_SHIFT_MASK) && (state & GDK_CONTROL_MASK))
    tools = tk->ctrl_shift_button_tool;
  else if (state & GDK_SHIFT_MASK)
    tools = tk->shift_button_tool;
  else if (state & GDK_CONTROL_MASK)
    tools = tk->ctrl_button_tool;
  else
    tools = tk->button_tool;

  g_assert (tools != NULL);

  guppi_unref (tools[button - 1]);

  tool->button = button;

  guppi_ref (tool);
  guppi_sink (tool);
  tools[button - 1] = tool;
}

static guint
massage_keycode (guint key, guint state)
{
  guint m = key << 2;
  if (state & GDK_SHIFT_MASK)
    m |= 1;
  if (state & GDK_CONTROL_MASK)
    m |= 2;
  return m;
}

GuppiPlotTool *
guppi_plot_toolkit_get_key_tool (GuppiPlotToolkit * tk,
				 guint key, guint state)
{
  guint mkey;
  gpointer ptr;

  g_return_val_if_fail (tk != NULL && GUPPI_IS_PLOT_TOOLKIT (tk), NULL);

  if (tk->key_tools == NULL)
    return NULL;

  mkey = massage_keycode (key, state);
  ptr = g_hash_table_lookup (tk->key_tools, GUINT_TO_POINTER (mkey));

  return ptr ? GUPPI_PLOT_TOOL (ptr) : NULL;
}

void
guppi_plot_toolkit_set_key_tool (GuppiPlotToolkit * tk,
				 guint key, guint state, GuppiPlotTool * tool)
{
  guint mkey;
  gpointer old_tool;

  g_return_if_fail (tk != NULL && GUPPI_IS_PLOT_TOOLKIT (tk));
  g_return_if_fail (tool != NULL && GUPPI_IS_PLOT_TOOL (tool));

  if (tk->key_tools == NULL)
    tk->key_tools = g_hash_table_new (NULL, NULL);

  tool->button = 0;

  mkey = massage_keycode (key, state);
  old_tool = g_hash_table_lookup (tk->key_tools, GUINT_TO_POINTER (mkey));

  if (old_tool != (gpointer) tool) {

    g_hash_table_remove (tk->key_tools, GUINT_TO_POINTER (mkey));
    guppi_unref (old_tool);

    g_hash_table_insert (tk->key_tools, GUINT_TO_POINTER (mkey), tool);
    guppi_ref (tool);
    guppi_sink (tool);
  }
}

static void
key_iter (gpointer key, gpointer value, gpointer user_data)
{
  guint key_code = GPOINTER_TO_UINT (key);
  GuppiPlotTool *tool = GUPPI_PLOT_TOOL (value);
  GList **key_list = (GList **) user_data;
  GuppiPlotKeystroke *ks = guppi_new0 (GuppiPlotKeystroke, 1);

  ks->key_code = key_code >> 2;
  ks->state = key_code & 3;
  ks->tool = tool;

  *key_list = g_list_append (*key_list, ks);
}

static gint
key_sort (gconstpointer aa, gconstpointer bb)
{
  const GuppiPlotKeystroke *a = (const GuppiPlotKeystroke *) aa;
  const GuppiPlotKeystroke *b = (const GuppiPlotKeystroke *) bb;
  gint p, s;

  p = (a->key_code > b->key_code) - (b->key_code > a->key_code);
  s = (a->state > b->state) - (b->state > a->state);

  return p ? p : s;
}

GList *
guppi_plot_toolkit_get_all_keys (GuppiPlotToolkit * tk)
{
  GList *key_list = NULL;

  g_return_val_if_fail (tk != NULL && GUPPI_IS_PLOT_TOOLKIT (tk), NULL);

  if (tk->key_tools == NULL)
    return NULL;

  g_hash_table_foreach (tk->key_tools, key_iter, &key_list);

  key_list = g_list_sort (key_list, key_sort);

  return key_list;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

struct alltooldata {
  void (*fn) (GuppiPlotTool *, gpointer);
  gpointer user_data;
};

static void
alltool_iter (gpointer key, gpointer val, gpointer user_data)
{
  struct alltooldata *x = (struct alltooldata *)user_data;
  GuppiPlotTool *tool = GUPPI_PLOT_TOOL (val);

  x->fn (tool, x->user_data);
}

void
guppi_plot_toolkit_foreach (GuppiPlotToolkit *tk,
			    void (*fn) (GuppiPlotTool *, gpointer),
			    gpointer user_data)
{
  struct alltooldata x;
  gint i;

  g_return_if_fail (tk && GUPPI_IS_PLOT_TOOLKIT (tk));
  g_return_if_fail (fn);

  /* Do all buttons */
  for (i=0; i<GUPPI_PLOT_TOOLKIT_BUTTON_MAX; ++i) {
    if (tk->button_tool[i])
      fn (tk->button_tool[i], user_data);
    if (tk->shift_button_tool[i])
      fn (tk->shift_button_tool[i], user_data);
    if (tk->ctrl_button_tool[i])
      fn (tk->ctrl_button_tool[i], user_data);
    if (tk->ctrl_shift_button_tool[i])
      fn (tk->ctrl_shift_button_tool[i], user_data);
  }

  /* Do all keys */
  if (tk->key_tools) {
    x.fn = fn;
    x.user_data = user_data;
    g_hash_table_foreach (tk->key_tools, alltool_iter, &x);
  }
  
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
sig_iter_fn (gpointer key, gpointer val, gpointer user_data)
{
  guint *seed = (guint *) user_data;
  GuppiPlotTool *tool = GUPPI_PLOT_TOOL (val);

  *seed = 13 * (*seed) + guppi_plot_tool_signature (tool);
}

guint guppi_plot_toolkit_signature (GuppiPlotToolkit * tk)
{
  guint seed = 0xd0d00d1e;
  gint i;

  g_return_val_if_fail (tk && GUPPI_IS_PLOT_TOOLKIT (tk), 0);

  for (i = 0; i < GUPPI_PLOT_TOOLKIT_BUTTON_MAX; ++i) {
    if (tk->button_tool[i])
      seed = 3 * seed + guppi_plot_tool_signature (tk->button_tool[i]);
    if (tk->shift_button_tool[i])
      seed = 5 * seed + guppi_plot_tool_signature (tk->shift_button_tool[i]);
    if (tk->ctrl_button_tool[i])
      seed = 7 * seed + guppi_plot_tool_signature (tk->ctrl_button_tool[i]);
    if (tk->ctrl_shift_button_tool[i])
      seed =
	11 * seed + guppi_plot_tool_signature (tk->ctrl_shift_button_tool[i]);
  }

  if (tk->key_tools)
    g_hash_table_foreach (tk->key_tools, sig_iter_fn, &seed);

  return seed;
}

/*************************************************************************/

struct tb_info {

  GtkToggleButton *button;
  GtkWidget *gadget;

  GuppiPlotToolkit *toolkit;
  GtkToolbar *toolbar;

  void (*select_cb) (GuppiPlotToolkit *, gpointer);
  void (*unselect_cb) (GuppiPlotToolkit *, gpointer);
  gpointer user_data;

};

static void
free_tb_info (gpointer ptr)
{
  guppi_free (ptr);
}

static void
toolbar_button_toggled_cb (GtkToggleButton * b, struct tb_info *info)
{
  struct tb_info *current;

  g_return_if_fail (b != NULL);
  g_return_if_fail (GTK_IS_TOGGLE_BUTTON (b));
  g_return_if_fail (info != NULL);


  current =
    (struct tb_info *) gtk_object_get_data (GTK_OBJECT (info->toolbar),
					    "current_toolkit");

  if (current == info) {
    gtk_toggle_button_set_active (b, TRUE);
    return;
  }

  if (!gtk_toggle_button_get_active (b))
    return;

  gtk_object_set_data (GTK_OBJECT (info->toolbar), "current_toolkit", info);

  if (info->select_cb)
    info->select_cb (info->toolkit, info->user_data);

  if (info->gadget)
    gtk_widget_set_sensitive (info->gadget, TRUE);


  if (current) {

    gtk_toggle_button_set_active (current->button, FALSE);

    if (current->unselect_cb)
      current->unselect_cb (current->toolkit, current->user_data);

    if (current->gadget)
      gtk_widget_set_sensitive (current->gadget, FALSE);

  }
}

void
guppi_plot_toolkit_add_to_toolbar (GuppiPlotToolkit * tk,
				   GtkToolbar * tb,
				   void (*select_cb) (GuppiPlotToolkit *,
						      gpointer),
				   void (*unselect_cb) (GuppiPlotToolkit *,
							gpointer),
				   gpointer user_data)
{
  struct tb_info *info;
  GtkWidget *button;
  GtkWidget *button_label = NULL;
  GtkWidget *gadget = NULL;
  gchar *path;

  g_return_if_fail (tk != NULL);
  g_return_if_fail (GUPPI_IS_PLOT_TOOLKIT (tk));
  g_return_if_fail (tb != NULL);
  g_return_if_fail (GTK_IS_TOOLBAR (tb));

  info = guppi_new0 (struct tb_info, 1);


  /* Build up the mode button for the toolbar */

  button = gtk_toggle_button_new ();

  if (tk->toolbar_button_image &&
      (path = guppi_find_pixmap (tk->toolbar_button_image))) {

    button_label = gnome_pixmap_new_from_file (path);
    guppi_free (path);

  } else if (tk->build_toolbar_button) {

    button_label = tk->build_toolbar_button (tk);

  }

  if (button_label == NULL)
    button_label = gtk_label_new (guppi_plot_toolkit_name (tk));

  gtk_widget_show (button_label);
  gtk_container_add (GTK_CONTAINER (button), button_label);
  gtk_widget_show (GTK_WIDGET (button));

  gtk_signal_connect (GTK_OBJECT (button),
		      "clicked",
		      GTK_SIGNAL_FUNC (toolbar_button_toggled_cb), info);


  /* Try to build the associated "gadget" widget */

  if (tk->build_toolbar_gadget) {
    gadget = tk->build_toolbar_gadget (tk);
    if (gadget)
      gtk_widget_show (gadget);
  }

  /* Populate our info structure and attach it to our button */

  info->button = GTK_TOGGLE_BUTTON (button);
  info->gadget = gadget;
  info->toolkit = tk;
  info->toolbar = tb;
  info->select_cb = select_cb;
  info->unselect_cb = unselect_cb;
  info->user_data = user_data;
  gtk_object_set_data_full (GTK_OBJECT (button), "tb_info", info,
			    free_tb_info);

  /* Add our widgets to the toolbar */

  gtk_toolbar_append_widget (tb, button, guppi_plot_toolkit_name (tk), NULL);
  if (gadget)
    gtk_toolbar_append_widget (tb, gadget, NULL, NULL);


  /* If this is the first toolkit added, activate it */

  if (gtk_object_get_data (GTK_OBJECT (tb), "current_toolkit") == NULL)
    gtk_toggle_button_set_active (info->button, TRUE);
}

/* $Id$ */
