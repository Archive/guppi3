/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-geometry.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-geometry.h"

#include <string.h>

#include <gtk/gtksignal.h>
#include <gnome-xml/xmlmemory.h>
#include <gnan.h>
#include <guppi-memory.h>
#include <guppi-debug.h>
#include <guppi-convenient.h>
#include <guppi-unique-id.h>

#define EPSILON 1e-8

typedef struct _GuppiGeometryPrivate GuppiGeometryPrivate;
struct _GuppiGeometryPrivate {

  gchar *debug_label;
  guppi_uniq_t uid;
  
  gboolean positioned;
  double left, right, top, bottom;

  double natural_width, natural_height;
};

static GtkObjectClass *parent_class = NULL;

enum {
  CHANGED_POSITION,
  CHANGED_SIZE,
  LAST_SIGNAL
};

static guint gg_signals[LAST_SIGNAL] = { 0 };

static void
guppi_geometry_finalize (GtkObject * obj)
{
  GuppiGeometry *gg = GUPPI_GEOMETRY (obj);

  guppi_finalized (obj);

#if 0
  guppi_geometry_disconnect_from_layout (gg);
#endif

  guppi_free0 (gg->priv);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_geometry_class_init (GuppiGeometryClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;

  parent_class = gtk_type_class (GTK_TYPE_OBJECT);

  object_class->finalize = guppi_geometry_finalize;

  gg_signals[CHANGED_POSITION] =
    gtk_signal_new ("changed_position",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GuppiGeometryClass, changed_position),
		    gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);

  gg_signals[CHANGED_SIZE] =
    gtk_signal_new ("changed_size",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GuppiGeometryClass, changed_size),
		    gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);

  gtk_object_class_add_signals (object_class, gg_signals, LAST_SIGNAL);

}

static void
guppi_geometry_init (GuppiGeometry * obj)
{
  obj->priv = guppi_new0 (GuppiGeometryPrivate, 1);

  obj->priv->uid = guppi_unique_id ();

  obj->priv->natural_width  = -1;
  obj->priv->natural_height = -1;
}

GtkType
guppi_geometry_get_type (void)
{
  static GtkType guppi_geometry_type = 0;
  if (!guppi_geometry_type) {
    static const GtkTypeInfo guppi_geometry_info = {
      "GuppiGeometry",
      sizeof (GuppiGeometry),
      sizeof (GuppiGeometryClass),
      (GtkClassInitFunc) guppi_geometry_class_init,
      (GtkObjectInitFunc) guppi_geometry_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_geometry_type =
      gtk_type_unique (GTK_TYPE_OBJECT, &guppi_geometry_info);
  }
  return guppi_geometry_type;
}

/**
 * guppi_geometry_new
 *
 * Constructs a new, unpositioned, #GuppiGeometry object.
 * Returns: The newly-allocated object.
 */
GuppiGeometry *
guppi_geometry_new (void)
{
  return GUPPI_GEOMETRY (guppi_type_new (guppi_geometry_get_type ()));
}

const gchar *
guppi_geometry_get_debug_label (GuppiGeometry *gg)
{
  g_return_val_if_fail (GUPPI_IS_GEOMETRY (gg), NULL);
  return gg->priv->debug_label;
}

void
guppi_geometry_set_debug_label (GuppiGeometry *gg, const gchar *str)
{
  g_return_if_fail (GUPPI_IS_GEOMETRY (gg));

  if (gg->priv->debug_label != str) {
    guppi_free (gg->priv->debug_label);
    gg->priv->debug_label = guppi_strdup (str);
  }
}

guppi_uniq_t
guppi_geometry_get_unique_id (GuppiGeometry *gg)
{
  g_return_val_if_fail (gg && GUPPI_IS_GEOMETRY (gg), 0);
  return gg->priv->uid;
}

void
guppi_geometry_unposition (GuppiGeometry *gg)
{
  g_return_if_fail (GUPPI_IS_GEOMETRY (gg));
  gg->priv->positioned = FALSE;
}

/**
 * guppi_geometry_positioned
 * @gg: A geometry object
 *
 * We say that a #GuppiGeometry is "positioned" if it has been
 * initialized by specifying the bounding corners of the associated
 * rectangle have been specified.  If guppi_geometry_positioned()
 * returns %FALSE, the object is still in an uninitialized state.
 *
 * Returns: %TRUE if the object has been initialized, false otherwise.
 */
gboolean
guppi_geometry_positioned (GuppiGeometry *gg)
{
  g_return_val_if_fail (gg != NULL && GUPPI_IS_GEOMETRY (gg), FALSE);
  return gg->priv->positioned;
}

/**
 * guppi_geometry_left
 * @gg: A geometry object
 *
 * Returns: The x-coordinate of the left edge of @gg.
 */
double
guppi_geometry_left (GuppiGeometry *gg)
{
  g_return_val_if_fail (gg != NULL && GUPPI_IS_GEOMETRY (gg), 0);
  return gg->priv->left;
}

/**
 * guppi_geometry_right
 * @gg: A geometry object
 *
 * Returns: The x-coordinate of the right edge of @gg.
 */
double
guppi_geometry_right (GuppiGeometry *gg)
{
  g_return_val_if_fail (gg != NULL && GUPPI_IS_GEOMETRY (gg), 0);
  return gg->priv->right;
}

/**
 * guppi_geometry_top
 * @gg: A geometry object
 *
 * Returns: The y-coordinate of the top edge of @gg.
 */
double
guppi_geometry_top (GuppiGeometry *gg)
{
  g_return_val_if_fail (gg != NULL && GUPPI_IS_GEOMETRY (gg), 0);
  return gg->priv->top;
}

/**
 * guppi_geometry_bottom
 * @gg: A geometry object
 *
 * Returns: The y-coordinate of the bottom edge of @gg.
 */
double
guppi_geometry_bottom (GuppiGeometry *gg)
{
  g_return_val_if_fail (gg != NULL && GUPPI_IS_GEOMETRY (gg), 0);
  return gg->priv->bottom;
}

/**
 * guppi_geometry_width
 * @gg: A geometry object
 *
 * Returns: The width of @gg.
 */
double
guppi_geometry_width (GuppiGeometry *gg)
{
  g_return_val_if_fail (gg != NULL && GUPPI_IS_GEOMETRY (gg), 0);
  return gg->priv->right - gg->priv->left;
}

/**
 * guppi_geometry_height
 * @gg: A geometry object
 *
 * Returns: The height of @gg.
 */
double
guppi_geometry_height (GuppiGeometry *gg)
{
  g_return_val_if_fail (gg != NULL && GUPPI_IS_GEOMETRY (gg), 0);
  return gg->priv->top - gg->priv->bottom;
}

gboolean
guppi_geometry_get_bbox (GuppiGeometry *gg,
			 double *x0, double *y0,
			 double *x1, double *y1)
{
  g_return_val_if_fail (gg && GUPPI_IS_GEOMETRY (gg), FALSE);

  if (! guppi_geometry_positioned (gg))
    return FALSE;

  if (x0)
    *x0 = guppi_geometry_left (gg);
  if (y0)
    *y0 = guppi_geometry_bottom (gg);
  if (x1)
    *x1 = guppi_geometry_right (gg);
  if (y1)
    *y1 = guppi_geometry_top (gg);

  return TRUE;
}

/**
 * guppi_geometry_conv
 * @gg: A geometry object
 * @x: An x-value
 * @y: A y-value
 * @t_x: Where to put the x-value after converting to unit coordinates.
 * @t_y: Where to put the y-value after converting to unit coordinates.
 *
 * Maps an (x,y) pair to unit coordinates (In other words, onto a square
 * with corners at (0,0) and (1,1).)
 */ 
void
guppi_geometry_conv (GuppiGeometry *gg,
		     double x, double y, double *t_x, double *t_y)
{
  GuppiGeometryPrivate *p;
  g_return_if_fail (gg != NULL && GUPPI_IS_GEOMETRY (gg));
  p = gg->priv;

  if (t_x) {
    *t_x = (x - p->left) / (p->right - p->left);
  }

  if (t_y) {
    *t_y = (y - p->bottom) / (p->top - p->bottom);
  }
}

/**
 * guppi_geometry_unconv
 * @gg: A geometry object
 * @x: An x-value, 
 * @y: A y-value
 * @t_x: Where to put the x-value after converting to unit coordinates.
 * @t_y: Where to put the y-value after converting to unit coordinates.
 *
 * Maps an (x,y) pair to unit coordinates (In other words, onto a square
 * with corners at (0,0) and (1,1).)
 */ 
void
guppi_geometry_unconv (GuppiGeometry *gg,
		       double t_x, double t_y, double *x, double *y)
{
  GuppiGeometryPrivate *p;
  g_return_if_fail (gg != NULL && GUPPI_IS_GEOMETRY (gg));
  p = gg->priv;

  if (x)
    *x = p->left + t_x * (p->right - p->left);

  if (y)
    *y = p->bottom + t_y * (p->top - p->bottom);
}

/**
 * guppi_geometry_set_position
 * @gg: A geometry object
 * @left: The leftmost x-value.
 * @right: The rightmost x-value.
 * @top: The top y-value.
 * @bottom: The bottom y-value.
 *
 * Attaches the specified position to @gg.
 */
void
guppi_geometry_set_position (GuppiGeometry *gg,
			     double l, double r, double t, double b)
{
  GuppiGeometryPrivate *p;
  g_return_if_fail (gg != NULL && GUPPI_IS_GEOMETRY (gg));
  p = gg->priv;

  guppi_2sort (&l, &r);
  guppi_2sort (&b, &t);

  if (!p->positioned ||
      p->left != l || p->right != r || p->top != t || p->bottom != b) {
    p->positioned = TRUE;
    p->left = l;
    p->right = r;
    p->top = t;
    p->bottom = b;
    gtk_signal_emit (GTK_OBJECT (gg), gg_signals[CHANGED_POSITION]);
  }
}

/**
 * guppi_geometry_contains
 * @gg: A geometry object
 * @x: An x-coordinate
 * @y: A y-coordinate
 *
 * Returns: Whether or not the point (x,y) is contained inside of the rectangle specified by @gg.
 * If @gg is not positioned, always returns %FALSE.
 */
gboolean
guppi_geometry_contains (GuppiGeometry *gg, double x, double y)
{
  GuppiGeometryPrivate *p;
  g_return_val_if_fail (gg != NULL && GUPPI_IS_GEOMETRY (gg), FALSE);
  p = gg->priv;

  return p->positioned && guppi_between (p->left, x, p->right) &&
    guppi_between (p->bottom, y, p->top);
}

void
guppi_geometry_set_natural_width (GuppiGeometry *gg, double width)
{
  g_return_if_fail (gg && GUPPI_IS_GEOMETRY (gg));
  g_return_if_fail (width >= 0);

  if (gg->priv->natural_width != width) {
    gg->priv->natural_width = width;
    gtk_signal_emit (GTK_OBJECT (gg), gg_signals[CHANGED_SIZE]);
  }
}

void
guppi_geometry_set_natural_height (GuppiGeometry *gg, double height)
{
  g_return_if_fail (gg && GUPPI_IS_GEOMETRY (gg));
  g_return_if_fail (height >= 0);

  if (gg->priv->natural_height != height) {
    gg->priv->natural_height = height;
    gtk_signal_emit (GTK_OBJECT (gg), gg_signals[CHANGED_SIZE]);
  }
}

void
guppi_geometry_set_natural_size (GuppiGeometry *gg, double width, double height)
{
  g_return_if_fail (gg && GUPPI_IS_GEOMETRY (gg));
  g_return_if_fail (width >= 0 && height >= 0);

  if (gg->priv->natural_width != width || gg->priv->natural_height != height) {
    gg->priv->natural_width  = width;
    gg->priv->natural_height = height;
    gtk_signal_emit (GTK_OBJECT (gg), gg_signals[CHANGED_SIZE]);
  }
}

void
guppi_geometry_unset_natural_width (GuppiGeometry *gg)
{
  g_return_if_fail (gg && GUPPI_IS_GEOMETRY (gg));
  
  if (gg->priv->natural_width >= 0) {
    gg->priv->natural_width = -1;
    gtk_signal_emit (GTK_OBJECT (gg), gg_signals[CHANGED_SIZE]);
  }
}

void
guppi_geometry_unset_natural_height (GuppiGeometry *gg)
{
  g_return_if_fail (gg && GUPPI_IS_GEOMETRY (gg));
  
  if (gg->priv->natural_height >= 0) {
    gg->priv->natural_height = -1;
    gtk_signal_emit (GTK_OBJECT (gg), gg_signals[CHANGED_SIZE]);
  }
}

void
guppi_geometry_unset_natural_size (GuppiGeometry *gg)
{
  g_return_if_fail (gg && GUPPI_IS_GEOMETRY (gg));
  
  if (gg->priv->natural_width >= 0 || gg->priv->natural_height >= 0) {
    gg->priv->natural_width  = -1;
    gg->priv->natural_height = -1;
    gtk_signal_emit (GTK_OBJECT (gg), gg_signals[CHANGED_SIZE]);
  }
}

gboolean
guppi_geometry_has_natural_width (GuppiGeometry *gg)
{
  g_return_val_if_fail (gg && GUPPI_IS_GEOMETRY (gg), FALSE);
  return gg->priv->natural_width >= 0;
}

gboolean
guppi_geometry_has_natural_height (GuppiGeometry *gg)
{
  g_return_val_if_fail (gg && GUPPI_IS_GEOMETRY (gg), FALSE);
  return gg->priv->natural_height >= 0;
}

double
guppi_geometry_get_natural_width (GuppiGeometry *gg)
{
  g_return_val_if_fail (gg && GUPPI_IS_GEOMETRY (gg), 0);
  return gg->priv->natural_width;
}

double
guppi_geometry_get_natural_height (GuppiGeometry *gg)
{
  g_return_val_if_fail (gg && GUPPI_IS_GEOMETRY (gg), 0);
  return gg->priv->natural_height;
}

xmlNodePtr
guppi_geometry_export_xml (GuppiGeometry *gg, GuppiXMLDocument *doc)
{
  xmlNodePtr root_node;
  gchar buf[64];
  gchar *s;

  g_return_val_if_fail (gg && GUPPI_IS_GEOMETRY (gg), NULL);
  g_return_val_if_fail (doc != NULL, NULL);

  root_node = xmlNewNode (doc->ns, "Geometry");

  s = guppi_uniq2str (gg->priv->uid);
  xmlNewProp (root_node, "UID", s);
  guppi_free (s);

  if (! guppi_xml_document_has_cached (doc, gg->priv->uid)) {

    if (gg->priv->positioned) {

      g_snprintf (buf, 64, "%g", gg->priv->left);
      xmlNewProp (root_node, "left", buf);
      
      g_snprintf (buf, 64, "%g", gg->priv->right);
      xmlNewProp (root_node, "right", buf);

      g_snprintf (buf, 64, "%g", gg->priv->top);
      xmlNewProp (root_node, "top", buf);

      g_snprintf (buf, 64, "%g", gg->priv->bottom);
      xmlNewProp (root_node, "bottom", buf);

      if (gg->priv->natural_width >= 0) {
	g_snprintf (buf, 64, "%g", gg->priv->natural_width);
	xmlNewProp (root_node, "natural_width", buf);
      }

      if (gg->priv->natural_height >= 0) {
	g_snprintf (buf, 64, "%g", gg->priv->natural_height);
	xmlNewProp (root_node, "natural_height", buf);
      }
    }

    guppi_ref (gg);
    guppi_xml_document_cache_full (doc, gg->priv->uid, gg, guppi_unref_fn);
  }

  return root_node;
}

GuppiGeometry *
guppi_geometry_import_xml (GuppiXMLDocument *doc, xmlNodePtr node)
{
  GuppiGeometry *gg;
  guppi_uniq_t uid;
  gchar *str;

  if (strcmp (node->name, "Geometry"))
    return FALSE;

  str = xmlGetProp (node, "UID");
  uid = guppi_str2uniq (str);
  xmlFree (str);

  gg = guppi_xml_document_lookup (doc, uid);
  if (gg && GUPPI_IS_GEOMETRY (gg))
    return gg;

  gg = guppi_geometry_new ();

  gg->priv->uid = uid;
  gg->priv->positioned = FALSE;

  gg->priv->natural_width = -1;
  str = xmlGetProp (node, "natural_width");
  if (str) {
    sscanf (str, "%lg", &gg->priv->natural_width);
    xmlFree (str);
  }

  gg->priv->natural_height = -1;
  str = xmlGetProp (node, "natural_height");
  if (str) {
    sscanf (str, "%lg", &gg->priv->natural_height);
    xmlFree (str);
  }

  str = xmlGetProp (node, "left");
  if (str == NULL)
    goto finished;
  sscanf (str, "%lg", &gg->priv->left);
  xmlFree (str);

  str = xmlGetProp (node, "right");
  if (str == NULL)
    goto finished;
  sscanf (str, "%lg", &gg->priv->right);
  xmlFree (str);

  str = xmlGetProp (node, "top");
  if (str == NULL)
    goto finished;
  sscanf (str, "%lg", &gg->priv->top);
  xmlFree (str);

  str = xmlGetProp (node, "bottom");
  if (str == NULL)
    goto finished;
  sscanf (str, "%lg", &gg->priv->bottom);
  xmlFree (str);
  
  gg->priv->positioned = TRUE;

 finished:
  guppi_xml_document_cache_full (doc, gg->priv->uid, gg, guppi_unref_fn);

  return gg;
}

/* $Id$ */
