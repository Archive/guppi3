/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-canvas-group.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_CANVAS_GROUP_H
#define _INC_GUPPI_CANVAS_GROUP_H

/* #include <gnome.h> */
#include  "guppi-canvas-item.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS 

typedef struct _GuppiCanvasGroup GuppiCanvasGroup;
typedef struct _GuppiCanvasGroupClass GuppiCanvasGroupClass;

struct _GuppiCanvasGroup {
  GuppiCanvasItem parent;
  gpointer opaque_internals;
};

struct _GuppiCanvasGroupClass {
  GuppiCanvasItemClass parent_class;

  void (*add_hook) (GuppiCanvasGroup *grp, GuppiCanvasItem *item);
  void (*remove_hook) (GuppiCanvasGroup *grp, GuppiCanvasItem *item);
};

#define GUPPI_TYPE_CANVAS_GROUP (guppi_canvas_group_get_type())
#define GUPPI_CANVAS_GROUP(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_CANVAS_GROUP,GuppiCanvasGroup))
#define GUPPI_CANVAS_GROUP0(obj) ((obj) ? (GUPPI_CANVAS_GROUP(obj)) : NULL)
#define GUPPI_CANVAS_GROUP_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_CANVAS_GROUP,GuppiCanvasGroupClass))
#define GUPPI_IS_CANVAS_GROUP(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_CANVAS_GROUP))
#define GUPPI_IS_CANVAS_GROUP0(obj) (((obj) == NULL) || (GUPPI_IS_CANVAS_GROUP(obj)))
#define GUPPI_IS_CANVAS_GROUP_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_CANVAS_GROUP))

GtkType guppi_canvas_group_get_type (void);

GuppiCanvasItem *guppi_canvas_group_find_by_view (GuppiCanvasGroup *,
						  GuppiElementView *);

void guppi_canvas_group_foreach_item (GuppiCanvasGroup *, GuppiCanvasItemFunc, gpointer);

void guppi_canvas_group_auto_position_all (GuppiCanvasGroup *);


END_GUPPI_DECLS

#endif /* _INC_GUPPI_CANVAS_GROUP_H */

/* $Id$ */
