/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-text-block.h
 *
 * Copyright (C) 2001 Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_TEXT_BLOCK_H
#define _INC_GUPPI_TEXT_BLOCK_H

#include <gnome.h>
#include  "guppi-defs.h"
#include "guppi-element-print.h"
#include "guppi-text-tokens.h"

BEGIN_GUPPI_DECLS;

typedef struct _GuppiTextBlock GuppiTextBlock;
typedef struct _GuppiTextBlockClass GuppiTextBlockClass;

struct _GuppiTextBlock {
  GtkObject parent;
  gpointer opaque_internals;
};

struct _GuppiTextBlockClass {
  GtkObjectClass parent_class;

  void (*changed) (GuppiTextBlock *);
};

#define GUPPI_TYPE_TEXT_BLOCK (guppi_text_block_get_type ())
#define GUPPI_TEXT_BLOCK(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_TEXT_BLOCK,GuppiTextBlock))
#define GUPPI_TEXT_BLOCK0(obj) ((obj) ? (GUPPI_TEXT_BLOCK(obj)) : NULL)
#define GUPPI_TEXT_BLOCK_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_TEXT_BLOCK,GuppiTextBlockClass))
#define GUPPI_IS_TEXT_BLOCK(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_TEXT_BLOCK))
#define GUPPI_IS_TEXT_BLOCK0(obj) (((obj) == NULL) || (GUPPI_IS_TEXT_BLOCK(obj)))
#define GUPPI_IS_TEXT_BLOCK_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_TEXT_BLOCK))

GtkType guppi_text_block_get_type (void);

GuppiTextBlock *guppi_text_block_new (void);

void guppi_text_block_add (GuppiTextBlock *, GuppiTextToken *);
void guppi_text_block_clear (GuppiTextBlock *);

gchar *guppi_text_block_text (GuppiTextBlock *text);
void guppi_text_block_set_text (GuppiTextBlock *, const gchar *);

GnomeFont *guppi_text_block_font (GuppiTextBlock *text);
void guppi_text_block_set_font (GuppiTextBlock *text, GnomeFont *font);

double guppi_text_block_angle (GuppiTextBlock *text);
void guppi_text_block_set_angle (GuppiTextBlock *text, double angle);

void guppi_text_block_bbox (GuppiTextBlock *text, ArtDRect *bbox);
double guppi_text_block_width (GuppiTextBlock *text);
double guppi_text_block_height (GuppiTextBlock *text);

double guppi_text_block_max_width (GuppiTextBlock *text);
void guppi_text_block_set_max_width (GuppiTextBlock *text, double);

typedef void (*text_word_fn) (const gchar *str,
			      GnomeFont *font,
			      double offset_x, double offset_y,
			      gpointer user_data);

typedef void (*text_char_fn) (gint glyph,
			      const GnomeFontFace *face,
			      double affine[6],
			      gpointer user_data);

void guppi_text_block_foreach_word (GuppiTextBlock *text,
				    text_word_fn fn,
				    gpointer user_data);

void guppi_text_block_foreach_char (GuppiTextBlock *text,
				    text_char_fn fn,
				    gpointer user_data);

void guppi_text_block_print (GuppiTextBlock *text,
			     GuppiElementPrint *ep,
			     double x0, double y0,
			     GtkAnchorType anchor);

gboolean guppi_text_block_line_dimensions (GuppiTextBlock *text,
					   gint line_no,
					   double *length,
					   double *height_ascend,
					   double *height_descend);

void guppi_text_block_changed (GuppiTextBlock *);
void guppi_text_block_changed_delayed (GuppiTextBlock *);
void guppi_text_block_flush_changes (GuppiTextBlock *);

void guppi_text_block_freeze (GuppiTextBlock *);
void guppi_text_block_thaw (GuppiTextBlock *);

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_TEXT_BLOCK_H */

/* $Id$ */
