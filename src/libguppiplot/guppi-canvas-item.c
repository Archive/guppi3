/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-element-canvas-item.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-canvas-item.h"

#include <math.h>

#include <libgnomeui/gnome-canvas.h>
#include <libgnomeui/gnome-canvas-util.h>

#include <guppi-debug.h>
#include <guppi-convenient.h>
#include <guppi-memory.h>
#include <guppi-unique-id.h>
#include "guppi-element-view.h"

#include "guppi-basic-tools.h"

struct _GuppiCanvasItemPrivate {

  guppi_uniq_t id;

  /* Item bounds in canvas coordinates. */
  gint cx0, cy0, cx1, cy1;

  /* We keep both the state and the view, so we save an occasional
     function call. */
  GuppiElementState *state;
  guint state_changed_handler;

  GuppiElementView *view;
  guint view_changed_handler;

  gboolean clip_buf;

  double scale_factor;

  gint realize_idle;

  gboolean disable_local_tools;
  gboolean disable_class_tools;

  GuppiPlotToolkit *item_toolkit;
};

#define priv(x) ((x)->priv)

static GtkObjectClass *parent_class = NULL;

enum {
  CHANGED_SCALE,
  CHANGED_STATE,
  CHANGED_VIEW,
  LAST_SIGNAL
};

guint item_signals[LAST_SIGNAL] = { 0 };


static void
guppi_canvas_item_finalize (GtkObject *obj)
{
  GuppiCanvasItem *item = GUPPI_CANVAS_ITEM (obj);
  GuppiCanvasItemPrivate *p = priv (item);

  guppi_finalized (obj);

  if (p->view) {
    gtk_signal_disconnect (GTK_OBJECT (p->view), p->view_changed_handler);
    guppi_unref (p->view);
  }
  
  if (p->state) {
    gtk_signal_disconnect (GTK_OBJECT (p->state), p->state_changed_handler);
    guppi_unref (p->state);
  }
  
  if (p->realize_idle) {
    gtk_idle_remove (p->realize_idle);
    p->realize_idle = 0;
  }

  guppi_unref0 (p->item_toolkit);

  guppi_free0 (item->priv);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/***************************************************************************/

static gint
post_realize_idle_fn (gpointer ptr)
{
  GuppiCanvasItem *item = GUPPI_CANVAS_ITEM (ptr);
  GuppiCanvasItemClass *klass;

  if (priv (item)->view != NULL) {
    klass = GUPPI_CANVAS_ITEM_CLASS (GTK_OBJECT (item)->klass);
    g_assert (klass->post_realization_init);
    klass->post_realization_init (item);
    priv (item)->realize_idle = 0;
    return FALSE;
  }

  return TRUE;
}

static void
realize (GnomeCanvasItem *gnoitem)
{
  GuppiCanvasItem *item = GUPPI_CANVAS_ITEM (gnoitem);
  GuppiCanvasItemClass *klass;

  /* chain up */
  if (GNOME_CANVAS_ITEM_CLASS (parent_class)->realize)
    GNOME_CANVAS_ITEM_CLASS (parent_class)->realize (gnoitem);

  klass = GUPPI_CANVAS_ITEM_CLASS (GTK_OBJECT (gnoitem)->klass);
  if (klass->post_realization_init) {
    if (priv (item)->view != NULL) {
      klass->post_realization_init (item);
    } else {
      priv (item)->realize_idle = gtk_idle_add (post_realize_idle_fn, item);
    }
  }
}

static void
update (GnomeCanvasItem *item, double aff[6], ArtSVP *clip_path, gint flags)
{
  GuppiCanvasItem *gec;
  GuppiCanvasItemClass *klass;
  double x1, y1, x2, y2;

  g_return_if_fail (item != NULL);

  GUPPI_ENTER_MESSAGE (gtk_type_name (GTK_OBJECT_TYPE (item)));

  gec = GUPPI_CANVAS_ITEM (item);

  /* Save our bounding box */
  x1 = item->x1;
  y1 = item->y1;
  x2 = item->x2;
  y2 = item->y2;

  /* Chain our calls to update. */
  if (parent_class && GNOME_CANVAS_ITEM_CLASS (parent_class)->update) {
    GNOME_CANVAS_ITEM_CLASS (parent_class)->update (item, aff, clip_path,
						    flags);
  }

  /* Restore our bounding box.  We have to do this because the
     GnomeCanvasGroup's update function clobbers it... */
  item->x1 = x1;
  item->y1 = y1;
  item->x2 = x2;
  item->y2 = y2;

  /* Call our guppi_update function. */
  klass = GUPPI_CANVAS_ITEM_CLASS (GTK_OBJECT (gec)->klass);

  if (guppi_canvas_item_scale (gec) > 0 &&
      klass->guppi_update && (item->object.flags & GNOME_CANVAS_ITEM_VISIBLE))
    (klass->guppi_update) (gec, aff, clip_path, flags);

  guppi_canvas_item_request_total_redraw (gec);

  GUPPI_EXIT;
}

static void
render (GnomeCanvasItem *item, GnomeCanvasBuf *in_buf)
{
  GuppiCanvasItem *gec;
  GuppiCanvasItemPrivate *p;
  GuppiCanvasItemClass *klass;
  GuppiElementView *view;
  GnomeCanvasBuf my_buf;
  GnomeCanvasBuf *buf;
  gboolean all_gone = FALSE;

  g_return_if_fail (item != NULL);
  g_return_if_fail (in_buf != NULL);

  gec = GUPPI_CANVAS_ITEM (item);
  p = priv (gec);
  view = guppi_canvas_item_view (gec);

  /* If we are hidden, this is as far as we go... */
  if (!(item->object.flags & GNOME_CANVAS_ITEM_VISIBLE)) {
    return;
  }

  if (!guppi_element_view_visible (view)) {
    return;
  }

  /* Do the right thing if we are the first to draw on the canvas. */
  if (in_buf->is_bg) {
    gnome_canvas_buf_ensure_buf (in_buf);
    in_buf->is_bg = FALSE;
  }

  /*
     g_message("%d:%d %d:%d / %d:%d %d:%d", 
     in_buf->rect.x0, in_buf->rect.x1,
     in_buf->rect.y0, in_buf->rect.y1,
     gec->cx0, gec->cx1, gec->cy0, gec->cy1);
   */

  /* Build our clipped GnomeCanvasBuf. */
  if (p->clip_buf) {
    my_buf.buf_rowstride = in_buf->buf_rowstride;

    my_buf.rect.x0 = MAX (in_buf->rect.x0, p->cx0);
    my_buf.rect.x1 = MIN (in_buf->rect.x1, p->cx1);
    my_buf.rect.y0 = MAX (in_buf->rect.y0, p->cy0);
    my_buf.rect.y1 = MIN (in_buf->rect.y1, p->cy1);

    my_buf.bg_color = in_buf->bg_color;
    my_buf.is_bg = in_buf->is_bg;
    my_buf.is_buf = in_buf->is_buf;

    my_buf.buf = in_buf->buf;
    my_buf.buf += 3 * (my_buf.rect.x0 - in_buf->rect.x0);
    my_buf.buf += in_buf->buf_rowstride * (my_buf.rect.y0 - in_buf->rect.y0);

    buf = &my_buf;

    if (buf->rect.x0 >= buf->rect.x1 || buf->rect.y0 >= buf->rect.y1)
      all_gone = TRUE;


  } else {

    buf = in_buf;

  }

  /* Only render if our scale value looks right. */
  if (guppi_canvas_item_scale (gec) > 0) {

    /* Chain our calls to render. */
    if (parent_class && GNOME_CANVAS_ITEM_CLASS (parent_class)->render)
      GNOME_CANVAS_ITEM_CLASS (parent_class)->render (item, in_buf);

    /* Call our guppi_render function. */
    klass = GUPPI_CANVAS_ITEM_CLASS (GTK_OBJECT (gec)->klass);
    if (klass->guppi_render && !all_gone)
      (klass->guppi_render) (gec, buf);
  }
}

static double
point (GnomeCanvasItem *gci, double x, double y, gint cx, gint cy,
       GnomeCanvasItem **actual_item)
{
  double dist;

  if (gci->x1 <= x && x < gci->x2 && gci->y1 <= y && y < gci->y2)
    dist = 0;
  else
    dist = MIN (MIN (fabs (x - gci->x1), fabs (x - gci->x2)),
		MIN (fabs (y - gci->y1), fabs (y - gci->y2)));

  *actual_item = gci;
  return dist;
}

static void
foreach (GuppiCanvasItem *item, GuppiCanvasItemFunc fn, gpointer ptr)
{
  fn (item, ptr);
}

static void
changed_scale (GuppiCanvasItem *item, double scale)
{
  guppi_canvas_item_request_update (item);
}

static void
changed_state (GuppiCanvasItem *item)
{
  guppi_canvas_item_request_update (item);
}

static void
changed_view (GuppiCanvasItem *item)
{
  guppi_canvas_item_request_update (item);
}

typedef void (*GuppiSignal_NONE__DOUBLE) (GtkObject *, double, gpointer user_data);

static void
guppi_marshal_NONE__DOUBLE (GtkObject *object, GtkSignalFunc func, gpointer func_data, GtkArg *args)
{
  GuppiSignal_NONE__DOUBLE rfunc;

  rfunc = (GuppiSignal_NONE__DOUBLE) func;
  
  (*rfunc) (object, GTK_VALUE_DOUBLE (args[0]), func_data);
}

static void
guppi_canvas_item_class_init (GuppiCanvasItemClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GnomeCanvasItemClass *item_class = GNOME_CANVAS_ITEM_CLASS (klass);

  parent_class = gtk_type_class (GNOME_TYPE_CANVAS_GROUP);

  object_class->finalize = guppi_canvas_item_finalize;

  item_class->realize = realize;
  item_class->update  = update;
  item_class->render  = render;
  item_class->point   = point;

  klass->foreach       = foreach;
  klass->changed_scale = changed_scale;
  klass->changed_state = changed_state;
  klass->changed_view  = changed_view;

  item_signals[CHANGED_SCALE] =
    gtk_signal_new ("changed_scale",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GuppiCanvasItemClass, changed_scale),
		    guppi_marshal_NONE__DOUBLE,
		    GTK_TYPE_NONE, 1, GTK_TYPE_DOUBLE);
  
  item_signals[CHANGED_STATE] =
    gtk_signal_new ("changed_state",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GuppiCanvasItemClass, changed_state),
		    gtk_marshal_NONE__NONE,
		    GTK_TYPE_NONE, 0, GTK_TYPE_NONE);

  item_signals[CHANGED_VIEW] =
    gtk_signal_new ("changed_view",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GuppiCanvasItemClass, changed_view),
		    gtk_marshal_NONE__NONE,
		    GTK_TYPE_NONE, 0, GTK_TYPE_NONE);

  gtk_object_class_add_signals (object_class, item_signals, LAST_SIGNAL);
}

static void
guppi_canvas_item_init (GuppiCanvasItem *obj)
{
  GuppiCanvasItemPrivate *p;
  obj->priv = p = guppi_new0 (GuppiCanvasItemPrivate, 1);

  p->id = guppi_unique_id ();

  p->clip_buf = TRUE;
}

GtkType
guppi_canvas_item_get_type (void)
{
  static GtkType guppi_canvas_item_type = 0;
  if (!guppi_canvas_item_type) {
    static const GtkTypeInfo guppi_canvas_item_info = {
      "GuppiCanvasItem",
      sizeof (GuppiCanvasItem),
      sizeof (GuppiCanvasItemClass),
      (GtkClassInitFunc) guppi_canvas_item_class_init,
      (GtkObjectInitFunc) guppi_canvas_item_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_canvas_item_type = gtk_type_unique (GNOME_TYPE_CANVAS_GROUP,
					      &guppi_canvas_item_info);
  }
  return guppi_canvas_item_type;
}

/***************************************************************************/

/**
 * guppi_canvas_item_state
 * @item: the canvas item
 *
 * Returns the state object associated with this canvas item.
 **/
GuppiElementState *
guppi_canvas_item_state (GuppiCanvasItem *item)
{
  g_return_val_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item), NULL);

  return priv (item)->state;
}

/**
 * guppi_canvas_item_view
 * @item: the canvas item
 *
 * Returns the view object associated with this canvas item.
 **/
GuppiElementView *
guppi_canvas_item_view (GuppiCanvasItem *item)
{
  g_return_val_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item), NULL);

  return priv (item)->view;
}

/**
 * guppi_canvas_item_unique_id
 * @item: the canvas item
 *
 * Returns the canvas item's unique ID.
 **/
guppi_uniq_t
guppi_canvas_item_unique_id (GuppiCanvasItem *item)
{
  g_return_val_if_fail (item && GUPPI_IS_CANVAS_ITEM (item), 0);
  return priv (item)->id;
}

/**
 * guppi_canvas_item_scale
 * @item: the canvas item
 *
 * Returns the item's current scale factor.
 **/
double
guppi_canvas_item_scale (GuppiCanvasItem *item)
{
  g_return_val_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item), 0);

  return priv (item)->scale_factor;
}

/**
 * guppi_canvas_item_geometry
 * @item: the canvas item
 *
 * Returns the item's current geometry.
 **/
GuppiGeometry *
guppi_canvas_item_geometry (GuppiCanvasItem *item)
{
  g_return_val_if_fail (item && GUPPI_IS_CANVAS_ITEM (item), FALSE);
  return guppi_element_view_geometry (priv (item)->view);
}

/**
 * guppi_canvas_item_positioned
 * @item: the canvas item
 *
 * Returns %TRUE if the item's geometry has been specified yet,
 * and %FALSE otherwise.
 *
 **/
gboolean
guppi_canvas_item_positioned (GuppiCanvasItem *item)
{
  GuppiGeometry *geom;
  g_return_val_if_fail (item && GUPPI_IS_CANVAS_ITEM (item), FALSE);
  geom = guppi_element_view_geometry (priv (item)->view);
  return guppi_geometry_positioned (geom);
}

static void
state_changed_cb (GuppiElementState *state, gpointer item)
{
  gtk_signal_emit (GTK_OBJECT (item), item_signals[CHANGED_STATE]);
}

static void
view_changed_cb (GuppiElementView *view, gpointer item)
{
  gtk_signal_emit (GTK_OBJECT (item), item_signals[CHANGED_VIEW]);
}

/**
 * guppi_canvas_item_set_view
 * @item: the canvas item
 * @view: a view
 *
 * Binds a specific #GuppiElementView to the canvas item.  This
 * function can only be called once per #GuppiCanvasItem, and will
 * fail (printing an warning to the console) if the item already has a
 * different view bound to it.
 *
 */
void
guppi_canvas_item_set_view (GuppiCanvasItem *item, GuppiElementView *view)
{
  GuppiCanvasItemPrivate *p;

  g_return_if_fail (GUPPI_IS_CANVAS_ITEM (item));
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));

  p = priv (item);
  g_return_if_fail (p->view == NULL);

  p->view = view;
  p->state = guppi_element_view_state (view);

  guppi_ref (p->view);
  guppi_ref (p->state);

  p->state_changed_handler = gtk_signal_connect (GTK_OBJECT (p->state),
						 "changed",
						 GTK_SIGNAL_FUNC (state_changed_cb),
						 item);
  p->view_changed_handler = gtk_signal_connect (GTK_OBJECT (p->view),
						"changed",
						GTK_SIGNAL_FUNC (view_changed_cb),
						item);
}

/**
 * guppi_canvas_item_set_scale
 * @item: the canvas item
 * @scale: the desired scale factor
 *
 * Scales the canvas item by the desired scale factor.  Items with
 * non-positive scale factors are simply not rendered.
 *
 **/
void
guppi_canvas_item_set_scale (GuppiCanvasItem *item, double s)
{
  GuppiCanvasItemClass *klass;
  g_return_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item));
  g_return_if_fail (s >= 0);

  klass = GUPPI_CANVAS_ITEM_CLASS (GTK_OBJECT (item)->klass);

  if (priv (item)->scale_factor != s) {
    priv (item)->scale_factor = s;
    gtk_signal_emit (GTK_OBJECT (item), item_signals[CHANGED_SCALE], s);
    guppi_canvas_item_request_update (item);
  }
}

/***************************************************************************/

/**
 * guppi_canvas_item_set_bbox_c
 * @item: the canvas item
 * @cx0: the lesser x-coordinate of a bounding box
 * @cx1: the greater x-coordinate of a bounding box
 * @cy0: the lesser y-coordinate of a bounding box
 * @cy1: the greater y-coordinate of a bounding box
 *
 * Sets the bounding box of the canvas item, in canvas coordinates.
 **/
void
guppi_canvas_item_set_bbox_c (GuppiCanvasItem *item,
			      gint cx0, gint cy0, gint cx1, gint cy1)
{
  GuppiCanvasItemClass *klass;
  GuppiCanvasItemPrivate *p;

  g_return_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item));
  p = priv (item);

  guppi_2sort_i (&cx0, &cx1);
  guppi_2sort_i (&cy0, &cy1);

  if (p->cx0 != cx0 || p->cy0 != cy0 || p->cx1 != cx1 || p->cy1 != cy1) {

    klass = GUPPI_CANVAS_ITEM_CLASS (GTK_OBJECT (item)->klass);

    p->cx0 = cx0;
    p->cx1 = cx1;
    p->cy0 = cy0;
    p->cy1 = cy1;

    gnome_canvas_update_bbox (GNOME_CANVAS_ITEM (item), cx0, cy0, cx1, cy1);

    if (klass->bbox_changed_hook)
      klass->bbox_changed_hook (item, cx0, cy0, cx1, cy1);

    guppi_canvas_item_request_update (item);
  }
}

/**
 * guppi_canvas_item_get_bbox_c
 * @item: the canvas item
 * @cx0: the lesser x-coordinate of a bounding box
 * @cy0: the lesser y-coordinate of a bounding box
 * @cx1: the greater x-coordinate of a bounding box
 * @cy1: the greater y-coordinate of a bounding box
 *
 * Gives the bounding box of the canvas
 * item, in canvas coordinates.  If %NULL is passed to any of the
 * coordinate arguments, that coordinate is ignored.
 *
 **/
void
guppi_canvas_item_get_bbox_c (GuppiCanvasItem *item,
			      gint *cx0, gint *cy0, gint *cx1, gint *cy1)
{
  GuppiCanvasItemPrivate *p;

  g_return_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item));
  p = priv (item);

  if (cx0)
    *cx0 = p->cx0;
  if (cx1)
    *cx1 = p->cx1;
  if (cy0)
    *cy0 = p->cy0;
  if (cy1)
    *cy1 = p->cy1;
}

/***************************************************************************/

/**
 * guppi_canvas_item_get_bbox_vp
 * @item: the canvas item
 * @x0: a pointer to the lesser x-coordinate of a bounding box
 * @y0: a pointer to the lesser y-coordinate of a bounding box
 * @x1: a pointer to the greater x-coordinate of a bounding box
 * @y1: a pointer to the greater y-coordinate of a bounding box
 *
 * Gives the bounding box of the canvas
 * item, in virtual coordinates.  If %NULL is passed to any of the
 * coordinate arguments, that coordinate is ignored.
 *
 **/
void
guppi_canvas_item_get_bbox_vp (GuppiCanvasItem *item,
			       double *x0, double *y0, double *x1, double *y1)
{
  g_return_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item));

  guppi_element_view_get_bbox_vp (guppi_canvas_item_view (item),
				  x0, y0, x1, y1);
}

/**
 * guppi_canvas_item_get_bbox_pt
 * @item: the canvas item
 * @x0: a pointer to the lesser x-coordinate of a bounding box
 * @y0: a pointer to the lesser y-coordinate of a bounding box
 * @x1: a pointer to the greater x-coordinate of a bounding box
 * @y1: a pointer to the greater y-coordinate of a bounding box
 *
 * Gives the bounding box of the canvas
 * item, in ps-points.  If %NULL is passed to any of the
 * coordinate arguments, that coordinate is ignored.
 *
 **/
void
guppi_canvas_item_get_bbox_pt (GuppiCanvasItem *item,
			       double *x0, double *y0, double *x1, double *y1)
{
  g_return_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item));

  guppi_element_view_get_bbox_pt (guppi_canvas_item_view (item),
				  x0, y0, x1, y1);
}

/**************************************************************************/

/**
 * guppi_canvas_item_conv
 * @item: the canvas item
 * @c_x: an x-value, in canvas coordinates
 * @c_y: a y-value, in canvas coordinates
 * @conv_x: a pointer to the converted x-value, in unit coordinates
 * @conv_y: a pointer to the converted y-value, in unit coordinates
 *
 * Transform an (x,y)-pair from canvas to unit coordinates.
 *
 **/
void
guppi_canvas_item_conv (GuppiCanvasItem *item,
			double c_x, double c_y,
			double *conv_x, double *conv_y)
{
  GuppiCanvasItemPrivate *p;
  g_return_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item));
  p = priv (item);

  if (conv_x)
    *conv_x = (c_x - p->cx0) / (p->cx1 - p->cx0);

  if (conv_y)
    *conv_y = (c_y - p->cy0) / (p->cy1 - p->cy0);
}

/**
 * guppi_canvas_item_unconv
 * @item: the canvas item
 * @conv_x: an x-value, in unit coordinates
 * @conv_y: a y-value, in unit coordinates
 * @c_x: a pointer the converted x-value, in canvas coordinates
 * @c_y: a pointer to the converted y-value, in canvas coordinates
 *
 * Transform an (x,y)-pair from unit to canvas coordinates.
 *
 **/
void
guppi_canvas_item_unconv (GuppiCanvasItem *item,
			  double conv_x, double conv_y,
			  double *c_x, double *c_y)
{
  GuppiCanvasItemPrivate *p;
  g_return_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item));
  p = priv (item);

  if (c_x)
    *c_x = p->cx0 + conv_x * (p->cx1 - p->cx0);

  if (c_y)
    *c_y = p->cy0 + conv_y * (p->cy1 - p->cy0);
}

/**
 * guppi_canvas_item_vp2c
 * @item: the canvas item
 * @vp_x: an x-value, in virtual coordinates
 * @vp_y: a y-value, in virtual coordinates
 * @c_x: a pointer to the converted x-value, in canvas coordinates
 * @c_y: a pointer to the converted y-value, in canvas coordinates
 *
 * Transform an (x,y)-pair from virtual to canvas coordinates.
 *
 **/
void
guppi_canvas_item_vp2c (GuppiCanvasItem *item,
			double vp_x, double vp_y, gint* c_x, gint *c_y)
{
  double x = 0, y = 0;

  g_return_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item));

  guppi_canvas_item_vp2c_d (item, vp_x, vp_y,
			    c_x ? &x : NULL, c_y ? &y : NULL);

  if (c_x)
    *c_x = rint (x);
  if (c_y)
    *c_y = rint (y);
}

/**
 * guppi_canvas_item_vp2c_d
 * @item: the canvas item
 * @vp_x: an x-value, in virtual coordinates
 * @vp_y: a y-value, in virtual coordinates
 * @c_x: a pointer to the converted x-value, in fractional canvas coordinates
 * @c_y: a pointer to the converted y-value, in fractional canvas coordinates
 *
 * Transform an (x,y)-pair from virtual to canvas coordinates, but don't
 * round off the canvas coordinates after converting.
 *
 **/
void
guppi_canvas_item_vp2c_d (GuppiCanvasItem *item,
			  double vp_x, double vp_y, double *c_x, double *c_y)
{
  GuppiElementView *view;
  GuppiViewInterval *vix;
  GuppiViewInterval *viy;
  double tx, ty;

  g_return_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item));

  view = guppi_canvas_item_view (item);
  vix = guppi_element_view_axis_view_interval (view, GUPPI_X_AXIS);
  viy = guppi_element_view_axis_view_interval (view, GUPPI_Y_AXIS);

  tx = guppi_view_interval_conv (vix, vp_x);
  ty = guppi_view_interval_conv (viy, vp_y);

  guppi_canvas_item_unconv (item, tx, 1 - ty, c_x, c_y);
}

/**
 * guppi_canvas_item_x_vp2c_d_bulk
 * @item: the canvas item
 * @vp_x: an array of x-values, in virtual coordinates
 * @c_x: an array to hold the converted fractional canvas x-coordinates
 * @N: the size of the arrays
 *
 * Convert @N x-values from virtual coordinates to fractional
 * canvas coordinates.
 *
 **/
void
guppi_canvas_item_x_vp2c_d_bulk (GuppiCanvasItem *item,
				 const double *vp_x,
				 double *c_x,
				 gsize N)
{
  GuppiCanvasItemPrivate *p;
  GuppiElementView *view;
  GuppiViewInterval *vix;
  double xw;
  gsize i;

  g_return_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item));
  if (N == 0)
    return;
  g_return_if_fail (vp_x != NULL && c_x != NULL);

  p = priv (item);

  view = guppi_canvas_item_view (item);
  vix = guppi_element_view_axis_view_interval (view, GUPPI_X_AXIS);

  guppi_view_interval_conv_bulk (vix, vp_x, c_x, N);

  xw = p->cx1 - p->cx0;
  for (i = 0; i < N; ++i) 
    c_x[i] = p->cx0 + c_x[i] * xw;

}

/**
 * guppi_canvas_item_y_vp2c_d_bulk
 * @item: the canvas item
 * @vp_y: an array of y-values, in virtual coordinates
 * @c_y: an array to hold the converted fractional canvas y-coordinates
 * @N: the size of the arrays
 *
 * Convert @N y-values from virtual coordinates to fractional
 * canvas coordinates.
 *
 **/
void
guppi_canvas_item_y_vp2c_d_bulk (GuppiCanvasItem *item,
				 const double *vp_y,
				 double *c_y,
				 gsize N)
{
  GuppiCanvasItemPrivate *p;
  GuppiElementView *view;
  GuppiViewInterval *viy;
  double yw;
  gsize i;

  g_return_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item));
  if (N == 0)
    return;
  g_return_if_fail (vp_y != NULL && c_y != NULL);

  p = priv (item);

  view = guppi_canvas_item_view (item);
  viy = guppi_element_view_axis_view_interval (view, GUPPI_Y_AXIS);

  guppi_view_interval_conv_bulk (viy, vp_y, c_y, N);

  yw = p->cy1 - p->cy0;
  for (i = 0; i < N; ++i) 
    c_y[i] = p->cy1 - c_y[i] * yw;

}

/**
 * guppi_canvas_item_vp2c_d_bulk
 * @item: the canvas item
 * @vp_x: an array of x-values, in virtual coordinates
 * @vp_y: an array of y-values, in virtual coordinates
 * @c_x: an array to hold the converted fractional canvas x-coordinates
 * @c_y: an array to hold the converted fractional canvas y-coordinates
 * @N: the size of the arrays
 *
 * Convert @N (x,y)-pairs from virtual coordinates to fractional
 * canvas coordinates.
 *
 **/
void
guppi_canvas_item_vp2c_d_bulk (GuppiCanvasItem *item,
			       const double *vp_x,
			       const double *vp_y,
			       double *c_x, double *c_y, gsize N)
{
  GuppiCanvasItemPrivate *p;
  GuppiElementView *view;
  GuppiViewInterval *vix;
  GuppiViewInterval *viy;
  double xw, yw;
  gsize i;

  g_return_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item));
  if (N == 0)
    return;
  g_return_if_fail (vp_x != NULL && vp_y != NULL && c_x != NULL
		    && c_y != NULL);

  p = priv (item);

  view = guppi_canvas_item_view (item);
  vix = guppi_element_view_axis_view_interval (view, GUPPI_X_AXIS);
  viy = guppi_element_view_axis_view_interval (view, GUPPI_Y_AXIS);

  guppi_view_interval_conv_bulk (vix, vp_x, c_x, N);
  guppi_view_interval_conv_bulk (viy, vp_y, c_y, N);

  xw = p->cx1 - p->cx0;
  yw = p->cy1 - p->cy0;
  for (i = 0; i < N; ++i) {
    c_x[i] = p->cx0 + c_x[i] * xw;
    c_y[i] = p->cy1 - c_y[i] * yw;
  }
}

/**
 * guppi_canvas_item_c2vp
 * @item: the canvas item
 * @c_x: an x-value, in canvas coordinates
 * @c_y: a y-value, in canvas coordinates
 * @vp_x: a pointer to the converted x-value, in virtual coordinates
 * @vp_y: a pointer to the converted y-value, in virtual coordinates
 *
 * Transform an (x,y)-pair from canvas to virtual coordinates.
 *
 **/
void
guppi_canvas_item_c2vp (GuppiCanvasItem *gci,
			double c_x, double c_y, double *vp_x, double *vp_y)
{
  GuppiElementView *view;
  double tx, ty;

  g_return_if_fail (gci != NULL);
  g_return_if_fail (GUPPI_IS_CANVAS_ITEM (gci));

  view = guppi_canvas_item_view (gci);

  guppi_canvas_item_conv (gci, c_x, c_y, &tx, &ty);

  if (vp_x) {
    GuppiViewInterval *vi = guppi_element_view_axis_view_interval (view, GUPPI_X_AXIS);
    *vp_x = guppi_view_interval_unconv (vi, tx);
  }

  if (vp_y) {
    GuppiViewInterval *vi = guppi_element_view_axis_view_interval (view, GUPPI_Y_AXIS);
    *vp_y = guppi_view_interval_unconv (vi, 1 - ty);
  }

}

/**
 * guppi_canvas_item_pt2c
 * @item: the canvas item
 * @pt_x: an x-value, in ps-point coordinates
 * @pt_y: a y-value, in ps-point coordinates
 * @c_x: a pointer to the converted x-value, in canvas coordinates
 * @c_y: a pointer to the converted y-value, in canvas coordinates
 *
 * Transform an (x,y)-pair from virtual to canvas coordinates.
 *
 **/
void
guppi_canvas_item_pt2c (GuppiCanvasItem *gci,
			double pt_x, double pt_y, gint *c_x, gint *c_y)
{
  double x = 0, y = 0;

  g_return_if_fail (gci != NULL);
  g_return_if_fail (GUPPI_IS_CANVAS_ITEM (gci));

  guppi_canvas_item_pt2c_d (gci, pt_x, pt_y,
			    c_x ? &x : NULL, c_y ? &y : NULL);

  if (c_x)
    *c_x = rint (x);
  if (c_y)
    *c_y = rint (y);
}

/**
 * guppi_canvas_item_pt2c_d
 * @item: the canvas item
 * @pt_x: an x-value, in ps-point coordinates
 * @pt_y: a y-value, in ps-point coordinates
 * @c_x: a pointer to the converted x-value, in fractional canvas coordinates
 * @c_y: a pointer to the converted y-value, in fractional canvas coordinates
 *
 * Transform an (x,y)-pair from ps-point to canvas coordinates, but don't
 * round off the canvas coordinates after converting.
 *
 **/
void
guppi_canvas_item_pt2c_d (GuppiCanvasItem *gci,
			  double pt_x, double pt_y, double *c_x, double *c_y)
{
  GuppiGeometry *geom;
  double tx, ty;

  g_return_if_fail (gci != NULL);
  g_return_if_fail (GUPPI_IS_CANVAS_ITEM (gci));

  geom = guppi_element_view_geometry (guppi_canvas_item_view (gci));

  guppi_geometry_conv (geom, pt_x, pt_y, &tx, &ty);

  guppi_canvas_item_unconv (gci, tx, 1 - ty, c_x, c_y);
}

/**
 * guppi_canvas_item_c2pt
 * @item: the canvas item
 * @c_x: an x-value, in canvas coordinates
 * @c_y: a y-value, in canvas coordinates
 * @pt_x: a pointer to the converted x-value, in ps-point coordinates
 * @pt_y: a pointer to the converted y-value, in ps-point coordinates
 *
 * Transform an (x,y)-pair from canvas to ps-point coordinates.
 *
 **/
void
guppi_canvas_item_c2pt (GuppiCanvasItem *gci,
			double c_x, double c_y, double *pt_x, double *pt_y)
{
  GuppiGeometry *geom;
  double tx, ty;

  g_return_if_fail (gci != NULL);
  g_return_if_fail (GUPPI_IS_CANVAS_ITEM (gci));

  geom = guppi_element_view_geometry (guppi_canvas_item_view (gci));

  guppi_canvas_item_conv (gci, c_x, c_y, &tx, &ty);
  guppi_geometry_unconv (geom, tx, 1 - ty, pt_x, pt_y);
}

/**
 * guppi_canvas_item_vpath_vp2c
 * @item: the canvas item
 * @path: a path
 *
 * Transform the vertices of @path from virtual to canvas coordinates.
 *
 **/
void
guppi_canvas_item_vpath_vp2c (GuppiCanvasItem *gci, ArtVpath *path)
{
  gint i;

  g_return_if_fail (gci != NULL);
  g_return_if_fail (GUPPI_IS_CANVAS_ITEM (gci));
  g_return_if_fail (path != NULL);

  for (i = 0; path[i].code != ART_END; ++i) {
    guppi_canvas_item_vp2c_d (gci,
			      path[i].x, path[i].y, &path[i].x, &path[i].y);
  }
}

/**
 * guppi_canvas_item_vpath_pt2c
 * @item: the canvas item
 * @path: a path
 *
 * Transform the vertices of @path from ps-point to canvas coordinates.
 *
 **/
void
guppi_canvas_item_vpath_pt2c (GuppiCanvasItem *gci, ArtVpath *path)
{
  gint i;

  g_return_if_fail (gci != NULL);
  g_return_if_fail (GUPPI_IS_CANVAS_ITEM (gci));
  g_return_if_fail (path != NULL);

  for (i = 0; path[i].code != ART_END; ++i) {
    guppi_canvas_item_pt2c_d (gci,
			      path[i].x, path[i].y, &path[i].x, &path[i].y);
  }
}


/***************************************************************************/

/**
 * guppi_canvas_item_request_redraw_c
 * @item: the canvas item
 * @cx0: the lesser x-coordinate of a box
 * @cy0: the lesser y-coordinate of a box
 * @cx1: the greater x-coordinate of a box
 * @cy1: the greater y-coordinate of a box
 *
 * Request that the piece of the canvas item intersecting the specified box
 * is redrawn.  The box is taken to be in canvas coordinates.
 *
 **/
void
guppi_canvas_item_request_redraw_c (GuppiCanvasItem *gec,
				    gint cx0, gint cy0, gint cx1, gint cy1)
{
  GuppiCanvasItemPrivate *p;
  GnomeCanvas *canvas;
  double wx0, wy0, wx1, wy1;
  gint c_cx0, c_cy0, c_cx1, c_cy1;

  g_return_if_fail (gec != NULL && GUPPI_IS_CANVAS_ITEM (gec));
  p = priv (gec);

  canvas = GNOME_CANVAS_ITEM (gec)->canvas;

  guppi_2sort_i (&cx0, &cx1);
  guppi_2sort_i (&cy0, &cy1);

  guppi_canvas_item_get_bbox_c (gec, &c_cx0, &c_cy0, &c_cx1, &c_cy1);

  cx0 = MAX (cx0, c_cx0);
  cx1 = MIN (cx1, c_cx1);
  cy0 = MAX (cy0, c_cy0);
  cy1 = MIN (cy1, c_cy1);

  gnome_canvas_c2w (canvas, cx0, cy0, &wx0, &wy0);
  gnome_canvas_c2w (canvas, cx1, cy1, &wx1, &wy1);

  gnome_canvas_request_redraw (canvas, wx0, wy0, wx1, wy1);
}

/**
 * guppi_canvas_item_request_redraw_vp
 * @item: the canvas item
 * @vx0: the lesser x-coordinate of a box
 * @vy0: the lesser y-coordinate of a box
 * @vx1: the greater x-coordinate of a box
 * @vy1: the greater y-coordinate of a box
 *
 * Request that the piece of the canvas item intersecting the specified box
 * is redrawn.  The box is taken to be in virtual coordinates.
 *
 **/
void
guppi_canvas_item_request_redraw_vp (GuppiCanvasItem *gec,
				     double vx0, double vy0,
				     double vx1, double vy1)
{
  gint cx0, cy0, cx1, cy1;
  g_return_if_fail (gec != NULL);

  guppi_canvas_item_vp2c (gec, vx0, vy0, &cx0, &cy0);
  guppi_canvas_item_vp2c (gec, vx1, vy1, &cx1, &cy1);

  guppi_canvas_item_request_redraw_c (gec, cx0, cy0, cx1 + 1, cy1 + 1);
}

/**
 * guppi_canvas_item_request_total_redraw
 * @item: the canvas item
 * 
 * Request that the entire canvas item be redrawn.
 *
 **/
void
guppi_canvas_item_request_total_redraw (GuppiCanvasItem *item)
{
  gint cx0, cx1, cy0, cy1;

  g_return_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item));
  guppi_canvas_item_get_bbox_c (item, &cx0, &cy0, &cx1, &cy1);

  gnome_canvas_request_redraw (GNOME_CANVAS_ITEM (item)->canvas,
			       cx0, cy0, cx1, cy1);
}

/**
 * guppi_canvas_item_request_update
 * @item: the canvas item
 *
 * Request a canvas update for the specified item.
 *
 **/
void
guppi_canvas_item_request_update (GuppiCanvasItem *gec)
{
  g_return_if_fail (gec != NULL);

  gnome_canvas_item_request_update (GNOME_CANVAS_ITEM (gec));
}

/***************************************************************************/

/**
 * guppi_canvas_item_foreach
 * @item: the canvas item
 * @func: a callback function
 * @user_data: a pointer passed to the callback function
 *
 * Call the function @func on @item and each subitem of @item.
 * If @item is not some sort of canvas group, this is just equivalent
 * to calling the function @func with @item and @user_data as
 * arguments.
 *
 **/
void
guppi_canvas_item_foreach (GuppiCanvasItem *item,
			   GuppiCanvasItemFunc fn, gpointer user_data)
{
  GuppiCanvasItemClass *klass;

  g_return_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item));
  g_return_if_fail (fn != NULL);

  klass = GUPPI_CANVAS_ITEM_CLASS (GTK_OBJECT (item)->klass);
  if (klass->foreach)
    klass->foreach (item, fn, user_data);
}

struct foreach_at {
  double pt_x, pt_y;
  GuppiCanvasItemFunc fn;
  gpointer user_data;
};

static void
foreach_at_fn (GuppiCanvasItem *item, gpointer ptr)
{
  struct foreach_at *fa = (struct foreach_at *) ptr;
  GuppiGeometry *geom = guppi_canvas_item_geometry (item);

  if (guppi_geometry_contains (geom, fa->pt_x, fa->pt_y))
    fa->fn (item, fa->user_data);
}

/**
 * guppi_canvas_item_foreach_at
 * @item: the canvas item
 * @pt_x: an x-coordinate, in ps-points
 * @pt_y: a y-coordinate, in ps-points
 * @func: a callback function
 * @user_data: a pointer passed to the callback function
 *
 * Call the function @func on the item, and each subitem whose bounding
 * box contains the point (@pt_x, @py_y).  Again, if @item isn't a group
 * object, this just causes @func to be called with @item and @user_data
 * as arguments.
 *
 **/
void
guppi_canvas_item_foreach_at (GuppiCanvasItem *item,
			      double pt_x, double pt_y,
			      GuppiCanvasItemFunc fn, gpointer user_data)
{
  GuppiGeometry *geom;
  struct foreach_at foo = { pt_x, pt_y, fn, user_data };

  g_return_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item));
  g_return_if_fail (fn != NULL);

  geom = guppi_canvas_item_geometry (item);
  if (!guppi_geometry_contains (geom, pt_x, pt_y))
    return;

  guppi_canvas_item_foreach (item, foreach_at_fn, &foo);
}

struct first_matching {
  GuppiCanvasItemTest fn;
  gpointer user_data;
  GuppiCanvasItem *match;
};

static void
find_first_match (GuppiCanvasItem *item, gpointer ptr)
{
  struct first_matching *foo = (struct first_matching *) ptr;

  if (foo->match)
    return;

  if (foo->fn (item, foo->user_data))
    foo->match = item;
}

/**
 * guppi_canvas_item_first_matching_at
 * @item: the canvas item
 * @pt_x: an x-coordinate, in ps-points
 * @pt_y: a y-coordinate, in ps-points
 * @test_func: a callback function
 * @user_data: a points passed to the callback
 *
 * Returns the first #GuppiCanvasItem which is a subitem of @item
 * and for which @test_func returns %TRUE.  If all of the item's subitems
 * return %FALSE from @test_func, %NULL is returned.
 *
 **/
GuppiCanvasItem *
guppi_canvas_item_first_matching_at (GuppiCanvasItem *item,
				     double pt_x, double pt_y,
				     GuppiCanvasItemTest fn,
				     gpointer user_data)
{
  struct first_matching foo = { fn, user_data, NULL };

  guppi_canvas_item_foreach_at (item, pt_x, pt_y, find_first_match, &foo);
  return foo.match;
}

static void
item_list_fn (GuppiCanvasItem *item, gpointer ptr)
{
  GList **lst = (GList **) ptr;
  *lst = g_list_append (*lst, item);
}

static GList *
list_of_items_at (GuppiCanvasItem *item, double pt_x, double pt_y)
{
  GList *lst = NULL;
  guppi_canvas_item_foreach_at (item, pt_x, pt_y, item_list_fn, &lst);
  return lst;
}

/***************************************************************************/

static GuppiCanvasItem *
find_item_supporting_tool (GuppiPlotTool *tool, GList *lst)
{
  g_return_val_if_fail (tool != NULL && GUPPI_IS_PLOT_TOOL (tool), NULL);
  while (lst) {
    GuppiCanvasItem *item = GUPPI_CANVAS_ITEM (lst->data);
    if (guppi_plot_tool_supports_item (tool, item))
      return item;
    lst = g_list_next (lst);
  }
  return NULL;
}

static gboolean
find_tool (GuppiCanvasItem *item, double pt_x, double pt_y,
	   gboolean is_button, guint button_or_key, guint state,
	   GuppiCanvasItem **op_item, GuppiPlotTool **op_tool)
{

  GList *item_list = list_of_items_at (item, pt_x, pt_y);
  GList *iter = item_list;
  gboolean found = FALSE;

  while (iter != NULL && !found) {
    GuppiCanvasItemClass *klass;

    GuppiPlotTool *this_tool;
    GuppiCanvasItem *this_item = GUPPI_CANVAS_ITEM (iter->data);
    GuppiCanvasItemPrivate *p = priv (this_item);
    GuppiCanvasItem *tool_item;

    /* Try the local toolkit */

    if (p->item_toolkit && !p->disable_local_tools) {
      if (is_button)
	this_tool = guppi_plot_toolkit_get_button_tool (p->item_toolkit,
							button_or_key, state);
      else
	this_tool = guppi_plot_toolkit_get_key_tool (p->item_toolkit,
						     button_or_key, state);

      if (this_tool) {
	tool_item = find_item_supporting_tool (this_tool, iter);
	if (tool_item) {
	  if (op_item)
	    *op_item = tool_item;
	  if (op_tool)
	    *op_tool = this_tool;
	  found = TRUE;
	}
      }
    }

    /* OK, no try the class toolkit */

    klass = GUPPI_CANVAS_ITEM_CLASS (GTK_OBJECT (this_item)->klass);
    if (!found && klass->item_class_toolkit && !p->disable_class_tools) {
      if (is_button)
	this_tool =
	  guppi_plot_toolkit_get_button_tool (klass->item_class_toolkit,
					      button_or_key, state);
      else
	this_tool =
	  guppi_plot_toolkit_get_key_tool (klass->item_class_toolkit,
					   button_or_key, state);

      if (this_tool) {
	tool_item = find_item_supporting_tool (this_tool, iter);
	if (tool_item) {
	  if (op_item)
	    *op_item = tool_item;
	  if (op_tool)
	    *op_tool = this_tool;
	  found = TRUE;
	}
      }
    }

    iter = g_list_next (iter);
  }

  g_list_free (item_list);

  return found;
}

static gboolean
drop_candidate_test (GuppiCanvasItem *item, gpointer foo)
{
  GuppiCanvasItemClass *klass;
  klass = GUPPI_CANVAS_ITEM_CLASS (GTK_OBJECT (item)->klass);
  return klass->data_drop != NULL;
}

GuppiCanvasItem *
guppi_canvas_item_data_drop_candidate (GuppiCanvasItem *item,
				       double pt_x, double pt_y)
{
  return guppi_canvas_item_first_matching_at (item, pt_x, pt_y,
					      drop_candidate_test, NULL);
}

gboolean
guppi_canvas_item_data_drop (GuppiCanvasItem *item, GuppiData *data)
{
  GuppiCanvasItemClass *klass;

  g_return_val_if_fail (item && GUPPI_IS_CANVAS_ITEM (item), FALSE);
  g_return_val_if_fail (data && GUPPI_IS_DATA (data), FALSE);

  klass = GUPPI_CANVAS_ITEM_CLASS (GTK_OBJECT (item)->klass);

  return klass->data_drop && klass->data_drop (item, data);
}

gboolean
guppi_canvas_item_locate_button_tool (GuppiCanvasItem *item,
				      double pt_x, double pt_y,
				      guint button, guint state,
				      GuppiCanvasItem **op_item,
				      GuppiPlotTool **op_tool)
{
  g_return_val_if_fail (item && GUPPI_IS_CANVAS_ITEM (item), FALSE);

  return find_tool (item, pt_x, pt_y, TRUE, button, state, op_item, op_tool);
}

gboolean
guppi_canvas_item_locate_key_tool (GuppiCanvasItem *item,
				   double pt_x, double pt_y,
				   guint button, guint state,
				   GuppiCanvasItem **op_item,
				   GuppiPlotTool **op_tool)
{
  g_return_val_if_fail (item && GUPPI_IS_CANVAS_ITEM (item), FALSE);

  return find_tool (item, pt_x, pt_y, FALSE, button, state, op_item, op_tool);
}

gboolean
guppi_canvas_item_double_click (GuppiCanvasItem *item,
				double pt_x, double pt_y,
				guint button, guint state)
{
  GuppiCanvasItemClass *klass;
  GuppiGeometry *geom;

  g_return_val_if_fail (item && GUPPI_IS_CANVAS_ITEM (item), FALSE);

  geom = guppi_canvas_item_geometry (item);

  if (!guppi_geometry_contains (geom, pt_x, pt_y))
    return FALSE;

  klass = GUPPI_CANVAS_ITEM_CLASS (GTK_OBJECT (item)->klass);

  return klass->double_click &&
    klass->double_click (item, button, state, pt_x, pt_y);
}

void
guppi_canvas_item_set_local_toolkit (GuppiCanvasItem *item,
				     GuppiPlotToolkit *tk)
{
  GuppiCanvasItemPrivate *p;

  g_return_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item));
  g_return_if_fail (tk != NULL && GUPPI_IS_PLOT_TOOLKIT (tk));

  p = priv (item);

  guppi_refcounting_assign (p->item_toolkit, tk);
}

void
guppi_canvas_item_enable_local_toolkit (GuppiCanvasItem *item, gboolean x)
{
  GuppiCanvasItemPrivate *p;
  g_return_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item));
  p = priv (item);
  p->disable_local_tools = !x;
}

void
guppi_canvas_item_enable_class_toolkit (GuppiCanvasItem *item, gboolean x)
{
  GuppiCanvasItemPrivate *p;
  g_return_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item));
  p = priv (item);
  p->disable_class_tools = !x;
}

/**************************************************************************/

gboolean guppi_canvas_item_nonlocal_tools_disabled (GuppiCanvasItem *gci)
{
  g_return_val_if_fail (gci != NULL && GUPPI_IS_CANVAS_ITEM (gci), TRUE);
  return priv (gci)->disable_class_tools;
}

GuppiPlotTool *
guppi_canvas_item_local_button_tool (GuppiCanvasItem *item,
				     guint button, guint state)
{
  GuppiCanvasItemPrivate *p;
  GuppiCanvasItemClass *klass;
  GuppiPlotTool *tool = NULL;
  g_return_val_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item), NULL);
  p = priv (item);

  if (p->disable_local_tools)
    return NULL;

  if (p->item_toolkit) {
    tool = guppi_plot_toolkit_get_button_tool (p->item_toolkit,
					       button, state);
  }

  if (tool == NULL) {
    klass = GUPPI_CANVAS_ITEM_CLASS (GTK_OBJECT (item)->klass);
    if (klass->item_class_toolkit) {
      tool = guppi_plot_toolkit_get_button_tool (klass->item_class_toolkit,
						 button, state);
    }
  }

  return (tool && guppi_plot_tool_supports_item (tool, item)) ? tool : NULL;
}

GuppiPlotTool *
guppi_canvas_item_local_key_tool (GuppiCanvasItem *item,
				  guint key, guint state)
{
  GuppiCanvasItemPrivate *p;
  GuppiCanvasItemClass *klass;
  GuppiPlotTool *tool = NULL;
  g_return_val_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item), NULL);
  p = priv (item);

  if (p->disable_local_tools)
    return NULL;

  if (p->item_toolkit) {
    tool = guppi_plot_toolkit_get_key_tool (p->item_toolkit, key, state);
  }

  if (tool == NULL) {
    klass = GUPPI_CANVAS_ITEM_CLASS (GTK_OBJECT (item)->klass);
    if (klass->item_class_toolkit) {
      tool = guppi_plot_toolkit_get_key_tool (klass->item_class_toolkit,
					      key, state);
    }
  }

  return (tool && guppi_plot_tool_supports_item (tool, item)) ? tool : NULL;
}

void
guppi_canvas_item_foreach_class_toolkit (GuppiCanvasItem *item,
					 void (*fn) (GuppiPlotToolkit *, gpointer),
					 gpointer user_data)
{
  GuppiCanvasItemClass *klass;

  g_return_if_fail (item && GUPPI_IS_CANVAS_ITEM (item));
  g_return_if_fail (fn != NULL);

  klass = GUPPI_CANVAS_ITEM_CLASS (GTK_OBJECT (item)->klass);

  if (klass->uses_vp_coordinates) {
    GuppiPlotToolkit *tk;

    tk = guppi_basic_toolkit_zoom ();
    fn (tk, user_data);
    guppi_unref (tk);

    tk = guppi_basic_toolkit_move ();
    fn (tk, user_data);
    guppi_unref (tk);

    tk = guppi_basic_toolkit_reframe ();
    fn (tk, user_data);
    guppi_unref (tk);

    tk = guppi_basic_toolkit_home ();
    fn (tk, user_data);
    guppi_unref (tk);
  }

  if (klass->foreach_class_toolkit)
    klass->foreach_class_toolkit (item, fn, user_data);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void 
perm_wrap (GuppiPlotTool *x, gpointer y)
{
  guppi_permanent_alloc (x);
  guppi_permanent_alloc (x->name);
}

void
guppi_canvas_item_class_set_item_class_toolkit (GuppiCanvasItemClass *klass,
						GuppiPlotToolkit *tk)
{
  g_return_if_fail (GUPPI_IS_CANVAS_ITEM_CLASS (klass));
  g_return_if_fail (GUPPI_IS_PLOT_TOOLKIT (tk));

  klass->item_class_toolkit = tk;

  guppi_plot_toolkit_foreach (tk, perm_wrap, NULL);

  guppi_permanent_alloc (tk);
  guppi_permanent_alloc (tk->name);
}


/* $Id$ */

