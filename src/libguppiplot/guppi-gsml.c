/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-gsml.c
 *
 * GSML == Guppi's Stupid Markup Language
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-gsml.h"

#include <guppi-memory.h>
#include <gnome-xml/parser.h>
#include <gnome-xml/xmlmemory.h>
#include "guppi-text-tokens.h"


static void
push_and_shift_and_scale (GuppiTextBlock *block, double shift, double factor)
{
  guppi_text_block_add (block, guppi_text_token_new_push ());

  if (shift != 0)
    guppi_text_block_add (block, guppi_text_token_new_raise_lower (shift));

  if (factor > 0)
    guppi_text_block_add (block, guppi_text_token_new_resize_font (RESIZE_FONT_SCALED, factor));
}

static void
guppi_text_block_process_text (GuppiTextBlock *block, gchar *str)
{
  gchar **strv = g_strsplit (str, " ", 0);
  gint i = 0;

  while (strv[i]) {
    g_strstrip (strv[i]);
    if (i != 0)
      guppi_text_block_add (block, guppi_text_token_new_space (1.0));
    guppi_text_block_add (block, guppi_text_token_new_word (strv[i]));
    ++i;
  }

  g_strfreev (strv);
}

typedef enum {
  GSML_GSML,
  GSML_SP,
  GSML_BR,
  GSML_NOBR,
  GSML_SCALE,
  GSML_CENTER,
  GSML_LEFT,
  GSML_RIGHT,
  GSML_FILL,
  GSML_SUP,
  GSML_SUB,
  GSML_RAT,
  GSML_SCI,
  GSML_INVALID,
  GSML_LAST
} gsmlTag;

typedef struct {
  gsmlTag code;
  const gchar *str;
  gboolean always_empty;
} gsmlTagInfo;

static gsmlTagInfo tag_info[GSML_LAST] = {
  { GSML_GSML,    "gsml",   FALSE },
  { GSML_SP,      "sp",     TRUE  },  /* space */
  { GSML_BR,      "br",     TRUE  },  /* line break */
  { GSML_NOBR,    "nobr",   FALSE },
  { GSML_SCALE,   "scale",  FALSE },
  { GSML_CENTER,  "center", FALSE },
  { GSML_LEFT,    "left",   FALSE },
  { GSML_RIGHT,   "right",  FALSE },
  { GSML_FILL,    "fill",   FALSE },
  { GSML_SUP,     "sup",    FALSE },
  { GSML_SUB,     "sub",    FALSE },
  { GSML_RAT,     "rat",    TRUE  }, /* rational number */
  { GSML_SCI,     "sci",    TRUE  }, /* scientific notation number */
  { GSML_INVALID, NULL,     FALSE }
};

static void
guppi_text_block_parse_xml (GuppiTextBlock *block, xmlNodePtr node)
{
  const double sup_raise = 0.6;
  const double sup_scale = 0.6;
  const double sub_lower = -0.3;
  const double sub_scale = sup_scale;

  gsmlTag code = GSML_INVALID;
  gchar *prop;
  gint i;

  if (node == NULL)
    return;

  if (xmlNodeIsText (node)) {

    gchar *txt = xmlNodeGetContent (node);
    guppi_text_block_process_text (block, txt);
    xmlFree (txt);

  } else {
    
    for (i = 0; tag_info[i].code != GSML_INVALID; ++i) {
      if (!strcmp (node->name, tag_info[i].str)) {
	code = i;
	break;
      }
    }

    /* Pre-descent processing */
    switch (code) {

    case GSML_GSML:
      /* do nothing */
      break;

    case GSML_SP: {
      double sz = 1.0;
      prop = xmlGetProp (node, "size");
      if (prop)
	sz = atof (prop);
      xmlFree (prop);

      guppi_text_block_add (block, guppi_text_token_new_space (sz));
      break;
    }

    case GSML_BR:
      guppi_text_block_add (block, guppi_text_token_new_hard_break ());
      break;
      
    case GSML_NOBR:
      guppi_text_block_add (block, guppi_text_token_new_push ());
      guppi_text_block_add (block, guppi_text_token_new_nobreak ());
      break;

    case GSML_SCALE: {
      double factor = -1.0;
      prop = xmlGetProp (node, "factor");
      if (prop)
	factor = atof (prop);
      xmlFree (prop);
      
      guppi_text_block_add (block, guppi_text_token_new_push ());

      if (factor > 0)
	guppi_text_block_add (block, guppi_text_token_new_resize_font (RESIZE_FONT_SCALED, factor));
      
      break;
    }
  
    case GSML_CENTER:
      guppi_text_block_add (block, guppi_text_token_new_push ());
      guppi_text_block_add (block, guppi_text_token_new_justify (GTK_JUSTIFY_CENTER));
      break;

    case GSML_LEFT:
      guppi_text_block_add (block, guppi_text_token_new_push ());
      guppi_text_block_add (block, guppi_text_token_new_justify (GTK_JUSTIFY_LEFT));
      break;

    case GSML_RIGHT:
      guppi_text_block_add (block, guppi_text_token_new_push ());
      guppi_text_block_add (block, guppi_text_token_new_justify (GTK_JUSTIFY_RIGHT));
      break;

    case GSML_FILL:
      guppi_text_block_add (block, guppi_text_token_new_push ());
      guppi_text_block_add (block, guppi_text_token_new_justify (GTK_JUSTIFY_FILL));
      break;

    case GSML_SUB: 
      push_and_shift_and_scale (block, sup_raise, sup_scale);
      break;

    case GSML_SUP:
      push_and_shift_and_scale (block, sub_lower, sub_scale);
      break;
      
    case GSML_RAT: {
      gchar *ip, *num, *den;
      const double size = 0.6;
      const double num_raise = 0.4;
      const double den_lower = -0.1;

      ip  = xmlGetProp (node, "integer_part");
      num = xmlGetProp (node, "numerator");
      den = xmlGetProp (node, "denominator");

      if (ip) {
	guppi_text_block_add (block, guppi_text_token_new_word (ip));

	if (num || den)
	  guppi_text_block_add (block, guppi_text_token_new_space (0.3));
      }

      if (num) {

	push_and_shift_and_scale (block, num_raise, size);
	
	guppi_text_block_add (block, guppi_text_token_new_word (num));

	guppi_text_block_add (block, guppi_text_token_new_pop ());
      }
      
      if (num || den)
	guppi_text_block_add (block, guppi_text_token_new_word ("/"));

      if (den) {

	push_and_shift_and_scale (block, den_lower, size);

	guppi_text_block_add (block, guppi_text_token_new_word (den));

	guppi_text_block_add (block, guppi_text_token_new_pop ());
      }
    
      xmlFree (ip);
      xmlFree (num);
      xmlFree (den);

      break;
    }
      
    case GSML_SCI: {
      gchar *mantissa, *exponent, *base;
      gint sig;

      mantissa = xmlGetProp (node, "mantissa");
      exponent = xmlGetProp (node, "exponent");
      base     = xmlGetProp (node, "base");

      prop     = xmlGetProp (node, "significant_digits");
      sig = prop ? atoi (prop) : -1;
      xmlFree (prop);

      guppi_text_block_add (block, guppi_text_token_new_push ());
      guppi_text_block_add (block, guppi_text_token_new_nobreak ());

      guppi_text_block_add (block, guppi_text_token_new_word (mantissa ? mantissa : "1.0"));

      /* Slightly shrink & raise our mult sign */

      guppi_text_block_add (block, guppi_text_token_new_space (0.25));

      push_and_shift_and_scale (block, 0.1, 0.75);
      guppi_text_block_add (block, guppi_text_token_new_word ("x"));
      guppi_text_block_add (block, guppi_text_token_new_pop ());

      guppi_text_block_add (block, guppi_text_token_new_space (0.15));

      guppi_text_block_add (block, guppi_text_token_new_word (base ? base : "10"));

      /* Raise our exponent */

      push_and_shift_and_scale (block, sup_raise, sup_scale);
      guppi_text_block_add (block, guppi_text_token_new_word (exponent ? exponent : "0"));
      guppi_text_block_add (block, guppi_text_token_new_pop ());
      
      guppi_text_block_add (block, guppi_text_token_new_pop ());


      xmlFree (mantissa);
      xmlFree (exponent);
      xmlFree (base);
      
      break;
    }

    case GSML_INVALID:
      g_print ("Unknown GSML tag '%s'\n", node->name);
      break;

    case GSML_LAST:
      g_assert_not_reached ();
      break;
    };
  
    if (! tag_info[code].always_empty)
      guppi_text_block_parse_xml (block, node->xmlChildrenNode);

    /* Post-descent processing */
    switch (code) {

    case GSML_GSML:
    case GSML_SP:
    case GSML_BR:
    case GSML_RAT:
    case GSML_SCI:
    case GSML_INVALID:
      /* do nothing */
      break;
      
    case GSML_NOBR:
    case GSML_SCALE:
    case GSML_CENTER:
    case GSML_LEFT:
    case GSML_RIGHT:
    case GSML_FILL:
    case GSML_SUP:
    case GSML_SUB:
      guppi_text_block_add (block, guppi_text_token_new_pop ());
      break;

    case GSML_LAST:
      g_assert_not_reached ();
      break;
    };

  }

  guppi_text_block_parse_xml (block, node->next);
}

void
guppi_text_block_parse_gsml (GuppiTextBlock *block, const gchar *gsml_str)
{
  xmlDocPtr xml_doc;
  gchar *str;

  str = guppi_strdup_printf ("<gsml>%s</gsml>", gsml_str);
  xml_doc = xmlParseMemory (str, strlen (str));
  guppi_free (str);

  guppi_text_block_freeze (block);
  guppi_text_block_clear (block);
  guppi_text_block_parse_xml (block, xmlDocGetRootElement (xml_doc));
  guppi_text_block_thaw (block);

  xmlFreeDoc (xml_doc);
}

/* $Id$ */
