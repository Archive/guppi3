/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-view-interval.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_VIEW_INTERVAL_H
#define _INC_GUPPI_VIEW_INTERVAL_H

/* #include <gnome.h> */
#include <glib.h>

#include <gtk/gtkobject.h>
#include  "guppi-defs.h"
#include  "guppi-xml.h"
#include  "guppi-unique-id.h"

BEGIN_GUPPI_DECLS

enum {
  GUPPI_VIEW_NORMAL=0,
  GUPPI_VIEW_LOG=1,
  GUPPI_VIEW_LAST
};

typedef struct _GuppiViewInterval GuppiViewInterval;
typedef struct _GuppiViewIntervalClass GuppiViewIntervalClass;

struct _GuppiViewInterval {
  GtkObject parent;

  guppi_uniq_t id;
  gint type;
  double type_arg;

  double t0, t1;

  double min, max;
  double min_width;
  guint include_min : 1;
  guint include_max : 1;

  guint block_changed_signals : 1;
};

struct _GuppiViewIntervalClass {
  GtkObjectClass parent_class;

  /* signals */
  void (*changed) (GuppiViewInterval *);
  void (*preferred_range_request) (GuppiViewInterval *);
};

#define GUPPI_TYPE_VIEW_INTERVAL (guppi_view_interval_get_type())
#define GUPPI_VIEW_INTERVAL(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_VIEW_INTERVAL,GuppiViewInterval))
#define GUPPI_VIEW_INTERVAL0(obj) ((obj) ? (GUPPI_VIEW_INTERVAL(obj)) : NULL)
#define GUPPI_VIEW_INTERVAL_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_VIEW_INTERVAL,GuppiViewIntervalClass))
#define GUPPI_IS_VIEW_INTERVAL(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_VIEW_INTERVAL))
#define GUPPI_IS_VIEW_INTERVAL0(obj) (((obj) == NULL) || (GUPPI_IS_VIEW_INTERVAL(obj)))
#define GUPPI_IS_VIEW_INTERVAL_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_VIEW_INTERVAL))

GtkType guppi_view_interval_get_type (void);

GuppiViewInterval *guppi_view_interval_new (void);

void guppi_view_interval_set (GuppiViewInterval *, double, double);
void guppi_view_interval_grow_to (GuppiViewInterval *, double, double);
void guppi_view_interval_range (GuppiViewInterval *, double *, double *);

void guppi_view_interval_set_bounds (GuppiViewInterval *, double, double);
void guppi_view_interval_clear_bounds (GuppiViewInterval *);
void guppi_view_interval_set_min_width (GuppiViewInterval *, double);

gboolean guppi_view_interval_valid_fn (GuppiViewInterval *, double);
double guppi_view_interval_conv_fn (GuppiViewInterval *, double);
double guppi_view_interval_unconv_fn (GuppiViewInterval *, double);

#define guppi_view_interval_valid(v, x) ((v)->type == GUPPI_VIEW_NORMAL ? TRUE : guppi_view_interval_valid_fn((v),(x)))

#define guppi_view_interval_conv(v, x) ((v)->type == GUPPI_VIEW_NORMAL ? ((x)-(v)->t0)/((v)->t1-(v)->t0) : guppi_view_interval_conv_fn((v),(x)))

#define guppi_view_interval_unconv(v, x) ((v)->type == GUPPI_VIEW_NORMAL ? (v)->t0 + (x)*((v)->t1-(v)->t0) : guppi_view_interval_unconv_fn((v),(x)))


void guppi_view_interval_conv_bulk (GuppiViewInterval * vi,
				    const double *, double *, gsize);
void guppi_view_interval_unconv_bulk (GuppiViewInterval * vi,
				      const double *, double *, gsize);


void guppi_view_interval_rescale_around_point (GuppiViewInterval *, double t, double scale);
void guppi_view_interval_recenter_around_point (GuppiViewInterval *, double);
void guppi_view_interval_translate (GuppiViewInterval *, double);

void guppi_view_interval_conv_translate (GuppiViewInterval *, double);

void guppi_view_interval_request_preferred_range (GuppiViewInterval *);


void guppi_view_interval_scale_linearly (GuppiViewInterval *);
void guppi_view_interval_scale_logarithmically (GuppiViewInterval *, double base);

gboolean guppi_view_interval_is_logarithmic (const GuppiViewInterval *);
double guppi_view_interval_logarithm_base (const GuppiViewInterval *);


xmlNodePtr         guppi_view_interval_export_xml (GuppiViewInterval *, GuppiXMLDocument *doc);
GuppiViewInterval *guppi_view_interval_import_xml (GuppiXMLDocument *doc, xmlNodePtr node);


END_GUPPI_DECLS

#endif /* _INC_GUPPI_VIEW_INTERVAL_H */

/* $Id$ */
