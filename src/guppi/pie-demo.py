from Guppi import *
from Guppi.plot import *
import Guppi

pie_styles = GuppiSeqStyle()
pie_data = GuppiSeqScalar()
pie_labels = GuppiSeqString()

pie_data_numbers = (1500, 500, 150, 600, 350, 450, 120)
colors = ('red', 'blue', 'orange', 'green', 'purple', 'goldenrod', 'pink')
labels = ('Rent', 'Food', 'Utilities', 'Car', 'Pinball', 'Beer', 'Pork Rinds')


def init():
    for x in pie_data_numbers:
        pie_data.append(x)

    for x in colors:
        sty = GuppiStyle()
        sty.set_color(x)
        pie_styles.append(sty)

    for x in labels:
        pie_labels.append(x)
    
def pie_demo():
    root_state = GuppiRootGroupState()
    root_view = root_state.make_view()

    pie_state = GuppiPieState()
    legend_state = GuppiLegendState()
    text_state = GuppiTextState()
    
    pie_view = pie_state.make_view()
    legend_view = legend_state.make_view()
    text_view = text_state.make_view()

    root_state.set_label("Silly Pie Demo")
    pie_state.set_label("Pie Chart")
    legend_state.set_label("Legend")
    text_state.set_label("Title")

    root_view.set_size(72 * 6, 72 * 4.5)

    pie_state.set_data(pie_data)
    pie_state.set_styles(pie_styles)
    pie_state.set_labels(pie_labels)

    pie_state.set_radius_maximize(1)

    text_state.set_text('Monthly Spending')
    text_state.set_font_size(24)

    pie_state.connect('style-data', legend_state, 'style-data')
    pie_state.connect('label-data', legend_state, 'label-data')

    # do layout

    root_view.layout_flush_top(text_view, 7.2)
    root_view.layout_horizontal_fill(text_view, 0, 0)
    root_view.layout_natural_height(text_view)

    root_view.layout_flush_bottom(pie_view, 0)
    root_view.layout_vertically_adjacent(text_view, pie_view, 0)
    root_view.layout_horizontally_adjacent(pie_view, legend_view, 0)
    root_view.layout_flush_left(pie_view, 0)

    root_view.layout_natural_height(legend_view)
    root_view.layout_natural_width(legend_view)
    root_view.layout_flush_right(legend_view, 0)
    root_view.layout_same_y_center(pie_view, legend_view)

    root_view.show_in_window()

    return pie_state
    
if __name__ == '__main__':
    init()
    pie_demo()
