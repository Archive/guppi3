from Guppi import *
from Guppi.plot import *

from math import sin

def view_fully_connect(a, b):
    a.connect("x-view", b, "x-view")
    a.connect("y-view", b, "y-view")
    a.connect("x-markers", b, "x-markers")
    a.connect("y-markers", b, "y-markers")
    
x_data = GuppiSeqScalar()
y_data = GuppiSeqScalar()
c_data = GuppiSeqScalar()
sty_data = GuppiSeqInteger()

def iter(i):
    x_data.append(i)
    y_data.append(sin(i) * 5 * (i*i + 1))
    c_data.append(i)
    if (i < 50): iter(i + 0.1)

iter(-50)

def plot_demo():
    scatter_state = GuppiScatterState()
    frame_state = GuppiFrameState()
    line_state = GuppiLinegraphState()
    n_ax_state = GuppiAxisState()
    s_ax_state = GuppiAxisState()
    e_ax_state = GuppiAxisState()
    w_ax_state = GuppiAxisState()
    title_state = GuppiTextState()
    x_boxplot_state = GuppiBoxplotState()
    y_boxplot_state = GuppiBoxplotState()

    scatter_view = scatter_state.make_view()
    frame_view = frame_state.make_view()
    line_view = line_state.make_view()
    n_ax_view = n_ax_state.make_view()
    s_ax_view = s_ax_state.make_view()
    e_ax_view = e_ax_state.make_view()
    w_ax_view = w_ax_state.make_view()
    title_view = title_state.make_view()
    x_boxplot_view = x_boxplot_state.make_view()
    y_boxplot_view = y_boxplot_state.make_view()

    plot = GuppiPlot()

    gap = 3.6

    title_state.set_text("Guppi: A Gnome Plotting Tool")
    title_state.set_font_size(20)

    x_boxplot_state.set_horizontal(1)
    y_boxplot_state.set_vertical(1)

    line_state.set_x_data(x_data)
    line_state.set_y_data(y_data)

    scatter_state.connect("x-data", line_state, "x-data")
    scatter_state.connect("y-data", line_state, "y-data")
    scatter_state.set_size2_gradient_data(c_data)
    scatter_state.set_style_data(sty_data)

    ss = scatter_state.get_style(0)
    ss.set_marker_size1(5)

    x_boxplot_state.connect("misc-data", line_state, "x-data")
    y_boxplot_state.connect("misc-data", line_state, "y-data")

    n_ax_state.set_position("north")
    s_ax_state.set_position("south")
    e_ax_state.set_position("east")
    w_ax_state.set_position("west")

    view_fully_connect(scatter_view, frame_view)
    view_fully_connect(scatter_view, line_view)

    scatter_view.connect("x-view", n_ax_view, "x-view")
    scatter_view.connect("x-view", s_ax_view, "x-view")
    scatter_view.connect("y-view", e_ax_view, "y-view")
    scatter_view.connect("y-view", w_ax_view, "y-view")

    scatter_view.connect("x-markers", n_ax_view, "x-markers")
    scatter_view.connect("x-markers", s_ax_view, "x-markers")
    scatter_view.connect("y-markers", e_ax_view, "y-markers")
    scatter_view.connect("y-markers", w_ax_view, "y-markers")

    scatter_view.connect("x-view", x_boxplot_view, "x-view")
    scatter_view.connect("y-view", y_boxplot_view, "y-view")

    scatter_view.set_preferred_view()

    plot.set_size(72 * 7, 72 * 5)

#   do layout

    plot.layout_flush_top(title_view, 7.2)
    plot.layout_horizontal_fill(title_view, 0, 0)
    plot.layout_natural_height(title_view)

    plot.layout_same_place(frame_view, line_view)
    plot.layout_same_place(frame_view, scatter_view)

    plot.layout_natural_height(x_boxplot_view)
    plot.layout_natural_width(y_boxplot_view)

    plot.layout_vertically_adjacent(title_view, x_boxplot_view, 7.2)

    plot.layout_vbox2(x_boxplot_view, n_ax_view, 7.2)
    plot.layout_hbox2(e_ax_view, y_boxplot_view, 7.2)

    plot.layout_hbox3(w_ax_view, scatter_view,e_ax_view, 7.2)
    plot.layout_vbox3(n_ax_view, scatter_view, s_ax_view, 7.2)

    plot.layout_flush_left(w_ax_view, 7.2)
    plot.layout_flush_right(y_boxplot_view, 7.2)
    plot.layout_flush_bottom(s_ax_view, 7.2)

    plot.layout_height_relative(n_ax_view, 0.10)
    plot.layout_height_relative(s_ax_view, 0.10)
    plot.layout_width_relative(e_ax_view, 0.10)
    plot.layout_width_relative(w_ax_view, 0.10)

    #plot.print_ps_to_file("foo.ps")
    plot.show_in_window()

print "Running demo..."
plot_demo()
print "Done"
