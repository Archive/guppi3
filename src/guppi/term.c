/* $Id$ */

/*
 * term.c
 *
 * Copyright (C) 1999 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 * and Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
/* #include <gnome.h> */
#include <sys/types.h>
#include <zvt/zvtterm.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <guppi-paths.h>
#include "corba_guppi.h"
#include "term.h"

static GtkWidget *dialog = NULL;
static GtkWidget *term = NULL;

static void
close_guile_terminal_inner (gboolean already_destroyed)
{
  if (dialog != NULL && !already_destroyed)
    gtk_widget_destroy (dialog);
  dialog = NULL;
  term = NULL;
}

/*
static void
close_cb(GtkWidget* w, gpointer user_data)
{
  close_guile_terminal_inner(TRUE);
}
*/

static gboolean
delete_event_cb (GtkWidget * w, gpointer user_data)
{
  close_guile_terminal_inner (TRUE);
  return FALSE;
}

static void
child_died_cb (GtkWidget * w, gpointer user_data)
{
  /* make sure stuff gets on screen or something, i dunno */
  while (gtk_events_pending ())
    gtk_main_iteration ();

  sleep (1);			/* just long enough to read any errors  */

  close_guile_terminal_inner (FALSE);
}

void
guile_terminal (void)
{
  GtkWidget *scrollbar;
  GtkWidget *hbox;
  gint pid;
  const gchar *path;

  if (dialog != NULL) {
    gdk_window_show (dialog->window);
    gdk_window_raise (dialog->window);
    return;
  }

  /* Before we open the terminal, we should make sure that we have
     somewhere to go. */
  path = gnome_guile_repl_path ();
  if (path == NULL) {
    g_warning ("Couldn't run gnome-guile-repl.");
    return;
  }

  dialog = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (dialog),
			_("Guppi: Interactive Guile Terminal"));

  term = zvt_term_new_with_size (80, 25);
  zvt_term_set_scrollback (ZVT_TERM (term), 10000);
  zvt_term_set_scroll_on_keystroke (ZVT_TERM (term), TRUE);
  zvt_term_set_scroll_on_output (ZVT_TERM (term), TRUE);

  /*
     gtk_signal_connect(GTK_OBJECT(dialog),
     "close",
     GTK_SIGNAL_FUNC(close_cb),
     NULL);
   */
  gtk_signal_connect (GTK_OBJECT (dialog),
		      "delete_event",
		      GTK_SIGNAL_FUNC (delete_event_cb), NULL);
  gtk_signal_connect (GTK_OBJECT (term),
		      "child_died", GTK_SIGNAL_FUNC (child_died_cb), NULL);

  scrollbar =
    gtk_vscrollbar_new (GTK_ADJUSTMENT (ZVT_TERM (term)->adjustment));
  GTK_WIDGET_UNSET_FLAGS (scrollbar, GTK_CAN_FOCUS);

  hbox = gtk_hbox_new (FALSE, 0);

  gtk_box_pack_start (GTK_BOX (hbox), scrollbar, FALSE, FALSE, 0);
  gtk_box_pack_end (GTK_BOX (hbox), term, TRUE, TRUE, 0);

  gtk_container_add (GTK_CONTAINER (dialog), hbox);

  /* Hack around term weirdness */
  gtk_widget_realize (term);
  gtk_widget_show_now (term);

  while (gtk_events_pending ())
    gtk_main_iteration ();

  usleep (100);

  gtk_widget_show_all (dialog);
  gtk_widget_show_now (dialog);

  while (gtk_events_pending ())
    gtk_main_iteration ();

  usleep (100);

  while (gtk_events_pending ())
    gtk_main_iteration ();

  /* Make sure we are all synced up here. weirdo paranoia. */
  fflush (stdout);
  fflush (stderr);



  pid = zvt_term_forkpty (ZVT_TERM (term), FALSE);	/* FALSE == no utmp */

  if (pid == -1) {
    g_error ("Error: unable to fork");
  }

  if (pid != 0) {		/* the parent */
    gtk_widget_show_all (dialog);
    while (gtk_events_pending ())
      gtk_main_iteration ();

    /* Hack to get around bug with term scrolling. 
       Thanks to Mike Zucchi for the tip. */
    gtk_widget_queue_resize (GTK_WIDGET (term));

    return;
  }

  /* This is what the child does */

  /* touching the GUI == flaming death in the child */
  dialog = NULL;
  term = NULL;

  {
    gchar *const argv[3] = { (gchar * const) path,
      (gchar * const) guppi_guile_ior ()
    };
    gint err = 0;

    err = execv (path, argv);

    if (err != 0)
      g_error ("Failed to execute %s", path);

    fflush (stderr);
    sleep (10);
    _exit (1);
  }

}

void
close_guile_terminal (void)
{
  close_guile_terminal_inner (FALSE);
}



/* $Id$ */
