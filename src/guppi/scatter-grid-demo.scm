;; $Id: scatter-grid-demo.scm,v 1.2 2000/12/19 18:57:11 trow Exp $

(define (guppi-element-view-x-connect a b)
  (guppi-element-view-connect a "x-view" b "x-view")
  (guppi-element-view-connect a "x-markers" b "x-markers")
  )

(define (guppi-element-view-y-connect a b)
  (guppi-element-view-connect a "y-view" b "y-view")
  (guppi-element-view-connect a "y-markers" b "y-markers")
  )

(define (guppi-element-view-xy-connect a b)
  (guppi-element-view-x-connect a b)
  (guppi-element-view-y-connect a b)
)

(define (guppi-element-state-x-connect s1 s2 ax)
  (guppi-element-state-connect s1 "x-data" s2 "x-data")
  (guppi-element-state-connect s1 "x-data" ax "misc-data")
  )

(define (guppi-element-state-y-connect s1 s2 ax)
  (guppi-element-state-connect s1 "y-data" s2 "y-data")
  (guppi-element-state-connect s1 "y-data" ax "misc-data")
  )


(let* (
       
       (root-state (guppi-root-group-state-new))
       (root-view  (guppi-element-state-make-view root-state))

       (scatter-1-state (guppi-scatter-state-new))
       (scatter-2-state (guppi-scatter-state-new))
       (scatter-3-state (guppi-scatter-state-new))
       (scatter-4-state (guppi-scatter-state-new))

       (frame-1-state (guppi-frame-state-new))
       (frame-2-state (guppi-frame-state-new))
       (frame-3-state (guppi-frame-state-new))
       (frame-4-state (guppi-frame-state-new))

       (w-ax-1-state (guppi-axis-state-new))
       (w-ax-2-state (guppi-axis-state-new))
       (s-ax-1-state (guppi-axis-state-new))
       (s-ax-2-state (guppi-axis-state-new))

       (scatter-1-view (guppi-element-state-make-view scatter-1-state))
       (scatter-2-view (guppi-element-state-make-view scatter-2-state))
       (scatter-3-view (guppi-element-state-make-view scatter-3-state))
       (scatter-4-view (guppi-element-state-make-view scatter-4-state))

       (frame-1-view (guppi-element-state-make-view frame-1-state))
       (frame-2-view (guppi-element-state-make-view frame-2-state))
       (frame-3-view (guppi-element-state-make-view frame-3-state))
       (frame-4-view (guppi-element-state-make-view frame-4-state))

       (w-ax-1-view (guppi-element-state-make-view w-ax-1-state))
       (w-ax-2-view (guppi-element-state-make-view w-ax-2-state))
       (s-ax-1-view (guppi-element-state-make-view s-ax-1-state))
       (s-ax-2-view (guppi-element-state-make-view s-ax-2-state))

       (gap 3.2)
       
       )

  (guppi-axis-state-set-position w-ax-1-state 'west)
  (guppi-axis-state-set-position w-ax-2-state 'west)
  (guppi-axis-state-set-position s-ax-1-state 'south)
  (guppi-axis-state-set-position s-ax-2-state 'south)

  (guppi-element-view-set-x-axis-marker-type scatter-1-view 1)
  (guppi-element-view-set-y-axis-marker-type scatter-1-view 1)

  (guppi-element-view-set-x-axis-marker-type scatter-2-view 1)
  (guppi-element-view-set-y-axis-marker-type scatter-2-view 1)

  (guppi-element-view-set-x-axis-marker-type scatter-3-view 1)
  (guppi-element-view-set-y-axis-marker-type scatter-3-view 1)

  (guppi-element-view-set-x-axis-marker-type scatter-4-view 1)
  (guppi-element-view-set-y-axis-marker-type scatter-4-view 1)

  (guppi-element-view-x-connect s-ax-1-view scatter-1-view)
  (guppi-element-view-x-connect scatter-1-view scatter-3-view)

  (guppi-element-view-x-connect s-ax-2-view scatter-2-view)
  (guppi-element-view-x-connect scatter-2-view scatter-4-view)

  (guppi-element-view-y-connect w-ax-1-view scatter-1-view)
  (guppi-element-view-y-connect scatter-1-view scatter-2-view)

  (guppi-element-view-y-connect w-ax-2-view scatter-3-view)
  (guppi-element-view-y-connect scatter-3-view scatter-4-view)

  (guppi-element-view-xy-connect scatter-1-view frame-1-view)
  (guppi-element-view-xy-connect scatter-2-view frame-2-view)
  (guppi-element-view-xy-connect scatter-3-view frame-3-view)
  (guppi-element-view-xy-connect scatter-4-view frame-4-view)

  (guppi-element-state-x-connect scatter-1-state scatter-3-state 
				 s-ax-1-state)

  (guppi-element-state-x-connect scatter-2-state scatter-4-state 
				 s-ax-2-state)

  (guppi-element-state-y-connect scatter-1-state scatter-2-state 
				 w-ax-1-state)

  (guppi-element-state-y-connect scatter-3-state scatter-4-state 
				 w-ax-2-state)


  ;; do layout

  (guppi-group-view-layout-hbox3 root-view 
				 w-ax-1-view scatter-1-view scatter-2-view
				 gap)
  (guppi-group-view-layout-hbox3 root-view 
				 w-ax-2-view scatter-3-view scatter-4-view
				 gap)
  (guppi-group-view-layout-vbox3 root-view
				 scatter-1-view scatter-3-view s-ax-1-view
				 gap)
  (guppi-group-view-layout-vbox3 root-view
				 scatter-2-view scatter-4-view s-ax-2-view
				 gap)

  (guppi-group-view-layout-flush-left root-view w-ax-1-view gap)
  (guppi-group-view-layout-flush-right root-view scatter-2-view gap)
  (guppi-group-view-layout-flush-top root-view scatter-1-view gap)
  (guppi-group-view-layout-flush-bottom root-view s-ax-1-view gap)

  (guppi-group-view-layout-same-place root-view scatter-1-view frame-1-view)
  (guppi-group-view-layout-same-place root-view scatter-2-view frame-2-view)
  (guppi-group-view-layout-same-place root-view scatter-3-view frame-3-view)
  (guppi-group-view-layout-same-place root-view scatter-4-view frame-4-view)

  (guppi-group-view-layout-same-width root-view
				      scatter-1-view scatter-2-view)

  (guppi-group-view-layout-same-height root-view
				      scatter-1-view scatter-3-view)

  (guppi-group-view-layout-natural-width root-view w-ax-1-view)
  (guppi-group-view-layout-natural-width root-view w-ax-2-view)
  (guppi-group-view-layout-natural-height root-view s-ax-1-view)
  (guppi-group-view-layout-natural-height root-view s-ax-2-view)

  (guppi-root-group-view-set-size root-view (* 72 7.5) (* 72 7.5))
  (guppi-root-group-view-show-in-window root-view)

  )



    