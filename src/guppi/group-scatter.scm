(define (guppi-element-view-fully-connect a b)
  (guppi-element-view-connect a "x-view" b "x-view")
  (guppi-element-view-connect a "y-view" b "y-view")
  (guppi-element-view-connect a "x-markers" b "x-markers")
  (guppi-element-view-connect a "y-markers" b "y-markers"))

(define (guppi-plot-demo)

  (let* (
	 
	 (root-state (guppi-root-group-state-new))
	 (scatter-state (guppi-scatter-state-new))
	 (frame-state (guppi-frame-state-new))
	 (slinreg-state (guppi-slinreg-state-new))
	 (n-ax-state (guppi-axis-state-new))
	 (s-ax-state (guppi-axis-state-new))
	 (e-ax-state (guppi-axis-state-new))
	 (w-ax-state (guppi-axis-state-new))
	 (title-state (guppi-text-state-new))
	 (x-boxplot-state (guppi-boxplot-state-new))
	 (y-boxplot-state (guppi-boxplot-state-new))

	 (root-view (guppi-element-state-make-view root-state))
	 (scatter-view (guppi-element-state-make-view scatter-state))
	 (frame-view (guppi-element-state-make-view frame-state))
	 (slinreg-view (guppi-element-state-make-view slinreg-state))
	 (n-ax-view (guppi-element-state-make-view n-ax-state))
	 (s-ax-view (guppi-element-state-make-view s-ax-state))
	 (e-ax-view (guppi-element-state-make-view e-ax-state))
	 (w-ax-view (guppi-element-state-make-view w-ax-state))
	 (title-view (guppi-element-state-make-view title-state))
	 (x-boxplot-view (guppi-element-state-make-view x-boxplot-state))
	 (y-boxplot-view (guppi-element-state-make-view y-boxplot-state))

	 (gap 3.6)
	 
	 )

    (guppi-element-state-set-label root-state "A Silly Demo")
    (guppi-element-state-set-label scatter-state "Scatter Plot")
    (guppi-element-state-set-label title-state "Title")
    (guppi-element-state-set-label slinreg-state "Linear Regression")

    (guppi-text-state-set-text title-state "Guppi: A Gnome Plotting Tool")
    (guppi-text-state-set-font-size title-state 20)
    
    (guppi-boxplot-state-set-horizontal x-boxplot-state #t)
    (guppi-boxplot-state-set-vertical y-boxplot-state #t)

    (guppi-element-state-connect scatter-state "x-data" slinreg-state "x-data")
    (guppi-element-state-connect scatter-state "y-data" slinreg-state "y-data")

    (let ((ss (guppi-scatter-state-get-style scatter-state 0)))
	
      (guppi-style-set-marker-size1 ss 5))

    (guppi-element-state-connect x-boxplot-state "misc-data"
				 scatter-state "x-data")

    (guppi-element-state-connect n-ax-state "misc-data"
				 scatter-state "x-data")

    (guppi-element-state-connect s-ax-state "misc-data"
				 scatter-state "x-data")

    (guppi-element-state-connect y-boxplot-state "misc-data"
				 scatter-state "y-data")

    (guppi-element-state-connect w-ax-state "misc-data"
				 scatter-state "y-data")

    (guppi-element-state-connect e-ax-state "misc-data"
				 scatter-state "y-data")


    (guppi-axis-state-set-position n-ax-state 'north)
    (guppi-axis-state-set-position s-ax-state 'south)
    (guppi-axis-state-set-position e-ax-state 'east)
    (guppi-axis-state-set-position w-ax-state 'west)

    ;; axis default for show-legend is #t
    ;(guppi-axis-state-set-show-legend n-ax-state #f)
    ;(guppi-axis-state-set-show-legend e-ax-state #f)

    (guppi-element-view-fully-connect scatter-view frame-view)
    (guppi-element-view-fully-connect scatter-view slinreg-view)

    (guppi-element-view-connect scatter-view "x-view" n-ax-view "x-view")
    (guppi-element-view-connect scatter-view "x-view" s-ax-view "x-view")
    (guppi-element-view-connect scatter-view "y-view" e-ax-view "y-view")
    (guppi-element-view-connect scatter-view "y-view" w-ax-view "y-view")

    (guppi-element-view-connect scatter-view "x-markers" n-ax-view "x-markers")
    (guppi-element-view-connect scatter-view "x-markers" s-ax-view "x-markers")
    (guppi-element-view-connect scatter-view "y-markers" e-ax-view "y-markers")
    (guppi-element-view-connect scatter-view "y-markers" w-ax-view "y-markers")

    (guppi-element-view-connect scatter-view "x-view" x-boxplot-view "x-view")
    (guppi-element-view-connect scatter-view "y-view" y-boxplot-view "y-view")

    (guppi-element-view-set-preferred-view scatter-view)

    ;;; do layout

    (guppi-group-view-layout-flush-top root-view title-view gap)
    (guppi-group-view-layout-horizontal-fill root-view title-view 0 0)
    (guppi-group-view-layout-natural-height root-view title-view)

    (guppi-group-view-layout-same-place root-view frame-view slinreg-view)
    (guppi-group-view-layout-same-place root-view frame-view scatter-view)

    (guppi-group-view-layout-natural-height root-view x-boxplot-view)
    (guppi-group-view-layout-natural-width root-view y-boxplot-view)

    (guppi-group-view-layout-vertically-adjacent root-view title-view x-boxplot-view gap)

    (guppi-group-view-layout-vbox2 root-view x-boxplot-view n-ax-view gap)
    (guppi-group-view-layout-hbox2 root-view e-ax-view y-boxplot-view gap)


    (guppi-group-view-layout-hbox3 root-view w-ax-view scatter-view e-ax-view gap)
    (guppi-group-view-layout-vbox3 root-view n-ax-view scatter-view s-ax-view gap)

    (guppi-group-view-layout-flush-left root-view w-ax-view gap)
    (guppi-group-view-layout-flush-right root-view y-boxplot-view gap)
    (guppi-group-view-layout-flush-bottom root-view s-ax-view gap)

    (guppi-group-view-layout-natural-height root-view n-ax-view)
    (guppi-group-view-layout-natural-height root-view s-ax-view)
    (guppi-group-view-layout-natural-width root-view w-ax-view)
    (guppi-group-view-layout-natural-width root-view e-ax-view)

    (guppi-group-view-commit-changes root-view)
    
    ;(guppi-element-view-print-ps-to-file root-view "foo.ps")
    (guppi-root-group-view-show-in-window root-view)	
    
    (cons root-view w-ax-view)
    )
)

(define atxt-state (guppi-text-state-new))
(define atxt-view (guppi-element-state-make-view atxt-state))
(guppi-text-state-set-text atxt-state "Aieee!!!")

(define xxx (guppi-plot-demo))
(define root-view (car xxx))
(define ax-view (cdr xxx))

;(guppi-open-guile-terminal)

(guppi-group-view-replace root-view ax-view atxt-view)
(guppi-group-view-commit-changes root-view)


