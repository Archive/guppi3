;;; $Id: barchart-demo.scm,v 1.8 2000/12/17 05:24:55 trow Exp $

(define barchart-data-list1 '(5 3 10 3 7 9 12))
(define barchart-data-list2 '(4 7  4 2 9 3 8))
(define barchart-data-list3 '(8 3  9 8 2 5 11))
(define barchart-row-labels '("Chicago"
			      "New York"
			      "New Delhi"
			      "Sidney"
			      "London"
			      "Tokyo"
			      "Beijing"))
(define barchart-column-labels '("One" "Two" "Three"))
(define barchart-column-colors '("red" "green" "lightblue"))


(define (list->scalar-data data-list)
  (let ((data (guppi-seq-scalar-new)))
    (for-each (lambda (x) (guppi-seq-scalar-append data x)) data-list)
    data))

(define (list->string-data data-list)
  (let ((data (guppi-seq-string-new)))
    (for-each (lambda (x) (guppi-seq-string-append data x)) data-list)
    data))

(define (list->style-data data-list)
  (let ((data (guppi-seq-style-new)))
    (for-each (lambda (x)
		(let ((sty (guppi-style-new)))
		  (guppi-style-set-color sty x)
		  (guppi-seq-style-append data sty))) data-list)
    data))






(let* (

       (barchart-data1 (list->scalar-data barchart-data-list1))
       (barchart-data2 (list->scalar-data barchart-data-list2))
       (barchart-data3 (list->scalar-data barchart-data-list3))
       (barchart-data-rl (list->string-data barchart-row-labels))
       (barchart-data-cl (list->string-data barchart-column-labels))
       (barchart-data-sty (list->style-data barchart-column-colors))

       (barchart-data (guppi-seq-data-new))
       
       (root-state (guppi-root-group-state-new))
       (root-view  (guppi-element-state-make-view root-state))

       (text-state      (guppi-text-state-new)) 
       (more-text-state (guppi-text-state-new)) 
       (barchart-state  (guppi-barchart-state-new))
       (frame-state     (guppi-frame-state-new))

       (w-ax-state      (guppi-axis-state-new))
       (s-ax-state      (guppi-axis-state-new))
       (legend-state    (guppi-legend-state-new))

       (text-view      (guppi-element-state-make-view text-state))
       (more-text-view (guppi-element-state-make-view more-text-state))
       (barchart-view  (guppi-element-state-make-view barchart-state))
       (frame-view     (guppi-element-state-make-view frame-state))
       (w-ax-view      (guppi-element-state-make-view w-ax-state))
       (s-ax-view      (guppi-element-state-make-view s-ax-state))
       (legend-view    (guppi-element-state-make-view legend-state))

       (gap 7.2)
       )

  (guppi-seq-data-append barchart-data barchart-data1)
  (guppi-seq-data-append barchart-data barchart-data2)
  (guppi-seq-data-append barchart-data barchart-data3)

  (guppi-element-state-set-label root-state "Bar Chart Demo")
  (guppi-element-state-set-label text-state "Title")
  (guppi-element-state-set-label more-text-state "Subtitle")
  (guppi-element-state-set-label barchart-state "Bar Chart")
  (guppi-element-state-set-label w-ax-state "y-axis")
  (guppi-element-state-set-label s-ax-state "x-axis")
  (guppi-element-state-set-label legend-state "Legend")

  (guppi-root-group-view-set-size root-view (* 72 7) (* 72 5))

  (guppi-text-state-set-text text-state "Guppi Makes Graphs For Gnome")
  (guppi-text-state-set-font-size text-state 24)

  (guppi-text-state-set-text more-text-state
			     "Total World Domination, One Desktop at a Time")
  (guppi-text-state-set-font-size more-text-state 18)

  (guppi-barchart-state-set-data barchart-state barchart-data)
  (guppi-barchart-state-set-row-labels barchart-state barchart-data-rl)
  (guppi-barchart-state-set-column-labels barchart-state barchart-data-cl)
  (guppi-barchart-state-set-column-styles barchart-state barchart-data-sty)

  (guppi-element-state-connect barchart-state "style-data"
			       legend-state "style-data")
  (guppi-element-state-connect barchart-state "label-data"
			       legend-state "label-data")
  

  (guppi-element-view-connect s-ax-view "x-view"
			      barchart-view "x-view")
  (guppi-element-view-connect s-ax-view "x-markers"
			      barchart-view "x-markers")

  (guppi-element-view-connect w-ax-view "y-view"
			      barchart-view "y-view")
  (guppi-element-view-connect w-ax-view "y-markers"
			      barchart-view "y-markers")

  (guppi-element-view-connect frame-view "y-view"
			      barchart-view "y-view")
  (guppi-element-view-connect frame-view "y-markers"
			      barchart-view "y-markers")

  (guppi-axis-state-set-position w-ax-state 'west)

  (guppi-axis-state-set-position s-ax-state 'south)


  (guppi-element-view-set-preferred-view barchart-view)

  ;; do layout

  (guppi-group-view-layout-same-place root-view frame-view barchart-view)

  (guppi-group-view-layout-natural-width root-view legend-view)
  (guppi-group-view-layout-natural-height root-view legend-view)
  (guppi-group-view-layout-flush-right root-view legend-view 0)
  (guppi-group-view-layout-same-y-center root-view legend-view barchart-view)

  (guppi-group-view-layout-horizontal-fill root-view text-view gap gap)
  (guppi-group-view-layout-horizontal-fill root-view more-text-view gap gap)

  (guppi-group-view-layout-natural-height root-view text-view)
  (guppi-group-view-layout-natural-height root-view more-text-view)

  (guppi-group-view-layout-flush-top root-view text-view gap)
  (guppi-group-view-layout-vertically-adjacent root-view
					       text-view more-text-view 0)

  (guppi-group-view-layout-hbox2 root-view w-ax-view barchart-view 0)
  (guppi-group-view-layout-flush-left root-view w-ax-view gap)
  (guppi-group-view-layout-natural-width root-view w-ax-view)

  (guppi-group-view-layout-horizontally-adjacent root-view 
						 barchart-view legend-view gap)

  (guppi-group-view-layout-vbox2 root-view barchart-view s-ax-view 0)
  (guppi-group-view-layout-natural-height root-view s-ax-view)
  (guppi-group-view-layout-flush-bottom root-view s-ax-view gap)

  (guppi-group-view-layout-vertically-adjacent root-view more-text-view
					       barchart-view gap)
;  (guppi-group-view-layout-flush-bottom root-view barchart-view gap)

  (guppi-root-group-view-show-in-window root-view)

  )



