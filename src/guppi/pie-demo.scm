
(let* (

       (pie-data (guppi-seq-scalar-new))
       (pie-data-numbers '(1500 500 150 600 350 450 120))

       (pie-styles (guppi-seq-style-new))
       (pie-style-colors '("red" "blue" "orange" "green" "purple"
			   "goldenrod" "pink"))

       (pie-labels (guppi-seq-string-new))
       (pie-labels-text '("Rent" "Food" "Utilities" "Car" "Pinball"
			  "Beer" "Pork Rinds"))

       (root-state (guppi-root-group-state-new))
       (root-view  (guppi-element-state-make-view root-state))

       (pie-state (guppi-pie-state-new))
       (legend-state (guppi-legend-state-new))
       (text-state (guppi-text-state-new))

       (pie-view (guppi-element-state-make-view pie-state))
       (legend-view (guppi-element-state-make-view legend-state))
       (text-view (guppi-element-state-make-view text-state))

       )

  (for-each (lambda (x) (guppi-seq-scalar-append pie-data x))
	    pie-data-numbers)

  (for-each (lambda (x) 
	      (let ((sty (guppi-style-new)))
		(guppi-style-set-color sty x)
		(guppi-seq-style-append pie-styles sty)))
	    pie-style-colors)

  (for-each (lambda (x) (guppi-seq-string-append pie-labels x))
	    pie-labels-text)

  (guppi-element-state-set-label root-state "Silly Pie Demo")
  (guppi-element-state-set-label pie-state "Pie Chart")
  (guppi-element-state-set-label legend-state "Legend")
  (guppi-element-state-set-label text-state "Title")

  (guppi-root-group-view-set-size root-view (* 72 6) (* 72 4.5))

  (guppi-pie-state-set-data pie-state pie-data)
  (guppi-pie-state-set-styles pie-state pie-styles)
  (guppi-pie-state-set-labels pie-state pie-labels)

  (guppi-pie-state-set-radius-maximize pie-state #t)

  (guppi-text-state-set-text text-state "Monthly Spending")
  (guppi-text-state-set-font-size text-state 24)

  (guppi-element-state-connect pie-state "style-data" 
			       legend-state "style-data")

  (guppi-element-state-connect pie-state "label-data" 
			       legend-state "label-data")


  ;; do layout
  
  (guppi-group-view-layout-flush-top root-view text-view 7.2)
  (guppi-group-view-layout-horizontal-fill root-view text-view 0 0)
  (guppi-group-view-layout-natural-height root-view text-view)

  (guppi-group-view-layout-flush-bottom root-view pie-view 0)
  (guppi-group-view-layout-vertically-adjacent root-view
					       text-view pie-view 0)
  (guppi-group-view-layout-horizontally-adjacent root-view
						 pie-view legend-view 0)
  (guppi-group-view-layout-flush-left root-view pie-view 0)

  (guppi-group-view-layout-natural-height root-view legend-view)
  (guppi-group-view-layout-natural-width root-view legend-view)
  (guppi-group-view-layout-flush-right root-view legend-view 0)
  (guppi-group-view-layout-same-y-center root-view pie-view legend-view)


  (guppi-root-group-view-show-in-window root-view)

  )

(gc)
