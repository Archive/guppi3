;;; $Id: scatter-demo.scm,v 1.22 2001/08/21 03:51:55 trow Exp $

;(define poly-curve (guppi-curve-new-polynomial))
;(define poly (guppi-curve-get-polynomial poly-curve))
;(define (fact n) (if (zero? n) 1 (* n (fact (1- n)))))

;(define (sin-coeff n)
;  (if (odd? n)
;      (* (if (odd? (/ (1- n) 2)) -1 1) (/ 1 (fact n)))
;      0))

;(define (set-poly-sin-term poly n)
;  (guppi-polynomial-set-coefficient poly n (sin-coeff n)))

;(define (set-poly-sin poly MAX)
;  (define (iter n)
;    (set-poly-sin-term poly n)
;    (if (< n MAX)
;	(iter (1+ n))))
;  (iter 0)
;  poly)

(let* (
       
       (root-state (guppi-root-group-state-new))
       (root-view  (guppi-element-state-make-view root-state))
       
       (xybox-state (guppi-xybox-state-new))
       (scatter-state (guppi-scatter-state-new))
;       (linegraph-state (guppi-linegraph-state-new))
       (frame-state (guppi-frame-state-new))
       (slinreg-state (guppi-slinreg-state-new))
       (n-ax-state (guppi-axis-state-new))
       (s-ax-state (guppi-axis-state-new))
       (e-ax-state (guppi-axis-state-new))
       (w-ax-state (guppi-axis-state-new))
       (title-state (guppi-text-state-new))
       (x-boxplot-state (guppi-boxplot-state-new))
       (y-boxplot-state (guppi-boxplot-state-new))
       
       (xybox-view (guppi-element-state-make-view xybox-state))
       (scatter-view (guppi-element-state-make-view scatter-state))
;       (linegraph-view (guppi-element-state-make-view linegraph-state))
       (frame-view (guppi-element-state-make-view frame-state))
       (slinreg-view (guppi-element-state-make-view slinreg-state))
       (n-ax-view (guppi-element-state-make-view n-ax-state))
       (s-ax-view (guppi-element-state-make-view s-ax-state))
       (e-ax-view (guppi-element-state-make-view e-ax-state))
       (w-ax-view (guppi-element-state-make-view w-ax-state))
       (title-view (guppi-element-state-make-view title-state))
       (x-boxplot-view (guppi-element-state-make-view x-boxplot-state))
       (y-boxplot-view (guppi-element-state-make-view y-boxplot-state))

       ;(fit (guppi-fit-slr-new))

       (gap 3.6)
       
       )

  (guppi-element-state-set-label xybox-state "The Magic X-Y Box")
  (guppi-element-state-set-label root-state "Scatter Demo")
  (guppi-element-state-set-label scatter-state "Scatter Plot")
  (guppi-element-state-set-label slinreg-state "Linear Regression")
  (guppi-element-state-set-label n-ax-state "Upper x-axis")
  (guppi-element-state-set-label s-ax-state "Lower x-axis")
  (guppi-element-state-set-label e-ax-state "Right y-axis")
  (guppi-element-state-set-label w-ax-state "Left y-axis")
  (guppi-element-state-set-label title-state "Plot Title")
  (guppi-element-state-set-label x-boxplot-state "X-Boxplot")
  (guppi-element-state-set-label y-boxplot-state "Y-Boxplot")

  (guppi-root-group-view-set-size root-view (* 72 7) (* 72 5))

  (guppi-text-state-set-text title-state "Guppi: A Gnome Plotting Tool")
  (guppi-text-state-set-font-size title-state 20)
;  (guppi-text-state-set-rot-angle title-state 5);
  
  (guppi-boxplot-state-set-horizontal x-boxplot-state #t)
  (guppi-boxplot-state-set-vertical y-boxplot-state #t)

  (guppi-element-state-connect scatter-state "x-data" slinreg-state "x-data")
  (guppi-element-state-connect scatter-state "y-data" slinreg-state "y-data")

;  (guppi-element-state-connect scatter-state "x-data" linegraph-state "x-data")
;  (guppi-element-state-connect scatter-state "y-data" linegraph-state "y-data")

;  (guppi-linegraph-state-set-data linegraph-state poly-curve)

  (let ((ss (guppi-scatter-state-get-style scatter-state 0)))
    
    (guppi-style-set-marker-size1 ss 5))

  (guppi-element-state-connect x-boxplot-state "misc-data"
			       scatter-state "x-data")

  (guppi-element-state-connect n-ax-state "misc-data"
			       scatter-state "x-data")

  (guppi-element-state-connect s-ax-state "misc-data"
			       scatter-state "x-data")

  (guppi-element-state-connect y-boxplot-state "misc-data"
			       scatter-state "y-data")

  (guppi-element-state-connect w-ax-state "misc-data"
			       scatter-state "y-data")

  (guppi-element-state-connect e-ax-state "misc-data"
			       scatter-state "y-data")


  (guppi-axis-state-set-position n-ax-state 'north)
  (guppi-axis-state-set-position s-ax-state 'south)
  (guppi-axis-state-set-position e-ax-state 'east)
  (guppi-axis-state-set-position w-ax-state 'west)

  ;; axis default for show-legend is #t
  (guppi-axis-state-set-show-legend n-ax-state #f)
  (guppi-axis-state-set-show-legend e-ax-state #f)
  
  (guppi-element-view-connect xybox-view "x-view" n-ax-view "x-view")
  (guppi-element-view-connect xybox-view "x-view" s-ax-view "x-view")
  (guppi-element-view-connect xybox-view "y-view" e-ax-view "y-view")
  (guppi-element-view-connect xybox-view "y-view" w-ax-view "y-view")

  (guppi-element-view-set-x-axis-marker-type xybox-view 1)
  (guppi-element-view-set-y-axis-marker-type xybox-view 1)

  (guppi-element-view-connect xybox-view "x-markers" n-ax-view "x-markers")
  (guppi-element-view-connect xybox-view "x-markers" s-ax-view "x-markers")
  (guppi-element-view-connect xybox-view "y-markers" e-ax-view "y-markers")
  (guppi-element-view-connect xybox-view "y-markers" w-ax-view "y-markers")

  (guppi-element-view-connect xybox-view "x-view" x-boxplot-view "x-view")
  (guppi-element-view-connect xybox-view "y-view" y-boxplot-view "y-view")

  (guppi-element-view-set-preferred-view scatter-view)

  ;; Put our xy-ish objects into the xybox view
  (guppi-group-view-add xybox-view frame-view)
  (guppi-group-view-add xybox-view scatter-view)
;  (guppi-group-view-add xybox-view linegraph-view)
  (guppi-group-view-add xybox-view slinreg-view)

  ;;; do layout

  (guppi-group-view-layout-flush-top root-view title-view gap)
  (guppi-group-view-layout-horizontal-fill root-view title-view 0 0)
  (guppi-group-view-layout-natural-height root-view title-view)


  (guppi-group-view-layout-natural-height root-view x-boxplot-view)
  (guppi-group-view-layout-natural-width root-view y-boxplot-view)

  (guppi-group-view-layout-vertically-adjacent root-view
					       title-view x-boxplot-view gap)

  (guppi-group-view-layout-vbox2 root-view x-boxplot-view n-ax-view gap)
  (guppi-group-view-layout-hbox2 root-view e-ax-view y-boxplot-view gap)


  (guppi-group-view-layout-hbox3 root-view
				 w-ax-view xybox-view e-ax-view gap)
  (guppi-group-view-layout-vbox3 root-view
				 n-ax-view xybox-view s-ax-view gap)

  (guppi-group-view-layout-flush-left root-view w-ax-view gap)
  (guppi-group-view-layout-flush-right root-view y-boxplot-view gap)
  (guppi-group-view-layout-flush-bottom root-view s-ax-view gap)

  (guppi-group-view-layout-natural-height root-view n-ax-view)
  (guppi-group-view-layout-natural-height root-view s-ax-view)
  (guppi-group-view-layout-natural-width root-view w-ax-view)
  (guppi-group-view-layout-natural-width root-view e-ax-view)

  ;; show-in-window automatically does a commit-changes on the view
  (guppi-root-group-view-show-in-window root-view)

  (guppi-element-view-spew-xml root-view)
  
  )

