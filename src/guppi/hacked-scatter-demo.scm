;;; $Id: hacked-scatter-demo.scm,v 1.6 2000/06/17 23:15:57 trow Exp $

(define (guppi-element-view-fully-connect a b)
  (guppi-element-view-connect a "x-view" b "x-view")
  (guppi-element-view-connect a "y-view" b "y-view")
  (guppi-element-view-connect a "x-markers" b "x-markers")
  (guppi-element-view-connect a "y-markers" b "y-markers"))

(define x-data (guppi-seq-scalar-new))
(define y-data (guppi-seq-scalar-new))
(define c-data (guppi-seq-scalar-new))
(define sty-data (guppi-seq-integer-new))

(define (iter i)
  (guppi-seq-scalar-append x-data i)
  (guppi-seq-scalar-append y-data (* (sin i) (* 5 (1+ (* i i)))))
  (guppi-seq-scalar-append c-data i)
  (guppi-seq-integer-append sty-data 0)
  (if (< i 50) (iter (+ i 0.5))))
(iter -50)

(define (guppi-plot-demo)

  (let* (

	 (scatter-state (guppi-scatter-state-new))
	 (frame-state (guppi-frame-state-new))
	 (line-state (guppi-linegraph-state-new))
	 (n-ax-state (guppi-axis-state-new))
	 (s-ax-state (guppi-axis-state-new))
	 (e-ax-state (guppi-axis-state-new))
	 (w-ax-state (guppi-axis-state-new))
	 (title-state (guppi-text-state-new))
	 (x-boxplot-state (guppi-boxplot-state-new))
	 (y-boxplot-state (guppi-boxplot-state-new))

	 (scatter-view (guppi-element-state-make-view scatter-state))
	 (frame-view (guppi-element-state-make-view frame-state))
	 (line-view (guppi-element-state-make-view line-state))
	 (n-ax-view (guppi-element-state-make-view n-ax-state))
	 (s-ax-view (guppi-element-state-make-view s-ax-state))
	 (e-ax-view (guppi-element-state-make-view e-ax-state))
	 (w-ax-view (guppi-element-state-make-view w-ax-state))
	 (title-view (guppi-element-state-make-view title-state))
	 (x-boxplot-view (guppi-element-state-make-view x-boxplot-state))
	 (y-boxplot-view (guppi-element-state-make-view y-boxplot-state))

	 (plot (guppi-plot-new))
	 
	 )

    (guppi-element-state-popup-config scatter-state)

    (guppi-text-state-set-text title-state "Guppi: A Gnome Plotting Tool")
    (guppi-text-state-set-font-size title-state 20)
    
    (guppi-boxplot-state-set-horizontal x-boxplot-state #t)
    (guppi-boxplot-state-set-vertical y-boxplot-state #t)

    (guppi-linegraph-state-set-x-data line-state x-data)
    (guppi-linegraph-state-set-y-data line-state y-data)

    (guppi-element-state-connect scatter-state "x-data" line-state "x-data")
    (guppi-element-state-connect scatter-state "y-data" line-state "y-data")
;    (guppi-scatter-state-set-color-gradient-data scatter-state c-data)
;    (guppi-scatter-state-set-size1-gradient-data scatter-state c-data)
    (guppi-scatter-state-set-style-data scatter-state sty-data)

    (let ((ss (guppi-scatter-state-get-style scatter-state 0)))
	
      (guppi-style-set-marker-size1 ss 5))

    (guppi-element-state-connect x-boxplot-state "misc-data"
				 line-state "x-data")

    (guppi-element-state-connect y-boxplot-state "misc-data"
				 line-state "y-data")

    (guppi-axis-state-set-position n-ax-state 'north)
    (guppi-axis-state-set-position s-ax-state 'south)
    (guppi-axis-state-set-position e-ax-state 'east)
    (guppi-axis-state-set-position w-ax-state 'west)

    (guppi-element-view-fully-connect scatter-view frame-view)
    (guppi-element-view-fully-connect scatter-view line-view)

    (guppi-element-view-connect scatter-view "x-view" n-ax-view "x-view")
    (guppi-element-view-connect scatter-view "x-view" s-ax-view "x-view")
    (guppi-element-view-connect scatter-view "y-view" e-ax-view "y-view")
    (guppi-element-view-connect scatter-view "y-view" w-ax-view "y-view")

    (guppi-element-view-connect scatter-view "x-markers" n-ax-view "x-markers")
    (guppi-element-view-connect scatter-view "x-markers" s-ax-view "x-markers")
    (guppi-element-view-connect scatter-view "y-markers" e-ax-view "y-markers")
    (guppi-element-view-connect scatter-view "y-markers" w-ax-view "y-markers")

    (guppi-element-view-connect scatter-view "x-view" x-boxplot-view "x-view")
    (guppi-element-view-connect scatter-view "y-view" y-boxplot-view "y-view")

    (guppi-element-view-set-preferred-view scatter-view)

    (guppi-plot-set-size plot (* 72 7) (* 72 5))

    ;;; do layout

    (guppi-plot-layout-flush-top plot title-view 7.2)
    (guppi-plot-layout-horizontal-fill plot title-view 0 0)
    (guppi-plot-layout-natural-height plot title-view)

    (guppi-plot-layout-same-place plot frame-view line-view)
    (guppi-plot-layout-same-place plot frame-view scatter-view)

    (guppi-plot-layout-natural-height plot x-boxplot-view)
    (guppi-plot-layout-natural-width plot y-boxplot-view)

    (guppi-plot-layout-vertically-adjacent plot title-view x-boxplot-view 7.2)

    (guppi-plot-layout-vbox2 plot x-boxplot-view n-ax-view 7.2)
    (guppi-plot-layout-hbox2 plot e-ax-view y-boxplot-view 7.2)


    (guppi-plot-layout-hbox3 plot w-ax-view scatter-view e-ax-view 7.2)
    (guppi-plot-layout-vbox3 plot n-ax-view scatter-view s-ax-view 7.2)

    (guppi-plot-layout-flush-left plot w-ax-view 7.2)
    (guppi-plot-layout-flush-right plot y-boxplot-view 7.2)
    (guppi-plot-layout-flush-bottom plot s-ax-view 7.2)

    (guppi-plot-layout-height-relative plot n-ax-view 0.10)
    (guppi-plot-layout-height-relative plot s-ax-view 0.10)
    (guppi-plot-layout-width-relative plot e-ax-view 0.10)
    (guppi-plot-layout-width-relative plot w-ax-view 0.10)

    ;(guppi-plot-print-ps-to-file plot "foo.ps")
    (guppi-plot-show-in-window plot)	

    scatter-state
    )
  )

;(display "Running demo...") (newline)
(define ss (guppi-plot-demo))
;(display "Done") (newline)

(define sty (guppi-scatter-state-get-style ss 0))
(define cg (guppi-color-gradient-new))
(guppi-color-gradient-set-ice cg 1 1)
(guppi-scatter-style-set-color-gradient sty cg)
;(guppi-style-edit-window sty)
