from Guppi import *
from Guppi.plot import *

barchart_data_list1 = (5, 3, 10, 3, 7, 9, 12)
barchart_data_list2 = (4, 7, 4, 2, 9, 3, 8)
barchart_data_list3 = (8, 3, 9, 8, 2, 5, 11)
barchart_row_labels = ('Chicago',
                       'New York',
                       'New Delhi',
                       'Sidney',
                       'London',
                       'Tokyo',
                       'Beijing')

barchart_column_labels = ('One', 'Two', 'Three')
barchart_column_colors = ('red', 'green', 'lightblue')

def scalar_data(data_list):
    data = GuppiSeqScalar()
    for x in data_list:
        data.append(x)
    return data

def string_data(data_list):
    data = GuppiSeqString()
    for x in data_list:
        data.append(x)
    return data

def style_data(data_list):
    data = GuppiSeqStyle()
    for x in data_list:
        sty = GuppiStyle()
        sty.set_color(x)
        data.append(sty)
    return data

def barchart_demo():
    barchart_data1 = scalar_data(barchart_data_list1)
    barchart_data2 = scalar_data(barchart_data_list2)
    barchart_data3 = scalar_data(barchart_data_list3)
    barchart_data_rl = string_data(barchart_row_labels)
    barchart_data_cl = string_data(barchart_column_labels)
    barchart_data_sty = style_data(barchart_column_colors)

    barchart_data = GuppiSeqData()

    root_state = GuppiRootGroupState()
    root_view = root_state.make_view()

    text_state = GuppiTextState()
    more_text_state = GuppiTextState()
    barchart_state = GuppiBarchartState()
    frame_state = GuppiFrameState()

    w_ax_state = GuppiAxisState()
    s_ax_state = GuppiAxisState()
    legend_state = GuppiLegendState()

    text_view = text_state.make_view()
    more_text_view = more_text_state.make_view()
    barchart_view = barchart_state.make_view()
    frame_view = frame_state.make_view()
    w_ax_view = w_ax_state.make_view()
    s_ax_view = s_ax_state.make_view()
    legend_view = legend_state.make_view()

    gap = 7.2

    barchart_data.append(barchart_data1)
    barchart_data.append(barchart_data2)
    barchart_data.append(barchart_data3)

    root_state.set_label("Bar Chart Demo")
    text_state.set_label("Titel")
    more_text_state.set_label("Subtitle")
    barchart_state.set_label("Bar Chart")
    w_ax_state.set_label("y-axis")
    s_ax_state.set_label("x-axis")
    legend_state.set_label("Legend")

    root_view.set_size(72 * 7, 72 * 5)

    text_state.set_text("Guppi Makes Graphs for Gnome")
    text_state.set_font_size(24)

    more_text_state.set_text("Total World Domination, One Desktop at a Time")
    more_text_state.set_font_size(18)

    barchart_state.set_data(barchart_data)
    barchart_state.set_row_labels(barchart_data_rl)
    barchart_state.set_column_labels(barchart_data_cl)
    barchart_state.set_column_styles(barchart_data_sty)

    barchart_state.connect("style-data", legend_state, "style-data")
    barchart_state.connect("label-data", legend_state, "label-data")

    s_ax_view.connect("x-view", barchart_view, "x-view")
    s_ax_view.connect("x-markers", barchart_view, "x-markers")

    w_ax_view.connect("y-view", barchart_view, "y-view")
    w_ax_view.connect("y-makers", barchart_view, "y-markers")

    frame_view.connect("y-view", barchart_view, "y-view")
    frame_view.connect("y-markers", barchart_view, "y-makers")

    w_ax_state.set_position("west")
    s_ax_state.set_position("south")

    barchart_view.set_preferred_view()

    root_view.layout_same_place(frame_view, barchart_view)

    root_view.layout_natural_width(legend_view)
    root_view.layout_natural_height(legend_view)
    root_view.layout_flush_right(legend_view, 0)
    root_view.layout_same_y_center(legend_view, barchart_view)

    root_view.layout_horizontal_fill(text_view, gap, gap)
    root_view.layout_horizontal_fill(more_text_view, gap, gap)

    root_view.layout_natural_height(text_view)
    root_view.layout_natural_height(more_text_view)

    root_view.layout_flush_top(text_view, gap)
    root_view.layout_vertically_adjacent(text_view, more_text_view, 0)

    root_view.layout_hbox2(w_ax_view, barchart_view, 0)
    root_view.layout_flush_left(w_ax_view, gap)
    root_view.layout_natural_width(w_ax_view)

    root_view.layout_horizontally_adjacent(barchart_view, legend_view, gap)

    root_view.layout_vbox2(barchart_view, s_ax_view, 0)
    root_view.layout_natural_height(s_ax_view)
    root_view.layout_flush_bottom(s_ax_view, gap)

    root_view.layout_vertically_adjacent(more_text_view, barchart_view, gap)
    #root_view.layout_flush_bottom(barchart_view, gap)

    root_view.show_in_window()

if __name__ == '__main__':
    barchart_demo()    








