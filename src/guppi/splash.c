/* $Id$ */

/*
 * guppi-splash.c
 *
 * Copyright (C) 1999, 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 * and Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-splash.h"

/* #include <gnome.h> */
#include <gtk/gtkwidget.h>
#include <gtk/gtkmain.h>
#include <gtk/gtkwindow.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkvbox.h>
#include <gtk/gtklabel.h>

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-i18n.h>

#include <libgnomeui/gnome-pixmap.h>

#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include "guppi-debug.h"
#include "guppi-version.h"
#include "guppi-paths.h"
#include "guppi-memory.h"

static gboolean please_destroy_splash = FALSE;
static GtkWidget *splash = NULL;
static GtkWidget *splash_label1 = NULL;
static GtkWidget *splash_label2 = NULL;

static time_t splash_time;
static const gint min_splash_time = 10;

static void
gtk_catchup (void)
{
  /* Flush all events */
  while (gtk_events_pending ())
    gtk_main_iteration ();

  /* Sleep a little bit, so that gtk's idle handlers have time to catch up
     and do any rendering that needs to be done. */
  usleep (150);

  /* Flush all events again */
  while (gtk_events_pending ())
    gtk_main_iteration ();
}

static gint
destroy_timeout (gpointer user_data)
{
  time_t t;

  if (!please_destroy_splash)
    return TRUE;

  if (splash == NULL)
    return FALSE;

  time (&t);
  if (t - splash_time < min_splash_time)
    return TRUE;

  if (splash)
    gtk_widget_destroy (splash);
  gtk_catchup ();
  splash = NULL;
  please_destroy_splash = FALSE;

  return FALSE;
}

static void
set_style (GtkWidget * w, gpointer foo)
{
  GtkStyle *style;
  GdkColor bg = { 0xaedaee, 0xae00, 0xda00, 0xee00 };
  GdkColor fg = { 0x0000ff, 0x0000, 0x0000, 0xffff };

  style = gtk_style_copy (gtk_widget_get_style (w));
  style->bg[0] = bg;
  style->fg[0] = fg;
  gtk_widget_set_style (w, style);
  gtk_widget_set_style (GTK_WIDGET (foo), style);
}

void
guppi_splash_text (void)
{
  static const gchar *msg[] = {
    "Guppi comes with ABSOLUTELY NO WARRENTY.",
    "This is free software, distributed under the terms of the GNU General",
    "Public License.  You are welcome to redistribute Guppi under certain",
    "conditions.  For details, run Guppi with the --license option.",
    "For Guppi news and information, visit http://www.gnome.org/guppi.",
    NULL
  };
  const gchar **strv;

  gint i;

  fputc ('\n', stderr);
  fprintf (stderr, "Guppi (version %s)\n", guppi_version ());

  strv = guppi_copyright ();
  for (i=0; strv[i]; ++i) {
    fputs (strv[i], stderr);
    fputc ('\n', stderr);
  }

  for (i = 0; msg[i] != NULL; ++i) {
    fputs (msg[i], stderr);
    fputc ('\n', stderr);
  }

  fputc ('\n', stderr);
}

static void
delete_event_cb (GtkWidget * w)
{
  gtk_widget_destroy (splash);
  splash = NULL;
}

void
guppi_splash_create (void)
{
  GtkWidget *vbox;
  GtkWidget *pixmap;

  gchar *buffer;

  g_return_if_fail (splash == NULL);

  splash = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_position (GTK_WINDOW (splash), GTK_WIN_POS_CENTER);
  gtk_window_set_policy (GTK_WINDOW (splash), FALSE, FALSE, FALSE);

  gtk_signal_connect (GTK_OBJECT (splash),
		      "delete_event",
		      GTK_SIGNAL_FUNC (delete_event_cb), NULL);


  buffer = guppi_strdup_printf (_("Welcome to Guppi %s"), guppi_version ());
  gtk_window_set_title (GTK_WINDOW (splash), buffer);
  guppi_free (buffer);

  vbox = gtk_vbox_new (FALSE, 0);

  pixmap = gnome_pixmap_new_from_file (guppi_logo_graphic_path ());

  splash_label1 = gtk_label_new ("");
  splash_label2 = gtk_label_new ("");

  /* We change the style and adjust the color scheme upon
     realization... */
  gtk_signal_connect (GTK_OBJECT (splash),
		      "realize", GTK_SIGNAL_FUNC (set_style), vbox);

  gtk_box_pack_start (GTK_BOX (vbox), pixmap, TRUE, TRUE, 1);
  gtk_box_pack_end (GTK_BOX (vbox), splash_label2, TRUE, TRUE, 0);
  gtk_box_pack_end (GTK_BOX (vbox), splash_label1, TRUE, TRUE, 0);
  gtk_container_add (GTK_CONTAINER (splash), vbox);

  gtk_widget_show (pixmap);
  gtk_widget_show (splash_label1);
  gtk_widget_show (splash_label2);
  gtk_widget_show (vbox);
  gtk_widget_show (splash);

  gtk_catchup ();
  time (&splash_time);

  gtk_timeout_add (1000 * min_splash_time / 5, destroy_timeout, NULL);
}

void
guppi_splash_destroy (void)
{
  if (splash == NULL)
    return;

  guppi_splash_message (_("Visit us at"), _("http://www.gnome.org/guppi"));

  please_destroy_splash = TRUE;
}

void
guppi_splash_message (const gchar * line1, const gchar * line2)
{

  gtk_catchup ();

  if (splash) {

    if (line1)
      gtk_label_set_text (GTK_LABEL (splash_label1), (gchar *) line1);

    if (line2)
      gtk_label_set_text (GTK_LABEL (splash_label2), (gchar *) line2);
  }

  if (guppi_is_verbose ()) {
    fputs ("*** Guppi: ", stderr);
    if (line1) {
      fputs (line1, stderr);
      fputs (" ", stderr);
    }
    if (line2)
      fputs (line2, stderr);
    fputc ('\n', stderr);
  }

  if (splash) {
    gtk_catchup ();
  }
}

/* $Id$ */
