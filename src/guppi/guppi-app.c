/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-app.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>

#include <string.h>

#include <gtk/gtkscrolledwindow.h>

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-util.h>

#include <libgnomeui/gnome-app.h>
#include <libgnomeui/gnome-app-helper.h>
#include <libgnomeui/gnome-appbar.h>
#include <libgnomeui/gnome-types.h>
#include <libgnomeui/gnome-dialog.h>
#include <libgnomeui/gnome-stock.h>

#include <guppi-useful.h>
#include <guppi-data-tree-view.h>
#include "about.h"
#include "term.h"
#include "file-open.h"
#include "plug-ins.h"
#include "guile-load.h"
#ifdef HAVE_PYTHON
#include "python-load.h"
#include "python-term.h"
#endif
#include "guppi-app.h"

#include <libgnome/gnome-config.h>

#define OPEN_AGAIN_ITEM_PATH "File/Open Again"
#define OPEN_AGAIN_MENU_PATH "File/Open Again/"

struct _RecentFile {
  GuppiApp *app;
  gchar *filename_full;
  gchar *filename_short;
  gchar *hint;
  gint inode;
};

static void
recent_file_free (RecentFile * rf)
{
  if (rf) {
    guppi_free (rf->filename_full);
    guppi_free (rf->hint);
  }
  guppi_free (rf);
}

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0
};

static void
guppi_app_get_arg (GtkObject * obj, GtkArg * arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_app_set_arg (GtkObject * obj, GtkArg * arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_app_destroy (GtkObject * obj)
{
  if (parent_class->destroy)
    parent_class->destroy (obj);
}

static void
guppi_app_finalize (GtkObject * obj)
{
  GuppiApp *app = GUPPI_APP (obj);
  gint i;

  guppi_finalized (obj);

  for (i=0; i<RECENT_FILE_MAX; ++i) {
    if (app->recent_file[i]) 
      recent_file_free (app->recent_file[i]);
  }

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_app_realize (GtkWidget * w)
{
  GnomeApp *app = GNOME_APP (w);
  GuppiApp *guppi_app = GUPPI_APP (w);

  GtkWidget *appbar;
  GtkWidget *swin;
  GtkWidget *browser;
  static void guppi_app_setup_menus (GuppiApp *);
  static void exit_cb (GtkWidget *, gpointer);

  if (GTK_WIDGET_CLASS (parent_class)->realize)
    GTK_WIDGET_CLASS (parent_class)->realize (w);

  gtk_window_set_default_size (GTK_WINDOW (w), 400, 250);

  appbar = gnome_appbar_new (TRUE, TRUE, GNOME_PREFERENCES_USER);
  gnome_app_set_statusbar (app, appbar);
  guppi_progress_register (gnome_appbar_get_progress (GNOME_APPBAR (appbar)));

  guppi_app_setup_menus (guppi_app);

  swin = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (swin),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

  browser = guppi_data_tree_view_new (guppi_data_tree_main ());
  gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (swin), browser);
  gtk_widget_show (browser);

  gnome_app_set_contents (app, swin);

  gtk_signal_connect (GTK_OBJECT (app), "delete_event",
		      GTK_SIGNAL_FUNC (exit_cb), NULL);
}

static void
guppi_app_unrealize (GtkWidget * w)
{

}

static void
guppi_app_class_init (GuppiAppClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  parent_class = gtk_type_class (gnome_app_get_type ());

  object_class->get_arg = guppi_app_get_arg;
  object_class->set_arg = guppi_app_set_arg;
  object_class->destroy = guppi_app_destroy;
  object_class->finalize = guppi_app_finalize;

  widget_class->realize = guppi_app_realize;
  widget_class->unrealize = guppi_app_unrealize;
}

static void
guppi_app_init (GuppiApp * obj)
{

}

GtkType guppi_app_get_type (void)
{
  static GtkType guppi_app_type = 0;
  if (!guppi_app_type) {
    static const GtkTypeInfo guppi_app_info = {
      "GuppiApp",
      sizeof (GuppiApp),
      sizeof (GuppiAppClass),
      (GtkClassInitFunc) guppi_app_class_init,
      (GtkObjectInitFunc) guppi_app_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_app_type = gtk_type_unique (gnome_app_get_type (), &guppi_app_info);
  }
  return guppi_app_type;
}

GtkWidget *
guppi_app_new (void)
{
  GtkObject *obj = guppi_type_new (guppi_app_get_type ());
  gnome_app_construct (GNOME_APP (obj), PACKAGE, "Guppi");
  return GTK_WIDGET (obj);
}

/***************************************************************************/

/*** Callbacks for app menus ***/

static void
data_entry_cb (GtkWidget * w, gpointer data)
{
  guppi_unimplemented_function_dialog ("Data Entry");
}

static void
save_cb (GtkWidget * w, gpointer data)
{
  guppi_unimplemented_function_dialog ("Save");
}

static void
save_as_cb (GtkWidget * w, gpointer data)
{
  guppi_unimplemented_function_dialog ("Save as");
}

static void
exit_cb (GtkWidget * w, gpointer data)
{
  guppi_exit ();
}

static void
guile_terminal_cb (GtkWidget * w, gpointer data)
{
  guile_terminal ();
}

#ifdef HAVE_PYTHON
static void
python_terminal_cb (GtkWidget * w, gpointer data)
{
  python_terminal ();
}
#endif

static void
hacked_scatter_plot_cb (GtkWidget * w, gpointer data)
{
  gchar *path;

  path = guppi_find_guile_script ("scatter-demo.scm");

  if (path)
    guppi_safe_load (path);
  else
    g_warning ("Can't find \"scatter-demo.scm\".");

  guppi_free (path);
}

static void
hacked_scatter_grid_cb (GtkWidget * w, gpointer data)
{
  gchar *path;

  path = guppi_find_guile_script ("scatter-grid-demo.scm");

  if (path)
    guppi_safe_load (path);
  else
    g_warning ("Can't find \"scatter-grid-demo.scm\".");

  guppi_free (path);
}

static void
hacked_pie_cb (GtkWidget * w, gpointer data)
{
  gchar *path;

  path = guppi_find_guile_script ("pie-demo.scm");

  if (path)
    guppi_safe_load (path);
  else
    g_warning ("Can't find \"pie-demo.scm\".");

  guppi_free (path);
}

static void
hacked_bar_cb (GtkWidget * w, gpointer data)
{
  gchar *path;

  path = guppi_find_guile_script ("barchart-demo.scm");

  if (path)
    guppi_safe_load (path);
  else
    g_warning ("Can't find \"barchart-demo.scm\".");

  guppi_free (path);

}

static void
price_bars_cb (GtkWidget * w, gpointer data)
{
  gchar *path;

  path = guppi_find_guile_script ("pbars.scm");

  if (path)
    guppi_safe_load (path);
  else
    g_warning ("Can't find \"pbars.scm\".");

  guppi_free (path);

}

static void
guile_load_cb (GtkWidget * w, gpointer data)
{
  guile_select_and_load (GNOME_APP (data));
}

#ifdef HAVE_PYTHON
static void
python_load_cb (GtkWidget * w, gpointer data)
{
  python_select_and_load (GNOME_APP (data));
}
#endif

static void
about_cb (GtkWidget * w, gpointer data)
{
  static GtkWidget *dialog = NULL;
  GtkWidget *app;

  app = GTK_WIDGET (data);

  if (dialog != NULL) {
    g_assert (GTK_WIDGET_REALIZED (dialog));
    gdk_window_show (dialog->window);
    gdk_window_raise (dialog->window);
  } else {

    dialog = guppi_about ();

    gtk_signal_connect (GTK_OBJECT (dialog),
			"destroy",
			GTK_SIGNAL_FUNC (gtk_widget_destroyed), &dialog);

    gnome_dialog_set_parent (GNOME_DIALOG (dialog), GTK_WINDOW (app));

    gtk_widget_show (dialog);
  }
}

static void
about_plug_in_cb (GtkWidget * w, gpointer data)
{
  loaded_plug_ins ();
}


/***************************************************************************/

static void
guppi_app_setup_menus (GuppiApp * app)
{
  GtkMenuShell *ms;
  gint pos = 0;
  static void guppi_app_load_visited_files (GuppiApp *);

  static GnomeUIInfo recent_files_menu[] = { GNOMEUIINFO_END };

  static GnomeUIInfo file_menu[] = {
    GNOMEUIINFO_MENU_OPEN_ITEM (file_open, NULL),
    {GNOME_APP_UI_SUBTREE,
     N_("Open Again"), N_("Re-Open a Recently Visited File"),
     recent_files_menu, NULL, NULL,
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_OPEN,
     0, (GdkModifierType) 0, NULL},
    {GNOME_APP_UI_ITEM,
     N_("Data Entry"), N_("Directly Enter Data For Import"),
     data_entry_cb, NULL, NULL,
     GNOME_APP_PIXMAP_NONE, NULL,
     0, (GdkModifierType) 0, NULL},
    GNOMEUIINFO_MENU_SAVE_ITEM (save_cb, NULL),
    GNOMEUIINFO_MENU_SAVE_AS_ITEM (save_as_cb, NULL),
    GNOMEUIINFO_MENU_EXIT_ITEM (exit_cb, NULL),
    GNOMEUIINFO_END
  };

  static GnomeUIInfo plot_menu[] = {
    {GNOME_APP_UI_ITEM,
     N_("New Scatter Plot"), N_("Create a New Scatter Plot Window"),
     hacked_scatter_plot_cb, NULL, NULL,
     GNOME_APP_PIXMAP_NONE, NULL,
     0, (GdkModifierType) 0, NULL},
    {GNOME_APP_UI_ITEM,
     N_("New 2x2 Scatter Grid"),
     N_("Create a New 2x2 Scatter Plot Grid Window"),
     hacked_scatter_grid_cb, NULL, NULL,
     GNOME_APP_PIXMAP_NONE, NULL,
     0, (GdkModifierType) 0, NULL},
    {GNOME_APP_UI_ITEM,
     N_("Pie Chart Demo"), N_("A (Canned) Demo of Guppi's Pie Charts"),
     hacked_pie_cb, NULL, NULL,
     GNOME_APP_PIXMAP_NONE, NULL,
     0, (GdkModifierType) 0, NULL},
    {GNOME_APP_UI_ITEM,
     N_("Bar Chart Demo"), N_("A (Canned) Demo of Guppi's Bar Charts"),
     hacked_bar_cb, NULL, NULL,
     GNOME_APP_PIXMAP_NONE, NULL,
     0, (GdkModifierType) 0, NULL},
    {GNOME_APP_UI_ITEM,
     N_("Price Bars Demo"), N_("Know Your Enemy"),
     price_bars_cb, NULL, NULL,
     GNOME_APP_PIXMAP_NONE, NULL,
     0, (GdkModifierType) 0, NULL},
    GNOMEUIINFO_END
  };

  static GnomeUIInfo scripting_menu[] = {
    {GNOME_APP_UI_ITEM,
     N_("Open Guile Code"), N_("Load and Execute Guile Code"),
     guile_load_cb, NULL, NULL,
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_OPEN,
     0, (GdkModifierType) 0, NULL},
    {GNOME_APP_UI_ITEM,
     N_("Guile Terminal"), N_("Open an Interactive Guile Terminal"),
     guile_terminal_cb, NULL, NULL,
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_EXEC,
     0, (GdkModifierType) 0, NULL},
#ifdef HAVE_PYTHON
    GNOMEUIINFO_SEPARATOR,
    {GNOME_APP_UI_ITEM,
     N_("Open Python Code"), N_("Load and Execute Python Code"),
     python_load_cb, NULL, NULL,
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_OPEN,
     0, (GdkModifierType) 0, NULL},
    {GNOME_APP_UI_ITEM,
     N_("Python Terminal"), N_("Open an Interactive Python Terminal"),
     python_terminal_cb, NULL, NULL,
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_EXEC,
     0, (GdkModifierType) 0, NULL},
#endif
    GNOMEUIINFO_END
  };

  static GnomeUIInfo help_menu[] = {
    GNOMEUIINFO_HELP ("guppi"),
    GNOMEUIINFO_MENU_ABOUT_ITEM (about_cb, NULL),
    {GNOME_APP_UI_ITEM,
     N_("About Plug-Ins"), N_("Find Out About Currently Loaded Plug-Ins"),
     about_plug_in_cb, NULL, NULL,
     GNOME_APP_PIXMAP_NONE, NULL,
     0, (GdkModifierType) 0, NULL},
    GNOMEUIINFO_END
  };

  static GnomeUIInfo app_menus[] = {
    GNOMEUIINFO_MENU_FILE_TREE (file_menu),
    {GNOME_APP_UI_SUBTREE,
     N_("Plot"), N_("Plot"),
     plot_menu, NULL, NULL,
     GNOME_APP_PIXMAP_NONE, NULL,
     0, (GdkModifierType) 0, NULL},
    {GNOME_APP_UI_SUBTREE,
     N_("Scripting"), N_("Scripting"),
     scripting_menu, NULL, NULL,
     GNOME_APP_PIXMAP_NONE, NULL,
     0, (GdkModifierType) 0, NULL},
    GNOMEUIINFO_MENU_HELP_TREE (help_menu),
    GNOMEUIINFO_END
  };

  gnome_app_create_menus_with_data (GNOME_APP (app), app_menus, app);
  gnome_app_install_menu_hints (GNOME_APP (app), app_menus);


  /* Find the "Open Again" item, make it insensitive */

  ms = GTK_MENU_SHELL (gnome_app_find_menu_pos (GNOME_APP (app)->menubar,
						OPEN_AGAIN_ITEM_PATH, &pos));
  --pos;
  if (ms)
    gtk_widget_set_sensitive (GTK_WIDGET
			      (g_list_nth (ms->children, pos)->data), FALSE);

  guppi_app_load_visited_files (app);

}

/***************************************************************************/

#define VISITED_FILES_PREFIX "/Guppi/RecentlyVisited/"

static gboolean block_saves = FALSE;

static void
guppi_app_load_visited_files (GuppiApp * app)
{
  gint i;
  gchar key[8];
  gchar *val;
  gboolean bs_val;

  g_return_if_fail (app != NULL);

  /* If we save things out as we add, we corrupt the config info */
  bs_val = block_saves;
  block_saves = TRUE;

  gnome_config_push_prefix (VISITED_FILES_PREFIX);

  for (i = RECENT_FILE_MAX - 1; i >= 0; --i) {
    g_snprintf (key, 8, "%d", i);
    val = gnome_config_get_string (key);
    if (val) {
      guppi_app_add_recently_visited_file (app, val);
    }
  }

  gnome_config_pop_prefix ();

  block_saves = bs_val;
}

static void
guppi_app_save_visited_files (GuppiApp * app)
{
  gint i;
  gchar key[8];

  g_return_if_fail (app != NULL);

  if (block_saves)
    return;

  gnome_config_push_prefix (VISITED_FILES_PREFIX);

  for (i = 0; i < RECENT_FILE_MAX; ++i) {
    g_snprintf (key, 8, "%d", i);
    if (app->recent_file[i]) {
      gnome_config_set_string (key, app->recent_file[i]->filename_full);
    } else {
      gnome_config_clean_key (key);
    }
  }

  gnome_config_pop_prefix ();

  gnome_config_sync ();
}

static void
recent_visit_cb (GtkWidget * w, gpointer user_data)
{
  RecentFile *rf = (RecentFile *) user_data;
  gchar *tmp;

  /* Revisiting a file shouldn't change our current default open
     directory */

  tmp = rf->app->most_recent_open_directory;
  rf->app->most_recent_open_directory = NULL;

  file_open_ask_about_import_type (rf->filename_full,
				   FALSE,
				   DEFAULT_FILE_OPEN_THRESHOLD, rf->app);

  guppi_free (rf->app->most_recent_open_directory);
  rf->app->most_recent_open_directory = tmp;

}

static void
remove_recent_file (GuppiApp * app, RecentFile * rf, gint pos)
{
  g_return_if_fail (app != NULL);
  g_return_if_fail (rf != NULL);

  gnome_app_remove_menu_range (GNOME_APP (app), OPEN_AGAIN_MENU_PATH, pos, 1);

  recent_file_free (rf);
}

static void
shift_and_add_recent_file (GuppiApp * app, RecentFile * rf)
{
  gint i, m, top, pos;
  GnomeUIInfo ui_info[2] = { GNOMEUIINFO_END, GNOMEUIINFO_END };
  GtkMenuShell *ms;

  g_return_if_fail (app != NULL);
  g_return_if_fail (rf != NULL);

  /* Find how many recent files we have. */
  for (top = 0; top < RECENT_FILE_MAX && app->recent_file[top]; ++top);

  /* First, check if this one is already in there. */
  for (m = 0; m < top && app->recent_file[m]->inode != rf->inode; ++m);

  if (m == 0 && top != 0) {	/* If we are already on top, do nothing. */
    recent_file_free (rf);
    return;
  }

  if (m == top && m != RECENT_FILE_MAX)
    ++top;
  else if (m == RECENT_FILE_MAX)
    --m;

  if (m != top && app->recent_file[m])
    remove_recent_file (app, app->recent_file[m], m);

  /* Shift everything down */
  for (i = m; i > 0; --i)
    app->recent_file[i] = app->recent_file[i - 1];


  /* Build up the GnomeUIInfo, install our latest RecentFile on top. */

  app->recent_file[0] = rf;

  ui_info[0].type = GNOME_APP_UI_ITEM;
  ui_info[0].label = rf->filename_short;
  ui_info[0].hint = rf->hint;
  ui_info[0].moreinfo = recent_visit_cb;
  ui_info[0].user_data = rf;
  ui_info[0].pixmap_type = GNOME_APP_PIXMAP_NONE;

  gnome_app_insert_menus (GNOME_APP (app), "File/Open Again/", ui_info);
  gnome_app_install_menu_hints (GNOME_APP (app), ui_info);


  /* Find the "Open Again" menu item, make it sensitive */

  if (m == 0) {
    ms = GTK_MENU_SHELL (gnome_app_find_menu_pos (GNOME_APP (app)->menubar,
						  OPEN_AGAIN_ITEM_PATH,
						  &pos));
    --pos;
    if (ms)
      gtk_widget_set_sensitive (GTK_WIDGET
				(g_list_nth (ms->children, pos)->data), TRUE);
  }
}

void
guppi_app_add_recently_visited_file (GuppiApp * app, const gchar * filename)
{
  RecentFile *rf;
  struct stat buf;
  const gchar *user_home;
  gchar *foo;
  gint homelen;

  g_return_if_fail (app != NULL);
  g_return_if_fail (filename != NULL);

  if (stat (filename, &buf)) {
    g_warning ("stat() failed on alleged recently visited file: %s",
	       filename);
    return;
  }

  rf = guppi_new0 (RecentFile, 1);

  rf->app = app;
  rf->filename_full = guppi_strdup (filename);
  rf->filename_short = g_filename_pointer (rf->filename_full);

  rf->inode = buf.st_ino;

  /* We try to make a nice hint, mapping the user's home dir to ~ */
  user_home = gnome_util_user_home ();
  if (strncmp (rf->filename_full, user_home, homelen = strlen (user_home)) ==
      0) {
    foo = rf->filename_full + homelen;
    if (*foo == '/')
      ++foo;
    rf->hint = guppi_strdup_printf (_("Open ~/%s"), foo);
  } else
    rf->hint = guppi_strdup_printf (_("Open %s"), rf->filename_full);

  shift_and_add_recent_file (app, rf);

  guppi_app_save_visited_files (app);
}



/* $Id$ */
