from Guppi import *
from Guppi.data_impl import *

def show_stock_chart(symb, name):
    pd = load_stock_data(symb)

    root_state = GuppiRootGroupState()
    root_view = root_state.make_view()

    title_state = GuppiTextState()
    title_view = title_state.make_view()

    dt_ax_state = GuppiAxisState()
    pr_ax_state = GuppiAxisState()

    pb_state = GuppiPricebarsState()
    pb_view = pb_state.make_view()

    ln_state = GuppiLinegraphState()
    ln_view = ln_state.make_view()

    ln2_state = GuppiLinegraphState()
    ln2_view = ln2_state.make_view()

    dt_ax_view = dt_ax_state.make_view()
    pr_ax_view = pr_ax_state.make_view()

    root_view.set_size(72 * 7, 72 * 5)

    title_state.set_text(name)
    title_state.set_font_size(20)
    
    pb_state.set_data(pd)
    dt_ax_state.set_position('south')
    pr_ax_state.set_position('west')

    ln_state.set_color('red')

    #these functions aren't actually in there yet

    ln_data = date_series_new_moving_average(data_series_new_price_series_close(pd), 20)
    ln_state.set_data(ln_data)
    
#    (guppi-linegraph-state-set-data ln-state (guppi-date-series-new-moving-average (guppi-date-series-new-price-series-close pd) 20))

    ln2_state.set_color('green')

#    (guppi-linegraph-state-set-data ln2-state (guppi-date-series-new-moving-average (guppi-date-series-new-price-series-close pd) 120))

    pb_view.set_preferred_view()

    pb_view.force_preferred_y_view(1)
    ln_view.force_preferred_y_view(1)
    ln2_view.force_preferred_y_view(1)

    pb_view.connect("x-view", dt_ax_view, "x-view")
    pb_view.connect("y-view", dt_ax_view, "y-view")

    pb_view.connect("x-view", ln_view, "x-view")
    pb_view.connect("y-view", ln_view, "y-view")

    pb_view.connect("x-view", ln2_view, "x-view")
    pb_view.connect("y-view", ln2_view, "y-view")

    pb_view.set_x_axis_marker(5)
    pb_view.set_y_axis_marker(1)

    pb_view.connect("x-markers", dt_ax_view, "x-markers")
    pb_view.connect("y-markers", pr_ax_view, "y-markers")

    root_view.layout_horizontal_fill(title_view, 0, 0)
    root_view.layout_flush_top(title_view, 0)
    root_view.layout_natural_height(title_view)
    
    root_view.layout_vbox2(pb_view, dt_ax_view, 0)
    root_view.layout_hbox2(pr_ax_view, pb_view, 0)

    root_view.layout_natural_height(dt_ax_view)
    root_view.layout_natural_width(pr_ax_view)

    root_view.layout_vertically_adjacent(title_view, pb_view, 0)
    root_view.layout_flush_right(pb_view, 0)
    root_view.layout_flush_bottom(dt_ax_view, 0)
    root_view.layout_flush_left(pr_ax_view, 0)

    root_view.layout_same_plae(pb_view, ln_view)
    root_view.layout_same_plae(pb_view, ln2_view)
    
    root_view.show_in_window()

show_stock_chart('msft', 'MSFT: Microsoft')

