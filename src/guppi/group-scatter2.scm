(define (guppi-element-view-fully-connect a b)
  (guppi-element-view-connect a "x-view" b "x-view")
  (guppi-element-view-connect a "y-view" b "y-view")
  (guppi-element-view-connect a "x-markers" b "x-markers")
  (guppi-element-view-connect a "y-markers" b "y-markers"))

(define (guppi-scatter-group)

  (let* (
	 
	 (group-state (guppi-group-state-new))
	 (scatter-state (guppi-scatter-state-new))
	 (frame-state (guppi-frame-state-new))
	 (slinreg-state (guppi-slinreg-state-new))
	 (n-ax-state (guppi-axis-state-new))
	 (s-ax-state (guppi-axis-state-new))
	 (e-ax-state (guppi-axis-state-new))
	 (w-ax-state (guppi-axis-state-new))
	 (title-state (guppi-text-state-new))
	 (x-boxplot-state (guppi-boxplot-state-new))
	 (y-boxplot-state (guppi-boxplot-state-new))

	 (group-view (guppi-element-state-make-view group-state))
	 (scatter-view (guppi-element-state-make-view scatter-state))
	 (frame-view (guppi-element-state-make-view frame-state))
	 (slinreg-view (guppi-element-state-make-view slinreg-state))
	 (n-ax-view (guppi-element-state-make-view n-ax-state))
	 (s-ax-view (guppi-element-state-make-view s-ax-state))
	 (e-ax-view (guppi-element-state-make-view e-ax-state))
	 (w-ax-view (guppi-element-state-make-view w-ax-state))
	 (title-view (guppi-element-state-make-view title-state))
	 (x-boxplot-view (guppi-element-state-make-view x-boxplot-state))
	 (y-boxplot-view (guppi-element-state-make-view y-boxplot-state))

	 (gap 3.6)
	 
	 )

    (guppi-text-state-set-text title-state "Graphs Everywhere!")
    (guppi-text-state-set-font-size title-state 16)
    
    (guppi-boxplot-state-set-horizontal x-boxplot-state #t)
    (guppi-boxplot-state-set-vertical y-boxplot-state #t)

    (guppi-element-state-connect scatter-state "x-data" slinreg-state "x-data")
    (guppi-element-state-connect scatter-state "y-data" slinreg-state "y-data")

    (let ((ss (guppi-scatter-state-get-style scatter-state 0)))
	
      (guppi-style-set-marker-size1 ss 5))

    (guppi-element-state-connect x-boxplot-state "misc-data"
				 scatter-state "x-data")

    (guppi-element-state-connect n-ax-state "misc-data"
				 scatter-state "x-data")

    (guppi-element-state-connect s-ax-state "misc-data"
				 scatter-state "x-data")

    (guppi-element-state-connect y-boxplot-state "misc-data"
				 scatter-state "y-data")

    (guppi-element-state-connect w-ax-state "misc-data"
				 scatter-state "y-data")

    (guppi-element-state-connect e-ax-state "misc-data"
				 scatter-state "y-data")


    (guppi-axis-state-set-position n-ax-state 'north)
    (guppi-axis-state-set-position s-ax-state 'south)
    (guppi-axis-state-set-position e-ax-state 'east)
    (guppi-axis-state-set-position w-ax-state 'west)

    ;; axis default for show-legend is #t
    ;(guppi-axis-state-set-show-legend n-ax-state #f)
    ;(guppi-axis-state-set-show-legend e-ax-state #f)

    (guppi-element-view-fully-connect scatter-view frame-view)
    (guppi-element-view-fully-connect scatter-view slinreg-view)

    (guppi-element-view-connect scatter-view "x-view" n-ax-view "x-view")
    (guppi-element-view-connect scatter-view "x-view" s-ax-view "x-view")
    (guppi-element-view-connect scatter-view "y-view" e-ax-view "y-view")
    (guppi-element-view-connect scatter-view "y-view" w-ax-view "y-view")

    (guppi-element-view-connect scatter-view "x-markers" n-ax-view "x-markers")
    (guppi-element-view-connect scatter-view "x-markers" s-ax-view "x-markers")
    (guppi-element-view-connect scatter-view "y-markers" e-ax-view "y-markers")
    (guppi-element-view-connect scatter-view "y-markers" w-ax-view "y-markers")

    (guppi-element-view-connect scatter-view "x-view" x-boxplot-view "x-view")
    (guppi-element-view-connect scatter-view "y-view" y-boxplot-view "y-view")

    (guppi-element-view-set-preferred-view scatter-view)

    ;;; do layout

    (guppi-group-view-layout-flush-top group-view title-view gap)
    (guppi-group-view-layout-horizontal-fill group-view title-view 0 0)
    (guppi-group-view-layout-natural-height group-view title-view)

    (guppi-group-view-layout-same-place group-view frame-view slinreg-view)
    (guppi-group-view-layout-same-place group-view frame-view scatter-view)

    (guppi-group-view-layout-natural-height group-view x-boxplot-view)
    (guppi-group-view-layout-natural-width group-view y-boxplot-view)

    (guppi-group-view-layout-vertically-adjacent group-view title-view x-boxplot-view gap)

    (guppi-group-view-layout-vbox2 group-view x-boxplot-view n-ax-view gap)
    (guppi-group-view-layout-hbox2 group-view e-ax-view y-boxplot-view gap)


    (guppi-group-view-layout-hbox3 group-view w-ax-view scatter-view e-ax-view gap)
    (guppi-group-view-layout-vbox3 group-view n-ax-view scatter-view s-ax-view gap)

    (guppi-group-view-layout-flush-left group-view w-ax-view gap)
    (guppi-group-view-layout-flush-right group-view y-boxplot-view gap)
    (guppi-group-view-layout-flush-bottom group-view s-ax-view gap)

    (guppi-group-view-layout-natural-height group-view n-ax-view)
    (guppi-group-view-layout-natural-height group-view s-ax-view)
    (guppi-group-view-layout-natural-width group-view w-ax-view)
    (guppi-group-view-layout-natural-width group-view e-ax-view)

    (guppi-group-view-commit-changes group-view)
    
    group-view
))

(define rs (guppi-root-group-state-new))
(define rv (guppi-element-state-make-view rs))

(define sg1 (guppi-scatter-group))
(define sg2 (guppi-scatter-group))
(define sg3 (guppi-scatter-group))
(define sg4 (guppi-scatter-group))

(define (2x2-grid-group A B C D) 
  (let* ((s (guppi-group-state-new))
	 (v (guppi-element-state-make-view s)))
    
    (guppi-group-view-layout-same-height v A B)
    (guppi-group-view-layout-same-height v B C)
    (guppi-group-view-layout-same-height v C D)

    (guppi-group-view-layout-same-width v A B)
    (guppi-group-view-layout-same-width v B C)
    (guppi-group-view-layout-same-width v C D)

    (guppi-group-view-layout-hbox2 v A B 0)
    (guppi-group-view-layout-hbox2 v C D 0)
    
    (guppi-group-view-layout-vbox2 v A C 0)
    (guppi-group-view-layout-vbox2 v B D 0)

    (guppi-group-view-layout-flush-left v A 0)
    (guppi-group-view-layout-flush-top v A 0)

    (guppi-group-view-layout-flush-right v D 0)
    (guppi-group-view-layout-flush-bottom v D 0)
    (guppi-group-view-commit-changes v)
    v))

(define (2x2-scat) (2x2-grid-group (guppi-scatter-group)
				   (guppi-scatter-group)
				   (guppi-scatter-group)
				   (guppi-scatter-group)))

(define 4x4 (2x2-grid-group (2x2-scat) (2x2-scat) (2x2-scat) (2x2-scat) ))
(guppi-group-view-commit-changes 4x4)
(guppi-group-view-layout-horizontal-fill rv 4x4 0 0)
(guppi-group-view-layout-vertical-fill rv 4x4 0 0)

(guppi-group-view-commit-changes rv)

(guppi-root-group-view-show-in-window rv)
(guppi-root-group-view-set-size rv (* 20 72) (* 20 72))


