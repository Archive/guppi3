(define (show-stock-chart symb name)

  (let* (
	 
	 (pd (load-stock-data symb))
	 
	 (root-state (guppi-root-group-state-new))
	 (root-view  (guppi-element-state-make-view root-state))

	 (title-state (guppi-text-state-new))
	 (title-view (guppi-element-state-make-view title-state))

	 (dt-ax-state (guppi-axis-state-new))
	 (pr-ax-state (guppi-axis-state-new))
	 
	 (pb-state (guppi-pricebars-state-new))
	 (pb-view (guppi-element-state-make-view pb-state))

	 (ln-state (guppi-linegraph-state-new))
	 (ln-view (guppi-element-state-make-view ln-state))

	 (ln2-state (guppi-linegraph-state-new))
	 (ln2-view (guppi-element-state-make-view ln2-state))

	 (dt-ax-view (guppi-element-state-make-view dt-ax-state))
	 (pr-ax-view (guppi-element-state-make-view pr-ax-state)))

    
    (guppi-root-group-view-set-size root-view (* 72 7) (* 72 5))

    (guppi-text-state-set-text title-state name)
    (guppi-text-state-set-font-size title-state 20)
    
    (guppi-pricebars-state-set-data pb-state pd)
    (guppi-axis-state-set-position dt-ax-state 'south)
    (guppi-axis-state-set-position pr-ax-state 'west)

    (guppi-linegraph-state-set-color ln-state "red")
    (guppi-linegraph-state-set-data ln-state (guppi-date-series-new-moving-average (guppi-date-series-new-price-series-close pd) 20))

    (guppi-linegraph-state-set-color ln2-state "green")
    (guppi-linegraph-state-set-data ln2-state (guppi-date-series-new-moving-average (guppi-date-series-new-price-series-close pd) 120))
    
    (guppi-element-view-set-preferred-view pb-view)

    (guppi-element-view-force-preferred-y-view pb-view #t)
    (guppi-element-view-force-preferred-y-view ln-view #t)
    (guppi-element-view-force-preferred-y-view ln2-view #t)

    (guppi-element-view-connect pb-view "x-view" dt-ax-view "x-view")
    (guppi-element-view-connect pb-view "y-view" pr-ax-view "y-view")

    (guppi-element-view-connect pb-view "x-view" ln-view "x-view")
    (guppi-element-view-connect pb-view "y-view" ln-view "y-view")

    (guppi-element-view-connect pb-view "x-view" ln2-view "x-view")
    (guppi-element-view-connect pb-view "y-view" ln2-view "y-view")

    (guppi-element-view-set-x-axis-marker-type pb-view 5)
    (guppi-element-view-set-y-axis-marker-type pb-view 1)

    (guppi-element-view-connect pb-view "x-markers" dt-ax-view "x-markers")
    (guppi-element-view-connect pb-view "y-markers" pr-ax-view "y-markers")

    (guppi-group-view-layout-horizontal-fill root-view title-view 0 0)
    (guppi-group-view-layout-flush-top root-view title-view 0)
    (guppi-group-view-layout-natural-height root-view title-view)
    
    (guppi-group-view-layout-vbox2 root-view pb-view dt-ax-view 0)
    (guppi-group-view-layout-hbox2 root-view pr-ax-view pb-view 0)

    (guppi-group-view-layout-natural-height root-view dt-ax-view)
    (guppi-group-view-layout-natural-width root-view pr-ax-view)

    (guppi-group-view-layout-vertically-adjacent root-view title-view pb-view 0)
    (guppi-group-view-layout-flush-right root-view pb-view 0)
    (guppi-group-view-layout-flush-bottom root-view dt-ax-view 0)
    (guppi-group-view-layout-flush-left root-view pr-ax-view 0)

    (guppi-group-view-layout-same-place root-view pb-view ln-view)
    (guppi-group-view-layout-same-place root-view pb-view ln2-view)
    
    (guppi-root-group-view-show-in-window root-view)
    )
)

(show-stock-chart "msft" "MSFT: Microsoft")

