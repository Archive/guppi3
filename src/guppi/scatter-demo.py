from Guppi import *
from Guppi.plot import *

def plot_demo():
    root_state = GuppiRootGroupState()
    root_view = root_state.make_view()
    
    xybox_state = GuppiXYBoxState()
    scatter_state = GuppiScatterState()
    linegraph_state = GuppiLinegraphState()
    frame_state = GuppiFrameState()
    slinreg_state = GuppiSlinregState()
    n_ax_state = GuppiAxisState()
    s_ax_state = GuppiAxisState()
    e_ax_state = GuppiAxisState()
    w_ax_state = GuppiAxisState()
    title_state = GuppiTextState()
    x_boxplot_state = GuppiBoxplotState()
    y_boxplot_state = GuppiBoxplotState()

    xybox_view = xybox_state.make_view()
    scatter_view = scatter_state.make_view()
    linegraph_view = linegraph_state.make_view()
    frame_view = frame_state.make_view()
    slinreg_view = slinreg_state.make_view()
    n_ax_view = n_ax_state.make_view()
    s_ax_view = s_ax_state.make_view()
    e_ax_view = e_ax_state.make_view()
    w_ax_view = w_ax_state.make_view()
    title_view = title_state.make_view()
    x_boxplot_view = x_boxplot_state.make_view()
    y_boxplot_view = y_boxplot_state.make_view()

    gap = 3.6

    xybox_state.set_label("The Magic X Y Box")
    root_state.set_label("Scatter Demo")
    scatter_state.set_label("Scatter Plot")
    slinreg_state.set_label("Linear Regression")
    n_ax_state.set_label("Upper x-axis")
    s_ax_state.set_label("Lower x-axis")
    e_ax_state.set_label("Right y-axis")
    w_ax_state.set_label("Lift y-axis")
    title_state.set_label("Plot Title")
    x_boxplot_state.set_label("X-Boxplot")
    y_boxplot_state.set_label("Y-Boxplot")

    root_view.set_size(72 * 7, 72 * 5)

    title_state.set_text("Guppi: A Gnome Plotting Tool")
    title_state.set_font_size(20)
    #title_state.set_rot_angle(5)

    x_boxplot_state.set_horizontal(1)
    y_boxplot_state.set_vertical(1)

    scatter_state.connect("x-data", slinreg_state, "x-data")
    scatter_state.connect("y-data", slinreg_state, "y-data")

    scatter_state.connect("x-data", linegraph_state, "x-data")
    scatter_state.connect("y-data", linegraph_state, "y-data")

    ss = scatter_state.get_style(0)
    ss.set_marker_size1(5)

    x_boxplot_state.connect("misc-data", scatter_state, "x-data")
    n_ax_state.connect("misc-data", scatter_state, "x-data")
    s_ax_state.connect("misc-data", scatter_state, "x-data")

    y_boxplot_state.connect("misc-data", scatter_state, "y-data")
    w_ax_state.connect("misc-data", scatter_state, "y-data")
    e_ax_state.connect("misc-data", scatter_state, "y-data")

    n_ax_state.set_position("north")
    s_ax_state.set_position("south")
    e_ax_state.set_position("east")
    w_ax_state.set_position("west")

    n_ax_state.set_show_legend(0)
    e_ax_state.set_show_legend(0)
    
    xybox_view.connect("x-view", n_ax_view, "x-view")
    xybox_view.connect("x-view", s_ax_view, "x-view")
    xybox_view.connect("y-view", e_ax_view, "y-view")
    xybox_view.connect("y-view", w_ax_view, "y-view")

    xybox_view.set_x_axis_marker_type(1)
    xybox_view.set_y_axis_marker_type(1)

    xybox_view.connect("x-markers", n_ax_view, "x-markers")
    xybox_view.connect("x-markers", s_ax_view, "x-markers")
    xybox_view.connect("y-markers", e_ax_view, "y-markers")
    xybox_view.connect("y-markers", w_ax_view, "y-markers")

    xybox_view.connect("x-view", x_boxplot_view, "x-view")
    xybox_view.connect("y-view", y_boxplot_view, "y-view")

    scatter_view.set_preferred_view()

    # Put our xy-ish objects into the xybox view
    xybox_view.add(frame_view)
    xybox_view.add(scatter_view)
    #xybox_view.add(linegraph_view)
    xybox_view.add(slinreg_view)

    # do layout

    root_view.layout_flush_top(title_view, gap)
    root_view.layout_horizontal_fill(title_view, 0, 0)
    root_view.layout_natural_height(title_view)

    root_view.layout_natural_height(x_boxplot_view)
    root_view.layout_natural_width(y_boxplot_view)

    root_view.layout_vertically_adjacent(title_view, x_boxplot_view, gap)

    root_view.layout_vbox2(x_boxplot_view, n_ax_view, gap)
    root_view.layout_hbox2(e_ax_view, y_boxplot_view, gap)

    root_view.layout_hbox3(w_ax_view, xybox_view, e_ax_view, gap)
    root_view.layout_vbox3(n_ax_view, xybox_view, s_ax_view, gap)

    root_view.layout_flush_left(w_ax_view, gap)
    root_view.layout_flush_right(y_boxplot_view, gap)
    root_view.layout_flush_bottom(s_ax_view, gap)

    root_view.layout_natural_height(n_ax_view)
    root_view.layout_natural_height(s_ax_view)
    root_view.layout_natural_height(w_ax_view)
    root_view.layout_natural_height(e_ax_view)
    
    # show-in-window automatically does a commit-changes on the view
    root_view.show_in_window()

print "Running demo..."
plot_demo()
print "Done"
