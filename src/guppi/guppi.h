/* $Id$ */

/*
 * guppi.h
 *
 * Copyright (C) 1999 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 * and Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_H
#define _INC_GUPPI_H

#include <config.h>
/* #include <gnome.h> */

#include <libgnome/gnome-defs.h>

#include <libgnomeui/gnome-app.h>
#include <libgnomeui/gnome-app-helper.h>

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS

void guppi_add_menubar_item (const gchar * path, const gchar * name);
void guppi_add_menu (const gchar * path, GnomeUIInfo * menuinfo);
void guppi_quit (void);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_H */

/* $Id$ */
