/* $Id$ */

/*
 * app_scm.c
 *
 * Copyright (C) 1999 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 * and Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
/* #include <gnome.h> */
#include <guppi-useful.h>
#include <guppi-guile.h>
#include "term.h"
#include "guppi.h"
#include "app_scm.h"

GUPPI_PROC (gver, "guppi-version", 0, 0, 0, ())
{
  return gh_str02scm (VERSION);
}

/**************************************************************************/

/* Application control */


GUPPI_PROC (open_term, "guppi-open-guile-terminal", 0, 0, 0, ())
{
  guile_terminal ();
  return SCM_UNSPECIFIED;
}

GUPPI_PROC (close_term, "guppi-close-guile-terminal", 0, 0, 0, ())
{
  close_guile_terminal ();
  return SCM_UNSPECIFIED;
}

/**************************************************************************/


/* UI Customization/Extension */

/* Allow arbitrary guile code to be attached to custom menu items. */

/* By default, we add new menubar items directly to the right of "Help" */
GUPPI_PROC (addtree, "guppi-add-menubar-item", 1, 0, 0, (SCM scm_name))
{
  gchar *name;
  gint len;

  SCM_ASSERT (gh_string_p (scm_name), scm_name, SCM_ARG1, str_addtree);

  name = gh_scm2newstr (scm_name, &len);

  guppi_add_menubar_item (NULL, name);

  free (name);

  return SCM_UNSPECIFIED;
}

/* This allows us to place subtrees in a more general manner. */
GUPPI_PROC (addsubtree, "guppi-add-subtree-item",
	    2, 0, 0, (SCM scm_path, SCM scm_name))
{
  gchar *path;
  gchar *name;
  gint len;

  SCM_ASSERT (gh_string_p (scm_path), scm_path, SCM_ARG1, str_addsubtree);
  SCM_ASSERT (gh_string_p (scm_name), scm_name, SCM_ARG2, str_addsubtree);

  path = gh_scm2newstr (scm_path, &len);
  name = gh_scm2newstr (scm_name, &len);

  guppi_add_menubar_item (path, name);

  free (name);
  free (path);

  return SCM_UNSPECIFIED;
}

/* The thunk is executed when the menu item is selected.  Cool, huh? */
GUPPI_PROC (addmenu, "guppi-add-menu-item",
	    3, 0, 0, (SCM scm_position, SCM scm_name, SCM thunk))
{
  GnomeUIInfo item[] = {
    GNOMEUIINFO_ITEM_NONE (NULL, NULL, guppi_scm_thunk_cb),
    GNOMEUIINFO_END
  };

  gchar *pos;
  gchar *name;
  gint len;

  SCM_ASSERT (gh_string_p (scm_position), scm_position, SCM_ARG1,
	      str_addmenu);
  SCM_ASSERT (gh_string_p (scm_name), scm_name, SCM_ARG2, str_addmenu);
  SCM_ASSERT (gh_procedure_p (thunk), thunk, SCM_ARG3, str_addmenu);

  pos = gh_scm2newstr (scm_position, &len);
  name = gh_scm2newstr (scm_name, &len);

  item[0].label = item[0].hint = name;
  item[0].user_data = (gpointer) thunk;

  guppi_add_menu (pos, item);
  free (pos);
  free (name);

  return SCM_UNSPECIFIED;
}

void
guppi_app_scm_init (void)
{
#include "app_scm.x"
}



/* $Id$ */
