/*
 * python-load.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 * Havoc Pennington <hp@pobox.com> and 
 * Andrew H. Chatham <andrew.chatham@duke.edu
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_PYTHON_LOAD_H
#define _INC_PYTHON_LOAD_H

/* #include <gnome.h> */
#include <libgnomeui/gnome-app.h>

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS

void python_select_and_load (GnomeApp *);
void python_load (GnomeApp *, const gchar * filename);
void python_load_add_recent (GnomeApp * app, const gchar * filename);

END_GUPPI_DECLS

#endif /* _INC_PYTHON_LOAD_H */

/* $Id$ */
