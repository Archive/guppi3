/* $Id$ */

/*
 * about.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
/* #include <gnome.h> */

#include <gtk/gtksignal.h>
#include <gtk/gtktable.h>
#include <gtk/gtkseparator.h>
#include <gtk/gtkhseparator.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkvbox.h>

#include <libgnomeui/gnome-dialog.h>
#include <libgnomeui/gnome-stock.h>
#include <libgnomeui/gnome-href.h>

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>

#include <guppi-useful.h>
#include "about.h"

/*
 * This is all fairly hacky.  Shame on me.
 */

#define TITLE_FONT "-adobe-helvetica-bold-r-normal--*-180-*-*-*-*-iso8859-1"
#define COPYR_FONT "-adobe-helvetica-medium-r-normal--*-80-*-*-*-*-iso8859-1"
#define HEADER_FONT "-adobe-helvetica-bold-r-normal--*-140-*-*-*-*-iso8859-1"
#define FONT NULL

static void
style_realize (GtkWidget * w)
{
  GtkStyle *style;
  const gchar *f =
    (const gchar *) gtk_object_get_data (GTK_OBJECT (w), "gupfont");
  const gchar *fg =
    (const gchar *) gtk_object_get_data (GTK_OBJECT (w), "gupfg");
  const gchar *bg =
    (const gchar *) gtk_object_get_data (GTK_OBJECT (w), "gupbg");

  GdkFont *font = NULL;
  GdkColor fgcolor, bgcolor;

  style = gtk_style_copy (gtk_widget_get_style (w));

  if (f) {
    font = gdk_font_load (f);
    if (font)
      style->font = font;
  }

  if (fg && gdk_color_parse (fg, &fgcolor)) {
    style->fg[GTK_STATE_NORMAL] = fgcolor;
  }

  if (bg && gdk_color_parse (bg, &bgcolor)) {
    style->bg[GTK_STATE_NORMAL] = bgcolor;
  }

  gtk_widget_set_style (w, style);
}

static void
set_style (GtkWidget * w, const gchar * f,
	   const gchar * fg_color, const gchar * bg_color)
{
  if (f != NULL)
    gtk_object_set_data (GTK_OBJECT (w), "gupfont", (gpointer) f);

  if (fg_color != NULL)
    gtk_object_set_data (GTK_OBJECT (w), "gupfg", (gpointer) fg_color);

  if (bg_color != NULL)
    gtk_object_set_data (GTK_OBJECT (w), "gupbg", (gpointer) bg_color);

  gtk_signal_connect (GTK_OBJECT (w),
		      "realize", GTK_SIGNAL_FUNC (style_realize), NULL);
}

static gint *
permutation (gint N)
{
  gint i, j, k;
  gint *p = guppi_new (gint, N);

  for (i = 0; i < N; ++i)
    p[i] = i;

  srand ((int) time (NULL));

  for (i = N - 1; i > 0; --i) {
    j = rand () % (i + 1);
    k = p[i];
    p[i] = p[j];
    p[j] = k;
  }

  return p;
}

static GtkWidget *
credits (void)
{
  const gchar **developers = guppi_developers ();
  const gchar **helpers = guppi_helpers ();
  gint dN = 0, hN = 0;
  gint *devel_perm;
  gint *helper_perm;
  gint cols = 3, rows, r, i, j;
  GtkTable *table;
  GtkWidget *w;
  GtkWidget *frame;
  GtkAttachOptions opts = GTK_EXPAND | GTK_FILL;
  gint xpad = 2, ypad = 1;

  while (developers[dN] != NULL)
    dN++;
  while (helpers[hN] != NULL)
    hN++;

  /* This isn't about egos, it is about the software. */
  devel_perm = permutation (dN);
  helper_perm = permutation (hN);

  rows = 3;
  rows += dN / cols + (dN % cols ? 1 : 0);
  rows += hN / cols + (hN % cols ? 1 : 0);

  table = GTK_TABLE (gtk_table_new (rows, cols, FALSE));
  set_style (GTK_WIDGET (table), NULL, NULL, NULL);

  r = 0;

  w = gtk_label_new (_("Guppi Developers"));
  set_style (w, HEADER_FONT, NULL, NULL);
  gtk_table_attach (table, w, 0, cols, r, r + 1, opts, opts, xpad, ypad);;
  gtk_widget_show (w);
  ++r;

  j = 0;
  for (i = 0; i < dN; ++i) {
    w = gtk_label_new (developers[devel_perm[i]]);
    set_style (w, FONT, NULL, NULL);
    gtk_table_attach (table, w, j, j + 1, r, r + 1, opts, opts, xpad, ypad);
    gtk_widget_show (w);
    ++j;
    if (j == cols && i != dN - 1) {
      j = 0;
      ++r;
    }
  }
  ++r;

  w = gtk_hseparator_new ();
  gtk_table_attach (table, w, 0, cols, r, r + 1, opts, opts, xpad, ypad);;
  gtk_widget_show (w);
  ++r;

  w = gtk_label_new (_("Friends of Guppi"));
  set_style (w, HEADER_FONT, NULL, NULL);
  gtk_table_attach (table, w, 0, cols, r, r + 1, opts, opts, xpad, ypad);;
  gtk_widget_show (w);
  ++r;

  j = 0;
  for (i = 0; i < hN; ++i) {
    w = gtk_label_new (helpers[helper_perm[i]]);
    set_style (w, FONT, NULL, NULL);
    gtk_table_attach (table, w, j, j + 1, r, r + 1, opts, opts, xpad, ypad);;
    gtk_widget_show (w);
    ++j;
    if (j == cols && i != hN - 1) {
      j = 0;
      ++r;
    }
  }
  ++r;

  frame = gtk_frame_new (NULL);
  gtk_container_add (GTK_CONTAINER (frame), GTK_WIDGET (table));
  gtk_widget_show (GTK_WIDGET (table));

  guppi_free (devel_perm);
  guppi_free (helper_perm);

  return frame;
}

GtkWidget *
guppi_about (void)
{
  GtkWidget *dialog;
  GtkWidget *cred;
  GtkWidget *w;
  const gchar **strv;
  gchar *s;
  gint i;

  dialog = gnome_dialog_new (_("About Guppi"), GNOME_STOCK_BUTTON_OK, NULL);
  gnome_dialog_set_close (GNOME_DIALOG (dialog), TRUE);

  s = guppi_strdup_printf ("Guppi %s", guppi_version ());
  w = gtk_label_new (s);
  guppi_free (s);
  set_style (w, TITLE_FONT, "red", "blue");
  gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox),
		      w, TRUE, TRUE, 0);
  gtk_widget_show (w);



  strv = guppi_copyright (); 
  if (strv) {
    w = gtk_vbox_new (FALSE, 0);
    for (i=0; strv[i]; ++i) {
      GtkWidget *lab = gtk_label_new (strv[i]);
      set_style (lab, COPYR_FONT, NULL, NULL);
      gtk_box_pack_start (GTK_BOX (w), lab, TRUE, TRUE, 0);
    }
    gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox),
			w, TRUE, TRUE, 0);
    gtk_widget_show_all (w);
  }


  w =
    gtk_label_new (_
		   ("This is free software, distributed under the terms of the GNU General Public License.\n"));
  set_style (w, COPYR_FONT, NULL, NULL);
  gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox),
		      w, TRUE, TRUE, 0);
  gtk_widget_show (w);



  cred = credits ();
  gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox),
		      cred, TRUE, TRUE, 0);
  gtk_widget_show (cred);


  w = gnome_href_new ("http://www.gnome.org/guppi",
		      _("Visit the Guppi Home Page"));
  gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox), w, TRUE, TRUE,
		      0);
  gtk_widget_show (w);


  gtk_widget_show (dialog);

  return dialog;
}

/* $Id$ */
