/* $Id$ */

/*
 * file-open.c
 *
 * Copyright (C) 1999 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 * and Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
/* #include <gnome.h> */
#include <gtk/gtktogglebutton.h>
#include <gtk/gtkradiobutton.h>
#include <gtk/gtkbox.h>
#include <gtk/gtkfilesel.h>

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-util.h>

#include <libgnomeui/gnome-dialog.h>
#include <libgnomeui/gnome-stock.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <guppi-convenient.h>
#include <guppi-dialogs.h>
#include <guppi-data.h>
#include <guppi-data-tree.h>
#include <guppi-file.h>
#include <guppi-data-importer.h>
#include <guppi-data-importer-plug-in.h>

#include "file-open.h"
#include "guppi-app.h"

#define CANNOT_OPEN_FILE _("Can't open file \"%s\"")
#define CANNOT_OPEN_FILE_WITH_REASON _("Can't open file \"%s\": %s")

static void
import_hook (GuppiData *d, gpointer user_data)
{
  guppi_data_tree_add (guppi_data_tree_main (), d);
}

static void
post_hook (gint count, gpointer ptr)
{
  GuppiDataImporter *imp = GUPPI_DATA_IMPORTER (ptr);
  GuppiApp *app;
  const gchar *filename = NULL;

  if (count == 0) {
    guppi_warning_dialog (_("No data imported."));
    return;
  }

  app = GUPPI_APP (gtk_object_get_data (GTK_OBJECT (imp), "app"));
  filename = guppi_data_importer_filename (imp);
  if (filename)
    guppi_app_add_recently_visited_file (app, filename);
}

/**************************************************************************/

static void
do_import_config (GuppiDataImporterPlugIn * dipi, GuppiFile * file,
		  GuppiApp * app)
{
  GuppiDataImporter *imp;
  gchar *filename;

  g_return_if_fail (dipi != NULL);
  g_return_if_fail (file != NULL);

  g_return_if_fail (dipi->construct != NULL);
  imp = (dipi->construct) ();
  g_return_if_fail (imp != NULL);

  gtk_object_set_data (GTK_OBJECT (imp), "app", app);

  filename = guppi_strdup (guppi_file_filename (file));
  if (filename) {
    guppi_free (app->most_recent_open_directory);
    *(g_filename_pointer (filename)) = '\0';
    app->most_recent_open_directory = filename;
  }

  guppi_data_importer_set_source (imp, file);

  guppi_data_importer_import_interactive (imp,
					  import_hook, NULL,
					  post_hook, imp);

  guppi_unref (imp);
}

/**************************************************************************/

struct import_gadget {
  GuppiFile *file;
  GuppiDataImporterPlugIn *selected_plug_in;
  GuppiApp *app;
};

static void
on_radio_toggled (GtkToggleButton * b, struct import_gadget *gadget)
{
  if (gtk_toggle_button_get_active (b)) {
    gadget->selected_plug_in =
      GUPPI_DATA_IMPORTER_PLUG_IN (gtk_object_get_user_data (GTK_OBJECT (b)));
  }
}

static void
on_plugin_select_dialog_clicked (GnomeDialog * dialog, gint n,
				 struct import_gadget *gadget)
{
  GuppiDataImporterPlugIn *dipi = gadget->selected_plug_in;
  GuppiFile *file = gadget->file;
  gtk_widget_destroy (GTK_WIDGET (dialog));
  if (n == 0 && dipi)
    do_import_config (dipi, file, gadget->app);

}

static void
on_plugin_select_dialog_close (GnomeDialog * dialog, gpointer gadget)
{
  guppi_free (gadget);
}

void
file_open_ask_about_import_type (const gchar * filename,
				 gboolean force_ask,
				 double threshold, GuppiApp * app)
{
  GuppiFile *file;
  const gint buffer_size = 256;
  gchar buffer[256];
  gint peek_size;
  GList *assessment;
  GList *iter;
  GtkWidget *dialog;
  GtkWidget *rb;
  GtkWidget *first_rb;
  GuppiDataImporterAssessment *impass;
  GuppiPlugIn *pi;
  GuppiDataImporterPlugIn *dipi;
  struct import_gadget *gadget = NULL;
  gboolean unitary_assessment_count = 0, assessment_count = 0;
  GuppiDataImporterAssessment *last_unitary_assessment = NULL;

  g_return_if_fail (filename != NULL);

  file = guppi_file_open (filename);
  if (file == NULL) {
    guppi_error_dialog (CANNOT_OPEN_FILE, filename);
    return;
  }

  peek_size = guppi_file_peek (file, buffer, buffer_size);
  assessment = guppi_data_importer_plug_in_assess (filename,
						   peek_size ? buffer : NULL,
						   peek_size);

  if (assessment == NULL) {
    guppi_error_dialog (CANNOT_OPEN_FILE_WITH_REASON, 
			filename, _("Unknown file type"));
    return;
  }

  /* Scan through all of our possible plug-ins */

  for (iter = assessment; iter; iter = g_list_next (iter)) {
    impass = (GuppiDataImporterAssessment *)iter->data;
    if (impass->confidence > 0.999999) {
      ++unitary_assessment_count;
      last_unitary_assessment = impass;
    }
    ++assessment_count;
  }


  /*
    Check if one importer stands out in particular, and if so we just
    use it without asking. 

    If there isn't a way to override this, it could get annoying...
  */

  impass = NULL;

  if (assessment_count == 1) {

    /* If only one plug-in seems possible, don't ask --- just use it. */
    impass = (GuppiDataImporterAssessment *) assessment->data;

  } else if (unitary_assessment_count == 1) {

    /* If there is only one *unitary* assessment, use it w/o asking. */
    impass = last_unitary_assessment;

  }

  if (impass) {
    do_import_config (impass->plug_in, file, app);
    guppi_free (assessment->data);
    g_list_free (assessment);
    return;
  }


  /* If all else fails, ask. */

  dialog = gnome_dialog_new (_("Select Import Method"),
			     GNOME_STOCK_BUTTON_OK,
			     GNOME_STOCK_BUTTON_CANCEL, NULL);

  gadget = guppi_new (struct import_gadget, 1);
  gadget->file = file;
  gadget->selected_plug_in = NULL;
  gadget->app = app;

  first_rb = rb = NULL;
  for (iter = assessment; iter != NULL; iter = g_list_next (iter)) {
    impass = (GuppiDataImporterAssessment *) iter->data;
    if (impass->confidence >= threshold) {
      dipi = impass->plug_in;
      pi = GUPPI_PLUG_IN (dipi);

      if (rb == NULL) {
	first_rb = rb = gtk_radio_button_new_with_label (NULL,
							 impass->spec->name);
	gadget->selected_plug_in = dipi;
      } else {
	rb =
	  gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (rb),
						       impass->spec->name);
      }
      gtk_object_set_user_data (GTK_OBJECT (rb), dipi);
      gtk_signal_connect (GTK_OBJECT (rb),
			  "toggled",
			  GTK_SIGNAL_FUNC (on_radio_toggled), gadget);
      gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox),
			  rb, TRUE, TRUE, 0);
    }
  }

  if (first_rb)
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (first_rb), TRUE);

  gtk_signal_connect (GTK_OBJECT (dialog),
		      "clicked",
		      GTK_SIGNAL_FUNC (on_plugin_select_dialog_clicked),
		      gadget);
  gtk_signal_connect (GTK_OBJECT (dialog),
		      "close",
		      GTK_SIGNAL_FUNC (on_plugin_select_dialog_close),
		      gadget);
  gtk_widget_show_all (dialog);


  /* Deallocate list */
  for (iter = assessment; iter != NULL; iter = g_list_next (iter))
    guppi_free (iter->data);
  g_list_free (assessment);


}

/**************************************************************************/


static void
on_file_ok_clicked (GtkButton * b, GtkFileSelection * fs)
{
  gchar *filename;
  struct stat buf;
  GuppiApp *app;

  filename = gtk_file_selection_get_filename (fs);

  if (stat (filename, &buf)) {
    guppi_warning_dialog (CANNOT_OPEN_FILE, filename);
    gtk_widget_destroy (GTK_WIDGET (fs));
  } else if (S_ISDIR (buf.st_mode)) {
    filename = guppi_strdup_printf ("%s/", filename);
    gtk_file_selection_set_filename (fs, filename);
    guppi_free (filename);
  } else {
    app = GUPPI_APP (gtk_object_get_data (GTK_OBJECT (fs), "app"));
    filename = guppi_strdup (filename);
    gtk_widget_destroy (GTK_WIDGET (fs));

    file_open_ask_about_import_type (filename, FALSE,
				     DEFAULT_FILE_OPEN_THRESHOLD, app);

    guppi_free (filename);
  }
}

void
file_open (GtkWidget * w, GuppiApp * app)
{
  GtkWidget *file_selector;

  file_selector = gtk_file_selection_new (_("Select File To Open"));
  gtk_object_set_data (GTK_OBJECT (file_selector), "app", app);

  gtk_file_selection_hide_fileop_buttons (GTK_FILE_SELECTION (file_selector));

  if (app->most_recent_open_directory)
    gtk_file_selection_set_filename (GTK_FILE_SELECTION (file_selector),
				     app->most_recent_open_directory);

  gtk_signal_connect (GTK_OBJECT
		      (GTK_FILE_SELECTION (file_selector)->ok_button),
		      "clicked", GTK_SIGNAL_FUNC (on_file_ok_clicked),
		      file_selector);

  gtk_signal_connect_object (GTK_OBJECT
			     (GTK_FILE_SELECTION (file_selector)->
			      cancel_button), "clicked",
			     GTK_SIGNAL_FUNC (gtk_widget_destroy),
			     GTK_OBJECT (file_selector));

  gtk_widget_show_all (GTK_WIDGET (file_selector));
}

/* $Id$ */
