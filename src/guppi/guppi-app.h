/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-app.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_APP_H
#define _INC_GUPPI_APP_H

#include <config.h>
/* #include <gnome.h> */

#include <libgnomeui/gnome-app.h>

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS

typedef struct _GuppiApp GuppiApp;
typedef struct _GuppiAppClass GuppiAppClass;

#define RECENT_FILE_MAX 12
typedef struct _RecentFile RecentFile;

struct _GuppiApp {
  GnomeApp parent;

  gchar *most_recent_open_directory;
  RecentFile *recent_file[RECENT_FILE_MAX];
};

struct _GuppiAppClass {
  GnomeAppClass parent_class;
};

#define GUPPI_TYPE_APP (guppi_app_get_type())
#define GUPPI_APP(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_APP,GuppiApp))
#define GUPPI_APP0(obj) ((obj) ? (GUPPI_APP(obj)) : NULL)
#define GUPPI_APP_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_APP,GuppiAppClass))
#define GUPPI_IS_APP(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_APP))
#define GUPPI_IS_APP0(obj) (((obj) == NULL) || (GUPPI_IS_APP(obj)))
#define GUPPI_IS_APP_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_APP))

GtkType guppi_app_get_type (void);

GtkWidget *guppi_app_new (void);

void guppi_app_add_recently_visited_file (GuppiApp * app,
					  const gchar * filename);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_APP_H */

/* $Id$ */
