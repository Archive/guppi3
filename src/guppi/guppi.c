/* $Id$ */
/*
 * guppi.c
 *
 * Copyright (C) 1999 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 * and Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
/* #include <gnome.h> */
#include <glade/glade.h>
#include <signal.h>
#include <time.h>
#include <guppi-data-tree-view.h>
#include "corba_guppi.h"
#include <guppi-useful.h>
#include <guppi-dharma.h>
#include <guppi-gpl.h>
#include <guppi-scripting.h>
#include <guppi-data-init.h>
#include <guppi-data-ui-init.h>
#include <guppi-stat-init.h>
#include <guppi-eval-init.h>
#include <guppi-plot-init.h>
#include <guppi-plot-plug-in.h>
#ifdef USING_BONOBO
#  include <guppi-bonobo.h>
#  include <liboaf/liboaf.h>
#endif
#include "splash.h"
#include "term.h"
#include "file-open.h"
#include "plug-ins.h"
#include "guile-load.h"
#include "guppi-app.h"
#include "guppi.h"

static GnomeApp *guppi_app = NULL;

static void
dummy_uib_signal_connector (GnomeUIInfo * foo, gchar * bar,
			    GnomeUIBuilderData * foobar)
{
}

void
guppi_add_menubar_item (const gchar * path, const gchar * name)
{
  GnomeUIInfo item[] = {
    GNOMEUIINFO_SUBTREE (NULL, NULL),
    GNOMEUIINFO_END
  };

  GnomeUIBuilderData uibdata = { dummy_uib_signal_connector,
    NULL, FALSE, NULL, NULL
  };

  GtkWidget *parent;
  gint pos;

  g_return_if_fail (name != NULL);

  item[0].label = (gchar *) name;
  /* We need a pointer to a GNOMEUIINFO_END to put in as the contents
     of the empty item we just built, and item[1] is just sitting
     there looking very tempting... */
  item[0].moreinfo = &item[1];

  if (path == NULL || *path == '\0')
    parent = gnome_app_find_menu_pos (guppi_app->menubar, "Help", &pos);
  else
    parent = gnome_app_find_menu_pos (guppi_app->menubar, path, &pos);

  g_return_if_fail (parent != NULL);

  gnome_app_fill_menu_custom (GTK_MENU_SHELL (parent), item, &uibdata,
			      guppi_app->accel_group, TRUE, pos - 1);
}

void
guppi_add_menu (const gchar * path, GnomeUIInfo * menuinfo)
{
  gnome_app_insert_menus (guppi_app, path, menuinfo);
}


/**************************************************************************/

static void
load_rc_files (void)
{
  gchar buffer[1024];
  const gint buffer_len = 1024;

  g_snprintf (buffer, buffer_len, "%s/.guppirc", gnome_util_user_home ());
  if (g_file_exists (buffer)) {
    guppi_splash_message (_("Loading .guppirc"), "");
    guppi_safe_load (buffer);
  }

  strncpy (buffer, "./.guppilocalrc", buffer_len);
  if (g_file_exists (buffer)) {
    guppi_splash_message (_("Loading .guppilocalrc"), "");
    guppi_safe_load (buffer);
  }
}

/**************************************************************************/

/*****  Signal handlers  *****/

/*
  On the first try, we attempt a graceful exit.
  If a second attempt comes in soon enough, we give up and abort.
*/
static void
exit_signal_handler (gint x)
{
  static time_t then = 0;
  time_t now;

  time (&now);
  if (now - then > 10) {
    guppi_exit ();
    then = now;
  } else {
    guppi_abort ();
  }
}

static void
abort_signal_handler (gint x)
{
  guppi_abort ();
}

static void
setup_signal_handlers (void)
{
  struct sigaction xsh, ash;

  /* Trap ctrl-c from the starting console */
  xsh.sa_handler = exit_signal_handler;
  xsh.sa_flags = 0;

  ash.sa_handler = abort_signal_handler;
  ash.sa_flags = 0;

  sigaction (SIGTERM, &ash, NULL);
  sigaction (SIGINT, &xsh, NULL);
  sigaction (SIGHUP, &ash, NULL);
}

/**************************************************************************/

static gboolean batch_mode = FALSE;
static gboolean dump_paths = FALSE;
static gchar *opt_script_filename = NULL;

#ifdef USING_BONOBO
static gint bonobo_server_flag = 0;
#endif

static void
app_destroy (gpointer ptr)
{
  gtk_widget_destroy (GTK_WIDGET (ptr));
}

static void
real_main (void *closure, int argc, char *argv[])
{
  /* We have many, many libraries and misc. things to initialize */
  guppi_splash_message (_("Initializing"), _("libglade"));
  glade_gnome_init ();

  guppi_splash_message (_("Initializing"), _("Utility Library"));
  guppi_useful_init ();
  guppi_dharma_turn_wheel_automatically ();

  guppi_splash_message (_("Initializing"), _("Statistics Library"));
  guppi_stat_init ();

  guppi_splash_message (_("Initializing"), _("Data Library"));
  guppi_data_init ();

  guppi_splash_message (_("Initializing"), _("Data UI Library"));
  guppi_data_ui_init ();

  guppi_splash_message (_("Initializing"), _("Eval Library"));
  guppi_eval_init ();

  guppi_splash_message (_("Initializing"), _("Plot Library"));
  guppi_plot_init ();

  if (!batch_mode) {
    guppi_splash_message (_("Initializing"), _("Main Guppi Window"));
    guppi_app = GNOME_APP (guppi_app_new ());
    guppi_exit_connect_shutdown_func (app_destroy, guppi_app,
				      "Destroy App Window");
  }

  /* We load our rc files after building our app window so that we can
     freely monkey with the app's UI. */
  load_rc_files ();

  if (dump_paths) {
    guppi_plug_in_path_dump ();
  }

  guppi_splash_message (_("Scanning For Plug-Ins"), "");
  guppi_plug_in_load_all ();

  /* Check if we managed to find any plug-ins. */
  {
    gboolean problem = FALSE;

    if (guppi_plug_in_count () == 0) {
      problem = TRUE;
      g_warning ("No plug-ins found.");
    }

    if (guppi_plug_in_count_by_type ("plot") == 0) {
      problem = TRUE;
      g_warning ("No plot plug-ins found.");
    }

    if (guppi_plug_in_count_by_type ("data_import") == 0) {
      problem = TRUE;
      g_warning ("No data import plug-ins found.");
    }

    if (guppi_plug_in_count_by_type ("data_impl") == 0) {
      problem = TRUE;
      g_warning ("No data implementation plug-ins found.");
    }

    if (problem) {
      g_print ("\n\n");
      g_print
	("This lack of plug-ins is probably caused by an incorrectly\n");
      g_print ("installed or incorrectly configured Guppi.\n\n");
    }
  }

#ifdef USING_BONOBO
  if (oaf_activation_iid_get () || bonobo_server_flag) {
    g_message ("Activiating canned demo factory for bonobo");
    guppi_bonobo_canned_demo_init ();
  } else {
    if (guppi_is_very_verbose ()) {
      g_message ("Not initializing bonobo server");
    }
  }
#endif

  guppi_splash_destroy ();

  if (opt_script_filename != NULL) {
    g_message ("evaling %s", opt_script_filename);
    guppi_execute_script (opt_script_filename);
  }

  if (batch_mode) {
    guppi_exit ();
  } else {
#ifdef USING_BONOBO
    if (!oaf_activation_iid_get ())
      gtk_widget_show_all (GTK_WIDGET (guppi_app));
    bonobo_main ();
#else
    gtk_widget_show_all (GTK_WIDGET (guppi_app));
    gtk_main ();
#endif
  }
}

int
main (int argc, char *argv[])
{
  gint opt_batch_flag, opt_sync_flag = 0, opt_memtrace_flag = 0;
  gint opt_verb_s = 0, opt_verb_v = 0, opt_verb_vv = 0;
  gint gpl_dump = 0;

  struct poptOption options[] = {

    {"script", 's', POPT_ARG_STRING, &opt_script_filename, 0,
     N_("Load and evaluate code from FILENAME"), N_("FILENAME")},

    {"batch", 'b', POPT_ARG_NONE, &opt_batch_flag, 0,
     N_("Run in batch mode"), NULL},

    {"silent", '\0', POPT_ARG_NONE, &opt_verb_s, 0,
     N_("Run quietly, with no console message"), NULL},

    {"verbose", '\0', POPT_ARG_NONE, &opt_verb_v, 0,
     N_("Verbose console message"), NULL},

    {"very-verbose", '\0', POPT_ARG_NONE, &opt_verb_vv, 0,
     N_("Extremely verbose console message"), NULL},

    {"sync-debug", '\0', POPT_ARG_NONE, &opt_sync_flag, 0,
     N_("Make Guppi run synchronously (useful for debugging)"), NULL},

    {"memory-trace", '\0', POPT_ARG_NONE, &opt_memtrace_flag, 0,
     N_("Run Guppi with memory tracing enabled"), NULL},

#ifdef USING_BONOBO
    {"bonobo-server", '\0', POPT_ARG_NONE, &bonobo_server_flag, 0,
     N_("Allow Guppi to act as a Bonobo server."), NULL},
#endif

    {"license", '\0', POPT_ARG_NONE, &gpl_dump, 0,
     N_
     ("Describe the GNU GPL, the licence which Guppi is distributed under."),
     NULL},

    {"paths", '\0', POPT_ARG_NONE, &dump_paths, 0,
     N_("Dump file search paths."), NULL},

    {NULL, '\0', 0, NULL, 0, NULL, NULL}
  };
  poptContext pctx;
  gint i;


  /* We start up the non-graphical bits of guppi first, and don't
     do anything that needs an X server until we are sure that we are
     not going to be running in batch mode. */

  setup_signal_handlers ();

  bindtextdomain (PACKAGE, GNOMELOCALEDIR);
  textdomain (PACKAGE);

  /* We manually check to make sure we aren't starting guppi in batch mode. */
  for (i = 1; i < argc && !batch_mode; ++i) {
    if (!strcmp (argv[i], "-b") || !strcmp (argv[i], "--batch"))
      batch_mode = TRUE;
    if (!strcmp (argv[i], "--license"))
      gpl_dump = 1;
  }

#ifdef USING_BONOBO
  gnomelib_register_popt_table (oaf_popt_options, "oaf");
#endif

  if (batch_mode && gpl_dump) {

    /* Note: Corba isn't getting initialized. */
    gtk_type_init ();
    gnomelib_init (PACKAGE, VERSION);
    gnomelib_register_popt_table (options, PACKAGE);
    gnomelib_parse_args (argc, argv, 0);

  } else {

    guppi_corba_init (&argc, argv, options, 0, &pctx);
    guppi_exit_connect_shutdown_func (guppi_corba_shutdown, NULL,
				      "CORBA Shutdown");

  }

  if (opt_verb_s)
    guppi_set_verbosity (GUPPI_SILENT);
  else if (opt_verb_v)
    guppi_set_verbosity (GUPPI_VERBOSE);
  else if (opt_verb_vv)
    guppi_set_verbosity (GUPPI_VERY_VERBOSE);
  else
    guppi_set_verbosity (GUPPI_NORMAL_VERBOSITY);

  if (opt_sync_flag)
    guppi_set_synchronous ();

  if (opt_memtrace_flag)
    guppi_memory_trace (TRUE);

  if (guppi_is_not_silent () && !batch_mode)
    guppi_splash_text ();

  if (!batch_mode && !gpl_dump) {
#ifdef USING_BONOBO
    if (!oaf_activation_iid_get ())
#endif
      guppi_splash_create ();
  }

  if (gpl_dump) {
    FILE *out;
    out = popen ("less", "w");
    if (out == NULL)
      out = popen ("more", "w");
    guppi_print_gpl (out ? out : stdout);
    if (out)
      pclose (out);
    exit (0);
  }


  guppi_splash_message (_("Initializing"), _("Guile"));
  scm_boot_guile (argc, argv, real_main, NULL);

  g_assert_not_reached ();
  return 1;
}


/* $Id$ */
