from Guppi import *
from Guppi.plot import *

def view_fully_connect(a, b):
    a.connect("x-view", b, "x-view")
    a.connect("y-view", b, "y-view")
    a.connect("x-markers", b, "x-markers")
    a.connect("x-markers", b, "x-markers")

def plot_demo():
    root_state = GuppiRootGroupState()
    scatter_state = GuppiScatterState()
    frame_state = GuppiFrameState()
    slinreg_state = GuppiSlinregState()
    n_ax_state = GuppiAxisState()
    s_ax_state = GuppiAxisState()
    e_ax_state = GuppiAxisState()
    w_ax_state = GuppiAxisState()
    title_state = GuppiTextState()
    x_boxplot_state = GuppiBoxplotState()
    y_boxplot_state = GuppiBoxplotState()
    
    root_view = root_state.make_view()
    scatter_view = scatter_state.make_view()
    frame_view = frame_state.make_view()
    slinreg_view = slinreg_state.make_view()
    n_ax_view = n_ax_state.make_view()
    s_ax_view = s_ax_state.make_view()
    e_ax_view = e_ax_state.make_view()
    w_ax_view = w_ax_state.make_view()
    title_view = title_state.make_view()
    x_boxplot_view = x_boxplot_state.make_view()
    y_boxplot_view = y_boxplot_state.make_view()

    gap = 3.6

    root_state.set_label("A Silly Demo")
    scatter_state.set_label("Scatter Plot")
    title_state.set_label("Title")
    slinreg_state.set_label("Linear Regression")

    title_state.set_text("Guppi: A Gnome Plotting Tool")
    title_state.set_font_size(20)

    x_boxplot_state.set_horizontal(1)
    y_boxplot_state.set_vertical(1)

    scatter_state.connect("x-data", slinreg_state, "x-data")
    scatter_state.connect("y-data", slinreg_state, "y-data")

    ss = scatter_state.get_style(0)
    ss.set_marker_size1(5)

    x_boxplot_state.connect("misc-data", scatter_state, "x-data")
    n_ax_state.connect("misc-data", scatter_state, "x-data")
    s_ax_state.connect("misc-data", scatter_state, "x-data")

    y_boxplot_state.connect("misc-data", scatter_state, "y-data")
    w_ax_state.connect("misc-data", scatter_state, "y-data")
    e_ax_state.connect("misc-data", scatter_state, "y-data")

    n_ax_state.set_position('north')
    s_ax_state.set_position('south')
    e_ax_state.set_position('east')
    w_ax_state.set_position('west')

    #axis default for show-legend is true
    #n_ax_state.set_show_legend(0)
    #e_ax_state.set_show_legend(0)

    view_fully_connect(scatter_view, frame_view)
    view_fully_connect(scatter_view, slinreg_view)

    scatter_view.connect("x-view", n_ax_view, "x-view")
    scatter_view.connect("x-view", s_ax_view, "x-view")
    scatter_view.connect("y-view", e_ax_view, "y-view")
    scatter_view.connect("y-view", w_ax_view, "y-view")

    scatter_view.connect("x-markers", n_ax_view, "x-markers")
    scatter_view.connect("x-markers", s_ax_view, "x-markers")
    scatter_view.connect("y-markers", e_ax_view, "y-markers")
    scatter_view.connect("y-markers", w_ax_view, "y-markers")

    scatter_view.connect("x-view", x_boxplot_view, "x-view")
    scatter_view.connect("y-view", y_boxplot_view, "y-view")

    scatter_view.set_preferred_view()

    # do layout

    root_view.layout_flush_top(title_view, gap)
    root_view.layout_horizontal_fill(title_view, 0, 0)
    root_view.layout_natural_height(title_view)

    root_view.layout_same_place(frame_view, slineg_view)
    root_view.layout_same_place(frame_view, scatter_view)

    root_view.layout_natural_height(x_boxplot_view)
    root_view.layout_natural_width(y_boxplot_view)

    root_view.layout_vertically_adjacent(title_view, x_boxplot_view, gap)

    root_view.layout_vbox2(x_boxplot_view, n_ax_view, gap)
    root_view.layout_hbox2(e_ax_view, y_boxplot_view, gap)

    root_view.layout_hbox3(w_ax_view, scatter_view, e_ax_view, gap)
    root_view.layout_vbox3(n_ax_view, scatter_view, s_ax_view, gap)

    root_view.layout_flush_left(w_ax_view, gap)
    root_view.layout_flush_right(y_boxplot_view, gap)
    root_view.layout_flush_bottom(s_ax_view, gap)

    root_view.layout_natural_height(n_ax_view)
    root_view.layout_natural_height(s_ax_view)
    root_view.layout_natural_width(w_ax_view)
    root_view.layout_natural_width(e_ax_view)

    root_view.commit_changes()

    root_view.show_in_window()
    return (root_view, w_ax_view)

atxt_state = GuppiTextState()
atxt_view = atxt_state.make_view()
atxt_state.set_text("Aieeee!!!")

root_view, ax_view = plot_demo()
root_view.replace(ax_view, atxt_view)
root_view.commit_changes()


