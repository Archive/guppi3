from Guppi import *
from Guppi.plot import *

def view_fully_connect(a, b):
    a.connect("x-view", b, "x-view")
    a.connect("y-view", b, "y-view")
    a.connect("x-markers", b, "x-markers")
    a.connect("x-markers", b, "x-markers")

def guppi_scatter_group():
    group_state = GuppiGroupState()
    scatter_state = GuppiScatterState()
    frame_state = GuppiFrameState()
    slinreg_state = GuppiSlinregState()
    n_ax_state = GuppiAxisState()
    s_ax_state = GuppiAxisState()
    e_ax_state = GuppiAxisState()
    w_ax_state = GuppiAxisState()
    title_state = GuppiTextState()
    x_boxplot_state = GuppiBoxplotState()
    y_boxplot_state = GuppiBoxplotState()
    
    group_view = group_state.make_view()
    scatter_view = scatter_state.make_view()
    frame_view = frame_state.make_view()
    slinreg_view = slinreg_state.make_view()
    n_ax_view = n_ax_state.make_view()
    s_ax_view = s_ax_state.make_view()
    e_ax_view = e_ax_state.make_view()
    w_ax_view = w_ax_state.make_view()
    title_view = title_state.make_view()
    x_boxplot_view = x_boxplot_state.make_view()
    y_boxplot_view = y_boxplot_state.make_view()

    gap = 3.6

    title_state.set_text("Graphs Everywhere!")
    title_state.set_font_size(16)
    
    x_boxplot_state.set_horizontal(1)
    y_boxplot_state.set_vertical(1)

    scatter_state.connect("x-data", slinreg_state, "x-data")
    scatter_state.connect("y-data", slinreg_state, "y-data")

    ss = scatter_state.get_style(0)
    ss.set_marker_size1(5)

    x_boxplot_state.connect("misc-data", scatter_state, "x-data")
    n_ax_state.connect("misc-data", scatter_state, "x-data")
    s_ax_state.connect("misc-data", scatter_state, "x-data")

    y_boxplot_state.connect("misc-data", scatter_state, "y-data")
    w_ax_state.connect("misc-data", scatter_state, "y-data")
    e_ax_state.connect("misc-data", scatter_state, "y-data")

    n_ax_state.set_position('north')
    s_ax_state.set_position('south')
    e_ax_state.set_position('east')
    w_ax_state.set_position('west')

    #axis default for show-legend is true
    #n_ax_state.set_show_legend(0)
    #e_ax_state.set_show_legend(0)

    view_fully_connect(scatter_view, frame_view)
    view_fully_connect(scatter_view, slinreg_view)

    scatter_view.connect("x-view", n_ax_view, "x-view")
    scatter_view.connect("x-view", s_ax_view, "x-view")
    scatter_view.connect("y-view", e_ax_view, "y-view")
    scatter_view.connect("y-view", w_ax_view, "y-view")

    scatter_view.connect("x-markers", n_ax_view, "x-markers")
    scatter_view.connect("x-markers", s_ax_view, "x-markers")
    scatter_view.connect("y-markers", e_ax_view, "y-markers")
    scatter_view.connect("y-markers", w_ax_view, "y-markers")

    scatter_view.connect("x-view", x_boxplot_view, "x-view")
    scatter_view.connect("y-view", y_boxplot_view, "y-view")

    scatter_view.set_preferred_view()

    # do layout

    group_view.layout_flush_top(title_view, gap)
    group_view.layout_horizontal_fill(title_view, 0, 0)
    group_view.layout_natural_height(title_view)

    group_view.layout_same_place(frame_view, slinreg_view)
    group_view.layout_same_place(frame_view, scatter_view)

    group_view.layout_natural_height(x_boxplot_view)
    group_view.layout_natural_width(y_boxplot_view)

    group_view.layout_vertically_adjacent(title_view, x_boxplot_view, gap)

    group_view.layout_vbox2(x_boxplot_view, n_ax_view, gap)
    group_view.layout_hbox2(e_ax_view, y_boxplot_view, gap)

    group_view.layout_hbox3(w_ax_view, scatter_view, e_ax_view, gap)
    group_view.layout_vbox3(n_ax_view, scatter_view, s_ax_view, gap)

    group_view.layout_flush_left(w_ax_view, gap)
    group_view.layout_flush_right(y_boxplot_view, gap)
    group_view.layout_flush_bottom(s_ax_view, gap)

    group_view.layout_natural_height(n_ax_view)
    group_view.layout_natural_height(s_ax_view)
    group_view.layout_natural_width(w_ax_view)
    group_view.layout_natural_width(e_ax_view)

    group_view.commit_changes()

    return group_view

def grid2x2 (a, b, c, d):
    s = GuppiGroupState()
    v = s.make_view()

    v.layout_same_height(a, b)
    v.layout_same_height(b, c)
    v.layout_same_height(c, d)

    v.layout_same_width(a, b)
    v.layout_same_width(b, c)
    v.layout_same_width(c, d)

    v.layout_hbox2(a, b, 0)
    v.layout_hbox2(c, d, 0)

    v.layout_vbox2(a, c, 0)
    v.layout_vbox2(b, d, 0)

    v.layout_flush_left(a, 0)
    v.layout_flush_top(a, 0)

    v.layout_flush_right(d, 0)
    v.layout_flush_bottom(d, 0)
    v.commit_changes()

    return v

def scatter2x2():
    return grid2x2(guppi_scatter_group(),
                   guppi_scatter_group(),
                   guppi_scatter_group(),
                   guppi_scatter_group())

def grid4x4 ():
    return grid2x2 (scatter2x2(),
                    scatter2x2(),
                    scatter2x2(),
                    scatter2x2())

rs = GuppiRootGroupState()
rv = rs.make_view()

sg1 = guppi_scatter_group()
sg2 = guppi_scatter_group()
sg3 = guppi_scatter_group()
sg4 = guppi_scatter_group()

grid4 = grid4x4()
grid4.commit_changes()
rv.layout_horizontal_fill(grid4, 0, 0)
rv.layout_vertical_fill(grid4, 0, 0)

rv.commit_changes()

rv.show_in_window()
rv.set_size(20 * 72, 20 * 72)


