/* $Id$ */

/*
 * guile-load.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 * and Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
/* #include <gnome.h> */
#include <gtk/gtkbutton.h>
#include <gtk/gtkfilesel.h>

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>

#include <libgnomeui/gnome-app.h>
#include <libgnomeui/gnome-app-helper.h>

#include <guppi-memory.h>
#include <guppi-guile.h>
#include "guile-load.h"

static void
on_file_ok_clicked (GtkButton * b, gpointer fs_ptr)
{
  GtkFileSelection *fs;
  gchar *filename;
  GnomeApp *app;

  fs = GTK_FILE_SELECTION (fs_ptr);
  filename = guppi_strdup (gtk_file_selection_get_filename (fs));
  app = GNOME_APP (gtk_object_get_user_data (GTK_OBJECT (fs)));
  gtk_widget_destroy (GTK_WIDGET (fs));
  guile_load (app, filename);
  guppi_free (filename);
}

void
guile_select_and_load (GnomeApp * app)
{
  GtkFileSelection *file_selector;


  file_selector =
    GTK_FILE_SELECTION (gtk_file_selection_new
			(_("Select Guile Source File")));

  gtk_object_set_user_data (GTK_OBJECT (file_selector), app);

  gtk_signal_connect (GTK_OBJECT (file_selector->ok_button),
		      "clicked",
		      GTK_SIGNAL_FUNC (on_file_ok_clicked), file_selector);
  gtk_signal_connect_object (GTK_OBJECT (file_selector->cancel_button),
			     "clicked",
			     GTK_SIGNAL_FUNC (gtk_widget_destroy),
			     GTK_OBJECT (file_selector));

  gtk_widget_show_all (GTK_WIDGET (file_selector));
}

void
guile_load (GnomeApp * app, const gchar * filename)
{
  /* We should look at the return value of guppi_safe_load and display
     error messages, if any, in a dialog. */
  guppi_safe_load ((gchar *) filename);

  /* guile_load_add_recent(app, filename); */
}

static void
guile_file_menu_cb (GtkObject * foo, gpointer filename)
{
  GnomeApp *app;

  app = GNOME_APP (gtk_object_get_user_data (foo));
  guile_load (app, (gchar *) filename);
}

/*
  This code should update/maintain a "recently visited guile sources" list
  in the Scripting menu.

  What we have so far adds a new item (with an ugly, over-long label)
  on each load.  A finished version should
  * be less hacky
  * take care of duplicates
  * put an upper bound on the number of recently-visited files we display
  * save them in the config info, so that they can be saved and restored
    between sessions
*/

void
guile_load_add_recent (GnomeApp * app, const gchar * filename)
{
  GnomeUIInfo item[] = {
    GNOMEUIINFO_ITEM_NONE (NULL, NULL, guile_file_menu_cb),
    GNOMEUIINFO_END
  };

  g_return_if_fail (app != NULL);
  g_return_if_fail (filename != NULL);
  item[0].label = guppi_strdup (filename);
  item[0].user_data = guppi_strdup (filename);

  // Insert new item
  gnome_app_insert_menus (app, "Scripting/<Separator>", item);

  gtk_object_set_user_data (GTK_OBJECT (item[0].widget), app);
}

/* $Id$ */
