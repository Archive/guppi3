/* $Id$ */

/*
 * plug-ins.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 * and Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <gtk/gtkclist.h>
#include <gtk/gtkwindow.h>
#include <gtk/gtkscrolledwindow.h>
#include <gtk/gtkbox.h>

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>

#include <libgnomeui/gnome-dialog.h>
#include <libgnomeui/gnome-stock.h>

#include <guppi-useful.h>
#include "plug-ins.h"

static void
add_plug_in (GuppiPlugInSpec * spec, gpointer data)
{
  gchar *text[4];
  GtkCList *clist = GTK_CLIST (data);

  text[0] = guppi_plug_in_is_loaded (spec->type, spec->code) ? "X" : "";
  text[1] = (gchar *) spec->type;
  text[2] = (gchar *) spec->name;
  /* This is a memory leak. */
  text[3] = guppi_strdup_printf ("%d.%d.%d",
			     spec->major_version,
			     spec->minor_version, spec->micro_version);
  gtk_clist_append (clist, text);
}

void
loaded_plug_ins (void)
{
  GtkWidget *dialog;
  GtkWidget *clist;
  GtkWidget *swin;
  gchar *clist_titles[] = { N_("Loaded"), 
			    N_("Type"),
			    N_("Name"), 
			    N_("Version") };

  dialog = gnome_dialog_new (_("Loaded Plug-Ins"),
			     GNOME_STOCK_BUTTON_OK, NULL);
  gnome_dialog_set_close (GNOME_DIALOG (dialog), TRUE);
  gtk_window_set_policy (GTK_WINDOW (dialog), TRUE, TRUE, FALSE);

  clist = gtk_clist_new_with_titles (4, clist_titles);
  gtk_clist_set_column_auto_resize (GTK_CLIST (clist), 0, TRUE);
  gtk_clist_set_column_auto_resize (GTK_CLIST (clist), 1, TRUE);
  gtk_clist_set_column_auto_resize (GTK_CLIST (clist), 2, TRUE);
  gtk_clist_set_column_auto_resize (GTK_CLIST (clist), 3, TRUE);
  guppi_plug_in_spec_foreach (add_plug_in, clist);

  swin = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (swin),
				  GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
  gtk_widget_set_usize (swin, 300, 100);

  gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (swin), clist);

  gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox), swin, TRUE, TRUE,
		      0);

  gtk_widget_show_all (dialog);
}


/* $Id$ */
