/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-var-int.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <guppi-memory.h>
#include "guppi-var-private.h"
#include "guppi-var-int.h"

typedef struct _InfoInt InfoInt;
struct _InfoInt {
  gint x;
  gint min, max;
};

static gboolean
int_get_bool (GuppiVar *v)
{
  InfoInt *info = v->priv->object_data;
  return info->x != 0;
}

static gint
int_get_int (GuppiVar *v)
{
  InfoInt *info = v->priv->object_data;
  return info->x;
}

static gboolean
int_set_int (GuppiVar *v, gint x)
{
  InfoInt *info = v->priv->object_data;
  if (info->min <= x && x <= info->max) {
    gint old = info->x;
    info->x = x;
    return x != old;
  }
  v->priv->is_valid = FALSE;
  return FALSE;
}

static double
int_get_double (GuppiVar *v)
{
  InfoInt *info = v->priv->object_data;
  return info->x;
}

static void
int_finalizer (GuppiVar *v)
{
  guppi_free (v->priv->object_data);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
state2widget (GuppiVar *v, GtkSpinButton *w)
{
  gtk_spin_button_set_value (w, int_get_double (v));
}

static void
widget2state (GtkSpinButton *w, GuppiVar *v)
{
  guppi_var_set_int (v, gtk_spin_button_get_value_as_int (w));
}

static void
int_destroy (GtkWidget *w, GuppiVar *v)
{
  guint changed_handle = GPOINTER_TO_UINT (gtk_object_get_data (GTK_OBJECT (w), "sig"));
  gtk_signal_disconnect (GTK_OBJECT (v), changed_handle);
  guppi_unref (v);
}


static GtkWidget *
int_widget (GuppiVar *v)
{
  InfoInt *info = v->priv->object_data;
  GtkObject *adj;
  GtkWidget *w;
  guint changed_handle;

  adj = gtk_adjustment_new (info->x,
			    info->min, info->max,
			    1, 10,
			    info->max - info->min);
			    
  w = gtk_spin_button_new (GTK_ADJUSTMENT (adj), 1, 0);
  gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (w), TRUE);

  changed_handle = gtk_signal_connect (GTK_OBJECT (v),
				       "changed",
				       (GtkSignalFunc) state2widget,
				       w);
  gtk_object_set_data (GTK_OBJECT (w), "sig", GUINT_TO_POINTER (changed_handle));

  gtk_signal_connect (GTK_OBJECT (w),
		      "changed",
		      (GtkSignalFunc) widget2state,
		      v);

  guppi_ref (v);
  gtk_signal_connect (GTK_OBJECT (w),
		      "destroy",
		      (GtkSignalFunc) int_destroy,
		      v);
  
  return w;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

GuppiVar *
guppi_var_new_int_bounded (gint min, gint max)
{
  GuppiVar *v = guppi_type_new (guppi_var_get_type ());
  v->priv->object_data = guppi_new0 (InfoInt, 1);

  ((InfoInt * )v->priv->object_data)->x = CLAMP (0, min, max);
  ((InfoInt *) v->priv->object_data)->min = min;
  ((InfoInt *) v->priv->object_data)->max = max;

  v->priv->get_bool   = int_get_bool;
  v->priv->get_int    = int_get_int;
  v->priv->set_int    = int_set_int;
  v->priv->get_double = int_get_double;
  v->priv->finalizer  = int_finalizer;
  v->priv->widget     = int_widget;

  return v;
}

GuppiVar *
guppi_var_new_int (void)
{
  return guppi_var_new_int_bounded (G_MININT, G_MAXINT);
}

GuppiVar *
guppi_var_new_int_positive (void)
{
  return guppi_var_new_int_bounded (0, G_MAXINT);
}

GuppiVar *
guppi_var_new_int_non_negative (void)
{
  return guppi_var_new_int_bounded (1, G_MAXINT);
}
