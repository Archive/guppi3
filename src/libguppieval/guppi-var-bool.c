/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-var-bool.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <guppi-i18n.h>
#include <guppi-memory.h>
#include "guppi-var-private.h"
#include "guppi-var-bool.h"

static gboolean
bool_get_bool (GuppiVar *v)
{
  return (gboolean) GPOINTER_TO_INT (v->priv->object_data);
}

static gboolean
bool_set_bool (GuppiVar *v, gboolean x)
{
  gboolean old = (gboolean) GPOINTER_TO_INT (v->priv->object_data);
  v->priv->object_data = GINT_TO_POINTER ((gint) x);

  return old != x;
}

static gint
bool_get_int (GuppiVar *v)
{
  return bool_get_bool (v) ? 1 : 0;
}

static double
bool_get_double (GuppiVar *v)
{
  return bool_get_bool (v) ? 1.0 : 0.0;
}

static void
state2widget (GuppiVar *v, GtkWidget *w)
{
  gtk_option_menu_set_history (GTK_OPTION_MENU (w), guppi_var_get_bool (v) ? 0 : 1);
}

static void
widget2state (GtkWidget *w, GuppiVar *v)
{
  guppi_var_set_bool (v, gtk_object_get_data (GTK_OBJECT (w), "bool") != NULL);
}

static void
widget_destroy (GtkWidget *w, GuppiVar *v)
{
  guint changed_handle;
  changed_handle = GPOINTER_TO_UINT (gtk_object_get_data (GTK_OBJECT (w), "sig"));
  gtk_signal_disconnect (GTK_OBJECT (v), changed_handle);
  guppi_unref (v);
}

static GtkWidget *
bool_widget (GuppiVar *v)
{
  GtkWidget *opt, *menu, *mi;
  guint changed_handle;

  menu = gtk_menu_new ();

  mi = gtk_menu_item_new_with_label (_("True"));
  gtk_object_set_data (GTK_OBJECT (mi), "bool", mi);
  gtk_menu_append (GTK_MENU (menu), mi);
  gtk_widget_show (mi);
  gtk_signal_connect (GTK_OBJECT (mi),
		      "activate",
		      (GtkSignalFunc) widget2state,
		      v);

  mi = gtk_menu_item_new_with_label (_("False"));
  gtk_object_set_data (GTK_OBJECT (mi), "bool", NULL);
  gtk_menu_append (GTK_MENU (menu), mi);
  gtk_widget_show (mi);
  gtk_signal_connect (GTK_OBJECT (mi),
		      "activate",
		      (GtkSignalFunc) widget2state,
		      v);
  
  opt = gtk_option_menu_new ();
  gtk_option_menu_set_menu (GTK_OPTION_MENU (opt), menu);
  gtk_widget_show (menu);

  state2widget (v, opt);

  changed_handle = gtk_signal_connect (GTK_OBJECT (v),
				       "changed",
				       (GtkSignalFunc) state2widget,
				       opt);
  gtk_object_set_data (GTK_OBJECT (opt), "sig", GUINT_TO_POINTER (changed_handle));

  guppi_ref (v);
  gtk_signal_connect (GTK_OBJECT (opt),
		      "destroy",
		      (GtkSignalFunc) widget_destroy,
		      v);

  return opt;
}

GuppiVar *
guppi_var_new_bool (void)
{
  GuppiVar *v = guppi_type_new (guppi_var_get_type ());

  v->priv->get_bool   = bool_get_bool;
  v->priv->set_bool   = bool_set_bool;
  v->priv->get_int    = bool_get_int;
  v->priv->get_double = bool_get_double;
  v->priv->widget     = bool_widget;

  bool_set_bool (v, FALSE); /* set default value */
  
  return v;
}
