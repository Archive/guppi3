/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-var.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_VAR_H
#define _INC_GUPPI_VAR_H

#include <gtk/gtk.h>
#include  "guppi-defs.h"
#include  "guppi-data.h"

BEGIN_GUPPI_DECLS;

typedef struct _GuppiVar GuppiVar;
typedef struct _GuppiVarClass GuppiVarClass;

struct _GuppiVarPrivate;

struct _GuppiVar {
  GtkObject parent;
  struct _GuppiVarPrivate *priv;
};

struct _GuppiVarClass {
  GtkObjectClass parent_class;

  void (*changed) (GuppiVar *);
};

#define GUPPI_TYPE_VAR (guppi_var_get_type ())
#define GUPPI_VAR(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_VAR,GuppiVar))
#define GUPPI_VAR0(obj) ((obj) ? (GUPPI_VAR(obj)) : NULL)
#define GUPPI_VAR_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_VAR,GuppiVarClass))
#define GUPPI_IS_VAR(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_VAR))
#define GUPPI_IS_VAR0(obj) (((obj) == NULL) || (GUPPI_IS_VAR(obj)))
#define GUPPI_IS_VAR_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_VAR))

#define g_return_unless_is_guppi_var(x) (g_return_if_fail((x)&&GUPPI_IS_VAR((x))))
#define g_return_val_unless_is_guppi_var(x,y) (g_return_val_if_fail((x)&&GUPPI_IS_VAR((x)),(y)))

GtkType guppi_var_get_type (void);

const gchar *guppi_var_label     (GuppiVar *);
void         guppi_var_set_label (GuppiVar *, const gchar *);

void         guppi_var_changed     (GuppiVar *);
void         guppi_var_invalidated (GuppiVar *);

GtkWidget   *guppi_var_edit_widget (GuppiVar *);

gboolean     guppi_var_is_valid   (GuppiVar *);
gboolean     guppi_var_is_invalid (GuppiVar *);

gboolean     guppi_var_is_defined (GuppiVar *);
gboolean     guppi_var_is_undef   (GuppiVar *);
void         guppi_var_set_undef  (GuppiVar *);

gboolean     guppi_var_can_get_bool (GuppiVar *);
gboolean     guppi_var_can_set_bool (GuppiVar *);
gboolean     guppi_var_get_bool     (GuppiVar *);
void         guppi_var_set_bool     (GuppiVar *, gboolean);

gboolean     guppi_var_can_get_int (GuppiVar *);
gboolean     guppi_var_can_set_int (GuppiVar *);
gint         guppi_var_get_int     (GuppiVar *);
void         guppi_var_set_int     (GuppiVar *, gint);

gboolean     guppi_var_can_get_double (GuppiVar *);
gboolean     guppi_var_can_set_double (GuppiVar *);
double       guppi_var_get_double     (GuppiVar *);
void         guppi_var_set_double     (GuppiVar *, double);

gboolean     guppi_var_can_get_string (GuppiVar *);
gboolean     guppi_var_can_set_string (GuppiVar *);
const gchar *guppi_var_get_string     (GuppiVar *);
void         guppi_var_set_string     (GuppiVar *, const gchar *str);

gboolean     guppi_var_can_get_data   (GuppiVar *);
gboolean     guppi_var_can_set_data   (GuppiVar *);
GuppiData   *guppi_var_get_data       (GuppiVar *);
void         guppi_var_set_data       (GuppiVar *, GuppiData *);

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_VAR_H */

/* $Id$ */
