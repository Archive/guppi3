/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-var-private.h
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __GUPPI_VAR_PRIVATE_H__
#define __GUPPI_VAR_PRIVATE_H__

#include <gtk/gtk.h>
#include  "guppi-defs.h"
#include "guppi-var.h"

BEGIN_GUPPI_DECLS;

typedef struct _GuppiVarPrivate GuppiVarPrivate;
struct _GuppiVarPrivate {
  gchar *label;
  gboolean   can_read, can_write, is_undef, is_valid;

  gpointer   object_data;

  gboolean   (*get_bool)   (GuppiVar *);
  gboolean   (*set_bool)   (GuppiVar *, gboolean);
  gint       (*get_int)    (GuppiVar *);
  gboolean   (*set_int)    (GuppiVar *, gint);
  double     (*get_double) (GuppiVar *);
  gboolean   (*set_double) (GuppiVar *, double);
  gchar     *(*get_string) (GuppiVar *);
  gboolean   (*set_string) (GuppiVar *, const gchar *);
  GuppiData *(*get_data)   (GuppiVar *);
  gboolean   (*set_data)   (GuppiVar *, GuppiData *);
  void       (*finalizer)  (GuppiVar *);
  GtkWidget *(*widget)     (GuppiVar *);
};

GuppiVarPrivate *guppi_var_private_new (void);
void             guppi_var_private_free (GuppiVarPrivate *);

END_GUPPI_DECLS;

#endif /* __GUPPI_VAR_PRIVATE_H__ */












