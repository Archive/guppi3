/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-var-double.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <guppi-memory.h>
#include "guppi-var-private.h"
#include "guppi-var-double.h"

typedef struct _InfoDouble InfoDouble;
struct _InfoDouble {
  double x;
  gboolean no_inf, no_sup;
  double inf, sup;
  gboolean allow_inf, allow_sup;
};

static gboolean
in_bound (InfoDouble *info, double x)
{
  return (info->no_inf || (info->allow_inf ? (info->inf <= x) : (info->inf < x)))
    && (info->no_sup || (info->allow_sup ? (x <= info->sup) : (x < info->sup)));
}

static gboolean
double_get_bool (GuppiVar *v)
{
  InfoDouble *info = v->priv->object_data;
  return info->x != 0.0;
}

static double
double_get_double (GuppiVar *v)
{
  InfoDouble *info = v->priv->object_data;
  return info->x;
}

static gboolean
double_set_double (GuppiVar *v, double x)
{
  InfoDouble *info = v->priv->object_data;

  if (in_bound (info, x)) {
    double old = info->x;
    info->x = x;
    return x != old;
  }
  v->priv->is_valid = FALSE;
  return FALSE;
}

static gboolean
double_set_int (GuppiVar *v, gint x)
{
  return double_set_double (v, (double) x);
}



static void
double_finalizer (GuppiVar *v)
{
  guppi_free (v->priv->object_data);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
state2widget (GuppiVar *v, GtkSpinButton *w)
{
  gtk_spin_button_set_value (w, double_get_double (v));
}

static void
widget2state (GtkSpinButton *w, GuppiVar *v)
{
  guppi_var_set_double (v, gtk_spin_button_get_value_as_float (w));
}

static void
double_destroy (GtkWidget *w, GuppiVar *v)
{
  guint changed_handle = GPOINTER_TO_UINT (gtk_object_get_data (GTK_OBJECT (w), "sig"));
  gtk_signal_disconnect (GTK_OBJECT (v), changed_handle);
  guppi_unref (v);
}

static GtkWidget *
double_widget (GuppiVar *v)
{
  InfoDouble *info = v->priv->object_data;
  GtkObject *adj;
  GtkWidget *w;
  guint changed_handle;

  adj = gtk_adjustment_new (info->x,
			    -100, 100, /* CRAP */
			    0.2, 1,
			    200);
			    
  w = gtk_spin_button_new (GTK_ADJUSTMENT (adj), 1, 1);
  gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (w), TRUE);

  changed_handle = gtk_signal_connect (GTK_OBJECT (v),
				       "changed",
				       (GtkSignalFunc) state2widget,
				       w);
  gtk_object_set_data (GTK_OBJECT (w), "sig", GUINT_TO_POINTER (changed_handle));

  gtk_signal_connect (GTK_OBJECT (w),
		      "changed",
		      (GtkSignalFunc) widget2state,
		      v);

  guppi_ref (v);
  gtk_signal_connect (GTK_OBJECT (w),
		      "destroy",
		      (GtkSignalFunc) double_destroy,
		      v);
  
  return w;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

GuppiVar *
guppi_var_new_double (void)
{
  GuppiVar *v = guppi_type_new (guppi_var_get_type ());
  InfoDouble *info = guppi_new0 (InfoDouble, 1);

  v->priv->object_data = info;
  info->x = 0;
  info->no_inf = info->no_sup = TRUE;

  v->priv->get_bool   = double_get_bool;
  v->priv->set_int    = double_set_int;
  v->priv->get_double = double_get_double;
  v->priv->set_double = double_set_double;
  v->priv->finalizer  = double_finalizer;
  v->priv->widget     = double_widget;

  return v;
}
