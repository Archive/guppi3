/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-curve-fit.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <guppi-memory.h>
#include "guppi-curve-fit.h"

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0
};

enum {
  INITIALIZE_PARAMETERS,
  CHANGED_PARAMETERS,
  CREATE_CURVE,
  LAST_SIGNAL
};

static guint guppi_curve_fit_signals[LAST_SIGNAL] = { 0 };

typedef struct _GuppiCurveFitPrivate GuppiCurveFitPrivate;
struct _GuppiCurveFitPrivate {
  gchar *label;
  GuppiParameters *param;
  GuppiCurve *curve;
};

#define priv(x) ((GuppiCurveFitPrivate*)(GUPPI_CURVE_FIT((x))->opaque_internals))

static void
guppi_curve_fit_get_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_curve_fit_set_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_curve_fit_finalize (GtkObject *obj)
{
  GuppiCurveFit *x = GUPPI_CURVE_FIT(obj);
  GuppiCurveFitPrivate *p = priv (x);

  guppi_free0 (p->label);
  guppi_unref0 (p->param);
  guppi_unref0 (p->curve);

  g_free (x->opaque_internals);
  x->opaque_internals = NULL;

  guppi_finalized (obj);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

typedef gpointer (*GuppiSignal_POINTER__POINTER) (GtkObject *, gpointer, gpointer user_data);

static void
guppi_marshal_POINTER__POINTER (GtkObject *object, GtkSignalFunc func, gpointer func_data, GtkArg *args)
{
  GuppiSignal_POINTER__POINTER rfunc;
  gpointer *return_val;

  return_val = GTK_RETLOC_POINTER (args[1]);
  rfunc = (GuppiSignal_POINTER__POINTER) func;
  
  *return_val = (*rfunc) (object, GTK_VALUE_POINTER (args[0]), func_data);
  g_message ("marshalled %p", *return_val);
}

static void
guppi_curve_fit_class_init (GuppiCurveFitClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;

  parent_class = gtk_type_class (GTK_TYPE_OBJECT);

  object_class->get_arg = guppi_curve_fit_get_arg;
  object_class->set_arg = guppi_curve_fit_set_arg;
  object_class->finalize = guppi_curve_fit_finalize;

  guppi_curve_fit_signals[INITIALIZE_PARAMETERS] =
    gtk_signal_new ("initialize_parameters",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GuppiCurveFitClass, initialize_parameters),
                    gtk_marshal_NONE__POINTER,
		    GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

  guppi_curve_fit_signals[CHANGED_PARAMETERS] =
    gtk_signal_new ("changed_parameters",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GuppiCurveFitClass, changed_parameters),
                    gtk_marshal_NONE__POINTER,
		    GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

  guppi_curve_fit_signals[CREATE_CURVE] =
    gtk_signal_new ("create_curve",
                    GTK_RUN_LAST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GuppiCurveFitClass, create_curve),
                    guppi_marshal_POINTER__POINTER,
		    GTK_TYPE_POINTER, 1, GTK_TYPE_POINTER);

  gtk_object_class_add_signals (object_class, guppi_curve_fit_signals,
                                LAST_SIGNAL);
}

static void
guppi_curve_fit_init (GuppiCurveFit *obj)
{
  GuppiCurveFitPrivate *p = g_new0 (GuppiCurveFitPrivate, 1);
  obj->opaque_internals = p;

}

GtkType
guppi_curve_fit_get_type (void)
{
  static GtkType guppi_curve_fit_type = 0;
  if (!guppi_curve_fit_type) {
    static const GtkTypeInfo guppi_curve_fit_info = {
      "GuppiCurveFit",
      sizeof (GuppiCurveFit),
      sizeof (GuppiCurveFitClass),
      (GtkClassInitFunc)guppi_curve_fit_class_init,
      (GtkObjectInitFunc)guppi_curve_fit_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    guppi_curve_fit_type = gtk_type_unique (GTK_TYPE_OBJECT, &guppi_curve_fit_info);
  }
  return guppi_curve_fit_type;
}

GuppiCurveFit *
guppi_curve_fit_new (const gchar *label)
{
  GuppiCurveFit *fit = GUPPI_CURVE_FIT (guppi_type_new (guppi_curve_fit_get_type ()));

  priv (fit)->label = guppi_strdup (label);

  return fit;
}

const gchar *
guppi_curve_fit_label (GuppiCurveFit *fit)
{
  g_return_val_unless_is_guppi_curve_fit (fit, NULL);

  return priv (fit)->label;
}

static void
changed_proxy (GuppiParameters *param, GuppiVar *v, GuppiCurveFit *fit)
{
  gtk_signal_emit (GTK_OBJECT (fit), guppi_curve_fit_signals[CHANGED_PARAMETERS], param);
}

GuppiParameters *
guppi_curve_fit_parameters (GuppiCurveFit *fit)
{
  GuppiCurveFitPrivate *p;

  g_return_val_unless_is_guppi_curve_fit (fit, NULL);
  p = priv (fit);

  if (p->param == NULL) {
    p->param = guppi_parameters_new ();

    gtk_signal_emit (GTK_OBJECT (fit), guppi_curve_fit_signals[INITIALIZE_PARAMETERS], p->param);
    g_assert (guppi_parameters_count (p->param) > 0);

    gtk_signal_connect (GTK_OBJECT (p->param),
			"changed",
			GTK_SIGNAL_FUNC (changed_proxy),
			fit);

    { /* A silly hack */
      GtkWidget *win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
      GtkWidget *w = guppi_parameters_edit_widget (p->param);
      gtk_container_add (GTK_CONTAINER (win), w);
      gtk_widget_show_all (win);
    }
  }

  return p->param;
}

GuppiCurve *
guppi_curve_fit_curve (GuppiCurveFit *fit)
{
  GuppiCurveFitPrivate *p;

  g_return_val_unless_is_guppi_curve_fit (fit, NULL);
  p = priv (fit);

  if (p->curve == NULL) {
    GuppiParameters *param = guppi_curve_fit_parameters (fit);
    gtk_signal_emit (GTK_OBJECT (fit), guppi_curve_fit_signals[CREATE_CURVE], param, &p->curve);
    g_assert (p->curve != NULL && GUPPI_IS_CURVE (p->curve));
  }

  return p->curve;
}
