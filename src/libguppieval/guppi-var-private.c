/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-var-private.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include "guppi-var-private.h"

GuppiVarPrivate *
guppi_var_private_new (void)
{
  GuppiVarPrivate *p = guppi_new0 (GuppiVarPrivate, 1);

  p->can_read = TRUE;
  p->can_write = TRUE;
  p->is_undef = TRUE;
  
  return p;
}

void
guppi_var_private_free (GuppiVarPrivate *p)
{
  if (p) {
    if (p->finalizer)
      p->finalizer (p);
    guppi_free (p);
  }
}
