/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-parameters.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <guppi-memory.h>
#include "guppi-parameters.h"

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0
};

enum {
  CHANGED,
  LAST_SIGNAL
};

static guint guppi_parameters_signals[LAST_SIGNAL] = { 0 };

typedef struct _ParamPair ParamPair;
struct _ParamPair {
  gchar *cname;
  GuppiVar *v;
};

typedef struct _GuppiParametersPrivate GuppiParametersPrivate;
struct _GuppiParametersPrivate {
  GList *vlist;
};

#define priv(x) ((GuppiParametersPrivate*)(GUPPI_PARAMETERS((x))->opaque_internals))

static void
guppi_parameters_get_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_parameters_set_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
cleanup (gpointer data, gpointer foo)
{
  ParamPair *pp = (ParamPair *) data;

  gtk_signal_disconnect_by_data (GTK_OBJECT (pp->v), foo);
  guppi_free (pp->cname);
  guppi_unref (pp->v);
  guppi_free (pp);
}

static void
guppi_parameters_finalize (GtkObject *obj)
{
  GuppiParameters *x = GUPPI_PARAMETERS(obj);
  GuppiParametersPrivate *p = priv (x);

  g_list_foreach (p->vlist, cleanup, x);
  g_list_free (p->vlist);
  
  g_free (x->opaque_internals);
  x->opaque_internals = NULL;

  guppi_finalized (obj);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_parameters_class_init (GuppiParametersClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;

  parent_class = gtk_type_class (GTK_TYPE_OBJECT);

  object_class->get_arg = guppi_parameters_get_arg;
  object_class->set_arg = guppi_parameters_set_arg;
  object_class->finalize = guppi_parameters_finalize;

  /* Signal definition template */
  guppi_parameters_signals[CHANGED] =
    gtk_signal_new ("changed",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GuppiParametersClass, changed),
                    gtk_marshal_NONE__POINTER, GTK_TYPE_NONE,
		    1, GTK_TYPE_POINTER);


  gtk_object_class_add_signals (object_class, guppi_parameters_signals,
                                LAST_SIGNAL);
}

static void
guppi_parameters_init (GuppiParameters *obj)
{
  GuppiParametersPrivate *p = g_new0 (GuppiParametersPrivate, 1);
  obj->opaque_internals = p;

}

GtkType
guppi_parameters_get_type (void)
{
  static GtkType guppi_parameters_type = 0;
  if (!guppi_parameters_type) {
    static const GtkTypeInfo guppi_parameters_info = {
      "GuppiParameters",
      sizeof (GuppiParameters),
      sizeof (GuppiParametersClass),
      (GtkClassInitFunc)guppi_parameters_class_init,
      (GtkObjectInitFunc)guppi_parameters_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    guppi_parameters_type = gtk_type_unique (GTK_TYPE_OBJECT, &guppi_parameters_info);
  }
  return guppi_parameters_type;
}

GuppiParameters *
guppi_parameters_new (void)
{
  return GUPPI_PARAMETERS (guppi_type_new (guppi_parameters_get_type ()));
}

static void
changed_proxy (GuppiVar *v, GuppiParameters *param)
{
  gtk_signal_emit (GTK_OBJECT (param), guppi_parameters_signals[CHANGED], v);
}

void
guppi_parameters_add (GuppiParameters *param, const gchar *canonical_name, GuppiVar *v)
{
  ParamPair *pp;

  g_return_unless_is_guppi_parameters (param);
  g_return_if_fail (canonical_name && *canonical_name);
  g_return_unless_is_guppi_var (v);

  if (guppi_var_label (v) == NULL)
    guppi_var_set_label (v, _(canonical_name));

  guppi_ref (v);
  guppi_sink (v);

  pp = guppi_new0 (ParamPair, 1);
  pp->cname = guppi_strdup (canonical_name);
  pp->v = v;

  priv (param)->vlist = g_list_append (priv (param)->vlist, pp);

  gtk_signal_connect (GTK_OBJECT (v),
		      "changed",
		      GTK_SIGNAL_FUNC (changed_proxy),
		      param);
}

GuppiVar *
guppi_parameters_get (GuppiParameters *param, const gchar *cname)
{
  GList *i;

  g_return_val_unless_is_guppi_parameters (param, NULL);
  g_return_val_if_fail (cname && *cname, NULL);

  for (i = priv (param)->vlist; i != NULL; i = g_list_next (i)) {
    ParamPair *pp = (ParamPair *) i->data;
    if (!g_strcasecmp (pp->cname, cname))
      return pp->v;
  }
  
  return NULL;
}

gint
guppi_parameters_count (GuppiParameters *param)
{
  g_return_val_unless_is_guppi_parameters (param, 0);

  return g_list_length (priv (param)->vlist);
}

void
guppi_parameters_foreach_var (GuppiParameters *param,
			      GuppiParametersFn fn,
			      gpointer user_data)
{
  GList *i;

  g_return_unless_is_guppi_parameters (param);
  g_return_if_fail (fn != NULL);

  for (i = priv (param)->vlist; i != NULL; i = g_list_next (i)) {
    ParamPair *pp = (ParamPair *) i->data;
    fn (pp->cname, pp->v, user_data);
  }
}

GtkWidget *
guppi_parameters_edit_widget (GuppiParameters *param)
{
  GtkWidget *table;
  GList *i;
  gint j, N;

  g_return_val_unless_is_guppi_parameters (param, NULL);

  N = g_list_length (priv (param)->vlist);

  if (N == 0)
    return NULL;

  table = gtk_table_new (2, N, FALSE);

  j = 0;
  for (i = priv (param)->vlist; i != NULL; i = g_list_next (i)) {
    ParamPair *pp = (ParamPair *) i->data;
    GtkWidget *labelw, *editw;

    labelw = gtk_label_new (guppi_var_label (pp->v));
    editw  = guppi_var_edit_widget (pp->v);
    if (editw == NULL)
      editw = gtk_label_new ("(no widget)");

    gtk_table_attach (GTK_TABLE (table),
		      labelw,
		      0, 1, j, j+1,
		      GTK_EXPAND, GTK_EXPAND, 4, 4);


    gtk_table_attach (GTK_TABLE (table),
		      editw,
		      1, 2, j, j+1,
		      GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 4, 4);

    gtk_widget_show (labelw);
    gtk_widget_show (editw);

    ++j;
  }

  return table;
}
