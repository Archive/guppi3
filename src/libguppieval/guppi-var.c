/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-var.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <guppi-i18n.h>
#include <guppi-memory.h>
#include "guppi-var.h"
#include "guppi-var-private.h"

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0
};

enum {
  CHANGED,
  LAST_SIGNAL
};

static guint guppi_var_signals[LAST_SIGNAL] = { 0 };

static void
guppi_var_get_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_var_set_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_var_finalize (GtkObject *obj)
{
  GuppiVar *x = GUPPI_VAR(obj);

  guppi_free0 (x->priv->label);

  if (x->priv->finalizer)
    x->priv->finalizer (x);

  guppi_finalized (obj);

  guppi_free0 (x->priv);
  
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_var_class_init (GuppiVarClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;

  parent_class = gtk_type_class (GTK_TYPE_OBJECT);

  object_class->get_arg = guppi_var_get_arg;
  object_class->set_arg = guppi_var_set_arg;
  object_class->finalize = guppi_var_finalize;

  guppi_var_signals[CHANGED] =
    gtk_signal_new ("changed",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GuppiVarClass, changed),
                    gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);

  gtk_object_class_add_signals (object_class, guppi_var_signals,
                                LAST_SIGNAL);
}

static void
guppi_var_init (GuppiVar *obj)
{
  obj->priv = g_new0 (GuppiVarPrivate, 1);

  obj->priv->is_undef = TRUE;
  obj->priv->is_valid = TRUE;
  obj->priv->can_read = obj->priv->can_write = TRUE;
}

GtkType
guppi_var_get_type (void)
{
  static GtkType guppi_var_type = 0;
  if (!guppi_var_type) {
    static const GtkTypeInfo guppi_var_info = {
      "GuppiVar",
      sizeof (GuppiVar),
      sizeof (GuppiVarClass),
      (GtkClassInitFunc)guppi_var_class_init,
      (GtkObjectInitFunc)guppi_var_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    guppi_var_type = gtk_type_unique (GTK_TYPE_OBJECT, &guppi_var_info);
  }
  return guppi_var_type;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

const gchar *
guppi_var_label (GuppiVar *v)
{
  g_return_val_unless_is_guppi_var (v, NULL);
  return v->priv->label;
}

void
guppi_var_set_label (GuppiVar *v, const gchar *str)
{
  g_return_unless_is_guppi_var (v);
  guppi_free (v->priv->label);
  v->priv->label = guppi_strdup (str);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

void
guppi_var_changed (GuppiVar *v)
{
  g_return_unless_is_guppi_var (v);
  gtk_signal_emit (GTK_OBJECT (v), guppi_var_signals[CHANGED]);
}

GtkWidget *
guppi_var_edit_widget (GuppiVar *v)
{
  GuppiVarPrivate *p;
  g_return_val_unless_is_guppi_var (v, NULL);
  
  p = v->priv;

  return p ? p->widget (v) : NULL;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

gboolean
guppi_var_is_valid (GuppiVar *v)
{
  g_return_val_unless_is_guppi_var (v, FALSE);

  return v->priv->is_valid;
}

gboolean
guppi_var_is_invalid (GuppiVar *v)
{
  g_return_val_unless_is_guppi_var (v, FALSE);

  return ! v->priv->is_valid;
}

gboolean
guppi_var_is_defined (GuppiVar *v)
{
  g_return_val_unless_is_guppi_var (v, FALSE);

  return !v->priv->is_undef;
}

gboolean
guppi_var_is_undef (GuppiVar *v)
{
  g_return_val_unless_is_guppi_var (v, FALSE);

  return v->priv->is_undef;
}

void
guppi_var_set_undef (GuppiVar *v)
{
  g_return_unless_is_guppi_var (v);
  g_return_unless_is_guppi_var (v->priv->can_write);
  
  if (! v->priv->is_undef) {
    v->priv->is_valid = TRUE;
    v->priv->is_undef = TRUE;
    guppi_var_changed (v);
  }
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

gboolean
guppi_var_can_get_bool (GuppiVar *v)
{
  g_return_val_unless_is_guppi_var (v, FALSE);
  return v->priv->can_read && v->priv->get_bool != NULL;
}

gboolean
guppi_var_can_set_bool (GuppiVar *v)
{
  g_return_val_unless_is_guppi_var (v, FALSE);
  return v->priv->can_write && v->priv->set_bool != NULL;
}

gboolean
guppi_var_get_bool (GuppiVar *v)
{
  g_return_val_unless_is_guppi_var (v, FALSE);
  g_return_val_if_fail (guppi_var_can_get_bool (v), FALSE);

  return v->priv->get_bool (v);
}

void
guppi_var_set_bool (GuppiVar *v, gboolean x)
{
  g_return_unless_is_guppi_var (v);
  g_return_if_fail (guppi_var_can_set_bool (v));

  v->priv->is_valid = TRUE;
  if (v->priv->set_bool (v, x) || guppi_var_is_undef (v))
    guppi_var_changed (v);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

gboolean
guppi_var_can_get_int (GuppiVar *v)
{
  g_return_val_unless_is_guppi_var (v, FALSE);
  return v->priv->can_read && v->priv->get_int != NULL;
}

gboolean
guppi_var_can_set_int (GuppiVar *v)
{
  g_return_val_unless_is_guppi_var (v, FALSE);
  return v->priv->can_write && v->priv->set_int != NULL;
}

gint
guppi_var_get_int (GuppiVar *v)
{
  g_return_val_unless_is_guppi_var (v, FALSE);
  g_return_val_if_fail (guppi_var_can_get_int (v), 0);

  return v->priv->get_int (v);
}

void
guppi_var_set_int (GuppiVar *v, gint x)
{
  g_return_unless_is_guppi_var (v);
  g_return_if_fail (guppi_var_can_set_int (v));

  v->priv->is_valid = TRUE;
  if (v->priv->set_int (v, x) || guppi_var_is_undef (v))
    guppi_var_changed (v);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

gboolean
guppi_var_can_get_double (GuppiVar *v)
{
  g_return_val_unless_is_guppi_var (v, FALSE);
  return v->priv->can_read && v->priv->get_double != NULL;
}

gboolean
guppi_var_can_set_double (GuppiVar *v)
{
  g_return_val_unless_is_guppi_var (v, FALSE);
  return v->priv->can_write && v->priv->set_double != NULL;
}

double
guppi_var_get_double (GuppiVar *v)
{
  g_return_val_unless_is_guppi_var (v, FALSE);
  g_return_val_if_fail (guppi_var_can_get_double (v), 0);

  return v->priv->get_double (v);
}

void
guppi_var_set_double (GuppiVar *v, double x)
{
  g_return_unless_is_guppi_var (v);
  g_return_if_fail (guppi_var_can_set_double (v));

  v->priv->is_valid = TRUE;
  if (v->priv->set_double (v, x) || guppi_var_is_undef (v))
    guppi_var_changed (v);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

gboolean
guppi_var_can_get_string (GuppiVar *v)
{
  g_return_val_unless_is_guppi_var (v, FALSE);
  return v->priv->can_read && v->priv->get_string != NULL;
}

gboolean
guppi_var_can_set_string (GuppiVar *v)
{
  g_return_val_unless_is_guppi_var (v, FALSE);
  return v->priv->can_write && v->priv->set_string != NULL;
}

const gchar *
guppi_var_get_string (GuppiVar *v)
{
  g_return_val_unless_is_guppi_var (v, FALSE);
  g_return_val_if_fail (guppi_var_can_get_string (v), FALSE);

  return v->priv->get_string (v);
}

void
guppi_var_set_string (GuppiVar *v, const gchar *x)
{
  g_return_unless_is_guppi_var (v);
  g_return_if_fail (guppi_var_can_set_string (v));

  v->priv->is_valid = TRUE;
  if (v->priv->set_string (v, x) || guppi_var_is_undef (v))
    guppi_var_changed (v);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

gboolean
guppi_var_can_get_data (GuppiVar *v)
{
  g_return_val_unless_is_guppi_var (v, FALSE);
  return v->priv->can_read && v->priv->get_data != NULL;
}

gboolean
guppi_var_can_set_data (GuppiVar *v)
{
  g_return_val_unless_is_guppi_var (v, FALSE);
  return v->priv->can_write && v->priv->set_data != NULL;
}

GuppiData *
guppi_var_get_data (GuppiVar *v)
{
  g_return_val_unless_is_guppi_var (v, FALSE);
  g_return_val_if_fail (guppi_var_can_get_data (v), FALSE);

  return v->priv->get_data (v);
}

void
guppi_var_set_data (GuppiVar *v, GuppiData *x)
{
  g_return_unless_is_guppi_var (v);
  g_return_if_fail (guppi_var_can_set_data (v));

  v->priv->is_valid = TRUE;
  if (v->priv->set_data (v, x) || guppi_var_is_undef (v))
    guppi_var_changed (v);
}
