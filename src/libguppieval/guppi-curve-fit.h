/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-curve-fit.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_CURVE_FIT_H
#define _INC_GUPPI_CURVE_FIT_H

#include <gnome.h>
#include  "guppi-defs.h"
#include "guppi-parameters.h"
#include "guppi-curve.h"

BEGIN_GUPPI_DECLS;

typedef struct _GuppiCurveFit GuppiCurveFit;
typedef struct _GuppiCurveFitClass GuppiCurveFitClass;

struct _GuppiCurveFit {
  GtkObject parent;
  gpointer opaque_internals;
};

struct _GuppiCurveFitClass {
  GtkObjectClass parent_class;

  void        (*initialize_parameters) (GuppiCurveFit *, GuppiParameters *);
  void        (*changed_parameters)    (GuppiCurveFit *, GuppiParameters *);
  GuppiCurve *(*create_curve)          (GuppiCurveFit *, GuppiParameters *);
};

#define GUPPI_TYPE_CURVE_FIT (guppi_curve_fit_get_type ())
#define GUPPI_CURVE_FIT(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_CURVE_FIT,GuppiCurveFit))
#define GUPPI_CURVE_FIT0(obj) ((obj) ? (GUPPI_CURVE_FIT(obj)) : NULL)
#define GUPPI_CURVE_FIT_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_CURVE_FIT,GuppiCurveFitClass))
#define GUPPI_IS_CURVE_FIT(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_CURVE_FIT))
#define GUPPI_IS_CURVE_FIT0(obj) (((obj) == NULL) || (GUPPI_IS_CURVE_FIT(obj)))
#define GUPPI_IS_CURVE_FIT_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_CURVE_FIT))

#define g_return_unless_is_guppi_curve_fit(x) (g_return_if_fail((x)&&GUPPI_IS_CURVE_FIT((x))))
#define g_return_val_unless_is_guppi_curve_fit(x,y) (g_return_val_if_fail((x)&&GUPPI_IS_CURVE_FIT((x)),(y)))

GtkType guppi_curve_fit_get_type (void);

GuppiCurveFit   *guppi_curve_fit_new        (const gchar *label);
const gchar     *guppi_curve_fit_label      (GuppiCurveFit *);
GuppiParameters *guppi_curve_fit_parameters (GuppiCurveFit *);
GuppiCurve      *guppi_curve_fit_curve      (GuppiCurveFit *);

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_CURVE_FIT_H */

/* $Id$ */
