/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-var-data.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <guppi-memory.h>
#include <guppi-data-select.h>
#include "guppi-var-private.h"
#include "guppi-var-data.h"

typedef struct _InfoData InfoData;
struct _InfoData {
  GuppiData *data;
  GtkType type;
};

static gboolean
data_get_bool (GuppiVar *v)
{
  InfoData *info = v->priv->object_data;
  return info->data != NULL;
}

static GuppiData *
data_get_data (GuppiVar *v)
{
  InfoData *info = v->priv->object_data;
  return info->data;
}

static gboolean
data_set_data (GuppiVar *v, GuppiData *new_data)
{
  InfoData *info = v->priv->object_data;

  if (new_data == NULL 
      || gtk_type_is_a (GTK_OBJECT_TYPE (new_data), info->type)) {
    
    if (new_data != info->data) {
      guppi_refcounting_assign (info->data, new_data);
      if (new_data == NULL)
	v->priv->is_undef = TRUE;
      return TRUE;
    }
    return FALSE;
  }
  g_message ("Rejected %s", new_data ? gtk_type_name (GTK_OBJECT_TYPE (new_data)) : "null");
  v->priv->is_valid = FALSE;
  return FALSE;
}

static void
data_finalizer (GuppiVar *v)
{
  InfoData *info = v->priv->object_data;

  guppi_unref (info->data);
  guppi_free (info);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
state2widget (GuppiVar *v, GuppiDataSelect *w)
{
  InfoData *info = v->priv->object_data;
  guppi_data_select_set_selected_data (w, info->data);
}

static void
widget2state (GuppiDataSelect *w, GuppiData *d, GuppiVar *v)
{
  guppi_var_set_data (v, d);
}

static void
data_destroy (GtkWidget *w, GuppiVar *v)
{
  guint changed_handle = GPOINTER_TO_UINT (gtk_object_get_data (GTK_OBJECT (w), "sig"));
  gtk_signal_disconnect (GTK_OBJECT (v), changed_handle);
  guppi_unref (v);
}

static GtkWidget *
data_widget (GuppiVar *v)
{
  InfoData *info = v->priv->object_data;
  GtkWidget *w;
  guint changed_handle;

  if (info->type)
    w = guppi_data_select_new_by_type (info->type);
  else
    w = guppi_data_select_new ();

  state2widget (v, GUPPI_DATA_SELECT (w));

  changed_handle = gtk_signal_connect (GTK_OBJECT (v),
				       "changed",
				       (GtkSignalFunc) state2widget,
				       w);
  gtk_object_set_data (GTK_OBJECT (w), "sig", GUINT_TO_POINTER (changed_handle));

  gtk_signal_connect (GTK_OBJECT (w),
		      "selected_data",
		      (GtkSignalFunc) widget2state,
		      v);

  guppi_ref (v);
  gtk_signal_connect (GTK_OBJECT (w),
		      "destroy",
		      (GtkSignalFunc) data_destroy,
		      v);

  return w;
}


/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

GuppiVar *
guppi_var_new_data (GtkType type)
{
  GuppiVar *v = guppi_type_new (guppi_var_get_type ());
  InfoData *info = guppi_new0 (InfoData, 1);

  info->type = type ? type : GUPPI_TYPE_DATA;

  v->priv->object_data = info;

  v->priv->get_bool  = data_get_bool;
  v->priv->get_data  = data_get_data;
  v->priv->set_data  = data_set_data;
  v->priv->finalizer = data_finalizer;
  v->priv->widget    = data_widget;

  return v;
}



