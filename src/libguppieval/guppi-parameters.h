/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-parameters.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_PARAMETERS_H
#define _INC_GUPPI_PARAMETERS_H

#include <gnome.h>
#include  "guppi-defs.h"
#include "guppi-var.h"

BEGIN_GUPPI_DECLS;

typedef struct _GuppiParameters GuppiParameters;
typedef struct _GuppiParametersClass GuppiParametersClass;

struct _GuppiParameters {
  GtkObject parent;
  gpointer opaque_internals;
};

struct _GuppiParametersClass {
  GtkObjectClass parent_class;

  void (*changed) (GuppiParameters *, GuppiVar *);
};

typedef void (*GuppiParametersFn) (const gchar *cname, GuppiVar *v, gpointer user_data);

#define GUPPI_TYPE_PARAMETERS (guppi_parameters_get_type ())
#define GUPPI_PARAMETERS(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_PARAMETERS,GuppiParameters))
#define GUPPI_PARAMETERS0(obj) ((obj) ? (GUPPI_PARAMETERS(obj)) : NULL)
#define GUPPI_PARAMETERS_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_PARAMETERS,GuppiParametersClass))
#define GUPPI_IS_PARAMETERS(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_PARAMETERS))
#define GUPPI_IS_PARAMETERS0(obj) (((obj) == NULL) || (GUPPI_IS_PARAMETERS(obj)))
#define GUPPI_IS_PARAMETERS_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_PARAMETERS))

#define g_return_unless_is_guppi_parameters(x) (g_return_if_fail((x)&&GUPPI_IS_PARAMETERS((x))))
#define g_return_val_unless_is_guppi_parameters(x,y) (g_return_val_if_fail((x)&&GUPPI_IS_PARAMETERS((x)),(y)))

GtkType guppi_parameters_get_type (void);

GuppiParameters *guppi_parameters_new (void);

void       guppi_parameters_add         (GuppiParameters *, const gchar *canonical_name, GuppiVar *);
GuppiVar  *guppi_parameters_get         (GuppiParameters *, const gchar *canonical_name);

gint       guppi_parameters_count       (GuppiParameters *);

void       guppi_parameters_foreach_var (GuppiParameters *, GuppiParametersFn, gpointer user_data);

GtkWidget *guppi_parameters_edit_widget (GuppiParameters *);

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_PARAMETERS_H */

/* $Id$ */
