/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-fit-slr.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <guppi-memory.h>
#include <guppi-seq-scalar.h>
#include <guppi-regression-linear.h>
#include "guppi-var-data.h"
#include "guppi-fit-slr.h"

static void
slr_initialize_parameters (GuppiCurveFit *fit, GuppiParameters *param, gpointer user_data)
{
  guppi_parameters_add (param, "X-Data", guppi_var_new_data (GUPPI_TYPE_SEQ_SCALAR));
  guppi_parameters_add (param, "Y-Data", guppi_var_new_data (GUPPI_TYPE_SEQ_SCALAR));
}

static void
slr_changed_parameters (GuppiCurveFit *fit, GuppiParameters *param, gpointer user_data)
{
  GuppiRegressionLinear *slr = GUPPI_REGRESSION_LINEAR (user_data);
  GuppiVar *var;
  GuppiSeqScalar *seq;

  var = guppi_parameters_get (param, "X-Data");
  seq = guppi_var_is_valid (var) ? GUPPI_SEQ_SCALAR0 (guppi_var_get_data (var)) : NULL;
  guppi_regression2d_set_x_data (GUPPI_REGRESSION2D (slr), seq);

  var = guppi_parameters_get (param, "Y-Data");
  seq = guppi_var_is_valid (var) ? GUPPI_SEQ_SCALAR0 (guppi_var_get_data (var)) : NULL;
  guppi_regression2d_set_y_data (GUPPI_REGRESSION2D (slr), seq);
}

static void
slr_adjust_curve (GuppiRegressionLinear *slr, GuppiData *curve)
{
  double slope, intercept;

  slope = guppi_regression_linear_slope (slr);
  intercept = guppi_regression_linear_intercept (slr);

  gtk_object_set (GTK_OBJECT (curve),
		  "c0", intercept,
		  "c1", slope,
		  NULL);
}

static GuppiCurve *
slr_create_curve (GuppiCurveFit *fit, GuppiParameters *param, gpointer user_data)
{
  GuppiData *curve;
  GuppiRegressionLinear *slr = GUPPI_REGRESSION_LINEAR (user_data);

  curve = guppi_data_new ("GuppiCurvePoly");

  gtk_signal_connect (GTK_OBJECT (slr),
		      "changed",
		      GTK_SIGNAL_FUNC (slr_adjust_curve),
		      curve);

  g_message ("created %p", curve);

  return GUPPI_CURVE (curve);
}

GuppiCurveFit *
guppi_fit_slr_new (void)
{
  GuppiCurveFit *fit = guppi_curve_fit_new (_("Simple Linear Regression"));
  GuppiRegression2D *slr = guppi_regression_linear_new ();

  gtk_signal_connect (GTK_OBJECT (fit),
		      "initialize_parameters",
		      GTK_SIGNAL_FUNC (slr_initialize_parameters),
		      NULL);

  gtk_signal_connect (GTK_OBJECT (fit),
		      "changed_parameters",
		      GTK_SIGNAL_FUNC (slr_changed_parameters),
		      slr);

  gtk_signal_connect (GTK_OBJECT (fit),
		      "create_curve",
		      GTK_SIGNAL_FUNC (slr_create_curve),
		      slr);

  gtk_signal_connect_object (GTK_OBJECT (fit),
			     "destroy",
			     GTK_SIGNAL_FUNC (guppi_unref_fn),
			     GTK_OBJECT (slr));

  return fit;
}
