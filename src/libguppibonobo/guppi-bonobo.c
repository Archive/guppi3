/* $Id$ */
/*
 * guppi-bonobo.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *                    Helix Code, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>, Ji Yu <g.yu@mail.utexas.edu>,
 * Michael Meeks <michael@helixcode.com>
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <guppi-useful.h>
#include <bonobo.h>
#include <liboaf/liboaf.h>

#include "guppi-bonobo.h"
#include "guppi-embeddable.h"
#include "guppi-bonobo-print.h"

/*
 * guppi_bonobo_new :
 *
 * A simple wraper to merge the embedabble and the print interface.
 *
 * Why is this here ?  We'll move it as I get more familiar with this code.
 * Actually why is all of this here.  I'll probably move most of it into the
 * canned demo file itself.
 */
BonoboObject *
guppi_bonobo_new (void)
{
  GuppiBonoboEmbeddable *embeddable;
  BonoboPrint *print;

  embeddable = guppi_type_new (GUPPI_BONOBO_EMBEDDABLE_TYPE);

  g_return_val_if_fail (embeddable != NULL, NULL);

  embeddable = guppi_bonobo_embeddable_construct (embeddable);

  /* Register the Bonobo::Print interface */
  print = bonobo_print_new (guppi_bonobo_print, embeddable);
  if (!print) {
    bonobo_object_unref (BONOBO_OBJECT (embeddable));
    return NULL;
  }

  bonobo_object_add_interface (BONOBO_OBJECT (embeddable),
			       BONOBO_OBJECT (print));

  return BONOBO_OBJECT (embeddable);
}

/**************************************************************************/

/*
 * Management routines for the canned demo bonobo factory
 */

#if 0

static BonoboGenericFactory *canned_factory = NULL;
static gint running_objects = 0;

static void
guppi_bonobo_destroy (BonoboEmbeddable * embeddable, gpointer user_data)
{
  running_objects--;
  if (running_objects > 0)
    return;

  /* NOTE : This is only relevant for the canned demo */
  if (canned_factory != NULL) {
    bonobo_object_unref (BONOBO_OBJECT (canned_factory));
    canned_factory = NULL;
  } else
    g_warning ("More canned demos unreferenced than created?");

  guppi_exit ();
}

static BonoboObject *
guppi_bonobo_factory (BonoboGenericFactory * f, gpointer data)
{
  BonoboObject *obj = guppi_bonobo_new ();
  GuppiBonoboEmbeddable *gbe = GUPPI_BONOBO_EMBEDDABLE (obj);

  gbe->plot = guppi_bonobo_canned_demo ();

  /* Install destructor for the canned demos */
  gtk_signal_connect (GTK_OBJECT (obj), "destroy",
		      GTK_SIGNAL_FUNC (guppi_bonobo_destroy), NULL);
  running_objects++;

  return obj;
}

void
guppi_bonobo_canned_demo_init (void)
{
  canned_factory =
    bonobo_generic_factory_new
    ("OAFIID:GNOME_Guppi_Factory",
     guppi_bonobo_factory, NULL);

  if (canned_factory != NULL)
    printf ("Creating a canned_factory = %p\n", canned_factory);
  else
    g_warning ("Couldn't register guppi object canned_factory");
}
#endif

/* $Id$ */
