/* $Id$ */
/*
 * guppi-embeddable.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *                    Helix Code, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and 
 * Michael Meeks <michael@helixcode.com>.
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_BONOBO_EMBEDDABLE_H
#define _INC_GUPPI_BONOBO_EMBEDDABLE_H

#include <bonobo.h>
#include  "guppi-root-group-view.h"

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS 

typedef struct _GuppiBonoboEmbeddable GuppiBonoboEmbeddable;

#define GUPPI_BONOBO_EMBEDDABLE_TYPE        (guppi_bonobo_embeddable_get_type ())
#define GUPPI_BONOBO_EMBEDDABLE(o)          (GTK_CHECK_CAST ((o), GUPPI_BONOBO_EMBEDDABLE_TYPE, GuppiBonoboEmbeddable))
#define GUPPI_BONOBO_EMBEDDABLE_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GUPPI_BONOBO_EMBEDDABLE_TYPE, GuppiBonoboEmbeddableClass))
#define GUPPI_BONOBO_IS_EMBEDDABLE(o)       (GTK_CHECK_TYPE ((o), GUPPI_BONOBO_EMBEDDABLE_TYPE))
#define GUPPI_BONOBO_IS_EMBEDDABLE_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GUPPI_BONOBO_EMBEDDABLE_TYPE))

typedef GuppiRootGroupView *(*EmbeddableGetPlotFn) (gpointer user_data);

struct _GuppiBonoboEmbeddable {
  BonoboEmbeddable embeddable;
  EmbeddableGetPlotFn get_plot;
  gpointer user_data;
  GuppiRootGroupView *plot;
};

typedef struct {
  BonoboEmbeddableClass parent_class;
} GuppiBonoboEmbeddableClass;

GtkType                guppi_bonobo_embeddable_get_type    (void);

GuppiBonoboEmbeddable *guppi_bonobo_embeddable_construct   (GuppiBonoboEmbeddable * embeddable);
GuppiRootGroupView    *guppi_bonobo_embeddable_get_plot    (GuppiBonoboEmbeddable * embeddable);

void                   guppi_bonobo_embeddable_set_plotter (GuppiBonoboEmbeddable * embeddable,
							    EmbeddableGetPlotFn get_plot,
							    gpointer user_data);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_BONOBO_EMBEDDABLE_H */

/* $Id$ */
