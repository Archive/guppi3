#ifndef GUPPI_BONOBO_PRINT_H
#define GUPPI_BONOBO_PRINT_H

#include "guppi-embeddable.h"
#include <libgnomeprint/gnome-print.h>

#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS

void guppi_bonobo_print (GnomePrintContext * ctx,
			 double width,
			 double height,
			 const Bonobo_PrintScissor * scissor,
			 gpointer user_data);

END_GUPPI_DECLS

#endif

/* $Id$ */


