#include <config.h>
#include "guppi-bonobo-print.h"

void
guppi_bonobo_print (GnomePrintContext * ctx,
		    double width,
		    double height,
		    const Bonobo_PrintScissor * scissor, gpointer user_data)
{
  GuppiBonoboEmbeddable *embeddable = user_data;
  GuppiRootGroupView *plot;

  plot = guppi_bonobo_embeddable_get_plot (embeddable);

  guppi_element_view_print_to_bbox (GUPPI_ELEMENT_VIEW (plot), ctx,
				    0, 0, width, height);

}
