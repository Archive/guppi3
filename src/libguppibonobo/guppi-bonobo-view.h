/* $Id$ */
/*
 * guppi-bonobo-view.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *                    Helix Code, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and 
 * Michael Meeks <michael@helixcode.com>.
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_BONOBO_VIEW_H
#define _INC_GUPPI_BONOBO_VIEW_H

#include <bonobo.h>
#include  "guppi-canvas-item.h"
#include  "guppi-defs.h"

BEGIN_GUPPI_DECLS

#define GUPPI_BONOBO_VIEW_TYPE        (guppi_bonobo_view_get_type ())
#define GUPPI_BONOBO_VIEW(o)          (GTK_CHECK_CAST ((o), GUPPI_BONOBO_VIEW_TYPE, GuppiBonoboView))
#define GUPPI_BONOBO_VIEW_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GUPPI_BONOBO_VIEW_TYPE, GuppiBonoboViewClass))
#define GUPPI_BONOBO_IS_VIEW(o)       (GTK_CHECK_TYPE ((o), GUPPI_BONOBO_VIEW_TYPE))
#define GUPPI_BONOBO_IS_VIEW_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GUPPI_BONOBO_VIEW_TYPE))
typedef struct _GuppiBonoboView GuppiBonoboView;

struct _GuppiBonoboView {
  BonoboView view;
  GnomeCanvas *canvas;
  GuppiCanvasItem *item;
};

typedef struct {
  BonoboViewClass parent_class;
} GuppiBonoboViewClass;

GtkType guppi_bonobo_view_get_type (void);
BonoboView *guppi_bonobo_view_factory (BonoboEmbeddable * embeddable,
				       const Bonobo_ViewFrame view_frame,
				       void *closure);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_BONOBO_VIEW_H */

/* $Id$ */
