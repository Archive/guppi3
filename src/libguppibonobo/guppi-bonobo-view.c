/* $Id$ */
/*
 * guppi-bonobo-view.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *                    Helix Code, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and 
 * Michael Meeks <michael@helixcode.com>.
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <guppi-memory.h>
#include <guppi-root-group-item.h>
#include "guppi-embeddable.h"
#include "guppi-bonobo-view.h"

static BonoboViewClass *guppi_bonobo_view_parent_class = NULL;

BonoboView *
guppi_bonobo_view_factory (BonoboEmbeddable * embeddable,
			   const Bonobo_ViewFrame view_frame, void *closure)
{
  GuppiBonoboEmbeddable *guppi_embeddable;
  GuppiBonoboView *view;
  GuppiRootGroupView *root_view;
  GtkWidget *widget;

  g_return_val_if_fail (GUPPI_BONOBO_IS_EMBEDDABLE (embeddable), NULL);
  g_return_val_if_fail (embeddable != NULL, NULL);

  guppi_embeddable = GUPPI_BONOBO_EMBEDDABLE (embeddable);

  view = guppi_type_new (GUPPI_BONOBO_VIEW_TYPE);

  /*
   * Build a nice widget that is a view on the GuppiElementView sitting
   * inside of the BonoboEmbeddable.
   */
  gtk_widget_push_visual (gdk_rgb_get_visual ());
  gtk_widget_push_colormap (gdk_rgb_get_cmap ());
  widget = gnome_canvas_new_aa ();
  gtk_widget_pop_colormap ();
  gtk_widget_pop_visual ();

  view->canvas = GNOME_CANVAS (widget);

  root_view = guppi_bonobo_embeddable_get_plot (guppi_embeddable);

  view->item =
    guppi_element_view_make_canvas_item (GUPPI_ELEMENT_VIEW (root_view),
					 view->canvas, NULL);

  guppi_root_group_item_set_resize_semantics (GUPPI_ROOT_GROUP_ITEM (view->item),
					      ROOT_GROUP_RESIZE_FILL_SPACE);

  gtk_widget_show_all (widget);

  view =
    GUPPI_BONOBO_VIEW (bonobo_view_construct
		       (BONOBO_VIEW (view), widget));
  if (!view)
    return NULL;

  bonobo_view_set_view_frame (BONOBO_VIEW (view), view_frame);

  return BONOBO_VIEW (view);
}

static void
guppi_bonobo_view_destroy (GtkObject * object)
{
  GuppiBonoboView *view;

  g_return_if_fail (object != NULL);
  g_return_if_fail (GUPPI_BONOBO_IS_VIEW (object));

  view = GUPPI_BONOBO_VIEW (object);
  gtk_object_destroy (GTK_OBJECT (view->canvas));
  view->item = NULL;

  GTK_OBJECT_CLASS (guppi_bonobo_view_parent_class)->destroy (object);
}

static void
hello_bonobo_view_activate (BonoboControl * control, gboolean state)
{
  bonobo_view_activate_notify (BONOBO_VIEW (control), state);
}

static void
guppi_bonobo_view_class_init (GuppiBonoboViewClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  BonoboControlClass *control_class = (BonoboControlClass *) klass;

  guppi_bonobo_view_parent_class = gtk_type_class (bonobo_view_get_type ());

  control_class->activate = hello_bonobo_view_activate;

  object_class->destroy = guppi_bonobo_view_destroy;
}

static void
guppi_bonobo_view_init (GuppiBonoboView * view)
{
}

GtkType guppi_bonobo_view_get_type (void)
{
  static GtkType type = 0;

  if (!type) {
    GtkTypeInfo info = {
      "GuppiBonoboView",
      sizeof (GuppiBonoboView),
      sizeof (GuppiBonoboViewClass),
      (GtkClassInitFunc) guppi_bonobo_view_class_init,
      (GtkObjectInitFunc) guppi_bonobo_view_init,
      NULL,			/* reserved 1 */
      NULL,			/* reserved 2 */
      (GtkClassInitFunc) NULL
    };

    type = bonobo_x_type_unique (
	    bonobo_view_get_type (),
	    NULL, NULL, 0,
	    &info);
  }

  return type;
}
