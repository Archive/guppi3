/* $Id$ */

/*
 * guppi-bonobo-canned-demo.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <guppi-seq-scalar.h>
#include <guppi-seq-string.h>
#include <guppi-seq-style.h>
#include <guppi-useful.h>
#include <guppi-shared-hash.h>
#include <guppi-root-group-state.h>
#include "guppi-bonobo-canned-demo.h"

const gchar *label_data[] = {
  "Pride", "Envy", "Gluttony", "Lust", "Anger", "Covetousness", "Sloth", NULL
};

const double values[] = { 7, 5, 8, 19, 3, 9, 2 };

const gchar *colors[] = {
  "orange", "green", "salmon", "yellow", "red", "purple", "grey"
};

GuppiRootGroupView *
guppi_bonobo_canned_demo (void)
{
  GuppiElementState *root_state;
  GuppiGroupView *root_view;
  gint i;
  GuppiStyle *sty;

  /* Data Objects */
  GuppiSeqScalar *data;
  GuppiSeqString *labels;
  GuppiSeqStyle *styles;


  /* State Objects */
  GuppiElementState *text1_state;
  GuppiElementState *text2_state;
  GuppiElementState *pie_state;
  GuppiElementState *legend_state;

  /* View Objects */
  GuppiElementView *text1_view;
  GuppiElementView *text2_view;
  GuppiElementView *pie_view;
  GuppiElementView *legend_view;


  /* Assemble some data */

  data = GUPPI_SEQ_SCALAR (guppi_seq_scalar_new ());
  labels = GUPPI_SEQ_STRING (guppi_seq_string_new ());
  styles = GUPPI_SEQ_STYLE (guppi_seq_style_new ());

  for (i = 0; label_data[i] != NULL; ++i) {
    guppi_seq_scalar_append (data, values[i]);
    guppi_seq_string_append (labels, label_data[i]);

    sty = guppi_style_new ();
    guppi_style_set_color (sty, guppi_str2color_rgba (colors[i]));
    guppi_seq_style_append (styles, sty);
  }

  /* Build our state objects */

  text1_state = guppi_element_state_new ("text",
					 "text", "Time Allocation",
					 "font_size", 24.0, NULL);
  text2_state = guppi_element_state_new ("text",
					 "text",
					 "(Excludes Weekends & Holidays)",
					 NULL);

  pie_state = guppi_element_state_new ("pie",
				       "data", data,
				       "data_labels", labels,
				       "data_styles", styles,
				       "radius_maximize", TRUE,
				       "radius_lock", TRUE, NULL);

  legend_state = guppi_element_state_new ("legend", NULL);

  guppi_element_state_connect (pie_state, SHARED_LABEL_DATA,
			       legend_state, SHARED_LABEL_DATA);
  guppi_element_state_connect (pie_state, SHARED_STYLE_DATA,
			       legend_state, SHARED_STYLE_DATA);


  /* Build our views */

  text1_view = guppi_element_state_make_view (text1_state);
  text2_view = guppi_element_state_make_view (text2_state);
  pie_view = guppi_element_state_make_view (pie_state);
  legend_view = guppi_element_state_make_view (legend_state);

  /* Do our layout */

  root_state = guppi_root_group_state_new ();
  root_view = GUPPI_GROUP_VIEW (guppi_element_state_make_view (root_state));

  guppi_group_view_layout_flush_top (root_view, text1_view, 7.2);
  guppi_group_view_layout_horizontal_fill (root_view, text1_view, 7.2, 7.2);
  guppi_group_view_layout_vbox2 (root_view, text1_view, text2_view, 7.2);

  guppi_group_view_layout_natural_height (root_view, text1_view);
  guppi_group_view_layout_natural_height (root_view, text2_view);

  guppi_group_view_layout_vertically_adjacent (root_view, text2_view,
					       pie_view, 7.2);
  guppi_group_view_layout_flush_left (root_view, pie_view, 7.2);
  guppi_group_view_layout_flush_bottom (root_view, pie_view, 7.2);

  guppi_group_view_layout_horizontally_adjacent (root_view, pie_view,
						 legend_view, 7.2);

  guppi_group_view_layout_natural_width (root_view, legend_view);
  guppi_group_view_layout_natural_height (root_view, legend_view);
  guppi_group_view_layout_flush_right (root_view, legend_view, 7.2);
  guppi_group_view_layout_same_y_center (root_view, pie_view, legend_view);

  guppi_root_group_view_set_size (GUPPI_ROOT_GROUP_VIEW (root_view),
				  6 * 72, 4.5 * 72);

  return GUPPI_ROOT_GROUP_VIEW (root_view);
}

/* $Id$ */
