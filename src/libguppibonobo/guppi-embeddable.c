/* $Id$ */
/*
 * guppi-embeddable.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *                    Helix Code, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and 
 * Michael Meeks <michael@helixcode.com>.
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <guppi-convenient.h>
#include "guppi-embeddable.h"
#include "guppi-bonobo-view.h"

GuppiBonoboEmbeddable *
guppi_bonobo_embeddable_construct (GuppiBonoboEmbeddable * embeddable)
{
  g_return_val_if_fail (GUPPI_BONOBO_IS_EMBEDDABLE (embeddable), NULL);

  bonobo_embeddable_construct (BONOBO_EMBEDDABLE (embeddable),
			       guppi_bonobo_view_factory, NULL);

  embeddable->get_plot = NULL;
  embeddable->user_data = NULL;
  embeddable->plot = NULL;

  return embeddable;
}

GuppiRootGroupView *
guppi_bonobo_embeddable_get_plot (GuppiBonoboEmbeddable * embeddable)
{
  if (embeddable->plot == NULL) {
    g_assert (embeddable->get_plot != NULL);
    embeddable->plot = embeddable->get_plot (embeddable->user_data);
  }

  return embeddable->plot;
}

void
guppi_bonobo_embeddable_set_plotter (GuppiBonoboEmbeddable * embeddable,
				     EmbeddableGetPlotFn get_plot,
				     gpointer user_data)
{
  embeddable->get_plot = get_plot;
  embeddable->user_data = user_data;
}

static BonoboEmbeddableClass *guppi_bonobo_embeddable_parent_class = NULL;

static void
guppi_bonobo_embeddable_destroy (GtkObject * object)
{
  GuppiBonoboEmbeddable *embeddable = GUPPI_BONOBO_EMBEDDABLE (object);

  puts ("GUPPI : embeddable destroyed");

  guppi_unref (embeddable->plot);

  GTK_OBJECT_CLASS (guppi_bonobo_embeddable_parent_class)->destroy (object);
}

static void
guppi_bonobo_embeddable_class_init (BonoboEmbeddableClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;

  guppi_bonobo_embeddable_parent_class =
    gtk_type_class (bonobo_embeddable_get_type ());

  object_class->destroy = guppi_bonobo_embeddable_destroy;
}

static void
guppi_bonobo_embeddable_init (BonoboObject * object)
{
  /*
   * GuppiBonoboEmbeddable *embeddable = GUPPI_BONOBO_EMBEDDABLE (object);
   */
}

GtkType guppi_bonobo_embeddable_get_type (void)
{
  static GtkType type = 0;

  if (!type) {
    GtkTypeInfo info = {
      "GuppiBonoboEmbeddable",
      sizeof (GuppiBonoboEmbeddable),
      sizeof (GuppiBonoboEmbeddableClass),
      (GtkClassInitFunc) guppi_bonobo_embeddable_class_init,
      (GtkObjectInitFunc) guppi_bonobo_embeddable_init,
      NULL,			/* reserved 1 */
      NULL,			/* reserved 2 */
      (GtkClassInitFunc) NULL
    };

    type = bonobo_x_type_unique (
	    bonobo_embeddable_get_type (),
	    NULL, NULL, 0,
	    &info);
  }

  return type;
}
