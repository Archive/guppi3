/* 
 * test-server.c
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/* #include <gnome.h> */
#include <libgnorba/gnorba.h>
#include "Guile.h"

#include <stdlib.h>
#include <libguile.h>
#include <guile/gh.h>

#include "config.h"

static CORBA_boolean guile_eval (PortableServer_Servant servant,
				 const CORBA_char * expr,
				 CORBA_char ** result,
				 CORBA_char ** output,
				 CORBA_char ** error, CORBA_Environment * ev);

static CORBA_boolean guile_get_pending (PortableServer_Servant servant,
					CORBA_char ** result,
					CORBA_char ** output,
					CORBA_char ** error,
					CORBA_Environment * ev);

static void guile_clear_pending (PortableServer_Servant servant,
				 CORBA_Environment * ev);

static void guile_prompt (PortableServer_Servant servant,
			  CORBA_char ** prompt, CORBA_Environment * ev);


/* Variables */
CORBA_ORB orb = CORBA_OBJECT_NIL;
CORBA_Environment ev;
Gnome_Guile gg = CORBA_OBJECT_NIL;


static void
die (const char *err)
{
  fprintf (stderr, "%s\n", err);
  if (gg)
    CORBA_Object_release (gg, &ev);
  if (orb)
    CORBA_Object_release ((CORBA_Object) orb, &ev);

  exit (1);
}

static void
check_errors (CORBA_Environment * ev)
{
  switch (ev->_major) {
  case CORBA_NO_EXCEPTION:
    return;
    break;
  case CORBA_USER_EXCEPTION:
    die ("CORBA user exception, shouldn't happen, yikes!");
    break;
  default:
    fprintf (stderr, "Exception: %s\n", CORBA_exception_id (ev));
    die ("CORBA system exception, aborting.");
    break;
  }

  CORBA_exception_free (ev);
}


gchar *ior;

PortableServer_ServantBase__epv base_epv = {
  NULL,
  NULL,
  NULL
};
POA_Gnome_Guile__epv Guile_epv = {
  NULL,
  &guile_eval,
  &guile_get_pending,
  &guile_clear_pending,
  &guile_prompt
};

POA_Gnome_Guile__vepv poa_Guile_vepv = { &base_epv, &Guile_epv };
POA_Gnome_Guile poa_Guile_servant = { NULL, &poa_Guile_vepv };


static void
real_main (void *closure, int argc, char **argv)
{
  PortableServer_ObjectId objid =
    { 0, sizeof ("gnome_guile_interface"), "gnome_guile_interface" };
  PortableServer_POA poa;

  CORBA_exception_init (&ev);
  orb = gnome_CORBA_init (PACKAGE, VERSION, &argc, argv,
			  GNORBA_INIT_SERVER_FUNC, &ev);

  check_errors (&ev);

  poa =
    (PortableServer_POA) CORBA_ORB_resolve_initial_references (orb, "RootPOA",
							       &ev);

  check_errors (&ev);

  POA_Gnome_Guile__init (&poa_Guile_servant, &ev);

  check_errors (&ev);

  PortableServer_POAManager_activate (PortableServer_POA__get_the_POAManager
				      (poa, &ev), &ev);

  check_errors (&ev);

  PortableServer_POA_activate_object_with_id (poa,
					      &objid, &poa_Guile_servant,
					      &ev);

  check_errors (&ev);

  gg =
    PortableServer_POA_servant_to_reference ((PortableServer_POA) orb->
					     root_poa, &poa_Guile_servant,
					     &ev);

  check_errors (&ev);

  if (!gg) {
    g_error ("Unable to initialize CORBA.\naborting...\n");
    exit (1);
  }
  ior = CORBA_ORB_object_to_string (orb, gg, &ev);

  check_errors (&ev);

  g_print ("%s\n", ior);

  /* totally pointless since we have no window! */
  gtk_main ();

  exit (0);
}

int
main (int argc, char *argv[])
{
  scm_boot_guile (argc, argv, real_main, 0);
  return 0;			/* never reached */
}

/* This stuff is taken from SCWM */

static SCM
make_output_strport (char *fname)
{
  return scm_mkstrport (SCM_INUM0, scm_make_string (SCM_INUM0,
						    SCM_UNDEFINED),
			SCM_OPN | SCM_WRTNG, fname);
}

/* Guile 1.3 is missing this function. */
#ifdef USING_GUILE_1_3_0
static SCM
scm_strport_to_string (SCM port)
{
  SCM answer;
  {				/* scope */
    gh_defer_ints ();
    answer = scm_makfromstr (SCM_CHARS (gh_cdr (SCM_STREAM (port))),
			     SCM_INUM (gh_car (SCM_STREAM (port))), 0);
    gh_allow_ints ();
  }
  return answer;
}
#endif

/* Well it isn't safe, but if this was a real server it would be made
   so. */
static SCM
safe_eval_str (char *str)
{

  return gh_eval_str (str);
}

/* Not exact but close enough */
#define STUPIDASS_ORBIT_LIMIT 8000

typedef struct _Pending Pending;

struct _Pending {
  unsigned char *result;
  int rlen;
  int rpos;

  unsigned char *output;
  int olen;
  int opos;

  unsigned char *error;

  int elen;
  int epos;

};

Pending pending = { NULL, 0, 0, NULL, 0, 0, NULL, 0, 0 };

static CORBA_boolean
guile_eval (PortableServer_Servant servant,
	    const CORBA_char * expr,
	    CORBA_char ** result,
	    CORBA_char ** output, CORBA_char ** error, CORBA_Environment * ev)
{
  SCM val, str_val;
  unsigned char *ret, *out, *err;
  SCM o_port, e_port;
  SCM saved_def_e_port;

  g_print ("Server: Received expression: %s\n", expr);

  guile_clear_pending (servant, ev);

  /* Temporarily redirect output and error to string ports. 
     Note that the port setting functions return the current previous
     port. */
  o_port = scm_set_current_output_port (make_output_strport ("guile_eval"));
  e_port = scm_set_current_error_port (make_output_strport ("guile_eval"));

  /* Workaround for a problem with older Guiles */
  saved_def_e_port = scm_def_errp;
  scm_def_errp = scm_current_error_port ();

  /* Evaluate the request expression and free it. */
  val = safe_eval_str ((gchar *) expr);

  str_val = scm_strprint_obj (val);
  ret = (unsigned char *) gh_scm2newstr (str_val, &pending.rlen);

  /* restore output and error ports. */
  o_port = scm_set_current_output_port (o_port);
  e_port = scm_set_current_error_port (e_port);
  scm_def_errp = saved_def_e_port;

  /* Retrieve output and errors */
  out = (unsigned char *) gh_scm2newstr (scm_strport_to_string (o_port),
					 &pending.olen);
  err = (unsigned char *) gh_scm2newstr (scm_strport_to_string (e_port),
					 &pending.elen);

  pending.result = ret;
  pending.output = out;
  pending.error = err;

  pending.rpos = 0;
  pending.opos = 0;
  pending.epos = 0;

  return guile_get_pending (servant, result, output, error, ev);
}


static CORBA_boolean
guile_get_pending (PortableServer_Servant servant,
		   CORBA_char ** result,
		   CORBA_char ** output,
		   CORBA_char ** error, CORBA_Environment * ev)
{
  int rlen = pending.rlen - pending.rpos;
  int olen = pending.olen - pending.opos;
  int elen = pending.elen - pending.epos;

  if (rlen > STUPIDASS_ORBIT_LIMIT)
    rlen = STUPIDASS_ORBIT_LIMIT;
  if (olen > STUPIDASS_ORBIT_LIMIT)
    olen = STUPIDASS_ORBIT_LIMIT;
  if (elen > STUPIDASS_ORBIT_LIMIT)
    elen = STUPIDASS_ORBIT_LIMIT;

  if (rlen > 0) {
    *result = CORBA_string_alloc (rlen + 2);
    g_snprintf (*result, rlen + 1, "%s", &pending.result[pending.rpos]);
  } else {
    *result = CORBA_string_dup ("");
  }

  if (olen > 0) {
    *output = CORBA_string_alloc (olen + 2);
    g_snprintf (*output, olen + 1, "%s", &pending.output[pending.opos]);
  } else {
    *output = CORBA_string_dup ("");
  }

  if (elen > 0) {
    *error = CORBA_string_alloc (elen + 2);
    g_snprintf (*error, elen + 1, "%s", &pending.error[pending.epos]);
  } else {
    *error = CORBA_string_dup ("");
  }

  g_print ("Server: Result: %s\n", *result);
  g_print ("Server: Output: %s\n", *output);
  g_print ("Server: Error: %s\n", *error);

  pending.rpos += rlen;
  pending.opos += olen;
  pending.epos += elen;

  if (pending.rpos >= pending.rlen &&
      pending.opos >= pending.olen && pending.epos >= pending.elen)
    return FALSE;
  else
    return TRUE;
}

static void
guile_clear_pending (PortableServer_Servant servant, CORBA_Environment * ev)
{
  if (pending.result)
    free (pending.result);
  if (pending.output)
    free (pending.output);
  if (pending.error)
    free (pending.error);

  memset (&pending, '\0', sizeof (Pending));
}

static void
guile_prompt (PortableServer_Servant servant,
	      CORBA_char ** prompt, CORBA_Environment * ev)
{
  *prompt = CORBA_string_dup ("testing> ");
}
