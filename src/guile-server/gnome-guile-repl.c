/* 
 * gnome-guile-repl.c 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc. and Maciej Stachowiak
 *
 * Developed by Havoc Pennington <hp@pobox.com>, but most of 
 * the code is from scwmrepl.c in the SCWM distribution. I simply
 * ported from scwmexec to CORBA.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <libgnome/libgnome.h>
#include <libgnorba/gnorba.h>

#include "Guile.h"

#include "config.h"

#ifdef HAVE_READLINE
#include <readline/readline.h>
#ifdef HAVE_HISTORY
#include <readline/history.h>
#endif /* HAVE_HISTORY */
#endif /* HAVE_READLINE */



/* Variables */
CORBA_ORB orb = NULL;
CORBA_Environment ev;
Gnome_Guile gg = NULL;
gchar *ior = NULL;
CORBA_char *prompt = NULL;


static void
die (const char *err)
{
  fprintf (stderr, "%s\n", err);
  if (gg)
    CORBA_Object_release (gg, &ev);
  if (orb)
    CORBA_Object_release ((CORBA_Object) orb, &ev);

  exit (1);
}

static void
check_errors (CORBA_Environment * ev)
{
  switch (ev->_major) {
  case CORBA_NO_EXCEPTION:
    return;
    break;
  case CORBA_USER_EXCEPTION:
    die ("CORBA user exception, shouldn't happen, yikes!");
    break;
  default:
    fprintf (stderr, "Exception: %s\n", CORBA_exception_id (ev));
    die ("CORBA system exception, aborting.");
    break;
  }

  CORBA_exception_free (ev);
}

static void
guile_eval_expr (const char *expr,
		 char **resultp, char **outputp, char **errorp)
{
  CORBA_char *result = NULL;
  CORBA_char *error = NULL;
  CORBA_char *output = NULL;

  *resultp = NULL;
  *outputp = NULL;
  *errorp = NULL;

  if (Gnome_Guile_eval (gg, expr, &result, &output, &error, &ev)) {
    check_errors (&ev);

    *resultp = result;
    *outputp = output;
    *errorp = error;


    {				/* Random brace so I can declare variables when I want */
      /* well this kind of bites and maybe the Guile IDL 
         should return the string length. */
      int rlen = strlen (*resultp);
      int olen = strlen (*outputp);
      int elen = strlen (*errorp);

      int notdone = 1;
      do {
	int rlen2, olen2, elen2;

	notdone = Gnome_Guile_get_pending (gg, &result, &output, &error, &ev);
	check_errors (&ev);

	rlen2 = strlen (result);
	olen2 = strlen (output);
	elen2 = strlen (error);

	if (rlen2 > 0) {
	  CORBA_char *newr = CORBA_string_alloc (rlen + rlen2);

	  strncpy (newr, *resultp, rlen);
	  strncpy (&newr[rlen], result, rlen2 + 1);

	  CORBA_free (*resultp);
	  *resultp = newr;

	  rlen += rlen2;
	}

	if (olen2 > 0) {
	  CORBA_char *newo = CORBA_string_alloc (olen + olen2);

	  strncpy (newo, *outputp, olen);
	  strncpy (&newo[olen], output, olen2 + 1);

	  CORBA_free (*outputp);
	  *outputp = newo;

	  olen += olen2;
	}

	if (elen2 > 0) {
	  CORBA_char *newe = CORBA_string_alloc (elen + elen2);

	  strncpy (newe, *errorp, elen);
	  strncpy (&newe[elen], error, elen2 + 1);

	  CORBA_free (*errorp);
	  *errorp = newe;

	  elen += elen2;
	}

	CORBA_free (result);
	CORBA_free (error);
	CORBA_free (output);

      } while (notdone);
    }
  } else {
    *resultp = result;
    *outputp = output;
    *errorp = error;
    return;
  }
}


#ifdef HAVE_READLINE
static char *
gnomeguile_complete (char *text, int state)
{
  static CORBA_char *output = NULL;
  static char *last = NULL;
  static char *completions[1024];

  if (!state) {
    CORBA_char *result = NULL;
    CORBA_char *error = NULL;

    unsigned char *c, *e, *query;
    unsigned char hold;
    unsigned n;

    if (!last || strcmp (last, text) != 0) {
      if (last) {
	free (last);
      }
      last = strdup (text);
      if (output) {
	CORBA_free (output);
	output = NULL;
      }
      query = (unsigned char *) malloc (strlen (text) + 14);
      strcat (strcat (strcpy (query, "(apropos \"^"), text), "\")");

      guile_eval_expr (query, &result, &output, &error);

      CORBA_free (result);
      CORBA_free (error);

      free (query);
    }

    c = output;
    n = 0;
    while (n < 1023) {
      c = strstr (c, ": ");
      if (!c)
	break;
      c += 2;
      e = strpbrk (c, "\t\n");
      if (!e) {
	completions[n++] = strdup (c);
	break;
      }
      hold = *e;
      *e = 0;
      completions[n++] = strdup (c);
      *e = hold;
      c = strchr (e, '\n');
      if (!c)
	break;
      c++;
    }
    completions[n] = NULL;
  }
  return completions[state];
}

static void
init_readline (void)
{
  rl_completion_entry_function = (Function *) gnomeguile_complete;
}
#endif

static int
appending_fgets (char **sofar)
{
#ifdef HAVE_READLINE
  char *buffer;
  unsigned pos, len;

  buffer = readline (prompt);
  if (buffer == NULL) {
    return 0;
  }
  len = strlen (buffer);
#if HAVE_HISTORY
  if (len > 0) {
    add_history (buffer);
  }
#endif
  pos = strlen (*sofar);
  *sofar = realloc (*sofar, pos + len + 2);
  strncpy (*sofar + pos, buffer, len);
  (*sofar)[pos + len] = '\n';
  (*sofar)[pos + len + 1] = 0;
#else
  char buffer[512];

  fputs (prompt, stdout);
  do {
    fgets (buffer, 512, stdin);
    if (strlen (buffer) == 0) {
      return 0;
    }
    *sofar = realloc (*sofar, strlen (*sofar) + strlen (buffer) + 1);
    strcat (*sofar, buffer);
  } while (buffer[strlen (buffer) - 1] != '\n');
#endif

  return 1;
}

static int
check_balance (char *expr)
{
  /* If you think _this_ is hairy, try doing it for C statements. */
  int i;
  int end;
  int non_whitespace_p = 0;
  int paren_count = 0;
  int prev_separator = 1;
  int quote_wait = 0;

  end = strlen (expr);
  i = 0;
  while (i < end) {
    switch (expr[i]) {
    case ';':
      /* skip till newline. */
      do {
	i++;
      } while (expr[i] != '\n' && i < end);
      break;
    case ' ':
    case '\n':
    case '\t':
    case '\r':
      if (non_whitespace_p && paren_count == 0 && !quote_wait) {
	return i;
      } else {
	prev_separator = 1;
	i++;
      }
      break;
    case '\"':
      if (non_whitespace_p && paren_count == 0 && !quote_wait) {
	return i;
      } else {
	/* skip past ", ignoring \" */
	do {
	  i++;
	  if (i < end && expr[i] == '\\') {
	    i++;
	  }
	} while (i < end && expr[i] != '\"');
	i++;
	if (paren_count == 0) {
	  if (i < end) {
	    return i;
	  } else {
	    return 0;
	  }
	} else {
	  prev_separator = 1;
	  non_whitespace_p = 1;
	  quote_wait = 0;
	}
      }
      break;
    case '#':
      if (non_whitespace_p && paren_count == 0 && !quote_wait) {
	return i;
      } else {
	if (prev_separator && i + 1 < end && expr[i + 1] == '{') {
	  /* skip past }#, ignoring \} */
	  do {
	    i++;
	    if (i < end && expr[i] == '\\') {
	      i++;
	    }
	  } while (i < end && !(expr[i] == '}' && i + 1 < end
				&& expr[i + 1] == '#'));
	  i += 2;
	  if (paren_count == 0) {
	    if (i < end) {
	      return i;
	    } else {
	      return 0;
	    }
	  } else {
	    prev_separator = 1;
	    non_whitespace_p = 1;
	    quote_wait = 0;
	  }
	  /* MS:FIXME:: Handle #\) properly! */
	} else {
	  prev_separator = 0;
	  quote_wait = 0;
	  non_whitespace_p = 1;
	  i++;
	}
      }
      break;
    case '(':
      if (non_whitespace_p && paren_count == 0 && !quote_wait) {
	return i;
      } else {
	i++;
	paren_count++;
	non_whitespace_p = 1;
	prev_separator = 1;
	quote_wait = 0;
      }
      break;
    case ')':
      paren_count--;
      if (non_whitespace_p && paren_count == 0) {
	return i + 1;
      } else {
	i++;
	non_whitespace_p = 1;
	prev_separator = 1;
	quote_wait = 0;
      }
      break;
    case '\'':
      if (prev_separator) {
	non_whitespace_p = 1;
	quote_wait = 1;
	prev_separator = 1;
	i++;
      } else {
	non_whitespace_p = 1;
	prev_separator = 0;
	i++;
      }
      break;
    default:
      prev_separator = 0;
      quote_wait = 0;
      non_whitespace_p = 1;
      i++;
      break;
    }
  }
  return 0;
}

static char *
split_at (char **to_split, int i)
{
  char *out;
  char *ret;

  out = strdup ((*to_split) + i);

  (*to_split)[i] = 0;
  ret = strdup (*to_split);
  free (*to_split);
  *to_split = out;
  return ret;
}

int
main (int argc, char *argv[])
{
  int splitpoint;
  char *expr;
  int done = 0;
  char *gather = calloc (1, 1);

  if (argc != 2)
    die ("Usage: gnome-guile-repl IOR\n");

  CORBA_exception_init (&ev);

  gnomelib_init (PACKAGE, VERSION);
  orb = gnorba_CORBA_init (&argc, argv, 0, &ev);
  check_errors (&ev);

  if (orb == NULL)
    die ("Couldn't get ORB");

  ior = argv[1];

  gg = CORBA_ORB_string_to_object (orb, ior, &ev);
  check_errors (&ev);

  if (gg == NULL) {
    die ("Couldn't connect - bad IOR?");
  }

  Gnome_Guile_prompt (gg, &prompt, &ev);
  check_errors (&ev);
  if (prompt == NULL)
    die ("Couldn't get a prompt from the Guile server.");

#ifdef HAVE_READLINE
  init_readline ();
#endif

  while (!done) {
    if ((splitpoint = check_balance (gather))) {
      CORBA_char *result;
      CORBA_char *output;
      CORBA_char *error;

      expr = split_at (&gather, splitpoint);

      guile_eval_expr (expr, &result, &output, &error);


      fputs (output, stdout);

      if (error && strlen (error) != 0) {
	CORBA_char *tmp_err = error;

	/* Remove excess leading newlines from error messages, since
	   they can be quite annoying when working in a guile-terminal
	   in guppi. */
	while (*tmp_err == '\n')
	  ++tmp_err;
	if (*tmp_err == '\0')
	  fputs ("<empty error?>\n", stderr);
	else
	  fputs (tmp_err, stderr);

      } else if (strcmp (result, "#<unspecified>") != 0) {
	fputs (result, stdout);
	putchar ('\n');
      }

      CORBA_free (result);
      CORBA_free (output);
      CORBA_free (error);
      result = NULL;
      output = NULL;
      error = NULL;

      free (expr);
      expr = NULL;
    } else {
      done = !appending_fgets (&gather);
    }
  }

  CORBA_free (prompt);
  CORBA_Object_release (gg, &ev);
  CORBA_Object_release ((CORBA_Object) orb, &ev);

  printf ("\nExiting Guile interaction...");

  return 0;
}
