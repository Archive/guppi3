/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-object-barchart.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <guppi-useful.h>
#include <guppi-data-table-core.h>
#include <guppi-group-state.h>
#include <guppi-group-view.h>
#include <guppi-group-view-layout.h>
#include <guppi-canvas-group.h>
#include <guppi-color-palette.h>
#include "guppi-object-barchart.h"

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0,
  ARG_DATA_ROWS,
  ARG_DATA_COLUMNS,
  ARG_DATA,
  ARG_ROW_LABELS,
  ARG_COLUMN_LABELS,
  ARG_COLUMN_COLORS,
  ARG_COLUMN_COLORS_RGB,
  ARG_LEGEND_REVERSED,
  ARG_LEGEND_FONT,
  ARG_AXIS_FONT,
  ARG_ROTATE_X_AXIS_LABELS,
  ARG_ROTATE_Y_AXIS_LABELS,
  ARG_STACKED,
  ARG_NORMALIZE_STACKS,

  ARG_BAR_CALLBACK1,
  ARG_BAR_CALLBACK1_DATA,
  ARG_BAR_CALLBACK1_NAME,
  ARG_BAR_CALLBACK2,
  ARG_BAR_CALLBACK2_DATA,
  ARG_BAR_CALLBACK2_NAME,
  ARG_BAR_CALLBACK3,
  ARG_BAR_CALLBACK3_DATA,
  ARG_BAR_CALLBACK3_NAME,

  ARG_LEGEND_CALLBACK1,
  ARG_LEGEND_CALLBACK1_DATA,
  ARG_LEGEND_CALLBACK1_NAME,
  ARG_LEGEND_CALLBACK2,
  ARG_LEGEND_CALLBACK2_DATA,
  ARG_LEGEND_CALLBACK2_NAME,
  ARG_LEGEND_CALLBACK3,
  ARG_LEGEND_CALLBACK3_DATA,
  ARG_LEGEND_CALLBACK3_NAME,

  ARG_X_AXIS_LABEL,
  ARG_Y_AXIS_LABEL
};

static void
freev (gchar ** ptr, gint N)
{
  gint i;

  g_assert (N > 0);

  if (ptr == NULL)
    return;

  for (i = 0; i < N; ++i)
    guppi_free (ptr[i]);
  guppi_free (ptr);
}

static gchar **
copyv (const gchar ** ptr, gint N)
{
  gint i;
  gchar **cpy;

  g_assert (N > 0);

  cpy = guppi_new (gchar *, N);
  for (i = 0; i < N; ++i)
    cpy[i] = guppi_strdup (ptr[i]);

  return cpy;
}


static void
guppi_object_barchart_set_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  GuppiObjectBarchart *obar = GUPPI_OBJECT_BARCHART (obj);
  const gchar **ptr;
  gint i;
  guint32 c;

  switch (arg_id) {

  case ARG_X_AXIS_LABEL:
    obar->x_axis_label = guppi_strdup (GTK_VALUE_POINTER (*arg));
    break; 

  case ARG_Y_AXIS_LABEL:
    obar->y_axis_label = guppi_strdup (GTK_VALUE_POINTER (*arg));
    break; 

  case ARG_DATA_ROWS:
    obar->data_rows = GTK_VALUE_INT (*arg);
    break;

  case ARG_DATA_COLUMNS:
    obar->data_cols = GTK_VALUE_INT (*arg);
    break;

  case ARG_DATA:
    guppi_free (obar->data);
    g_assert (obar->data_rows > 0 && obar->data_cols > 0);
    obar->data = guppi_new (double, obar->data_rows * obar->data_cols);
    memcpy (obar->data, GTK_VALUE_POINTER (*arg),
	    obar->data_rows * obar->data_cols * sizeof (double));
    break;

  case ARG_ROW_LABELS:
    freev (obar->row_labels, obar->data_rows);
    obar->row_labels = copyv ((const gchar **) GTK_VALUE_POINTER (*arg),
			      obar->data_rows);
    break;

  case ARG_COLUMN_LABELS:
    freev (obar->col_labels, obar->data_cols);
    obar->col_labels = copyv ((const gchar **) GTK_VALUE_POINTER (*arg),
			      obar->data_cols);
    break;

  case ARG_COLUMN_COLORS:
    g_assert (obar->data_cols > 0);

    guppi_free (obar->col_colors);
    obar->col_colors = guppi_new0 (guint32, obar->data_cols);

    ptr = (const gchar **) GTK_VALUE_POINTER (*arg);
    for (i = 0; i < obar->data_cols; ++i) {
      c = guppi_str2color_rgba (ptr[i]);
      if (c == 0)
	g_message ("Unknown color: \"%s\"", ptr[i]);
      obar->col_colors[i] = c;
    }
    break;

  case ARG_COLUMN_COLORS_RGB:
    g_assert (obar->data_cols > 0);

    guppi_free (obar->col_colors);
    obar->col_colors = guppi_new0 (guint32, obar->data_cols);

    for (i = 0; i < obar->data_cols; ++i)
      obar->col_colors[i] =
	RGB_TO_RGBA (((guint32 *) GTK_VALUE_POINTER (*arg))[i], 0xff);
    break;

  case ARG_LEGEND_REVERSED:
    obar->legend_reversed = GTK_VALUE_BOOL (*arg);
    break;

  case ARG_LEGEND_FONT:
    guppi_refcounting_assign (obar->legend_font,
			      GNOME_FONT (GTK_VALUE_POINTER (*arg)));
    break;

  case ARG_AXIS_FONT:
    guppi_refcounting_assign (obar->axis_font,
			      GNOME_FONT (GTK_VALUE_POINTER (*arg)));
    break;

  case ARG_ROTATE_X_AXIS_LABELS:
    obar->rot_x_axis_labels = GTK_VALUE_BOOL (*arg);
    break;

  case ARG_ROTATE_Y_AXIS_LABELS:
    obar->rot_y_axis_labels = GTK_VALUE_BOOL (*arg);
    break;


  case ARG_STACKED:
    obar->stacked = GTK_VALUE_BOOL (*arg);
    break;
  case ARG_NORMALIZE_STACKS:
    obar->normalize_stacks = GTK_VALUE_BOOL (*arg);
    break;


  case ARG_BAR_CALLBACK1:
    obar->bar_callback1 = GTK_VALUE_POINTER (*arg);
    break;

  case ARG_BAR_CALLBACK1_DATA:
    obar->bar_callback1_data = GTK_VALUE_POINTER (*arg);
    break;

  case ARG_BAR_CALLBACK1_NAME:
    obar->bar_callback1_name = guppi_strdup (GTK_VALUE_POINTER (*arg));
    break;


  case ARG_BAR_CALLBACK2:
    obar->bar_callback2 = GTK_VALUE_POINTER (*arg);
    break;

  case ARG_BAR_CALLBACK2_DATA:
    obar->bar_callback2_data = GTK_VALUE_POINTER (*arg);
    break;

  case ARG_BAR_CALLBACK2_NAME:
    obar->bar_callback2_name = guppi_strdup (GTK_VALUE_POINTER (*arg));
    break;



  case ARG_BAR_CALLBACK3:
    obar->bar_callback3 = GTK_VALUE_POINTER (*arg);
    break;

  case ARG_BAR_CALLBACK3_DATA:
    obar->bar_callback3_data = GTK_VALUE_POINTER (*arg);
    break;

  case ARG_BAR_CALLBACK3_NAME:
    obar->bar_callback3_name = guppi_strdup (GTK_VALUE_POINTER (*arg));
    break;



  case ARG_LEGEND_CALLBACK1:
    obar->legend_callback1 = GTK_VALUE_POINTER (*arg);
    break;

  case ARG_LEGEND_CALLBACK1_DATA:
    obar->legend_callback1_data = GTK_VALUE_POINTER (*arg);
    break;

  case ARG_LEGEND_CALLBACK1_NAME:
    obar->legend_callback1_name = guppi_strdup (GTK_VALUE_POINTER (*arg));
    break;


  case ARG_LEGEND_CALLBACK2:
    obar->legend_callback2 = GTK_VALUE_POINTER (*arg);
    break;

  case ARG_LEGEND_CALLBACK2_DATA:
    obar->legend_callback2_data = GTK_VALUE_POINTER (*arg);
    break;

  case ARG_LEGEND_CALLBACK2_NAME:
    obar->legend_callback2_name = guppi_strdup (GTK_VALUE_POINTER (*arg));
    break;


  case ARG_LEGEND_CALLBACK3:
    obar->legend_callback3 = GTK_VALUE_POINTER (*arg);
    break;

  case ARG_LEGEND_CALLBACK3_DATA:
    obar->legend_callback3_data = GTK_VALUE_POINTER (*arg);
    break;

  case ARG_LEGEND_CALLBACK3_NAME:
    obar->legend_callback3_name = guppi_strdup (GTK_VALUE_POINTER (*arg));
    break;


  default:
    break;
  };
}

static void
guppi_object_barchart_destroy (GtkObject *obj)
{
  if (parent_class->destroy)
    parent_class->destroy (obj);
}

static void
guppi_object_barchart_finalize (GtkObject *obj)
{
  GuppiObjectBarchart *obar = GUPPI_OBJECT_BARCHART (obj);

  guppi_free (obar->x_axis_label);
  guppi_free (obar->y_axis_label);
  obar->x_axis_label = NULL;
  obar->y_axis_label = NULL;

  guppi_free (obar->data);
  freev (obar->row_labels, obar->data_rows);
  freev (obar->col_labels, obar->data_cols);
  guppi_free (obar->col_colors);

  guppi_unref (obar->legend_font);
  guppi_unref (obar->axis_font);

  guppi_free (obar->bar_callback1_name);
  guppi_free (obar->bar_callback2_name);
  guppi_free (obar->bar_callback3_name);

  guppi_free (obar->legend_callback1_name);
  guppi_free (obar->legend_callback2_name);
  guppi_free (obar->legend_callback3_name);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/***************************************************************************/

static GuppiDataTable *
build_bar_data(GuppiObjectBarchart *obar) 
{
  GuppiDataTable *table;
  gint i, j, k = 0;
  /* Convert our data into the appropriate GuppiSeq-derived objects. */

  table = (GuppiDataTable *) guppi_data_table_core_new ();
  guppi_data_table_set_dimensions (table, obar->data_rows, obar->data_cols);
  
  for (j = 0; j < obar->data_cols; ++j) {
    for (i = 0; i < obar->data_rows; ++i) {
      guppi_data_table_set_entry (table, i, j, obar->data[k]);
      ++k;
    }
  }

  return table;
}

static void
add_row_labels(GuppiObjectBarchart *obar, 
	       GuppiDataTable *table,
	       int count, gchar **source_labels, gboolean reversed) 
{
  gint i;

  for (i = 0; i < count; ++i) {
    gchar *label =  source_labels[ reversed ? (count-1-i) : i];
    guppi_data_table_set_row_label (table, i, label);
  }
}

static void
add_col_labels (GuppiObjectBarchart *obar, 
		GuppiDataTable *table,
		int count, gchar **source_labels, gboolean reversed) 
{
  gint i;

  for (i = 0; i < count; ++i) {
    gchar *label =  source_labels[ reversed ? (count-1-i) : i];
    guppi_data_table_set_col_label (table, i, label);
  }
}

static GuppiColorPalette *
build_bar_colors(GuppiObjectBarchart *obar, gboolean reversed) 
{
  GuppiColorPalette *palette;
  gint i;

  palette = guppi_color_palette_new ();
  guppi_color_palette_set_custom (palette, obar->data_cols, NULL);

  for (i = 0; i < obar->data_cols; ++i) {
    guint32 c = obar->col_colors[ (reversed) ? (obar->data_cols-1-i) : i];
    guppi_color_palette_set (palette, i, c);
  }

  return palette;
}

static void
update (GuppiObject *obj) 
{
  GuppiObjectBarchart *obar;
  GuppiElementState *barchart_state = NULL;
  GuppiElementState *legend_state = NULL;
  GuppiElementState *bottom_axis_state = NULL;
  GuppiElementState *left_axis_state = NULL;
  GuppiElementState *frame_state = NULL;
  GuppiColorPalette *color_palette = NULL;
  GuppiColorPalette *color_palette_legend = NULL;
  GuppiDataTable *data_table;
  GnomeFont *font;
  

  obar = GUPPI_OBJECT_BARCHART (obj);
    	
  if (obar->data == NULL) {
    g_warning("No barchart data specified.\n");
    return;
  }
  
  
  barchart_state = guppi_element_view_state (obar->barchart_view);
  guppi_ref (barchart_state);
  
  /* FIXME: if there wasn't a legend in the construction of the
     object, it won't be created upon an update.  In the future we may
     want to support construction of the legend upon update, but for
     now..
  */


  if (obar->legend_view) {
    legend_state = guppi_element_view_state (obar->legend_view);
    guppi_ref (legend_state);
  }
  
  data_table = build_bar_data (obar);

  if (obar->row_labels) {
    add_row_labels (obar, data_table, obar->data_rows, obar->row_labels, FALSE);
  }

  /* FIXME: check for construction of legends. */

  if (obar->legend_view && obar->col_labels) {
    add_col_labels (obar, data_table, obar->data_cols, obar->col_labels, 
		    obar->legend_reversed);
  }

  if (obar->col_colors) {
    color_palette = build_bar_colors (obar, FALSE);
    color_palette_legend = build_bar_colors (obar, obar->legend_reversed);
  }
  
  guppi_element_state_set (barchart_state,
			   "data", data_table,
			   "bar_colors", color_palette,
			   "stacked", obar->stacked,
			   "normalize_stacks", obar->normalize_stacks,
			   NULL);
  
  font = obar->axis_font;
  if (font == NULL)
    font = guppi_default_font ();

  bottom_axis_state = guppi_element_view_state (obar->bottom_axis_view);
  guppi_ref (bottom_axis_state);

  guppi_element_state_set (bottom_axis_state,
			   "major_label_font", font,
			   "rotate_labels", obar->rot_x_axis_labels,
			   "legend", obar->x_axis_label,
			   "legend_font", font,
			   NULL);

  left_axis_state = guppi_element_view_state(obar->left_axis_view);
  guppi_ref (left_axis_state);

  guppi_element_state_set (left_axis_state,
			   "major_label_font", font,
			   "rotate_labels", obar->rot_y_axis_labels,
			   "legend", obar->y_axis_label,
			   "legend_font", font,
			   NULL);
  

  frame_state = guppi_element_view_state(obar->frame_view);
  guppi_ref (frame_state);
  
  if (obar->col_labels != NULL) {

    font = obar->legend_font;
    if (font == NULL)
      font = guppi_default_font ();


    guppi_element_state_set (legend_state,
			     "labels", data_table,
			     "swatch_colors", color_palette_legend,
			     "label_font", font,
			     NULL);
  }
  
 
  /* clean up */
  
  guppi_unref (color_palette);
  guppi_unref (color_palette_legend);
  guppi_unref (barchart_state);
  guppi_unref (legend_state);			   
  guppi_unref (bottom_axis_state);
  guppi_unref (left_axis_state);  
  guppi_unref (frame_state);  
  guppi_unref (data_table);
}


static gpointer
build (GuppiObject *obj, double hsize, double vsize)
{
  GuppiObjectBarchart *obar;
  GnomeFont *font;
  
  GuppiElementState *grp_state;
  GuppiGroupView *grp_view;

  GuppiDataTable *data_table;
  GuppiColorPalette *color_palette = NULL;
  GuppiColorPalette *color_palette_legend = NULL;

  GuppiElementState *barchart_state;
  GuppiElementState *legend_state = NULL;
  GuppiElementState *left_axis_state;
  GuppiElementState *bottom_axis_state;
  GuppiElementState *frame_state = NULL;

  GuppiElementView *barchart_view;
  GuppiElementView *legend_view = NULL;
  GuppiElementView *left_axis_view;
  GuppiElementView *bottom_axis_view;
  GuppiElementView *frame_view = NULL;

  const double outer_margin = 3.6;
  const double inner_gap = 3.6;

  obar = GUPPI_OBJECT_BARCHART (obj);

  if (obar->data == NULL) {
    g_warning ("No barchart data specified.");
    return NULL;
  }

  grp_state = guppi_group_state_new ();
  grp_view = GUPPI_GROUP_VIEW (guppi_element_state_make_view (grp_state));
  guppi_unref0 (grp_state);

  data_table = build_bar_data (obar);
  
  /* Build up data objects for our labels. */
  if (obar->row_labels) {
    add_row_labels (obar, data_table, obar->data_rows, obar->row_labels, FALSE);
  }

  if (obar->col_labels) {
    add_col_labels (obar, data_table, obar->data_cols, obar->col_labels, 
		    obar->legend_reversed);
  }


  /* Build up our color data */

  if (obar->col_colors) {
    color_palette = build_bar_colors (obar, FALSE);
    color_palette_legend = build_bar_colors (obar, obar->legend_reversed);
  }

  barchart_state = guppi_element_state_new ("barchart",
					    "data", data_table,
					    "bar_colors", color_palette,
					    "stacked", obar->stacked,
					    "normalize_stacks", obar->normalize_stacks,
					    NULL);

  barchart_view = guppi_element_state_make_view (barchart_state);

  font = obar->axis_font;
  if (font == NULL)
    font = guppi_default_font ();

  bottom_axis_state = guppi_element_state_new ("axis",
					       "position", GUPPI_SOUTH,
					       "major_label_font", font,
					       "rotate_labels", obar->rot_x_axis_labels,
					       "legend", obar->x_axis_label,
					       "legend_font", font,
					       NULL);
  bottom_axis_view = guppi_element_state_make_view (bottom_axis_state);
  guppi_element_view_set_tool_blocking (bottom_axis_view, TRUE);

  left_axis_state = guppi_element_state_new ("axis",
					     "position", GUPPI_WEST,
					     "major_label_font", font,
					     "rotate_labels", obar->rot_y_axis_labels,
					     "legend", obar->y_axis_label,
					     "legend_font", font,
					     NULL);

  left_axis_view = guppi_element_state_make_view (left_axis_state);
  guppi_element_view_set_tool_blocking (left_axis_view, TRUE);

  frame_state = guppi_element_state_new ("frame", NULL);
  frame_view = guppi_element_state_make_view (frame_state);

  guppi_element_view_connect_axis_markers (barchart_view, GUPPI_X_AXIS, bottom_axis_view, GUPPI_X_AXIS);
  guppi_element_view_connect_axis_markers (barchart_view, GUPPI_Y_AXIS, left_axis_view, GUPPI_Y_AXIS);
  guppi_element_view_connect_axis_markers (barchart_view, GUPPI_X_AXIS, frame_view, GUPPI_X_AXIS);
  guppi_element_view_connect_axis_markers (barchart_view, GUPPI_Y_AXIS, frame_view, GUPPI_Y_AXIS);

  if (obar->col_labels != NULL) {

    font = obar->legend_font;
    if (font == NULL)
      font = guppi_default_font ();

    legend_state = guppi_element_state_new ("legend",
					    "labels", data_table,
					    "swatch_colors", 
					    color_palette_legend,
					    "label_font", font,
					    NULL);

    legend_view = guppi_element_state_make_view (legend_state);

  }

  guppi_element_view_set_preferred_view (barchart_view, GUPPI_X_AXIS);
  guppi_element_view_set_preferred_view (barchart_view, GUPPI_Y_AXIS);

  /* Do the layout */

  guppi_group_view_layout_flush_top (grp_view, frame_view, outer_margin);

  guppi_group_view_layout_flush_bottom (grp_view, bottom_axis_view,
					outer_margin);

  guppi_group_view_layout_flush_left (grp_view, left_axis_view, outer_margin);

  guppi_group_view_layout_horizontally_aligned (grp_view, left_axis_view, frame_view, inner_gap);
  guppi_group_view_layout_vertically_aligned (grp_view, frame_view, bottom_axis_view, inner_gap);

  if (legend_view) {

    guppi_group_view_layout_flush_right (grp_view, legend_view, outer_margin);
    guppi_group_view_layout_horizontally_adjacent (grp_view, frame_view, legend_view, inner_gap);
    guppi_group_view_layout_same_vertical_center (grp_view, legend_view, frame_view);

  } else {

    guppi_group_view_layout_flush_right (grp_view, frame_view, outer_margin);

  }

  guppi_group_view_layout_same_place (grp_view, frame_view, barchart_view);

  /* Remember our view objects */

  obar->legend_view = legend_view;
  obar->barchart_view = barchart_view;
  obar->bottom_axis_view = bottom_axis_view;
  obar->left_axis_view = left_axis_view;
  obar->frame_view = frame_view;


  /* Clean up after ourselves... */

  guppi_unref (data_table);
  guppi_unref (color_palette);
  guppi_unref (color_palette_legend);

  guppi_unref (barchart_state);
  guppi_unref (bottom_axis_state);
  guppi_unref (left_axis_state);
  guppi_unref (legend_state);

  return GUPPI_ELEMENT_VIEW (grp_view);
}

static void
clicked_bar_cb (GuppiCanvasItem *item,
		gint r, gint c, guint button, guint state,
		GuppiObjectBarchart *obar)
{
  switch (button) {

  case 1:
    if (obar->bar_callback1)
      obar->bar_callback1 (r, c, obar->bar_callback1_data);
    break;

  case 2:
    if (obar->bar_callback2)
      obar->bar_callback2 (r, c, obar->bar_callback2_data);
    break;

  case 3:
    if (obar->bar_callback3)
      obar->bar_callback3 (r, c, obar->bar_callback3_data);
    break;

  default:
  }
}

static void
clicked_box_cb (GuppiCanvasItem *item,
		gint box, guint button, guint state,
		GuppiObjectBarchart *obar)
{
  switch (button) {

  case 1:
    if (obar->legend_callback1)
      obar->legend_callback1 ((obar->legend_reversed) ?
			      (obar->data_cols - 1 - box) : box, 
			      obar->legend_callback1_data);
    break;
    
  case 2:
    if (obar->legend_callback2)
      obar->legend_callback2 ((obar->legend_reversed) ?
			      (obar->data_cols - 1 - box) : box, 
			      obar->legend_callback2_data);
    break;
    
  case 3:
    if (obar->legend_callback3)
      obar->legend_callback3 ((obar->legend_reversed) ?
			      (obar->data_cols - 1 - box) : box, 
			      obar->legend_callback3_data);
    break;

  default:
  }
}

static void
item_init (GuppiObject *obj, GnomeCanvasItem *item)
{
  GuppiObjectBarchart *obar = GUPPI_OBJECT_BARCHART (obj);
  GuppiCanvasGroup *grp_item = GUPPI_CANVAS_GROUP (item);
  GuppiCanvasItem *barchart_item;
  GuppiCanvasItem *legend_item;

  barchart_item = guppi_canvas_group_find_by_view (grp_item,
						   obar->barchart_view);
  if (barchart_item) {
    gtk_signal_connect (GTK_OBJECT (barchart_item),
			"clicked_bar", GTK_SIGNAL_FUNC (clicked_bar_cb), obj);
  }

  legend_item = guppi_canvas_group_find_by_view (grp_item, obar->legend_view);
  if (legend_item)
    gtk_signal_connect (GTK_OBJECT (legend_item),
			"clicked_box", GTK_SIGNAL_FUNC (clicked_box_cb), obj);

}

/***************************************************************************/

#define add_arg(str, t, symb) \
gtk_object_add_arg_type("GuppiObjectBarchart::" str, t, GTK_ARG_WRITABLE, symb)

static void
guppi_object_barchart_class_init (GuppiObjectBarchartClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiObjectClass *gobj_class = GUPPI_OBJECT_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_OBJECT);

  object_class->set_arg = guppi_object_barchart_set_arg;
  object_class->destroy = guppi_object_barchart_destroy;
  object_class->finalize = guppi_object_barchart_finalize;

  gobj_class->build = build;
  gobj_class->item_init = item_init;
  gobj_class->update = update;

  add_arg ("data_rows", GTK_TYPE_INT, ARG_DATA_ROWS);
  add_arg ("data_columns", GTK_TYPE_INT, ARG_DATA_COLUMNS);
  add_arg ("data", GTK_TYPE_POINTER, ARG_DATA);
  add_arg ("row_labels", GTK_TYPE_POINTER, ARG_ROW_LABELS);
  add_arg ("column_labels", GTK_TYPE_POINTER, ARG_COLUMN_LABELS);
  add_arg ("column_colors", GTK_TYPE_POINTER, ARG_COLUMN_COLORS);
  add_arg ("column_colors_rgb", GTK_TYPE_POINTER, ARG_COLUMN_COLORS_RGB);
  add_arg ("legend_reversed", GTK_TYPE_BOOL, ARG_LEGEND_REVERSED);

  add_arg ("legend_font", GTK_TYPE_POINTER, ARG_LEGEND_FONT);
  add_arg ("axis_font", GTK_TYPE_POINTER, ARG_AXIS_FONT);

  add_arg ("rotate_x_axis_labels", GTK_TYPE_BOOL, ARG_ROTATE_X_AXIS_LABELS);
  add_arg ("rotate_y_axis_labels", GTK_TYPE_BOOL, ARG_ROTATE_Y_AXIS_LABELS);

  add_arg ("stacked", GTK_TYPE_BOOL, ARG_STACKED);
  add_arg ("normalize_stacks", GTK_TYPE_BOOL, ARG_NORMALIZE_STACKS);

  add_arg ("bar_callback1", GTK_TYPE_POINTER, ARG_BAR_CALLBACK1);
  add_arg ("bar_callback1_data", GTK_TYPE_POINTER, ARG_BAR_CALLBACK1_DATA);
  add_arg ("bar_callback1_name", GTK_TYPE_POINTER, ARG_BAR_CALLBACK1_NAME);

  add_arg ("bar_callback2", GTK_TYPE_POINTER, ARG_BAR_CALLBACK2);
  add_arg ("bar_callback2_data", GTK_TYPE_POINTER, ARG_BAR_CALLBACK2_DATA);
  add_arg ("bar_callback2_name", GTK_TYPE_POINTER, ARG_BAR_CALLBACK2_NAME);

  add_arg ("bar_callback3", GTK_TYPE_POINTER, ARG_BAR_CALLBACK3);
  add_arg ("bar_callback3_data", GTK_TYPE_POINTER, ARG_BAR_CALLBACK3_DATA);
  add_arg ("bar_callback3_name", GTK_TYPE_POINTER, ARG_BAR_CALLBACK3_NAME);

  add_arg ("legend_callback1", GTK_TYPE_POINTER, ARG_LEGEND_CALLBACK1);
  add_arg ("legend_callback1_data", GTK_TYPE_POINTER,
	   ARG_LEGEND_CALLBACK1_DATA);
  add_arg ("legend_callback1_name", GTK_TYPE_POINTER,
	   ARG_LEGEND_CALLBACK1_NAME);

  add_arg ("legend_callback2", GTK_TYPE_POINTER, ARG_LEGEND_CALLBACK2);
  add_arg ("legend_callback2_data", GTK_TYPE_POINTER,
	   ARG_LEGEND_CALLBACK2_DATA);
  add_arg ("legend_callback2_name", GTK_TYPE_POINTER,
	   ARG_LEGEND_CALLBACK2_NAME);

  add_arg ("legend_callback3", GTK_TYPE_POINTER, ARG_LEGEND_CALLBACK3);
  add_arg ("legend_callback3_data", GTK_TYPE_POINTER,
	   ARG_LEGEND_CALLBACK3_DATA);
  add_arg ("legend_callback3_name", GTK_TYPE_POINTER,
	   ARG_LEGEND_CALLBACK3_NAME);

  add_arg ("x_axis_label", GTK_TYPE_POINTER, ARG_X_AXIS_LABEL);
  add_arg ("y_axis_label", GTK_TYPE_POINTER, ARG_Y_AXIS_LABEL);

}

static void
guppi_object_barchart_init (GuppiObjectBarchart *obj)
{

}

GtkType guppi_object_barchart_get_type (void)
{
  static GtkType guppi_object_barchart_type = 0;
  if (!guppi_object_barchart_type) {
    static const GtkTypeInfo guppi_object_barchart_info = {
      "GuppiObjectBarchart",
      sizeof (GuppiObjectBarchart),
      sizeof (GuppiObjectBarchartClass),
      (GtkClassInitFunc) guppi_object_barchart_class_init,
      (GtkObjectInitFunc) guppi_object_barchart_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_object_barchart_type =
      gtk_type_unique (GUPPI_TYPE_OBJECT, &guppi_object_barchart_info);
  }
  return guppi_object_barchart_type;
}

GtkObject *
guppi_object_barchart_new (void)
{
  return GTK_OBJECT (guppi_type_new (guppi_object_barchart_get_type ()));
}



/* $Id$ */
