/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-object.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_OBJECT_H
#define _INC_GUPPI_OBJECT_H

#include <gnome.h>
#include <libgnomeprint/gnome-print.h>

typedef struct _GuppiObject GuppiObject;
typedef struct _GuppiObjectClass GuppiObjectClass;
typedef struct _GuppiObjectPrivate GuppiObjectPrivate;

struct _GuppiObjectPrivate;

struct _GuppiObject {
  GtkObject parent;

  GuppiObjectPrivate *priv;
};

struct _GuppiObjectClass {
  GtkObjectClass parent_class;

  gpointer (*build) (GuppiObject *, double hsize, double vsize);
  void (*update) (GuppiObject *);
  void (*item_init) (GuppiObject *, GnomeCanvasItem *);
  void (*widget_init) (GuppiObject *, GtkWidget *);
};

#define GUPPI_TYPE_OBJECT (guppi_object_get_type())
#define GUPPI_OBJECT(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_OBJECT,GuppiObject))
#define GUPPI_OBJECT0(obj) ((obj) ? (GUPPI_OBJECT(obj)) : NULL)
#define GUPPI_OBJECT_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_OBJECT,GuppiObjectClass))
#define GUPPI_IS_OBJECT(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_OBJECT))
#define GUPPI_IS_OBJECT0(obj) (((obj) == NULL) || (GUPPI_IS_OBJECT(obj)))
#define GUPPI_IS_OBJECT_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_OBJECT))

GtkType guppi_object_get_type (void);

GuppiObject *guppi_object_new (const gchar *type,
			       double horizontal_size_in_ps_pts,
			       double vertical_size_in_ps_pts,
			       const gchar *first_arg_name, ...);
GuppiObject *guppi_object_newv (const gchar *type,
				double horizontal_size_in_ps_pts,
				double vertical_size_in_ps_pts,
				guint nargs, GtkArg *args);

/* Remember, all lengths are measured in postscript points. */
double guppi_object_width (GuppiObject *);
double guppi_object_height (GuppiObject *);

GtkWidget *guppi_object_build_widget (GuppiObject *);

void guppi_object_print (GuppiObject *, GnomePrintContext *);

void guppi_object_update (GuppiObject *);

/* This is for object implementers only! */
gpointer guppi_object_view (GuppiObject *);


#endif /* _INC_GUPPI_OBJECT_H */

/* $Id$ */
