/* $Id$ */

/*
 * guppi-embed-test.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/* #include <gnome.h> */
#include <libgnome/gnome-defs.h>

#include <libgnomeui/gnome-init.h>

#include <guppi-memory.h>
#include "guppi-tank.h"

static void
delete_event_cb (GtkWidget *w, GdkEvent *ev, gpointer user_data)
{
  gtk_widget_destroy (w);
  guppi_tank_exit ();
}

int
main (gint argc, gchar * argv[])
{

  GuppiObject *gobj;
  GuppiObject *title_obj;

  GtkWidget *win;
  GtkWidget *w;

  gint data_size = 1000, i;
  double *x_data;
  double *y_data;

  x_data = guppi_new0 (double, data_size);
  y_data = guppi_new0 (double, data_size);

  for (i = 0; i < data_size; ++i) {
    double t = i > 0 ? y_data[i - 1] : 0.7;
    x_data[i] = i;
    y_data[i] = 4 * t * (1 - t);
  }


  gnome_init ("demo-scatter", "0.0", argc, argv);
  //guppi_set_synchronous();
  guppi_tank_init ();

  win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_widget_set_usize (win, 400, 300);

  gobj = guppi_object_new ("scatter",
			   4 * 72, 3 * 72,
			   "data_size", data_size,
			   "x_data", x_data,
			   "x_axis_label", "Iteration",
			   "y_data", y_data,
			   "y_axis_label", "Value",
			   "color_rgba", 0x0000ff80,
			   "marker", "filled circle", NULL);

  guppi_free (x_data);
  guppi_free (y_data);

  title_obj = guppi_object_new ("title",
				4 * 72, 3 * 72,
				"title", "The Logistic Map",
				"subobject", gobj, NULL);

  w = guppi_object_build_widget (title_obj);

  guppi_unref (title_obj);

  gtk_container_add (GTK_CONTAINER (win), w);
  gtk_widget_show_all (win);

  gtk_signal_connect (GTK_OBJECT (win),
		      "delete_event",
		      GTK_SIGNAL_FUNC (delete_event_cb),
		      NULL);

  gtk_main ();

  return 0;
}




/* $Id$ */
