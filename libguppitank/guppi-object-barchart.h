/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-object-barchart.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_OBJECT_BARCHART_H
#define _INC_GUPPI_OBJECT_BARCHART_H

/* #include <gnome.h> */
#include "guppi-object.h"

typedef struct _GuppiObjectBarchart GuppiObjectBarchart;
typedef struct _GuppiObjectBarchartClass GuppiObjectBarchartClass;

struct _GuppiObjectBarchart {
  GuppiObject parent;

  gchar *x_axis_label;
  gchar *y_axis_label;

  gint data_rows, data_cols;
  double *data;
  gchar **row_labels;
  gchar **col_labels;
  guint32 *col_colors;
  gboolean legend_reversed;

  GnomeFont *legend_font;
  GnomeFont *axis_font;

  gboolean rot_x_axis_labels;
  gboolean rot_y_axis_labels;

  gboolean stacked;
  gboolean normalize_stacks;

  void (*bar_callback1) (gint, gint, gpointer);
  gpointer bar_callback1_data;
  gchar *bar_callback1_name;

  void (*bar_callback2) (gint, gint, gpointer);
  gpointer bar_callback2_data;
  gchar *bar_callback2_name;

  void (*bar_callback3) (gint, gint, gpointer);
  gpointer bar_callback3_data;
  gchar *bar_callback3_name;

  void (*legend_callback1) (gint, gpointer);
  gpointer legend_callback1_data;
  gchar *legend_callback1_name;

  void (*legend_callback2) (gint, gpointer);
  gpointer legend_callback2_data;
  gchar *legend_callback2_name;

  void (*legend_callback3) (gint, gpointer);
  gpointer legend_callback3_data;
  gchar *legend_callback3_name;

  GuppiElementView *barchart_view;
  GuppiElementView *legend_view;
  GuppiElementView *left_axis_view;
  GuppiElementView *bottom_axis_view;
  GuppiElementView *frame_view;
};

struct _GuppiObjectBarchartClass {
  GuppiObjectClass parent_class;
};

#define GUPPI_TYPE_OBJECT_BARCHART (guppi_object_barchart_get_type())
#define GUPPI_OBJECT_BARCHART(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_OBJECT_BARCHART,GuppiObjectBarchart))
#define GUPPI_OBJECT_BARCHART0(obj) ((obj) ? (GUPPI_OBJECT_BARCHART(obj)) : NULL)
#define GUPPI_OBJECT_BARCHART_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_OBJECT_BARCHART,GuppiObjectBarchartClass))
#define GUPPI_IS_OBJECT_BARCHART(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_OBJECT_BARCHART))
#define GUPPI_IS_OBJECT_BARCHART0(obj) (((obj) == NULL) || (GUPPI_IS_OBJECT_BARCHART(obj)))
#define GUPPI_IS_OBJECT_BARCHART_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_OBJECT_BARCHART))

GtkType guppi_object_barchart_get_type (void);

GtkObject *guppi_object_barchart_new (void);

#endif /* _INC_GUPPI_OBJECT_BARCHART_H */

/* $Id$ */
