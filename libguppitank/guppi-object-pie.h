/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-object-pie.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_OBJECT_PIE_H
#define _INC_GUPPI_OBJECT_PIE_H

/* #include <gnome.h> */
#include "guppi-object.h"

typedef struct _GuppiObjectPie GuppiObjectPie;
typedef struct _GuppiObjectPieClass GuppiObjectPieClass;

struct _GuppiObjectPie {
  GuppiObject parent;

  gint data_size;
  double *data;
  gchar **labels;
  gchar **colors;
  guint32 *colors_rgb;

  double radius_size;
  gboolean radius_lock, radius_maximize;

  GnomeFont *legend_font;

  void (*slice_callback1) (gint, gpointer);
  gpointer slice_callback1_data;
  gchar *slice_callback1_name;

  void (*slice_callback2) (gint, gpointer);
  gpointer slice_callback2_data;
  gchar *slice_callback2_name;

  void (*slice_callback3) (gint, gpointer);
  gpointer slice_callback3_data;
  gchar *slice_callback3_name;

  void (*legend_callback1) (gint, gpointer);
  gpointer legend_callback1_data;
  gchar *legend_callback1_name;

  void (*legend_callback2) (gint, gpointer);
  gpointer legend_callback2_data;
  gchar *legend_callback2_name;

  void (*legend_callback3) (gint, gpointer);
  gpointer legend_callback3_data;
  gchar *legend_callback3_name;



  /* Constructed Objects */

  GuppiElementView *title_view;
  GuppiElementView *pie_view;
  GuppiElementView *legend_view;

};

struct _GuppiObjectPieClass {
  GuppiObjectClass parent_class;
};

#define GUPPI_TYPE_OBJECT_PIE (guppi_object_pie_get_type())
#define GUPPI_OBJECT_PIE(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_OBJECT_PIE,GuppiObjectPie))
#define GUPPI_OBJECT_PIE0(obj) ((obj) ? (GUPPI_OBJECT_PIE(obj)) : NULL)
#define GUPPI_OBJECT_PIE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_OBJECT_PIE,GuppiObjectPieClass))
#define GUPPI_IS_OBJECT_PIE(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_OBJECT_PIE))
#define GUPPI_IS_OBJECT_PIE0(obj) (((obj) == NULL) || (GUPPI_IS_OBJECT_PIE(obj)))
#define GUPPI_IS_OBJECT_PIE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_OBJECT_PIE))

GtkType guppi_object_pie_get_type (void);


#endif /* _INC_GUPPI_OBJECT_PIE_H */

/* $Id$ */
