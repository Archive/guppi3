/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-object-scatter.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_OBJECT_SCATTER_H
#define _INC_GUPPI_OBJECT_SCATTER_H

/* #include <gnome.h> */
#include <guppi-useful.h>
#include <guppi-marker.h>
#include "guppi-object.h"

typedef struct _GuppiObjectScatter GuppiObjectScatter;
typedef struct _GuppiObjectScatterClass GuppiObjectScatterClass;

struct _GuppiObjectScatter {
  GuppiObject parent;

  gchar *x_axis_label;
  gchar *y_axis_label;

  gint data_size;
  double *x_data;
  double *y_data;

  GuppiMarker marker;
  double size1, size2;
  guint32 color;

  GnomeFont *axis_label_font;
  GnomeFont *axis_marker_font;
};

struct _GuppiObjectScatterClass {
  GuppiObjectClass parent_class;
};

#define GUPPI_TYPE_OBJECT_SCATTER (guppi_object_scatter_get_type())
#define GUPPI_OBJECT_SCATTER(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_OBJECT_SCATTER,GuppiObjectScatter))
#define GUPPI_OBJECT_SCATTER0(obj) ((obj) ? (GUPPI_OBJECT_SCATTER(obj)) : NULL)
#define GUPPI_OBJECT_SCATTER_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_OBJECT_SCATTER,GuppiObjectScatterClass))
#define GUPPI_IS_OBJECT_SCATTER(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_OBJECT_SCATTER))
#define GUPPI_IS_OBJECT_SCATTER0(obj) (((obj) == NULL) || (GUPPI_IS_OBJECT_SCATTER(obj)))
#define GUPPI_IS_OBJECT_SCATTER_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_OBJECT_SCATTER))

GtkType guppi_object_scatter_get_type (void);

GtkObject *guppi_object_scatter_new (void);

#endif /* _INC_GUPPI_OBJECT_SCATTER_H */

/* $Id$ */
