/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* vim: set sw=8: */

/*
 * guppi-gnumeric-
 *
 * Copyright (C) 2001 Ximian, Inc
 *
 * Developed by Jody Goldberg (jgoldberg@home.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-gnumeric-graph.h"
#include "guppi-gnumeric-xml.h"
#include "guppi-gnumeric-vector.h"
#include "guppi-data-table-bundle.h"

#include <guppi-memory.h>
#include <gal/util/e-xml-utils.h>

static GupGnmSeriesElements const bar_spec[] = {
	{ N_("Values"), 	 "values",	FALSE, FALSE },
	{ N_("Category Labels"), "categories",	TRUE, TRUE },
	SERIES_ELEMENT_LAST
};

static void
bar_arrange_data (GupGnmGraph const *graph,
		  xmlNode *plot, xmlNode *data)
{
	gup_gnm_graph_arrange_data (graph, plot, data, NULL, 0);
}

static GSList *
bar_plot (GupGnmGraph *graph, xmlNode *plot)
{
	GuppiElementState *bar_state;
	GuppiColorPalette *colours;
	xmlNodePtr first_series;
	int cols = 0, rows = 0;

	xmlNode *desc = e_xml_get_child_by_name (plot, "Type");
	gboolean const is_horizontal	=
		gup_gnm_attr_get_bool (desc, "horizontal", TRUE);
	gboolean const is_stacked	=
		gup_gnm_attr_get_bool (desc, "stacked", FALSE);
	gboolean const as_percentage	=
		gup_gnm_attr_get_bool (desc, "as_percentage", FALSE);

	GuppiSeqString *categories = NULL;
	xmlNode *series = e_xml_get_child_by_name (plot, "Data");
	GuppiDataTableBundle *table =
		GUPPI_DATA_TABLE_BUNDLE (guppi_data_table_bundle_new ());

	g_assert (table != NULL);
	g_return_val_if_fail (series != NULL, NULL);

	cols = 0;

	first_series = series->xmlChildrenNode;

	/* First, walk through the tree and count how many series we have. */
	for (series = first_series; series != NULL; series = series->next) {
		GuppiSeqScalar *data;
		
		if (strcmp (series->name, "Series"))
			continue;
		data = gup_gnm_series_dim_get_scalar (series, "values", graph);
		if (data != NULL) {
			gint i1 = guppi_seq_max_index (GUPPI_SEQ (data)) + 1;
			if (i1 > rows)
				rows = i1;
			++cols;
		}
	}

	guppi_data_table_set_dimensions (GUPPI_DATA_TABLE (table), rows, cols);

	cols = 0;
	colours = guppi_color_palette_new ();

	/* Next, walk through the tree again and populate our data table.*/
	for (series = first_series; series != NULL ; series = series->next) {
		GuppiSeqString *tmp;
		GuppiSeqScalar *values;

		if (strcmp (series->name, "Series"))
			continue;
		values = gup_gnm_series_dim_get_scalar (series, "values", graph);
		if (values == NULL)
			continue;
		guppi_data_table_bundle_set_col_data (table,
			cols, values);

		tmp = gup_gnm_series_dim_get_string (series, "categories", graph);
		if (tmp != NULL) {
			if (categories == NULL) {
				categories = tmp;
				guppi_data_table_bundle_set_row_labels (table,
					categories);
			} else if (categories != tmp) {
				g_warning ("Multiple categories for bars in the same plot is not supported");
			}
		}

		guppi_color_palette_set (colours, cols,
			gup_gnm_graph_get_series_color (graph, series));
		cols++;
	}

	/* Default labels */
	if (categories == NULL)
		guppi_data_table_bundle_set_row_labels (table,
			gup_gnm_graph_stock_labels (graph, rows));

	/* Build our state objects */
	bar_state = guppi_element_state_new ("barchart",
		"data", 		table,
		"stacked",	  	is_stacked || as_percentage,
		"normalize_stacks",	as_percentage,
		"vertical_bars",	!is_horizontal,
		"use_stock_colors",	FALSE,

		/* Ideally this would be 0 for an
		 * un-anti-aliased un-scaled hair width
		 * line */
		"edge_thickness",	.5,

		"bar_colors",		colours,

		/* What is this measured in ? */
		"cluster_margin",	0.3,
		NULL);

	return g_slist_append (NULL, bar_state);
}

static void
enforce_pref (GuppiElementView *view)
{
	guppi_element_view_force_preferred_view (view, GUPPI_X_AXIS, TRUE);
	guppi_element_view_force_preferred_view (view, GUPPI_Y_AXIS, TRUE);
}

GupGnmPlotDescriptor const gup_gnm_bar_plot_type =
	{ "Bar",     bar_spec,     FALSE, bar_plot,	bar_arrange_data,     enforce_pref };
