/* This is -*- C -*- */
/* vim: set sw=8: */

/*
 * guppi-gnumeric-config-guru.h
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 * Developed by Jody Goldberg <jgoldberg@home.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_GNUMERIC_DATA_GURU_H
#define _INC_GUPPI_GNUMERIC_DATA_GURU_H

#include "guppi-gnumeric.h"
#include <gtk/gtktypeutils.h>

BEGIN_GUPPI_DECLS

#define GUP_GNM_DATA_GURU_TYPE        (gup_gnm_data_guru_get_type ())
#define GUP_GNM_DATA_GURU(o)          (GTK_CHECK_CAST ((o), GUP_GNM_DATA_GURU_TYPE, GupGnmDataGuru))
#define IS_GUP_GNM_DATA_GURU(o)       (GTK_CHECK_TYPE ((o), GUP_GNM_DATA_GURU_TYPE))

GtkType gup_gnm_data_guru_get_type (void);
GupGnmConfigGuru *gup_gnm_data_guru_new (GupGnmGraph *graph);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_GNUMERIC_DATA_GURU_H */
