/* This is -*- C -*- */
/* vim: set sw=8: */

/*
 * guppi-gnumeric-config-guru.h
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 * Developed by Jody Goldberg <jgoldberg@home.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_GNUMERIC_CONFIG_GURU_H
#define _INC_GUPPI_GNUMERIC_CONFIG_GURU_H

#include "guppi-gnumeric.h"
#include "guppi-gnumeric-version.h"
#include "guppi-gnumeric-graph.h"
#include "GNOME_Gnumeric_Graph.h"
#include <bonobo.h>

BEGIN_GUPPI_DECLS

struct _GupGnmConfigGuru {
	BonoboControl parent;

	GupGnmGraph *graph;
};

typedef struct {
	BonoboControlClass parent_class;

	POA_GUPPI_GNUMERIC_CONFIG_GURU(epv)	epv;
} GupGnmConfigGuruClass;

#define GUP_GNM_CONFIG_GURU_TYPE        (gup_gnm_config_guru_get_type ())
#define GUP_GNM_CONFIG_GURU(o)          (GTK_CHECK_CAST ((o), GUP_GNM_CONFIG_GURU_TYPE, GupGnmConfigGuru))
#define GUP_GNM_CONFIG_GURU_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GUP_GNM_CONFIG_GURU_TYPE, GupGnmConfigGuruClass))
#define IS_GUP_GNM_CONFIG_GURU(o)       (GTK_CHECK_TYPE ((o), GUP_GNM_CONFIG_GURU_TYPE))
#define IS_GUP_GNM_CONFIG_GURU_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GUP_GNM_CONFIG_GURU_TYPE))

GtkType gup_gnm_config_guru_get_type (void);
GupGnmConfigGuru *gup_gnm_config_guru_new	(GupGnmGraph *graph, GtkWidget *w);
GupGnmConfigGuru *gup_gnm_config_guru_construct	(GupGnmConfigGuru *cguru,
						 GupGnmGraph *graph, GtkWidget *w);

GupGnmConfigGuru *gup_gnm_config_guru_from_servant (PortableServer_Servant s);
GupGnmGraph	 *gup_gnm_config_guru_get_graph	   (GupGnmConfigGuru *cguru);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_GNUMERIC_CONFIG_GURU_H */
