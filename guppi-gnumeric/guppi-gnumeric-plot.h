/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-gnumeric-plot.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUP_GNM_PLOT_H
#define _INC_GUP_GNM_PLOT_H

#include <gnome.h>
#include "guppi-defs.h"

#include "guppi-root-group-view.h"

BEGIN_GUPPI_DECLS;

typedef struct _GupGnmPlot GupGnmPlot;
typedef struct _GupGnmPlotClass GupGnmPlotClass;

struct _GupGnmPlot {
  GtkObject parent;

  gchar *type_name;
  GuppiRootGroupView *rgv;
};

struct _GupGnmPlotClass {
  GtkObjectClass parent_class;

  /* methods */
  xmlNode            *(*serialize)    (GupGnmPlot *);
  GuppiRootGroupView *(*unserialize)  (GupGnmPlot *, xmlNode *);
  GuppiConfigModel   *(*config_model) (GupGnmPlot *);
  
  /* signals */
  void (*regenerated) (GupGnmPlot *, GuppiRootGroupView *old_view, GuppiRootGroupView *new_view);
};

#define GUP_GNM_PLOT_TYPE (gup_gnm_plot_get_type ())
#define GUP_GNM_PLOT(obj) (GTK_CHECK_CAST((obj),GUP_GNM_PLOT_TYPE,GupGnmPlot))
#define GUP_GNM_PLOT_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUP_GNM_PLOT_TYPE,GupGnmPlotClass))
#define IS_GUP_GNM_PLOT(obj) (GTK_CHECK_TYPE((obj), GUP_GNM_PLOT_TYPE))
#define IS_GUP_GNM_PLOT_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUP_GNM_PLOT_TYPE))

GtkType gup_gnm_plot_get_type (void);

void gup_gnm_plot_register_type (const gchar *name, GtkType type);

GupGnmPlot *gup_gnm_plot_new (xmlNode *);

xmlNode *gup_gnm_plot_serialize   (GupGnmPlot *);
void     gup_gnm_plot_unserialize (GupGnmPlot *, xmlNode *);

GuppiRootGroupView *gup_gnm_plot_get_root_group_view (GupGnmPlot *);
GnomeCanvas        *gup_gnm_plot_make_canvas         (GupGnmPlot *);
GuppiConfigModel   *gup_gnm_plot_make_config_model   (GupGnmPlot *);



END_GUPPI_DECLS;

#endif /* _INC_GUP_GNM_PLOT_H */

/* $Id$ */
