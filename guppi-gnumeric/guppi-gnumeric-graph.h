/* vim: set sw=8: */

/*
 * guppi-gnumeric-graph.h
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 * Developed by Jody Goldberg <jgoldberg@home.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_GNUMERIC_GRAPH_H
#define _INC_GUPPI_GNUMERIC_GRAPH_H

#include "guppi-gnumeric.h"
#include <guppi-seq-string.h>
#include <guppi-seq-scalar.h>
#include <guppi-marker.h>
#include <guppi-canvas-item.h>
#include <guppi-color-palette.h>
#include <guppi-root-group-view.h>

BEGIN_GUPPI_DECLS

struct _GupGnmGraph {
	GupGnmManager		*manager;
	GuppiRootGroupView	*plot;

	GList	  *views;
	xmlDoc	  *spec;
	GPtrArray *wrapper_sequences;

	struct {
		GuppiSeqString 	  *names;
		GuppiColorPalette *colours;
		GArray		  *markers;
	} series;
};

void gup_gnm_graph_construct (GupGnmGraph *graph, GupGnmManager *manager);
void gup_gnm_graph_release   (GupGnmGraph *graph);

void	 gup_gnm_graph_regenerate_plots	 (GupGnmGraph *graph);
gboolean gup_gnm_graph_set_series_dim	 (GupGnmGraph *graph,
					  int seriesID, char const *dim,
					  int vectorID);
void	 gup_gnm_graph_arrange_data	 (GupGnmGraph const *graph,
					  xmlNode *plot, xmlNode *data,
					  char const **extras, int extra_count);
void	 gup_gnm_graph_store_series_name (GupGnmGraph *graph,
					  int seriesID, char const *name);
void	 gup_gnm_graph_markup_spec	 (GupGnmGraph *graph);
void	 gup_gnm_graph_set_spec		 (GupGnmGraph *graph,
					  xmlDoc *doc, gboolean copy);
void	 gup_gnm_graph_set_plottype	 (GupGnmGraph *graph, xmlNode *plot);
void	 gup_gnm_graph_generate_series	 (GupGnmGraph *graph);
void	 gup_gnm_graph_regenerate_plots	 (GupGnmGraph *graph);

void	 gup_gnm_graph_add_wrapper	   (GupGnmGraph *graph,  GuppiData *wrapper);
GuppiSeqScalar *gup_gnm_graph_stock_index  (GupGnmGraph *graph, int const n);
GuppiSeqString *gup_gnm_graph_stock_labels (GupGnmGraph *graph, int const n);

GuppiRootGroupView *gup_gnm_graph_get_view (GupGnmGraph *graph);

guint32	    gup_gnm_graph_get_series_color  (GupGnmGraph const *g, xmlNode *series);
GuppiMarker gup_gnm_graph_get_series_marker (GupGnmGraph const *g, xmlNode *series);

/******************************************************************************/
typedef struct {
	char const *display_name;
	char const *dim_name;
	gboolean    optional;
	gboolean    shared;	/* common to all series */
} GupGnmSeriesElements;
/* Name is always added */
#define SERIES_ELEMENT_LAST	{ NULL, FALSE, FALSE }

typedef struct {
	char const *name;
	GupGnmSeriesElements const *spec;
	gboolean	singleton;
	GSList * (*plot)	 (GupGnmGraph *graph, xmlNode *plot);
	void     (*arrange_data) (GupGnmGraph const *graph, xmlNode *plot, xmlNode *data);
	void     (*process_view) (GuppiElementView *view);
} GupGnmPlotDescriptor;

GupGnmPlotDescriptor const *gup_gnm_plot_get_descriptor (xmlNode *plot);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_GNUMERIC_GRAPH_H */
