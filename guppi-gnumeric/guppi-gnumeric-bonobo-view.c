/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* vim: set sw=8: */

/*
 * guppi-gnumeric-bonobo-view.c: manage bonobo views of graphs
 *
 * Copyright (C) 2001 Jody Goldberg (jgoldberg@home.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-gnumeric-bonobo-view.h"
#include "guppi-gnumeric-manager.h"
#include "guppi-gnumeric-view.h"
#include <guppi-memory.h>
#include <guppi-root-group-item.h>

typedef struct {
	BonoboView parent;

	GupGnmView	view;
} GupGnmBonoboView;

typedef struct {
	BonoboViewClass parent_class;
} GupGnmBonoboViewClass;

#define GUP_GNM_BONOBO_VIEW_TYPE        (gup_gnm_bonobo_view_get_type ())
#define GUP_GNM_BONOBO_VIEW(o)          (GTK_CHECK_CAST ((o), GUP_GNM_BONOBO_VIEW_TYPE, GupGnmBonoboView))
#define GUP_GNM_BONOBO_VIEW_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GUP_GNM_BONOBO_VIEW_TYPE, GupGnmBonoboViewClass))
#define IS_GUP_GNM_BONOBO_VIEW(o)       (GTK_CHECK_TYPE ((o), GUP_GNM_BONOBO_VIEW_TYPE))
#define IS_GUP_GNM_BONOBO_VIEW_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GUP_GNM_BONOBO_VIEW_TYPE))
#define SERVANT_TO_GUP_GNM_BONOBO_VIEW(ptr) (GupGnmBonoboView *)(((void *)ptr) - GTK_STRUCT_OFFSET(GupGnmBonoboView, servant))

static BonoboViewClass *gup_gnm_bonobo_view_parent_class = NULL;

BonoboView *
gup_gnm_bonobo_view_factory (BonoboEmbeddable *embeddable,
			     const Bonobo_ViewFrame view_frame, void *closure)
{
	GupGnmBonoboView *bview;
	GupGnmManager *manager = GUP_GNM_MANAGER (embeddable);

	g_return_val_if_fail (manager != NULL, NULL);

	bview = guppi_type_new (GUP_GNM_BONOBO_VIEW_TYPE);
	gup_gnm_view_construct (&bview->view, &manager->graph);

	gtk_widget_show_all (GTK_WIDGET (bview->view.canvas));
	bonobo_view_construct (BONOBO_VIEW (bview),
		GTK_WIDGET (bview->view.canvas));
	bonobo_view_set_view_frame (BONOBO_VIEW (bview), view_frame);

	return BONOBO_VIEW (bview);
}

static void
gup_gnm_bonobo_view_destroy (GtkObject *obj)
{
	GupGnmBonoboView *bview = GUP_GNM_BONOBO_VIEW (obj);

	ggd (1, printf ("Destroy view %p \n", bview););

	GTK_OBJECT_CLASS (gup_gnm_bonobo_view_parent_class)->destroy (obj);
}

static void
gup_gnm_bonobo_view_activate (BonoboControl *control, gboolean state)
{
	bonobo_view_activate_notify (BONOBO_VIEW (control), state);
}

static void
gup_gnm_bonobo_view_class_init (GupGnmBonoboViewClass *klass)
{
	GtkObjectClass *object_class = (GtkObjectClass *) klass;
	BonoboControlClass *control_class = (BonoboControlClass *) klass;

	gup_gnm_bonobo_view_parent_class = gtk_type_class (bonobo_view_get_type ());
	control_class->activate = gup_gnm_bonobo_view_activate;
	object_class->destroy   = gup_gnm_bonobo_view_destroy;
}

GtkType
gup_gnm_bonobo_view_get_type (void)
{
	static GtkType type = 0;

	if (!type) {
		GtkTypeInfo info = {
			"GupGnmBonoboView",
			sizeof (GupGnmBonoboView),
			sizeof (GupGnmBonoboViewClass),
			(GtkClassInitFunc) gup_gnm_bonobo_view_class_init,
			(GtkObjectInitFunc) NULL,
			NULL,			/* reserved 1 */
			NULL,			/* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		type = bonobo_x_type_unique (
			bonobo_view_get_type (),
			NULL, NULL, 0,
			&info);
	}

	return type;
}
