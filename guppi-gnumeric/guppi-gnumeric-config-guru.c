/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* vim: set sw=8: */

/*
 * guppi-gnumeric-config-guru.c: Manage
 *
 * Copyright (C) 2001 Jody Goldberg (jgoldberg@home.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-gnumeric-config-guru.h"
#include "guppi-gnumeric-manager.h"

#include <guppi-memory.h>

static BonoboControlClass *gup_gnm_config_guru_parent_class = NULL;

GupGnmConfigGuru *
gup_gnm_config_guru_from_servant (PortableServer_Servant servant)
{
	return GUP_GNM_CONFIG_GURU (bonobo_object_from_servant (servant));
}

GupGnmGraph *
gup_gnm_config_guru_get_graph (GupGnmConfigGuru *cguru)
{
	g_return_val_if_fail (IS_GUP_GNM_CONFIG_GURU (cguru), NULL);
	return cguru->graph;
}

static void
gup_gnm_config_guru_destroy (GtkObject *obj)
{
	GupGnmConfigGuru *cguru = GUP_GNM_CONFIG_GURU (obj);

	ggd (1, printf ("Destroy guru %p \n", cguru););

	GTK_OBJECT_CLASS (gup_gnm_config_guru_parent_class)->destroy (obj);
}

static void
impl_applyChanges (PortableServer_Servant servant, CORBA_Environment *ev)
{
	GupGnmConfigGuru *cguru = gup_gnm_config_guru_from_servant (servant);

	g_return_if_fail (cguru != NULL);
	ggd(5, printf ("applychanges config-guru == %p\n", cguru));

	gup_gnm_graph_set_spec (&cguru->graph->manager->graph, cguru->graph->spec, TRUE);
}

static void
gup_gnm_config_guru_class_init (GupGnmConfigGuruClass *klass)
{
	GtkObjectClass *object_class = (GtkObjectClass *) klass;
	POA_GUPPI_GNUMERIC_CONFIG_GURU(epv) *epv = &klass->epv;

	gup_gnm_config_guru_parent_class = gtk_type_class (bonobo_control_get_type ());
	object_class->destroy   = gup_gnm_config_guru_destroy;

	epv->applyChanges	= impl_applyChanges;
}

GtkType
gup_gnm_config_guru_get_type (void)
{
	static GtkType type = 0;

	if (!type) {
		GtkTypeInfo info = {
			"GupGnmConfigGuru",
			sizeof (GupGnmConfigGuru),
			sizeof (GupGnmConfigGuruClass),
			(GtkClassInitFunc) gup_gnm_config_guru_class_init,
			(GtkObjectInitFunc) NULL,
			NULL,			/* reserved 1 */
			NULL,			/* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		type = bonobo_x_type_unique (
			bonobo_control_get_type (),
			POA_GUPPI_GNUMERIC_CONFIG_GURU(init),
			POA_GUPPI_GNUMERIC_CONFIG_GURU(fini),
			GTK_STRUCT_OFFSET (GupGnmConfigGuruClass, epv),
			&info);
	}

	return type;
}

GupGnmConfigGuru *
gup_gnm_config_guru_construct (GupGnmConfigGuru *cguru,
			       GupGnmGraph *graph, GtkWidget *w)
{
	gtk_widget_show_all (w);

	ggd(5, printf ("gup_gnm_config_guru_construct == %p\n", cguru));

	cguru->graph = graph;

	bonobo_control_construct (BONOBO_CONTROL (cguru), w);
	return cguru;
}

GupGnmConfigGuru *
gup_gnm_config_guru_new (GupGnmGraph *graph, GtkWidget *w)
{
	GupGnmConfigGuru *cguru = guppi_type_new (GUP_GNM_CONFIG_GURU_TYPE);

	ggd(5, printf ("gup_gnm_config_guru_new == %p\n", cguru));

	gup_gnm_config_guru_construct (cguru, graph, w);
	return cguru;
}
