/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* vim: set sw=8: */

/*
 * guppi-gnumeric-type-selector.c
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * Developed by Jody Goldberg <jgoldberg@home.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-gnumeric-format-guru.h"
#include "guppi-gnumeric-config-guru.h"
#include "guppi-gnumeric-graph.h"
#include "guppi-gnumeric-view.h"
#include "guppi-gnumeric-xml.h"

#include <guppi-paths.h>
#include <guppi-memory.h>
#include <glade/glade.h>
#include <gal/util/e-util.h>
#include <gal/util/e-xml-utils.h>
#include <gnome-xml/parser.h>	/* why is xmlStrdup in parser.h ? */

typedef struct
{
	GupGnmConfigGuru	parent;

	GupGnmView view;
	GladeXML   *glade;
	gboolean    enable_edit;
} GupGnmFormatGuru;

typedef struct {
	GupGnmConfigGuruClass	parent;
} GupGnmFormatGuruClass;

static E_MAKE_TYPE (gup_gnm_format_guru, "GupGnmFormatGuru", GupGnmFormatGuru,
		    NULL, NULL, GUP_GNM_CONFIG_GURU_TYPE);

static void
cb_legend_pos_toggle (GtkToggleButton *button, GupGnmFormatGuru *fguru)
{
	GupGnmGraph *graph;
	char const *new_pos;
	xmlNode *legend, *pos;

	if (!gtk_toggle_button_get_active (button) || !fguru->enable_edit)
		return;

	graph = fguru->view.graph;
	new_pos = gtk_object_get_data (GTK_OBJECT (button), "position");
	legend = e_xml_get_child_by_name (graph->spec->xmlRootNode, "Legend");
	pos = gup_gnm_attr_get (legend, "Position");

	if (new_pos) {
		if (legend == NULL)
			legend = xmlNewChild (graph->spec->xmlRootNode,
			      graph->spec->xmlRootNode->ns,
			      "Legend", NULL);
		if (pos != NULL)
			xmlNodeSetContent (pos, new_pos);
		else
			xmlNewChild (legend, legend->ns, "Position", new_pos);

		ggd(5, puts(new_pos));
	} else if (pos == NULL) {
		/* remove if necessary */
		xmlUnlinkNode (pos);
		xmlFreeNode (pos);
	}

	gup_gnm_graph_regenerate_plots (graph);
}

static void
fmt_dialog_init_legend_page (GupGnmFormatGuru *fguru)
{
	static struct {
		char const *name;
		char const *pos_str;
	} const pos_buttons[] = {
	    { "legend_north",	"north" },
	    { "legend_south",	"south" },
	    { "legend_east",	"east" },
	    { "legend_west",	"west" },
	    { "legend_none",	"invalid-compass" },
	    { NULL }
	};

	GupGnmGraph *graph = fguru->view.graph;
	int i;
	char const *name;
	xmlNode *node;
	xmlChar *pos = NULL;

	node = gup_gnm_attr_get (
		e_xml_get_child_by_name (graph->spec->xmlRootNode, "Legend"),
		"Position");
	if (node != NULL) 
		pos = xmlNodeGetContent (node);
	if (pos == NULL)
		pos = xmlStrdup ("invalid-compass");

	/* Setup the position buttons */
	for (i = 0; (name = pos_buttons[i].name) != NULL; ++i) {
		GtkWidget *w = glade_xml_get_widget (fguru->glade, name);
		g_return_if_fail (w != NULL);

		if (!strcmp (pos_buttons[i].pos_str, pos))
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w), TRUE);

		gtk_object_set_data (GTK_OBJECT (w),
			"position", (gpointer)pos_buttons[i].pos_str);
		gtk_signal_connect (GTK_OBJECT (w),
			"toggled", cb_legend_pos_toggle, fguru);
	}

	xmlFree (pos);
}

GupGnmConfigGuru *
gup_gnm_format_guru_new (GupGnmGraph *graph)
{
	GtkWidget *w;
	GupGnmFormatGuru *fguru = guppi_type_new (gup_gnm_format_guru_get_type ());
	char const *glade_path = guppi_glade_path ("guppi-gnumeric-format-guru.glade");

	g_return_val_if_fail (glade_path != NULL, NULL);

	fguru->glade = glade_xml_new (glade_path, "format_guru");
	g_return_val_if_fail (fguru->glade != NULL, NULL);

	gup_gnm_view_construct (&fguru->view, graph);

	w = glade_xml_get_widget (fguru->glade, "sample_frame");
	gtk_container_add (GTK_CONTAINER (w), GTK_WIDGET (fguru->view.canvas));

	w = glade_xml_get_widget (fguru->glade, "format_guru");

	fguru->enable_edit = FALSE;
	fmt_dialog_init_legend_page (fguru);
	fguru->enable_edit = TRUE;

	return gup_gnm_config_guru_construct (
		GUP_GNM_CONFIG_GURU (fguru), graph, w);
}
