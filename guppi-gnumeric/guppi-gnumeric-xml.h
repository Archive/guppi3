/* This is -*- C -*- */
/* vim: set sw=8: */

/*
 * guppi-gnumeric-xml.h
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 * Developed by Jody Goldberg <jgoldberg@home.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_GNUMERIC_XML_H
#define _INC_GUPPI_GNUMERIC_XML_H

#include "guppi-gnumeric.h"
#include <glib.h>
#include <gnome-xml/tree.h>
#include <guppi-data.h>
#include <guppi-marker.h>
#include <guppi-seq-scalar.h>
#include <guppi-seq-string.h>

BEGIN_GUPPI_DECLS

typedef gboolean (*GupGnmSeriesFn) (GupGnmGraph *graph,
				    xmlNode *plot, xmlNode *series,
				    gpointer user);
xmlNode	    *gup_gnm_graph_get_plot	   (GupGnmGraph *graph, int plotID);
gboolean     gup_gnm_graph_foreach_series  (GupGnmGraph *graph,
					    GupGnmSeriesFn func, gpointer data);

int	     gup_gnm_plot_get_id	     (xmlNode *plot);

int            gup_gnm_series_get_id	     (xmlNode *series);
xmlNode	      *gup_gnm_series_get_dimension  (xmlNode *series, char const *dim);
GupGnmVector  *gup_gnm_series_dim_get_vector (xmlNode *series, char const *dim,
					      GupGnmGraph const *graph);
GuppiSeqString *gup_gnm_series_dim_get_string (xmlNode *series, char const *dim,
					       GupGnmGraph const *graph);
GuppiSeqScalar *gup_gnm_series_dim_get_scalar (xmlNode *series, char const *dim,
					       GupGnmGraph const *graph);
void		gup_gnm_series_add_dimension  (xmlNode *series, char const *dim,
					       int id);

xmlNode    *gup_gnm_attr_get	    (xmlNode *node, char const *name);
gboolean    gup_gnm_attr_get_bool   (xmlNode *node, char const *name,
				     gboolean default_val);
int	    gup_gnm_attr_get_int    (xmlNode *node, char const *name,
				     int default_val);
double	    gup_gnm_attr_get_double (xmlNode *node, char const *name,
				     double default_val);
guint32	    gup_gnm_attr_get_color  (xmlNode *node, char const *name,
				     guint32 default_val);
GuppiMarker gup_gnm_attr_get_marker (xmlNode *node, char const *name,
				     GuppiMarker default_val);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_GNUMERIC_XML_H */
