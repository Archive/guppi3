/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* vim: set sw=8: */

/*
 * guppi-gnumeric-plot-scatter.c
 *
 * Copyright (C) 2001 Ximian, Inc
 *
 * Developed by Jody Goldberg (jgoldberg@home.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-gnumeric-graph.h"
#include "guppi-gnumeric-xml.h"
#include "guppi-gnumeric-vector.h"

#include <guppi-memory.h>
#include <guppi-marker.h>
#include <gal/util/e-xml-utils.h>

static GupGnmSeriesElements const scatter_spec[] = {
	{ N_("X Values"),	"categories",	FALSE, FALSE },
	{ N_("Y Values"),	"values",	TRUE, FALSE },
	{ N_("Bubble Size"),	"bubbles",	TRUE, FALSE },
	SERIES_ELEMENT_LAST
};

static void
scatter_arrange_data (GupGnmGraph const *graph,
		      xmlNode *plot, xmlNode *data)
{
	static char const *bubbles [] = { "bubbles", NULL };
	xmlNode *desc = e_xml_get_child_by_name (plot, "Type");

	gboolean const with_bubbles =
		gup_gnm_attr_get_bool (desc, "auto_allocate_bubble_size", FALSE);
	gup_gnm_graph_arrange_data (graph, plot, data, bubbles, with_bubbles ? 1 : 0);
}

static GSList *
scatter_plot (GupGnmGraph *graph, xmlNode *plot)
{
	GuppiElementState *scatter_state;
	GuppiSeqScalar *x_data, *y_data, *bubble_data;
	guint32 color;
	GSList *res = NULL;
	xmlNode *series = e_xml_get_child_by_name (plot, "Data");
	GupGnmVector *vec;

	g_return_val_if_fail (series != NULL, NULL);

	for (series = series->xmlChildrenNode ; series ; series = series->next) {
		int x_axis_type = GUPPI_AXIS_SCALAR;
		int y_axis_type = GUPPI_AXIS_SCALAR;

		if (strcmp (series->name, "Series"))
			continue;

		vec = gup_gnm_series_dim_get_vector (series, "values", graph);
		if (vec == NULL)
			continue;
		y_data = gup_gnm_vector_get_scalar (vec);
		if (y_data == NULL)
			continue;
		if (gup_gnm_vector_is_date (vec))
			y_axis_type = GUPPI_AXIS_XL_DATE;

		vec = gup_gnm_series_dim_get_vector (series, "categories", graph);
		if (vec != NULL) {
			x_data = gup_gnm_vector_get_scalar (vec);
			if (x_data != NULL && gup_gnm_vector_is_date (vec))
				x_axis_type = GUPPI_AXIS_XL_DATE;
		} else 
			x_data = NULL;
		if (x_data == NULL)
			x_data = gup_gnm_graph_stock_index (graph,
					guppi_seq_size (GUPPI_SEQ (y_data)));

		color = gup_gnm_graph_get_series_color (graph, series);
		scatter_state = guppi_element_state_new ("scatter",
			"x_data",  	x_data,
			"y_data",  	y_data,
			"x_axis_type",	x_axis_type,
			"y_axis_type",	y_axis_type,
			"color",   	color,
			NULL);

		bubble_data = gup_gnm_series_dim_get_scalar (series, "bubbles", graph);
		if (bubble_data != NULL) {
			guppi_element_state_set (scatter_state,
				"data_size1",	 bubble_data,
				"marker",  GUPPI_MARKER_CIRCLE,
				NULL);
		} else {
			GuppiMarker marker = gup_gnm_graph_get_series_marker (graph, series);
			guppi_element_state_set (scatter_state,
				"size1", 	.75,
				"marker",	marker,
				NULL);
		}
		res = g_slist_append (res, scatter_state);
	}
	return res;
}

GupGnmPlotDescriptor const gup_gnm_scatter_plot_type =
	{ "Scatter", scatter_spec, FALSE, scatter_plot,	scatter_arrange_data, NULL };
