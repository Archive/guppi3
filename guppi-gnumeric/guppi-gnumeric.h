/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* vim: set sw=8: */

/*
 * guppi-gnumeric.h
 *
 * Copyright (C) 2001 Helix Code, Inc.
 *
 * Developed by Jody Goldberg <jgoldberg@home.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_GNUMERIC_H
#define _INC_GUPPI_GNUMERIC_H

#include <guppi-defs.h>

BEGIN_GUPPI_DECLS

typedef struct _GupGnmManager	 GupGnmManager;
typedef struct _GupGnmGraph	 GupGnmGraph;
typedef struct _GupGnmVector	 GupGnmVector;
typedef struct _GupGnmView	 GupGnmView;
typedef struct _GupGnmConfigGuru GupGnmConfigGuru;
typedef struct _GupGnmDataGuru	 GupGnmDataGuru;

/* Some debug utilities */
#ifdef DEBUG_GUPPI_GNUMERIC
extern int debug_guppi_gnumeric;
#define ggd(level, code)	do { if (debug_guppi_gnumeric >= level) { code; } } while (0)
#else
#define ggd(level, code)	
#endif

END_GUPPI_DECLS

#endif /* _INC_GUPPI_GNUMERIC_H */
