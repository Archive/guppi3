/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* vim: set sw=8: */

/*
 * guppi-gnumeric.c
 *
 * Copyright (C) 2000-2001 Ximian Inc
 *
 * Developed by Jody Goldberg <jgoldberg@home.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-gnumeric-manager.h"

#include <glib.h>
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-i18n.h>
#include <guppi-debug.h>
#include <guppi-useful.h>
#include <guppi-plot-init.h>
#include <guppi-data-init.h>
#include <liboaf/liboaf.h>
#include <glade/glade.h>

#ifdef GUPPI_GNUMERIC_SHLIB
#include <bonobo/bonobo-shlib-factory.h>
#else
static BonoboGenericFactory *factory = NULL;
static int manager_count = 0;
#endif

#ifdef DEBUG_GUPPI_GNUMERIC
int debug_guppi_gnumeric = 10;
#else
int debug_guppi_gnumeric = 0;
#endif

#ifndef GUPPI_GNUMERIC_SHLIB
static void
cb_manager_destroy (GtkObject *object, gpointer ignored)
{
	manager_count--;

	if (manager_count > 0)
		return;

#ifndef DEBUG_GUPPI_GNUMERIC
	bonobo_object_unref (BONOBO_OBJECT (factory));
	gtk_main_quit ();
#endif
}
#endif

static BonoboObject *
gup_gnm_manager_factory (BonoboGenericFactory *Factory, void *closure)
{
	GupGnmManager *manager;
	static gboolean needs_init = TRUE;

	if (needs_init) {
		needs_init = FALSE;

		glade_gnome_init ();
		guppi_useful_init_without_guile ();
		guppi_data_init ();
		guppi_plot_init ();
		guppi_plug_in_spec_find_all ();

#ifdef DEBUG_GUPPI_GNUMERIC
		guppi_set_verbosity (GUPPI_VERY_VERBOSE);
#endif
	}

	manager = gup_gnm_manager_new ();

#ifndef GUPPI_GNUMERIC_SHLIB
	manager_count++;
	gtk_signal_connect (GTK_OBJECT (manager), "destroy",
			    GTK_SIGNAL_FUNC (cb_manager_destroy), NULL);
#endif

	return BONOBO_OBJECT (manager);
}

#ifdef GUPPI_GNUMERIC_SHLIB

BONOBO_OAF_SHLIB_FACTORY (
	"OAFIID:GNOME_Guppi_Gnumeric_Factory:58dfe833-ebc6-4893-bc23-58942c572432",
	"GNOME Guppi Gnumeric factory",
	gup_gnm_manager_factory, NULL);

#else

static void
guppi_gnumeric_gup_gnm_manager_factory_init (void)
{
	if (factory != NULL)
		return;

	factory = bonobo_generic_factory_new (
		"OAFIID:GNOME_Guppi_Gnumeric_Factory:58dfe833-ebc6-4893-bc23-58942c572432",
		&gup_gnm_manager_factory, NULL);

	if (factory == NULL)
		g_error ("I could not register a Gnumeric::Graph::Manager factory.");
}

static void
init_server_factory (int argc, char **argv)
{
	CORBA_ORB orb = oaf_init (argc, argv);

	if (bonobo_init (orb, NULL, NULL) == FALSE)
		g_error (_("Bonobo initialization failed."));

	guppi_gnumeric_gup_gnm_manager_factory_init ();
}

int
main (int argc, char* argv[])
{
	bindtextdomain(PACKAGE, GNOMELOCALEDIR);
	textdomain(PACKAGE);

	gtk_type_init ();
	gnomelib_register_popt_table(oaf_popt_options, "oaf");

	gnome_init_with_popt_table ("guppi-gnumeric", VERSION,
		argc, argv, oaf_popt_options, 0, NULL);

	init_server_factory (argc, argv);

	bonobo_main ();

	return 0;
}

#endif /* GUPPI_GNUMERIC_SHLIB */
