/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* vim: set sw=8: */

/*
 * guppi-gnumeric-plot-line.c
 *
 * Copyright (C) 2001 Ximian, Inc
 *
 * Developed by Jody Goldberg (jgoldberg@home.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-gnumeric-graph.h"
#include "guppi-gnumeric-xml.h"
#include "guppi-gnumeric-vector.h"

#include <guppi-rgb.h>
#include <gal/util/e-xml-utils.h>

static GupGnmSeriesElements const line_spec[] = {
	{ N_("Values"), 	 "values",	FALSE, FALSE },
	{ N_("Category Labels"), "categories",	TRUE, TRUE },
	SERIES_ELEMENT_LAST
};

static void
line_arrange_data (GupGnmGraph const *graph,
		   xmlNode *plot, xmlNode *data)
{
	gup_gnm_graph_arrange_data (graph, plot, data, NULL, 0);
}

static GSList *
line_plot (GupGnmGraph *graph, xmlNode *plot)
{
	GuppiElementState *linegraph_state;
	GuppiSeqScalar *x_data, *y_data;
	GSList *res = NULL;
	guint32 color;

#if 0
	xmlNode *desc = e_xml_get_child_by_name (plot, "Type");
	gboolean const is_stacked	=
		gup_gnm_attr_get_bool (desc, "stacked", FALSE);
	gboolean const as_percentage	=
		gup_gnm_attr_get_bool (desc, "as_percentage", FALSE);
#endif

	xmlNode *series = e_xml_get_child_by_name (plot, "Data");

	g_return_val_if_fail (series != NULL, NULL);

	for (series = series->xmlChildrenNode ; series ; series = series->next) {
		if (strcmp (series->name, "Series"))
			continue;

		y_data = gup_gnm_series_dim_get_scalar (series, "values", graph);
		if (y_data == NULL)
			continue;

		x_data = gup_gnm_series_dim_get_scalar (series, "categories", graph);
		if (x_data == NULL)
			x_data = gup_gnm_graph_stock_index (graph,
					guppi_seq_size (GUPPI_SEQ (y_data)));

		color = gup_gnm_graph_get_series_color (graph, series);
		linegraph_state = guppi_element_state_new ("linegraph",
			"x_data", x_data,
			"y_data", y_data,
			"color",  color,
			NULL);

		res = g_slist_append (res, linegraph_state);
	}
	return res;
}

GupGnmPlotDescriptor const gup_gnm_line_plot_type =
	{ "Line",    line_spec,    FALSE, line_plot,	line_arrange_data,    NULL };
