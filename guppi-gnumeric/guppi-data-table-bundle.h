/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-data-table-bundle.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_DATA_TABLE_BUNDLE_H
#define _INC_GUPPI_DATA_TABLE_BUNDLE_H

#include <gnome.h>
#include <guppi-defs.h>
#include <guppi-data-table.h>
#include <guppi-seq-scalar.h>
#include <guppi-seq-string.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiDataTableBundle GuppiDataTableBundle;
typedef struct _GuppiDataTableBundleClass GuppiDataTableBundleClass;

typedef struct _GuppiDataTableBundleColumn GuppiDataTableBundleColumn;

struct _GuppiDataTableBundleColumn {
  GuppiDataTableBundle *bundle;
  gint col_num;
  GuppiSeqScalar *data;
  guint changed_tag;
};

struct _GuppiDataTableBundle {
  GuppiDataTable parent;

  gint R, C;
  GuppiDataTableBundleColumn **col;

  GuppiSeqString *row_labels;
  guint row_labels_changed_tag;

  GuppiSeqString *col_labels;
  guint col_labels_changed_tag;

  gboolean block_signals_hack;
};

struct _GuppiDataTableBundleClass {
  GuppiDataTableClass parent_class;
};

#define GUPPI_TYPE_DATA_TABLE_BUNDLE (guppi_data_table_bundle_get_type ())
#define GUPPI_DATA_TABLE_BUNDLE(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_DATA_TABLE_BUNDLE,GuppiDataTableBundle))
#define GUPPI_DATA_TABLE_BUNDLE0(obj) ((obj) ? (GUPPI_DATA_TABLE_BUNDLE(obj)) : NULL)
#define GUPPI_DATA_TABLE_BUNDLE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_DATA_TABLE_BUNDLE,GuppiDataTableBundleClass))
#define GUPPI_IS_DATA_TABLE_BUNDLE(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_DATA_TABLE_BUNDLE))
#define GUPPI_IS_DATA_TABLE_BUNDLE0(obj) (((obj) == NULL) || (GUPPI_IS_DATA_TABLE_BUNDLE(obj)))
#define GUPPI_IS_DATA_TABLE_BUNDLE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_DATA_TABLE_BUNDLE))

GtkType guppi_data_table_bundle_get_type (void);

GuppiDataTable *guppi_data_table_bundle_new (void);

void guppi_data_table_bundle_set_col_data   (GuppiDataTableBundle *tb, gint col, GuppiSeqScalar *seq);
void guppi_data_table_bundle_set_row_labels (GuppiDataTableBundle *tb, GuppiSeqString *str);
void guppi_data_table_bundle_set_col_labels (GuppiDataTableBundle *tb, GuppiSeqString *str);



END_GUPPI_DECLS;

#endif /* _INC_GUPPI_DATA_TABLE_BUNDLE_H */

/* $Id$ */
