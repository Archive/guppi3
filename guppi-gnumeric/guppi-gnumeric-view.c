/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* vim: set sw=8: */

/*
 * guppi-gnumeric-view.c: simple wrapper to manage views
 *
 * Copyright (C) 2001 Ximian, Inc
 *
 * Developed by Jody Goldberg (jgoldberg@home.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-gnumeric-view.h"
#include "guppi-gnumeric-graph.h"
#include <guppi-memory.h>
#include <guppi-root-group-item.h>

static void
cb_gup_gnm_view_destroy (GtkObject *object, GupGnmView *view)
{
	GList *link;

	if (view->graph == NULL)
		return;

	ggd(0, printf ("Guppi : view destroy %p\n", view));

	link = g_list_find (view->graph->views, view);

	g_return_if_fail (link != NULL);

	view->graph->views = g_list_remove_link (view->graph->views, link);
	view->graph = NULL;
	view->canvas = NULL;
	view->root_item = NULL;
}

void
gup_gnm_view_construct (GupGnmView *view, GupGnmGraph *graph)
{
	GuppiRootGroupView *plot_view = gup_gnm_graph_get_view (graph);
	view->canvas = guppi_root_group_view_make_canvas (plot_view,
		&view->root_item);

	guppi_root_group_item_set_resize_semantics (
		GUPPI_ROOT_GROUP_ITEM (view->root_item),
		ROOT_GROUP_RESIZE_FILL_SPACE);

	view->graph = graph;
	graph->views = g_list_prepend (graph->views, view);

	gtk_signal_connect (GTK_OBJECT (view->canvas),
		"destroy",
		GTK_SIGNAL_FUNC (cb_gup_gnm_view_destroy), view);
}

void
gup_gnm_view_regenerate (GupGnmView *view)
{
	/* Don't regenerate what was not originally there */
	if (view->root_item == NULL)
		return;

	gtk_object_destroy (GTK_OBJECT (view->root_item));

	view->root_item = guppi_root_group_view_make_canvas_item (
		gup_gnm_graph_get_view (view->graph),
		view->canvas);
	guppi_root_group_item_set_resize_semantics (
		GUPPI_ROOT_GROUP_ITEM (view->root_item),
		ROOT_GROUP_RESIZE_FILL_SPACE);
}
