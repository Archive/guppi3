/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* vim: set sw=8: */

/*
 * guppi-gnumeric-plot-pie.c
 *
 * Copyright (C) 2001 Ximian, Inc
 *
 * Developed by Jody Goldberg (jgoldberg@home.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-gnumeric-graph.h"
#include "guppi-gnumeric-xml.h"
#include "guppi-gnumeric-vector.h"
#include "guppi-gnumeric-manager.h"

#include <guppi-memory.h>
#include <guppi-seq-string.h>
#include <gal/util/e-xml-utils.h>

static GupGnmSeriesElements const pie_spec[] = {
	{ N_("Values"), 	 "values",	FALSE, FALSE },
	{ N_("Category Labels"), "categories",	TRUE, TRUE },
	SERIES_ELEMENT_LAST
};

static GSList *
pie_plot (GupGnmGraph *graph, xmlNode *plot)
{
	GuppiColorPalette *palette;
	GuppiElementState *pie_state;

	xmlNode *desc = e_xml_get_child_by_name (plot, "Type");
	double const radians_of_first_pie =
		gup_gnm_attr_get_double (desc, "radians_of_first_pie", 0.);
	double const separation_percent_of_radius =
		gup_gnm_attr_get_double (desc, "separation_percent_of_radius", 0);

	/* Use only the first */
	xmlNode *series = e_xml_get_child_by_name (
		e_xml_get_child_by_name (plot, "Data"), "Series");
	GuppiSeqScalar *values;
	GuppiSeqString *cat;

	if (series == NULL)
		return NULL;

	values = gup_gnm_series_dim_get_scalar (series, "values", graph);
	cat = gup_gnm_series_dim_get_string (series, "categories", graph);
	if (cat == NULL)
		cat = gup_gnm_graph_stock_labels (graph,
			guppi_seq_size (GUPPI_SEQ (values)));

	if (values == NULL || cat == NULL)
		return NULL;

	palette = guppi_color_palette_new ();
	guppi_color_palette_set_alien_stock (palette);

	/* Build our state objects */
	pie_state = guppi_element_state_new ("pie",
		"data",			values,
		"label_data",		cat,
		"slice_colors",		palette,
		"show_percentage",	FALSE,
		"radius_maximize",	TRUE,
		"radius_lock",		FALSE,

		/* Ideally this would be 0 for an
		 * un-anti-aliased un-scaled hair width
		 * line */
		"edge_width",		0.6,

		"base_offset",		separation_percent_of_radius,
		"base_angle",		radians_of_first_pie - M_PI / 2.,
		"use_stock_colors",	0,
		NULL);
	return g_slist_append (NULL, pie_state);
}

static void
pie_arrange_data (GupGnmGraph const *graph,
		  xmlNode *plot, xmlNode *data)
{
	int i = 0;
	GupGnmManager *manager = graph->manager;
	xmlNode *series = xmlNewChild (data, data->ns, "Series", NULL);
	e_xml_set_integer_prop_by_name (series, "index", 0);

	if (manager->arrangement_len > 1) {
		gup_gnm_series_add_dimension (series, "categories", manager->data_ids[0]);
		i = 1;
	}

	gup_gnm_series_add_dimension (series, "values",
		manager->data_ids[i]);
	if (manager->header_ids[i] >= 0)
		gup_gnm_series_add_dimension (series, "labels",
			manager->header_ids[i]);
}

GupGnmPlotDescriptor const gup_gnm_pie_plot_type =
	{ "Pie",     pie_spec,     TRUE,  pie_plot,	pie_arrange_data,     NULL };
