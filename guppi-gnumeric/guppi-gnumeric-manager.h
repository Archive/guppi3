/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* vim: set sw=8: */

/*
 * guppi-gnumeric-manager.h
 *
 * Copyright (C) 2000-2001 Helix Code, Inc.
 *
 * Developed by Jody Goldberg <jgoldberg@home.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUP_GNM_MANAGER_H
#define _INC_GUP_GNM_MANAGER_H

#include "guppi-gnumeric-graph.h"

#include <gnome-xml/tree.h>
#include <bonobo.h>

BEGIN_GUPPI_DECLS

struct _GupGnmManager {
	BonoboEmbeddable parent;

	GPtrArray *vectors;
	int arrangement_len;
	int *data_ids, *header_ids;

	GupGnmGraph graph;
	
	/* TODO : we need a 'viewID' to handle multiple gurus at one time.
	 * for now just assume there will only be one editor at a time.
	 */
	GupGnmGraph gurus;
};

#define GUP_GNM_MANAGER_TYPE        (gup_gnm_manager_get_type ())
#define GUP_GNM_MANAGER(o)          (GTK_CHECK_CAST ((o), GUP_GNM_MANAGER_TYPE, GupGnmManager))
#define IS_GUP_GNM_MANAGER(o)       (GTK_CHECK_TYPE ((o), GUP_GNM_MANAGER_TYPE))

GtkType gup_gnm_manager_get_type (void);
GupGnmManager  *gup_gnm_manager_new (void);

void	gup_gnm_manager_set_plottype	  (GupGnmManager *manager,
					   xmlNode *plottype);

GupGnmVector *gup_gnm_manager_get_vector (GupGnmManager const *manager,
					  unsigned id);

END_GUPPI_DECLS

#endif /* _INC_GUP_GNM_MANAGER_H */
