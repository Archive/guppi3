/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* vim: set sw=8: */

/*
 * guppi-gnumeric-type-selector.c
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * Developed by Jody Goldberg <jgoldberg@home.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-gnumeric-type-selector.h"
#include "guppi-gnumeric-graph.h"
#include "guppi-gnumeric-config-guru.h"

#include <guppi-paths.h>
#include <guppi-memory.h>
#include <guppi-root-group-item.h>

#include <gal/util/e-xml-utils.h>
#include <gal/util/e-util.h>
#include <gnome-xml/xmlmemory.h>
#include <gnome-xml/parser.h>
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-i18n.h>
#include <libgnomeui/gnome-canvas-rect-ellipse.h>
#include <libgnomeui/gnome-pixmap.h>
#include <gdk-pixbuf/gnome-canvas-pixbuf.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gdk/gdkkeysyms.h>
#include <glib.h>

#define MINOR_PIXMAP_WIDTH	64
#define MINOR_PIXMAP_HEIGHT	60
#define BORDER	5

typedef struct
{
	GupGnmConfigGuru   parent;
	GtkWidget 	  *notebook;
	GtkWidget 	  *canvas;
	GtkWidget 	  *sample_button;
	GtkLabel 	  *label;
	GtkCList 	  *list;
	GnomeCanvasItem *selector;

	GuppiCanvasItem *sample_plot;
	GupGnmGraph *graph;

	GnomeCanvasGroup *plot_group;

	xmlNode const *plots, *current_major, *current_minor;
	GnomeCanvasGroup const *current_major_item;
	GnomeCanvasItem const  *current_minor_item;
} GupGnmTypeSelector;

typedef struct {
	GupGnmConfigGuruClass	parent;
} GupGnmTypeSelectorClass;

#define MINOR_KEY		"minor_chart_type"
#define MAJOR_KEY		"major_chart_type"
#define FIRST_MINOR_TYPE	"first_minor_type"

static GnomePixmap *
get_pixmap (xmlNode *node)
{
	static GHashTable *cache = NULL;
	char *sample_image_path, *sample_image_file;
	xmlNode *sample_image_file_node;
	GnomePixmap *pixmap;
	GtkWidget *tmp;

	g_return_val_if_fail (node != NULL, NULL);

	if (cache != NULL) {
		pixmap = g_hash_table_lookup (cache, node);
		if (pixmap != NULL)
			return pixmap;
	} else
		cache = g_hash_table_new (g_direct_hash, g_direct_equal);

	sample_image_file_node = e_xml_get_child_by_name (node, "sample_image_file");

	g_return_val_if_fail (sample_image_file_node != NULL, NULL);

	sample_image_file = xmlNodeGetContent (sample_image_file_node);
	g_return_val_if_fail (sample_image_file != NULL, NULL);

	sample_image_path = guppi_find_pixmap (sample_image_file);
	xmlFree (sample_image_file);

	g_return_val_if_fail (sample_image_path != NULL, NULL);

	tmp = gnome_pixmap_new_from_file (sample_image_path);
	pixmap = GNOME_PIXMAP (tmp);
	g_hash_table_insert (cache, node, pixmap);
	guppi_free (sample_image_path);

	return pixmap;
}

static GdkPixbuf *
get_pixbuf (xmlNode *node)
{
	static GHashTable *cache = NULL;
	char *sample_image_path, *sample_image_file;
	xmlNode *sample_image_file_node;
	GdkPixbuf *pixbuf;

	g_return_val_if_fail (node != NULL, NULL);

	if (cache != NULL) {
		pixbuf = g_hash_table_lookup (cache, node);
		if (pixbuf != NULL)
			return pixbuf;
	} else
		cache = g_hash_table_new (g_direct_hash, g_direct_equal);

	sample_image_file_node = e_xml_get_child_by_name (node, "sample_image_file");

	g_return_val_if_fail (sample_image_file_node != NULL, NULL);

	sample_image_file = xmlNodeGetContent (sample_image_file_node);
	g_return_val_if_fail (sample_image_file != NULL, NULL);

	sample_image_path = guppi_find_pixmap (sample_image_file);
	xmlFree (sample_image_file);

	g_return_val_if_fail (sample_image_path != NULL, NULL);

	pixbuf = gdk_pixbuf_new_from_file (sample_image_path);
	g_hash_table_insert (cache, node, pixbuf);
	guppi_free (sample_image_path);

	return pixbuf;
}

static void
get_pos (int col, int row, double *x, double *y)
{
	*x = (col-1) * (MINOR_PIXMAP_WIDTH + BORDER) + BORDER;
	*y = (row-1) * (MINOR_PIXMAP_HEIGHT + BORDER) + BORDER;
}

/*
 * gup_gnm_type_select_minor :
 *
 * @typesel :
 * @item : A CanvasItem in the selector.
 *
 * Moves the typesel::selector overlay above the @item.
 * Assumes that the item is visible
 */
static void
gup_gnm_type_select_minor (GupGnmTypeSelector *typesel,
			   GnomeCanvasItem *item)
{
	xmlNode *minor, *tmp;
	double x1, y1, x2, y2;
	char *description;

	if (typesel->current_minor_item == item)
		return;

	minor = gtk_object_get_data (GTK_OBJECT (item), MINOR_KEY);

	g_return_if_fail(minor != NULL);

	/* clear the current plot */
	if (typesel->sample_plot != NULL) {
		gtk_object_destroy (GTK_OBJECT (typesel->sample_plot));
		typesel->sample_plot = NULL;
	}

	tmp = e_xml_get_child_by_name_by_lang_list (
	       minor, "description", NULL);
	description = (tmp != NULL) ? xmlNodeGetContent (tmp) : NULL;
	typesel->current_minor = minor;
	typesel->current_minor_item = item;
	gnome_canvas_item_get_bounds (item, &x1, &y1, &x2, &y2);
	gnome_canvas_item_set (GNOME_CANVAS_ITEM (typesel->selector),
		"x1", x1-1., "y1", y1-1.,
		"x2", x2+1., "y2", y2+1.,
		NULL);
	gtk_label_set_text (typesel->label, description);
	gtk_widget_set_sensitive (typesel->sample_button, TRUE);

	if (description != NULL)
		xmlFree (description);

	gup_gnm_graph_set_plottype (typesel->graph, minor);
}

static gboolean
gup_gnm_type_select_minor_x_y (GupGnmTypeSelector *typesel,
			       double x, double y)
{
	GnomeCanvasItem *item = gnome_canvas_get_item_at (
		GNOME_CANVAS (typesel->canvas), x, y);

	if (item != NULL && item != typesel->selector) {
		gup_gnm_type_select_minor (typesel, item);
		return TRUE;
	}

	return FALSE;
}

static void
minor_chart_type_get_pos (xmlNode const *node, int *col, int *row)
{
	*col = *row = -1;
	node = e_xml_get_child_by_name (node, "position");

	g_return_if_fail (node != NULL);

	*col = e_xml_get_integer_prop_by_name (node, "col");
	*row = e_xml_get_integer_prop_by_name (node, "row");
}

static gboolean
cb_key_press_event (GtkWidget *wrapper, GdkEventKey *event,
		    GupGnmTypeSelector *typesel)
{
	GtkCornerType corner;
	int row, col;
	double x, y;
	xmlNode const *minor = gtk_object_get_data (
		GTK_OBJECT (typesel->current_minor_item), MINOR_KEY);

	g_return_val_if_fail (minor != NULL, FALSE);

	minor_chart_type_get_pos (minor, &col, &row);

	switch (event->keyval){
	case GDK_KP_Left:	case GDK_Left:
		corner = GTK_CORNER_BOTTOM_RIGHT;
		--col;
		break;

	case GDK_KP_Up:	case GDK_Up:
		corner = GTK_CORNER_BOTTOM_RIGHT;
		--row;
		break;

	case GDK_KP_Right:	case GDK_Right:
		corner = GTK_CORNER_TOP_LEFT;
		++col;
		break;

	case GDK_KP_Down:	case GDK_Down:
		corner = GTK_CORNER_TOP_LEFT;
		++row;
		break;

	default:
		return FALSE;
	}

	get_pos (col, row, &x, &y);
	gup_gnm_type_select_minor_x_y (typesel, x, y);

	return TRUE;
}

static gint
cb_button_press_event (GtkWidget *widget, GdkEventButton *event,
		       GupGnmTypeSelector *typesel)
{
	if (event->button == 1) {
		GnomeCanvas *c = GNOME_CANVAS (widget);
		double x, y;

		gnome_canvas_window_to_world (c, event->x, event->y, &x, &y);

		gup_gnm_type_select_minor_x_y (typesel, x, y);
	}

	return FALSE;
}

static void
cb_row_select (GtkWidget *widget, gint row, gint column,
	       GdkEventButton *event, GupGnmTypeSelector *typesel)
{
	GnomeCanvasItem *item;
	GnomeCanvasGroup *group = gtk_clist_get_row_data (typesel->list, row);

	if (typesel->current_major_item != NULL)
		gnome_canvas_item_hide (GNOME_CANVAS_ITEM (typesel->current_major_item));

	gnome_canvas_item_show (GNOME_CANVAS_ITEM (group));
	typesel->current_major_item = group;

	gnome_canvas_item_hide (GNOME_CANVAS_ITEM (typesel->selector));
	item = gtk_object_get_data (GTK_OBJECT (group), FIRST_MINOR_TYPE);
	if (item != NULL)
		gup_gnm_type_select_minor (typesel, item);
	gnome_canvas_item_show (GNOME_CANVAS_ITEM (typesel->selector));
}

static void
cb_sample_pressed (GtkWidget *button, GupGnmTypeSelector *typesel)
{
	if (typesel->current_major_item == NULL)
		return;

	if (typesel->sample_plot == NULL) {
		GuppiRootGroupView *plot = gup_gnm_graph_get_view (typesel->graph);

		g_return_if_fail (plot != NULL);

		typesel->sample_plot = guppi_element_view_make_canvas_item (
			GUPPI_ELEMENT_VIEW (plot),
			GNOME_CANVAS (typesel->canvas),
			typesel->plot_group);
#if 0
		guppi_root_group_item_set_resize_semantics (
			GUPPI_ROOT_GROUP_ITEM (typesel->sample_plot),
			ROOT_GROUP_RESIZE_FILL_SPACE);
#endif
	}

	gtk_layout_freeze (GTK_LAYOUT (typesel->canvas));
	gnome_canvas_item_hide (GNOME_CANVAS_ITEM (typesel->current_major_item));
	gnome_canvas_item_hide (GNOME_CANVAS_ITEM (typesel->selector));
	gnome_canvas_item_show (GNOME_CANVAS_ITEM (typesel->plot_group));
	guppi_root_group_item_best_fit (GUPPI_ROOT_GROUP_ITEM(typesel->sample_plot));
	gtk_layout_thaw (GTK_LAYOUT (typesel->canvas));
}

static void
cb_sample_released (GtkWidget *button, GupGnmTypeSelector *typesel)
{
	if (typesel->current_major_item == NULL)
		return;

	gtk_layout_freeze (GTK_LAYOUT (typesel->canvas));
	gnome_canvas_item_hide (GNOME_CANVAS_ITEM (typesel->plot_group));
	gnome_canvas_item_show (GNOME_CANVAS_ITEM (typesel->current_major_item));
	gnome_canvas_item_show (GNOME_CANVAS_ITEM (typesel->selector));
	gnome_canvas_set_scroll_region (GNOME_CANVAS (typesel->canvas), 0, 0,
		MINOR_PIXMAP_WIDTH*3 + BORDER*5,
		MINOR_PIXMAP_HEIGHT*3 + BORDER*5);
	gtk_layout_thaw (GTK_LAYOUT (typesel->canvas));
}

typedef struct
{
	GupGnmTypeSelector *typesel;
	GnomeCanvasGroup	*group;
	GnomeCanvasItem		*current_item;
	xmlNode 		*current_minor;
	int col, row;
} minor_list_closure;

static void
minor_list_init (xmlNode *minor, minor_list_closure *closure)
{
	double x1, y1, w, h;
	GnomeCanvasItem *item;
	int col, row;
	GdkPixbuf *image = get_pixbuf (minor);

	g_return_if_fail (image != NULL);

	minor_chart_type_get_pos (minor, &col, &row);

	get_pos (col, row, &x1, &y1);
	w = gdk_pixbuf_get_width (image);
	if (w > MINOR_PIXMAP_WIDTH)
		w = MINOR_PIXMAP_WIDTH;
	h = gdk_pixbuf_get_height (image);
	if (h > MINOR_PIXMAP_HEIGHT)
		h = MINOR_PIXMAP_HEIGHT;

	item = gnome_canvas_item_new (closure->group,
		gnome_canvas_pixbuf_get_type (),
		"x", x1, "y", y1,
		"width", w, "height", h,
		"pixbuf", image,
		NULL);
	gtk_object_set_data (GTK_OBJECT (item), MINOR_KEY, (gpointer)minor);

	if (closure->current_minor == NULL ||
	    closure->row > row ||
	    (closure->row == row && closure->col > col)) {
		closure->current_minor = minor;
		closure->current_item = item;
		closure->col = col;
		closure->row = row;
	}
}

static void
major_list_init (GupGnmTypeSelector *typesel, xmlNode *major)
{
	static gchar *dummies[1] = {""};
	xmlChar			*name;
	xmlNode			*node;
	GnomeCanvasGroup	*group;
	minor_list_closure	 closure;
	int row;
	GnomePixmap *pixmap = get_pixmap (major);

	g_return_if_fail (pixmap != NULL);

	row = gtk_clist_append (typesel->list, dummies);
	node = e_xml_get_child_by_name_by_lang_list (
	       major, "name", NULL);

	/* be really anal when parsing a user editable file */
	g_return_if_fail (node != NULL);
	name = xmlNodeGetContent (node);
	g_return_if_fail (name != NULL);

	gtk_clist_set_pixtext (typesel->list, row, 0, name, 1,
		pixmap->pixmap, pixmap->mask);
	xmlFree (name);

	/* Define a canvas group for all the minor types */
	group = GNOME_CANVAS_GROUP (gnome_canvas_item_new (
		gnome_canvas_root (GNOME_CANVAS (typesel->canvas)),
		gnome_canvas_group_get_type (),
		"x", 0.0,
		"y", 0.0,
		NULL));

	gnome_canvas_item_hide (GNOME_CANVAS_ITEM (group));
	gtk_clist_set_row_data (typesel->list, row, group);
	gtk_object_set_data (GTK_OBJECT (group), MAJOR_KEY, (gpointer)major);

	closure.typesel = typesel;
	closure.group = group;
	closure.current_minor = NULL;

	/* Init the list and the canvas group for each major type */
	for (node = major->xmlChildrenNode; node != NULL; node = node->next)
		if (!strcmp (node->name, "Minor"))
			minor_list_init (node, &closure);

	gtk_object_set_data (GTK_OBJECT (group), FIRST_MINOR_TYPE,
		closure.current_item);
}


static void
cb_canvas_realized (GtkWidget *widget, gpointer data)
{
	/*gdk_window_set_back_pixmap (GTK_LAYOUT (widget)->bin_window, NULL, FALSE); */
}

static E_MAKE_TYPE (gup_gnm_type_selector, "GupGnmTypeSelector", GupGnmTypeSelector,
		    NULL, NULL, GUP_GNM_CONFIG_GURU_TYPE);

GupGnmConfigGuru *
gup_gnm_type_selector_new (GupGnmGraph *graph)
{
	static xmlDoc *doc;
	xmlNode *major;

	GupGnmTypeSelector *typesel;
	GtkWidget *tmp, *vbox, *hbox;

	typesel = guppi_type_new (gup_gnm_type_selector_get_type ());
	typesel->current_major_item = NULL;
	typesel->current_minor_item = NULL;
	typesel->current_minor = NULL;
	typesel->graph = graph;
	typesel->sample_plot = NULL;

	hbox = gtk_hbox_new (FALSE, 5);

	/* List of major types */
	tmp = gtk_clist_new (1);
	gtk_box_pack_start (GTK_BOX (hbox), tmp, TRUE, TRUE, 0);
	typesel->list = GTK_CLIST (tmp);
	gtk_signal_connect (GTK_OBJECT (typesel->list), "select_row",
		GTK_SIGNAL_FUNC (cb_row_select), typesel);

	/* Setup an aa canvas to display the sample image & the sample plot. */
	typesel->canvas = gnome_canvas_new_aa ();
	gtk_signal_connect (GTK_OBJECT (typesel->canvas), "realize",
			    cb_canvas_realized, typesel);

	typesel->plot_group = GNOME_CANVAS_GROUP (gnome_canvas_item_new (
		gnome_canvas_root (GNOME_CANVAS (typesel->canvas)),
		gnome_canvas_group_get_type (),
		"x", 0.0,
		"y", 0.0,
		NULL));

	/*guppi_plot_canvas_set_insensitive (GUPPI_PLOT_CANVAS (typesel->canvas)); */
	gtk_widget_set_usize (typesel->canvas,
		MINOR_PIXMAP_WIDTH*3 + BORDER*5,
		MINOR_PIXMAP_HEIGHT*3 + BORDER*5);
	gnome_canvas_set_scroll_region (GNOME_CANVAS (typesel->canvas), 0, 0,
		MINOR_PIXMAP_WIDTH*3 + BORDER*5,
		MINOR_PIXMAP_HEIGHT*3 + BORDER*5);

	gtk_signal_connect_after (GTK_OBJECT (typesel->canvas), "key_press_event",
		GTK_SIGNAL_FUNC (cb_key_press_event), typesel);
	gtk_signal_connect (GTK_OBJECT (typesel->canvas), "button_press_event",
		GTK_SIGNAL_FUNC (cb_button_press_event), typesel);

	tmp = gtk_frame_new (NULL);
	gtk_frame_set_shadow_type (GTK_FRAME (tmp), GTK_SHADOW_IN);
	gtk_container_add (GTK_CONTAINER (tmp), typesel->canvas);
	gtk_box_pack_start (GTK_BOX (hbox), tmp, FALSE, TRUE, 0);

	if (doc == NULL) {
		char *plots_path = guppi_find_script ("guppi-gnumeric-plots.xml");

		g_return_val_if_fail (plots_path != NULL, NULL);

		doc = xmlParseFile (plots_path);
		guppi_free (plots_path);

		g_return_val_if_fail (doc != NULL, NULL);
	}

	g_return_val_if_fail (doc != NULL, NULL);

	/* Init the list and the canvas group for each major type */
	major = e_xml_get_child_by_name (doc->xmlRootNode, "MajorMinor");
	for (major = major->xmlChildrenNode; major != NULL; major = major->next)
		if (!strcmp (major->name, "Major"))
			major_list_init (typesel, major);

	/* The alpha blended selection box */
	typesel->selector = gnome_canvas_item_new (
		gnome_canvas_root (GNOME_CANVAS (typesel->canvas)),
		gnome_canvas_rect_get_type (),
		"fill_color_rgba",	0xe090f840,
		"outline_color_rgba",	0x000000ff,	/* black */
		"width_pixels", 1,
		NULL);

	/* Setup the description label */
	typesel->label = GTK_LABEL (gtk_label_new (""));
	gtk_label_set_justify (typesel->label, GTK_JUSTIFY_LEFT);
	gtk_label_set_line_wrap (typesel->label, TRUE);
	gtk_misc_set_alignment (GTK_MISC (typesel->label), 0., .2);
	gtk_misc_set_padding (GTK_MISC (typesel->label), 5, 5);

	/* ICK ! How can I set the number of lines without looking at fonts */
	gtk_widget_set_usize (GTK_WIDGET (typesel->label), 350, 65);

	vbox = gtk_vbox_new (FALSE, 5);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, 0);

	hbox = gtk_hbox_new (FALSE, 5);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, 0);

	/* Set up sample button */
	typesel->sample_button = gtk_button_new_with_label (_("Show\nSample"));
	gtk_widget_set_sensitive (typesel->sample_button, FALSE);
	gtk_signal_connect (GTK_OBJECT (typesel->sample_button), "pressed",
		GTK_SIGNAL_FUNC (cb_sample_pressed), typesel);
	gtk_signal_connect (GTK_OBJECT (typesel->sample_button), "released",
		GTK_SIGNAL_FUNC (cb_sample_released), typesel);

	tmp = gtk_frame_new (_("Description"));
	gtk_container_add (GTK_CONTAINER (tmp), GTK_WIDGET (typesel->label));
	gtk_box_pack_start (GTK_BOX (hbox), tmp, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), typesel->sample_button, FALSE, TRUE, 0);

	/* This selects row 0, make sure to do it AFTER all widgets set up */
	gtk_clist_set_selection_mode (typesel->list, GTK_SELECTION_BROWSE);

	typesel->notebook = gtk_notebook_new ();
	gtk_notebook_append_page (GTK_NOTEBOOK (typesel->notebook),
		vbox, gtk_label_new (_("Basic Types")));

	return gup_gnm_config_guru_construct (
		GUP_GNM_CONFIG_GURU (typesel), graph, typesel->notebook);
}
