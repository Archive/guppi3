/* This is -*- C -*- */
/* vim: set sw=8: */

/*
 * guppi-gnumeric-vector.h
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * Developed by Jody Goldberg <jgoldberg@home.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_GNUMERIC_VECTOR_H
#define _INC_GUPPI_GNUMERIC_VECTOR_H

#include "guppi-gnumeric.h"
#include "GNOME_Gnumeric_Graph.h"
#include <guppi-seq-string.h>
#include <guppi-seq-scalar.h>

BEGIN_GUPPI_DECLS

#define GUP_GNM_VECTOR_TYPE	(gup_gnm_vector_get_type ())
#define GUP_GNM_VECTOR(o)	(GTK_CHECK_CAST ((o), GUP_GNM_VECTOR_TYPE, GupGnmVector))
#define IS_GUP_GNM_VECTOR(o)	(GTK_CHECK_TYPE ((o), GUP_GNM_VECTOR_TYPE))

GtkType	gup_gnm_vector_get_type (void);
GupGnmVector *gup_gnm_vector_new (GNOME_Gnumeric_VectorSelection sub,
				  GNOME_Gnumeric_VectorType type,
				  GNOME_Gnumeric_VectorID id);

GuppiSeqScalar *gup_gnm_vector_get_scalar   (GupGnmVector *vec);
GuppiSeqString *gup_gnm_vector_get_string   (GupGnmVector *vec);
CORBA_Object    gup_gnm_vector_servant_get  (GupGnmVector *vec);
int	        gup_gnm_vector_id_get	    (GupGnmVector const *vec);
gboolean	gup_gnm_vector_is_date	    (GupGnmVector const *vec);
void		gup_gnm_vector_mark_as_name (GupGnmVector *vec,
					     GupGnmGraph *graph, unsigned i);
void		gup_gnm_vector_clear_names  (GupGnmVector *vec,
					     GupGnmGraph *graph, unsigned i);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_GNUMERIC_VECTOR_H */
