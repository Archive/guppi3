_name2cls = {}
_supported = map(lambda x: eval(x), filter(lambda x: x[:5] == 'Guppi', dir()))
_m = None
for _m in _supported:
    _name2cls[_m.__name__] = _m
del _m
_guppi.register_classes(_name2cls)
def _obj2inst(obj):
        objname = _guppi.type_name(obj)
        if _guppi.classes.has_key(objname):
                return _guppi.classes[objname](_obj=obj)
        else: return obj
