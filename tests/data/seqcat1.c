/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * int1.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <math.h>

#include "guppi-test-framework.h"
#include "check-missing.h"

#include <guppi-useful.h>
#include <guppi-data-init.h>

#include <guppi-seq-categorical.h>

static const gchar *catnames[] = { "One", "Two", "Three", "Four", "Five", NULL };

static GuppiCategory *
cat (void)
{
  static GuppiCategory *our_cat = NULL;
  gint i;

  if (our_cat == NULL) {
    our_cat = GUPPI_CATEGORY (guppi_data_new ("GuppiCategoryCore"));
    for (i = 0; catnames[i] != NULL; ++i) {
      guppi_category_add_by_name (our_cat, catnames[i]);  
    }
  }

  return our_cat;
}

static GuppiSeqCategorical *
build (gint N)
{
  GuppiSeqCategorical *sc = GUPPI_SEQ_CATEGORICAL (guppi_type_new (guppi_seq_categorical_get_type ()));
  gint i, j;

  guppi_seq_categorical_set_category (sc, cat ());

  for (i = 0, j = 0; i < N; ++i) {
    guppi_seq_categorical_append (sc, catnames[j]);
    ++j;
    if (catnames[j] == NULL)
      j = 0;
  }

  return sc;
}

static gboolean
plugins (gpointer foo)
{
  guppi_plug_in_path_set ("../../plug-ins/data_impl/seq_boolean");
  guppi_plug_in_path_append ("../../plug-ins/data_impl/seq_integer");
  guppi_plug_in_path_append ("../../plug-ins/data_impl/category");
  guppi_plug_in_spec_find_all ();
  return guppi_plug_in_count () > 0;
}

static gboolean
crdest (gpointer foo)
{
  GuppiSeqCategorical *sc;

  sc = build (0);
  if (sc == NULL)
    return FALSE;

  guppi_unref (sc);

  return TRUE;
}

static gboolean
core (gpointer foo)
{
  GuppiSeqCategorical *sc = build (100);
  gint i0, i1;
  
  guppi_test_subtest ("bounds");
  guppi_seq_bounds (GUPPI_SEQ (sc), &i0, &i1);
  if (i0 != 0 || i1 != 99)
    return FALSE;

  guppi_test_subtest ("unref");
  guppi_unref (sc);

  return TRUE;
}

static gboolean
missing (gpointer foo)
{
  GuppiSeqCategorical *sc = build (200);
  GuppiSeq *seq = GUPPI_SEQ (sc);

  guppi_test_subtest ("initial missing count");
  if (!check_missing (seq, 0))
    return FALSE;

  guppi_test_subtest ("set_missing");
  guppi_seq_set_missing (seq, 4);
  guppi_seq_set_missing (seq, 48);
  guppi_seq_set_missing (seq, 123);
  if (!check_missing (seq, 3, 4, 48, 123))
    return FALSE;

  guppi_test_subtest ("insert_missing");
  guppi_seq_insert_missing (seq, 100);
  if (!check_missing (seq, 4, 4, 48, 100, 124))
    return FALSE;

  guppi_test_subtest ("delete");
  guppi_seq_delete (seq, 77);
  guppi_seq_delete (seq, 99);
  if (!check_missing (seq, 3, 4, 48, 122))
    return FALSE;

  guppi_test_subtest ("insert");
  guppi_seq_categorical_insert (sc, 33, catnames[0]);
  if (!check_missing (seq, 3, 4, 49, 123))
    return FALSE;

  guppi_test_subtest ("set");
  guppi_seq_categorical_set (sc, 49, catnames[1]);
  if (!check_missing (seq, 2, 4, 123))
    return FALSE;

  guppi_test_subtest ("grow");
  guppi_seq_grow_to_include_range (seq, -100, 300);
  if (!check_missing (seq, 2, 4, 123))
    return FALSE;

  guppi_test_subtest ("unref");
  guppi_unref (sc);

  return TRUE;
}

int
main (int argc, char *argv[])
{
  guppi_test_init (&argc, &argv);

  guppi_useful_init_without_guile ();
  guppi_data_init ();

  guppi_test_do ("Locate and load relevant plug-ins",         plugins, NULL);
  guppi_test_do ("Create and destroy a GuppiSeqCategorical",  crdest, NULL);
  guppi_test_do ("Test core functions",                       core, NULL);
  guppi_test_do ("Missing values handling",                   missing, NULL);

  guppi_test_quit ();
  return 0;
}
