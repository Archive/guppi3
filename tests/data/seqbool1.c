/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * bool1.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>

#include "guppi-test-framework.h"
#include "check-missing.h"
#include "check-xml.h"

#include <guppi-useful.h>
#include <guppi-data-init.h>

#include <guppi-seq-boolean-core.h>

static GuppiSeqBoolean *
build (guint test_mask, gint count)
{
  gint i;

  GuppiSeqBoolean *sb = GUPPI_SEQ_BOOLEAN (guppi_seq_boolean_core_new ());
  if (sb == NULL)
    return NULL;

  for (i = 0; i < count; ++i) {
    guint x = test_mask;
    while (x) {
      guppi_seq_boolean_append (sb, x&1);
      x = x >> 1;
    }
  }

  return sb;
}

static gboolean
plugins (gpointer foo)
{
  guppi_plug_in_path_set ("../../plug-ins/data_impl/seq_boolean");
  guppi_plug_in_spec_find_all ();
  return guppi_plug_in_count () > 0;
}

static gboolean
construct (gpointer foo)
{
  GuppiData *d;

  d = guppi_data_new ("GuppiSeqBooleanCore");
  if (d == NULL)
    return FALSE;

  guppi_unref (d);

  return TRUE;
}

static gboolean
missing (gpointer foo)
{
  GuppiSeqBoolean *sb = build (0x1deadbee, 77);
  GuppiSeq *seq = GUPPI_SEQ (sb);

  if (!check_missing (seq, 0))
    return FALSE;

  guppi_test_subtest ("set_missing");
  guppi_seq_set_missing (seq, 4);
  guppi_seq_set_missing (seq, 48);
  guppi_seq_set_missing (seq, 123);
  if (!check_missing (seq, 3, 4, 48, 123))
    return FALSE;

  guppi_test_subtest ("insert_missing");
  guppi_seq_insert_missing (seq, 100);
  if (!check_missing (seq, 4, 4, 48, 100, 124))
    return FALSE;

  guppi_test_subtest ("delete");
  guppi_seq_delete (seq, 77);
  guppi_seq_delete (seq, 99);
  if (!check_missing (seq, 3, 4, 48, 122))
    return FALSE;

  guppi_test_subtest ("insert");
  guppi_seq_boolean_insert (sb, 33, TRUE);
  if (!check_missing (seq, 3, 4, 49, 123))
    return FALSE;

  guppi_test_subtest ("set");
  guppi_seq_boolean_set (sb, 49, TRUE);
  if (!check_missing (seq, 2, 4, 123))
    return FALSE;

  guppi_unref (sb);

  return TRUE;
}

static gboolean
grow (gpointer foo)
{
  GuppiSeqBoolean *sb = GUPPI_SEQ_BOOLEAN (guppi_data_new ("GuppiSeqBooleanCore"));
  GuppiSeq *seq = GUPPI_SEQ (sb);
  gint i0, i1;

  guppi_test_subtest ("initial grow");
  guppi_seq_grow_to_include_range (seq, -5, -3);
  guppi_seq_bounds (seq, &i0, &i1);
  if (!(i0 == -5 && i1 == -3))
    return FALSE;

  guppi_test_subtest ("next grow");
  guppi_seq_grow_to_include_range (seq, -4, 10);
  guppi_seq_bounds (seq, &i0, &i1);
  if (!(i0 == -5 && i1 == 10))
    return FALSE;

  guppi_test_subtest ("disjoint grow");
  guppi_seq_grow_to_include_range (seq, -20, -10);
  guppi_seq_bounds (seq, &i0, &i1);
  if (!(i0 == -20 && i1 == 10))
    return FALSE;

  guppi_test_subtest ("inside grow");
  guppi_seq_grow_to_include_range (seq, -5, 5);
  guppi_seq_bounds (seq, &i0, &i1);
  if (!(i0 == -20 && i1 == 10))
    return FALSE;

  guppi_test_subtest ("unref");
  guppi_unref (sb);

  return TRUE;
}

static gboolean
check_xml (gpointer unused)
{
  GuppiSeqBoolean *seq = build (0xdeadbeef, 200);

  if (! check_data_xml (GUPPI_DATA (seq)))
    return FALSE;
  
  guppi_unref (seq);

  return TRUE;
}

int
main (int argc, char *argv[])
{
  guppi_test_init (&argc, &argv);

  guppi_useful_init_without_guile ();
  guppi_data_init ();

  guppi_test_do ("Locate and load boolean plug-ins",         plugins, NULL);
  guppi_test_do ("Create and destroy a GuppiSeqBooleanCore", construct, NULL);
  guppi_test_do ("Missing values handling",                  missing, NULL);
  guppi_test_do ("Grow-to-include operations",               grow, NULL);
  guppi_test_do ("Checking XML import & export",             check_xml, NULL);

  guppi_test_quit ();
  return 0;
}
