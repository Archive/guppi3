/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * check-missing.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include "check-missing.h"

gboolean
check_missing (GuppiSeq *seq, gint count, ...)
{
  va_list args;
  gint *missing = g_new0 (gint, count);
  gint i, i0, i1, j;
  gboolean ok = TRUE;

  if (guppi_seq_missing_count (seq) != count) {
    g_print (">> missing count is %d, should be %d\n",
	     guppi_seq_missing_count (seq), count);
    return FALSE;
  }

  va_start (args, count);
  for (i=0; i<count; ++i) {
    missing[i] = va_arg (args, gint);
  }
  va_end (args);

  guppi_seq_bounds (seq, &i0, &i1);

  for (i = i0; i <= i1; ++i) {
    gboolean x = guppi_seq_missing (seq, i);

    if (x) {
      gboolean found = FALSE;
      for (j = 0; j < count && !found; ++j) {
	if (missing[j] == i)
	  found = TRUE;
      }
      if (!found) {
	g_print (">> %d spuriously missing\n", i);
	ok = FALSE;
      }
    } else {
      for (j = 0; j < count; ++j)
	if (missing[j] == i) {
	  g_print (">> %d spuriously present\n", i);
	  ok = FALSE;
	}
    }
  }

  return ok;
}

