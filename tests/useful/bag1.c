/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * bag1.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <guppi-test-framework.h>
#include <guppi-useful.h>
#include <guppi-attribute-bag.h>

static gboolean
construct (gpointer foo)
{
  GuppiAttributeBag *bag;

  guppi_test_subtest ("Create");
  bag = guppi_attribute_bag_new ();
  if (bag == NULL)
    return FALSE;

  guppi_test_subtest ("Destroy");
  guppi_unref (bag);

  return TRUE;
}

static gboolean
add (gpointer foo)
{
  GuppiAttributeBag *bag = guppi_attribute_bag_new ();
  const gchar *keys[] = {
    "bool", "int", "uint32", "double", "dimension", "string", "date", "rgb", "rgba", "font",
    NULL };
  gint i;

  guppi_test_subtest ("Adding");
  guppi_attribute_bag_add (bag, GUPPI_ATTR_BOOLEAN,   "bool",      "description");
  guppi_attribute_bag_add (bag, GUPPI_ATTR_INT,       "int",       "description");
  guppi_attribute_bag_add (bag, GUPPI_ATTR_UINT32,    "uint32",    "description");
  guppi_attribute_bag_add (bag, GUPPI_ATTR_DOUBLE,    "double",    "description");
  guppi_attribute_bag_add (bag, GUPPI_ATTR_DIMENSION, "dimension", "description");
  guppi_attribute_bag_add (bag, GUPPI_ATTR_STRING,    "string",    "description");
  guppi_attribute_bag_add (bag, GUPPI_ATTR_DATE,      "date",      "description");
  guppi_attribute_bag_add (bag, GUPPI_ATTR_RGB,       "rgb",       "description");
  guppi_attribute_bag_add (bag, GUPPI_ATTR_RGBA,      "rgba",      "description");
  guppi_attribute_bag_add (bag, GUPPI_ATTR_FONT,      "font",      "description");

  guppi_test_subtest ("Checking");
  for (i = 0; keys[i] != NULL; ++i) {
    if (!guppi_attribute_bag_contains (bag, keys[i]))
      return FALSE;
  }

  guppi_unref (bag);

  return TRUE;
}

static gboolean
test_int (gpointer foo)
{
  GuppiAttributeBag *bag;
  gint f = -666, b = -666;

  guppi_test_subtest ("constructing");
  bag = guppi_attribute_bag_new ();
  guppi_attribute_bag_add (bag, GUPPI_ATTR_INT, "foo", NULL);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_INT, "bar", NULL, 17);

  guppi_test_subtest ("checking defaults");
  guppi_attribute_bag_get (bag, "foo", &f, "bar", &b, NULL);
  if (f != 0 || b != 17)
    return FALSE;

  guppi_test_subtest ("setting");
  guppi_attribute_bag_set (bag, "foo", 1, "bar", 2, NULL);
  f = -666; b = -666;
  guppi_attribute_bag_get (bag, "foo", &f, "bar", &b, NULL);
  if (f != 1 || b != 2)
    return FALSE;

  guppi_test_subtest ("restore defaults");
  guppi_attribute_bag_restore_all_defaults (bag);
  guppi_attribute_bag_get (bag, "foo", &f, "bar", &b, NULL);
  if (f != 0 || b != 17)
    return FALSE;

  guppi_unref (bag);
  
  return TRUE;
}

static gboolean
test_double (gpointer foo)
{
  GuppiAttributeBag *bag;
  double f = -666, b = -666;

  guppi_test_subtest ("constructing");
  bag = guppi_attribute_bag_new ();
  guppi_attribute_bag_add (bag, GUPPI_ATTR_DOUBLE, "foo", NULL);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DOUBLE, "bar", NULL, 17.0);

  guppi_test_subtest ("checking defaults");
  guppi_attribute_bag_get (bag, "foo", &f, "bar", &b, NULL);
  if (f != 0.0 || b != 17.0)
    return FALSE;

  guppi_test_subtest ("setting");
  guppi_attribute_bag_set (bag, "foo", 1.0, "bar", 2.0, NULL);
  f = -666; b = -666;
  guppi_attribute_bag_get (bag, "foo", &f, "bar", &b, NULL);
  if (f != 1.0 || b != 2.0)
    return FALSE;

  guppi_test_subtest ("restore defaults");
  guppi_attribute_bag_restore_all_defaults (bag);
  guppi_attribute_bag_get (bag, "foo", &f, "bar", &b, NULL);
  if (f != 0.0 || b != 17.)
    return FALSE;

  guppi_unref (bag);
  
  return TRUE;
}

int
main (int argc, char *argv[])
{
  guppi_test_init (&argc, &argv);

  guppi_useful_init_without_guile ();

  guppi_test_do ("Create/Destroy a GuppiAttributeBag",   construct, NULL);
  guppi_test_do ("Adds",                                 add, NULL);
  guppi_test_do ("Get/Set w/ Integers",                  test_int, NULL);
  guppi_test_do ("Get/Set w/ Doubles",                   test_double, NULL);

  guppi_test_quit ();
  return 0;
}
