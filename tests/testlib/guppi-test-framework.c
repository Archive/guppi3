/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-test-framework.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include "guppi-test-framework.h"

#include <stdlib.h>
#include <stdio.h>
#include <gnome.h>

#define HELLO ">&>%% "

static int test_count = -1;
static int subtest_count = -1;
static int success_count = 0;

static void
outputf (const char *format, ...)
{
  va_list va;

  va_start (va, format);

  printf ("\n" HELLO);
  vprintf (format, va);
  printf ("\n");

  va_end (va);
}

void
guppi_test_init (gint *argc, gchar **argv[])
{
  gnome_init ((*argv)[0], "testing", *argc, *argv);

  outputf ("running %s", (*argv)[0]);
  test_count = 0;
  success_count = 0;
}

void
guppi_test_quit (void)
{
  g_assert (test_count >= 0);
  outputf ("quit %d %d", test_count, success_count);
}

void
guppi_test_begin (const gchar *description)
{
  g_assert (test_count >= 0);

  ++test_count;
  subtest_count = 0;

  outputf ("test %d %s", test_count, description ? description : "n/a");

}

void
guppi_test_end (gboolean successful)
{
  g_assert (test_count >= 0);

  outputf ("finished %d %s", test_count, successful ? "success" : "failure");

  if (successful)
    ++success_count;
}

void
guppi_test_subtest (const gchar *description)
{
  g_assert (test_count >= 0);
  g_assert (subtest_count >= 0);

  ++subtest_count;

  outputf ("subtest %d.%d %s", test_count, subtest_count, description ? description : "n/a");
}

void
guppi_test_do (const gchar *description, gboolean (*test_fn) (gpointer), gpointer closure)
{
  g_assert (test_fn != NULL);

  guppi_test_begin (description);
  guppi_test_end (test_fn (closure));
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

/* This is not an efficient implementation. */
gboolean
guppi_test_check_file_equality (const gchar *a, const gchar *b)
{
  FILE *in_a, *in_b;
  gchar buf_a[1024], buf_b[1024];
  gint N_a, N_b, i;
  gboolean files_are_equal = TRUE;

  g_return_val_if_fail (a != NULL && b != NULL, FALSE);

  in_a = fopen (a, "r");
  in_b = fopen (b, "r");

  if (in_a == NULL || in_b == NULL)
    return FALSE;

  do {

    N_a = fread (buf_a, 1, 1024, in_a);
    N_b = fread (buf_b, 1, 1024, in_b);

    if (N_a <= 0 && N_b <= 0) {

      break;

    } else if (N_a != N_b) {

	files_are_equal = FALSE;

    } else {

      for (i = 0; i < N_a && files_are_equal; ++i) {
	if (buf_a[i] != buf_b[i])
	  files_are_equal = FALSE;
      }

    }

  } while (files_are_equal);

  return files_are_equal;
}
