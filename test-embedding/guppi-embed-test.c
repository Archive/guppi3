/* $Id$ */

/*
 * guppi-embed-test.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <libguppitank/guppi-tank.h>

static const gint data_size = 7;
static const double data[] = { 1500, 500, 150, 600, 350, 450, 120  };
static const gchar* labels[] = { "Rent", "Food", "Utilities", "Car", 
				  "Pinball", "Beer", "Pork Rinds" };
static const gchar* colors[] = { "red", "blue", "orange", "green",
				  "purple", "goldenrod", "pink" };

static void
slice_click_cb(gint i, gpointer user_data)
{
  printf("Spent $%g on %s\n", data[i], labels[i]);
}

int
main(gint argc, const gchar* argv[])
{

  GuppiObject* gobj;

  GtkWidget* win;
  GtkWidget* w;

  gnome_init("guppi_embed_test", "0.0", argc, argv);
  guppi_tank_init();

  win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_widget_set_usize(win, 400, 300);

  gobj = guppi_object_new("pie",
			  4*72, 3*72,
			  "title", "Monthly Spending",
			  "data_size", data_size,
			  "data", data,
			  "labels", labels,
			  "colors", colors,
			  "slice_callback1",  slice_click_cb,
			  NULL);

  w = guppi_object_build_widget(gobj);
  
  gtk_container_add(GTK_CONTAINER(win), w);
  gtk_widget_show_all(win);

  gtk_main();
}




/* $Id$ */
