#!/usr/bin/perl -w

my %malloc = ();
my %objects = ();
my %permanent = ();

sub do_malloc {
    my $file = shift;
    my $line = shift;
    my $ptr = shift;

    $malloc{$ptr} = [$file, $line];
}

sub do_free {
    my $file = shift;
    my $line = shift;
    my $ptr = shift;

    if (not exists $malloc{$ptr}) {
	print "Unmatched free: $file:$line\n";
    }

    delete $malloc{$ptr};
}

sub flag_as_permanent {
    my $file = shift;
    my $line = shift;
    my $ptr = shift;

    $permanent{$ptr} = [$file, $line];
}

sub do_type_new {
    my $file = shift;
    my $line = shift;
    my $ptr = shift;
    my $name = shift;

    $objects{$ptr} = [$file, $line, $name];
}

sub do_finalize {
    my $file = shift;
    my $line = shift;
    my $ptr = shift;
    my $name = shift;
    
    if (not exists $objects{$ptr}) {
	print "Unmatched finalize: $name ($ptr)\n";
    }

    delete $objects{$ptr};
}


#########################################################################

sub unfreed_memory {
    my %leaks = ();
    my @leaks = ();
    my ($c1, $c2) = (0, 0);
    while (my ($k,$v) = each %malloc) {
	if (!$permanent{$k}) {
	    my ($file, $line) = @$v;
	    my $leak = "$file:$line";
	    if (! exists $leaks{$leak}) {
		push (@leaks, $leak);
		$leaks{$leak} = [ $k ];
	    } else {
		push(@{$leaks{$leak}}, $k);
	    }
	}
    }
    foreach my $l (sort @leaks) {
	my @ll = @{$leaks{$l}};
	my $n = @ll;
	print "LEAK $l ($n)\n";
	++$c1;
	$c2 += $n;

	print "     ";
	print join(" ", @ll);
	print "\n";
    }
    if ($c1) {
	print "LEAK total $c1 ($c2)\n";
    }
}

sub unfreed_objects {
    my %leaks = ();
    my @leaks = ();
    my ($c1, $c2) = (0, 0);
    while (my ($k,$v) = each %objects) {
	if (!$permanent{$k}) {
	    my ($file, $line, $name) = @$v;
	    my $leak = "$file:$line:$name";
	    if (! exists $leaks{$leak}) {
		push (@leaks, $leak);
		$leaks{$leak} = [ $k ];
	    } else {
		push(@{$leaks{$leak}}, $k);
	    }
	}
    }
    foreach my $l (sort @leaks) {
	my @ll = @{$leaks{$l}};
	my $n = @ll;
	print "UNDEST $l ($n)\n";
	++$c1;
	$c2 += $n;

	print "       ";
	print join(" ", @ll);
	print "\n";
    }
    if ($c1) {
	print "UNDEST total $c1 ($c2)\n";
    }
}

#########################################################################

my $tracefile = "/tmp/guppi.memtrace";

open (IN, $tracefile) || die;

my %op_index = (
		'malloc' => \&do_malloc,
		'free' => \&do_free,
		'new' => \&do_malloc,
		'ref' => undef,
		'unref' => undef,
		'sink' => undef,
		'type_new' => \&do_type_new,
		'destroy' => undef,
		'finalized' => \&do_finalize,
		'outside' => \&do_malloc,
		'outside_object' => \&do_type_new,
		'permanent' => \&flag_as_permanent,
		'contains' => undef,
		);

while (defined(my $line = <IN>)) {
    chomp $line;

    next unless $line =~ /\S/;

    my @F = split (/\s*\|\s*/, $line);

    my $file = shift @F;
    my $line = shift @F;
    my $ptr = shift @F;
    my $op = shift @F;

    die "Unknown op \"$op\"" unless exists $op_index{$op};

    my $fn = $op_index{$op};

    $fn->($file,$line,$ptr,@F) if defined $fn;
}

unfreed_memory;
print "\n";

unfreed_objects;
print "\n";
