#!/usr/bin/env python

import sys
import generate

input = sys.argv[1]
p = generate.FilteringParser(input=sys.argv[1], 
                             typeprefix='&')
p.startParsing()

p.writeOutput()
p.writePython()
