## $Id$

##
## gen-scm.pl
##
## Copyright (C) 2000 EMC Capital Management, Inc.
##
## Developed by Jon Trowbridge <trow@gnu.org>.
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 2 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
## USA
## 

use strict;

package Type;
use Carp;

my %registry = ();

sub new {
    my $class = shift;
    my $arg = shift;

    if (defined $arg) {
	warn "Unknown type \"$arg\"" unless defined $registry{$arg};
	return $registry{$arg};
    }

    bless { 'type' => '*invalid type*',
	    'c' => 'void*',
	    'c2s' => 'SCM_UNSPECIFIED',
	    's2c' => 'NULL',
	    '?' => 'TRUE' }, $class;
}

sub declare {
    my $class = shift;
    my $self = new $class;

    my $type = $self->{'type'} = shift;
    $self->{'c'} = shift;
    $self->{'c2s'} = shift;
    $self->{'s2c'} = shift;
    $self->{'?'} = shift;
    $self->{'post'} = shift;
    $self->{'rest'} = shift;

    die "Re-declaration of $type" if exists $registry{$type};

    $registry{$type} = $self;
}

sub declare_f {
    my $class = shift;
    my $self = new $class;

    my $type = $self->{'type'} = shift;
    $self->{'c'} = $self->xform(shift);
    $self->{'c2s'} = $self->xform(shift);
    $self->{'s2c'} = $self->xform(shift);
    $self->{'?'} = $self->xform(shift);

    die "Re-declaration of $type" if exists $registry{$type};
    
    $registry{$type} = $self;
}

sub type {
    my $self = shift;
    $self->{'type'};
}

sub set_parent {
    my $self = shift;
    my $arg = shift;
    $self->{'parent'} = $registry{$arg} || $arg;
}
 
sub parent {
    my $self = shift;
    $self->{'parent'};
}

sub c_type {
    my $self = shift;
    $self->{'c'};
}

sub c2s {
    my $self = shift;
    my $arg = shift;
    my $str = $self->{'c2s'};
    $str =~ s/%s/$arg/g;
    $str;
}

sub s2c {
    my $self = shift;
    my $arg = shift;
    my $str = $self->{'s2c'};
    $str =~ s/%s/$arg/g;
    $str;
}

sub check {
    my $self = shift;
    my $arg = shift;
    my $str = $self->{'?'};
    $str =~ s/%s/$arg/g;
    $str;
}

sub clean_up {
    my $self = shift;
    my $arg = shift;
    my $str = $self->{'post'};
    $str =~ s/%s/$arg/g if $str;
    $str;
}

sub prefix {
    my $self = shift;
    my $name = $self->{'type'};
    $name =~ s/([a-z])([A-Z])/$1_$2/g;
    $name = lc $name;
    $name;
}

sub scm_prefix {
    my $self = shift;
    my $name = $self->prefix;
    $name =~ s/_/-/g;
    $name;
}

sub type_macro {
    my $self = shift;
    my $arg = shift;
    my $str = uc $self->prefix;
    $str .= "($arg)" if $arg;
    $str;
}

sub type_check_macro {
    my $self = shift;
    my $arg = shift;
    my $str = $self->type_macro($arg);
    $str =~ s/^([^_]+)_/$1_IS_/;
    $str;
}

sub smob_tag {
    my $self = shift;
    $self->prefix . "_type_tag";
}

sub xform {
    my $self = shift;
    my $format = shift;

    if (defined $format) {
	$format =~ s/%\^\{([^\}]*)\}/$self->parent->xform($1)/eg;
	$format =~ s/%c2s\(([^\)]+)\)/$self->c2s($1)/eg;
	$format =~ s/%s2c\(([^\)]+)\)/$self->s2c($1)/eg;
	$format =~ s/%\?\(([^\)]+)\)/$self->check($1)/eg;
	$format =~ s /%o/$self->type/eg;
	$format =~ s/%p/$self->prefix/eg;
	$format =~ s/%c/$self->c_type/eg;
	$format =~ s/%P/$self->scm_prefix/eg;
	$format =~ s/%T/$self->type_macro/eg;
	$format =~ s/%Z/$self->type_check_macro/eg;
	$format =~ s/%S/$self->smob_tag/eg;
    }

    $format;
}

package main;

##############################################################################
##
## .c C Code Generator
##
##############################################################################

my @c = ();
my @c_inc = ();
my @init = ();

sub c_include {
    push(@c_inc, @_);
}

sub c {
    push(@c, @_);
}

sub c_f {
    my $fmt = shift;
    c(sprintf($fmt, @_));
}

sub c_init {
    push(@init, @_);
}

sub c_init_f {
    my $fmt = shift;
    c_init(sprintf($fmt, @_));
}

sub c_generate {
    my $filename = shift;

    print "/* This is auto-generated code.  Edit at your own peril! */\n\n";
    print "#include <config.h>\n";
    print "#include <guppi-convenient.h>\n";
    foreach my $x (@c_inc) {
	print "#include $x\n";
    }
    print "\n";

    foreach my $line (@c) {
	print "$line\n";
    }
    print "\n";
    
    my $init_fn = option("init_function");

    print <<EOF;
void
$init_fn(void)
{
  static gboolean init = FALSE;

  if (!guppi_guile_is_active())
    return;

  g_return_if_fail(init == FALSE);
  init = TRUE; /* brace matching hack } */
EOF
    print "\n";
    foreach my $line (@init) {
	print "$line\n";
    }
    print "}\n";
}

##############################################################################
##
## .h C Code Generator
##
##############################################################################

my @h = ();
my @h_inc = ();

sub h_include {
    push(@h_inc, @_);
}

sub h {
    push(@h, @_);
}

sub h_f {
    my $fmt = shift;
    f(sprintf($fmt, @_));
}

sub h_generate {
    my $filename = shift;

    c_include('"' . $filename . '"');

    my $guard = "_INC_" . uc $filename;
    $guard =~ s/[-.]/_/g;

    print "/* This is auto-generated code.  Edit at your own peril! */\n\n";
    
    print "#ifndef $guard\n#define $guard\n\n";

    if (option("includes")) {
	unshift(@h_inc, @{option("includes")});
	unshift(@h_inc, qw( <gnome.h> "guppi-guile.h" ));
    }

    foreach my $x (@h_inc) {
	print "#include $x\n";
    }
    print "\n";

    foreach my $line (@h) {
	print "$line\n";
    }
    print "void ",option("init_function"), "(void);\n";

    print "\n#endif /* $guard */\n";
}

##############################################################################
##
## General Code Generation 
##
##############################################################################

sub generate_code {
    my $filename_base = shift || option("output")->[0];

    $filename_base || die "No generation filename specified.";

    my $init_fn = $filename_base;
    $init_fn =~ s/-/_/g;
    $init_fn .= "_init";
    set_option("init_function", $init_fn);

    my $h = $filename_base . ".h";
    my $c = $filename_base . ".c";
    print STDERR "Building $h.\n";
    open(OUT, ">$h");
    my $saved_fh = select(OUT);
    h_generate($h);
    
    print STDERR "Building $c.\n";
    open(OUT, ">$c");
    select(OUT);
    c_generate($c);

    select($saved_fh);
}


##############################################################################
##
## Options
##
##############################################################################

my %opt = ();

sub set_option {
    my $key = shift;
    my $val = shift;
    $opt{$key} = $val;
}

sub option {
    my $key = shift;
    $opt{$key};
}

sub process_options {
    my @opt_list = @_;

    foreach (@opt_list) {
	my $key = shift @$_;
	set_option($key, $_);
    }
}

##############################################################################
##
## Parse Lisp/Scheme-Style Code
##
##############################################################################

my @stack = ();

sub parse_scheme {
    my $line = shift;
    my $callback = shift;

    chomp $line;
    $line =~ s/;.*//g;  # strip comments
    return if $line =~ /^\s*$/;

    while ($line =~ /\S/) {
	if ($line =~ /^\s*\((.*)/) {
	    unshift (@stack, []);
	    $line = $1;
	} elsif ($line =~ /^\s*\)(.*)/) {
	    my $done = shift @stack;
	    die "Unbalanced parens" if not defined $done;
	    if (@stack) {
		push(@{$stack[0]}, $done);
	    } else {
		$callback->($done);
	    }
	    $line = $1;
	} else {
	    $line =~ /^\s*([^ ()]+)(.*)/;
	    if (defined $1) {
		die "Text outside of parens" unless @stack;
		push(@{$stack[0]}, $1);
		$line = $2;
	    }
	}
    }
}

##############################################################################
##
## Declare Default Types
##
##############################################################################

Type->declare('none', undef,
	      'SCM_UNSPECIFIED',
	      undef,
	      'TRUE');

Type->declare('bool', 'gboolean',
	      'gh_bool2scm(%s)',
	      'gh_scm2bool(%s)',
	      'gh_boolean_p(%s)');

Type->declare('int', 'gint',
	      'gh_int2scm(%s)',
	      'gh_scm2int(%s)',
	      'gh_exact_p(%s)');

Type->declare('size', 'gsize',
	      'gh_int2scm(%s)',
	      '(gsize)gh_scm2int(%s)',
	      '(gh_exact_p(%s) && gh_scm2int(%s)>=0)');

Type->declare('char', 'gchar',
	      'gh_char2scm(%s)',
	      'gh_scm2char(%s)',
	      'gh_char_p(%s)');

Type->declare('string', 'gchar*',
	      'gh_str02scm(%s)',
	      'gh_scm2newstr(%s, NULL)',
	      'gh_string_p(%s)',
	      'free((gpointer)%s)');

Type->declare('const_string', 'const gchar*',
	      'gh_str02scm((gchar*)%s)',
	      'gh_scm2newstr(%s, NULL)',
	      'gh_string_p(%s)');

Type->declare('stringv', 'gchar**',
	      'stringv2scm(%s)',
	      'scm2stringv(%s)',
	      'scm_stringv_p(%s)',
	      'guppi_strfreev(%s)');

Type->declare('double', 'double',
	      'gh_double2scm(%s)',
	      'gh_scm2double(%s)',
	      'gh_number_p(%s)');

Type->declare('color', 'guint32',
	      'color2scm(%s)',
	      'scm2color(%s)',
	      'scm_color_p(%s)');

Type->declare('compass', 'guppi_compass_t',
	      'compass2scm(%s)',
	      'scm2compass(%s)',
	      'scm_compass_p(%s)');

Type->declare('argv', 'GtkArg*',
	      undef,
	      'scm2argv(%s, &rest_len)',
	      'gh_list_p(%s)',
	      'guppi_argv_free(%s, rest_len)',
	      'from-rest-arg');

Type->declare('fn_d__d',
	      'GuppiFnWrapper*',
	      undef,
	      'scm2fn_d__d(%s)',
	      'gh_procedure_p(%s)',
	      'guppi_unref(%s)');

Type->declare('fn_d__d_d',
	      'GuppiFnWrapper*',
	      undef,
	      'scm2fn_d__d_d(%s)',
	      'gh_procedure_p(%s)',
	      'guppi_unref(%s)');

Type->declare('fn_d__i',
	      'GuppiFnWrapper*',
	      undef,
	      'scm2fn_d__i(%s)',
	      'gh_procedure_p(%s)',
	      'guppi_unref(%s)');


##############################################################################
##
## New Type Definition
##
##############################################################################


sub declare_smob_type {
    my $type = shift;
    my $parent = shift;

    $type = Type->declare_f($type, '%o*',
			    '%p2scm(%T(%s))',
			    '%T(scm2%p(%s))',
			    'scm_%p_p(%s)');

    $type->set_parent($parent) if $parent;

    $type;
}

sub put_standard_smob_functions_in_header {
    my $type = shift;

    h $type->xform(<<'EOF');

gboolean scm_%p_p(SCM);
SCM %p2scm(%c);
%c scm2%p(SCM);

EOF
}

sub define_new_smob_type {
    my $type = shift;
    
    $type = declare_smob_type($type);
    
    put_standard_smob_functions_in_header($type);

    c $type->xform(<<'EOF');
static long %S;
#define SCM_TO_%T(x) (%T(SCM_CDR(x)))
#define SCM_%T_P(x) (SCM_NIMP(x) && SCM_CAR(x) == %S)

gboolean
scm_%p_p(SCM x)
{
    return SCM_%T_P(x);
} 

SCM
%p2scm(%c x)
{
    SCM smob;
    if (x == NULL || !%Z(x))
	return SCM_UNSPECIFIED;
    SCM_DEFER_INTS;
#ifdef USING_GUILE_1_3_X
    SCM_NEWCELL(smob);
    SCM_SETCAR(smob, %S);
    SCM_SETCDR(smob, x);
#else
    SCM_NEWSMOB(smob, %S, x);
#endif
    guppi_ref(x);
    guppi_sink(x);
    SCM_ALLOW_INTS;

    return smob;
}

%c
scm2%p(SCM x)
{
    g_return_val_if_fail(SCM_%T_P(x), NULL);
    return SCM_TO_%T(x);
}

static SCM
mark_%p(SCM x)
{
    return SCM_BOOL_F;
}

static scm_sizet
free_%p(SCM x)
{
    %c y = SCM_TO_%T(x);
    SCM_DEFER_INTS;
    guppi_unref(y);
    SCM_ALLOW_INTS;

    return 0;
}

static int
print_%p(SCM x, SCM port, scm_print_state* state)
{
    scm_puts("<%o>", port);
    return 1;
}

#ifdef USING_GUILE_1_3_X
static struct scm_smobfuns %p_smob_fns = {
    mark_%p, free_%p, print_%p, NULL
};
#endif

static SCM
fn_scm_%p_p(SCM x)
{
    return gh_bool2scm(SCM_%T_P(x));
}

EOF

    c_init $type->xform(<<EOF);
#ifdef USING_GUILE_1_3_X
  %S = scm_newsmob(&%p_smob_fns);
#else
  %S = scm_make_smob_type("%o", 1);
  scm_set_smob_mark(%S, mark_%p);
  scm_set_smob_free(%S, free_%p);
  scm_set_smob_print(%S, print_%p);
#endif
EOF

    c_init $type->xform('  scm_make_gsubr("%P?", 1, 0, 0, fn_scm_%p_p);');
}

sub define_derived_smob_type {
    my $type = shift;
    my $parent = new Type(shift);

    $type = declare_smob_type($type, $parent);
    put_standard_smob_functions_in_header($type);

    c $type->xform(<<'EOF');

gboolean
scm_%p_p(SCM x)
{
  %^{%c} foo;
  if (%^{%?(x)}) {
    foo = %^{%s2c(x)};
    if (foo != NULL && %Z(foo))
      return TRUE;
  }
  return FALSE;
}

SCM
%p2scm(%c x)
{
  return %^{%p}2scm(%^{%T}(x));
}

%c
scm2%p(SCM x)
{
  return %T(scm2%^{%p}(x));    
}

static SCM
fn_scm_%p_p(SCM x)
{
  return gh_bool2scm(scm_%p_p(x));
}

EOF

    c_init $type->xform('  scm_make_gsubr("%P?", 1, 0, 0, fn_scm_%p_p);');
}

sub define_object {
    my $type = shift;
    my $parent = shift;
    
    $parent = $parent->[0] if $parent;

    if ($parent) {
	define_derived_smob_type($type, $parent);
    } else {
	define_new_smob_type($type, $parent);
    }
}

sub define_object_extern {
    my $type = shift;

    declare_smob_type($type);
}

##############################################################################
##
## Enum Definition
##
##############################################################################

my $first_enum = 1;
sub define_enum {
    my $type = shift;
    my @vals = @_;

    $type = Type->declare_f($type, 
			    $type,
			    "%p2scm(%s)",
			    "scm2%p(%s)",
			    "scm_%p_p(%s)");

    c_include('<string.h>') if $first_enum;
    $first_enum = 0;

    h $type->xform(<<'EOF');
gboolean scm_%p_p(SCM);
SCM %p2scm(%c);
%c scm2%p(SCM);
EOF

    my $val_str = join(", ", (map { "\"$_\"" } @vals), "NULL");
    c $type->xform("static const gchar* scm_str_%p[] = { $val_str };");
    c $type->xform("static const %c scm_enum_%p[] = { ".join(", ",@vals)." };");

    c $type->xform(<<'EOF');

/* This sort of linear search is not the most efficient option, but is
   optimized for correctness. */

gboolean
scm_%p_p(SCM x)
{
  gchar* symb;
  gint i=0;
  if (!gh_symbol_p(x)) return FALSE;
  symb = gh_symbol2newstr(x, NULL);
  while (scm_str_%p[i] != NULL && strcmp(symb, scm_str_%p[i])) ++i;
  free(symb);
  return scm_str_%p[i] != NULL;
}

SCM
%p2scm(%c x)
{
  gint i=0;
  while (scm_str_%p[i] != NULL && scm_enum_%p[i] != x) ++i;
  if (scm_str_%p[i] != NULL)
    return gh_symbol2scm((gchar*)scm_str_%p[i]);
  else
    return SCM_UNDEFINED;
}

%c
scm2%p(SCM x)
{
  gchar* symb;
  gint i=0;
  symb = gh_symbol2newstr(x, NULL);
  while (scm_str_%p[i] != NULL && strcmp(symb, scm_str_%p[i])) ++i;
  g_assert(scm_str_%p[i] != NULL);
  free(symb);
  return scm_enum_%p[i];
}

static SCM
fn_scm_%p_p(SCM x)
{
  return gh_bool2scm(scm_%p_p(x));
}

EOF

    c_init $type->xform('  scm_make_gsubr("%P?", 1, 0, 0, fn_scm_%p_p);');

}
    



##############################################################################
##
## Function Definition
##
##############################################################################

sub define_function {
    my $fnname = shift;
    my $retval = shift;
    my $args = shift;
    my $options = shift;

    print STDERR "Wrapping $fnname...\n";

    my $has_rest = 0;

    my $scm_name = $fnname;
    $scm_name =~ s/_/-/g;

    my $arg_count = @$args;

    ## Convert type names to type objects
    foreach my $arg (@$args) {
	($arg->[0] = new Type($arg->[0])) || die;
	$has_rest = 1 if $arg->[0]->{'rest'};
    }

    $retval = new Type($retval);

    c "static const gchar* s_scm_$fnname = \"$scm_name\";";
    c "static SCM"; 

    my @args = map { "SCM s_" . $_->[1] } @$args;
    c "fn_scm_$fnname(" . join(", ", @args) . ")";
    c "{";

    ### Declare our local variables
    foreach (@$args) {
	my ($type, $name) = @$_;
	c $type->xform("  %c c_$name;");
    }
    c $retval->xform("  %c c_retval;") if $retval->type ne "none";
    c "  SCM s_retval = SCM_UNSPECIFIED;";

    if ($has_rest) {
	c "  guint rest_len;";
    }

    ### Check our function args
    my $i = 1;
    foreach (@$args) {
	my ($type, $name) = @$_;
	# We cast out const from arg 4 for guile 1.3 compatibility.
	c $type->xform("  SCM_ASSERT(%?(s_$name), s_$name, SCM_ARG$i, (gchar*)s_scm_$fnname);");
    ++$i;
    }

    ### Convert our function args
    foreach (@$args) {
	my ($type, $name) = @$_;
	c $type->xform("  c_$name = %s2c(s_$name);");
    }

    ### Make the function call
    @args = map { 
	my $x = "c_" . $_->[1];
	$x = "rest_len, $x" if ($_->[0]->{'rest'});
	$x } @$args;
    my $fc = $fnname . "(" . join(", ", @args) . ");";

    $fc = "c_retval = $fc" if $retval->type ne "none";

    c "  $fc";

    ### Handle return value conversion
    if ($retval->type ne "none") {
	c $retval->xform("  s_retval = %c2s(c_retval);");
	my $clean = $retval->clean_up("c_retval");
	c "  $clean;" if $clean;
    }

    ### Do any other necessary clean-up
    foreach (@$args) {
	my ($type, $name) = @$_;
	my $clean = $type->clean_up("c_$name");
	c "  $clean;" if $clean;
    }
    c "  return s_retval;";
    c "}\n";

    $arg_count -= $has_rest;
    # We cast out const from arg1 for guile 1.3 compatibility
    c_init "  scm_make_gsubr((gchar*)s_scm_$fnname, $arg_count, 0, $has_rest, fn_scm_$fnname);";
}

##############################################################################
##
## Other Main Functions
##
##############################################################################

my %dispatch = ("define-func" => \&define_function,
		"define-object" => \&define_object,
		"define-object-extern" => \&define_object_extern,
		"define-enum" => \&define_enum,
		"options" => \&process_options,
		);

sub callback {
    my $list = shift;
    my $head = shift @$list;

    if ($dispatch{$head}) {
	$dispatch{$head}->(@$list);
    } else {
	print STDERR "Unknown function: [$head]\n";
    }
}

while (defined(my $line = <>)) {
    parse_scheme($line, \&callback);
}

generate_code();
