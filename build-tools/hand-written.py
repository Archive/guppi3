#the below lifted from PyGtk's gtk.py

import _gtk

_name2cls = {}
_supported = map(lambda x: eval(x), filter(lambda x: x[:5] == 'Guppi', dir()))
for m in _supported:
	_name2cls[m.__name__] = m
del m

def _obj2inst(obj):
    if type(obj) is not _gtk.GtkObjectType:
	    return obj
    objname = _gtk.gtk_type_name(_gtk.GTK_OBJECT_TYPE(obj))
    if _name2cls.has_key(objname):
        return _name2cls[objname](_obj=obj)
    # if I don't know what the object is, guess
    elif _gtk.GTK_CHECK_TYPE(obj, GtkBox.get_type()):
        return GtkBox(_obj=obj)
    elif _gtk.GTK_CHECK_TYPE(obj, GtkBin.get_type()):
        return GtkBin(_obj=obj)
    elif _gtk.GTK_CHECK_TYPE(obj, GtkWindow.get_type()):
        return GtkWindow(_obj=obj)
    elif _gtk.GTK_CHECK_TYPE(obj, GtkContainer.get_type()):
        return GtkContainer(_obj=obj)
    elif _gtk.GTK_CHECK_TYPE(obj, GtkWidget.get_type()):
        return GtkWidget(_obj=obj)
    else:
        return GtkObject(_obj=obj)

