
"""
Generates .c .h and .py files from .defs files.

Taken from gnome-python. Originally written by James Henstridge <james@daa.com.au>
Modified and adapted for guppi by Andrew H. Chatham <andrew.chatham@duke.edu>
"""

#!/usr/bin/env python

import os
import string
import scmexpr
import re
import top

try:
	from cStringIO import StringIO
except ImportError:
	from StringIO import StringIO
# for fnmatchcase -- filename globbing
import fnmatch

TRUE  = 1
FALSE = 0


funcDefTmpl = '    { "%s", _wrap_%s, 1 },\n'
funcLeadTmpl = 'static PyObject *_wrap_%s(PyObject *self, PyObject *args) {\n'
setVarTmpl = '''  if (py_%s)
        %s = %s;\n'''
getTypeTmpl = '''static PyObject *_wrap_%s(PyObject *self, PyObject *args) {
    if (!PyArg_ParseTuple(args, ":%s"))
        return NULL;
    return PyInt_FromLong(%s());
}\n\n'''
enumCodeTmpl = '''    %s = py2%s(%s);\n'''
argvCodeTmpl = '''    %s = py2argv(%s, &rest_len);
    if (%s == NULL) return NULL;
'''

nullokTmpl = '''    if (PyGuppi_Check(py_%s))
        %s = %s(PyGuppi_Get(py_%s));
    else if (py_%s != Py_None) {
        PyErr_SetString(PyExc_TypeError, "%s argument must be a %s or None");
	return NULL;
    }\n'''

nullokBoxedTmpl = '''    if (Py%s_Check(py_%s))
        %s = %s(py_%s);
    else if (py_%s != Py_None) {
        PyErr_SetString(PyExc_TypeError, "%s argument must be a %s or None");
	return NULL;
    }\n'''

convSpecialCases = {
}

def toUpperStr(str):
	"""Converts a typename to the equivalent upercase and underscores
	name.  This is used to form the type conversion macros and enum/flag
	name variables"""
	if convSpecialCases.has_key(str):
		return convSpecialCases[str]
	ret = []
	while str:
		if len(str) > 1 and str[0] in string.uppercase and \
		   str[1] in string.lowercase:
			ret.append("_" + str[:2])
			str = str[2:]
		elif len(str) > 3 and \
		     str[0] in string.lowercase and \
		     str[1] in 'HV' and \
		     str[2] in string.uppercase and \
		     str[3] in string.lowercase:
			ret.append(str[0] + "_" + str[1:3])
			str = str[3:]
		elif len(str) > 2 and \
		     str[0] in string.lowercase and \
		     str[1] in string.uppercase and \
		     str[2] in string.uppercase:
			ret.append(str[0] + "_" + str[1])
			str = str[2:]
		else:
			ret.append(str[0])
			str = str[1:]
	return string.upper(string.join(ret, ''))

#get rid of this?
def enumName(typename):
	"""create a GTK_TYPE_* name from the given type"""
	return typename #this is the name we want

def conversionMacro(typename):
	"""get the conversion macro for the given type"""
	return toUpperStr(typename)[1:]

lowercache = {}
def toLowerStr(str):
	"""Converts from something like GuppiStream to guppi_stream"""
	newstr = [string.lower(str[0])]
	for let in str[1:]:
		if let in string.uppercase:
			newstr.append('_')
			newstr.append(string.lower(let))
		else:
			newstr.append(let)
	ret = string.join(newstr, '')
	lowercache[ret] = str
	return ret

def unLowerStr(str):
	"""Reverse toLowerStr. Must have called toLowerStr"""
	return lowercache[str]

# a dictionary of known enum types ...
# the value is a cached copy of the GTK_TYPE name
enums = {}
# and flags ...
flags = {}

# a dictionary of objects ...
# the values are the conversion macros
objects = {}

# the known boxed types
boxed = {}

class VarDefs:
	def __init__(self):
		self.vars = {}
	def add(self, type, name):
		if self.vars.has_key(type):
			self.vars[type] = self.vars[type] + (name,)
		else:
			self.vars[type] = (name,)
	def __str__(self):
		ret = []
		for type in self.vars.keys():
			ret.append('    ')
			ret.append(type)
			ret.append(' ')
			ret.append(string.join(self.vars[type], ', '))
			ret.append(';\n')
		if ret: ret.append('\n')
		return string.join(ret, '')

class TypesParser(scmexpr.Parser):
	"""A parser that only parses definitions -- no output"""
	def define_enum(self, name, *values):
		if not enums.has_key(name):
			enums[name] = enumName(name)
	def define_object(self, name, parent, *args):
		if not objects.has_key(name):
			objects[name] = conversionMacro(name)
	def define_object_extern(self, name, *args):
		#Define it so it won't complain but don't give enough
		#information to actually generate any code
		if not objects.has_key(name):
			objects[name] = conversionMacro(name)
	def include(self, filename):
		if filename[0] != '/':
			# filename relative to file being parsed
			afile = os.path.join(os.path.dirname(self.filename),
					     filename)
		else:
			afile = filename
		if not os.path.exists(afile):
			# fallback on PWD relative filename
			afile = filename
		#print "including file", afile
		fp = open(afile)
		self.startParsing(scmexpr.parse(fp))

init_template = '''    {NULL, NULL}
};

void %s_init() {
    static gboolean init = FALSE;
    if (!guppi_python_is_active()) return;
    g_return_if_fail(init == FALSE);
    init = TRUE;
    Py_InitModule("%s", local_guppi_methods);
}'''

header_template = '''#ifndef _%s_H
#define _%s_H

#include "guppi-useful.h"
#include "guppi-python.h"

void %s_init(void);

'''

func_template =	'''
def %s(%s):
    """Autogenerated from C function:
       %s"""
    _ret = %s.%s(%s)
    if type(_ret) is _guppi.GuppiObjectType: _ret = _obj2inst(_ret)
    return _ret
'''

class FunctionDefsParser(TypesParser):
	def __init__(self, input, prefix='gtkmodule', typeprefix=''):
		# typeprefix is set to & if type structs are not pointers
		TypesParser.__init__(self, input)
		self.impl = StringIO()# open(prefix + '_impl.c', "w")
		self.defs = StringIO()# open(prefix + '_defs.c', "w")
		self.includes = []
		self.pyimports = []
		self.plugin = 0
		self.tp = typeprefix
		self._options = {}
		self.allobjects = {}
		self.dependencies = {}
		self.allenums = {}
		self.simplefunctions = []
		self.prefixes = []

	def writeOutput(self):
		initname = re.sub('scm|guile', 'python', self._options['output'])
		output = initname + '.c'
		headerfile = initname + '.h'
		modulename = '_g_' + re.sub('guppi-python-','', initname)
		modulename = re.sub('-','_',modulename)

		self.modulename = modulename
		
		initname = re.sub('-', '_', initname)
		
		h = open(headerfile, 'w')
		u = string.upper(initname)
		h.write(header_template % (u, u, initname))
		myre = re.compile(r'scm|guile')
		for include in self.includes:
			if '"' in include or '<' in include:
				h.write('#include %s\n' % myre.sub('python', include))
			else:
				h.write('#include "%s"\n' % myre.sub('python', include))
		h.write('\n#endif\n')
		h.close()

		c = open(output, 'w')
		c.write("\n/* This is autogenerated code. Edit at your own risk. */\n\n")
		c.write('#include "%s"\n\n' % headerfile)
		c.write(self.impl.getvalue())
		c.write('static PyMethodDef local_guppi_methods[] = {')
		c.write(self.defs.getvalue())
		c.write(init_template % (initname, modulename))
		c.write('\n')
		c.close()

	def unknown(self, tup):
		print "Unknown function:", (tup and tup[0] or "")

	def options(self, *args):
		for entry in args:
			if entry[0] == 'includes':
				self.includes = entry[1:]
			elif entry[0] == 'pyimport':
				self.pyimports = entry[1:]
			elif entry[0] == 'plugin':
				self.plugin = 1
				self.get_plugin_info(entry[1])
			else:
				self._options[entry[0]] = entry[1]

	def get_plugin_info(self, filename):
		'''Gets the plugin type and code from the plugin file.'''
		strip = string.strip

		code_re = re.compile('Code\W*=\W*(\w+)', re.I)
		type_re = re.compile('Type\W*=\W*(\w+)', re.I)
		
		f = open(filename, 'r')
		for line in f.readlines():
			line = strip(line)
			m = code_re.match(line)
			if m:
				self.plugin_code = m.group(1)
			m = type_re.match(line)
			if m:
				self.plugin_type = m.group(1)


	def define_enum(self, name, *values):
		if not enums.has_key(name):
			Enum(self, name, values)
			enums[name] = enumName(name)

	def define_object(self, name, parent=(), fields=()):
		if not objects.has_key(name):
			Object(self, name, parent)
		TypesParser.define_object(self, name, parent)
		get_type = string.lower(objects[name]) + '_get_type'
		self.defs.write(funcDefTmpl % (get_type,get_type))
		self.impl.write(getTypeTmpl % (get_type,get_type,get_type))

		if fields and fields[0] == 'fields':
			for retType, attrname in fields[1:]:
				self.get_attr_func(name, retType, attrname)



	def get_attr_func(self, name, retType, attrname):
		impl = StringIO()
		funcName = string.lower(objects[name]) + '_get_' + attrname
		impl.write(funcLeadTmpl % (funcName,))
		impl.write('    PyObject *obj;\n\n')
		impl.write('    if (!PyArg_ParseTuple(args, "O!:')
		impl.write(funcName)
		impl.write('", ' + self.tp + 'PyGuppi_Type, &obj))\n')
		impl.write('        return NULL;\n')
		funcCall = '%s(PyGuppi_Get(obj))->%s' % (objects[name], attrname)
		if self.decodeRet(impl, funcCall, retType):
			return
		impl.write('}\n\n')
		# actually write the info to the output files
		self.defs.write(funcDefTmpl % (funcName, funcName))
		self.impl.write(impl.getvalue())

        def define_func(self, name, retType, args):
		# we write to a temp file, in case any errors occur ...
		impl = StringIO()
		impl.write(funcLeadTmpl % (name,))
		varDefs = VarDefs()
		parseStr = ''   # PyArg_ParseTuple string
		parseList = []  # args to PyArg_ParseTuple
		argList = []    # args to actual function
		extraCode = []  # any extra code (for enums, flags)
		retArgs = None

		varDefs.add('PyObject', '*_ret') # to hold the final value
		if retType in ('stringv', ):
			varDefs.add('gchar', '**ret')
		if retType in ('string', 'const_string'):
			varDefs.add('gchar', '*ret')

		if type(retType) == type(()):
			retArgs = retType[1:]
			retType = retType[0]
			
		#see if it should be a constructor
		_x = self.isamethod(name, constok=1)
		if _x:
			allsimple = 1
			for arg in args:
				argType = arg[0]
				argName = arg[1]
				if argType in objects.keys():
					allsimple = 0
					break
			if allsimple:
				_x.addconstructor(name, args)
			
		simple = (name, retType, args)
		for arg in args:
			argType = arg[0]
			argName = arg[1]
			# munge special names ...
			if argName in ('default', 'if', 'then', 'else',
				       'while', 'for', 'do', 'case', 'select'):
				argName = '_' + argName
			default = ''
			nullok = FALSE
			if len(arg) > 2:
				for a in arg[2:]:
					if type(a) == type(()) and a[0] == '=':
						default = ' = ' + a[1]
						if '|' not in parseStr:
							parseStr = parseStr+'|'
					elif type(a) == type(()) and \
					     a[0] == 'null-ok':
						nullok = TRUE
			if argType in ('string', ):
				varDefs.add('gchar', '*' + argName + default)
				if nullok:
					parseStr = parseStr + 'z'
				else:
					parseStr = parseStr + 's'
				parseList.append('&' + argName)
				argList.append(argName)
			elif argType in ('const_string', ):
				varDefs.add('const gchar', '*' + argName + default)
				if nullok:
					parseStr = parseStr + 'z'
				else:
					parseStr = parseStr + 's'
				parseList.append('&' + argName)
				argList.append(argName)
			elif argType in ('stringv', ):
				varDefs.add('gchar**', argName + default)
				parseStr = parseStr + 'c'
				parseList.append('&' + argName)
				argList.append(argName)
			elif argType in ('color', ):
				varDefs.add('guint32', argName + default)
				parseStr = parseStr + 'O&'
				parseList.append('py2color')
				parseList.append('&' + argName)
				argList.append(argName)
			elif argType in ('size', ):
				varDefs.add('gsize', argName + default)
				parseStr = parseStr + 'c'
				parseList.append('&' + argName)
				argList.append(argName)
			elif argType in ('char', 'uchar'):
				varDefs.add('char', argName + default)
				parseStr = parseStr + 'c'
				parseList.append('&' + argName)
				argList.append(argName)
			elif argType in ('bool', 'int', 'uint', 'long',
					 'ulong', 'guint', 'GdkAtom'):
				# pretend atoms are integers for input ...
				varDefs.add('int', argName + default)
				parseStr = parseStr + 'i'
				parseList.append('&' + argName)
				argList.append(argName)
			elif argType in ('float', 'double'):
				varDefs.add('double', argName + default)
				parseStr = parseStr + 'd'
				parseList.append('&' + argName)
				argList.append(argName)
			elif argType == 'FILE':
				varDefs.add('PyObject', '*' + argName+default)
				parseStr = parseStr + 'O!'
				parseList.append('&PyFile_Type')
				parseList.append('&' + argName)
				argList.append('PyFile_AsFile(' +
					       argName + ')')
			elif argType in ('compass', ):
				varDefs.add("guppi_compass_t ", argName + default)
				if default:
					varDefs.add('PyObject', '*py_' + \
					             argName + ' = NULL')
				else:
					varDefs.add('PyObject', '*py_'+argName)
				parseStr = parseStr + 'O&'
				parseList.append('py2compass')
				parseList.append('&' + argName)
				argList.append(argName)
						    
			elif argType in ('argv', ):
				varDefs.add("GtkArg ", '*' + argName + default)
				varDefs.add("int ", "rest_len")
				if default:
					varDefs.add('PyObject', '*py_' + \
						    argName + ' = NULL')
				else:
					varDefs.add('PyObject', '*py_'+argName)
				parseStr = parseStr + 'O'
				parseList.append('&py_' + argName)
				argList.append('rest_len')
				argList.append(argName)
				extraCode.append(argvCodeTmpl % (argName,
								 'py_' + argName,
								 'py_' + argName))
			elif argType in enums.keys():
				varDefs.add(argType, argName + default)
				if default:
					varDefs.add('PyObject', '*py_' + \
						    argName + ' = NULL')
				else:
					varDefs.add('PyObject', '*py_'+argName)
				parseStr = parseStr + 'O'
				parseList.append('&py_' + argName)
				argList.append(argName)
				extraCode.append(enumCodeTmpl %
						 (argName,
						  toLowerStr(argType),
						  'py_' + argName))
			elif argType in objects.keys():
				if len(argList) == 0: #1st arg an object
					obj = self.findobject(argType)
					obj.addmethod(name, retType, args)
					simple = None
				if nullok:
					parseStr = parseStr + 'O'
					parseList.append('&py_' + argName)
					varDefs.add('PyObject', '*py_' +
						    argName + ' = Py_None')
					varDefs.add(argType, '*' + argName +
						    ' = NULL')
					extraCode.append(nullokTmpl %
							 (argName, argName,
							  objects[argType],
							  argName, argName,
							  argName, argType))
					argList.append(argName)
				elif default:
					parseStr = parseStr + 'O!'
					parseList.append(self.tp +'PyGuppi_Type')
					varDefs.add('PyObject', '*py_' +
						    argName + ' = NULL')
					varDefs.add(argType, '*' + argName +
						    default)
					parseList.append('&py_' + argName)
					extraCode.append(setVarTmpl %
							 (argName, argName,
							  objects[argType] +
							  '(PyGuppi_Get(py_' +
							  argName + '))'))
					argList.append(argName)
				else:
					parseStr = parseStr + 'O!'
					parseList.append(self.tp +'PyGuppi_Type')
					varDefs.add('PyObject', '*' + argName)
					parseList.append('&' + argName)
					argList.append(objects[argType] +
						       '(PyGuppi_Get(' +
						       argName + '))')
			else:
				print "%s: unknown arg type '%s'" % (name,
								     argType)
				return
			
		if simple: self.simplefunctions.append(simple)

		impl.write(str(varDefs))
		impl.write('    if (!PyArg_ParseTuple(args, "')
		impl.write(parseStr)
		impl.write(':')
		impl.write(name)
		impl.write('"')
		if parseList:
			impl.write(', ')
			impl.write(string.join(parseList, ', '))
		impl.write('))\n        return NULL;\n')
		impl.write(string.join(extraCode, ''))

		funcCall = name + '(' + string.join(argList, ', ') + ')'
		if self.decodeRet(impl, funcCall, retType, retArgs, args=args):
			return
		impl.write('}\n\n')
		# actually write the info to the output files
		self.defs.write(funcDefTmpl % (name,name))
		self.impl.write(impl.getvalue())

	def decodeRet(self, impl, funcCall, retType, retArgs=None, args=None):
		nullok = FALSE
		if retArgs: #I'm not sure what this is for - AHC
			# XXX fix me
			if retArgs[0] == 'null-ok':
				nullok = TRUE
		        else:
				print "unknown return attribute '%s'" % (retArgs,)
				return 1
		if retType == 'none':
			impl.write('    ')
			impl.write(funcCall)
			impl.write(';\n    _ret = Py_None;\n    Py_INCREF(Py_None);\n')
		elif retType == 'static_string':
			impl.write('    _ret = PyString_FromString(')
			impl.write(funcCall)
			impl.write(');\n')

		elif retType == 'color':
			impl.write('    _ret = color2py(')
			impl.write(funcCall)
			impl.write(');\n')
		elif retType in ('stringv', ):
			if nullok:
				impl.write('    ret = %s;\n' % funcCall)
				impl.write('    if (ret) {\n')
				impl.write('        _ret = stringv2py(ret);\n'
					   '        g_strfreev(ret);\n'
					   '    } else {\n'
					   '        _ret = Py_None;\n'
					   '        Py_INCREF(Py_None);\n'
					   '    }\n')
			else:
				impl.write('    ret = %s;\n' % funcCall)
				impl.write('    _ret = stringv2py(ret);\n'
					   '    g_strfreev(ret);\n')
		elif retType in ('string', 'const_string'):
			if nullok:
				if retType == 'string':
					impl.write('    ret = %s;\n' % funcCall)
				else:
					impl.write('    ret = g_strudp(%s);\n' % funcCall)
				impl.write('    if (ret) {\n')
				impl.write('        _ret = PyString_FromString(ret);\n'
					   '        g_free(ret);\n'
					   '    } else {\n'
					   '        _ret = Py_None;\n'
					   '        Py_INCREF(Py_None);\n'
					   '    }\n')
			else:
				if retType == 'string':
					impl.write('    ret = %s;\n' % funcCall)
				else:
					impl.write('    ret = g_strdup(%s);\n' % funcCall)
				impl.write('    _ret = PyString_FromString(ret);\n'
					   '    g_free(ret);\n')
		elif retType in ('size', ):
			impl.write('    _ret = PyInt_FromLong((')
			impl.write(funcCall)
			impl.write('));\n')
		elif retType in ('char', 'uchar'):
			impl.write('    _ret = PyInt_FromLong((')
			impl.write(funcCall)
			impl.write('));\n')
		elif retType in ('bool','int','uint','long','ulong','guint'):
			impl.write('    _ret = PyInt_FromLong(')
			impl.write(funcCall)
			impl.write(');\n')
		elif retType in enums.keys():
			enum2py = self.allenums[retType].enum2py
			impl.write('    _ret = %s(' % enum2py)
			impl.write(funcCall)
			impl.write(');\n')
		elif retType in ('float','double'):
			impl.write('    _ret = PyFloat_FromDouble(')
			impl.write(funcCall)
			impl.write(');\n')
		elif retType in ('compass', ):
			impl.write('    _ret = compass2py(')
			impl.write(funcCall)
			impl.write(');\n')
		elif retType in ('argv', ):
			raise NotImplementedError
		elif retType in objects.keys():
			if nullok:
				impl.write('    {\n'
					   '        GtkObject *p = (GtkObject *) %s;' % funcCall)
				impl.write('        if (p)\n'
					   '            _ret = PyGuppi_New((GtkObject *) p);\n'
					   '        else {\n'
					   '            _ret = Py_None;\n')
				impl.write('            Py_INCREF(Py_None);\n'
					   '        }\n'
					   '    }\n')
			else:
				impl.write('    _ret = PyGuppi_New((GtkObject *)')
				impl.write(funcCall)
				impl.write(');\n')
		else:
			print "unknown return type '%s'" % (retType,)
			return 1
		if args:
			cleanup = self.gencleanup(args)
			if cleanup: impl.write(cleanup)
			
		impl.write('    return _ret;\n')
		return 0

	def findobject(self, name):
		return self.allobjects[name]

	def _simplestringcontains(self, s1, s2):
		s1 = string.lower(s1)
		s2 = string.lower(s2)
		s1 = string.replace(s1, '_', '')
		s2 = string.replace(s2, '_', '')
		return string.find(s1, s2) == 0

	def isamethod(self, name, constok = 0):
		"""Checks the name of the function to determine if
		it's a method or not. Returns the object for which
		it is a method"""
		match = None #want the longest match
		for prefix in self.prefixes:
			if self._simplestringcontains(name, prefix):
				if not constok and string.find(name, '_new') != -1:
					return None #don't want constructors
				newmatch = self.findobject(unLowerStr(prefix[:-1]))
				if not match: match = newmatch
				elif len(match.name) < len(newmatch.name):
					match = newmatch
		return match

	def writePython(self):
		"""Write the python wrapper code to the output file."""
		initname = re.sub('scm|guile', 'python',
				  self._options['output'])
		output = re.match(r'(guppi-)?(scm-)?(.+)', self._options['output']).group(3) + '.py'
		o = open(output, 'w')

		for imp in self.pyimports:
			o.write('from %s import *\n' % imp)
		if self.plugin:
			o.write("%(n)s = LazyPluginModule('%(n)s', '%(t)s', '%(c)s')\n" %
				{'n' : self.modulename,
				 't' : self.plugin_type,
				 'c' : self.plugin_code})
		else:
			o.write("import %s\n" % self.modulename)

		#first class declarations
		ordered = top.TopSorter(self.dependencies)()
		for name in ordered:
			if self.allobjects.has_key(name):
				object = self.allobjects[name]
				o.write(object.pycode())
			else:
				print "No %s in %s" % (name, self.modulename)
		o.write('\n')
		
		#now regular function wrappers
		for name, retType, args in self.simplefunctions:
			o.write(self.funcwrapper(name, retType, args))

		o.close()

	def funcwrapper(self, name, retType, args):
		pyname = string.replace(name, 'guppi_', '')

		cargs = map(lambda x: string.join(x, ' '), args)
		cargs = string.join(cargs, ', ')
		cdecl = "%s %s(%s)" % (retType, name, cargs)

		args = map(lambda x: x[1], args)
		args = string.join(args, ', ')
		text = func_template % (pyname, args, cdecl,
						self.modulename, name, args)
		return text
	
	def gencleanup(self, args):
		"""Generate C code for cleaning up (freeing memory, etc.)
		after the C API function has been called but before the
		return."""
		cleanup = []
		for type, name in args:
			if type in ('argv', ):
				c = 'guppi_argv_free(%s, rest_len);' % name
				cleanup.append(c)
		if cleanup: return indent(string.join(cleanup, '\n')) + '\n'
		else: return ''

class FilteringParser(FunctionDefsParser):
	"""A refinement of FunctionDefsParser with some common filter types
	built in"""

	def __init__(self, input, prefix='gtkmodule', typeprefix=''):
		FunctionDefsParser.__init__(self, input, prefix, typeprefix)
		# hash lookups are pretty fast ...
		self.excludeList = {}
		self.excludeGlob = []
	def filter(self, name):
		if self.excludeList.has_key(name):
			return 0
		for glob in self.excludeGlob:
			if fnmatch.fnmatchcase(name, glob):
				return 0
		return 1
	def define_func(self, name, retType, args):
		if self.filter(name):
			FunctionDefsParser.define_func(self,name,retType,args)
		else:
			print "%s: filtered out" % (name)
	def addExcludeFile(self, fname):
		"""Adds the function names from file fname to excludeList"""
		lines = open(fname).readlines()
		# remove \n from each line ...
		lines = map(lambda x: x[:-1], lines)
		# remove comments and blank lines ...
		lines = filter(lambda x: x and x[0] != '#', lines)
		for name in lines:
			self.excludeList[name] = None
	def addExcludeGlob(self, glob):
		if glob not in self.excludeGlob:
			self.excludeGlob.append(glob)

const_template = """
def __init__(self, %s _obj=None):
    if _obj: self._o = _obj; return
    %s
"""

method_template = '''
def %s(self, %s):
    """Autogenerated from C function:
       %s"""
    %s
    _ret = %s.%s(self._o, %s)
    if type(_ret) is _guppi.GuppiObjectType: _ret = _obj2inst(_ret)
    return _ret
'''

class Object:
	def __init__(self, parser, name, parent=None):
		self.name = name
		self.parent = parent
		self.lowername = toLowerStr(name)

		self.prefix = self.lowername + '_'
		parser.prefixes.append(self.lowername + '_')
		
		
		self.constructors = []
		self.methods = []
		parser.allobjects[name] = self
		parser.dependencies[name] = parent
		self.parser = parser
		
	def addmethod(self, funcname, retType, args):
		method = string.replace(funcname, self.lowername + '_', '') #remove prefix
		self.methods.append((method, retType, args[1:]))

	def addconstructor(self, funcname, args):
		self.constructors.append((funcname, args))

	def pycode(self):
		"""Generates beginning of class definition, up to and
		including the constructor."""
		if self.parent:
			parents = string.join(self.parent, ', ')
			s = "\nclass %s(%s):" % (self.name, parents)
		else: s = "\nclass %s:" % (self.name)
		s = s + self.defineinit() + self.definemethods()
		return s

	def defineinit(self):
		initcode = args = ""
		#print self.constructors
		min_args = 100
		best_constructor = None
		for cfunc, cargs in self.constructors:
			cargs = map(lambda x: x[1], cargs)
			if len(cargs) < min_args:
				min_args = len(cargs)
				args = string.join(cargs, ', ')
				best_constructor = (cfunc, args)
		if best_constructor:
			modname = self.parser.modulename
			(cfunc, args) = best_constructor
			initcode = 'self._o = %s.%s(%s)' % (modname,
							    cfunc, args)
		if args: args = args + ','
		if not initcode: # no constructor
			initcode = 'raise ValueError, "Class does not have a constructor."'
		init = const_template % (args, initcode)
		return indent(init)

	def definemethods(self):
		methdefs = []
		for name, retType, args in self.methods:
			cfunc = self.prefix + name

			cargs = [] #arguments used to show how it's invoked from C
			cargs = map(lambda x: "%s %s" % (x[0], x[1]), args)

			pyconv = []
			for pytype, pyname in args:
				if pytype in objects.keys():
					_s = "%(n)s = getattr(%(n)s, '_o', %(n)s)"
					pyconv.append(_s % {'n' : pyname})

			pyconv = string.join(pyconv, '\n    ')
			args = map(lambda x: x[1], args)
			args = string.join(args, ', ')
			cargs = string.join(cargs, ', ')
			docstring = "%s %s(self, %s)" % (retType, cfunc, cargs)

			modname = self.parser.modulename
			methdef = method_template % (name, args,
						     docstring,
						     pyconv,
						     modname,
						     cfunc, args)

			methdefs.append(methdef)
		return indent(string.join(methdefs,'\n')) + '\n'

strarray_template = "static const gchar* py_str_%s[] = {%s, NULL};\n\n"
valarray_template = "static const %s py_enum_%s[] = {%s};\n"

enum2py_template = '''
static PyObject*
%(lowname)s2py(%(ctype)s x)
{
   gint i = 0;
   while (py_str_%(lowname)s[i] != NULL &&
          py_enum_%(lowname)s[i] != x) ++i;
   if (py_str_%(lowname)s[i] != NULL)
       return PyString_FromString((gchar *)py_str_%(lowname)s[i]);
   PyErr_SetString(PyExc_ValueError, "Not a valid enum value.");
   return NULL;
}
'''

py2enum_template = '''
static %(ctype)s
py2%(lowname)s(PyObject *x)
{
   gchar* symb;
   gint i = 0;
   symb = PyString_AsString(x);
   while (py_str_%(lowname)s[i] != NULL &&
          strcmp(symb, py_str_%(lowname)s[i])) ++i;
   g_assert(py_str_%(lowname)s[i] != NULL);
   g_free(symb);
   return py_enum_%(lowname)s[i];
}

'''

class Enum:
	def __init__(self, parser, name, values):
		self.name = name
		self.lowname = toLowerStr(name)
		self.values = values
		self.parser = parser

		parser.allenums[name] = self
		self.definearrays()

	def definearrays(self):
		valnames = map(lambda x: '"%s"' % x, self.values)
		valnames = string.join(valnames, ',')
		enumvalues = string.join(self.values, ',')

		self.enum2py = "%s2py" % self.lowname
		self.py2enum = "py2%s" % self.lowname

		d = {'lowname' : self.lowname, 'ctype' : self.name}
		impl = StringIO()
		impl.write(strarray_template % (self.lowname, valnames))
		impl.write(valarray_template % (self.name, self.lowname,
		                                enumvalues))
		impl.write(enum2py_template % d)
		impl.write(py2enum_template % d)
		self.parser.impl.write(impl.getvalue())

_ind = " " * 4

def indent(text, times = 1):
	text = string.split(text, '\n')
	newtext = []
	for line in text: newtext.append(_ind * times + line)
	return string.join(newtext, '\n')


