#!/usr/bin/perl -w

my @pages = ();

sub emit_navigation_links {
    my $current_name = shift;

    foreach (@pages) {
	my ($name, $file, $content) = @$_;

	print OUT "<a href=\"./$file\">" if $name ne $current_name;
	print OUT $name;
	print OUT "</a>" if $name ne $current_name;
	print OUT "<br>\n";
    }
}

sub emit_included_file {
    my $filename = shift;

    open(INCLUDE_IN, $1) || die;
    print OUT "<pre>\n";
    while (defined (my $line = <INCLUDE_IN>) ) {
	$line =~ s/\</\&lt;/g;
	
	# try to spam-protect e-mail addresses 
	$line =~ s/@(\S+)\.(\S+)\.(\S+)/ at $1 dot $2 dot $3/g;
	$line =~ s/@(\S+)\.(\S+)/ at $1 dot $2/g;

	print OUT $line;
    }
    print OUT "</pre>\n";
}

sub emit_news {
    my $limit = shift || -1;
    my $first=1;

    open(NEWS_IN, "guppi.news") || die;
#    print OUT "<ul>\n";
    while (defined (my $date_line = <NEWS_IN>) && $limit != 0) {
	chomp $date_line;
	next unless $date_line =~ /\S/;

	my $heading_line = <NEWS_IN>;
	chomp $heading_line;

	if (!$first) {
	    print OUT "<br>";
	}
	$first = 0;

#	print OUT "<li>";
	print OUT "<font size=\"+1\">$heading_line</font> ";
	print OUT "<font size=\"-1\">($date_line)</font><br>";

	while (defined (my $content = <NEWS_IN>)) {
	    last unless $content =~ /\S/;
	    print OUT $content;
	}
	print OUT "<br>";

	--$limit;
    }
#    print OUT "</ul>\n";
}

sub emit_content {
    my $filename = shift;
    open(CON_IN, $filename) || die;
    while (defined (my $line = <CON_IN>)) {

	if ($line =~ /^<!-- include (\S+) -->$/) {

	    print STDERR "including $1\n";
	    emit_included_file($1);

	} elsif ($line =~ /^(.*)<!-- screenshot (\S+) -->(.*)$/) {

	    print STDERR "Screen shot $2\n";
	    my $sz = (stat("screenshots/$2"))[7]/1024;
	    my $alt = sprintf("A Guppi Screen Shot (%.1fk)",$sz);

	    print OUT $1;
	    print OUT "<A HREF=\"screenshots/$2\">";
	    print OUT "<IMG SRC=\"screenshots/thumb-$2\" ALT=\"$alt\">";
	    print OUT "</A>";
	    print OUT $3;

	} elsif ($line =~ /^(.*)<!-- news (\d*)\s*-->(.*)$/) {

	    emit_news($2);

	} else {
	    
	    print OUT $line;

	}
    }
    close(CON_IN);
}

sub emit_file {
    my $name = shift;
    my $content = shift;

    open(TEMPLATE_IN, "guppi-template.html");
    while (<TEMPLATE_IN>) {
	if (/INSERT:PAGETITLE/) {
	    print OUT $name;
	} elsif (/INSERT:NAVLINKS/) {
	    emit_navigation_links($name);
	} elsif (/INSERT:CONTENT/) {
	    emit_content($content);
	} elsif (/INSERT:LASTMOD/) {
	    my @s = stat($content);
	    my $lastmod = $s[9];
	    my @t = localtime($lastmod);
	    my ($m,$d,$y) = ($t[4], $t[3], $t[5]);
	    $m += 1;
	    $y += 1900;
	    print OUT "$m/$d/$y";
	} else {
	    print OUT $_;
	}
    }
    close(TEMPLATE_IN);
}

open(IN, "guppi-pages.toc") || die;
while (<IN>) {
    chomp;

    my ($name, $file, $content) = split / : /;
    next unless defined $content;

    push(@pages, [$name, $file, $content]);
}
close(IN);

foreach my $p (@pages) {
    my ($name, $file, $content) = @$p;
    printf STDERR "Building $file\n";
    open(OUT, ">$file");
    emit_file($name, $content);
    close(OUT);
}
